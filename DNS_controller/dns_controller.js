const dns   = require('dns');
var rest    = require('restler');
var bodyParser = require("body-parser");
var express = require('express');
var app     = express();
var server  = require("http").Server(app);
var exec    = require('child_process').spawn;

var Me = null;
var TheOtherGuy = null;
var HaveDNS = false;
var dnsEntry = null;
var IsDualMode = false;
var ServerMode = 'STANDALONE';
var ThisServerHostName = null;
var CustomDnsEntry = null;
var port = 3001;

var StrHB = '/heartbeat';
var StrStartSig = '/startupSignal';
var StrCustomDNSSet = '/customdnsset';
var StrCustomDNSClear = '/customdnsclear';

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.text())

GetSysParams(function(ProcessStop){
	if(ProcessStop){

	} else {
	  WaitForTrigger();
    }
});

function WaitForTrigger(){
	setTimeout(function(){
	  console.log('New system interval');
	  if(TheOtherGuy != null && TheOtherGuy != ""){
	    SendHB(function(Ack){
	      ProcessValues(Ack, function(){
		    WaitForTrigger();
	      });
	    });
	  } else {
		ProcessValues(false, function(){
		  WaitForTrigger();
		});
	  }
	}, 60000);
}

function GetSysParams(callback){
   Me = '10.211.110.131';
   TheOtherGuy = '10.211.110.132';
   dnsEntry = 'mdwcs.mtn.co.za';
   IsDualMode = 'TRUE';
   ThisServerHostName = 'mdawcsrva01.mtn.co.za';

   if(IsDualMode == 'TRUE' &&
      TheOtherGuy != null &&
      TheOtherGuy != ''){
	   if(Me != null && Me != ''){
			dns.lookup(ThisServerHostName , function(err, MyCompareIP) {
				if(MyCompareIP == Me){
					if(dnsEntry != null && dnsEntry != ''){
					  console.log('All good to GO');
					  return callback(false);
					} else {
					  console.log('DNS name not present in server_profile.sh');
					  return callback(true);
					}
				} else {
				  console.log('My IP does not match this servers IP address ');
				  return callback(true);
			    }
			});
	   } else {
		 console.log('Could not find my ipddress in server_profile.sh');
		 return callback(true);
	   }
   } else {
	 console.log('System either not set to DUAL_MODE or the OTHER_NODE IP address does not exist in server_profile.sh');
	 return callback(false);
   }
}

function ProcessValues(OtherServerOnline, callback){
	dns.lookup(dnsEntry , function(err, DNS_IP){
	   if(!err){
		 if(DNS_IP != null && DNS_IP != ''){

		   if(CustomDnsEntry != null){
		     DNS_IP = CustomDnsEntry;
		   }

		   var NewState = null;
		   var OldState = ServerMode;
		   HaveDNS = false;
		   if(DNS_IP == Me){
			   console.log('DNS entry is pointing to ME - DNS IP: ' + DNS_IP);
			   NewState = 'MASTER';
			   HaveDNS = true;
		   } else if(DNS_IP == TheOtherGuy){
			   console.log('DNS entry is pointing to the TheOtherGuy - DNS IP: ' + DNS_IP);
			   NewState = 'SLAVE';
		   } else {
			   console.log('DNS entry does not seem to be pointing to any of us so I am now STANDALONE - DNS IP: ' + DNS_IP);
			   NewState = 'STANDALONE';
		   }

		   if(!OtherServerOnline){
			   console.log("TheOtherGuy is not online");
			   NewState = 'STANDALONE';
		   }

		   console.log("I'm " + NewState + " ||||| -> My old state is " + OldState);

		   if(NewState == 'MASTER'){
		     if(OldState == 'STANDALONE'){
			   console.log("Checking whether my services are running. If not then I'll start them up");
			   CheckForStartup(NewState, OldState, function(){
			     return callback();
			   });
			 } else if(OldState == 'SLAVE'){
			   console.log("Waiting for signal from TheOtherGuy (" + TheOtherGuy + ") to startup my services");
			   return callback();
			 } else {
			   return callback();
		     }
		   } else if(NewState == 'SLAVE'){
		     if(OldState == 'STANDALONE'){
			   console.log("Checking whether my services are running. If they are then I'll stop them");
			   CheckForStopping(NewState, OldState, false, function(){
			     return callback();
			   });
			 } else if(OldState == 'MASTER'){
			   console.log("Need to stop all my services then alert TheOtherGuy (" + TheOtherGuy + ") to startup his services");
			   CheckForStopping(NewState, OldState, true, function(){
			     return callback();
			   });
			 } else {
			   return callback();
		     }
		   } else if(NewState == 'STANDALONE'){
			 //console.log("carry on as normal");
			 if(HaveDNS){
			   console.log("Checking whether my services are running. If not then I'll start them up");
			   CheckForStartup(NewState, OldState, function(){
			     return callback();
			   });
			 } else {
			   console.log("Checking whether my services are running. If they are then I'll stop them");
			   CheckForStopping(NewState, OldState, false, function(){
			     return callback();
			   });
			 }
		   }
		 } else {
		   console.log('Failed to Resolve Hostname to IP');
		   return callback();
		 }
	   } else {
		 console.log('Failed to Resolve Hostname to IP');
		 return callback();
	   }
	});
}

function CheckForStopping(NewState, OldState, SendSignal, callback){
  StepDownMongoDbToSlave(function(){
	  var StopServices = false;
	  const { spawn } = require('child_process');
	  const ls = spawn('pm2', ['describe', 'NEW_UI']);

	  ls.stdout.on('data', (data) => {
		console.log(`stdout: ${data}`);
		console.log('processes running');
		StopServices = true;
	  });

	  ls.stderr.on('data', (data) => {
		console.log(`stderr: ${data}`);
		console.log('processes NOT running');
	  });

	  ls.on('close', (code) => {
		if(StopServices){
		  var child = exec("pm2", ["delete", process.env.SYS_DIR + "services_startup.config.js"]);

		  child.on('close', function(code){
			console.log('My services stopped, check pm2 list');
			if(SendSignal){
			  SendStartupSignal(NewState, OldState, function(){
				return callback();
			  });
			} else {
			  ServerMode = NewState;
			  return callback();
			}
		  });

		  child.on('error', function(code){
			console.log('Services failed to stop, check pm2 list');
			ServerMode = OldState;
			return callback();
		  });
		} else {
		  if(SendSignal){
			SendStartupSignal(NewState, OldState, function(){
			  return callback();
			});
		  } else {
			ServerMode = NewState;
			return callback();
		  }
		}
	  });
  });
}

function StepDownMongoDbToSlave(callback){
  var child = exec(process.env.SYS_DIR + "mongodb_control.sh");

  child.on('close', function(code){
	console.log('mongoDB should be in slave mode now');
	setTimeout(function(){
	  return callback();
    }, 30000);
  });

  child.on('error', function(code){
	console.log('rs.stepDown() Failed, mongoDB already in slave mode');
	setTimeout(function(){
	  return callback();
	}, 30000);
  });
}

function CheckForStartup(NewState, OldState, callback){
  var Startup = false;
  const { spawn } = require('child_process');
  const ls = spawn('pm2', ['describe', 'NEW_UI']);

  ls.stdout.on('data', (data) => {
	console.log(`stdout: ${data}`);
	console.log('processes running');
  });

  ls.stderr.on('data', (data) => {
    console.log(`stderr: ${data}`);
    console.log('processes NOT running');
    Startup = true;
  });

  ls.on('close', (code) => {
    if(Startup){
      var child = exec("pm2", ["start", /*process.env.SYS_DIR +*/ "/apps/wcs/source/services_startup.config.js"]);

	  child.on('close', function(code){
		console.log('My services are started, check pm2 list');
		ServerMode = NewState;
		return callback();
	  });

	  child.on('error', function(code){
		console.log('Services failed to start, check pm2 list');
		ServerMode = OldState;
		return callback();
	  });
	} else {
	  ServerMode = NewState;
	  return callback();
    }
  });
}

function SendStartupSignal(NewState, OldState, callback){
  rest.post('http://' + TheOtherGuy + ':' + port + StrStartSig, {
    data: {HB:'START'}
  }).on('complete', function (result) {
    if(result instanceof Error) {
      console.log(result);
      ServerMode = OldState;
    } else {
	  ServerMode = NewState;
    }
    return callback();
  });
}

function SendHB(callback){
  rest.post('http://' + TheOtherGuy + ':' + port + StrHB, {
    data: {HB:'ALIVE'}
  }).on('complete', function (result) {
    if(result instanceof Error) {
	  return callback(false);
	} else {
	  return callback(true);
	}
  });
}

app.listen(port, function(){
	console.log('DNS CONTROLLER LISTENING ON PORT 3001');
});

app.post(StrStartSig, function(req, res){
	console.log('Need to Start Your Services');

    var child = exec("pm2", ["start", process.env.SYS_DIR + "services_startup.config.js"]);

	child.on('close', function(code){
		console.log('services started');
		ServerMode = 'MASTER';
		res.send({data: 'Services started on ' + Me});
	});

	child.on('error', function(code){
		console.log('Services failed to start');
		res.send({error: 'Services failed to start on ' + Me});
	});
});

app.post(StrHB, function(req, res){
	console.log('Heartbeat received from ' + TheOtherGuy);

	res.send({data: 'ACK'});
});

app.post(StrCustomDNSSet, function(req, res){
	console.log('Received signal to set custom dns');
	CustomDnsEntry = req.body;
	console.log('CustomDnsEntry = ' + CustomDnsEntry);
	res.send({data: 'Success'});
});

app.post(StrCustomDNSClear, function(req, res){
	console.log('Received signal to clear custom dns');
    CustomDnsEntry = null;
    console.log('CustomDnsEntry = ' + CustomDnsEntry);
	res.send({data: 'Success'});
});

