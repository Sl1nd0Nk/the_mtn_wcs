module.exports = {
   apps : [
    {
      name      : "PACKSTN",
      cwd       : "/apps/wcs/source/services/desktop_interface/",
      script    : "UI.js",
      instances : -1,
      exec_mode : "cluster",
      interpreter_args: "--max-old-space-size=4096"
    },
    {
      name      : "NEW_UI",
      cwd       : "/apps/wcs/source/services/New_UI/",
      script    : "NEW_UI.js"
   },
    {
      name      : "PICKLIST_GEN",
      cwd       : "/apps/wcs/source/services/wms_api/",
      script    : "pickListGenerator.js"
    },
    {
      name      : "ORDER_HEADER",
      cwd       : "/apps/wcs/source/services/wms_api/",
      script    : "OrderHeaderListener.js"
    },
    {
      name      : "ORDER_SHIP_REQUIRE",
      cwd       : "/apps/wcs/source/services/wms_api/",
      script    : "OrderShipReqListener.js"
    },
    {
      name      : "PACK_WEIGHT_UPDATE",
      cwd       : "/apps/wcs/source/services/wms_api/",
      script    : "PackListWeightsListener.js"
    },
    {
      name      : "SERIAL_UPDATE",
      cwd       : "/apps/wcs/source/services/wms_api/",
      script    : "SerialListener.js"
    },
    {
      name      : "SERIAL_PULL",
      cwd       : "/apps/wcs/source/services/wms_api/",
      script    : "pull_serials_from_wms.js"
    },
    {
      name      : "SERIAL_PUSH",
      cwd       : "/apps/wcs/source/services/wms_api/",
      script    : "push_serials_to_wms.js"
    },
    {
      name      : "ERP_STATUS_SENDER",
      cwd       : "/apps/wcs/source/services/wms_update/",
      script    : "WmsErpUpdate.js"
    },
    {
      name      : "COURIER_CARTON_SENDER",
      cwd       : "/apps/wcs/source/services/wms_update/",
      script    : "WmsCourierUpdate.js"
    },
    {
      name      : "PTL_RECEIVER",
      cwd       : "/apps/wcs/source/services/Pick_To_Light/",
      script    : "ptl_rcv.js",
      max_restarts: 500
    },
    {
      name      : "PTL_SENDER",
      cwd       : "/apps/wcs/source/services/Pick_To_Light/",
      script    : "ptl_snd.js",
      max_restarts: 500
    },
    {
      name      : "PTL_ENDPOINT",
      cwd       : "/apps/wcs/source/services/Pick_To_Light/",
      script    : "ptl_end_point.js",
      max_restarts: 500
    },
    {
      name      : "CHUTE_MAN",
      cwd       : "/apps/wcs/source/services/New_UI/",
      script    : "chute_man.js",
      max_restarts: 500
    },
    {
      name      : "CONVEYOR",
      cwd       : "/apps/wcs/source/services/conveyor/Apex_Lib/",
      script    : "real.js",
      interpreter_args: "--max-old-space-size=4096",
      log_date_format: "YYYY-MM-DD HH:mm:ss"
    },
    {
      name      : "PULLWALL_UI",
      cwd       : "/apps/wcs/source/services/New_UI/",
      script    : "pullWallserver.js",
      interpreter_args: "--max-old-space-size=4096",
      max_restarts: 500
    },
    {
      name      : "PUTWALL_CONFIRM",
      cwd       : "/apps/wcs/source/services/New_UI/",
      script    : "putWallConfirm.js",
      interpreter_args: "--max-old-space-size=4096",
      max_restarts: 500
    },
    {
      name      : "TOTE_CONSOL",
      cwd       : "/apps/wcs/source/services/wms_update/",
      script    : "ToteOrderConsol.js",
      interpreter_args: "--max-old-space-size=4096",
      max_restarts: 500
    },
    {
      name      : "VSURDT",
      cwd       : "/apps/wcs/source/services/RDT/",
      script    : "rdtServer.js",
      max_restarts: 500,
      log_date_format: "YYYY-MM-DD HH:mm"
    },
    {
      name      : "VSU_SERVER",
      cwd       : "/apps/wcs/source/services/VSU/",
      script    : "vsuServer.js",
      max_restarts: 500,
      log_date_format: "YYYY-MM-DD HH:mm"
    },
    {
      name      : "WMS_STATUS_UPDATE",
      cwd       : "/apps/wcs/source/services/wms_update/",
      script    : "WMSUpdatePickLists.js",
      max_restarts: 500
    },
    {
      name      : "PICK_TROLLEY",
      cwd       : "/apps/wcs/source/services/pick_trolley/",
      script    : "pickTrolleyServer.js",
      max_restarts: 500
    },
    {
      name      : "CANCEL_REQ",
      cwd       : "/apps/wcs/source/services/wms_api/",
      script    : "cancel_request.js",
      max_restarts: 500
    },
    {
      name      : "GEN_CANCEL_Q",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenCancelInvoice.js",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_1",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_1",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_2",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_2",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_3",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_3",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_4",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_4",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_5",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_5",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_6",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_6",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_7",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_7",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_8",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_8",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_9",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_9",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_10",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_10",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_11",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_11",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_12",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_12",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_13",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_13",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_14",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_14",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_15",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_15",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_16",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_16",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_17",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_17",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_18",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_18",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_19",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_19",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_20",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_20",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_21",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_21",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_22",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_22",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_23",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_23",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_24",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_24",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_25",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_25",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_26",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_26",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_27",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_27",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_28",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_28",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_29",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_29",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_30",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_30",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_31",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_31",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_32",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_32",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_33",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_33",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_34",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_34",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_35",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_35",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_36",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_36",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "GEN_INV_41",
      cwd       : "/apps/wcs/source/services/PrintQueueManager/",
      script    : "GenInvoice.js",
      args      : "PACKING_41",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_restarts: 500
    },
    {
      name      : "CHUTE_QTY",
      cwd       : "/apps/wcs/source/services/chutes/",
      script    : "chute_calc.js",
      max_restarts: 500
    }
   ]
}
