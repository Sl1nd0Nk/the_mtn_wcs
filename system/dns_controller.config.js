module.exports = {
   apps : [
    {
      name      : "DNS_CONTROLLER",
      cwd       : "/apps/wcs/source/services/DNS_controller/",
      script    : "dns_controller.js",
      max_restarts: 500
    }
   ]
}
