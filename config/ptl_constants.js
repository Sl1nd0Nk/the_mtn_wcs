exports.BlinkColours =
{
	RED    : '200',
	GREEN  : '020',
	BLUE   : '002',
	YELLOW : '220',
	VIOLET : '202',
	MAGENTA: '022',
	WHITE  : '222'
};

exports.SteadyColours =
{
	RED     : '100',
	GREEN   : '010',
	BLUE    : '001',
	YELLOW  : '110',
	VIOLET  : '101',
	MAGENTA : '011',
	WHITE   : '111',
	NO_COLOR: '000'
};

exports.Status =
{
	NEW   : 'NEW',
	LOGIN : 'LOGIN',
	PICK  : 'PICK',
  PICKED: 'PICKED',
  ERROR : 'ERROR',
  DONE  : 'DONE'
};

exports.ZoneDisplayValue =
{
	LOGIN  : 'LOGIN',
	PICK   : 'PICK',
  DONE   : 'DONE',
  NOTHING: ' '
};

exports.Prefix =
{
	ZONE      : 'PZ',
	PALLET_LOC: 'PP',
  CASE_LOC  : 'CP',
  REDIS_ZONE: 'ZONE:'
};