var moment         = require('moment');
var mongoose       = require('mongoose');
mongoose.Promise   = require('bluebird');
var config         = require('ui');

var Stations = require('packStationModel');

mongoose.connect(config.mongo.url, function(err) {
  if(err) {
    console.error( 'MONGO CONNECT ERROR ' + err);
  } else {
    console.log(' Connected To Mongo');
    CheckInActivePackStations();
  }
}); /* mongoose.connect */

function diff_minutes(dt2, dt1){
  var diff =(dt2.getTime() - dt1.getTime()) / 1000;
  diff /= 60;
  return Math.abs(Math.round(diff));
}

function CheckInActivePackStations() {
  var cursor = Stations.find({active: true}).cursor();

  cursor.on('data', function(Station) {

    if(Station){
      cursor.pause();

      var date1 = Station.updated_at;
      var date2 = new Date();
      var diff = diff_minutes(date1, date2);

      if(diff > 10){
        console.log('Station ' + Station.stationNumber + ' last update was ' + diff + ' minutes ago')
        Station.active = false;
        Station.save(function(err, savedDoc) {
          console.log('Successfully deactivated station ' + savedDoc.stationNumber);
	      cursor.resume();
        });
      } else {
        cursor.resume();
      }
    }
  });

  cursor.on('end', function(){
    WaitForTrigger();
  });
} /* CheckInActivePackStations */

/* Intervals per minute */
function WaitForTrigger() {
  setTimeout(function() {
    CheckInActivePackStations();
  }, 30000);
} /* WaitForTrigger */
