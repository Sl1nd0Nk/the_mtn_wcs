var express        = require('express');
var server         = require("http").Server(app);
var fs             = require('fs');
var formidable     = require("formidable");
var moment         = require('moment');
var redis          = require('redis');
var client         = redis.createClient();
var redisClient    = redis.createClient();
var session        = require('client-sessions');
var mongoose       = require('mongoose');
var templatesjs    = require('templatesjs');
var async          = require('async');

var app            = express();

var PwLib          = require('classPutwallLocs');
var logLib	       = require('classLogging');
var serialLib 	   = require('classSerial');
var paramLib       = require('classParams');
var prnLib	       = require('classPrinting');
var pickListLib	   = require('classPicklist');
var pwStnLib	   = require('classPullwallStn');
var UserLib        = require('classUser');
var PackUpdate     = require('PackUpdatesModel');

var PwLocs         = new PwLib();
var log            = new logLib();
var serls          = new serialLib();
var param          = new paramLib();
var prn            = new prnLib();
var pickList       = new pickListLib();
var PwStn          = new pwStnLib();
var sysuser	       = new UserLib();

var mongoUrl       = require('ui').mongo.url;

mongoose.connect(mongoUrl, function(err){
	if(err){
		log.WriteToFile(null, 'MONGO CONNECT ERROR ' + err);
	} else {
		log.WriteToFile(null, 'Connected To Mongo');
		app.listen(8085);
		log.WriteToFile(null, "pullwall server is listening on port " + 8085);
	}
}); /* mongoose.connect */

app.use(session({
	cookieName: 'session',
	secret: 'eg[isfd-8yF9-7w2315df{}+Ijsli;;to8',
	duration: 30 * 60 * 1000,
	activeDuration: 5 * 60 * 1000,
	httpOnly: true,
	secure: true,
	ephemeral: true
}));

app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
	var list = PwStn.FormHTMLReplaceList();

  	if(!req.session.username){
		Login(req, res);
  	} else{
		PwStn.FindOne({stationID: req.session.Station}, function(Resp){
			var Stn = Resp.pwStn;
			if(!Resp.Err && Stn){
				list.Logout = "";
				list.EndLogout = "";
				ProcessStationActivity(list, req, res, Stn);
			} else {
				ShowStationError(list, req, res);
			}
		});
  	}
});

app.post('/', function(req, res){
	var list = PwStn.FormHTMLReplaceList();

  	if(!req.session.username){
		Login(req, res);
  	} else{
		PwStn.FindOne({stationID: req.session.Station}, function(Resp){
			var Stn = Resp.pwStn;
			if(!Resp.Err && Stn){
				list.Logout = "";
				list.EndLogout = "";
				ProcessStationActivity(list, req, res, Stn);
			} else {
				ShowStationError(list, req, res);
			}
		});
  	}
});

app.post('/login', function(req, res){
	var form = new formidable.IncomingForm();
	var list = PwStn.FormHTMLReplaceList();

	form.parse(req, function (err, fields, files){
		if((fields.uid != null) && (fields.uid != "") && (fields.psw != null) && (fields.psw != "")){
			sysuser.FindOne({'username': fields.uid, 'password': fields.psw}, function(Resp){
				var doc = Resp.User;
				if(!Resp.Err && doc){
					if((fields.stnid != null) && (fields.stnid != "")){
						PwStn.FindOne({'stationID': fields.stnid}, function(Resp){
							var Stn = Resp.pwStn;
							if(!Resp.Err && Stn){
								req.session.Station = Stn.stationID;
								req.session.user = doc.name;
								req.session.username = doc.username;
								list.Logout = "";
								list.EndLogout = "";
								ProcessStationActivity(list, req, res, Stn);
							} else {
								log.WriteToFile(null, 'Unable to find station ' + fields.stnid + ' supplied by user ' + fields.stnid);
								ShowStationError(list, req, res);
							}
						})
					} else {
						log.WriteToFile(null, 'No station Id supplied by user ' + fields.stnid);
						ShowStationError(list, req, res);
					}
				} else {
					log.WriteToFile(null, 'User supplied incorrect login info');
					list.StnID = "";
					list.EndStnID = "";
					list.Instruction = '<label style="color: red">incorrect username/password supplied</label>' + '<br><br>';
					SetAndRenderResponse(list, req, res);
				}
			});
		} else {
			log.WriteToFile(null, 'User supplied incorrect login info');
			list.StnID = "";
			list.EndStnID = "";
			list.Instruction = '<label style="color: red">incorrect username/password supplied</label>' + '<br><br>';
			SetAndRenderResponse(list, req, res);
		}
	});
});

app.post('/transadd', function(req, res){
  	if(!req.session.username){
		Login(req, res);
  	} else{
		var form = new formidable.IncomingForm();
		var list = PwStn.FormHTMLReplaceList();

		list.endPoint = "transadd";
		list.Logout = "";
		list.EndLogout = "";
		list.Input = "";
		list.EndInput = "";

		form.parse(req, function (err, fields, files){
			if((fields.input != null) && (fields.input != "")){
				log.WriteToFile(null, 'User ' + req.session.user + ' scanned transporter ' + fields.input);
				var compare = fields.input;
				if(parseInt(compare[0]) != 7 || compare.length != 8){
					list.ScannedStnID = req.session.Station;
					list.TransID = "";
					list.EndTransID = "";
					list.Instruction = '<label style="color: red">Invalid transporter ID scanned</label><br><br>';
                    			SetAndRenderResponse(list, req, res);
					return;
				} else {
					PwStn.FindOne({'stationID': req.session.Station}, function(Resp){
						var Stn = Resp.pwStn;
						if(!Resp.Err && Stn){
							AddTransporterToStation(Stn, fields.input, function(Result){
								if(Result.error){
									list.ScannedStnID = req.session.Station;
									list.TransID = "";
									list.EndTransID = "";
                                					list.Instruction = '<label style="color: red">Please scan an empty transporter</label><br><br>';
									SetAndRenderResponse(list, req, res);
								} else {
									ProcessStationActivity(list, req, res, Result.Stn);
								}
							});
						} else {
							ShowStationError(list, req, res);
						}
					});
            	}
			} else {
				list.ScannedStnID = req.session.Station;
				list.TransID = "";
				list.EndTransID = "";
				list.Instruction = '<label style="color: red">Please scan transporter</label>' + '<br><br>';
				SetAndRenderResponse(list, req, res);
			}
		});
	}
});

app.get('/login', function(req, res){
	WEB_CallPageRedirect(res, '/');
});

app.get('/transadd', function(req, res){
	WEB_CallPageRedirect(res, '/');
});

app.post('/logout', function(req, res){
	delete req.session.username;
	delete req.session.Station;
	delete req.session.user;

    Login(req, res);
});

app.get('/logout', function(req, res){
	WEB_CallPageRedirect(res, '/');
});

app.get('/releaseTr', function(req, res){
	WEB_CallPageRedirect(res, '/');
});

app.post('/releaseTr', function(req, res){
	var list = PwStn.FormHTMLReplaceList();

	list.endPoint = "transadd";
	list.Logout = "";
	list.EndLogout = "";
	list.Input = "";
	list.EndInput = "";

	PwStn.FindOne({'stationID': req.session.Station}, function(Resp){
		var Stn = Resp.pwStn;
		if(!Resp.Err && Stn){
			ClearTransporterFromStation(Stn, list, req, res);
		} else {
			ShowStationError(list, req, res);
		}
	});
});

app.get('/parcelconfirm', function(req, res){
	WEB_CallPageRedirect(res, '/');
});

app.post('/parcelconfirm', function(req, res){
  	if(!req.session.username){
		Login(req, res);
  	} else{
		var form = new formidable.IncomingForm();
		var list = PwStn.FormHTMLReplaceList();
		list.endPoint = "parcelconfirm";
		list.Logout = "";
		list.EndLogout = "";
		list.Input = "";
		list.EndInput = "";

		form.parse(req, function (err, fields, files){
	    	if((fields.input != null) && (fields.input != "")){
				PwStn.FindOne({'stationID': req.session.Station}, function(Resp){
					var Stn = Resp.pwStn;
					if(!Resp.Err && Stn){
						var RawCartonString = fields.input;
						var CartonArr = RawCartonString.split('M'); //Remove the NTM1000...
						var CartonStr = '';
						if(CartonArr.length <= 1){
							CartonArr = RawCartonString.split('m'); //Remove the ntm1000...
							if(CartonArr.length > 1){
							  CartonStr = CartonArr[1];
							} else {
								CartonStr = CartonArr[0];
							}
						} else {
							CartonStr = CartonArr[1];
						}

						log.WriteToFile(null, "Scanned Carton: " + CartonStr);
						pickList.FindOne({'orders.cartonID': CartonStr}, function(Resp){
							var PL = Resp.PL;
							if(!Resp.Err && PL){
								if(PL.orders[0].orderID == Stn.orderID){
									Stn.parcelConfirmed = true;
									PwStn.Update(Stn, function(Resp){
										var savedStn = Resp.SavedDoc;
										ProcessStationActivity(list, req, res, savedStn);
									});
								} else {
									list.ScannedStnID = req.session.Station;
									list.ScannedTransporter = '<label style="color: Green; font-size: 13px">TransporterID: <b>' + Stn.transporter + '</b><br/><br/>';
									list.ShowBBox = "";
									list.EndShowBBox = "";
									ShowCartonError(list, req, res);
								}
							} else {
								list.ScannedStnID = req.session.Station;
								list.ScannedTransporter = '<label style="color: Green; font-size: 13px">TransporterID: <b>' + Stn.transporter + '</b><br/><br/>';
								list.ShowBBox = "";
								list.EndShowBBox = "";
								ShowCartonError(list, req, res);
							}
						});
					} else {
						ShowStationError(list, req, res);
					}
				});
			} else {
				list.ScannedStnID = req.session.Station;
				list.ShowBBox = "";
				list.EndShowBBox = "";
				list.Parcel = "";
				list.EndParcel = "";
				list.Instruction = '<label style="color: red">Please Scan Parcel barcode</label>' + '<br><br>';
				SetAndRenderResponse(list, req, res);
			}
		});
  	}
});

app.get('/idle', function(req, res){
	WEB_CallPageRedirect(res, '/');
});

app.post('/idle', function(req, res){
  	if(!req.session.username){
		Login(req, res);
  	} else{
		var list = PwStn.FormHTMLReplaceList();
		list.endPoint = "idle";
		list.Logout = "";
		list.EndLogout = "";
		list.RelBtn = "";
		list.EndRelBtn = "";

		PwStn.FindOne({'stationID': req.session.Station}, function(Resp){
			var Stn = Resp.pwStn;
			if(!Resp.Err && Stn){
				list.ScannedStnID = req.session.Station;
				list.ScannedTransporter = '<label style="color: Green; font-size: 13px">TransporterID: <b>' + Stn.transporter + '</b><br/><br/>';
				list.ShowBBox = "";
				list.EndShowBBox = "";
				FindParcelToRetrieve(Stn, function(Details){
					if(Details.result == 'Loc Found'){
						list.Instruction = '<br /><label style="color: blue">confirm loc ' + Details.Station.Location + ' with <b>light on</b> and place items in bag</label>' + '<br><br>';
						list.Parcel = "";
						list.EndParcel = "";
						list.endPoint = "parcelconfirm";
						list.Input = "";
						list.EndInput = "";
						SetAndRenderResponse(list, req, res);
					} else {
						list.Idle = "";
						list.EndIdle = "";
						list.endPoint = "idle";
						list.ShowAlert = "";
						SetAndRenderResponse(list, req, res);
					}
				});
			} else {
				ShowStationError(list, req, res);
			}
		});
	}
});

app.get('/trconfirm', function(req, res){
	WEB_CallPageRedirect(res, '/');
});

app.post('/trconfirm', function(req, res){
  	if(!req.session.username){
		Login(req, res);
 	 } else {
		var form = new formidable.IncomingForm();
		var list = PwStn.FormHTMLReplaceList();
		list.endPoint = "trconfirm";
		list.Logout = "";
		list.EndLogout = "";
		list.Input = "";
		list.EndInput = "";

		form.parse(req, function (err, fields, files){
			if((fields.input != null) && (fields.input != "")){
				PwStn.FindOne({'stationID': req.session.Station}, function(Resp){
					var Stn = Resp.pwStn;
					if(!Resp.Err && Stn){
						list.ScannedStnID = req.session.Station;
						list.ScannedTransporter = '<label style="color: Green; font-size: 13px">TransporterID: <b>' + Stn.transporter + '</b><br/><br/>';
						list.ShowBBox = "";
						list.EndShowBBox = "";
						if(Stn.transporter == fields.input){
							pickList.FindOne({'orders.orderID': Stn.orderID, 'Status': {$in:['STORED', 'PACKED']}}, function(Resp){
								var PL = Resp.PL;
								if(!Resp.Err && PL){
									PL.Status = 'PACKED';
									PL.PackUpdateRequired = true;
									PL.PackedDate = moment(new Date()).format('YYYY-MM-DD HH:mm');
									PL.transporterID = Stn.transporter;
									PL.orders[0].transporterID = Stn.transporter;
									PL.orders[0].useTransporter = true;
									PL.orders[0].packComplete = true;

									CreatePackUpdate(PL);

									pickList.Update(PL, function(Resp){
										var OrderData = PL.orders[0];
										serls.UpdateSerialsToHost(OrderData, function(){
											log.WriteToFile(null, "Serial update done from /trconfirm");
										});

										Stn.orderID = null;
										Stn.parcelConfirmed = false;
										Stn.trConfirmed = false;
										Stn.Location = null;
										PwStn.Update(Stn, function(Resp){
											var savedStn = Resp.SavedDoc;
											FindParcelToRetrieve(savedStn, function(Details){
												if(Details.result == 'Loc Found'){
													list.Instruction = '<br /><label style="color: blue">confirm loc ' + Details.Station.Location + ' with <b>light on</b> and place items in bag</label>' + '<br><br>';
													list.Parcel = "";
													list.EndParcel = "";
													list.endPoint = "parcelconfirm";
													list.Input = "";
													list.EndInput = "";
													list.RelBtn = "";
													list.EndRelBtn = "";
													SetAndRenderResponse(list, req, res);
												} else {
													list.Idle = "";
													list.EndIdle = "";
													list.endPoint = "idle";
													list.ShowAlert = "";
													list.RelBtn = "";
													list.EndRelBtn = "";
													SetAndRenderResponse(list, req, res);
												}
											});
										});
									});
								} else {
								}
							});
						} else {
							list.ScannedStnID = req.session.Station;
							list.ScannedTransporter = '<label style="color: Green; font-size: 13px">TransporterID: <b>' + Stn.transporter + '</b><br/><br/>';
							list.ShowBBox = "";
							list.EndShowBBox = "";
							list.Instruction = '<label style="color: red">Scanned Transporter is not at station</label>' + '<br><br>';
							ShowTransporterConfirm(list, Stn, req, res);
						}
					} else {
						ShowStationError(list, req, res);
					}
				});
			} else {
				list.ScannedStnID = req.session.Station;
				list.ScannedTransporter = '<label style="color: Green; font-size: 13px">TransporterID: <b>' + Stn.transporter + '</b><br/><br/>';
				list.ShowBBox = "";
				list.EndShowBBox = "";
				list.Instruction = '<label style="color: red">Scanned Transporter is not at station</label>' + '<br><br>';
				ShowTransporterConfirm(list, Stn, req, res);
			}
		});
	}
});

function ProcessStationActivity(list, req, res, Stn){
	if(Stn.transporter != null){
		list.ScannedStnID = Stn.stationID;
		list.ScannedTransporter = '<label style="color: Green; font-size: 13px">TransporterID: <b>' + Stn.transporter + '</b><br/><br/>';
		list.ShowBBox = "";
		list.EndShowBBox = "";

		if(Stn.orderID != null){
			if(Stn.Location != null){
				var num = Stn.Location;
				var strLoc = num.toString();
				if(strLoc.length <= 2){
				    strLoc = "00" + strLoc;
				} else {
				  if(strLoc.length <= 3){
					strLoc = "0" + strLoc;
				  } else {
					strLoc = Stn.Location;
				  }
				}

				PwLocs.LightupLoc(strLoc, '_ON_GREEN', Stn.stationID, function(Result){
					if(Result.error){
						UpdateLightError(Stn.Location, function(){
							Stn.Location = null;
							Stn.orderID = null;
							Stn.parcelConfirmed = false;
							Stn.trConfirmed = false;
							PwStn.Update(Stn, function(Resp){
								var SavedStn = Resp.SavedDoc;
								ProcessStationActivity(list, req, res, SavedStn);
							});
						});
					} else {
						list.Instruction = '<br /><label style="color: blue">confirm loc ' + Stn.Location + ' with <b>light on</b> and place items in bag</label>' + '<br><br>';
						list.Parcel = "";
						list.EndParcel = "";
						list.endPoint = "parcelconfirm";
						list.Input = "";
						list.EndInput = "";
						list.RelBtn = "";
						list.EndRelBtn = "";
				    		SetAndRenderResponse(list, req, res);
					}
				});
			} else {
				if(!Stn.parcelConfirmed){
					list.Parcel = "";
					list.EndParcel = "";
				  	list.endPoint = "parcelconfirm";
					list.Input = "";
					list.EndInput = "";
					list.RelBtn = "";
					list.EndRelBtn = "";
					SetAndRenderResponse(list, req, res);
				} else {
					if(!Stn.trConfirmed){
						ShowTransporterConfirm(list, Stn, req, res);
					} else {
						FindParcelToRetrieve(Stn, function(Details){
							if(Details.result == 'Loc Found'){
								list.Instruction = '<br /><label style="color: blue">confirm loc ' + Details.Station.Location + ' with <b>light on</b> and place items in bag</label>' + '<br><br>';
								list.Parcel = "";
								list.EndParcel = "";
								list.endPoint = "parcelconfirm";
								list.Input = "";
								list.EndInput = "";
								list.RelBtn = "";
								list.EndRelBtn = "";
								SetAndRenderResponse(list, req, res);
							} else {
								list.Idle = "";
								list.EndIdle = "";
								list.endPoint = "idle";
								list.ShowAlert = "";
								list.RelBtn = "";
								list.EndRelBtn = "";
								SetAndRenderResponse(list, req, res);
							}
						});
					}
				}
			}
		} else {
			FindParcelToRetrieve(Stn, function(Details){
				if(Details.result == 'Loc Found'){
					list.Instruction = '<br /><label style="color: blue">confirm loc ' + Details.Station.Location + ' with <b>light on</b> and place items in bag</label>' + '<br><br>';
					list.Parcel = "";
					list.EndParcel = "";
					list.endPoint = "parcelconfirm";
					list.Input = "";
					list.EndInput = "";
					list.RelBtn = "";
					list.EndRelBtn = "";
					SetAndRenderResponse(list, req, res);
				} else {
					list.Idle = "";
					list.EndIdle = "";
					list.endPoint = "idle";
					list.ShowAlert = "";
					list.RelBtn = "";
					list.EndRelBtn = "";
					SetAndRenderResponse(list, req, res);
				}
			});
		}
	} else {
		list.ScannedStnID = req.session.Station;
		list.TransID = "";
		list.EndTransID = "";
		list.endPoint = "transadd";
		list.Input = "";
		list.EndInput = "";
		SetAndRenderResponse(list, req, res);
	}
} /* ProcessStationActivity */

function Login(req, res){
	var list = PwStn.FormHTMLReplaceList();

	list.StnID = "";
	list.EndStnID = "";
	SetAndRenderResponse(list, req, res);

} /* Login */

function ClearTransporterFromStation(Stn, list, req, res){
	UpDateTransporterExpectedWeight(Stn.transporter, function(){
		Stn.transporter = null;
		Stn.courier = null;

		PwStn.Update(Stn, function(Resp){
			var SavedStn = Resp.SavedDoc;

			/* Show textbox to add a transporter */
			list.ScannedStnID = req.session.Station;
			list.TransID = "";
			list.EndTransID = "";
			list.Input = "";
			list.EndInput = "";
			SetAndRenderResponse(list, req, res);
		});
	});
} /* ClearTransporterFromStation */

function UpDateTransporterExpectedWeight(transporter, callback){
	pickList.Find({'transporterID': transporter}, function(Resp){
		var docArr = Resp.pickListArr;
		if(!Resp.Err && docArr){
			CalcTotalTransporterWeight(docArr, transporter, function(TrObj){
				var qSetEachParcelWeight1  = async.queue(SetEachParcelWeight1, docArr.length);
				let x = 0;
				while(x < docArr.length){
					qSetEachParcelWeight1.push({doc: docArr[x], TrObj: TrObj}, function (err, data) {
						if(err){
							console.log('Failed setting transporter weight for parcel. | Err: ' + err);
						} else {
							console.log('Finished setting parcel ' + data.toteID + ' transporter weight');
						}
						//return callback();
					});
					x++;
				}

				qSetEachParcelWeight1.drain = function(){
				 	console.log('Finished setting all parcels transporter weights');
					return callback();
				}
				/*SetEachParcelWeight(docArr, 0, TrObj, function(){
					return callback();
				});*/
			});
		} else {
			return callback();
		}
	});
} /* UpDateTransporterExpectedWeight */

function SetEachParcelWeight1(task, callback){
	let doc = task.doc;
	let TrObj = task.TrObj;

	if(doc && TrObj){
		doc.orders[0].transporterWeight = (TrObj.EmptyTransporter + TrObj.TransporterWeight);
		doc.orders[0].minWeightLimit = (TrObj.EmptyTransporter + TrObj.TransporterWeight) - TrObj.TranLowerTol;
		doc.orders[0].maxWeightLimit = (TrObj.EmptyTransporter + TrObj.TransporterWeight) + TrObj.TranUpperTol;

		pickList.Update(doc, function(Resp){
			return callback(Resp.Err, Resp.SavedDoc);
		});
	} else {
		return callback('No Data', null);
	}
} /* SetEachParcelWeight1 */

function SetEachParcelWeight(docArr, index, TrObj, callback){
	if(index < docArr.length){
		docArr[index].orders[0].transporterWeight = (TrObj.EmptyTransporter + TrObj.TransporterWeight);
		docArr[index].orders[0].minWeightLimit = (TrObj.EmptyTransporter + TrObj.TransporterWeight) - TrObj.TranLowerTol;
		docArr[index].orders[0].maxWeightLimit = (TrObj.EmptyTransporter + TrObj.TransporterWeight) + TrObj.TranUpperTol;

		pickList.Update(docArr[index], function(Resp){
			index++;
			SetEachParcelWeight(docArr, index, TrObj, callback);
		});
	} else {
		return callback();
	}
} /* SetEachParcelWeight */

function CalcTotalTransporterWeight(docArr, transporter, callback){
	param.GetTransporterTolerance(transporter, function(TrTolObj){
		var x = 0;
		var PLweights = [];
		var TrWeight = 0;
		while(x < docArr.length){
			var itemNdx = 0;
			var TotalWeight = docArr[x].orders[0].picklistWeight;

			TrWeight += TotalWeight;
			PLweights[x] = TotalWeight;
			x++;
		}

		var TrObj = {
			EachParcelWeight: PLweights,
			TransporterWeight: TrWeight,
			TranLowerTol: TrTolObj.MinTolerance,
			TranUpperTol: TrTolObj.MaxTolerance,
			EmptyTransporter: TrTolObj.EmptyTransporter
		}

		return callback(TrObj);
	});
} /* CalcTotalTransporterWeight */

function FindParcelToRetrieve(Stn, callback){
	PwLocs.FindCompleteLoc({'pullWallStation': Stn.stationID, 'Complete': true}, function(Resp){
		var Loc = Resp.pwLocsArr;
		if(!Resp.Err && Loc){
			var x = 0;
			while(x < Loc.length){
				if(Loc[x].pullLightError == true){
				} else if(Stn.courier == null || (Stn.courier == Loc[x].courier)){
				  break;
				}
				x++;
			}

			if(x < Loc.length){
                		var num = Loc[x].pullAddress;
				var strLoc = num.toString();
				if(strLoc.length <= 2){
				  	strLoc = "00" + strLoc;
				} else {
				  	if(strLoc.length <= 3){
						strLoc = "0" + strLoc;
				  	} else {
						strLoc = Loc[x].pullAddress;
				  	}
				}

				PwLocs.LightupLoc(strLoc, '_ON_GREEN', Stn.stationID, function(Result){
					if(Result.error){
						UpdateLightError(Loc[x].pullAddress, function(){
							FindParcelToRetrieve(Stn, callback);
						});
					} else {
						Stn.orderID = Loc[x].orderID;
						Stn.courier = Loc[x].courier;
						Stn.Location = Loc[x].pullAddress;
						Stn.parcelConfirmed = false;
						Stn.trConfirmed = false;

						PwStn.Update(Stn, function(Resp){
							var savedStn = Resp.SavedDoc;
							return callback({result: 'Loc Found', Station: savedStn});
						});
					}
				});
			} else {
				return callback({result: 'No more available locations to process', Station: Stn});
			}
		} else {
			return callback({result: 'No more available locations to process', Station: Stn});
		}
	});
} /* FindParcelToRetrieve */

function UpdateLightError(Lane, callback){
	PwLocs.FindOne({'pullAddress': Lane}, function(Resp){
		var PWLOC = Resp.pwLoc;
		if(!Resp.Err && PWLOC){
			PWLOC.pullLightError = true;
			PwLocs.Update(PWLOC, function(Resp){
				return callback();
			});
		} else {
			return callback();
		}
	});
} /* UpdateLightError */

function AddTransporterToStation(Stn, transporter, callback){
	pickList.Find({'transporterID': transporter}, function(Resp){
        var docArr = Resp.pickListArr;
		var Unique = true;
		if(!Resp.Err && docArr){
			var x = 0;
			while(x < docArr.length){
				if(docArr[x].Status != 'SHIPPED'){
					Unique = false;
					break;
				}
				x++;
			}
		}

		if(Unique){
			if(docArr){
				if(docArr.length > 0){
					var qClearShipped1  = async.queue(ClearShippedOrdersFromTransporter1, docArr.length);
					qClearShipped1.push(docArr, function (err, data) {
					});

					qClearShipped1.drain = function(){
					}
					//ClearShippedOrdersFromTransporter(docArr, 0, function(){
					//});
				}
			}

			Stn.transporter = transporter;

			if(!Stn.orderID){
				Stn.orderID = null;
				Stn.courier = null;
				Stn.parcelConfirmed = false;
				Stn.trConfirmed = false;
				Stn.Location = null;
			}

			PwStn.Update(Stn, function(Resp){
				var savedDoc = Resp.SavedDoc;
				if(!Resp.Err && savedDoc){
					return callback({error: null, Stn: savedDoc});
				} else {
					return callback({error: 'Failed to add transporter to station'});
				}
			});
		} else {
			return callback({error: 'Scanned Transporter is not empty'});
		}
	});
} /* AddTransporterToStation */

function ClearShippedOrdersFromTransporter1(task, callback){
	let doc = task;

	if(doc){
		doc.transporterID = null;
		pickList.Update(doc, function(Resp){
			return callback();
		});
	} else {
		return callback();
	}
} /* ClearShippedOrdersFromTransporter1 */

function ClearShippedOrdersFromTransporter(docArr, index, callback){
	if(index < docArr.length){
		docArr[index].transporterID = null;
		pickList.Update(docArr[index], function(Resp){
			index++;
			ClearShippedOrdersFromTransporter(docArr, index, callback);
		});
	} else {
		return callback();
	}
} /* ClearShippedOrdersFromTransporter */

function ShowStationError(list, req, res){
	/* show scan station textbox */
	list.StnID = "";
	list.EndStnID = "";
	list.Instruction = '<label style="color: red">Scanned station is not found</label>' + '<br><br>';
	list.Input = "<!--";
	list.EndInput = "-->";
	SetAndRenderResponse(list, req, res);
} /* ShowStationError */

function ShowCartonError(list, req, res){
	/* show scan station textbox */
	list.Parcel = "";
	list.EndParcel = "";
	list.Instruction = '<label style="color: red">Scanned parcel Id is not for order at station</label>' + '<br><br>';
	list.Input = "";
	list.EndInput = "";
	list.RelBtn = "";
	list.EndRelBtn = "";
	SetAndRenderResponse(list, req, res);
} /* ShowCartonError */

function ShowTransporterConfirm(list, Stn, req, res){
	list.Confirm = "";
	list.ConTR = Stn.transporter;
	list.EndConfirm = "";
	list.endPoint= "trconfirm";
	list.Input = "";
	list.EndInput = "";
	SetAndRenderResponse(list, req, res);
} /* ShowTransporterConfirm */

function SetAndRenderResponse(list, req, res){
	fs.readFile("./public/pullwall.html", function(err,data){
	  if(err) throw err

	  templatesjs.set(data, function(err,data){
			if(err) throw err;

			templatesjs.renderAll(list, function(err,data){
				if(err) throw err;
				res.write(data);
				res.end();
			});
		});
	});
} /* SetAndRenderResponse */

function WEB_CallPageRedirect(res, Url){
	res.statusCode = 302;
	res.setHeader('Location',Url);
	res.end();
} /* WEB_CallPageRedirect */

function CreateRecordForEachLine(mPL, lineNdx, callback){
	if(lineNdx >= mPL.orders[0].items.length){
		return callback();
	}

	let Order = mPL.orders[0];
	let Line = Order.items[lineNdx];

	PackUpdate.findOne({'PickListID': Order.pickListID, 'PickListLine': Line.pickListOrderLine, 'OrderId': Order.orderID}, function(err, uPL){
		if(err){
			lineNdx++;
			return CreateRecordForEachLine(mPL, lineNdx, callback);
		}

		if(uPL){
			lineNdx++;
			return CreateRecordForEachLine(mPL, lineNdx, callback);
		}

		let NewRec = new PackUpdate({
			PickListID: Order.pickListID,
			PickListLine: Line.pickListOrderLine,
			WcsId: Order.cartonID,
			OrderId: Order.orderID,
			Required: true,
			CreateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
		});

		NewRec.save(function(err, savedDoc){
			console.log('Marked picklist: ' + Order.pickListID + ' PickListLine: ' + Line.pickListOrderLine + ' | Order: ' + Order.orderID + ' | Carton: ' + Order.cartonID + ' PackUpdate to true');
			lineNdx++;
			return CreateRecordForEachLine(mPL, lineNdx, callback);
		});
	});
} /* CreateRecordForEachLine */

function CreatePackUpdate(mPL){
	CreateRecordForEachLine(mPL, 0, function(){
		return;
	});
} /* CreatePackUpdate */
