/*
 * @Author: Clive Matlawa
 * @Date: 2017-09-12
 */

/*require('appdynamics').profile({
    controllerHostName: '10.20.88.95',
    controllerPort: 8090,
    accountName: 'customer1',
    accountAccessKey: '91f9b93a-0a53-4caa-8bde-f4f8328773dc',
    applicationName: 'Midrand Prod WCS',
    tierName: 'NodeJS',
    nodeName: 'Desktop Interface',
    debug: true,
logging:{
   'logfiles': [{
   'root_directory':'/tmp/appd',
   'filename':'echo_%N.log',
   'level': 'DEBUG',
   'max_size': 10000,
   'max_files': 1
}]
}
});*/

var express = require('express')
var path = require('path')
var mongoose = require('mongoose')
var bodyParser = require('body-parser')
var session = require('client-sessions')
var dbUrl = require('ui').mongo.url
var port = require('ui').port

// var dateConf = require('./config/dateStore')

var index = require('./routes/index.js') // The main web page route.

var systemParameterRouter = require('./routes/systemParameter/systemParameterRouter.js')
var systemParameterGroupRouter = require('./routes/systemParameter/systemParameterGroupRouter.js') // The system parameter router.

var deviceRouter = require('./routes/device/deviceRouter.js') // The device router.
var deviceGroupRouter = require('./routes/device/deviceGroupRouter.js') // The device group router.

var userRouter = require('./routes/user/userRouter.js') // The user router.
var userRoleRouter = require('./routes/user/userRoleRouter.js') // The user role router.

var pickListRouter = require('./routes/pickList/pickListRouter.js') // The pick list router.
var pickListPwRouter = require('./routes/pickList/pickListPwRouter.js') // The pick list router.
var packListRouter = require('./routes/packList/packListRouter.js') // The pack list router.
var cartonRouter = require('./routes/pickList/cartonRouter.js') // The carton router.
var transporterRouter = require('./routes/pickList/transporterRouter.js') // The transporter router.
var printingRouter = require('./routes/printing/printingRouter.js') // The printing router.
var toteInfo = require('./routes/toteInfo/toteInfoRouter.js')
var pickList = require('./routes/pickListInfo/pickListInfoRouter.js')
var orderList = require('./routes/orderInfo/orderInfoRouter.js')
var reject = require('./routes/pickList/rejectRouter')
var packStation = require('./routes/Station/stationInfoRouter.js')
var palletHandover = require('./routes/palletHandover/palletHandoverRoute.js')
var toteTracking = require('./routes/toteTracking/toteTrackingRoute')
var socketRoute = require('./routes/socketIOTCP/socketIORoute')
var alarmRoute = require('./routes/alarms/alarmRoute')
var nonConvayable = require('./routes/nonConvayable/nonConvRouter.js')
var unpickaTote = require('./routes/unpickaTote_2/unpickToteRouter.js') // The user role router.
var unconsolidateOrders = require('./routes/unconsolidatedOrders/unconsolidatedRouter.js') // The user role router.
// var CronJob = require('cron').CronJob;

var apexLib = require('apex_lib/moduleIndex.js') // The apex lib package.

// dateConf.theDate = new Date().toISOString().substr(0, 10)

apexLib.mongo.client(dbUrl, function (error, client, db) {
    if (error) {
        console.log('Error occured on Mongo db connection. ' + error)
    } else {
        console.log('Connection to Mongo DB established')
        global.mongo = {}
        global.mongo.client = client
        global.mongo.db = db
    }
})

mongoose.connect(dbUrl, {useMongoClient: true})

var app = express()

app.use(session({
  cookieName: 'session',
  secret: 'eg[isfd-8yF9-7w2315df{}+Ijsli;;to8',
  duration: 30 * 60 * 1000,
  activeDuration: 5 * 60 * 1000,
  httpOnly: true,
  secure: true,
  ephemeral: true
}));



app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.text())
app.use(express.static(path.join(__dirname, 'public'))) // Serve the public folder.
app.use(express.static(path.join(__dirname, 'views'))) // Serve the views folder.

// app.use(systemParameterRouter) // Set the system parameter router.//
app.use(systemParameterGroupRouter) // Set the system parameter group router.
app.use(deviceRouter) // Set the device router.
app.use(deviceGroupRouter) // Set the device group router.
app.use(systemParameterRouter) // Set the system parameter router.//

app.use(userRouter) // Set the user router.
app.use(userRoleRouter) // Set the user role router.

app.use(pickListRouter) // Set the pick list router.
app.use(pickListPwRouter) // Set the pick list put wall router.
app.use(packListRouter) // Set the pack list router.
app.use(cartonRouter) // Set the carton router.
app.use(transporterRouter) // Set the transporter router.
app.use(printingRouter) // Set the printing router.
app.use(toteInfo)
app.use(pickList)
app.use(orderList)
app.use(reject)
app.use(packStation)
app.use(palletHandover)
app.use(toteTracking)
app.use(socketRoute)
app.use(alarmRoute)
app.use(nonConvayable)
app.use(unpickaTote);
app.use(unconsolidateOrders);

app.use(index) // Set the page router.

// Create the server.
var server = app.listen(port, function ()
{
  console.log('Server listening on port ' + server.address().port)
})

module.exports = app
