// var SubSysConnect = require('subSystemCommsModel')

var AlarmModule = function(){}

AlarmModule.prototype.addSubSystemCommsError = function(){
    var SubSysConnect = new SubSysConnect({
        errorMessage : 'Serial Scan: Error while trying to connect to WMS for serial validation',
        Date         : moment().format('DD-MM-YYYY, h:mm a'),
        processed    : false
    })

    SubSysConnect.save()
}

module.exports = new AlarmModule()
