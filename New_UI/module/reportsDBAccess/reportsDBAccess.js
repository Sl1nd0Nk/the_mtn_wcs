var QCRejectedData = require('rejectedQC')
var Picklist        = require('pickListModel')

var ReportsDBAccess = function(){}

ReportsDBAccess.prototype.qcRejectView = function(callback){
    QCRejectedData.find(function(error, data){
        if(!error && data){
            callback({code:'00', message:'success', data:data})
        }else{
            callback({code:'01', message:'success', data:error})
        }
    })
}

ReportsDBAccess.prototype.qcRejectGetItems = function(transporterID, callback){
    Picklist.find({'orders.transporterID':transporterID}, function(error, data){
        if(!error && data){
            // console.log('DATA: ' + data)
            callback({code:'00', message:'success', data:data})
        }else{
            callback({code:'01', message:'success', data:error})
        }
    })
}

module.exports = new ReportsDBAccess()