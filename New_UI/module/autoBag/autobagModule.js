var Params 			= require('parameterModel')
var PackStation 	= require('packStationModel')
var PickListModel 	= require('pickListModel')
var redis 			= require('redis');
var Pubclient 		= redis.createClient();
var moment 			= require('moment')

var AutoBagMod = function() {}

AutoBagMod.prototype.printBagLabel = function (toteID, callback) {
	PicklistInfo(toteID, function(response) {
		var pickListInfo 

		pickListInfo = response.data
		cartonID = response.cartonID

		if(response.code == '00') {
			getPackStationData(pickListInfo, function(packResp) {

				if(packResp.data) {
					var packStation = packResp.data

					PrintToAutobagger(packStation.autobagPrinterQName, packStation.stationNumber, response.data, response.orderDetails, packStation, cartonID, function(response) {
						console.log('_____________________________________________________________ | ' + response)
						callback({code: '00', message: 'success', docs: response})
					})
				} else {
					callback({code: '01', message: 'There is no packstation information.'})
				}
			})
		} else {
			callback({code: '01', message: 'No pick llist information found for this tote. '})
		}
	})
}

function getPackStationData(pickListInfo, callback) {
	PackStation.findOne({stationNumber: pickListInfo.toteInUseBy}, function(err, docs) {
		if(!err && docs) {
			callback({code: '00', message: 'success', data: docs})
		} else {
			callback({code: '01', message: 'could not find this packstation.'})
		}
	})
}

function generateCarton(callback) {
	var Msg = null;
	Params.find({}, function(err2, Parameters)
	{
		if(err2 || !Parameters || Parameters.length <= 0)
		{
			Msg = "Failed to find parameters in the system ";
			WriteToFile(Msg);
			return callback({error:Msg});
		}
		else
		{
			if(Parameters[0].autoBagsSeqNo){
				var GenCartonID = 10000000 + Parameters[0].autoBagsSeqNo;
				Parameters[0].autoBagsSeqNo += 1;

				Parameters[0].save(function(err3, savedParam){
					if(!err3 && savedParam){
						return callback({CartonID: GenCartonID});
					} else {
						Msg = "Failed to update new parameter sequence number";
						WriteToFile(Msg);
						return callback({error:Msg});
					}
				});
			} else {
				Msg = "Failed to find autobagger carton sequence number parameter in the system ";
				WriteToFile(Msg);
				return callback({error:Msg});
			}
		}
	});
} /* WriteToFile */

function PrintToAutobagger(printer, packStation, savedDoc, orderDetails, packStationInfo, cartonID, callback) {
console.log('*** ' + orderDetails.Cartons)
	var PrintData = {
		orderId: savedDoc.orders[0].orderID,
		printerName: packStationInfo.labelPrinterQName,
		labelprinterName: printer,
		items: orderDetails,
		ErpItems: savedDoc.orders[0].items,
		cartonId: cartonID,
		courierId: savedDoc.orders[0].courierID,
		station: packStation,
		CartonNo: 1,
		NoOfCartons: savedDoc.orders[0].numberOfTotes,
		CartionWeight: savedDoc.orders[0].picklistWeight,
		FirstCarton: true,
		LastCarton: true,
		orderType: savedDoc.orders[0].orderType,
		Cartons: orderDetails.Cartons
	}

	Pubclient.publish('PACKING_' + packStation, JSON.stringify(PrintData), function(err){
		if(err){
			return callback({error:"Error printing label for order " + savedDoc.orders[0].orderID, PickList:savedDoc});
		} else {
			savedDoc.orders[0].printed = true
			savedDoc.save(function(err, PickList){
				return callback({PickList:PickList});
			});
		}
	});
}

function GetAllItemsAndCartonsForOrder(savedDoc, CurrentOrderToProcess, callback)
{
	var Msg = null;
	PickListModel.find({'orders.orderID': savedDoc.orders[CurrentOrderToProcess].orderID}, function(err, PickLists)
	{
		if(err || !PickLists || PickLists.length <= 0)
		{
			Msg = "Error finding all picklists for order " + Data.orders[CurrentOrderToProcess].orderID;
			WriteToFile(Msg);
			return callback({error:"Problem encountered fetching all picklists for order - please call supervisor"});
		}
		else
		{
			var Cartons = [];
			var OrdItems = [];
			var PL = 0;
			while(PL < PickLists.length)
			{
				var OrdIndex = 0;

				while(OrdIndex < PickLists[PL].orders.length)
				{
					if(PickLists[PL].orders[OrdIndex].orderID == savedDoc.orders[CurrentOrderToProcess].orderID)
					{
						var itemIndx = 0;
						while(itemIndx < PickLists[PL].orders[OrdIndex].items.length)
						{
							OrdItems.push(PickLists[PL].orders[OrdIndex].items[itemIndx]);
							Cartons.push(PickLists[PL].orders[OrdIndex].cartonID);
							itemIndx++;
						}
					}
					OrdIndex++;
				}
				PL++;
			}

			var ReturnData =
			{
				Cartons: Cartons,
				OrdItems: OrdItems
			}

			return callback(ReturnData);
		}
	});
} 

function PicklistInfo(toteID, callback) {
	if(toteID) {
		PickListModel.findOne({toteID: toteID}, function(err, docs) {
			if(!err && docs) {
				generateCarton(function(cartonInfo) {
					if(cartonInfo.CartonID) {
						console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ | ' + cartonInfo.CartonID + ' | ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
						// docs.orders[0].cartonID = cartonInfo.CartonID
						docs.orders[0].toteNumber = 1

						docs.save(function(saveError, saveDocs) {
							if(!saveError && saveDocs) {
								GetAllItemsAndCartonsForOrder(saveDocs, 0, function (OrdDetails) {
									callback({code:'00', message: 'success', data: docs, orderDetails: OrdDetails, cartonID: cartonInfo.CartonID})
								})
								// console.log('RETURN: ' + saveDocs.orders[0].toteNumber)
								// callback({code:'00', message: 'success', data: saveDocs})
							} else {
								callback({code: '01', message:'Tote carton information was not saved.'})
							}
						})
					} else {
						callback({code: '01', message:'Carton information was not generated properly.'})
					}
				})
			} else {
				callback({code: '01', message:'Tote information was not found.'})
			}
		})
	} else {
		callback({code: '01', messages: 'There is no tote ID.'})
	}
}

function WriteToFile(Msg){
	if(Msg){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' ' + Msg);
	}
} /* WriteToFile */

module.exports = new AutoBagMod()
