var SubSystemModule = require('subSystemCommsModel')
var RejectedQC      = require('rejectedQC')
var Users           = require('userModel')
var CourierWeightFail=require('alarmsAndEvents/weightFailLog')
var PickerNotLogged = require('alarmsAndEvents/ptlTimerLog')

var timeoutParameter = 3
var releaseTimeout   = 3

var AlarmDBAccesModule = function(){}

// ==================================== SUB-SYSTEM COMMS
AlarmDBAccesModule.prototype.getSubSystemFails = function(callback){
    SubSystemModule.find({processed:false}, function(error, data){
        if(!error && data){
            callback({code:'00', message:'success', data:data})
        }else{
            callback({code:'01', message:'fail', data:error})
        }
    })
}

AlarmDBAccesModule.prototype.updateSubsystem = function(callback){
    SubSystemModule.find(function(error, data){
        if(!error && data){
            for(var x = 0; x < data.length; x++){
                data[x].processed = true

                data[x].save()
            }

            callback({code:'00', message:'success', data:data})            
        }else{
            callback({code:'01', message:'fail', data:error})
        }
    })
}


// ==================================== REJECTED QC
AlarmDBAccesModule.prototype.getRejectedQC = function(callback){
    RejectedQC.find({processed:false}, function(error, data){
        if(!error && data){
            callback({code:'00', message:'success', data:data})
        }else{
            callback({code:'01', message:'fail', data:error})
        }
    })
}

AlarmDBAccesModule.prototype.updateQCReject = function(callback){
    RejectedQC.find(function(error, docs){
        if(!error && docs){
            for(var x = 0; x < docs.length; x++){
                docs[x].processed = true

                docs[x].save()
            }

            callback({code:'00', message:'success', data:docs})  
        }else{
            callback({code:'01', message:'fail', data:error})  
        }
    })
}

// ==================================== FAILED LOGIN
AlarmDBAccesModule.prototype.getFailedLogin = function(callback){
    Users.find(function(error, data){
        if(!error && data){
            var failedUsers = []

            for(var x = 0; x < data.length; x++){
                if(data[x].failedLogin > 0){
                    failedUsers.push(data[x])
                }
            }
            callback({code:'00', message:'success', data:failedUsers})
        }else{
            callback({code:'01', message:'fail', data:error})
        }
    })
}

// ==================================== COURIER WEIGHT FAIL
AlarmDBAccesModule.prototype.courierWeightFail = function(callback){
    CourierWeightFail.find({processed:false},function(error, data){
        if(!error && data){
            callback({code:'00', message:'success', data:data})
        }else{
            callback({code:'01', message:'fail', data:error})
        }
    })
}

AlarmDBAccesModule.prototype.updateCourierWeight = function(callback){
    CourierWeightFail.find(function(error, docs){
        if(!error && docs){
            // console.log('*********************************************************************')
            for(var x = 0; x < docs.length; x++){
                docs[x].processed = true

                docs[x].save()
            }

            callback({code:'00', message:'success', data:docs})  
        }else{
            callback({code:'01', message:'fail', data:error})  
        }
    })
}

// ==================================== PICKER NOT LOGGED IN
AlarmDBAccesModule.prototype.pickerNotLogged = function(callback){
    PickerNotLogged.find(function(error, data){
        if(!error && data){

            var dataResp = []
            
            for(var x = 0; x < data.length; x++){
                var timout = data[x].TimeIn.getTime() + (timeoutParameter * 60 * 1000)

                if(timout < Date.now()){
                    dataResp.push(data[x])
                }
            }
            
            callback({code:'00', message:'success', data:dataResp})
        }else{
            callback({code:'01', message:'fail', data:error})
        }
    })
}

// ==================================== PICKER NOT RELEASING
AlarmDBAccesModule.prototype.pickerNotReleasing = function(callback){
    PickerNotLogged.find(function(error, data){
        if(!error && data){

            var dataResp = []
            
            for(var x = 0; x < data.length; x++){
                var timout = data[x].TimeIn.getTime() + (releaseTimeout * 60 * 1000)

                if(timout < Date.now()){
                    dataResp.push(data[x])
                }
            }
            
            callback({code:'00', message:'success', data:dataResp})
        }else{
            callback({code:'01', message:'fail', data:error})
        }
    })
}

// ==================================== PACKER  NOT PACKING
AlarmDBAccesModule.prototype.packerNotPacking = function(callback){
    PickerNotLogged.find(function(error, data){
        if(!error && data){

            var dataResp = []
            
            for(var x = 0; x < data.length; x++){
                var timout = data[x].TimeIn.getTime() + (releaseTimeout * 60 * 1000)

                if(timout < Date.now()){
                    dataResp.push(data[x])
                }
            }
            
            callback({code:'00', message:'success', data:dataResp})
        }else{
            callback({code:'01', message:'fail', data:error})
        }
    })
}

module.exports = new AlarmDBAccesModule()
