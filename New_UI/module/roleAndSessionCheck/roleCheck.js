var UserModel   = require('userModel')

var CheckRole = function() {}

CheckRole.prototype.checkUserRole = function(username, callback) {
    UserModel.findOne({username: username}, function(error, docs) {
		if(!error && docs) {
			callback({code:'00', data: docs})
		} else {
			callback({code:'01'})
		}
	})
}

module.exports = new CheckRole()
