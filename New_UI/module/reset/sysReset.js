var CourierReject  = require('courierRejectModel')
var PickToZone     = require('pickToZoneModel')
var PtlReject      = require('ptlRejectModel')
var ChuteModel     = require('chuteModel')
var CourierDivert  = require('courierDivertModel')
var WeightFailLog  = require('weightFailLog')
var ScaleThroughput= require('scaleThroughModel')
var PickThrouput    = require('pickThroughputModel')
var PutWallDash     = require('putWallDashModel')
var mongoose        = require('mongoose')

var Reset = function() {}

Reset.prototype.systemReset = function() {

    mongoose.connect('mongodb://localhost:27017/MTNWCS', {useMongoClient:true})


    PickToZone.find(function(error, docs) {
        if(!error && docs) {
            for(var x = 0; x < docs.length; x++) {
                docs[x].Totes = []

                docs[x].save(function(saveErr, saveDocs) {
                    if(!saveErr && saveDocs) {
                        console.log('pick to zone data saved')
                    } 
                })
            }

            pickThrough()
        } else {
		    console.log('WE DO NOT FIN ANY PICK TO ZONES.')
	    }
    })

    function pickThrough() {
        PickThrouput.find(function(err, docs) {
            if(!err && docs) {
                for(var x = 0; x < docs.length; x++) {
                    docs[x].Totes = []

                    docs[x].save(function (saveErr, saveData) {
                        if(!saveErr && saveData) {
                            console.log('pick-throughput')
                        }
                    })
                }

                putWall()
            }
        })
    }

    function putWall() {
        PutWallDash.find(function(err, docs) {
            if(!err && docs) {
                for(var x = 0; x < docs.length; x++) {
                    docs[x].Totes = []

                    docs[x].save(function (saveErr, saveData) {
                        if(!saveErr && saveData) {
                            console.log('Put wall dashboard.')
                        }
                    })
                }

                scaleThrough()
            }
        })
    }

    function scaleThrough () {
        ScaleThroughput.find(function(err, docs) {
            if(!err && docs) {
                for(var x = 0; x < docs.length; x++) {
                    docs[x].scaleObjects = []

                    docs[x].save(function (saveErr, saveData) {
                        if(!saveErr && saveData) {
                            console.log('Scale Throughput')
                        }
                    })
                }

                ptlReject()
            }
        })
    }

    function ptlReject() {
        PtlReject.find(function(err, docs) {
            if(!err && docs) {
		console.log('+++++++++++++++++++++++++++++++++++++++++++++: ' + docs.length)

                // docs = []

                docs[0].remove(function(saveErr, saveDocs) {
                    if(!saveErr && saveDocs) {
                        console.log('PTL Reject saved.')
                        courierDivert()
                    }
                })
            }
        })
    }

    function courierDivert() {
        CourierDivert.find(function(err, docs) {
            if(!err && docs) {
                for(var x = 0; x < docs.length; x++) {
                    docs[x].containers = []

                    docs[x].save(function(saveErr, saveDocs) {
                        if(!saveErr && saveDocs) {
                            console.log('Courier Divert saved.')
                        }
                    })
                }

                courierReject()
            }
        })
    }

    function courierReject() {
        CourierReject.find(function(err, docs) {
            if(!err && docs) {
                for(var x = 0; x < docs.length; x++) {
                    docs[x].containers = []

                    docs[x].save(function(saveErr, saveDocs) {
                        if(!saveErr && saveDocs) {
                            console.log('Courier reject was not found.')
                        }
                    })
                }

                chuteModel()
            }
        })
    }

    function chuteModel() {
        ChuteModel.find(function(err, docs) {
            if(!err && docs) {
                for(var x = 0; x < docs.length; x++) {
                    docs[x].Totes = []

                    docs[x].save(function(saveErr, saveDocs) {
                        if(!saveErr && saveDocs) {
                            console.log('Chute model saved.')
                        }
                    })
                }
            }
        })
    }
}

module.exports = new Reset();
