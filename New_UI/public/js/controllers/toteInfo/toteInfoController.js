/*
 * @Author: Ntokozo Majozi
 * @Date: 2017-04-06 10:26:29
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-08-22 11:33:42
 */

/**
* @controller toteInfoController
* @function getToteInfo - gets Tote Information.
*/

app.controller('toteInfoController', function ($scope, $location, toteInfoService) {
    $scope.itemsToDisplay = []
    $scope.ordersToDisplay = []
    $scope.submit = function () {
        toteInfoService.getToteInfo($scope.toteId)
        .then(function (doc) {
            if (doc) {
                if (doc.data.code === '00') {
                    $scope.itemsToDisplay = doc.data.data.items
                    $scope.ordersToDisplay = doc.data.data.orders
                    $scope.errorDisplay = ''
                    $scope.toteId = ''
                } else {
                    $scope.errorDisplay = 'Tote: ' + $scope.toteId + ' is not linked to a Pick List'
                    $scope.itemsToDisplay = []
                    $scope.ordersToDisplay = []
                    $scope.toteId = ''
                }
            } else {
                $scope.errorDisplay = 'Invalid tote ID. Please ensure that tote was scanned properly.'
                $scope.itemsToDisplay = []
                $scope.ordersToDisplay = []
                $scope.toteId = ''
            }
        })
    }

    $scope.clear = function () {
        $scope.itemsToDisplay = []
        $scope.ordersToDisplay = []
        $scope.toteId = ''
    }
})
