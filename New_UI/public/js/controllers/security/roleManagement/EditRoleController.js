/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-15 08:04:55
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-07-03 17:21:32
 */

/**
* @controller EditUserRoleController
* @function  getUserRole - gets a specific user role based on the device id.
* @function  toggleEdit - toggle between the view of a device, and edit mode of the device.
* @function  back - cancels edit mode.
* @function  saveUserRole - if changes made in edit mode, the user role is saved. DB updated, based on the id of the device.
* @function  deleteUserRole - deletes a user role from the db, where id matches.
*/

app.controller('EditUserRoleController', function ($scope, $routeParams, UserRoles) {
    UserRoles.getUserRole($routeParams.roleId)
    .then(function (doc) {
        $scope.userRoles = doc.data
        $scope.userRole = $scope.userRoles[0]
    }, function (response) {
        window.alert(response)
    })

    $scope.toggleEdit = function () {
        $scope.editMode = true
        $scope.userRoleFormUrl = '/security/roleManagement/roleForm.html'
    }

    $scope.back = function () {
        $scope.editMode = false
        $scope.userRoleFormUrl = ''
    }

    $scope.saveUserRole = function (userRole) {
        UserRoles.editUserRole(userRole)
        $scope.editMode = false
        $scope.userRoleFormUrl = ''
    }

    $scope.deleteUserRole = function (userRole) {
        UserRoles.deleteUserRole(userRole)
    }
})
