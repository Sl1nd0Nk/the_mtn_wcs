/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-15 08:05:09
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-03-23 10:16:00
 */

/**
* @controller UserRoleController
* @scope  - this will return a list of all the user roles from the db.
*/

app.controller('UserRoleController', function (userRoles, $scope) {
    $scope.userRoles = userRoles.data
})
