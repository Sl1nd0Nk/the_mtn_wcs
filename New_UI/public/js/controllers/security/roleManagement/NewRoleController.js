/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-15 08:05:02
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-04-25 07:47:50
 */

/**
* @controller NewUserRoleController
* @function  back - return to previous screen.
* @function  saveUserRole - creates a new user role in the db. db will generate first available id.
*/

app.controller('NewUserRoleController', function ($scope, $location, UserRoles) {
    $scope.back = function () {
        $location.path('/roleMan/')
    }

    $scope.saveUserRole = function (userRole) {
        UserRoles.createUserRole(userRole)
        .then(function (doc) {
            if (doc.data === 'Success') {
                $location.path('/roleMan/')
            }
        }, function (response) {
            window.alert(response)
        })
    }
})
