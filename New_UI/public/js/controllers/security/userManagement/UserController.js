app.controller('UserCntrl', function ($scope, $http, $mdDialog, $rootScope, $location) {
    $http.get('/getLoggedInUser').then(function(response) {
        if(response.data.code == '00') {
            $http.get('/API/getShowAllUsers/').then(function(response){
                if(response.data.code == '00') {
                   $scope.users = response.data.data
        
                   $rootScope.refreshData = function() {
                        $http.get('/API/getShowAllUsers/').then(function(response){
                            $scope.users = response.data.data
                        })
                   }
        
                   $scope.addUser = function() {
                        $mdDialog.show({
                            controller  : addUserController,
                            templateUrl : '/security/userManagement/dialogueTemplates/addUserDialog.html',
                            parent: angular.element(document.body),
                            clickOutsideToClose:true,
                            fullscreen  : $scope.customFullscreen 
                        })
                        .then(function(answer) {}, function() {});
                   }
        
                   $scope.removeUser = function(userName) {
                    var confirm = $mdDialog.confirm()
                    .title('Are you sure that you want to remove ' + userName)
                    .ok('Ok')
                    .cancel('Cancel')
        
                    $mdDialog.show(confirm).then(function() {
                        $http.post('/removeUser/'+userName).then(function(response) {
                            if(response.data.code == '00') {
                                var confirm = $mdDialog.alert()
                                .title('User ' + userName + ' has successfully been removed.' )
                                .ok('Ok')
        
                                $mdDialog.show(confirm).then(function() {
                                    $rootScope.refreshData()
                                }, function() {});
                            } else {
                                var confirm = $mdDialog.alert()
                                .title(response.data.message)
                                .ok('Ok')
        
                                $mdDialog.show(confirm).then(function() {}, function() {});
                            }
                        })
                    });
                }
        
                $scope.editCourier = function(userData) {
                    $rootScope.editUser = userData
        
                    $mdDialog.show({
                        controller  : editUserController,
                        templateUrl : '/security/userManagement/dialogueTemplates/editUserDialog.html',
                        parent: angular.element(document.body),
                        clickOutsideToClose:true,
                        fullscreen  : $scope.customFullscreen 
                    })
                    .then(function(answer) {}, function() {});
                }
                
                function editUserController($scope, $rootScope, $mdDialog) {
                    $scope.userData = {
                        name        : $rootScope.editUser.name,
                        surname     : $rootScope.editUser.surname,
                        username    : $rootScope.editUser.username,
                        password    : $rootScope.editUser.password,
                        selected    : $rootScope.editUser.selected,
                    }
        
                    $scope.answer = function(answer, addData) {
        
                        $scope.sendData = {
                            name        : addData.name,
                            surname     : addData.surname,
                            username    : addData.username,
                            password    : addData.password,
                            selected    : addData.selected,
                            oldUsername : $rootScope.editUser.username
                        }
        
                        $mdDialog.hide(answer)
                        if(answer == 'editUser') {
                            $http.post('/updateUser/'+JSON.stringify($scope.sendData)).then(function(response) {
                                if(response.data.code == '00') {
                                    var confirm = $mdDialog.alert()
                                    .title('User has been updated sussefully.')
                                    .ok('Ok')
        
                                    $mdDialog.show(confirm).then(function() {
                                        $rootScope.refreshData()
                                    }, function() {});
                                } else {
                                    var confirm = $mdDialog.alert()
                                    .title(response.data.message)
                                    .ok('Ok')
        
                                    $mdDialog.show(confirm).then(function() {
					// console.log(' THIS IS THE CORRECT PART OF THE OK BUTTON: ')
				    }, function() {});
                                }
                            })
                        } else {
                            
                        }
                    }
                }
        
                   function addUserController($scope, $rootScope, $mdDialog) {
                        $scope.answer = function(answer, addData) {
                            $mdDialog.hide(answer)
                            if(answer == 'addUser') {
                                $http.post('/addUser/'+JSON.stringify(addData)).then(function(response) {
                                    if(response.data.code == '00') {
                                        var confirm = $mdDialog.alert()
                                        .title('User has successfully been added.')
                                        .ok('Ok')
        
                                        $mdDialog.show(confirm).then(function() {
                                            $rootScope.refreshData()
                                        }, function() {});
                                    } else {
                                        var confirm = $mdDialog.alert()
                                        .title(response.data.message)
                                        .ok('Ok')
        
                                        $mdDialog.show(confirm).then(function() {}, function() {});
                                    }
                                })
                            } else {
                                // DO NOTHING!!
                            }
                        }
                    }
                } else {
                    var confirm = $mdDialog.alert()
                    .title(response.data.message)
                    .ok('Ok')
        
                    $mdDialog.show(confirm).then(function() {
			$location.path('/toteTracking')
		    }, function() {});
                }
            })
        } else {
            var confirm = $mdDialog.alert()
            .title('User session was not found. Please log into the system before attempting to access this page.')
            .ok('Ok')

            $mdDialog.show(confirm).then(function() {
		$location.path('/')
	    }, function() {});
}
    })
})

