/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-14 20:18:51
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-07-28 14:02:29
 */

/**
* @controller DeviceGroupController
* @scope  - this will return a list of all the device groups from the db.
*/

app.controller('DeviceGroupController', function (deviceGroups, $scope) {
    $scope.deviceGroups = { // building an obeject like this, in order for the pagination (grid) to work.
        data: [],
        sort: {
            predicate: 'companyName',
            direction: 'asc',
            urlSync: true
        }
    }

    $scope.deviceGroups.data = deviceGroups.data
})
