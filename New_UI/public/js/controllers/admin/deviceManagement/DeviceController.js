/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-14 20:18:51
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-07-28 14:02:19
 */

/**
* @controller DeviceController
* @scope  - this will return a list of all the devices from the db.
*/

app.controller('DeviceController', function (devices, $scope) {
    $scope.devices = { // building an obeject like this, in order for the pagination (grid) to work.
        data: [],
        sort: {
            predicate: 'companyName',
            direction: 'asc',
            urlSync: true
        }
    }

    $scope.devices.data = devices.data
})
