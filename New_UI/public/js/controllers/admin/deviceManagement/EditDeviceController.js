/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-14 20:19:41
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-07-28 14:02:41
 */

/**
* @controller EditDeviceController
* @function getDevice - gets a specific device based on the device id.
* @function toggleEdit - toggle between the view of a device, and edit mode of the device.
* @function back - cancels edit mode.
* @function saveDevice - if changes made in edit mode, the device is saved. DB updated, based on the id of the device.
* @function deleteDevice - deletes a device from the db, where id matches.
* @function selectedValueItemChanged - sets the selected drop down item
* @function selectedRoleValueItemChanged - sets the selected dropdown item
* @function getListOfDeviceGroups - this will return all the device groups from the db.
* @function getListOfUserRoles - this will return all the roles from the db.
* @function setSelectedValue - sets the selected values of dropdowns.
*/

app.controller('EditDeviceController', function ($scope, $routeParams, Devices, DeviceGroups, UserRoles) {
    $scope.device = {}
    $scope.device.device_group_name = ''
    $scope.device.user_role_name = ''

    Devices.getDevice($routeParams.deviceId)
    .then(function (doc) {
        $scope.devices = doc.data
        $scope.device = $scope.devices[0]
    }, function (response) {
        window.alert(response)
    })

    $scope.toggleEdit = function () {
        $scope.editMode = true
        $scope.deviceFormUrl = '/admin/deviceManagement/deviceForm.html'

        $scope.device.device_group_name = $scope.device.device_group_id
        $scope.device.user_role_name = $scope.device.user_role_id
    }

    $scope.back = function () {
        $scope.editMode = false
        $scope.deviceFormUrl = ''
    }

    $scope.saveDevice = function (device) {
        Devices.editDevice(device)
        $scope.editMode = false
        $scope.deviceFormUrl = ''
    }

    $scope.deleteDevice = function (deviceId) {
        Devices.deleteDevice(deviceId)
    }

    $scope.selectedValueItemChanged = function (selectedValueItem) {
        $scope.selectedValueItemId = selectedValueItem.id
    }

    $scope.selectedRoleValueItemChanged = function (selectedRoleValueItem) {
        $scope.selectedRoleValueItemId = selectedRoleValueItem.id
    }

    $scope.setDropDownValues = function () {
        let objGroupSelect = document.getElementById('deviceGroup')
        setSelectedValue(objGroupSelect, $scope.device.device_group_name)

        let objRoleSelect = document.getElementById('deviceRole')
        setSelectedValue(objRoleSelect, $scope.device.user_role_name)
    }

    getListOfDeviceGroups(DeviceGroups, $scope)
    getListOfUserRoles(UserRoles, $scope)

    function getListOfDeviceGroups (DeviceGroups, $scope) {
        DeviceGroups.getDeviceGroups()
        .then(function (doc) {
            $scope.listOfDevices = doc.data
        }, function (response) {
            window.alert(response)
        })
    }

    function getListOfUserRoles (UserRoles, $scope) {
        UserRoles.getUserRoles()
        .then(function (doc) {
            $scope.listOfUserRoles = doc.data
        }, function (response) {
            window.alert(response)
        })
    }

    function setSelectedValue (selectObj, valueToSet) {
        for (var i = 0; i < selectObj.options.length; i++) {
            if (selectObj.options[i].text === valueToSet) {
                selectObj.options[i].selected = true
                return
            }
        }
    }
})
