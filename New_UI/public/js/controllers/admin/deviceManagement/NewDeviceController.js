/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-14 20:21:55
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-07-25 16:38:53
 */

/**
* @controller NewDeviceController
* @scope -listOfDevices -this is the list of all device types.
* @function  back - return to previous screen.
* @function  saveDevice - creates a new device in the db. db will generate first available id.
* @function selectedValueItemChanged - sets the selected drop down item
* @function selectedRoleValueItemChanged - sets the selected dropdown item
* @function getListOfDeviceGroups - this will return all the device groups from the db.
* @function getListOfUserRoles - this will return all the roles from the db.
*/

app.controller('NewDeviceController', function ($scope, $location, Devices, DeviceGroups, UserRoles) {
    $scope.selectedValueItemId = ''
    $scope.selectedRoleValueItemId = ''

    $scope.back = function () {
        $location.path('/deviceMan/')
    }

    $scope.saveDevice = function (device) {
        let itemType = $scope.selectedValueItemId
        device.groupID = itemType

        let itemRole = $scope.selectedRoleValueItemId
        device.roleID = itemRole

        let deviceGroupName = device.device_group_name.name
        device.device_group_name = deviceGroupName

        let deviceRoleName = device.user_role_name.name
        device.user_role_name = deviceRoleName

        Devices.createDevice(device)
        .then(function (doc) {
            if (doc.data === 'Success') {
                $location.path('/deviceMan/')
            }
        }, function (response) {
            window.alert(response)
        })
    }

    $scope.selectedValueItemChanged = function (selectedValueItem) {
        $scope.selectedValueItemId = selectedValueItem
    }

    $scope.selectedRoleValueItemChanged = function (selectedRoleValueItem) {
        $scope.selectedRoleValueItemId = selectedRoleValueItem
    }

    getListOfDeviceGroups(DeviceGroups, $scope)
    getListOfUserRoles(UserRoles, $scope)

    function getListOfDeviceGroups (DeviceGroups, $scope) {
        DeviceGroups.getDeviceGroups()
        .then(function (doc) {
            $scope.listOfDevices = doc.data
        }, function (response) {
            window.alert(response)
        })
    }

    function getListOfUserRoles (UserRoles, $scope) {
        UserRoles.getUserRoles()
        .then(function (doc) {
            $scope.listOfUserRoles = doc.data
        }, function (response) {
            window.alert(response)
        })
    }
})
