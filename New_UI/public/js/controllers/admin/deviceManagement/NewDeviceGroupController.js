/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-14 20:21:55
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-03-20 09:38:15
 */

/**
* @controller NewDeviceGroupController
* @function  back - return to previous screen.
* @function  saveDeviceGroup - creates a new device group in the db. db will generate first available id.
*/

app.controller('NewDeviceGroupController', function ($scope, $location, DeviceGroups) {
    $scope.back = function () {
        $location.path('/deviceManGroup/')
    }

    $scope.saveDeviceGroup = function (deviceGroup) {
        DeviceGroups.createDeviceGroup(deviceGroup)
        .then(function (doc) {
            if (doc.data === 'Success') {
                $location.path('/deviceManGroup/')
            }
        }, function (response) {
            window.alert(response)
        })
    }
})
