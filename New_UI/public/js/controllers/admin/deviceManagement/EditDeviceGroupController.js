/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-14 20:19:41
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-04-25 06:38:24
 */

/**
* @controller EditDeviceGroupController
* @function  getDeviceGroup - gets a specific device group based on the device id.
* @function  toggleEdit - toggle between the view of a device, and edit mode of the device.
* @function  back - cancels edit mode.
* @function  saveDeviceGroup - if changes made in edit mode, the device group is saved. DB updated, based on the id of the device.
* @function  deleteDeviceGroup - deletes a device group from the db, where id matches.
*/

app.controller('EditDeviceGroupController', function ($scope, $routeParams, DeviceGroups) {
    DeviceGroups.getDeviceGroup($routeParams.deviceGroupId)
    .then(function (doc) {
        $scope.deviceGroups = doc.data
        $scope.deviceGroup = $scope.deviceGroups[0]
    }, function (response) {
        window.alert(response)
    })

    $scope.toggleEdit = function () {
        $scope.editMode = true
        $scope.deviceFormGroupUrl = '/admin/deviceManagement/deviceFormGroup.html'
    }

    $scope.back = function () {
        $scope.editMode = false
        $scope.deviceFormGroupUrl = ''
    }

    $scope.saveDeviceGroup = function (deviceGroup) {
        DeviceGroups.editDeviceGroup(deviceGroup)
        $scope.editMode = false
        $scope.deviceFormGroupUrl = ''
    }

    $scope.deleteDeviceGroup = function (deviceGroup) {
        DeviceGroups.deleteDeviceGroup(deviceGroup)
    }
})
