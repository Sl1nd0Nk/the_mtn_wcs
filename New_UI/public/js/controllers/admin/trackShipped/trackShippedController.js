app.controller('trackShippedController', function ($scope, $location, $http, $mdDialog, $filter) {
    $scope.trackHeader = "Shipped And Dispatched"

    $scope.tableHeaders = {
        cartonID:'Carton ID',
        PackedDate: 'Packed Date',
        DispatchedDate: 'Dispatched Date',
        shipped: 'Shipped',
        dispatched: 'Dispatched'
    }

    $http.get('/getLoggedInUser').then(function(response) {
        if(response.data.code == '00') {
            $scope.$watch('myDate',function(val) {
				let dd = null;
				if(val){
					dd = val;
				}else{
					dd= new Date();
				}

				let theDate = $filter('date')(dd, "yyyy-MM-dd").substr(0,10);
                $http.get('/getCartonShipped/' + theDate).then(function(response) {
                    if(response.data.Data.length > 0) {
                        $scope.count = response.data.Data.length;
                        $scope.cartons = [];
                        $scope.cartons = response.data.Data;
                    } else {
						$scope.count = 0;
						$scope.cartons = [];
                        console.log('no shipped data was found.')
                    }
                })
            })
        } else {
			$scope.count = 0;
			$scope.cartons = [];
            console.log('logged in user not found.')
            $location.path('/')
        }
    })

    $scope.getCartonID = function(cartonID) {
        $http.get('/fetchCarton/' + cartonID).then(function(response) {
            if(response.data.Data) {
 				$scope.CartonID = '';
				$scope.cartons = []
                $scope.cartons.push(response.data.Data);
                $scope.count = $scope.cartons.length;
            } else {
                var confirm = $mdDialog.alert()
                .title(response.data.message)
                .ok('Ok')

                $mdDialog.show(confirm).then(function() {
				$scope.CartonID = '';
				});
            }
        })
    }
})
