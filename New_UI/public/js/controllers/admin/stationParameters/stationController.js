app.controller('stationController', function ($scope, $location, $http, $mdDialog, $filter) {
   $scope.paramHeader = 'Chute Management | All'
   $scope.tableHeaders = {
       stationNumber    : "Station Number",
       schuteNumber     : "Chute Number",
       dailyTotal       : "Daily Total",
       currentTote      : "Current Tote",
       activeState      : "Active State",
       activeState2     : "Active State 2"
   }

   $http.get('/getLoggedInUser').then(function(response) {
       if(response.data.code == '00') {
        $http.get('/getStation').then(function(response) {
            if(response.data.code === '00') {
                $scope.stationParamDisplay = response.data.data
        
                $scope.show = function(value) {
                    var displayState = []
                    switch (value) {
                        case 'All':
                            $http.get('/getStation').then(function(allResponse) {
                                $scope.paramHeader = 'Chute Management | All'
                                console.log(allResponse.data.data[0].schuteNumber)
                                $scope.stationParamDisplay = allResponse.data.data
                            })
                            break
                        case 'Inactive':
                            $http.get('/getStation').then(function(inactiveResponse) {
                                $scope.paramHeader = 'Chute Management | Inactive'
                                var x = 0
                                while (x < inactiveResponse.data.data.length) {
                                    if (!inactiveResponse.data.data[x].active) {
                                        displayState.push(inactiveResponse.data.data[x])
                                    }
                                    x++
                                }
            
                                $scope.stationParamDisplay = displayState
                            })
                            break
                        case 'Active':
                            $http.get('/getStation').then(function(activeResponse) {
                                $scope.paramHeader = 'Chute Management | Active'
                                var x = 0
                                while (x < activeResponse.data.data.length) {
                                    if (activeResponse.data.data[x].active) {
                                        displayState.push(activeResponse.data.data[x])
                                    }
                                    x++
                                }
            
                                $scope.stationParamDisplay = displayState
                            })
                            break
                        case 'deactivate':
                            $http.post('/updateAll/'+value).then(function(updateResp){
                                if(updateResp.data.code == '00') {
                                    var confirm = $mdDialog.alert()
                                    .title('All the stations have been Deactivated')
                                    .ok('Ok')
                          
                                    $mdDialog.show(confirm).then(function() {
                                        $http.get('/getStation').then(function(allResponse) {
                                            $scope.paramHeader = 'Chute Management | All'
                                            $scope.stationParamDisplay = allResponse.data.data
                                        })
                                    });
                                } else {
                                    var confirm = $mdDialog.alert()
                                    .title('There was a problem deactivating stations.')
                                    .ok('Ok')
                          
                                    $mdDialog.show(confirm).then(function() {
                                        $http.get('/getStation').then(function(allResponse) {
                                            $scope.paramHeader = 'Chute Management | All'
                                            $scope.stationParamDisplay = allResponse.data.data
                                        })
                                    });
                                }
                            })
                            break
                        case 'activate':
                            $http.post('/updateAll/'+value).then(function(updateResp){
                                if(updateResp.data.code == '00') {
                                    var confirm = $mdDialog.alert()
                                    .title('All the stations have been Activated')
                                    .ok('Ok')
                          
                                    $mdDialog.show(confirm).then(function() {
                                        $http.get('/getStation').then(function(allResponse) {
                                            $scope.paramHeader = 'Chute Management | All'
                                            $scope.stationParamDisplay = allResponse.data.data
                                        })
                                    });
                                } else {
                                    var confirm = $mdDialog.alert()
                                    .title('There was a problem activating the stations.')
                                    .ok('Ok')
                          
                                    $mdDialog.show(confirm).then(function() {
                                        $http.get('/getStation').then(function(allResponse) {
                                            $scope.paramHeader = 'Chute Management | All'
                                            $scope.stationParamDisplay = allResponse.data.data
                                        })
                                    });
                                }
                            })
                            break
                        default:
        
                            break
                    }
                }
        
                $scope.updateState = function(stationID, state){
                    var stateToggle = ''
                    if (state) stateToggle='Deactivate'; else stateToggle='Activate'
                    var confirm = $mdDialog.confirm()
                    .title('Would you like to ' + stateToggle + ' this chute?')
                    //.textContent('Would you like to ' + stateToggle + ' station ' + stationID)
                    .ariaLabel('Lucky day')
                    .targetEvent(stationID)
                    .ok('Yes')
                    .cancel('No');
          
                    $mdDialog.show(confirm).then(function() {
                        $http.post('/updateStation/' + stationID).then(function(response) {
                            if(response.data.code == '00') {
                                $http.get('/getStation').then(function(response) {
                                    $scope.paramHeader = 'Chute Management | All'
                                    $scope.stationParamDisplay = response.data.data
                                })
                            }
                        })
                    }, function() {
                        
                    });
                }
            } else {
                var confirm = $mdDialog.alert()
                .title('No Station has been found.')
                .ok('Ok')

                $mdDialog.show(confirm).then(function() {
                    location.path('/')
                }, function() {});
            }
           })
       } else {
        var confirm = $mdDialog.alert()
        .title('The user session was not found. Please log in before requesting the page.')
        .ok('Ok')

        $mdDialog.show(confirm).then(function() {
            $location.path('/')
        }, function() {});
       }
   })
})
