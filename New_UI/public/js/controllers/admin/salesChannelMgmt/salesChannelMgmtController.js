app.controller('salesChannelMgmtController', function ($scope, $http, $mdDialog, $rootScope, $location) {
    $http.get('/getLoggedInUser').then(function (response) {
        if (response.data.code == '00') {
            $http.get('/getParameters/').then(function (response) {
                if (response.data.code == '00') {
                    $scope.ordertypes = response.data.data[0].orderTypes
//                    console.log('zzz ' + response.data.data[0].orderTypes.length)
                    $rootScope.refreshData = function () {
                        $http.get('/getParameters/').then(function (response) {
                            $scope.ordertypes = response.data.data[0].orderTypes
                        })
                    }

                    $scope.addOrderType = function() {
                        $mdDialog.show({
                            controller: addOrderTypeController,
                            templateUrl: '/admin/salesChannelMgmt/dialogueTemplates/addOrderTypeDialog.html',
                            parent: angular.element(document.body),
                            clickOutsideToClose: true,
                            fullscreen: $scope.customFullscreen
                        })
                            .then(function(answer) { }, function() { });
                    }

                    function addOrderTypeController($scope, $rootScope, $mdDialog) {
                        $scope.answer = function (answer, addData) {
                            $mdDialog.hide(answer)
                            if (answer == 'addOrderType') {
                                $http.post('/addOrderType/' + JSON.stringify(addData)).then(function (response) {
                                    if (response.data.code == '00') {
                                        var confirm = $mdDialog.alert()
                                                                .title('Order Type has successfully been added.')
                                                                .ok('Ok')
                                        $mdDialog.show(confirm).then(function () { $rootScope.refreshData() }, function () { });
                                    } else {
                                        var confirm = $mdDialog.alert()
                                                                .title(response.data.message)
                                                                .ok('Ok')
                                        $mdDialog.show(confirm).then(function () { }, function () { });
                                    }
                                })
                            } else {
                                // DO NOTHING
                            }
                        }
                    }

                    $scope.editOrderType = function (ordertypeData) {
                        $rootScope.editOrderType = ordertypeData
                        $mdDialog.show({
                            controller: editOrderTypeController,
                            templateUrl: '/admin/salesChannelMgmt/dialogueTemplates/editOrderTypeDialog.html',
                            parent: angular.element(document.body),
                            clickOutsideToClose: true,
                            fullscreen: $scope.customFullscreen
                        })
                            .then(function (answer) { }, function () { });
                    }

                    function editOrderTypeController($scope, $rootScope, $mdDialog) {
                        $scope.ordertypeData = {
                            orderType:          $rootScope.editOrderType.orderType,
                            maxOrdersPerTote:   $rootScope.editOrderType.maxOrdersPerTote
                        }
                        $scope.answer = function (answer, editData) {
                            $scope.sendData = {
                                orderType:          editData.orderType,
                                maxOrdersPerTote:   editData.maxOrdersPerTote
                            }
                            $mdDialog.hide(answer)
                            if (answer == 'editOrderType') {
                                $http.post('/editOrderType/' + JSON.stringify($scope.sendData)).then(function (response) {
                                    if (response.data.code == '00') {
                                        var confirm = $mdDialog.alert()
                                                                .title('Order Type has been updated successfully.')
                                                                .ok('Ok')
                                        $mdDialog.show(confirm).then(function () { $rootScope.refreshData() }, function () { });
                                    } else {
                                        var confirm = $mdDialog.alert()
                                                                .title(response.data.message)
                                                                .ok('Ok')
                                        $mdDialog.show(confirm).then(function () { }, function () { });
                                    }
                                })
                            } else {
                                // DO NOTHING
                            }
                        }
                    }

                    $scope.deleteOrderType = function (ordertype) {
                        var confirm = $mdDialog.confirm()
                                                .title('Are you sure that you want to delete ' + ordertype)
                                                .ok('Ok')
                                                .cancel('Cancel')
                        $mdDialog.show(confirm).then(function () {
                            $http.post('/deleteOrderType/' + ordertype).then(function (response) {
                                if (response.data.code == '00') {
                                    var confirm = $mdDialog.alert()
                                                            .title('Order Type ' + ordertype + ' has successfully been deleted.')
                                                            .ok('Ok')
                                    $mdDialog.show(confirm).then(function () { $rootScope.refreshData() }, function () { });
                                } else {
                                    var confirm = $mdDialog.alert()
                                                            .title(response.data.message)
                                                            .ok('Ok')
                                    $mdDialog.show(confirm).then(function () { }, function () { });
                                }
                            })
                        });
                    }

                } else {
                    var confirm = $mdDialog.alert()
                                            .title(response.data.message)
                                            .ok('Ok')
                    $mdDialog.show(confirm).then(function () { $location.path('/toteTracking') }, function () { });
                }
            })
        } else {
            var confirm = $mdDialog.alert()
                                    .title('User session was not found. Please log into the system before attempting to access this page.')
                                    .ok('Ok')
            $mdDialog.show(confirm).then(function () { $location.path('/') }, function () { });
        }
    })
})
