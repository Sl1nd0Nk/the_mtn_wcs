app.controller('CancelledController', function ($scope, $location, $mdDialog, $routeParams, loginService, PickListService, $window, $http) {
	var NewStnPrintQ;
    $scope.header = 'Cancelled Order View';

	$scope.CheckUser = function(){
		loginService.getLoggedInUser().then(function(response){
			if(response.status == 200){
				let UserData = response.data;

				if($scope.SetViewState(UserData, true)){
					$scope.UpdateLoggedInUser(UserData.user);
					$scope.ShowView();
				}
			}
		});
	} /* CheckUser */

	$scope.SetViewState = function(Data, Init){
		if(Data.NoUser){
			$location.path('/');
			return false;
		}

		return true;
	} /* SetViewState */

	$scope.UpdateLoggedInUser = function(user){
		loggedInUser.innerHTML = '<i class="fa fa-user fa-fw"></i>' + user;
	} /* UpdateLoggedInUser */

	$scope.ShowView = function(){
		$scope.ListView = true;
		$scope.OrderListView = false;
		$scope.PrintView = false;

		PickListService.getCancelledOrderList().then(function(doc){
			if(doc.status != 200){
				return window.alert('Failed to load cancelled orders view');
			}

			if($scope.SetViewState(doc.data, false)){
				let Data = doc.data.Orders;
				$scope.ViewDisplay = Data;
			}
		});
	} /* ShowView */

	$scope.orderView = function(OrderID){
		$scope.ListView = false;
		$scope.OrderListView = true;
		$scope.PrintView = false;
		$scope.CurrentOrder = OrderID;

		let data = {
			OrderID: OrderID
		}

		PickListService.CanReqPicklists(JSON.stringify(data)).then(function(doc){
			if(doc.status != 200){
				return window.alert('Failed To Find Details For Order');
			}

			if($scope.SetViewState(doc.data, false)){
				let Err = doc.data.Err;
				if(Err){
					return window.alert(Err);
				}

				let Data = doc.data.Picklists;
				let ReadyForManifest = doc.data.ReadyForManifest;
				let PrintQs = doc.data.PrintQs;
				$scope.OrderViewDisplay = Data;
				$scope.ReadyForManifest = ReadyForManifest;
				$scope.PrintQs = PrintQs;
			}
		});
	} /* orderView */

	$scope.closeDetailView = function(){
		$scope.ListView = true;
		$scope.OrderListView = false;
		$scope.PrintView = false;
	} /* closeDetailView */

	$scope.PrintManifest = function(){
		$scope.PrintView = true;
		$scope.ListView = false;
		$scope.OrderListView = false;
	} /* PrintManifest */

	$scope.ClosePrintManifest = function(){
		$scope.PrintView = false;
		$scope.ListView = false;
		$scope.OrderListView = true;
	} /* ClosePrintManifest */

	$scope.selectedPrintQChanged = function (StnPrintQ){
		NewStnPrintQ = StnPrintQ;
	} /* selectedCartonSizeChanged */

	$scope.ConfirmPrint = function(){
		$scope.PrintView = false;
		$scope.ListView = false;
		$scope.OrderListView = true;
		let Queue = NewStnPrintQ.QName;

		let data = {
			Queue: NewStnPrintQ.QName,
			Order: $scope.CurrentOrder
		}

		PickListService.PrintCancelManifest(JSON.stringify(data)).then(function(doc){
			if(doc.status != 200){
				return window.alert('Failed to load cancelled orders view');
			}

			if($scope.SetViewState(doc.data, false)){
				let Msg = doc.data.Msg;
				if(Msg){
					window.alert(Msg);
				}

				$window.location.reload();
			}
		});
	} /* ConfirmPrint */

    $scope.CheckUser();
});




