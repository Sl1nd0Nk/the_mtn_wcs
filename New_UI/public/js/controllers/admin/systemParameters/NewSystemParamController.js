/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-14 20:25:52
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-04-25 06:55:52
 */

/**
* @controller NewSystemParamController
* @scope -listOfSystemParameters -this is the list of all system parameter types.
* @scope -listOfBoolValues -this is a list of the boolean dropdown option values.
* @function back - returns to previous view.
* @function saveSystemParam - creates a new system parameter. db will generate the first available id.
* @function selectedValueItemChanged - sets the type selected in the dropdown.
* @function selectedRoleValueItemChanged - sets the role selected in the dropdown.
* @function selectedGroupItemChanged - sets the group selected in the dropdown.
* @function getListOfSystemParameterGroups - retrieves a list of all system parameter groups from the db.
* @function getListOfUserRoles - retrieves a list of all user roles.

*/

app.controller('NewSystemParamController', function ($scope, $location, SystemParams, SystemParamGroups, UserRoles) {
    $scope.listOfSystemParameters = ['Integer', 'Boolean', 'IP']
    $scope.listOfBoolValues = ['True', 'False']

    $scope.default = true
    $scope.selectedValueItemId = ''
    $scope.selectedRoleValueItemId = ''
    $scope.selectedGroupValueItemId = ''

    $scope.back = function () {
        $location.path('/systemParameter')
    }

    $scope.saveSystemParam = function (systemParam) {
        let itemType = $scope.selectedValueItem
        systemParam.dataType = itemType

        let itemGroup = $scope.selectedGroupValueItemId
        systemParam.groupID = itemGroup

        let itemRole = $scope.selectedRoleValueItemId
        systemParam.roleID = itemRole

        SystemParams.createSystemParam(systemParam)
        .then(function (doc) {
            if (doc.data === 'Success') {
                $location.path('/systemParameter/')
            }
        }, function (response) {
            window.alert(response)
        })
    }

    $scope.selectedValueItemChanged = function () {
        switch ($scope.selectedValueItem) {
        case 'Integer':
            $scope.intSelected = true
            $scope.boolSelected = false
            $scope.devLookSelected = false
            $scope.ipSelected = false
            $scope.default = false
            break
        case 'Boolean':
            $scope.intSelected = false
            $scope.boolSelected = true
            $scope.devLookSelected = false
            $scope.ipSelected = false
            $scope.default = false
            break
        case 'IP':
            $scope.intSelected = false
            $scope.boolSelected = false
            $scope.devLookSelected = false
            $scope.ipSelected = true
            $scope.default = false
            break
        }
    }

    $scope.selectedRoleValueItemChanged = function (selectedRoleValueItem) {
        $scope.selectedRoleValueItemId = selectedRoleValueItem.id
    }

    $scope.selectedGroupItemChanged = function (selectedGroupValueItem) {
        $scope.selectedGroupValueItemId = selectedGroupValueItem.id
    }

    getListOfSystemParameterGroups(SystemParamGroups, $scope)
    getListOfUserRoles(UserRoles, $scope)

    function getListOfSystemParameterGroups (SystemParamGroups, $scope) {
        SystemParamGroups.getSystemParamGroups()
        .then(function (doc) {
            $scope.listOfSystemParameterGroups = doc.data
        }, function (response) {
            window.alert(response)
        })
    }

    function getListOfUserRoles (UserRoles, $scope) {
        UserRoles.getUserRoles()
        .then(function (doc) {
            $scope.listOfUserRoles = doc.data
        }, function (response) {
            window.alert(response)
        })
    }
})
