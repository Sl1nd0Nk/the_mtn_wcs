/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-14 09:21:41
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-07-28 14:09:56
 */

/**
* @controller EditSystemParamGroupController
* @function  getSystemParamGroup - gets a specific system parameter group based on the id of the system parameter group.
* @function  toggleEdit - toggle between the view of a system parameter group, and edit mode of it.
* @function  back - cancels edit mode.
* @function  saveSystemParamGroup - if changes made in edit mode, the system parameter group is saved. DB updated, based on the id of the system parameter group.
* @function  deleteSystemParamGroup - deletes a system parameter group from the db, where id matches.
*/

app.controller('EditSystemParamGroupController', function ($scope, $routeParams, SystemParamGroups) {
    SystemParamGroups.getSystemParamGroup($routeParams.systemParameterGroupId)
    .then(function (doc) {
        $scope.systemParamGroups = doc.data
        $scope.systemParamGroup = $scope.systemParamGroups[0]
    }, function (response) {
        window.alert(response)
    })

    $scope.toggleEdit = function () {
        $scope.editMode = true
        $scope.systemParamGroupFormUrl = '/admin/systemParameters/parameterFormGroup.html'
    }

    $scope.back = function () {
        $scope.editMode = false
        $scope.systemParamGroupFormUrl = ''
    }

    $scope.saveSystemParamGroup = function (systemParamGroup) {
        SystemParamGroups.editSystemParamGroup(systemParamGroup)
        $scope.editMode = false
        $scope.systemParamGroupFormUrl = ''
    }

    $scope.deleteSystemParamGroup = function (systemParamGroupId) {
        SystemParamGroups.deleteSystemParamGroup(systemParamGroupId)
    }
})
