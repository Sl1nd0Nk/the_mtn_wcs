/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-14 20:23:53
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-04-25 06:42:03
 */

/**
* @controller EditSystemParamController
* @function  getSystemParam - gets a specific system parameter based on the id of the system parameter.
* @function  toggleEdit - toggle between the view of a system parameter, and edit mode of it.
* @function  back - cancels edit mode.
* @function  saveSystemParam - if changes made in edit mode, the system parameter is saved. DB updated, based on the id of the system parameter.
* @function  deleteSystemParam - deletes a system parameter from the db, where id matches.
*/

app.controller('EditSystemParamController', function ($scope, $routeParams, $location, SystemParams) {
    SystemParams.getSystemParam($routeParams.systemParameterId)
    .then(function (doc) {
        $scope.systemParams = doc.data
        $scope.systemParam = $scope.systemParams[0]
    }, function (response) {
        window.alert(response)
    })

    $scope.toggleEdit = function () {
        $scope.editMode = true
        $scope.systemParamFormUrl = '/admin/systemParameters/parameterForm.html'
    }

    $scope.back = function () {
        $scope.editMode = false
        $scope.systemParamFormUrl = ''
    }

    $scope.saveSystemParam = function (systemParam) {
        SystemParams.editContact(systemParam)
        $scope.editMode = false
        $scope.systemParamFormUrl = ''
    }

    $scope.deleteSystemParam = function (systemParam) {
        SystemParams.deleteSystemParam(systemParam)
        .then(function (doc) {
            if (doc.data === 'Success') {
                $location.path('/systemParameter/')
            }
        }, function (response) {
            window.alert(response)
        })
    }
})
