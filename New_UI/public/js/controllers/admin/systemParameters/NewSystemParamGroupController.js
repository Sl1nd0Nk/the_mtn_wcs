/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-14 09:21:24
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-04-25 06:56:10
 */

/**
* @controller NewSystemParamGroupController
* @function back - returns to previous view.
* @function saveSystemParamGroup - creates a new system parameter group. db will generate the first available id..
*/

app.controller('NewSystemParamGroupController', function ($scope, $location, SystemParamGroups) {
    $scope.back = function () {
        $location.path('/systemParameterGroup')
    }

    $scope.saveSystemParamGroup = function (systemParamGroup) {
        SystemParamGroups.createSystemParamGroup(systemParamGroup)
        .then(function (doc) {
            if (doc.data === 'Success') {
                $location.path('/systemParameterGroup/')
            }
        }, function (response) {
            window.alert(response)
        })
    }
})
