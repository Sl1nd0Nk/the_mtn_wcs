/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-14 09:21:53
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-03-27 14:00:37
 */

/**
* @controller SystemParamGroupController
* @scope  - this will return a list of all the system parameter groups from the db.
*/


app.controller('SystemParamGroupController', function (systemParamGroups, $scope) {
    $scope.systemParamGroups = {
        data: [],
        sort: {
            predicate: 'companyName',
            direction: 'asc',
            urlSync: true
        }
    }

    $scope.systemParamGroups.data = systemParamGroups.data
})
