app.controller('SystemParamController', function ($scope, $http, $mdDialog, $rootScope) {
    $scope.toteConsole      = []
    $scope.courierRoutes    = []
    $scope.weightSettings   = []
    $scope.trolleySettings  = []

    $scope.header = 'Parameters'

    $scope.hideCourierRoute = true
    $scope.weightSettingDiv = true
    $scope.toteConsolidation = true
    $scope.trolleySet       = true

    $scope.tableConsoleHeader = {
        active        : 'Active State',
        maxOrder      : 'Maximum Orders to Consolidate',
        toteMaxVol    : 'Tote Maximum Volume',
        toteMaxWeight : 'Tote Maximum Weight',
        consolidationInProgress: 'Consolidation In Progress',
        consolSeqNum  : 'Consolidation Sequence Number',
        minProdMatches: 'Minimum Product Matches'
    }

    $scope.tableCourierRoutes = {
        courierName        : 'Courier Name',
        routeNumber        : 'Route Number'
    }

    $scope.tableWeightSettingsHeader = {
        stockSize           : 'Size',
        upperTolerance      : 'Upper Tolerance',
        lowerTolerance      : 'Lower Tolerance',
        weight              : 'Weight',
        useTransporter      : 'Transporter',
        width               : 'Width',
        height              : 'Height'
    }

    $scope.showConsole = function() {
        if($scope.toteConsolidation)
            $scope.toteConsolidation = false
        else
            $scope.toteConsolidation = true
    }

    $scope.tableTrolleyHeader = {
        name : 'Name', 
        isDefaultMode : 'Default Mode', 
        isActive : 'Active', 
        instructionMsgFinal : 'Instruction Message', 
        toteIDLeadDigit : 'Lead Digit',
	maxTotesPerTrolley : 'Maximum Totes Per Trolley: ', 
        trolleyZone : 'Trolley Zone: ',
    }

    $scope.showWeightSet = function() {
        if($scope.weightSettingDiv)
            $scope.weightSettingDiv = false
        else
            $scope.weightSettingDiv = true
    } 

    $scope.showCourier = function(){
        if($scope.hideCourierRoute)
            $scope.hideCourierRoute = false
        else
            $scope.hideCourierRoute = true
    }

    $scope.showTrolleySetting = function() {
        if($scope.trolleySet)
            $scope.trolleySet = false
        else
            $scope.trolleySet = true
    }

    $scope.addCourier = function(){
      console.log('Adding Courier Company')
    }

    $scope.addCourier = function(ev){
        $mdDialog.show({
            controller: DialogController,
            templateUrl: '/admin/systemParameters/dialog1.tmpl.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        })
        .then(function(answer) {
            // $scope.status = 'You said the information was "' + answer + '".';
        }, function() {
            // $scope.status = 'You cancelled the dialog.';
        });
    }

    $scope.editWeight = function(stockZize, upper, lower, weight, transporter, width, height) {
	$rootScope.weightFields = {
            stockSize: stockZize,
            upper: upper, 
            lower: lower,
            weight: weight,
            transporter: transporter,
            width: width,
            height: height
        }

        if(stockZize) {
            $mdDialog.show({
                controller  : WeightController,
                templateUrl : '/admin/systemParameters/dialogTemplates/editWeightDialog.html',
                parent: angular.element(document.body),
                targetEvent : stockZize,
                clickOutsideToClose:true,
                fullscreen  : $scope.customFullscreen 
            })
            .then(function(answer) {}, function() {});
        }
    }

    $scope.removeCourier = function(courierCo){
        var confirm = $mdDialog.confirm()
        .title('Are you sure that you want to remove courier ', courierCo + '?')
        .ok('Ok')
        .cancel('Cancel')

        $mdDialog.show(confirm).then(function() {
            $http.get('/removeCourier/' + courierCo).then(function(response) {
                if(response.data.code == '00') {
                    var confirm = $mdDialog.confirm()
                    .title(response.data.message)
                    .ok('Ok')

                    $mdDialog.show(confirm).then(function() {
                        $rootScope.fetchParams()
                    }, function() {});
                } else {
                    var confirm = $mdDialog.confirm()
                    .title(response.data.message)
                    .ok('Ok')

                    $mdDialog.show(confirm).then(function() {}, function() {});
                }
            })
        }, function() {});
    }

     $scope.addWeight = function() {
        $mdDialog.show({
            controller: AddWeightController,
            templateUrl: '/admin/systemParameters/dialogTemplates/addWeightDialog.html',
            parent: angular.element(document.body),
            clickOutsideToClose:true,
            fullscreen: $scope.customFullscreen 
        })
        .then(function(answer) {}, function() {});
    }

     $scope.editCourier = function(courier, routeNumber){
	$rootScope.courier = courier
	$rootScope.routeNumber = routeNumber

	// console.log('COURIER NAME' + $scope.courierData.courierName)

        $mdDialog.show({
            controller: EditCourierController,
            templateUrl: '/admin/systemParameters/dialogTemplates/editCourierRoute.html',
            parent: angular.element(document.body),
            targetEvent: courier,
            clickOutsideToClose:true,
            fullscreen: $scope.customFullscreen 
        })
        .then(function(answer) {}, function() {});
    }

    $rootScope.fetchParams = function(){
    $http.get('/getParameters').then(function(response){
        if(response.data.code == '00'){
            $scope.toteConsole.push(response.data.data[0].toteConsolidation)
            $scope.courierRoutes   = response.data.data[0].courierRoutes
            $scope.weightSettings  = response.data.data[0].weightSettings
            $scope.trolleySettings = response.data.data[0].trolleySettings.pickModes

	    $scope.maxTrolleyTotes = response.data.data[0].trolleySettings.maxTotesPerTrolley
            $scope.trolleyZone     = response.data.data[0].trolleySettings.trolleyZone
        }else{
            console.log('PARAMETERS NOT FOUND')
        }
     })
    }
	$rootScope.fetchParams()

    function EditCourierController($scope, $rootScope  ,$mdDialog) {
	$scope.courierName = 'Courier Name'
	
	$scope.courierData = {
            courierName : $rootScope.courier,
	    routeNumber : $rootScope.routeNumber 
        }
	
        $scope.answer = function(answer, courierData) {
            $mdDialog.hide(answer);
            if(answer == 'addCourier'){
   		console.log($rootScope.courier, ' | ', courierData.routeNumber, ' | ', courierData.courierName, ' | ', $rootScope.routeNumber )
                var sendData = {
                    oldCourier: $rootScope.courier,
                    newCourier: courierData.courierName,
                    oldRoute: $rootScope.routeNumber,
                    newRoute: courierData.routeNumber
                }

                var strSendData = JSON.stringify(sendData)
		console.log(strSendData)
                $http.post('/changeCourier/'+strSendData).then(function(response){
                    if(response.data.code == '00') {
                        var confirm = $mdDialog.confirm()
                        .title('Courier successfully updated. ')
                        .ok('Ok')

                        $mdDialog.show(confirm).then(function() {
                            $rootScope.fetchParams()
                        }, function() {})
                    } else {
                        var confirm = $mdDialog.confirm()
                        .title(response.data.message)
                        .ok('Ok')

                        $mdDialog.show(confirm).then(function() {}, function() {})
                    }
                })             
            } else {
                console.log('There is no answer edit controller!!')
            }
        };
    }
	
     function AddWeightController($scope, $rootScope, $mdDialog) {
        $scope.answer = function(answer, addData) {
            $mdDialog.hide(answer)
            if(answer == 'addWeight') {
                var sendAddData = JSON.stringify(addData) 
                $http.post('/addWeight/'+sendAddData).then(function(response) {
                    if(response.data.code == '00') {
                        var confirm = $mdDialog.alert()
                        .title('Weight Setting Updated Successfully.')
                        .ok('Ok')

                        $mdDialog.show(confirm).then(function() {
                            $rootScope.fetchParams()
                        }, function() {});
                    } else {
                        var confirm = $mdDialog.alert()
                        .title(response.data.message)
                        .ok('Ok')

                        $mdDialog.show(confirm).then(function() {
                            $rootScope.fetchParams()
                        }, function() {});
                    }
                })
            } else {
                console.log('No answer. Cancel has been pressed on modal. Add Weight!')
            }
        }
    }
     
     function WeightController($scope, $rootScope, $mdDialog) {
        $scope.weightData = {
            stockSize   : $rootScope.weightFields.stockSize,
            upper       : $rootScope.weightFields.upper,
            lower       : $rootScope.weightFields.lower,
            weight      : $rootScope.weightFields.weight,
            transporter : $rootScope.weightFields.transporter,
            width       : $rootScope.weightFields.width,
            height      : $rootScope.weightFields.height
        }

        $scope.answer = function(answer, weightData) {
            $mdDialog.hide(answer);

            var weightDataUpdate = {
                stockSizeOld    : $rootScope.weightFields.stockSize,
                stockSize       : $scope.weightData.stockSize,
                upper           : $scope.weightData.upper,
                lower           : $scope.weightData.lower,
                weight          : $scope.weightData.weight,
                transporter     : $scope.weightData.transporter,
                width           : $scope.weightData.width,
                height          : $scope.weightData.height,
            }

            var strWeightData = JSON.stringify(weightDataUpdate)

            if(answer == 'editWeight') {
                $http.post('/updateWeightData/'+strWeightData).then(function(response) {
                    if(response.data.code == '00') {
                        var confirm = $mdDialog.alert()
                        .title('Weight successfully updated.')
                        .ok('Ok')

                        $mdDialog.show(confirm).then(function() {
                            $rootScope.fetchParams()
                        }, function() {});
                    } else {
                        var confirm = $mdDialog.alert()
                        .title(response.data.message)
                        .ok('Ok')

                        $mdDialog.show(confirm).then(function() {
                            $rootScope.fetchParams()
                        }, function() {});
                    }
                })
            } else {
                console.log('There is no answer Weight controller!!')
            }
        }
    }

     function DialogController($scope, $mdDialog) {
        $scope.answer = function(answer, courierData) {
            $mdDialog.hide(answer);
            if(answer == 'addCourier'){
                console.log('THE ANSWER!! ' + answer + ' | ' + courierData.courierName + ' - ' + courierData.routeNumber)
                $http.get('/updateCourierParam/' + JSON.stringify(courierData)).then(function(response){
                    if(response.data.code == '00'){
                        var confirm = $mdDialog.alert()
                        .title('New courier route successfully added.')
                        .ok('Ok')

                        $mdDialog.show(confirm).then(function() {
                            $rootScope.fetchParams()
                        }, function() {
                            $rootScope.fetchParams()
                        });
                    }else{
                        // console.log(response.data.message)
			var confirm = $mdDialog.confirm()
                        .title(response.data.message)
                        .ok('Ok')

                        $mdDialog.show(confirm).then(function() {}, function() {});
                    }
                })
            } else {
                console.log('There is no answer!!')
            }
        };
    }
})
