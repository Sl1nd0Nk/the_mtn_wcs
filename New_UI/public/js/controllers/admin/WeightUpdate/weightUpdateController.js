app.controller('weightUpdateController', function ($scope, $location, $routeParams, $http, loginService, PickListService, $window, $timeout){
	var NewUom;
	$scope.NotAuth = false;
	$scope.ScanProd = false;
	$scope.PrdDet = false;
	$scope.PrdUp = false;

	$scope.Permissions = function(){
		loginService.getLoggedInUser().then(function(response){
			if(response.status == 200){
				let UserData = response.data;

				if($scope.SetViewState(UserData, true)){
					$scope.UpdateLoggedInUser(UserData.user);
					$scope.ScanProd = true;
				}
			}
		});
	} /* Permissions */

	$scope.SetViewState = function(Data, Init){
		if(Init)
			$scope.InitPage();

		if(Data.NoUser){
			$location.path('/');
			return false;
		}

		if(!Data.roleID || Data.roleID <= 1){
			$scope.UnAutherised();
			return false;
		}

		return true;
	} /* SetViewState */

	$scope.InitPage = function(){
		$scope.NotAuth = false;
		$scope.ScanProd = false;
		$scope.PrdDet = false;
		$scope.PrdUp = false;
	} /* InitPage */

	$scope.UnAutherised = function(){
		$scope.InitPage();
		$scope.NotAuth = true;
		return false;
	} /* UnAutherised */

	$scope.ClearInput = function(id){
		let Input = document.getElementById(id);
		Input.value = '';
		Input.focus();
	} /* ClearInput */

	$scope.UpdateLoggedInUser = function(user){
		loggedInUser.innerHTML = '<i class="fa fa-user fa-fw"></i>' + user;
	} /* UpdateLoggedInUser */

	$scope.getProduct = function(Product){
		$scope.ClearInput('scanProduct');

		let data = {
			Product: Product
		}

		PickListService.WADGetProduct(JSON.stringify(data)).then(function(doc){
			if(doc.status != 200){
				return window.alert(doc.data.Err);
			}

			if(doc.data.Err){
				return window.alert(doc.data.Err);
			}

			if($scope.SetViewState(doc.data, false)){
				let Data = doc.data.ProdData;
				$scope.Sku = Data.Sku;
				$scope.SkuDesc = Data.SkuDesc;
				let UOMs = Data.UOMs;
				$scope.UOMs = UOMs;
				$scope.ScanProd = false;
				$scope.PrdDet = true;
				$scope.PrdUp = false;
			}
		});
	} /* getProduct */

	$scope.CancelProcess = function(){
		$scope.Permissions();
	} /* CancelProcess */

	$scope.selectedUomChanged = function (Uom){
		NewUom = Uom;
	} /* selectedUomChanged */

	$scope.SelectUom = function(){
		$scope.MyUom = NewUom.Uom;
		$scope.Weight = NewUom.Weight;
		$scope.Height = NewUom.Height;
		$scope.Length = NewUom.Length;
		$scope.Width = NewUom.Width;

		$scope.ScanProd = false;
		$scope.PrdUp = true;
		$scope.PrdDet = false;

		return;

	} /* SelectUom */

	$scope.UpdateUom = function(mWeight, mHeight, mLength, mWidth){
		if(mWeight){
			if(isNaN(mWeight)){
				window.alert('Entered weight must be a number');
				return $scope.SelectUom();
			}
		}

		if(mLength){
			if(isNaN(mLength)){
				window.alert('Entered length must be a number');
				return $scope.SelectUom();
			}
		}

		if(mWidth){
			if(isNaN(mWidth)){
				window.alert('Entered width must be a number');
				return $scope.SelectUom();
			}
		}

		if(mHeight){
			if(isNaN(mHeight)){
				window.alert('Entered height must be a number');
				return $scope.SelectUom();
			}
		}

		let Weight = mWeight? mWeight: mWeight==''? mWeight: $scope.Weight;
		let Height = mHeight? mHeight: mHeight==''? mHeight: $scope.Height;
		let Length = mLength? mLength: mLength==''? mLength: $scope.Length;
		let Width = mWidth? mWidth: mWidth==''? mWidth: $scope.Width;

		if(!Weight){
			window.alert('Entered weight must be a number');
			return $scope.SelectUom();
		} else {
			if(isNaN(Weight)){
				window.alert('Entered weight must be a number');
				return $scope.SelectUom();
			}
		}

		if(!Height){
			window.alert('Entered height must be a number');
			return $scope.SelectUom();
		} else {
			if(isNaN(Height)){
				window.alert('Entered height must be a number');
				return $scope.SelectUom();
			}
		}

		if(!Length){
			window.alert('Entered length must be a number');
			return $scope.SelectUom();
		} else {
			if(isNaN(Length)){
				window.alert('Entered length must be a number');
				return $scope.SelectUom();
			}
		}

		if(!Width){
			window.alert('Entered width must be a number');
			return $scope.SelectUom();
		} else {
			if(isNaN(Width)){
				window.alert('Entered width must be a number');
				return $scope.SelectUom();
			}
		}

		let data = {
			Product: $scope.Sku,
			Desc: $scope.SkuDesc,
			Uom: $scope.MyUom,
			FromWeight: $scope.Weight,
			Weight: Weight,
			FromHeight: $scope.Height,
			Height: Height,
			FromLength: $scope.Length,
			Length: Length,
			FromWidth: $scope.Width,
			Width: Width
		}

		PickListService.WADUpdateProduct(JSON.stringify(data)).then(function(doc){
			if(doc.status != 200){
				window.alert(doc.data.Err);
			}

			if(doc.data.Err){
				window.alert(doc.data.Err);
			}

			let Msg = doc.data.Msg;
			if(Msg){
				window.alert(Msg);
			}

			$scope.CancelProcess();
		});
	} /* SelectUom */

	$scope.Permissions();
});