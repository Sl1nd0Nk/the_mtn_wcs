app.controller('PrintToPDFController', function ($scope, $http, $mdDialog, $rootScope, $location, $window) {
   $scope.RedirectToURL = function() {
	   var searchObject = $location.search();
           var host = $window.location.host;
           var IpArr = host.split(':');
           var landingUrl = "http://"+ IpArr[0] +":3500/printtopdf/";
           $window.location.href = landingUrl;
      };

       $scope.RedirectToURL();
});
