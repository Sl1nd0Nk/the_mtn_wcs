app.controller('reprintController', function ($scope, $location, $http, $mdDialog) {
    $scope.printHeader = 'Print Documents'
    $scope.displayOrderID = false
    $scope.displayCartonID = true
    $scope.displayWhiteframe = false

    $http.get('/getLoggedInUser').then(function(response) {
        if(response.data.code == '00') {
            $http.get('/checkRole/three').then(function(response) {
                if(response.data.code == '00') {
                    $scope.show = function(state) {
                        if (state == 'OrderID') {
                            if (!$scope.displayOrderID) {
                                $scope.displayOrderID = !$scope.displayOrderID
                                $scope.displayCartonID = !$scope.displayCartonID
                            }
                        } else if (state == 'CartonID') {
                            if (!$scope.displayCartonID){
                                $scope.displayCartonID = !$scope.displayCartonID
                                $scope.displayOrderID = !$scope.displayOrderID
                            }
                        }
                    }
                
                    $scope.getCartonID = function (cartonID) {
                        $http.post('/reprintSingle/' + cartonID).then(function(response){
                            if (response) {
                                var confirm = $mdDialog.confirm()
                                .title('Carton ID: ' + cartonID)
                                .textContent('Carton ID: ' + cartonID + ' documents have been processed')
                                .ariaLabel('Lucky day')
                                .targetEvent(cartonID)
                                .ok('Ok')
                                .cancel('Back');
                      
                                $mdDialog.show(confirm).then(function() {
                                    $scope.CartonID = ''
                                }, function() {
                                    // $scope.status = 'You decided not to clear the tote.';
                                });
                            }
                        })
                    }
                
                    $scope.getOrderID = function (orderID) {
                        $http.post('/reprint/' + orderID).then(function(response){})
                        // $http.post('/reprint/' + orderID).then(function(response){})
                        var confirm = $mdDialog.confirm()
                        .title('Order ID: ' + orderID)
                        .textContent('Order ID: ' + orderID + ' documents are being processed')
                        .ariaLabel('Lucky day')
                        .targetEvent(orderID)
                        .ok('Ok')
                        .cancel('Back');
                
                        $mdDialog.show(confirm).then(function() {
                            $scope.OrderID = ''
                        }, function() {
                            // $scope.status = 'You decided not to clear the tote.';
                        });
                    }
                } else {
                    var confirm = $mdDialog.alert()
                    .title(response.data.message)
                    .ok('Ok')

                    $mdDialog.show(confirm).then(function() {
                        $location.path('/toteTracking')
                    }, function() {});
                }
            })
        } else {
            var confirm = $mdDialog.alert()
            .title('The user session was not found. Please log in before requesting the page.')
            .ok('Ok')

            $mdDialog.show(confirm).then(function() {
                $location.path('/')
            }, function() {});
        }
    })
})
