let match = false;

app.controller('unconsolidateController', function ($scope, $rootScope, $window, $sessionStorage, $location,  $http, $mdDialog, $filter, CheckToteStateService) {

    $http.get('/getLoggedInUser').then(function(response) {

        if (response.data.code == '00') {
            var loggedInUser = response.data.username;

           // alert('Hello Hi !! ');
           $scope.Start = function()
           {
            $scope.Clearance();
            $scope.unconsolidateStart = true;
            $scope.unconsolidateDisplay = false;
            $scope.un_NewTote = false;
            $scope.un_scanOrder = false;
            $scope.un_confirmOrder = false;
            $scope.un_confirmTote = false;
            $scope.unconsolidateDone = false;

            return;
            }

            $scope.Back1 = function()
            {
                $scope.Clearance();
                $scope.unconsolidateStart = true;
                $scope.unconsolidateDisplay = false;
                $scope.un_NewTote = false;
                $scope.un_scanOrder = false;
                $scope.un_confirmOrder = false;
                $scope.un_confirmTote = false;
                $scope.unconsolidateDone = false;

                //Remove my sessions
                delete $sessionStorage.myData;
                delete $sessionStorage.picklist_Av;
                delete $sessionStorage.New_Tote;
                delete $sessionStorage.N_tote;
                delete $sessionStorage.un_orderID;
                delete $sessionStorage.orderN;
                delete $sessionStorage.mainStatus;

                $window.sessionStorage.removeItem(loggedInUser + "_Tote");
                $window.sessionStorage.removeItem(loggedInUser +"_count");
                return;
            }

            $scope.Back3 = function()
            {
                $scope.Clearance();
                $scope.unconsolidateStart = true;
                $scope.unconsolidateDisplay = false;
                $scope.un_NewTote = false;
                $scope.un_scanOrder = false;
                $scope.un_confirmOrder = false;
                $scope.un_confirmTote = false;
                $scope.unconsolidateDone = false;

                //Remove my sessions
                delete $sessionStorage.myData;
                delete $sessionStorage.picklist_Av;
                delete $sessionStorage.New_Tote;
                delete $sessionStorage.N_tote;
                delete $sessionStorage.un_orderID;
                delete $sessionStorage.orderN;
                delete $sessionStorage.mainStatus;

                $window.sessionStorage.removeItem(loggedInUser + "_Tote");
                $window.sessionStorage.removeItem(loggedInUser +"_count");
                $location.path('/toteTracking');
                return;
            }

            $scope.Back2 = function()
            {
                $scope.Clearance();
                $scope.unconsolidateStart = true;
                $scope.unconsolidateDisplay = false;
                $scope.un_NewTote = false;
                $scope.un_scanOrder = false;
                $scope.un_confirmOrder = false;
                $scope.un_confirmTote = false;
                $scope.unconsolidateDone = false;
                //$sessionStorage.myData
                //Remove my sessions
                delete $sessionStorage.myData;
                delete $sessionStorage.picklist_Av;
                delete $sessionStorage.New_Tote;
                delete $sessionStorage.N_tote;
                delete $sessionStorage.un_orderID;
                delete $sessionStorage.orderN;
                delete $sessionStorage.mainStatus;

                $window.sessionStorage.removeItem(loggedInUser + "_Tote");
                $window.sessionStorage.removeItem(loggedInUser +"_count");

                //alert($scope.ToteID + ' AS ID');
                return;
            }

            $scope.ConsolidateValidate = function()
            {
                $scope.ClearInput('orderUN');
                var toteID = $scope.ToteID;

                var myData = {
                    toteID: toteID,
                    user: loggedInUser
                }
                //alert('Uzer 2');

                CheckToteStateService.GetConsolidatedPicklist(JSON.stringify(myData))
                .then(function (response) {
                    if (response.status != 200) {
                    var confirm = $mdDialog.confirm()
                    .title('Tote ID: ' + toteID)
                    .textContent(response.data)
                    .ariaLabel('Lucky day')
                    .targetEvent(' HALALA ')
                    .ok('Ok');

                    $mdDialog.show(confirm);

                    $scope.unconsolidateStart = true;
                    //$location.path('/UnconsolidateOrders');
                    } else {
                    $window.sessionStorage.setItem(loggedInUser + "_Tote", toteID);
                        //$scope.toteID = $window.sessionStorage.getItem("TheTote");
                     $window.sessionStorage.setItem(loggedInUser +"_count", '1');
                    $sessionStorage.myData = myData;
                    $sessionStorage.picklist_Av = response.data;

                    //Set the status using mainStatus...!!!
                    $sessionStorage.mainStatus = $sessionStorage.picklist_Av.Status;

                    $scope.OrderStatus = $sessionStorage.mainStatus;
                    //Set the status using mainStatus...!!!
                    $scope.unconsolidateStart = false;
                    $scope.unconsolidateDisplay = true;
                    $scope.setHeaders();
                    $scope.setData($sessionStorage.picklist_Av);
                    //$sessionStorage.orderN = $scope.orderUN;
                    //alert( $sessionStorage.picklist_Av[0].orders[0].orderID + ' Is the 1st 1');
                    }
                });
                return;
            }

            $scope.GetValidatedPickList = function()
            {
               // alert('Uzer The lists 1111 ');

                if ($sessionStorage.myData != undefined)
                {
                CheckToteStateService.GetConsolidatedPicklist(JSON.stringify($sessionStorage.myData))
                .then(function (response) {

                    if (response.status != 200) {
                    //alert(response.status + ' STUATUS');
                    var confirm = $mdDialog.confirm()
                    .title('Tote ID: ' + $sessionStorage.myData.toteID)
                    .textContent(response.data)
                    .ariaLabel('Lucky day')
                    .targetEvent(' HALALA ')
                    .ok('Ok');

                    $mdDialog.show(confirm);

                    delete $sessionStorage.myData;
                    $scope.unconsolidateStart = true;
                    $location.path('/UnconsolidateOrders');
                    } else {
                    //alert(response.status + ' STUATUS');
                    $sessionStorage.picklist_Av = response.data;
                    //alert( $sessionStorage.picklist_Av[0].orders[0].orderID + ' Is the 1st 1');
                    $sessionStorage.mainStatus = $sessionStorage.picklist_Av.Status;
                    //Set the status using mainStatus...!!!
                    $scope.OrderStatus = $sessionStorage.mainStatus;
                    //Set the status using mainStatus...!!!

                    $scope.unconsolidateStart = false;
                    $scope.unconsolidateDisplay = true;
                    $scope.setHeaders();
                    $scope.setData($sessionStorage.picklist_Av);
                  //  $sessionStorage.orderN = $scope.orderUN;
                    }
                });

            } else {
                $scope.unconsolidateStart = true;
                $location.path('/UnconsolidateOrders');
            }
                return;
            }

            $scope.ClearInput = function(id){
                let Input = document.getElementById(id);
                Input.value = '';
                Input.focus();
            } /* ClearInput */

            $scope.ScanTote = function()
            {
                //It sets onclick of a button ....

                $sessionStorage.orderN = $scope.orderUN;
                var validFound = false;

                for (var i = 0; i < $sessionStorage.picklist_Av.orders.length; i++)
                {
                    //alert($sessionStorage.picklist_Av.orders[i].orderID)
                    if ($sessionStorage.orderN == $sessionStorage.picklist_Av.orders[i].orderID)
                    {
                        validFound = true;
                    }
                }

                if (!validFound)
                {
                    //if invalid the start afresh!!!
                    var confirm = $mdDialog.confirm()
                    .title('orderID: ' + $sessionStorage.orderN)
                    .textContent('Invalid for tote ' + $sessionStorage.picklist_Av.toteID)
                    .ariaLabel('Lucky day')
                    .targetEvent(' HALALA ')
                    .ok('Ok');

                    $mdDialog.show(confirm);
                   return  $scope.Rfresh();

                } else {
                    $scope.OrderIDm = $sessionStorage.orderN;
                    $sessionStorage.mainStatus = $sessionStorage.picklist_Av.Status;
            }

              //  $se
                //alert($sessionStorage.orderN + ' hhhh ');
                $scope.New_ToteID = '';
                $scope.unconsolidateDisplay = false;
                $scope.un_NewTote = true;
               // alert('Alive in Mecxioxo');
               return;
            }

            $scope.ToteNotEmpty = function()
            {
                $scope.Order_ID = '';
                var toteID_N = $scope.New_ToteID;
                //alert($sessionStorage.orderN + ' ORDER TEST');
                if (toteID_N != $sessionStorage.myData.toteID)
                {
                   // alert('*'+ toteID_N+'*');
                        if (toteID_N != '') {
                        CheckToteStateService.QtyCheck(toteID_N)
                        .then(function(response) {
                            //alert(response.data);
                            if (response.status != 200)
                            {
                                var confirm = $mdDialog.confirm()
                                .title('Tote ID: ' + toteID_N)
                                .textContent(response.data)
                                .ariaLabel('Lucky day')
                                .targetEvent(' HALALA ')
                                .ok('Ok');

                                $mdDialog.show(confirm);
                            } else if (response.data.message == 'Empty') {
                                //alert(response.data);

                               // var myData
                                $sessionStorage.New_Tote = toteID_N;
                                //
                                /*
                                $sessionStorage.New_Tote = toteID_N; // new tote
                                $sessionStorage.orderN; order
                                $sessionStorage.picklist_Av.toteID; // old tote
                                */
                                $scope.confOrder = $sessionStorage.orderN;
                                //Confirm the order 1st

                                $scope.un_NewTote = false;
                                $scope.unconsolidateDisplay = false;
                                $scope.un_scanOrder = true;

                            // $scope.ScanToUnconsolidate();
                            }
                        });
                    } else {
                     var confirm = $mdDialog.confirm()
                    .textContent('Enter toteID first')
                    .ariaLabel('Lucky day')
                    .targetEvent(' HALALA ')
                    .ok('Ok');

                    $mdDialog.show(confirm);

                    }

                } else {

                    var confirm = $mdDialog.confirm()
                    .title('Tote ID: ' + $sessionStorage.N_tote)
                    .textContent('Is similar to the first tote entered, Please enter a different tote')
                    .ariaLabel('Lucky day')
                    .targetEvent(' HALALA ')
                    .ok('Ok');

                    $mdDialog.show(confirm);
                }
                return;
            }

            $scope.ScanToUnconsolidate = function()
            {
                var UnconsolidateData = {
                    oldTote: $sessionStorage.picklist_Av.toteID,
                    newTote: $sessionStorage.New_Tote,
                    orderID: $sessionStorage.orderN
                }

                $scope.UnconsolidateDone(UnconsolidateData);
            }
            //These are fine!!!!! ....

            $scope.UnconsolidateDone = function(Data)
            {
                CheckToteStateService.Unconsolidate(JSON.stringify(Data))
                .then(function (response) {
                    if (response.status != 200)
                    {

                        var confirm = $mdDialog.confirm()
                        .title('OrderID: ' + response.data.order)
                        .textContent( response.data.message)
                        .ariaLabel('Lucky day')
                        .targetEvent(' HALALA ')
                        .ok('Ok');
                        $mdDialog.show(confirm);

                    } else {
                    $scope.DisplayUnconsolidated();
                    }
                });

                return;
            }

            $scope.DisplayUnconsolidated  = function()
            {
                $scope.un_scanOrder = false;
                $scope.unconsolidateDisplay = true;

                $scope.orderUN ='';

                delete $sessionStorage.orderN;
                delete $sessionStorage.OrderStatus;

                return $scope.Rfresh();
            }

            $scope.setHeaders = function()
            {
                $scope.pickListID = 'PicklistID';
                $scope.OrderID = 'OrderID';
                $scope.Status = 'Status';
               // $scope.User = 'User';

                return;
            }

            $scope.setData = function(value)
            {

                $scope.Clearance();

                let DisplayData  = [];
                let Status1 = [];
                let x = 0;
               // let Data = {};

               // alert(value.orders.length);

                while(x < value.orders.length) {
                        if (value.orders[x].picked == true)
                        {
                            Status1[x] = 'PICKED';
                        } else {
                            Status1[x] = 'STARTED';
                        }
                       //console.log('  ***** _> ' + Status1[x]);
                        let Data = {
                            pickListID: value.orders[x].pickListID,
                            orderID: value.orders[x].orderID,
                            WmsOrderStatus: Status1[x],
                            //unconsolidatedBy: value.orders[x].unconsolidatedBy
                        }
                        DisplayData.push(Data);
                        x++;
                }

                $scope.picklists = DisplayData;
                $scope.FullList = value;

                return;
            }


            $scope.Rfresh = function()
            {
                if ($window.sessionStorage.getItem(loggedInUser + "_Tote") != undefined && $window.sessionStorage.getItem(loggedInUser +"_count") =='1')
                {
                    $scope.unconsolidateStart = false;
                    $scope.unconsolidateDisplay = false;
                    $scope.un_NewTote = false;
                    $scope.un_scanOrder = false;
                    $scope.un_confirmOrder = false;
                    $scope.un_confirmTote = false;
                    $scope.unconsolidateDone = false;

                    $scope.ClearInput('orderUN');
                    $scope.Clearance();
                    $scope.GetValidatedPickList();
                } else {
                    $scope.ClearInput('orderUN');
                    return $scope.Start();
                }
            }

            $scope.Clearance = function()
            {
                    $scope.ToteID = '';
                    $scope.picklists = '';
                    $scope.New_ToteID = '';
                    $scope.Order_ID = '';
                    $scope.confirm_order = '';
                    $scope.confirmToteID = '';
                    $scope.picklists1 = '';
                    $scope.Order_ID = '';

                    return;
            }

            $scope.Rfresh();

        } else {

            var confirm = $mdDialog.confirm()
            .title(' ')
            .textContent('Please log in to the system first')
            .ariaLabel('Lucky day')
            .targetEvent(' HALALA ')
            .ok('Ok');

            $mdDialog.show(confirm);
            $location.path('/');
        }
    });

});
