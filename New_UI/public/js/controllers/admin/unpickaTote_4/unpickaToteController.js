let ind = 0;
let pArr = [];
let ndx = 0;
let selectedPicks = [];

app.controller('unpickaTote_4', function ($scope, $rootScope, $window, $sessionStorage, $location, $http, $mdDialog, $filter, unpickToteService, cancelOrderService, GetPickListService ) {

    $http.get('/getLoggedInUser').then(function(response) {
       // $window.sessionStorage.setItem(loggedInUser + "_index", pArr)
        $window.sessionStorage.removeItem(loggedInUser + "_index");

        if (response.data.code == '00') {
            var loggedInUser = response.data.username;

           $scope.GoBack = function()
           {
              // $sessionStorage.user.empty();
             delete $sessionStorage;
             $window.sessionStorage.removeItem(loggedInUser + "_Tote");
             $window.sessionStorage.removeItem(loggedInUser +"_count");
              //delete $sessionStorage

               $scope.DeleteSessions();

               $location.path('/toteTracking');
               return;
           }
		

	   $scope.GoBack2 = function()
           {
            delete $sessionStorage;
            $window.sessionStorage.removeItem(loggedInUser + "_Tote");
            $window.sessionStorage.removeItem(loggedInUser +"_count");
             //delete $sessionStorage
            $scope.DeleteSessions();
           
            $scope.unpickScan = true;
            $scope.unpickDisplay = false;
            $scope.unpickConfirm = false;
            $scope.unpickCancelled = false; 
            $location.path('/unpickaTote');
           
	    return;   
           }

           $scope.DeleteSessions = function()
           {
            delete $sessionStorage.myTote;
            delete $sessionStorage.PickLists;
            delete $sessionStorage.OrderID;
            delete $sessionStorage.cancelOrder;
            delete $sessionStorage.CancelIDs;
            delete $sessionStorage.Corder;
            delete $sessionStorage.Clists;
            delete $sessionStorage.cancelledData;
            delete $sessionStorage.CancelCount;
            delete $sessionStorage.TOTEmine;
            delete $sessionStorage.Totes;
           }

            $scope.Init = function()
            {
                $scope.unpickScan = true;
                $scope.unpickDisplay = false;
                $scope.unpickConfirm = false;
                $scope.unpickCancelled = false;
            }

            $scope.Init1 = function()
            {
                $scope.unpickScan = true;
                $scope.unpickDisplay = false;
                $scope.unpickConfirm = false;
                $scope.unpickCancelled = false;
                delete $sessionStorage.myTote;
                delete $sessionStorage.PickLists;
                $scope.selection = [];
                $scope.selection1 = [];
            }

            $scope.ClearInput = function(id){
                let Input = document.getElementById(id);
                Input.value = '';
                Input.focus();
            } /* ClearInput */

            $scope.SetPicklist = function(toteVal)
            {
                $scope.ClearInput('ToteID');
               //var  = $scope.ToteID;
               $sessionStorage.myTote = toteVal;
              //alert( ' Tote ' + toteVal);
               if (toteVal !== undefined)
               {
                $window.sessionStorage.setItem(loggedInUser + "_Tote", toteVal);
                $window.sessionStorage.setItem(loggedInUser +"_count", '1');   

                unpickToteService.picklistTemp(toteVal)
               .then(function (response)
               {
                if (response.status != 200)
                {
                    var confirm = $mdDialog.confirm()

                    .title('Tote : ' + response.data.tote)
                    .textContent(response.data.message)
                    .ariaLabel('Lucky day')
                    .targetEvent(' HALALA ')
                    .ok('Ok');

                    $mdDialog.show(confirm);

                    $scope.DeleteSessions();
                    $location.path('/unpickaTote');
                    //alert(response.data); //Validations
                    //return false;
                } else {
                 ////alert( ' In session ' + $localStorage.Tote);
                 let mypicklist = response.data.Data;
                 if ($scope.validatePicklist(mypicklist))
                 {
                    $scope.unpickDisplay = true;
                    $scope.unpickScan = false;
                   // alert('Correct!');

                   $scope.setHeaders();
                   $scope.setData(mypicklist);

                 } else {
                    $location.path('/unpickaTote');
                    return false;
                }
             }
               });
            } else {

                    var confirm = $mdDialog.confirm()
                    .textContent('Enter toteID first ')
                    .ariaLabel('Lucky day')
                    .targetEvent(' HALALA ')
                    .ok('Ok');

                    $mdDialog.show(confirm);
                    return false;
                }

            } /*SetPicklist*/

            $scope.GetPickList = function()
            {   
                var value = $sessionStorage.myTote;
                var mpick;
                console.log(' My value !! ' + value)
                if (!value)
                {
                    $scope.DeleteSessions();
                    $location.path('/unpickaTote');
                    $scope.unpickScan = true;
                } else {
                unpickToteService.picklistTemp(value).then(function (response)
                {
                 if (response.status != 200)
                 {  
                     //alert(response.data.message + "  !!!! ");//

                     var confirm = $mdDialog.confirm()

                     .title('Tote : ' + response.data.tote)
                     .textContent(response.data.message)
                     .ariaLabel('Lucky day')
                     .targetEvent(' HALALA ')
                     .ok('Ok');
 
                     $mdDialog.show(confirm);

                     $scope.DeleteSessions();
                     $location.path('/unpickaTote');
                     $scope.unpickScan = true;
                     //$window.sessionStorage.removeItem(loggedInUser + "_Tote");
                     return;
                 } else {
            
                  let mypicklist = response.data.Data;
                  if ($scope.validatePicklist(mypicklist))
                  {
                    // alert('Success again!! ');

                     $scope.unpickDisplay = true;
                     $scope.unpickScan = false;
                     $scope.unpickConfirm = false;
                     $scope.unpickCancelled = false; 
                     //mpick = mypicklist;
                     
                     $scope.setHeaders();
                     $scope.setData(mypicklist);
                    
                  } else {
                     //alert('Tote ' + value + ' no longer in the system');
                     $location.path('/unpickaTote');
                     $window.sessionStorage.clear();
                  }
                }
                });
            }
            } /*SetPickList Just with a different name */

            $scope.validatePicklist = function(list) {
                if (list[0].orders == undefined)
                 {
                     return false;
                 } else {
                     return true;
                 }
            } /*ValidatePicklist*/

            $scope.setHeaders = function()
            {
                $scope.Unpick = 'Unpick';
                $scope.pickListID = 'PicklistID';
                $scope.orderID = 'OrderID';
                $scope.currStatus = 'Current Status';

                $scope.Unpick1 = 'Unpick';
                $scope.pickListID1 = 'PicklistID';
                $scope.orderID1 = 'OrderID';
                $scope.currStatus1 = 'Current Status';
                $scope.user1 = 'User';
                return;
            }

            $scope.setData = function(value)
            {
                let DisplayData  = [];
                let x = 0;
                while(x < value[0].orders.length){
                    let Data = {
                        pickListID: value[0].orders[x].pickListID,
                        orderID: value[0].orders[x].orderID,
                        Status: value[0].Status
                    }

                    DisplayData.push(Data);
                    x++;
                }
                $scope.picklists = DisplayData;
                $scope.FullList = value[0];
                //alert($scope.FullList.toteID + ' AS TOTE');
                //alert($scope.picklists[0].orderID + ' PL TEST');
                return;
            }

            $scope.CheckSelection = function (picklist) {

                $scope.selection = [];
                $scope.storeOrder = [];

                    //console.log(picklist + ' My PLZ ');
                    if ($sessionStorage.PickLists != undefined)
                    {
                        for (var i = 0; i <  $sessionStorage.PickLists.length; i++)
                        {
                        $scope.selection[i] = $sessionStorage.PickLists[i];
                      //  $scope.storeOrder
                        //console.log(' My picklists ' + $scope.selection[i]);
                        }
                    //   x = $sessionStorage.PickLists.length;
                    } 

                    //Removing duplicates ....
                var unique = new Set($scope.selection);

                var unique1 = Array.from(unique);
                for (var i = 0; i < unique1.length; i++)
                {
                console.log(unique1[i] + ' My  better list');
                }
                //Done removing duplicates ....

                var idx = $scope.selection.indexOf(picklist);

                if (idx > -1) {
                $scope.selection.splice(idx, 1);
                }
                else {
                $scope.selection.push(picklist);
                }

                for (var i = 0; i < $scope.selection.length; i++) {
                console.log(' It now has ' + $scope.selection[i]);
                }


                //alert($scope.picklists[0].orderID + " *** JOHNY BOI 1");
                //alert($scope.picklists[1].orderID + " *** JOHNY BOI 2");
               // alert($scope.picklists[3].orderID + " *** JOHNY BOI 3");
                
                $sessionStorage.OrderID = $scope.picklists[0].orderID;
                $sessionStorage.PickLists = $scope.selection;
                
            }

            $scope.CheckSelection1 = function (picklist) {
                //alert(idx);
    
                $scope.selection1 = picklist;

                var unique = new Set($scope.selection1);

                var unique1 = Array.from(unique);
                for (var i = 0; i < unique1.length; i++)
                {
                console.log(unique1[i] + ' My  better list');
                }
                //Done removing duplicates ....
                var idx = $scope.selection1.indexOf(picklist);
                if (idx > -1) {
                // is currently selected
                //alert( ' Removing ' + picklist);
                $scope.selection1.splice(idx, 1);
                }
                else {
                // is newly selected
                // alert('Adding ' + picklist);
                $scope.selection1.push(picklist);
                // alert($scope.selection[0]);
                }
               /* for (var i = 0; i < $scope.selection1.length; i++) {
               // console.log(' It now has ' + $scope.selection1[i]);
                }*/
                $sessionStorage.OrderID = $scope.picklists[0].orderID;
                $sessionStorage.PickLists = $scope.selection1;
                //$scope.ComfirmScreen($sessionStorage.OrderID, $sessionStorage.PickLists);
            }

            $scope.CancelPicklists = function()
            {
                //console.log($sessionStorage.PickLists + ' STILL ');
               // alert($sessionStorage.myTote + " PRODUCTION URL ");
                $scope.PLForCancel($sessionStorage.PickLists, $sessionStorage.OrderID, $sessionStorage.myTote);
                //Store in a session
                //$sessionStorage.PickLists = selectedPicks;
                /*for (var t = 0; t < pArr.length; t++)
                {
                alert(selectedPicks[t] + ' Chosen ');
                }*/
                //alert('We alive');
            }

            $scope.PLForCancel = function(pickLists, orderId, toteM)
            {
                var Data = {
                    picklists: pickLists,
                    orderID: orderId,
                    toteID: toteM
                }

                //alert(orderId + ' AS ORDER');
                for (var i = 0; i < Data.picklists.length; i++)
                {
                    console.log(Data.picklists[i] + ' SYS Test ');
                }

                GetPickListService.GetPickList(JSON.stringify(Data))
                .then(function (response)
                {
                    if (response.status != 200) {
                    // alert (response.data + ' As a result ');
                    } else {
                        //alert(response.data.orderID + ' As My order ');
                        $sessionStorage.cancelOrder = response.data.orderID;
                        $sessionStorage.CancelIDs = response.data.picklists;
                        $sessionStorage.Totes = response.data.toteID;
                        //console.log(response.data.picklists + ' ***** ');
                        $scope.ComfirmScreen($sessionStorage.cancelOrder, $sessionStorage.CancelIDs, $sessionStorage.Totes);
                    }
                });
                
            }

        //Confirm the order before you cancel it's picklists
            $scope.ComfirmScreen = function(value, picklists, tote)
            {

            $sessionStorage.Corder = value;
           // alert('     TEST ORD  --->  ' + value);
            $sessionStorage.Clists = picklists;
            $sessionStorage.TOTEmine = tote;

                $scope.unpickScan = false;
                $scope.unpickDisplay = false;
                $scope.unpickConfirm = true;
                $scope.unpickCancelled = false; 

                $scope.OrderID = $sessionStorage.Corder;
            }   
            
            $scope.CancelLists = function()
            {
                var cancelData = {
                    orderID : $sessionStorage.Corder,
                    picklistID: $sessionStorage.Clists,
                    cancelledby: loggedInUser,
                    toteID: $sessionStorage.TOTEmine
                }
                
                cancelOrderService.CancelPicks(JSON.stringify(cancelData))
                .then(function (response) {

                    if (response.status != 200)
                    {
                        //alert(response.data);
                    } else {
                        if(!response.data.count){
                            $scope.GoBack2();
                            return;
                        }

                        $sessionStorage.cancelledData = response.data.Data;
                        $sessionStorage.CancelCount = response.data.count;
                        //alert(' Afta RETURN MAN!!! ');
                        $scope.CancelledDisplay($sessionStorage.cancelledData, $sessionStorage.CancelCount);
                    }
                });
            }

            $scope.CancelledDisplay = function(data, count)
            {
                /*$scope.unpickCancelled = true;
                $scope.unpickScan = false;
                $scope.unpickDisplay = false;
                $scope.unpickConfirm = false;
                */
                //$scope.setHeaders();
                $scope.picklists1 = data.orders;
                $scope.Testing = data;

                // console.log(lists +  ' LLL ');
                for (var i = 0; i < $scope.picklists1.length; i++)
                {
                 //   alert($scope.Testing.orders[i].pickListID + '<-     Pl Id  && TOTE ->    ' + $scope.Testing.toteID);
                    console.log($scope.picklists1[i] + ' With DATA ');
                }

                //console.log($scope.picklists1[0].pickListID + ' With DATA 2 ');

               /* var cancelledlists = [];
                
                var j = 0;
               // var k = 0;
                for (var i = 0; i < $scope.picklists1.length; i++)
                {
                    console.log(lists[0] + ' My Jacket');
                    for (var k = 0; k < lists.picklistID.length; k++)
                    {   //My huge validation ...
                        if ($scope.picklists1[i].pickListID == lists.picklistID[k]) {
                            cancelledlists[j] = $scope.picklists1[i].pickListID;  
                            console.log(cancelledlists[j]);
                            j++;
                        }
                    }
                }*/
                return $scope.GetPickList();
            }

            /////////////////////////////////////////Here
            $scope.Rfresh = function()
            {
                if ($window.sessionStorage.getItem(loggedInUser + "_Tote") != undefined && $window.sessionStorage.getItem(loggedInUser +"_count") =='1')
                {
                    //alert("After Refresh Ntanga! -> " + $window.sessionStorage.getItem(loggedInUser + "_Tote"));
                    var toteID = $window.sessionStorage.getItem(loggedInUser + "_Tote"); 
                    //var plreturn;
                    $scope.selection = $sessionStorage.PickLists;
                    $scope.GetPickList();
                } else {
                    $scope.Init();
                }
            }

            $scope.Rfresh();

        } else {
            //alert('Please log in first');
            
            var confirm = $mdDialog.confirm()
            .textContent('Please log in first')
            .ariaLabel('Lucky day')
            .targetEvent(' HALALA ')
            .ok('Ok');

            $mdDialog.show(confirm);

            $location.path('/');
        }
    });

});
