/*
 * @Author: Clive Matlawa
 * @Date: 2017-09-12
 */

app.controller('LoginController', function ($scope, $rootScope, $location, $cookies, $mdDialog, $http, loginService)
{
    $rootScope.loggedInUser = ''
    $rootScope.loggedInIP = ''
	$rootScope.loggedInStation = ''
	$rootScope.notification = ''

    let login = document.getElementById('_login')
    let logout = document.getElementById('_logout')
    let navItms = document.getElementById('nav-items')

    $scope.logIn = function (username, password, path)
    {
			login.style.display = 'none'
			logout.style.display = ''
			navItms.style.display = ''
			$location.path(path)
    } /* logIn */

    $scope.logOut = function (path)
    {
			login.style.display = ''
			logout.style.display = 'none'
			$cookies.clear()
			$location.path(path)
    } /* logOut */

    $scope.logInStation = function ()
    {
			if($scope.userName === undefined || $scope.userPassword === undefined)
			{
				var confirm = $mdDialog.alert()
				.title('Invalid Username and Password Combination.').ok('Ok');
				$mdDialog.show(confirm).then(function() {}, function() {});
			}
			else
			{
				$scope.logInUser()
			}
    } /* logInStation */

    $scope.logInUser = function ()
    {
			var data =
			{
				userName: $scope.userName,
				userPassword: $scope.userPassword
			}

			loginService.userloginTemp(JSON.stringify(data))
			.then(function (response)
			{
				if(response.status == 200)
				{
					var Data = response.data;

					loggedInUser.innerHTML = '<i class="fa fa-user fa-fw"></i>' + Data.UserID

					$scope.getNotification()

					loginService.getStationDetail()

					.then(function (response)
					{
						if(response.status == 200)
						{
							var StationData = response.data;

							if(StationData.NoStation)
							{
								$location.path('/toteTracking')
							}
							else
							{
								$location.path('/manifesting')
							}
						}
						else
						{
							$location.path('/toteTracking')
						}
					});
				}
				else
				{
					var confirm = $mdDialog.alert()
					.title(response.data).ok('Ok');
					$mdDialog.show(confirm).then(function() {}, function() {});
				}
			});
	} /* logInUser */
	
	$scope.getNotification = function(){
		$http.post('/getNotifications').then(function(response){
			if(response){
				console.log('___________________________________ | ' + response.data.total + ' |________________________________')
				$rootScope.notification = response.data.total
			}
		})
	}
})
