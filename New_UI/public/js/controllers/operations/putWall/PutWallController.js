/*
 * @Author: Deon Wolmarans
 * @Date: 2017-07-16 14:08:48
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-07-25 16:38:52
 */

/**
* @controller PutWallController
* @function getToteId - this will get the tote id from Redis, and compare to the scanned id.
* @scope toteContent - this will get all content from the picklist, that is supposed to be in the tote.
*/

app.controller('PutWallController', function ($scope, $rootScope, $location, PickListService, PackListService, CartonService, TransporterService) {
    var currentOrderPack = {}

    $scope.signedInStation = $rootScope.loggedInStation
    $scope.scanningMode = false
    $scope.scannedTote = ''
    $scope.pulWallShelfPlacing = ''
    $scope.scannedItemDesc = ''
    $scope.scannedItemSerial = ''
    $scope.currentToteInProgress = ''
    $scope.currentOrderId = ''

    $scope.getToteId = function (toteId) {
        let scannedTote = document.getElementById('scannedTote')

        // clearing previously scanned items, if neccessary
        // $scope.currentToteInProgess = ''
        // $scope.orderItems = []
        // $scope.itemsInOrder = []
        // $scope.itemsToDisplay = []
        // $scope.scannedSerialItems = []
        // currentOrderInProcess = 0
        // $scope.qtyToPack = 0
        // $scope.qtyPacked = 0

        // get the tote lock properties
        // if locked, by who, and current signed in user
        var currentUserIp = $rootScope.loggedInIP

        $scope.currentToteInProgress = toteId
        PickListService.getPickList(toteId)
        .then(function (doc) {
            if (doc.statusText === 'OK') {
                // check if tote is valid
                // check if tote is for the bagging process
                // check if tote has completed picking

                if (doc.data !== '') {
                    $scope.scannedTote = doc.data
                    let toteData = doc.data
                    let totePackType = toteData.packType
                    if (totePackType === 'PUT') {
                        let toteInUse = toteData.toteInUse
                        let toteInUseBy = toteData.toteInUseBy
                        if (toteInUse === false || toteInUse === true && toteInUseBy === currentUserIp) {
                            // check to see if all orders and items are picked
                            var orderPicked = true
                            var itemPicked = true

                            let toteOrders = toteData.orders
                            for (var i = 0; i < toteOrders.length; i++) {
                                let currentToteOrder = toteOrders[i]
                                let currentToteOrderId = currentToteOrder.orderID
                                let currentToteOrderPicked = currentToteOrder.picked
                                if (currentToteOrderPicked === true) {
                                    let orderItems = currentToteOrder.items
                                    for (var j = 0; j < orderItems.length; j++) {
                                        let currentItem = orderItems[j]
                                        let currentItemPicked = currentItem.picked
                                        if (currentItemPicked === false) {
                                            itemPicked = false
                                            window.alert('Items in order ' + currentToteOrderId + ' has not completed picking.')
                                            break
                                        }
                                    }
                                } else {
                                    orderPicked = false
                                    window.alert('Order ' + currentToteOrderId + ' has not completed picking.')
                                    break
                                }
                            }
                            if (orderPicked === true && itemPicked === true) {
                                // if tote is not in use, it should be locked
                                if (toteInUse === false) {
                                    $scope.inUse = true
                                    $scope.inUseBy = currentUserIp

                                    // edit picklist to lock tote
                                    let pickListToUpdateInMongo = $scope.pickList
                                    pickListToUpdateInMongo.toteInUse = $scope.inUse
                                    pickListToUpdateInMongo.toteInUseBy = $scope.inUseBy
                                    PickListService.editPickList(pickListToUpdateInMongo)
                                }

                                // all orders and items has been picked.
                                // proceed to scanning screen
                                $scope.currentToteInProgess = toteId
                                scannedTote.value = ''
                                $scope.scanningMode = true
                                $scope.scanningPutWallFormUrl = '/operations/putWall/putWallScanning.html'
                            }
                        } else {
                            scannedTote.value = ''
                            scannedTote.focus()
                            window.alert('Tote currently in use at another station')
                        }
                    } else {
                        scannedTote.value = ''
                        scannedTote.focus()
                        window.alert('Tote cannot be handled here. Take tote to Manifesting station.')
                    }
                } else {
                    // tote not in the system
                    scannedTote.value = ''
                    scannedTote.focus()
                    window.alert('Tote not currently being handled in the system.')
                }
            }
        }, function (error) {
            window.alert(error)
        })
    }

    $scope.getScannedItem = function (scannedItem) {
        // barcode/sku/serial can be scanned
        // check if serialised or not
        // if serialised, validate against db
        // if not, just add count


        // if more items are to be scanned, go back to scan barcode screen
        // if all items are scanned, proceed to print documentation


        // find the scanned item in the current picklist
        // check if the item has already been scanned
        // if not, update qtyPacked
        // if all qtyPacked, then update order to packed
        // if order is packed, then print documentation

        PickListService.getSerials(scannedItem)
        .then(function (responseData) {
            let dbResult = responseData.data
            let serial = dbResult.serial
            let sku = dbResult.SKU

            // get the input of scan item
            let inputScanInput = document.getElementById('itemScan')

            // compare scanned item against all items` barcode and sku
            // if item in order is serialised, compare sku received against item sku

            let toteOrders = $scope.scannedTote.orders
            for (var k = 0; k < toteOrders.length; k++) {
                let itemProcessed = false

                let currentOrder = toteOrders[k]
                let currentOrderPacked = currentOrder.packed
                if (currentOrderPacked === false) {
                    let currentOrderItems = currentOrder.items
                    for (var m = 0; m < currentOrderItems.length; m++) {
                        var itemsInOrder = currentOrderItems.length
                        let currentItem = currentOrderItems[m]
                        let currentItemQtyPicked = currentItem.pickQty
                        let currentItemSku = currentItem.sku
                        let currentItemBarcode = currentItem.barcode
                        let currentItemSerialised = currentItem.serialised
                        let currentItemCode = currentItem.itemCode

                        if (currentItemSerialised === true) {
                            if (sku === currentItemSku && serial === scannedItem) {
                                // update qtyPacked with 1
                                currentItem.qtyPacked += 1
                                itemProcessed = true
                                $scope.scannedItemDesc = currentItemCode
                                $scope.scannedItemSerial = scannedItem

                                // if qtyPacked matches qtyPicked, update item to 'packed'
                                if (currentItem.qtyPacked === currentItemQtyPicked) {
                                    currentItem.packed = true
                                }
                                break
                            }
                        } else {
                            if (scannedItem === currentItemSku || scannedItem === currentItemBarcode) {
                                // update qtyPacked with 1
                                currentItem.qtyPacked += 1
                                itemProcessed = true
                                $scope.scannedItemDesc = currentItemCode
                                $scope.scannedItemSerial = scannedItem

                                // if qtyPacked matches qtyPicked, update order to 'packed'
                                if (currentItem.qtyPacked === currentItemQtyPicked) {
                                    currentItem.packed = true
                                }
                                break
                            }
                        }
                        if (itemProcessed === false && itemsInOrder === m) {
                            inputScanInput.value = ''
                            inputScanInput.focus()
                            window.alert('Scanned item not part of this tote')
                            break
                        }
                    }
                }
                if (itemProcessed === true) {
                    // view the item details
                    $scope.pulWallShelfPlacing = '12' // set the shelf location where item should go
                    document.getElementById('itemScan').value = ''
                    document.getElementById('displayItemText').style.display = ''
                    document.getElementById('displaySerialText').style.display = ''
                    document.getElementById('scanItemScreen').style.display = 'none'
                    document.getElementById('putWallPlacingText').style.display = 'block'
                    break
                }
            }

            // light up the light. wait for response to get back, that button has been pressed
            // if button pressed, and repsonse is true...

            var responseButtonPressed = true
            if (responseButtonPressed) {
                // update the item in the db

                // if all items qty picked and qtypacked matches, update the order as well
                for (var l = 0; l < $scope.scannedTote.orders; l++) {
                    let currentOrder = $scope.scannedTote.orders[l]
                    if (currentOrder.packed === false) {
                        // allOrdersPacked = false
                        let itemsPacked = true
                        for (var n = 0; n < currentOrder.items.length; n++) {
                            let currentItem = currentOrder.items[n]
                            if (currentItem.packed === false) {
                                itemsPacked = false
                                break
                            }
                        }
                        if (itemsPacked === true) {
                            $scope.currentOrderId = currentOrder.orderID
                            currentOrder.packed = true

                            // update order in picklist
                            PickListService.editPickList($scope.scannedTote)

                            // print order documentation
                            // insert print method here

                            // proceed to scan documentation
                            let scanDocumentScreen = document.getElementById('scanDocumentScreen')
                            scanDocumentScreen.style.display = ''

                            // hide some elements
                            let putWallPlacingText = document.getElementById('putWallPlacingText')
                            putWallPlacingText.style.display = 'none'

                            let displayItemText = document.getElementById('displayItemText')
                            displayItemText.style.display = 'none'

                            let displaySerialText = document.getElementById('displaySerialText')
                            displaySerialText.style.display = 'none'

                            break
                        } else { // there is still items that needs picking
                            // go to scan item screen
                            PickListService.editPickList($scope.scannedTote)

                            // hide some elements
                            let putWallPlacingText = document.getElementById('putWallPlacingText')
                            putWallPlacingText.style.display = 'none'

                            let displayItemText = document.getElementById('displayItemText')
                            displayItemText.style.display = 'none'

                            let displaySerialText = document.getElementById('displaySerialText')
                            displaySerialText.style.display = 'none'

                            // show scan item
                            let scanItemScreen = document.getElementById('scanItemScreen')
                            scanItemScreen.style.display = ''
                        }
                    }
                }
            }
        }, function (error) {
            window.alert(error)
        })
    }

    $scope.getScannedDocument = function (scannedDocument) {
        // get elements
        let documentInput = document.getElementById('documentScan')
        let displayOrderIdText = document.getElementById('displayOrderIdText')
        let documentPlacingText = document.getElementById('documentPlacingText')
        let scanDocumentScreen = document.getElementById('scanDocumentScreen')
        let diplayToteText = document.getElementById('diplayToteText')

        if ($scope.currentOrderId === scannedDocument) {
            documentInput.value = ''
            displayOrderIdText.style.display = ''
            documentPlacingText.style.display = ''
            scanDocumentScreen.style.display = 'none'

            // once got the response back from light, check if there are more orders to process.
            // if so, go to scan item screen, else go to scan tote screen
            let responseBtnPressed = true
            if (responseBtnPressed === true) {
                let allOrdersPackComplete = true
                for (var p = 0; p < $scope.scannedTote.orders.length; p++) {
                    let currentOrder = $scope.scannedTote.orders[p]
                    let packed = currentOrder.packed
                    if (packed === false) {
                        allOrdersPackComplete = false
                    }
                }
                if (allOrdersPackComplete === true) { // all orders have been packed. Proceed to scan tote
                    diplayToteText.style.display = 'none'
                    displayOrderIdText.style.display = 'none'
                    documentPlacingText.style.display = 'none'

                    $location.path('/putWall/')
                }
            }

            // create the packlist
            $scope.orderPackList = {}
            $scope.orderPackList.orderID = $scope.currentOrder_orderId
            $scope.orderPackList.cartonID = $scope.currentOrder_cartonId
            $scope.orderPackList.courierID = $scope.currentOrder_courierId
            $scope.orderPackList.packingWeight = $scope.totalWeight
            $scope.orderPackList.rejected = false
            $scope.orderPackList.complete = false
            $scope.orderPackList.weight = $scope.totalWeight
            $scope.orderPackList.items = $scope.currentOrder_items

            currentOrderPack.cartonId = ''
            currentOrderPack.carton = $scope.orderPackList
            currentOrderPack.transporterId = ''
            currentOrderPack.cartons = []

            PackListService.createPackList(currentOrderPack)
        } else {
            documentInput.value = ''
            documentInput.focus()
            window.alert('Scanned document is not valid for this order.')
            window.alert('Please scan correct document for order: ' + $scope.currentOrderId)
        }
    }

    $scope.resetTote = function () {
        // reset scope values
        $scope.scanningMode = false
        $scope.pulWallShelfPlacing = ''
        $scope.scannedItemDesc = ''
        $scope.scannedItemSerial = ''
        $scope.currentToteInProgress = ''

        // clear current order
        // update db
        // direct back to scan tote

        for (var q = 0; q < $scope.scannedTote.orders; q++) {
            let currentOrder = $scope.scannedTote.orders[q]
            if (currentOrder.orderID === $scope.currentOrderId) {
                currentOrder.packed = false
                // loop through items, and set to default values
                for (var r = 0; r < currentOrder.items; r++) {
                    let currentItem = currentOrder.items[r]
                    currentItem.qtyPacked = 0
                    currentItem.packed = false
                    currentItem.serials = []
                }
            }
        }

        $scope.scannedTote.toteInUse = false
        $scope.scannedTote.toteInUseBy = ''

        PickListService.editPickList($scope.scannedTote)

        $location.path('/putWall/')
    }
})
