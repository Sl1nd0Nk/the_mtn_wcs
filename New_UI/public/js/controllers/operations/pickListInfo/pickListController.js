app.controller('pickListController', function ($scope, $location, $rootScope, pickListService, toteInfoService, orderInfoService, loginService) {
    loginService.getStationDetail($rootScope.loggedInIP)
     .then(function (station) {
         if (station) {
            $scope.header = 'PICK LIST INFORMATION'
            $scope.pickListArr = []
            $scope.pickLists = []
            $scope.statusTable = {
                header: 'Status Count',
                newHeader: 'NEW',
                cancelledHeader: 'CANCELLED',
                startedHeader: 'STARTED',
                pickedHeader: 'PICKED',
                packedHeader: 'PACKED'
            }
            pickListService.getPickListInfo()
             .then(function (doc) {
                 var intArr = []

				 $scope.statusNew = doc.data.data.statusNew
				 $scope.statusStarted = doc.data.data.statusStarted
				 $scope.statusPicked = doc.data.data.statusPicked
				 $scope.statusStored = doc.data.data.statusStored
				 $scope.statusPacked = doc.data.data.statusPacked
				 $scope.statusShipped = doc.data.data.statusShipped
				 $scope.statusCancelled = doc.data.data.statusCancelled

				 $scope.statusNewAdd = doc.data.data.statusNewAdd
				 $scope.statusStartedAdd = doc.data.data.statusStartedAdd
				 $scope.statusPickedAdd = doc.data.data.statusPickedAdd
				 $scope.statusStoredAdd = doc.data.data.statusStoredAdd
				 $scope.statusPackedAdd = doc.data.data.statusPackedAdd
				 $scope.statusShippedAdd = doc.data.data.statusShippedAdd
				 $scope.statusCancelledAdd = doc.data.data.statusCancelledAdd

				 $scope.pickListsArr = doc.data.data;
				 $scope.pickLists = doc.data.PicksNew;
				 let View = document.getElementById('NewOrders');
				 View.style.display = '';
				 let View1 = document.getElementById('TheRest');
				 View1.style.display = 'none';
				 let View2 = document.getElementById('PickedOrders');
				 View2.style.display = 'none';
				 let View3 = document.getElementById('PackedOrders');
				 View3.style.display = 'none';
				 let View4 = document.getElementById('StartedOrders');
				 View4.style.display = 'none';
				 let View5 = document.getElementById('CancelledOrders');
				 View5.style.display = 'none';

                 $scope.newSubmit = function () {
					 let View = document.getElementById('NewOrders');
					 View.style.display = '';
					 let View1 = document.getElementById('TheRest');
					 View1.style.display = 'none';
					 let View2 = document.getElementById('PickedOrders');
					 View2.style.display = 'none';
					 let View3 = document.getElementById('PackedOrders');
					 View3.style.display = 'none';
					 let View4 = document.getElementById('StartedOrders');
					 View4.style.display = 'none';
					 let View5 = document.getElementById('CancelledOrders');
					 View5.style.display = 'none';
					 $scope.pickLists = doc.data.PicksNew;
                 }

                 $scope.startedSubmit = function () {
					 let View = document.getElementById('NewOrders');
					 View.style.display = 'none';
					 let View1 = document.getElementById('TheRest');
					 View1.style.display = 'none';
					 let View2 = document.getElementById('PickedOrders');
					 View2.style.display = 'none';
					 let View3 = document.getElementById('PackedOrders');
					 View3.style.display = 'none';
					 let View4 = document.getElementById('StartedOrders');
					 View4.style.display = '';
					 let View5 = document.getElementById('CancelledOrders');
					 View5.style.display = 'none';
					 $scope.pickLists = doc.data.PicksStarted;
                 }

                 $scope.pickedSubmit = function () {
					 let View = document.getElementById('NewOrders');
					 View.style.display = 'none';
					 let View1 = document.getElementById('TheRest');
					 View1.style.display = 'none';
					 let View2 = document.getElementById('PickedOrders');
					 View2.style.display = '';
					 let View3 = document.getElementById('PackedOrders');
					 View3.style.display = 'none';
					 let View4 = document.getElementById('StartedOrders');
					 View4.style.display = 'none';
					 let View5 = document.getElementById('CancelledOrders');
					 View5.style.display = 'none';
					 $scope.pickLists = doc.data.PicksPicked;
                 }

                 $scope.packedSubmit = function () {
					 let View = document.getElementById('NewOrders');
					 View.style.display = 'none';
					 let View1 = document.getElementById('TheRest');
					 View1.style.display = 'none';
					 let View2 = document.getElementById('PickedOrders');
					 View2.style.display = 'none';
					 let View3 = document.getElementById('PackedOrders');
					 View3.style.display = '';
					 let View4 = document.getElementById('StartedOrders');
					 View4.style.display = 'none';
					 let View5 = document.getElementById('CancelledOrders');
					 View5.style.display = 'none';
					 $scope.pickLists = doc.data.PicksPacked;
                 }

                 $scope.storedSubmit = function () {
					 let View = document.getElementById('NewOrders');
					 View.style.display = 'none';
					 let View1 = document.getElementById('TheRest');
					 View1.style.display = '';
					 let View2 = document.getElementById('PickedOrders');
					 View2.style.display = 'none';
					 let View3 = document.getElementById('PackedOrders');
					 View3.style.display = 'none';
					 let View4 = document.getElementById('StartedOrders');
					 View4.style.display = 'none';
					 let View5 = document.getElementById('CancelledOrders');
					 View5.style.display = 'none';
                     $scope.pickLists = doc.data.PicksStored;
                 }

                 $scope.shippedSubmit = function () {
					 let View = document.getElementById('NewOrders');
					 View.style.display = 'none';
					 let View1 = document.getElementById('TheRest');
					 View1.style.display = 'none';
					 let View2 = document.getElementById('PickedOrders');
					 View2.style.display = 'none';
					 let View3 = document.getElementById('PackedOrders');
					 View3.style.display = '';
					 let View4 = document.getElementById('StartedOrders');
					 View4.style.display = 'none';
					 let View5 = document.getElementById('CancelledOrders');
					 View5.style.display = 'none';
                     $scope.pickLists = doc.data.PicksShipped;
                 }

                 $scope.cancelledSubmit = function () {
					 let View = document.getElementById('NewOrders');
					 View.style.display = 'none';
					 let View1 = document.getElementById('TheRest');
					 View1.style.display = 'none';
					 let View2 = document.getElementById('PickedOrders');
					 View2.style.display = 'none';
					 let View3 = document.getElementById('PackedOrders');
					 View3.style.display = 'none';
					 let View4 = document.getElementById('StartedOrders');
					 View4.style.display = 'none';
					 let View5 = document.getElementById('CancelledOrders');
					 View5.style.display = '';
                     $scope.pickLists = doc.data.PicksCancelled;
                 }
             })

            $scope.getOrderID = function (orderID) {
                var arr = []
                pickListService.getListByOrder(orderID)
                 .then(function (doc) {
                     // console.log('_____________________ISSA DOC: ' + JSON.stringify(doc.data.data))
                     var x = 0
                     while (x < doc.data.data.length) {
                         arr.push(doc.data.data[x])
                         x++
                     }
                     // $scope.pickLists = doc.data.data
                     $scope.pickLists = arr
                 })
            }

            $scope.submit = function (value) {
                toteInfoService.setToteInfo(value)
                $location.path('/toteInfo')
            }

            $scope.submit1 = function (value) {
                orderInfoService.setOrderInfo(value)
                $location.path('/orderInfo')
            }
         } else {
            $location.path('/')
         }
     })
})
