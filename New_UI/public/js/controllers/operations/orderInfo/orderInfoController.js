app.controller('orderInfoController', function ($scope, $location, $rootScope, orderInfoService, toteInfoService, loginService, $mdDialog){
    loginService.getStationDetail($rootScope.loggedInIP).then(function (station){
         if(station){
			orderInfoService.getOrderByID().then(function (docs) {
				if(docs.data.data.length > 0){
					$scope.orderList = docs.data.data;
					$scope.MyOrder = docs.data.data[0].OrderID;
					$scope.MyOrderType = docs.data.data[0].OrderType;
					$scope.MyOrderCourier = docs.data.data[0].Courier;
				}
			});

            $scope.submit = function(){
                console.log('Does the submit button work?');
                let Input = document.getElementById('scannedTote');
				Input.value = '';
				Input.focus();
                orderInfoService.getOrder($scope.orderID).then(function (docs){
					if(docs.data.data == 'Order Not Found'){
						let confirm = $mdDialog.alert().title('Scanned Order ' + $scope.orderID + ' is not found').ok('Ok');
						$mdDialog.show(confirm).then(function() {});
					} else {
						$scope.orderList = docs.data.data;
						$scope.MyOrder = docs.data.data[0].OrderID;
						$scope.MyOrderType = docs.data.data[0].OrderType;
						$scope.MyOrderCourier = docs.data.data[0].Courier;
					}
                });
            }

            $scope.submitOrder = function(toteID){
                toteInfoService.setToteInfo(toteID);
                $location.path('/toteInfo');
            }
         } else {
            $location.path('/');
         }

		 $scope.showItems = function (items, PickListID)
		 {
			 let orderContent = document.getElementById('orderContent');
			 orderContent.style.display = '';
			 $scope.pkListID = PickListID;
			 $scope.LineItems = items;
		 } /* showItems */

		 $scope.showSerials = function (Serials, sku, orderLine, pickListOrderLine, itemCode, qty)
		 {
			 let orderHeader = document.getElementById('orderHeader');
			 orderHeader.style.display = 'none';
			 let viewSerialsModal = document.getElementById('viewSerialsModal');
			 viewSerialsModal.style.display = '';

			 $scope.Serials = Serials;
			 $scope.sku = sku;
			 $scope.orderLine = orderLine;
			 $scope.pickListOrderLine = pickListOrderLine;
			 $scope.itemCode = itemCode;
			 $scope.qty = qty;
		 } /* showSerials */

		 $scope.closeViewSerialsModal = function ()
		 {
			 $scope.Serials = null;
			 $scope.sku = null;
			 $scope.orderLine = null;
			 $scope.pickListOrderLine = null;
			 $scope.itemCode = null;

			 let orderHeader = document.getElementById('orderHeader');
			 orderHeader.style.display = '';
			 let viewSerialsModal = document.getElementById('viewSerialsModal');
			 viewSerialsModal.style.display = 'none';

		 } /* closeViewSerialsModal */
     });
});
