app.controller('courierRejectController', function ($scope, $location, $mdDialog, $routeParams, loginService, PickListService, $http) {
	$scope.CheckUser = function(){
		loginService.getLoggedInUser().then(function(response){
			if(response.status == 200){
				let UserData = response.data;

				if($scope.SetViewState(UserData, true)){
					$scope.UpdateLoggedInUser(UserData.user);
				}
			}
		});
	} /* CheckUser */

	$scope.SetViewState = function(Data, Init){
		if(Init)
			$scope.InitPage();

		if(Data.NoUser){
			$location.path('/');
			return false;
		}

		return true;
	} /* SetViewState */

	$scope.UpdateLoggedInUser = function(user){
		loggedInUser.innerHTML = '<i class="fa fa-user fa-fw"></i>' + user;
	} /* UpdateLoggedInUser */

	$scope.InitPage = function(){
		$scope.ContainerScanned = false;
		$scope.ContainerHistory = false;
	} /* InitPage */

	$scope.ClearInput = function(id){
		let Input = document.getElementById(id);
		Input.value = '';
		Input.focus();
	} /* ClearInput */

	$scope.getMasterId = function(ContainerId){
		$scope.ClearInput('scanContainer');

		PickListService.getCourierContainer(ContainerId).then(function(doc){
			if(doc.status != 200){
				window.alert(doc.Err);
			}

			if($scope.SetViewState(doc.data, false)){
				$scope.InitPage();

				let Err = doc.data.Err;
				if(Err){
					return window.alert(Err);
				}

				let Data = doc.data;
				$scope.ContainerScanned = true;
				$scope.cartonScanned = Data.Container;
				$scope.ErrorMsg = Data.CurrentError;

				if(Data.ShowHistory){
					$scope.ContainerHistory = true;
					$scope.listOfScannedItems = Data.History;
				}
			}
		});
	} /* getToteId */

	$scope.CheckUser();
});