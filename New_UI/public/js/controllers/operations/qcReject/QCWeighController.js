/*
 * @Author: Ntokozo Majozi
 * @Date: 2017-04-06 10:26:29
 */

app.controller('QCWeighController', function ($scope, $location, RejectService, $mdDialog, $routeParams) {
    let scanCarton = document.getElementById('scanCarton')
    scanCarton.focus()

    $scope.getCartonID = function (cartonID) {
        if (cartonID.substr(0,1) === '7') {
            var confirm = $mdDialog.alert()
            .title('Please scan carton ID, not the transporter.')
            .textContent('Invalid Carton ID')
            .ariaLabel('Lucky day')
            .targetEvent(cartonID)
            .ok('Ok')

            $mdDialog.show(confirm).then(function() {
                scanCarton.focus()
                $scope.cartonID = ''
            }, function() {
                
            });
        } else {
            var cID = ''
            if(cartonID.substr(0,1) == 'N'){cID = cartonID.substr(3)}else{cID=cartonID}
            RejectService.setWeightCartonID(cID, $routeParams.ToDo)
            var bothJson = {
                transporterID :$routeParams.ToDo,
                cartonID: cID
            }
            var strCartons = JSON.stringify(bothJson)
            $location.path('/qcWeighed/' + strCartons)
        }
    }
})

