/*
 * @Author: Deon Wolmarans
 * @Date: 2017-04-06 10:26:29
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-04-25 07:23:59
 */

/**
* @controller QCRejectController
* @function getData - gets all rejected cartons from redis.
* @function showDetail - show the details of the carton that was rejected.
*/

app.controller('QCRejectsListController', function ($scope, $location, PickListFactory) {
    $scope.rejectTotes = 0

    $scope.picklists = {
        data: [],
        sort: {
            predicate: 'companyName',
            direction: 'asc',
            urlSync: true
        }
    }

    PickListFactory.getData()
    .then(function (responseData) {
        $scope.picklists.data = responseData.data.data
        $scope.totalPicklists = $scope.picklists.data.length

        for (var i = 0; i < $scope.picklists.data.length; i++) {
            // check each picklist, check the status of each, to see if rejected.
            let picklist = $scope.picklists.data
            let rejected = picklist[i].rejected
            let rejectReason = picklist[i].rejectedReason

            if (rejected === true) {
                $scope.rejectTotes += 1
                $scope.rejectReason = rejectReason
            }
        }
    })

    $scope.showDetail = function (picklistItem) {
        // check if active already

        let itemClassActive = document.getElementById('picklist-' + picklistItem).classList
        let hideDetail = document.getElementById('detailHide-' + picklistItem)
        let showDetail = document.getElementById('detail-' + picklistItem)
        let btnIcon = document.getElementById('btn-' + picklistItem).classList

        if (itemClassActive.contains('itemClassActive') && btnIcon.contains('glyphicon-chevron-up')) {
            if (hideDetail && showDetail) {
                hideDetail.style.display = ''
                showDetail.style.display = 'none'
            }

            itemClassActive.remove('itemClassActive')
            btnIcon.remove('glyphicon-chevron-up')
            btnIcon.add('glyphicon-chevron-down')
        } else {
            itemClassActive.add('itemClassActive')
            btnIcon.remove('glyphicon-chevron-down')
            btnIcon.add('glyphicon-chevron-up')

            if (hideDetail && showDetail) {
                hideDetail.style.display = 'none'
                showDetail.style.display = ''
            }
        }
    }
})

/*
@joke -
- what does aircons and computers have in common?
 -- they both become useless when you open windows.. ;)
*/
