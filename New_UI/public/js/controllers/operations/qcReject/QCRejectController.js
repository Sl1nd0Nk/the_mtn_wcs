app.controller('QCRejectController', function ($scope, $location, RejectService, $mdDialog, $routeParams, $http, loginService, PickListService, $window, $timeout){
	$scope.NotConfigured = false;
	$scope.NoContainerScanned = false;
	$scope.ContainerScanned = false;

	$scope.GetStation = function(){
		loginService.getQCStationDetail().then(function(response){
			if(response.status == 200){
				let StationData = response.data;

				if($scope.SetViewState(StationData, true)){
					$scope.signedInStation = StationData.stationNumber;
					$scope.UpdateLoggedInUser(StationData.user);
					$scope.EvaluateContainerAtStation(StationData);
				}
			}
		});
	} /* GetStation */

	$scope.SetViewState = function(Data, Init){
		if(Init)
			$scope.InitPage();

		if(Data.NoUser){
			$location.path('/');
			return false;
		}

		if(Data.NoStation){
			$scope.NoStation();
			return false;
		}

		return true;
	} /* SetViewState */

	$scope.InitPage = function(){
		$scope.NotConfigured = false;
		$scope.NoContainerScanned = false;
		$scope.ContainerScanned = false;
		$scope.TrCartonWeighing = false;
		$scope.TrCartonChecking = false;
		$scope.CartonChecking = false;
		$scope.TrCartonAskForWeight = false;
		$scope.TrCartonWaitForWeight = false;
		$scope.TrCartonWeightError = false;
		$scope.TrCartonWeightSuccess = false;
		$scope.TrAllChecked = false;
		$scope.TrAllCheckedButErrors = false;
	}

	$scope.NoStation = function(){
		$scope.InitPage();
		$scope.NotConfigured = true;
		return false;
	} /* NoStation */

	$scope.NoToteAtStation = function(){
		$scope.NoContainerScanned = true;
	} /* NoToteAtStation */

	$scope.ClearInput = function(id){
		let Input = document.getElementById(id);
		Input.value = '';
		Input.focus();
	} /* ClearInput */

	$scope.ValidateScannedContainer = function(ContainerID){
		$scope.ClearInput('scanContainer');

		let data = {
			ContainerID: ContainerID,
			Scanned: true
		}

		PickListService.getQCPickList(JSON.stringify(data)).then(function(doc){
			if(doc.status != 200){
				window.alert(doc.Err);
			}

			if($scope.SetViewState(doc.data, false)){
				let Data = doc.data.Document;

				if(ContainerID[0] != '7' && Data.transporterID != null){
					window.alert('Scanned Parcel should be in transporter ' + Data.transporterID);
					return;
				}

				if(ContainerID[0] != '7'){
					return $scope.HandleScannedCarton(Data);
				}

				return $scope.HandleScannedTransporter(Data);
			}
		});
	} /* ValidateScannedContainer */

	$scope.ValidateContainerAtStation = function(ContainerAtStation){
		$scope.ClearInput('scanContainer');

		let data = {
			ContainerID: ContainerAtStation
		}

		PickListService.getQCPickList(JSON.stringify(data)).then(function(doc){
			if(doc.status != 200){
				return $scope.NoToteAtStation();
			}

			if($scope.SetViewState(doc.data, false)){
				let Data = doc.data.Document;
				if(ContainerAtStation[0] != '7'){
					return $scope.ProceedToCartonContentChecking(ContainerAtStation);
				}

				return $scope.ProceedToTransporterContentChecking(ContainerAtStation);
			}
		});
	} /* ValidateContainerAtStation */

	$scope.ProceedToTransporterContentChecking = function(ContainerID){
		$scope.InitPage();

		PickListService.AddQCTrPickListData(ContainerID).then(function(doc){
			if(doc.status != 200){
				return window.alert('Failed to add container to the qc station');
			}

			if($scope.SetViewState(doc.data, false)){
				let StationData = doc.data.StationData;
				let x = 0;
				let NotChecked = -1;
				let Inprogress = -1;
				let ProblemCartons = -1;
				while(x < StationData.Cartons.length){
					if((!StationData.Cartons[x].MeasuredWeight || StationData.Cartons[x].MeasuredWeight <= 0) && NotChecked < 0){
						NotChecked = x;
					}

					if(StationData.Cartons[x].CartonID == StationData.Inprogress && !StationData.Cartons[x].WeightCheckComplete && Inprogress < 0){
						Inprogress = x;
						break;
					}

					if(StationData.Cartons[x].CartonProblem && ProblemCartons < 0){
						ProblemCartons = x;
					}
					x++;
				}

				$scope.cartonScanned = ContainerID;
				$scope.TrContainer = ContainerID;

				if(Inprogress >= 0){
					$scope.TrCurrentCarton = StationData.Cartons[Inprogress].CartonID;

					if(StationData.Cartons[Inprogress].CartonCancelled){
						$scope.TrCurrentCarton = StationData.Cartons[Inprogress].CartonID;
						$scope.cartonScanned = StationData.Cartons[Inprogress].transporterID;
						$scope.ErrorWeightMsg = StationData.Cartons[Inprogress].CartonReason;
						$scope.TrCartonCancelled = true;
						return;
					}

					if(!StationData.Cartons[Inprogress].MeasuredWeight){
						if(StationData.ScaleQueryCounts < 30 && !StationData.ScaleManualMode && !StationData.AverageWeight){
							return $scope.CheckIfWaitReceived();
						}

						$scope.TrCartonAskForWeight = true;
						return;
					}

					if(StationData.LastScannedResult){
						$scope.TrCartonWeightError = true;
						$scope.ErrorWeightMsg = StationData.LastScannedResult;
					}

					$scope.ContainerScanned = true;
					$scope.TrCartonChecking = true;
					return $scope.DisplayItemsToCheck(StationData);
				}

				if(NotChecked >= 0){
					if(StationData.LastScannedResult){
						$scope.TrCartonWeightSuccess = true;
						$scope.SuccessWeightMsg = StationData.LastScannedResult;
					}

					if(StationData.Cartons[NotChecked].CartonCancelled){
						$scope.TrCurrentCarton = StationData.Cartons[NotChecked].CartonID;
						$scope.cartonScanned = StationData.Cartons[NotChecked].transporterID;
						$scope.ErrorWeightMsg = StationData.Cartons[NotChecked].CartonReason;
						$scope.TrCartonCancelled = true;
						return;
					}

					$scope.TrCartonWeighing = true;
					return;
				}

				if(ProblemCartons >= 0){
					return $scope.TransporterCheckingCompleteWithError();
				}

				return $scope.TransporterCheckingComplete();
			}
		});
	} /* ProceedToTransporterContentChecking */

	$scope.TransporterCheckingComplete = function(){
		$scope.TrAllChecked = true;
	} /* TransporterCheckingComplete */

	$scope.TransporterCheckingCompleteWithError = function(){
		$scope.TrAllCheckedButErrors = true;
	} /* TransporterCheckingCompleteWithError */

	$scope.HandleScannedCarton = function(Data){
		$scope.ClearInput('scanContainer');

		if(Data.Status == 'CANCELED'){
			window.alert('Scanned Parcel is cancelled');
			return;
		}

		if(Data.Status != 'PACKED'){
			if(Data.toteID != Data.orders[0].cartonID){
				window.alert('Scanned Parcel is not yet packed.');
				return;
			}

			if(Data.Status == 'SHIPPED'){
				window.alert('Scanned Parcel is already shipped');
				return;
			}

			window.alert('Scanned Parcel is not in a packed status');
			return;
		}

		if(!Data.orders[0].actualWeight1){
			if(!Data.orders[0].QcVerificationAttempts || Data.orders[0].QcVerificationAttempts < 3){
				window.alert('Scanned Parcel has not been weight. place the Parcel back on the conveyor before the weight scale to get a weight reading');
				return;
			}
		}

		if(Data.orders[0].actualWeight1 >= Data.orders[0].minWeightLimit && Data.orders[0].actualWeight1 <= Data.orders[0].maxWeightLimit){
			window.alert('Scanned Parcel weight is ok. Place the Parcel back on the conveyor.');
			return;
		}

		return $scope.ProceedToCartonContentChecking(Data.toteID);
	} /* HandleScannedCarton */

	$scope.ProceedToCartonContentChecking = function(ContainerID){
		$scope.InitPage();

		PickListService.AddQCPickListData(ContainerID).then(function(doc){
			if(doc.status != 200){
				return window.alert('Failed to add container to the qc station');
			}

			if($scope.SetViewState(doc.data, false)){
				$scope.ContainerScanned = true;

				if(ContainerID[0] == '7'){
					$scope.TrCartonChecking = true;
				} else {
					$scope.CartonChecking = true;
				}

				let StationData = doc.data.StationData;
				return $scope.DisplayItemsToCheck(StationData);
			}
		});
	} /* ProceedToCartonContentChecking */

	$scope.DisplayItemsToCheck = function(StationData){
		return $scope.DrawQcCheckingDisplay(StationData);
	} /* DisplayItemsToCheck */

	$scope.DrawQcCheckingDisplay = function(StationData){
		let x = 0;
		while(x < StationData.Cartons.length){
			if(!StationData.Cartons[x].WeightCheckComplete){
				break;
			}
			x++;
		}

		if(StationData.Inprogress){
			let y = 0;
			while(y < StationData.Cartons.length){
				if(StationData.Cartons[y].CartonID == StationData.Inprogress){
					x = y;
					break;
				}
				y++;
			}
		}

		if(x < StationData.Cartons.length){
			if(StationData.ContainerID[0] != '7' && StationData.Cartons[x].WeightFailReason){
				$scope.ErrorWeightMsg = StationData.Cartons[x].WeightFailReason;
				$scope.TrCartonWeightError = true;
			}
			$scope.cartonOrder = StationData.Cartons[x].OrderID;
			$scope.cartonScanned = StationData.ContainerID;
			$scope.listOfScannedItems = StationData.Cartons[x].Items;
			$scope.NoMoreToScan = StationData.Cartons[x].CartonChecked;
			return;
		}
	} /* DrawQcCheckingDisplay */

	$scope.HandleScannedTransporter = function(Data){
		return $scope.ProceedToTransporterContentChecking(Data.transporterID);
	} /* HandleScannedTransporter */

	$scope.SetDisplay = function(Data){
		$scope.InitDisplayView();
	} /* SetDisplay */

	$scope.InitDisplayView = function(){
	} /* InitDisplayView */

	$scope.UpdateLoggedInUser = function(user){
		loggedInUser.innerHTML = '<i class="fa fa-user fa-fw"></i>' + user;
	} /* UpdateLoggedInUser */

	$scope.EvaluateContainerAtStation = function(StationData){
		let ContainerAtStation = StationData.currentContainer;

		if(ContainerAtStation){
			return $scope.ValidateContainerAtStation(ContainerAtStation);
		}

		return $scope.NoToteAtStation();
	} /* EvaluateContainerAtStation */

	$scope.getMasterId = function(ContainerId){
		return $scope.ValidateScannedContainer(ContainerId);
	} /* getToteId */

	$scope.getTrCartonScan = function(TrCartonId){
		$scope.ClearInput('scanTrCarton');
		let data = {
			TrCartonId: TrCartonId,
			Container: $scope.cartonScanned
		}

		PickListService.QCFindCartonAndWeigh(JSON.stringify(data)).then(function(doc){
			if(doc.status != 200){
				return window.alert('Scanned Parcel ID is invalid');
			}

			if($scope.SetViewState(doc.data, false)){
				let Err = doc.data.Err;
				if(Err){
					return window.alert(Err);
				}

				$scope.InitPage();
				$scope.TrCurrentCarton = TrCartonId;
				let StationData = doc.data.StationData;
				let AskForWait = doc.data.AskForWait;
				let x = 0;

				while(x < StationData.Cartons.length){
					if(StationData.Cartons[x].CartonID == StationData.Inprogress){
						if(StationData.Cartons[x].CartonCancelled){
							break;
						}
					}

					x++;
				}

				if(x < StationData.Cartons.length){
					$scope.TrCurrentCarton = StationData.Cartons[x].CartonID;
					$scope.cartonScanned = StationData.Cartons[x].transporterID;
					$scope.ErrorWeightMsg = StationData.Cartons[x].CartonReason;
					$scope.TrCartonCancelled = true;
					return;
				}

				if(AskForWait){
					$scope.TrCartonAskForWeight = true;
					return;
				}

				if(!StationData.AverageWeight){
					$scope.TrCartonWaitForWeight = true;
					$scope.TrCurrentCarton = StationData.Inprogress;
					return $scope.CheckIfWaitReceived();
				}

				return $scope.checkTrCartonWeight(StationData.AverageWeight);
			}
		});
	} /* getTrCartonScan */

	$scope.CheckIfWaitReceived = function(){
		PickListService.QCCheckIfWaitReceived().then(function(doc){
			if(doc.status != 200){
				return window.alert('Failed to retrieve weight from scale.');
			}

			if($scope.SetViewState(doc.data, false)){
				let StationData = doc.data.StationData;
				$scope.InitPage();

				if(StationData.Inprogress){
					$scope.TrCurrentCarton = StationData.Inprogress;
				}

				if(StationData.AverageWeight){
					return $scope.checkTrCartonWeight(StationData.AverageWeight);
				}

				if(StationData.ScaleQueryCounts >= 30){
					window.alert('Failed to retrieve weight from scale. Enter the weight manually');
					$scope.TrCartonAskForWeight = true;
					return;
				}

				$scope.TrCartonWaitForWeight = true;
				$timeout(function(){
					return $scope.CheckIfWaitReceived();
				}, 500);
			}
		});
	} /* CheckIfWaitReceived */

	$scope.checkTrCartonWeight = function(TrCartonWeight){
		$scope.ClearInput('scanTrCartonWeight');
		let data = {
			TrCartonId: $scope.TrCurrentCarton,
			Container: $scope.cartonScanned,
			EnteredWeight: TrCartonWeight
		}

		PickListService.QCCompareEnteredWeight(JSON.stringify(data)).then(function(doc){
			if(doc.status != 200){
				return window.alert('Invalid Weight Entered');
			}

			if($scope.SetViewState(doc.data, false)){
				let Err = doc.data.Err;
				if(Err){
					return window.alert(Err);
				}

				$scope.InitPage();
				let WeightOk = doc.data.WeightOk;
				let StationData = doc.data.StationData;
				let NotComplete = -1;
				let ProblemCartons = -1;
				let NotChecked = -1;
				if(WeightOk){
					let x = 0;
					while(x < StationData.Cartons.length){
						if(!StationData.Cartons[x].WeightCheckComplete && NotComplete < 0){
							NotComplete = x;
						}

						if(StationData.Cartons[x].CartonProblem && ProblemCartons < 0){
							ProblemCartons = x;
						}

						if((!StationData.Cartons[x].MeasuredWeight || StationData.Cartons[x].MeasuredWeight <= 0) && NotChecked < 0){
							NotChecked = x;
						}
						x++;
					}

					$scope.TrCartonWeightSuccess = true;
					$scope.SuccessWeightMsg = StationData.LastScannedResult;

					if(NotChecked >= 0){
						$scope.TrCartonWeighing = true;
						return;
					}

					if(ProblemCartons >= 0){
						return $scope.TransporterCheckingCompleteWithError();
					}

					return $scope.TransporterCheckingComplete();
				}

				$scope.ContainerScanned = true;
				$scope.TrCartonChecking = true;
				$scope.TrCartonWeightError = true;
				$scope.ErrorWeightMsg = StationData.LastScannedResult;

				return $scope.DisplayItemsToCheck(StationData);
			}
		});
	} /* checkTrCartonWeight */

	$scope.getScannedItem = function(ScannedItem){
		$scope.ClearInput('barcodeScan');
		let data = {
			ScannedItem: ScannedItem,
			CartonID: ($scope.cartonScanned[0] == '7')? $scope.TrCurrentCarton: $scope.cartonScanned
		}

		PickListService.QCFindScannedItem(JSON.stringify(data)).then(function(doc){
			if(doc.status != 200){
				return window.alert('Scanned Barcode/Serial Not Recognised');
			}

			if($scope.SetViewState(doc.data, false)){
				let Err = doc.data.Err;
				if(Err){
					return window.alert(Err);
				}

				let StationData = doc.data.StationData;
				return $scope.DrawQcCheckingDisplay(StationData);
			}
		});
	} /* getScannedItem */

	$scope.resetCarton = function(){
		$scope.ClearInput('scanContainer');
		$scope.ClearInput('barcodeScan');

		PickListService.ClearQCPickListData().then(function(doc){
			if(doc.status != 200){
				return window.alert('Failed to reset the qc station');
			}

			if($scope.SetViewState(doc.data, false)){
				$window.location.reload();
			}
		});
	} /* resetCarton */

	$scope.cartonComplete = function(){
		let data = {
			CartonID: ($scope.cartonScanned[0] == '7')? $scope.TrCurrentCarton: $scope.cartonScanned
		}

		PickListService.CompleteQCPickListCarton(JSON.stringify(data)).then(function(doc){
			if(doc.status != 200){
				return window.alert('Failed to complete the qc station');
			}

			if($scope.SetViewState(doc.data, false)){
				$window.location.reload();
			}
		});
	} /* cartonComplete */

	$scope.setAsideCarton = function(){
		let data = {
			CartonID: ($scope.cartonScanned[0] == '7')? $scope.TrCurrentCarton: $scope.cartonScanned
		}

		PickListService.AsideQCPickListCarton(JSON.stringify(data)).then(function(doc){
			if(doc.status != 200){
				return window.alert('Failed to set aside parcel at the qc station');
			}

			if($scope.SetViewState(doc.data, false)){
				$window.location.reload();
			}
		});
	} /* setAsideCarton */

	$scope.TrComplete = function(){
		let data = {
			Transporter: $scope.cartonScanned
		}

		PickListService.CompleteQCPickListTransporter(JSON.stringify(data)).then(function(doc){
			if(doc.status != 200){
				return window.alert('Failed to complete the qc station');
			}

			if($scope.SetViewState(doc.data, false)){
				$window.location.reload();
			}
		});
	} /* TrComplete */

	$scope.TrRemoveConfirm = function(){
		let data = {
			Transporter: $scope.cartonScanned,
			Carton: $scope.TrCurrentCarton
		}

		PickListService.TrRemoveConfirm(JSON.stringify(data)).then(function(doc){
			if(doc.status != 200){
				return window.alert('Failed to remove parcel from transporter');
			}

			if($scope.SetViewState(doc.data, false)){
				$window.location.reload();
			}
		});
	} /* TrRemoveConfirm */

	$scope.GetStation();
});

app.directive('focusMe', function($timeout, $parse) {
  return {
    link: function(scope, element, attrs) {
      var model = $parse(attrs.focusMe);
      scope.$watch(model, function(value) {
        if(value === true) {
          $timeout(function() {
            element[0].focus();
          });
        }
      });
    }
  };
});
