app.controller('QCRejectProcessController', function ($scope, $location, $mdDialog, RejectService, $http, $routeParams) {
    let barcodeScan = document.getElementById('barcodeScan')
    barcodeScan.focus()
    RejectService.getCartonID()
    .then(function (cartonID) {
      
            RejectService.getQCStation()
            .then(function (doc) {
                let cartonID = doc.data.orders[0].cartonID;
            if (cartonID !== 'undefined' && cartonID !== null || doc.data.active == true) {
                if (doc) {

                    if (doc.code == '00') {
                        $scope.NoStation = "none"
                        $scope.ShowStation = "none"
                        let totalQty = 0
                        let itemCount = 0;
                        $scope.cartonScanned = cartonID;
                        $scope.disableOpt = true;
                        var p = 0;

                        

                        while (p < doc.data.orders.length) {
                            if (cartonID == doc.data.orders[p].cartonID) {
                                $scope.listOfScannedItems = doc.data.orders[p].items
                                for (var x = 0; x < $scope.listOfScannedItems.length; x++) {
                                    totalQty = totalQty + $scope.listOfScannedItems[x].qty
                                }
                                break
                            }
                            p++
                        } 


                        let dataArry = [];
        
                        $scope.getScannedItem = function (scannedItem) {
                            RejectService.getQCStation()
                            .then(function (docs) {
                            $scope.errorDisplay = ''
                            let itemFound = false;
                            let serialScanned = false;
                            let serialIndex;
                            let totalQtyAdded = 0;
                            let serialCount;
                            if (cartonID.substr(0,1) == '3' || cartonID.substr(0,1) == '4' || cartonID.substr(0,1) == '5' || cartonID.substr(0,1) == '6' || cartonID.substr(0,1) == '1') {
                                let z = 0
                                let v = 0;
                                while (v < docs.data.orders.length) {
                                    if (cartonID == docs.data.orders[v].cartonID || docs.data.orders[v].cartonID != null) {
                                        
                                        for (var w = 0; w < docs.data.orders[v].items.length  ; w++) {
                                            totalQtyAdded = totalQtyAdded + docs.data.orders[v].items[w].checked;
                                        }
                                        break
                                    }
                                    v++
                                } 

                                while (z < docs.data.orders.length) {
                                    if (cartonID == docs.data.orders[z].cartonID) {
                                        let x = 0
                                        while (x < docs.data.orders[z].items.length) {
                                            let y = 0
                                        if(docs.data.orders[z].items[x].serialised == true){
                                            while (y < docs.data.orders[z].items[x].qty) {
                                                

                                                    if (scannedItem == docs.data.orders[z].items[x].serials[y] || '892700000'+scannedItem == docs.data.orders[z].items[x].serials[y]) {
                                                        itemFound = true;
                                                        
                                                        if (docs.data.orders[z].items[x].RescannedSerials.length < docs.data.orders[z].items[x].qty) {
                                                           
                                                            serialIndex =  docs.data.orders[z].items[x].RescannedSerials.indexOf(scannedItem)
                                                              if(serialIndex < 0){
                                                                

                                                                  $http.post("/updateQCStation/" + scannedItem).then(function(result){
                                                                      if(result){
                                                                            
                                                                            RejectService.getQCStation()
                                                                                .then(function (doc) {
                                                                                    

                                                                                     serialIndex =  docs.data.orders[z].items[x-1].RescannedSerials.indexOf(scannedItem)
                                                                                     doc.data.orders[z].items[x-1].checked = doc.data.orders[z].items[x-1].RescannedSerials.length;
                                                                                     $scope.listOfScannedItems = doc.data.orders[z].items;
                                                                                     
                                                                                    $scope.scannedItem = '';
                                                                                    barcodeScan.focus();
                                                                                })
                                                                        }else{
                                                                            console.log("encountered an error" + err)
                                                                        }
                                                                   
                                                                  })           
                                                                 
                                                              }else{
                                                                  var confirm = $mdDialog.alert()
                                                                    .title('Items has already been scanned. Please scan next item.')
                                                                    .ariaLabel('Lucky day')
                                                                    .targetEvent(cartonID)
                                                                    .ok('Ok')
                                                    
                                                                    $mdDialog.show(confirm).then(function() {
                                                                        $scope.scannedItem = ''
                                                                        barcodeScan.focus()
                                                                    }, function() {
                                                                         
                                                                    });
                                                              }
                                                            
                                                            
                                                            if ((totalQtyAdded+1) == totalQty) {
                                                                $scope.disableOpt = false;
                                                                var confirm = $mdDialog.alert()
                                                                .title('Carton ID: ' + cartonID)
                                                                .textContent('All the items have been scanned. Please place transporter back on the conveyor')
                                                                .ariaLabel('Lucky day')
                                                                .targetEvent(cartonID)
                                                                .ok('Ok')
                                                
                                                                $mdDialog.show(confirm).then(function() {
                                                                    var enteredWeight = $routeParams.ToDo
                                                                    var updateInfo = {
                                                                        weight: $routeParams.ToDo, 
                                                                        cartonID: cartonID,
                                                                        cartonComplete: true
                                                                    }

                                                                    var jsonInfo = JSON.stringify(updateInfo)

                                                                    $http.post('/updateRejected/' + jsonInfo).then(function(updateResp) {
                                                                        if (updateResp.data.code == '00') {
                                                                            if(updateResp.data.data.transporterID !== null && updateResp.data.data.transporterID.substr(0,1) == '7') {
                                                                                $http.get('/API/checkPickList/'+ updateResp.data.data.transporterID).then(function(checkResp){
                                                                                    if(checkResp.data.code == '00') {
                                                                                        var e = 0
                                                                                        var done = false
                                                                                        while(e < checkResp.data.data.length) {
                                                                                            var d = 0
                                                                                            while(d< checkResp.data.data[e].orders.length) {
                                                                                                if(checkResp.data.data[e].orders[d].rejected) {
                                                                                                    done = true
                                                                                                    $location.path('/qcWeigh/' + updateResp.data.data.transporterID)
                                                                                                    break
                                                                                                }
                                                                                                d++
                                                                                            }
                                                                                            e++
                                                                                        }

                                                                                        if(!done) {
                                                                                            var confirm = $mdDialog.alert()
                                                                                            .title('Transport ID: ' + updateResp.data.data.transporterID)
                                                                                            .textContent('Transporter item check completed, place transporter back on the conveyor')
                                                                                            .ariaLabel('Lucky day')
                                                                                            .targetEvent(updateResp.data.data.transporterID)
                                                                                            .ok('Ok')
                                                                                
                                                                                            $mdDialog.show(confirm).then(function() {
                                                                                                $http.get('/updateQcReject/'+updateResp.data.data.transporterID).then(function(qcRejectRes){
                                                                                                    if(qcRejectRes.data.code == '00'){
                                                                                                        $location.path('/qcReject')
                                                                                                    } else {
                                                                                                        $location.path('/qcReject')
                                                                                                    }
                                                                                                })
                                                                                            }, function() {
                                                                                                $location.path('/qcReject')  
                                                                                            });
                                                                                        }
                                                                                    } else {
                                                                                        $location.path('/qcReject')
                                                                                    }
                                                                                })
                                                                            } else {
                                                                                $location.path('/qcReject')
                                                                            }
                                                                        } else {
                                                                            $location.path('/qcReject')
                                                                        }
                                                                    })
                                                                }, function() {
                                                                    
                                                                });
                                                            }
                                                            break
                                                        } else {
                                                            var confirm = $mdDialog.alert()
                                                            .title('Item has already been scanned. Please scan next item.')
                                                            .ariaLabel('Lucky day')
                                                            .targetEvent(cartonID)
                                                            .ok('Ok')
                                            
                                                            $mdDialog.show(confirm).then(function() {
                                                                $scope.scannedItem = ''
                                                                barcodeScan.focus()
                                                            }, function() {
                                                                
                                                            });
                                                        }

                                                        break
                                                    }
                                                
                                           

                                                y++
                
                                                // if ((y-1) > docs.data.orders[z].items[x].serials.length) {
                                                //     $scope.errorDisplay = 'Item not found. Contact superviser.'
                                                // }
                                            }
                                        }else if(docs.data.orders[z].items[x].barcode == scannedItem){
                                                    itemFound = true;
                                                    if(docs.data.orders[z].items[x].qty > docs.data.orders[z].items[x].checked){
                                                        
                                                     $http.post("/updateQCStation/" + scannedItem).then(function(result){
                                                                      if(result){
                                                                            
                                                                            RejectService.getQCStation()
                                                                                .then(function (doc) {
                                                                                    
                                                                                     doc.data.orders[z].items[x-1].checked = doc.data.orders[z].items[x-1].RescannedSerials.length;
                                                                                     $scope.listOfScannedItems = doc.data.orders[z].items;
                                                                                     
                                                                                    $scope.scannedItem = '';
                                                                                    barcodeScan.focus();
                                                                                })
                                                                        }else{
                                                                            console.log("encountered an error" + err)
                                                                        }
                                                                   
                                                                  })
                                                        } else{
                                                            var confirm = $mdDialog.alert()
                                                            .title('All non serialised items have been scanned. Please scan next item.')
                                                            .ariaLabel('Lucky day')
                                                            .targetEvent(cartonID)
                                                            .ok('Ok')
                                            
                                                            $mdDialog.show(confirm).then(function() {
                                                                $scope.scannedItem = ''
                                                                barcodeScan.focus()
                                                            }, function() {
                                                                
                                                            });
                                                 }

                                                 
                                                  }
                                            x++
                                        }
                                        break
                                    }
                                    z++

                                    if (z > docs.data.orders.length) {
                                        console.log('Carton ID: ' + cartonID + ' Not Found.')
                                    }
                                }

                                if (!itemFound) {
                                    var confirm = $mdDialog.alert()
                                    .title('Item not found. Contact superviser.')
                                    .ariaLabel('Lucky day')
                                    .targetEvent(cartonID)
                                    .ok('Ok')
                    
                                    $mdDialog.show(confirm).then(function() {
                                        $scope.scannedItem = ''
                                        barcodeScan.focus()
                                    });
                                }
                            } else {
                                var confirm = $mdDialog.alert()
                                .title('Carton ID: ' + cartonID + ' Not found.')
                                .ariaLabel('Lucky day')
                                .targetEvent(cartonID)
                                .ok('Ok')
                
                                $mdDialog.show(confirm).then(function() {
                                    $scope.scannedItem = ''
                                    barcodeScan.focus()
                                }, function() {
                                    
                                });
                            }
                        })
                        }

                        $scope.cartonComplete = function () {
                            var confirm = $mdDialog.alert()
                            .title('Carton ID: ' + cartonID)
                            .textContent('Item check complete. Place carton on conveyor.')
                            .ariaLabel('Lucky day')
                            .targetEvent(cartonID)
                            .ok('Ok')
            
                            $mdDialog.show(confirm).then(function() {
                                var enteredWeight = $routeParams.ToDo
                                var updateInfo = {
                                    weight: $routeParams.ToDo, 
                                    cartonID: cartonID,
                                    cartonComplete: true
                                }

                                console.log('The weight: ' + updateInfo.weight + ' The Carton ID: ' + updateInfo.cartonID)

                                var jsonInfo = JSON.stringify(updateInfo)

                                $http.post('/updateRejected/' + jsonInfo).then(function(updateResp) {
                                    if (updateResp.data.code == '00') {
                                        if(updateResp.data.data.transporterID !== null && updateResp.data.data.transporterID.substr(0,1) == '7') {
                                            $http.get('/API/checkPickList/'+ updateResp.data.data.transporterID).then(function(checkResp){
                                                if(checkResp.data.code == '00') {
                                                    var e = 0
                                                    var done = false
                                                    while(e < checkResp.data.data.length) {
                                                        var d = 0
                                                        while(d< checkResp.data.data[e].orders.length) {
                                                            if(checkResp.data.data[e].orders[d].rejected) {
                                                                done = true
                                                                $location.path('/qcWeigh/' + updateResp.data.data.transporterID)
                                                                break
                                                            }
                                                            d++
                                                        }
                                                        e++
                                                    }

                                                    if(!done) {
                                                        var confirm = $mdDialog.confirm()
                                                        .title('Transport ID: ' + updateResp.data.data.transporterID)
                                                        .textContent('Transporter item check completed, place transporter back on the conveyor')
                                                        .ariaLabel('Lucky day')
                                                        .targetEvent(updateResp.data.data.transporterID)
                                                        .ok('Ok')
                                            
                                                        $mdDialog.show(confirm).then(function() {
                                                            $http.get('/updateQcReject/'+updateResp.data.data.transporterID).then(function(qcRejectRes){
                                                                if(qcRejectRes.data.code == '00'){
                                                                    $location.path('/qcReject')
                                                                } else {
                                                                    $location.path('/qcReject')
                                                                }
                                                            })
                                                        }, function() {
                                                            $location.path('/qcReject')  
                                                        });
                                                    }
                                                } else {
                                                    $location.path('/qcReject')
                                                }
                                            })
                                        } else {
                                            $location.path('/qcReject')
                                        }
                                    } else {
                                        $location.path('/qcReject')
                                    }
                                })
                            }, function() {
                                
                            });
                        }

                        $scope.resetCarton = function () {
                            var confirm = $mdDialog.confirm()


                            .title('Carton ID: ' + cartonID)
                            .textContent('Incomplete checkup. Please contact superviser.')
                            .ariaLabel('Lucky day')
                            .targetEvent(cartonID)
                            .ok('Ok')

                            


                            $mdDialog.show(confirm).then(function() {
                               $http.post("/resetQCReject/" + cartonID).then(function(result){
                                 if(result){
                                    $location.path('/qcReject')
                                 }
                                 
                             })
                            });
                        }
                    } else {
                        
                         $scope.getMasterId = function (cartonID) {
                        if (cartonID) {
                            RejectService.checkReject(cartonID)
                            .then(function (resp) {
                                // console.log('RESPONSE: ************** | ' + resp)
                                if (resp.code == '00') {
                                    let jsonData = JSON.stringify(resp.data)
                                    $http.post(/insertNewQCSession/+jsonData).then(function(){
                                        $location.path('/qcReject')
                                    })
                                }else{
                                    window.alert('Carton ID Not Found.' + cartonID)
                                    $location.path('/qcReject')
                                   } 
                                    })
                                }
                            }
                        }

        

        } else {
            $scope.errorDisplay = 'Invalid Carton ID Scanned.'
              $location.path('/qcReject')

        }
    }
    })

    })
})
