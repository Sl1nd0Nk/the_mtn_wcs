app.controller('QCWeighedController', function ($scope, $location, RejectService, $mdDialog, $routeParams, $http) {
    let body = JSON.parse($routeParams.ToDo)
    let scanCarton = document.getElementById('scanCarton')
    RejectService.getParameters()
    .then(function (parameterDocs) {
        if (parameterDocs.code == '00') {
            RejectService.getWeighedCartonID2(body.cartonID)
            .then (function (docs) {
                if (docs.code == '00') {
                    $scope.getWeight = function(){
                        $http.post('/getManualWeight').then(function(tcpResp){
                            if(tcpResp.data.code == '00'){
                                var confirm = $mdDialog.confirm()
                                .title('Carton ' + body.cartonID + ' Weighs ' + tcpResp.data.data.substr(19, 4))
                                .textContent('The weight as per manual scale.')
                                .ariaLabel('Lucky day')
                                .targetEvent(body.cartonID)
                                .ok('Ok')
                                .cancel('re-weigh')
    
                                $mdDialog.show(confirm).then(function() {
                                    console.log('======================' + tcpResp.data.data.substr(19, 4) + '======')
                                    $scope.processWight(tcpResp.data.data.substr(19, 4))
                                }, function() {
                                    $scope.getWeight()
                                });
                            }else{
                                var confirm = $mdDialog.alert()
                                .title('There Was A Problem Finding The Cartons Weight.')
                                .textContent('Please start this process again.')
                                .ariaLabel('Lucky day')
                                .targetEvent(body.cartonID)
                                .ok('Ok')
    
                                $mdDialog.show(confirm).then(function() {
                                    // scanCarton.focus()
                                    // $scope.cartonID = ''
                                }, function() {
                                    
                                });
                            }
                        })
                    }

                    $scope.dummyWeight = function(){
                        $http.post('/getManualWeight').then(function(tcpResp){
                            if(tcpResp.data.code == '00'){
                                console.log(tcpResp.data.data.substr(18, 6))
                                setTimeout(function(){
                                    $scope.getWeight()
                                }, 2000)
                            }else{
                                $scope.getWeight()
                            }
                        })
                    }

                    $scope.dummyWeight()

                    $scope.resetCarton = function(){
                        $scope.getWeight()
                    }

                    $scope.processWight = function (weight) {
                        RejectService.getWeighedCartinID()
                        .then(function (cartonID) {
                            var x = 0
                            while (x < docs.data.orders.length) {
                                if (cartonID == docs.data.orders[x].cartonID) {
                                    var y = 0
                                    while (y < parameterDocs.data[0].weightSettings.length) {
                                        if (parameterDocs.data[0].weightSettings[y].StockSize == cartonID.substr(0,1)) {
                                            if (weight >= (docs.data.orders[x].picklistWeight - parameterDocs.data[0].weightSettings[y].LowerTolerance) 
                                                && weight <= (docs.data.orders[x].picklistWeight + parameterDocs.data[0].weightSettings[y].UpperTolerance)) {
                                                var confirm = $mdDialog.alert()
                                                .title('Carton ID: ' + cartonID)
                                                .textContent('Correct weight range. Place back onto transporter: ' + body.transporterID)
                                                .ariaLabel('Lucky day')
                                                .targetEvent(cartonID)
                                                .ok('Ok')
                                    
                                                $mdDialog.show(confirm).then(function() {
                                                    var weightAndCarton = {cartonID: cartonID, weight: weight}
                                                    console.log('~~~~~~~~~~~~~~~~ carton ID: ' + weightAndCarton.cartonID + ' ~~~~~~~~~~~~~~~~~~~~~')
                                                    var strweightAndCarton = JSON.stringify(weightAndCarton)
                                                    $http.post('/updateRejected/' + strweightAndCarton).then(function(updateResp) {
                                                        if(updateResp.data.code == '00') {
                                                            console.log('~~~~~~~~~~~~~~~~ CODE: ' + updateResp.data.code + ' ~~~~~~~~~~~~~~~~~~~~~')
                                                            scanCarton.focus()
                                                            $scope.cartonID = ''
                                                            RejectService.checkReject(body.transporterID)
                                                            .then(function(resp){
                                                                if (resp.code == '00') {
                                                                    if (body.transporterID.substr(0,1) === '7') {
                                                                        let rejected = false
                                                                        var c = 0
                                                                        while (c < resp.data.length) {
                                                                            var p = 0
                                                                            while (p < resp.data[c].orders.length) {
                                                                                if (resp.data[c].orders[p].rejected) {

                                                                                    $location.path('/qcWeigh/' + body.transporterID)
                                                                                    rejected = true
                                                                                    break
                                                                                }
                                                                                p++
                                                                            }
                                                                            c++
                                                                        }

                                                                        if(!rejected) {
                                                                            var confirm = $mdDialog.alert()
                                                                            .title('Transport ID: ' + body.transporterID)
                                                                            .textContent('Transport item check completed, place transporter back on the conveyor')
                                                                            .ariaLabel('Lucky day')
                                                                            .targetEvent(body.transporterID)
                                                                            .ok('Ok')
                                                                
                                                                            $mdDialog.show(confirm).then(function() {
                                                                                $http.post('/updateTransporter/' + body.transporterID).then(function(resp){
                                                                                    if(resp.data.code == '00') {
                                                                                        $location.path('/qcReject')  
                                                                                    } else {
                                                                                        console.log('___________________ACTUALL UPDATED!!!')
                                                                                        $location.path('/qcReject')  
                                                                                    }
                                                                                })
                                                                            }, function() {
                                                                                $location.path('/qcReject')  
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                            })
                                                        } else {
                                                            console.log('~~~~~~~~~~~~~~~~ CODE: ' + updateResp.data.code + ' ~~~~~~~~~~~~~~~~~~~~~')
                                                        }
                                                    })
                                                }, function() {
                                                    
                                                });
                                                break
                                            } else {
                                                var confirm = $mdDialog.alert()
                                                .title('Carton ID: ' + cartonID)
                                                .textContent('Fail. Weight is either too high, or too low. Press ok for further inspection')
                                                .ariaLabel('Lucky day')
                                                .targetEvent(cartonID)
                                                .ok('Ok')
                                    
                                                $mdDialog.show(confirm).then(function() {
                                                    RejectService.setCarton(cartonID)
                                                    if (cartonID.substr(0,1) === '7') {
                                                        $location.path('/qcWeigh/')    
                                                    } else {
                                                        $location.path('/cartonScanning/' + weight)
                                                    }
                                                }, function() {
                                                    
                                                });
                                                break
                                            }
                                        }
                                        y++
                                    }
                                }
                                x++
                            }
                        })
                    }
                } else {
                    console.log('Error finding the weight.')
                }
            })
        } else {
            window.alert('Fail. Parameters not found Contact supervisor.')
        }
    })
})
