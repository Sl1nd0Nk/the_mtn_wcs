app.controller('palletScanningController', function ($scope, $location, RejectService, $mdDialog, $routeParams, $http) {
    $scope.palletScanned = $routeParams.ToDo
    $http.get('/getLoadInfoDisplay/'+$routeParams.ToDo).then(function(response){
        if(response.data.code == '00'){
            var displayObj = {} 
            $scope.listOfScannedItems = [] 
            displayObj.itemCode = response.data.data.orders[0].items[0].itemCode
            displayObj.itemDescription = response.data.data.orders[0].items[0].itemDescription
            displayObj.qty = response.data.data.orders[0].items[0].qty
            displayObj.qtyPacked = response.data.data.orders[0].items[0].serials.length

            console.log('SEIRIALS LENGTH: ' + response.data.data.orders[0].items[0].serials.length)

            $scope.listOfScannedItems.push(displayObj)

            $scope.getScannedItem = function(docID){
                var palletInfo = {}
                palletInfo.loadID = $routeParams.ToDo
                palletInfo.docID = docID
                var palletSendInfo = JSON.stringify(palletInfo)

                $location.path('/palletComplete/'+palletSendInfo)
            }
        }else{
            var confirm = $mdDialog.alert()
            .title('Load ID: ' + loadID)
            .textContent('Pallet load ID not found in the picklist. Please contact supervisor')
            .ok('Ok')

            $mdDialog.show(confirm).then(function() {
                $scope.masterId = ''
                palletId.focus()
            }, function() {
                $scope.masterId = ''
                palletId.focus()
            });
        }
    })
})
