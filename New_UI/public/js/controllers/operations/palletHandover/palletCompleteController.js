app.controller('palletComplete', function ($scope, $location, RejectService, $mdDialog, $routeParams, $http) {
    $scope.header = 'PALLET HANDOVER COMPLETE'
    var palletInfoJson = JSON.parse($routeParams.ToDo)
    $scope.loadID = palletInfoJson.loadID

    console.log('THE LOAD ID: ' + $routeParams.ToDo)

    $http.get('/updateShipped/'+ palletInfoJson.docID).then(function(response){
        if(response.data.code == '00'){
            $scope.reset = "COMPLETE"
            $scope.displayMessage = 'ATTACH COURIER LABEL AND TRANSFER PALLET TO COURIER'
        }else{
            $scope.reset = "RESET"
            $scope.displayMessage = response.data.message
        }
    })

    $scope.resetCarton = function(){
        $http.get('/reset/'+JSON.stringify(palletInfoJson)).then(function(response){
            if(response.data.code == '00'){
                $location.path('/palletHandover')
            }else{
                $location.path('/palletHandover')
            }
        })
    }

    $scope.done = function(){
        $location.path('/palletHandover')
    }
})
