app.controller('palletHandovers', function ($scope, $location, RejectService, $mdDialog, $routeParams, $http, $window){
	$scope.Packing = false;
	$scope.Handover = false;

	$scope.GetStation = function(){
		$http.get('/getPalletStation/').then(function(response){
			if(response.status != 200){
				$location.path('/');
				return;
			}

			let Data = response.data;
			if(Data.NoUser){
				$location.path('/');
				return;
			}

			$scope.Packing = true;
			$scope.Handover = false;
			let palletId = document.getElementById('palletId');
			palletId.focus();
		});
	} /* GetStation */

    $scope.getMasterId = function(loadID){
		let Input = document.getElementById('palletId');
		Input.value = '';

        $http.get('/getLoadInfo/'+loadID).then(function(response){
            if(response.data.code == '00'){
				let Data = response.data.data;

				$scope.Packing = false;
				$scope.Handover = true;
				$scope.HandoverID = loadID;
				$scope.reset = "COMPLETE";

				if(response.data.message == 'PACKED'){
					$scope.displayMessage = 'TRANSFER PALLET TO COURIER - ' + Data.orders[0].courierID + ' -';
				} else {
					$scope.displayMessage = 'ATTACH COURIER LABEL AND TRANSFER PALLET TO COURIER - ' + Data.orders[0].courierID + ' -';
				}
            }else{
				$scope.reset = "RESET";

                let confirm = $mdDialog.alert()
                .title('Load ID: ' + loadID)
                .textContent(response.data.message)
                .ok('Ok');

                $mdDialog.show(confirm).then(function() {
                    $scope.masterId = '';
                    Input.focus();
                });
            }
        });
    }

    $scope.resetCarton = function(){
        $window.location.reload();
    }

    $scope.done = function(){
		$http.get('/updateShipped/'+ $scope.HandoverID).then(function(response){
			if(response.data.code == '00'){
				$window.location.reload();
			}else{
                let confirm = $mdDialog.alert()
                .title('Load ID: ' + $scope.HandoverID)
                .textContent(response.data.message)
                .ok('Ok');

                $mdDialog.show(confirm).then(function() {
                    $scope.masterId = '';
                });
			}
		});
    }

    $scope.GetStation();
});

app.directive('focusMe', function($timeout, $parse) {
  return {
    link: function(scope, element, attrs) {
      var model = $parse(attrs.focusMe);
      scope.$watch(model, function(value) {
        if(value === true) {
          $timeout(function() {
            element[0].focus();
          });
        }
      });
    }
  };
});
