app.controller('PTLRejectController', function ($scope, $location, loginService, PickListService, $http, $mdDialog, $window) {
	$scope.CheckUser = function(){
		loginService.getLoggedInUser().then(function(response){
			if(response.status == 200){
				let UserData = response.data;

				if($scope.SetViewState(UserData, true)){
					$scope.UpdateLoggedInUser(UserData.user);
				}
			}
		});
	} /* CheckUser */

	$scope.SetViewState = function(Data, Init){
		if(Init)
			$scope.InitPage();

		if(Data.NoUser){
			$location.path('/');
			return false;
		}

		return true;
	} /* SetViewState */

	$scope.UpdateLoggedInUser = function(user){
		loggedInUser.innerHTML = '<i class="fa fa-user fa-fw"></i>' + user;
	} /* UpdateLoggedInUser */

	$scope.InitPage = function(){
		$scope.ContainerScanned = false;
		$scope.ContainerHistory = false;
	} /* InitPage */

	$scope.ClearInput = function(id){
		let Input = document.getElementById(id);
		Input.value = '';
		Input.focus();
	} /* ClearInput */

	$scope.getToteId = function(ContainerId){
		$scope.ClearInput('scannedTote');

		PickListService.getConveyorRejectedToteId(ContainerId).then(function(doc){
			if(doc.status != 200){
				window.alert(doc.Err);
			}

			if($scope.SetViewState(doc.data, false)){
				$scope.InitPage();

				let Err = doc.data.Err;
				if(Err){
					return window.alert(Err);
				}

				let Data = doc.data;
				$scope.ContainerScanned = true;
				$scope.cartonScanned = Data.Container;
				$scope.ErrorMsg = Data.CurrentError;
			}
		});
	} /* getToteId */

	$scope.clearErr = function(){
		let ToteId = $scope.cartonScanned;
		PickListService.ClearPtlRejectError(ToteId).then(function(doc){
			if(doc.status != 200){
				return window.alert('Failed to clear error for tote ' + ToteId);
			}

			if($scope.SetViewState(doc.data, false)){
				$window.location.reload();
			}
		});
	} /* resetCarton */

	$scope.CheckUser();
});
