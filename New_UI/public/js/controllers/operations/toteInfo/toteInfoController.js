/*
 * @Author: Ntokozo Majozi
 * @Date: 2017-04-06 10:26:29
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-08-28 11:06:18
 */

/**
* @controller toteInfoController
* @function getToteInfo - gets Tote Information.
*/

app.controller('toteInfoController', function ($scope, $location, $rootScope, toteInfoService, loginService, $mdDialog, $http) {
//    loginService.getStationDetail($rootScope.loggedInIP)
  //   .then(function (station) {
    //     if (station) {
             $scope.itemsToDisplay = []
             $scope.ordersToDisplay = []
             toteInfoService.getTote()
            .then(function (tInfo) {
                if (tInfo) {
                    if (tInfo.data.code === '00') {
                        $scope.itemsToDisplay = tInfo.data.data.items
                        $scope.ordersToDisplay = tInfo.data.data.orders
                        $scope.barcode = tInfo.data.data.toteID
                        $scope.errorDisplay = ''
                        $scope.toteId = ''
                    } else {
                        // $scope.errorDisplay = 'Tote: ' + $scope.toteId + ' is not linked to a Pick List'
                        $scope.itemsToDisplay = []
                        $scope.ordersToDisplay = []
                        $scope.toteId = ''
                        $scope.barcode = ''
                    }
                } else {
                    $scope.errorDisplay = 'Invalid tote ID. Please ensure that tote was scanned properly.'
                    $scope.itemsToDisplay = []
                    $scope.ordersToDisplay = []
                    $scope.toteId = ''
                    $scope.barcode = ''
                }
            })
             $scope.submit = function () {
                 toteInfoService.getToteInfo($scope.toteId)
                .then(function (doc) {
                    if (doc) {
                        if (doc.data.code === '00') {
                            console.log('ITEMS: ' + doc.data.data.items)
                            $scope.itemsToDisplay = doc.data.data.items
                            $scope.ordersToDisplay = doc.data.data.orders
                            $scope.barcode = $scope.toteId
                            $scope.errorDisplay = ''
                            $scope.toteId = ''
                        } else {
                            $scope.errorDisplay = 'Tote: ' + $scope.toteId + ' is not linked to a Pick List'
                            $scope.itemsToDisplay = []
                            $scope.ordersToDisplay = []
                            $scope.toteId = ''
                            $scope.barcode = ''
                        }
                    } else {
                        $scope.errorDisplay = 'Invalid tote ID. Please ensure that tote was scanned properly.'
                        $scope.itemsToDisplay = []
                        $scope.ordersToDisplay = []
                        $scope.toteId = ''
                        $scope.barcode = ''
                    }
                })
             }
        
             $scope.submitOrder = function (orderID) {
                 toteInfoService.getItemByOrder(orderID)
                 .then(function (docs) {
                     $scope.itemsToDisplay = docs.data.data.items
                     $scope.orderHead = orderID
                 })
             }
        
             $scope.clear = function () {
                 $scope.itemsToDisplay = []
                 $scope.ordersToDisplay = []
                 $scope.toteId = ''
                 $scope.barcode = ''
                 $scope.orderHead = ''
             }

	     $scope.showSerials = function(barcode, itemCode) {
          $http.get('/fetchItems/' + barcode).then(function(response) {
            if(response.data.code == '00') {
                var serialsToDisplay = []
                var foundItem = false

                var x = 0
                while(x < response.data.data.orders.length) {
                    var y = 0
                    while(y < response.data.data.orders[x].items.length) {
                        if(response.data.data.orders[x].items[y].sku == itemCode) {
                            serialsToDisplay = response.data.data.orders[x].items[y].serials
                            foundItem = true

                            break
                        }
                        y++
                    }

                    if(foundItem) {
                        break
                    }
                    x++
                }

                if(foundItem) {
                    console.log('Items were found: ' + serialsToDisplay)

		    var confirm = $mdDialog.alert()
                    .title(itemCode + ' Serials:  ' + serialsToDisplay)
                    .ok('Ok')
            
                    $mdDialog.show(confirm).then(function() {});
                } else {
                    console.log('The serials were not found for tote: ' + barcode)

		    var confirm = $mdDialog.alert()
                    .title('The serials were not found for tote: ' + barcode)
                    .ok('Ok')
            
                    $mdDialog.show(confirm).then(function() {});
                }
            } else {
                console.log(response.data.message)
		var confirm = $mdDialog.alert()
                .title(response.data.message)
                .ok('Ok')
        
                $mdDialog.show(confirm).then(function() {});
            }
        })
    }

             $scope.conDash = function(){
                 $location.path('/conveyorNet')
             }
});
