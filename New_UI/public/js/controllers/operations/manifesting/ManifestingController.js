
app.controller('ManifestingController', function ($scope, $rootScope, $location, $window,
                ModalService, PickListService, PackListService, CartonService, TransporterService, PrintingService, loginService)
{
	 var newCartonSize

	 /*=========== Functions ========================*/

	 $scope.GetStation = function()
	 {
		 $scope.noStation=false
		 loginService.getStationDetail()
		 .then(function (response)
		 {
		   if(response.status == 200)
			 {
				 var StationData = response.data;

				 if($scope.SetViewState(StationData, true))
				 {
					 $scope.signedInStation = StationData.stationNumber
					 $scope.UpdateLoggedInUser(StationData.user);
					 $scope.EvaluateToteAtStation(StationData);
				 }
			 }
		 })
	 } /* GetStation */

	 $scope.EvaluateToteAtStation = function(StationData)
	 {
		 var ToteAtStation = StationData.currentTote

		 if(ToteAtStation)
		 {
			 $scope.ValidateToteAtStation(ToteAtStation)
		 }
		 else
		 {
			 $scope.NoToteAtStation();
		 }
	 } /* EvaluateToteAtStation */

	 $scope.NoToteAtStation = function()
	 {
		 let ToteProcess = document.getElementById('ToteProcess');
		 ToteProcess.style.display = '';
		 let Station = document.getElementById('Station');
		 Station.style.display = '';

		 let Input = document.getElementById('scannedTote');
		 Input.value = '';
		 Input.focus();
	 } /* NoToteAtStation */

	 $scope.NoStation = function()
	 {
		 let ToteProcess = document.getElementById('ToteProcess');
		 ToteProcess.style.display = '';
		 let NoStation = document.getElementById('NoStation');
		 NoStation.style.display = '';
	 } /* NoStation */

	 $scope.InitDisplayView = function()
	 {
		 let View = document.getElementById('PackProcess');
		 View.style.display = '';

		 View = document.getElementById('orderScanContent');
		 View.style.display = '';

		 View = document.getElementById('viewScannedSerialsModal');
		 View.style.display = 'none';

		 View = document.getElementById('changeCartonSizeScreen');
		 View.style.display = 'none';
	 } /* InitDisplayView */

	 $scope.SetDisplay = function(Data)
	 {
		 $scope.InitDisplayView();

		 $scope.signedInStation = Data.signedInStation;
		 $scope.toteTypeDetailName = Data.toteTypeDetailName;
		 $scope.scannedTote = Data.scannedTote;
		 $scope.useCartonSize =	Data.useCartonSize;
		 $scope.scannedCartonIdDisplay = Data.scannedCartonIdDisplay;
		 $scope.cartonSizeUsed = Data.cartonSizeUsed;
		 $scope.currentOrderId = Data.currentOrderId;
		 $scope.currentOrderInToteProcess = Data.currentOrderInToteProcess;
		 $scope.totalOrdersInToteToProcess = Data.totalOrdersInToteToProcess;
		 $scope.itemsToDisplay = Data.itemsToDisplay;

		 if(Data.Next == 'BARCODE/SERIAL')
		 {
			 let View = document.getElementById('scanBarcode');
			 View.style.display = '';

			 View = document.getElementById('toteDetailHeading');
			 View.style.display = '';

			 View = document.getElementById('cartonToUseHeading');
			 View.style.display = '';

			 let btnReset = document.getElementById('btnReset');
			 btnReset.style.display = '';

			 let btnCartonSize = document.getElementById('btnCartonSize');
			 btnCartonSize.style.display = '';

			 let btnScannedSerials = document.getElementById('btnScannedSerials');
			 btnScannedSerials.style.display = '';

			 var Input = document.getElementById('barcodeScan');
			 Input.value = '';
			 Input.focus();
		 }
		 else if(Data.Next == 'CARTON')
		 {
			 let View = document.getElementById('scanCarton');
			 View.style.display = '';

			 View = document.getElementById('toteDetailHeading');
			 View.style.display = '';

			 View = document.getElementById('cartonToUseHeading');
			 View.style.display = '';

			 let btnReset = document.getElementById('btnReset');
			 btnReset.style.display = '';

			 let btnCartonSize = document.getElementById('btnCartonSize');
			 btnCartonSize.style.display = '';

			 let btnScannedSerials = document.getElementById('btnScannedSerials');
			 btnScannedSerials.style.display = '';

			 var Input = document.getElementById('cartonScan');
			 Input.value = '';
			 Input.focus();
		 }
		 else if(Data.Next == 'TRANS')
		 {
			 let scanTransporter = document.getElementById('scanTransporter');
			 scanTransporter.style.display = '';

			 let scannedCatonIdHeading = document.getElementById('scannedCatonIdHeading');
			 scannedCatonIdHeading.style.display = '';

			 let cartonSizeHeading = document.getElementById('cartonSizeHeading');
			 cartonSizeHeading.style.display = '';

			 let btnRePrint = document.getElementById('btnRePrint');
			 btnRePrint.style.display = '';

			 let btnScannedSerials = document.getElementById('btnScannedSerials');
			 btnScannedSerials.style.display = '';

			 var Input = document.getElementById('transporterScan');
			 Input.value = '';
			 Input.focus();
		 }
		 else if(Data.Next == 'DOC')
		 {
			 let scanDocument = document.getElementById('scanDocument');
			 scanDocument.style.display = '';

			 let scannedCatonIdHeading = document.getElementById('scannedCatonIdHeading');
			 scannedCatonIdHeading.style.display = '';

			 let cartonSizeHeading = document.getElementById('cartonSizeHeading');
			 cartonSizeHeading.style.display = '';

			 let btnRePrint = document.getElementById('btnRePrint');
			 btnRePrint.style.display = '';

			 let btnScannedSerials = document.getElementById('btnScannedSerials');
			 btnScannedSerials.style.display = '';

			 var Input = document.getElementById('documentScan');
			 Input.value = '';
			 Input.focus();
		 }
	 } /* SetDisplay */

	 $scope.ValidateToteAtStation = function(Tote)
	 {
		 var Input = document.getElementById('scannedTote')
		 PickListService.getPickList(Tote)
		 .then(function (doc)
		 {
			 if(doc.status == 200)
			 {
				 var Data = doc.data;

				 if($scope.SetViewState(Data, true))
				   $scope.SetDisplay(Data);
			 }
			 else
			 {
				 window.alert(doc.data);
				 Input.value = '';
				 Input.focus();
			 }
		 })
	 } /* ValidateToteAtStation */

	 $scope.InitPage = function()
	 {
		 let View = document.getElementById('ToteProcess');
		 View.style.display = 'none';

		 View = document.getElementById('Station');
		 View.style.display = 'none';
		 View = document.getElementById('NoStation');
		 View.style.display = 'none';

		 View = document.getElementById('PackProcess');
		 View.style.display = 'none';

		 View = document.getElementById('orderScanContent');
		 View.style.display = 'none';
		 View = document.getElementById('toteDetailHeading');
		 View.style.display = 'none';
		 View = document.getElementById('cartonToUseHeading');
		 View.style.display = 'none';
		 View = document.getElementById('scannedCatonIdHeading');
		 View.style.display = 'none';
		 View = document.getElementById('cartonSizeHeading');
		 View.style.display = 'none';

		 View = document.getElementById('scanBarcode');
		 View.style.display = 'none';
		 View = document.getElementById('scanCarton');
		 View.style.display = 'none';
		 View = document.getElementById('scanDocument');
		 View.style.display = 'none';
		 View = document.getElementById('scanTransporter');
		 View.style.display = 'none';
		 View = document.getElementById('reScanTote');
		 View.style.display = 'none';

		 View = document.getElementById('btnReset');
		 View.style.display = 'none';
		 View = document.getElementById('btnRePrint');
		 View.style.display = 'none';
		 View = document.getElementById('btnCartonSize');
		 View.style.display = 'none';
		 View = document.getElementById('btnScannedSerials');
		 View.style.display = 'none';

		 View = document.getElementById('changeCartonSizeScreen');
		 View.style.display = 'none';

		 View = document.getElementById('viewScannedSerialsModal');
		 View.style.display = 'none';
	 } /* ValidateToteAtStation */

	 $scope.SetViewState = function(Data, Init)
	 {
		 if(Data.ToteMode == 'PUT'){
			 $location.path('/putWallManifesting');
			 return false;
		 }

		 if(Init)
		   $scope.InitPage();

		 if(Data.NoUser)
		 {
			 $location.path('/');
			 return false;
		 }

		 if(Data.NoStation)
		 {
       $scope.NoStation();
			 return false;
		 }

		 if(Data.ScanMode == false)
		 {
       $scope.NoToteAtStation();
			 return false;
		 }

		 return true;
	 } /* SetViewState */

	 $scope.UpdateLoggedInUser = function(user)
	 {
		 loggedInUser.innerHTML = '<i class="fa fa-user fa-fw"></i>' + user

	 } /* UpdateLoggedInUser */

	 $scope.ShowConfirmMsgBox = function(Msg)
	 {
		 var result = true
		 while(result)
		 {
			 result = window.confirm(Msg + ' - Press cancel to clear');
		 }
	 } /* ShowConfirmMsgBox */

   /*=========== Functions End ====================*/

   /*=========== Input ============================*/

   $scope.getToteId = function (toteId)
   {
     $scope.ValidateToteAtStation(toteId);
	 } /* getToteId */

	 $scope.getScannedBarcode = function (scannedCodeId)
	 {
		 var Input = document.getElementById('barcodeScan')

		 PickListService.getSerials(scannedCodeId)
		 .then(function (responseData)
		 {
			 if(responseData.status == 200)
			 {
				 Input.value = '';
				 var Data = responseData.data;

				 if($scope.useCartonSize == 1 && Data.Next == 'CARTON')
				 {
					console.log('The Tote ID: ' + $scope.scannedTote)
					PickListService.printAutobag($scope.scannedTote ).then(function(respData) {

						console.log(respData.data.data)

						if($scope.SetViewState(Data, true))
						{
							$scope.SetDisplay(Data);
						}
					})
				 } else {
					if($scope.SetViewState(Data, true)) {
					   $scope.SetDisplay(Data);
					}
				 }
			 }
			 else
			 {
         		 $scope.ShowConfirmMsgBox(responseData.data);
				 Input.value = '';
				 Input.focus();
			 }
		 });
	 } /* getScannedBarcode */


   	 $scope.getScannedBarcode_old = function (scannedCodeId)
	 {
		 var Input = document.getElementById('barcodeScan')

		 PickListService.getSerials(scannedCodeId)
		 .then(function (responseData)
		 {
			 if(responseData.status == 200)
			 {
				 Input.value = '';
				 var Data = responseData.data;

				console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ | ' + Data.Next)

				if($scope.useCartonSize == 1 && Data.Next == 'CARTON'){
					PickListService.getBagCartonID().then(function(respData) {
						$rootScope.bagCartonID = respData.bagCartonID
						console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ | ' + respData.bagCartonID + ' | ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
						if($scope.SetViewState(Data, true))
						{
							$scope.SetDisplay(Data);
						}
					})
				} else {
				   if($scope.SetViewState(Data, true))
                                   {
                                      $scope.SetDisplay(Data);
                                   }
				}
			 }
			 else
			 {
         			$scope.ShowConfirmMsgBox(responseData.data);
				 Input.value = '';
				 Input.focus();
			 }
		 });
	 } /* getScannedBarcode */

	 $scope.getScannedCarton = function (scannedCartonId)
	 {
		console.log('~~~~~~~~~~~~~~ | ' + $scope.useCartonSize + ' | ' + $rootScope.bagCartonID)

		 var Input = document.getElementById('cartonScan')
		 var ScannedPrefix = parseInt(scannedCartonId[0]);
		 var ScannedBarcodeLength = scannedCartonId.length;

		if(scannedCartonId.substr(0,1) == 'N') {
			 var bagNumber = parseInt(scannedCartonId.substr(3,1))
			 scannedCartonId = scannedCartonId.substr(3)
			 ScannedPrefix = bagNumber

			 ScannedBarcodeLength = scannedCartonId.length

			console.log(scannedCartonId + ' ¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬ ' + ScannedPrefix + ' :: ' + ScannedBarcodeLength)
		 }

	   if(parseInt($scope.useCartonSize) === ScannedPrefix)
	   {
			 if(ScannedBarcodeLength === 8)
			 {
				 PickListService.updateScannedCarton(scannedCartonId)
				 .then(function (doc)
				 {
					 if(doc.status == 200)
					 {
						 var Data = doc.data;

						 if($scope.SetViewState(Data, true))
						 {
							 $scope.SetDisplay(Data);
						 }
					 }
					 else
					 {
             $scope.ShowConfirmMsgBox(doc.data);
						 Input.value = '';
						 Input.focus();
					 }
				 });
			 }
			 else
			 {
				 $scope.ShowConfirmMsgBox('invalid length on scanned barcode');
				 Input.value = '';
				 Input.focus();
			 }
		 }
		 else
		 {
			 $scope.ShowConfirmMsgBox('Scanned carton prefix does not match the carton size selected');
			 Input.value = '';
			 Input.focus();
		 }
	 } /* getScannedCarton */

	 $scope.getScannedDocument = function (scannedDocument)
	 {
		 var Input = document.getElementById('documentScan')
		 if($scope.currentOrderId == scannedDocument)
		 {
			 PickListService.updateVerifyScannedDoc(scannedDocument)
			 .then(function (doc)
			 {
				 if(doc.status == 200)
				 {
					 var Data = doc.data;

					 if($scope.SetViewState(Data, true))
					 {
						 $scope.SetDisplay(Data);
					 }
				 }
				 else
				 {
					 $scope.ShowConfirmMsgBox(doc.data);
					 Input.value = '';
					 Input.focus();
				 }
			 });
		 }
		 else
		 {
			 $scope.ShowConfirmMsgBox('Scanned Document barcode is not valid for this order');
			 Input.value = '';
			 Input.focus();
		 }
	 } /* getScannedDocument */

	 $scope.getScannedTransporter = function (scannedTransporterId)
	 {
		 var Input = document.getElementById('transporterScan')
		 var Prefix = parseInt(scannedTransporterId[0]);
		 var len = scannedTransporterId.length;
		 if(Prefix == 7)
		 {
			 if(len == 8)
			 {
				 PickListService.updateScannedTransporter(scannedTransporterId)
				 .then(function (doc)
				 {
					 if(doc.status == 200)
					 {
						 var Data = doc.data;

						 if($scope.SetViewState(Data, true))
						 {
							 $scope.SetDisplay(Data);
						 }
					 }
					 else
					 {
						 $scope.ShowConfirmMsgBox(doc.data);
						 Input.value = '';
						 Input.focus();
					 }
				 });
			 }
			 else
			 {
				 $scope.ShowConfirmMsgBox('Invalid barcode length for scanned Transporter');
				 Input.value = '';
				 Input.focus();
			 }
		 }
		 else
		 {
			 $scope.ShowConfirmMsgBox('Invalid barcode prefix for scanned Transporter');
			 Input.value = '';
			 Input.focus();
		 }
	 } /* getScannedTransporter */

	 /*=========== Input End ========================*/

	 /*=========== Buttons ==========================*/

	 $scope.showChangeCarton = function (cartonTote)
	 {
		console.log('____________________ | ' + cartonTote)
		 PickListService.getCartons(cartonTote)
		 .then(function (doc)
		 {
			 if(doc.status == 200)
			 {
				 var Data = doc.data;

				 if($scope.SetViewState(Data, false))
				 {
					 let orderScanContent = document.getElementById('orderScanContent');
					 orderScanContent.style.display = 'none';

					 let changeCartonSizeScreen = document.getElementById('changeCartonSizeScreen');
					 changeCartonSizeScreen.style.display = '';

					 $scope.listOfCartonSizes = Data;
				 }
			 }
		 });
	 } /* showChangeCarton */

   $scope.closeChangeCartonModal = function ()
   {
		 let PackProcess = document.getElementById('orderScanContent');
		 PackProcess.style.display = '';

		 let changeCartonSizeScreen = document.getElementById('changeCartonSizeScreen');
		 changeCartonSizeScreen.style.display = 'none';

   } /* closeChangeCartonModal */

   $scope.confirmCartonSize = function ()
   {
		 PickListService.updateCartonSize(newCartonSize)
		 .then(function (response)
		 {
			 if(response.status == 200)
			 {
				 var Data = response.data;

				 if($scope.SetViewState(Data, false))
				 {
					 let PackProcess = document.getElementById('orderScanContent');
					 PackProcess.style.display = '';

					 let changeCartonSizeScreen = document.getElementById('changeCartonSizeScreen');
					 changeCartonSizeScreen.style.display = 'none';

					 $scope.SetDisplay(Data);
				 }
			 }
			 else
			 {
				 window.alert(response.data);
			 }
		 });
   } /* confirmCartonSize */

   $scope.selectedCartonSizeChanged = function (selectedCartonSize)
   {
     newCartonSize = selectedCartonSize
   } /* selectedCartonSizeChanged */

   $scope.showScannedSerials = function ()
   {
		 $scope.scannedSerialItems = [];

		 let PackProcess = document.getElementById('orderScanContent');
		 PackProcess.style.display = 'none';
		 let viewScannedSerialsModal = document.getElementById('viewScannedSerialsModal');
		 viewScannedSerialsModal.style.display = '';

		 for (var j = 0; j < $scope.itemsToDisplay.length; j++)
		 {
			 var k = 0;
			 while(k < $scope.itemsToDisplay[j].SkuSerials.length)
			 {
				 let serialisedItem = {}
				 serialisedItem.prodCode = $scope.itemsToDisplay[j].itemCode;
				 serialisedItem.serial = $scope.itemsToDisplay[j].SkuSerials[k];
				 $scope.scannedSerialItems.push(serialisedItem);
				 k++;
			 }
		 }
   } /* showScannedSerials */

   $scope.closeViewSerialsModal = function ()
   {
		 let PackProcess = document.getElementById('orderScanContent');
		 PackProcess.style.display = '';
		 let viewScannedSerialsModal = document.getElementById('viewScannedSerialsModal');
		 viewScannedSerialsModal.style.display = 'none';
   } /* closeViewSerialsModal */

   $scope.reprintCarton = function ()
   {
		 PickListService.reprintCarton()
		 .then(function (response)
		 {
			 if(response.status == 200)
			 {
				 var Data = response.data;

				 if($scope.SetViewState(Data, true))
				 {
					 $scope.SetDisplay(Data);
				 }
			 }
			 else
			 {
				 window.alert(response.data);
			 }
		 });
	 } /* reprintCarton */

	 $scope.resetCarton = function ()
	 {
		 PickListService.resetStation()
		 .then(function (response)
		 {
			 if(response.status == 200)
			 {
				 var Input = document.getElementById('scannedTote')
				 var Data = response.data;

         $scope.SetViewState(Data, true);
				 Input.value = '';
			 }
			 else
			 {
				 window.alert(response.data);
			 }
		 });
	 } /* resetCarton */

	 /*=========== Buttons End ======================*/

  // $scope.GetStation();
      $scope.RedirectToURL = function() {
           var searchObject = $location.search();
           /*var host = $window.location.host;*/
           var host = $window.location.origin;
           var IpArr1 = host.split('://');
           var IpArr = IpArr1[1].split(':');
           var landingUrl = "http://"+ IpArr[0] +":3500/manifesting/";
           $window.location.href = landingUrl;
      };

       $scope.RedirectToURL();  
});
