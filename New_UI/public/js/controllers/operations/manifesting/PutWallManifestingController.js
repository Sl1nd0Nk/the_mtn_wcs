
app.controller('PutWallManifestingController', function ($scope, $rootScope, $location, $window, $timeout,
                ModalService, PutWallService, loginService){
	 /*=========== Functions ========================*/

	 $scope.GetStation = function(){
		 $scope.noStation=false
		 loginService.getStationDetail()
		 .then(function (response){
		   if(response.status == 200){
				 var StationData = response.data;

				 if($scope.SetViewState(StationData, true)){
					 $scope.signedInStation = StationData.stationNumber
					 $scope.UpdateLoggedInUser(StationData.user);
					 $scope.EvaluateToteAtStation(StationData);
				 }
			 }
		 })
	 } /* GetStation */

	 $scope.EvaluateToteAtStation = function(StationData){
		 var ToteAtStation = StationData.currentTote

		 if(ToteAtStation){
			 $scope.ValidateToteAtStation(ToteAtStation)
		 } else {
			 $scope.NoToteAtStation();
		 }
	 } /* EvaluateToteAtStation */

	 $scope.NoToteAtStation = function(){
		 let ToteProcess = document.getElementById('ToteProcess');
		 ToteProcess.style.display = '';
		 let Station = document.getElementById('Station');
		 Station.style.display = '';

		 let Input = document.getElementById('scannedTote');
		 Input.value = '';
		 Input.focus();
	 } /* NoToteAtStation */

	 $scope.NoStation = function(){
		 let ToteProcess = document.getElementById('ToteProcess');
		 ToteProcess.style.display = '';
		 let NoStation = document.getElementById('NoStation');
		 NoStation.style.display = '';
	 } /* NoStation */

	 $scope.InitDisplayView = function(){
		 let View = document.getElementById('PackProcess');
		 View.style.display = '';

		 View = document.getElementById('orderScanContent');
		 View.style.display = '';
	 } /* InitDisplayView */

	 $scope.SetDisplay = function(Data){
		 $scope.InitDisplayView();

		 $scope.signedInStation = Data.signedInStation;

		 //$scope.scannedTote = Data.scannedTote;
		 //$scope.currentOrderId = Data.currentOrderId;

		 if(Data.Next == 'BARCODE/SERIAL'){
			 let View = document.getElementById('scanBarcode');
			 View.style.display = '';

			 let btnReset = document.getElementById('btnReset');
			 btnReset.style.display = '';

			 var Input = document.getElementById('barcodeScan');
			 Input.value = '';
			 Input.focus();
		 } else if(Data.Next == 'DOC'){
			 let View = document.getElementById('scanDocument');
			 View.style.display = '';

			 var Input = document.getElementById('documentScan');
			 Input.value = '';
			 Input.focus();
		 } else if(Data.Next == 'PUT_ITEM'){
			 let View = document.getElementById('instruction');
			 View.style.display = '';

			 if(Data.Instruction){
				 $scope.instruction = Data.Instruction;
			 }

			 $scope.CheckIfLocationConfirmed(Data.Location);
		 }
	 } /* SetDisplay */

	 $scope.CheckIfLocationConfirmed = function(Location){
		 PutWallService.checkLocConfirmed(Location)
		 .then(function (doc){
			 if(doc.status == 200)
			 {
				 var Data = doc.data;

				 if(Data.Confirmed){
					 $scope.GetStation();
				 } else {
					 $timeout(function(){
						 $scope.CheckIfLocationConfirmed(Location);
					 }, 500);
				 }
			 } else {
				 window.alert(doc.data);
			 }
		 })
	 } /* CheckIfLocationConfirmed */

	 $scope.ValidateToteAtStation = function(Tote){
		 var Input = document.getElementById('scannedTote')
		 PutWallService.getPickList(Tote)
		 .then(function (doc){
			 if(doc.status == 200)
			 {
				 var Data = doc.data;

				 if($scope.SetViewState(Data, true))
				   $scope.SetDisplay(Data);
			 } else {
				 window.alert(doc.data);
				 Input.value = '';
				 Input.focus();
			 }
		 })
	 } /* ValidateToteAtStation */

	 $scope.InitPage = function(){
		 let View = document.getElementById('ToteProcess');
		 View.style.display = 'none';

		 View = document.getElementById('Station');
		 View.style.display = 'none';
		 View = document.getElementById('NoStation');
		 View.style.display = 'none';

		 View = document.getElementById('PackProcess');
		 View.style.display = 'none';

		 View = document.getElementById('orderScanContent');
		 View.style.display = 'none';

		 View = document.getElementById('btnReset');
		 View.style.display = 'none';

		 View = document.getElementById('scanBarcode');
		 View.style.display = 'none';
		 View = document.getElementById('instruction');
		 View.style.display = 'none';
		 View = document.getElementById('scanDocument');
		 View.style.display = 'none';

	 } /* ValidateToteAtStation */

	 $scope.SetViewState = function(Data, Init){

		 if(Data.ToteMode == 'MAN'){
			 $location.path('/manifesting');
			 return false;
		 }

		 if(Init)
		   $scope.InitPage();

		 if(Data.NoUser){
			 $location.path('/');
			 return false;
		 }

		 if(Data.NoStation){
       $scope.NoStation();
			 return false;
		 }

		 if(Data.ScanMode == false){
       $scope.NoToteAtStation();
			 return false;
		 }

		 return true;
	 } /* SetViewState */

	 $scope.UpdateLoggedInUser = function(user){
		 loggedInUser.innerHTML = '<i class="fa fa-user fa-fw"></i>' + user

	 } /* UpdateLoggedInUser */

	 $scope.ShowConfirmMsgBox = function(Msg){
		 var result = true
		 while(result)
		 {
			 result = window.confirm(Msg + ' - Press cancel to clear');
		 }
	 } /* ShowConfirmMsgBox */

   /*=========== Functions End ====================*/

   /*=========== Input ============================*/

   $scope.getToteId = function (toteId){
     $scope.ValidateToteAtStation(toteId);
	 } /* getToteId */

   $scope.getScannedBarcode = function (scannedCodeId){
		 var Input = document.getElementById('barcodeScan')

		 PutWallService.getSerials(scannedCodeId)
		 .then(function (responseData){
			 if(responseData.status == 200){
				 Input.value = '';
				 var Data = responseData.data;

         if($scope.SetViewState(Data, true)){
				   $scope.SetDisplay(Data);
				 }
			 } else {
         $scope.ShowConfirmMsgBox(responseData.data);
				 Input.value = '';
				 Input.focus();
			 }
		 });
	 } /* getScannedBarcode */

	 $scope.getScannedDocument = function (scannedDocument){
		 var Input = document.getElementById('documentScan')
		 //if($scope.currentOrderId == scannedDocument){
			 PutWallService.updateVerifyScannedDoc(scannedDocument)
			 .then(function (doc){
				 if(doc.status == 200){
					 var Data = doc.data;

					 if($scope.SetViewState(Data, true)){
						 $scope.SetDisplay(Data);
					 }
				 } else {
					 $scope.ShowConfirmMsgBox(doc.data);
					 Input.value = '';
					 Input.focus();
				 }
			 });
		 //} else {
		//	 $scope.ShowConfirmMsgBox('Scanned Document barcode is not valid for this order');
		//	 Input.value = '';
		//	 Input.focus();
		// }
	 } /* getScannedDocument */

	 /*=========== Input End ========================*/

	 /*=========== Buttons ========================*/

   $scope.reprintCarton = function (){
		 PutWallService.reprintCarton()
		 .then(function (response){
			 if(response.status == 200)
			 {
				 var Data = response.data;

				 if($scope.SetViewState(Data, true)){
					 $scope.SetDisplay(Data);
				 }
			 } else {
				 window.alert(response.data);
			 }
		 });
	 } /* reprintCarton */

	 $scope.resetCarton = function (){
		 PutWallService.resetStation()
		 .then(function (response){
			 if(response.status == 200){
				 var Input = document.getElementById('scannedTote')
				 var Data = response.data;

         $scope.SetViewState(Data, true);
				 Input.value = '';
			 } else {
				 window.alert(response.data);
			 }
		 });
	 } /* resetCarton */

	 /*=========== Buttons End ======================*/

//   $scope.GetStation();
    $scope.RedirectToURL = function() {
           var searchObject = $location.search();
           
           var host = $window.location.origin;
           var IpArr1 = host.split('://');
           var IpArr = IpArr1[1].split(':');
           var landingUrl = "http://"+ IpArr[0] +":3500/manifesting/";
           $window.location.href = landingUrl;
      };

       $scope.RedirectToURL();
});
