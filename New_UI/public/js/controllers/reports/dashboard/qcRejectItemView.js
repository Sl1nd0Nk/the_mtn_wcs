app.controller('QCRejectedItemViewController', function ($scope, $location, $http, $filter, $routeParams, toteInfoService) {
    $scope.tableHeader = {
        "Cartons"       :"Carton ID"
    }

    $scope.header = 'QC REJECTED TRANSPORTER ITEMS ' + $routeParams.Todo

    $http.get('/getLoggedInUser').then(function(response) {
        if(response.data.code == '00') {
            $http.get('/getPicklistItem/' + $routeParams.Todo).then(function(response){
                if(response.data.code){
                    
                    let dataStoreArr = []
        
                    var x = 0
                    while(x < response.data.data.length){
                        var y = 0
                        while(y < response.data.data[x].orders.length){
                            dataStoreArr.push(response.data.data[x].orders[y])
                            y++
                        }
                        x++
                    }
        
                    $scope.zoneParamDisplay = dataStoreArr
                }else{
                    console.log('DATA NOT FOUND!!!')
                }
           })
        
           $scope.displayItems = function(cartonID){
                toteInfoService.setToteInfo(cartonID)
                $location.path('/toteInfo')
           }
        } else {
            var confirm = $mdDialog.alert()
            .title('The user session was not found. Please log in before requesting the page.')
            .ok('Ok')

            $mdDialog.show(confirm).then(function() {
                $location.path('/')
            }, function() {});
        } 
    })
})




