app.controller('alarmController', function ($scope, $location, $http, $rootScope, $mdDialog, $timeout, $filter, toteInfoService) {
    $http.get('/getLoggedInUser').then(function(response) {
        if(response.data.code == '00') {
            $scope.header = 'ALARM AND EVENT VIEWER'
            $scope.commError = []
            $scope.rejectedQCError = []
            $scope.notification = 5
            
            $rootScope.notification = 0

            //========================Alarm And Event Controller!
            $scope.zoneParamDisplay = [];
                    
            $scope.zoneParamDisplay = [{commm: "Communications issues between WCS and a sub-system is not operational.", isDisabled: false}, 
            {commm:"Conveyor Alarms such as an emergency stop or system jam", isDisabled: false}, {commm:"Reject Station - Picking and Weight Scale fails.", isDisabled: false}, 
            {commm:"Courier weight check failure.", isDisabled: false}, {commm:"System errors such as disk capacity, process falure", isDisabled: false}, {commm:"Failed user logins.", isDisabled: false},
            {commm:"Picker not logging in.", isDisabled: false}, {commm:"Picker not releasing tote at Pick station.", isDisabled: false}, 
            {commm:"Packer not packing tote within x time.", isDisabled: false}, {commm:"Put-Wall Operator not completed tote in x-time", isDisabled: false},
            {commm:"Auto-bagger operator not completing shelf in x time.", isDisabled: false}]

            // ===================================================== SUB-SYSTEMS
            $scope.initiate = function(){
                $http.get('/subSystemComs').then(function(response){
                    if(response.data.code == '00'){
                        $scope.hideValue = true
                        if(response.data.data.length > 0)
                            $scope.zoneParamDisplay[0].isDisabled = true
                        else
                            $scope.zoneParamDisplay[0].isDisabled = false

                        $scope.commError = response.data.data
                    }
                })
            }

            // ===================================================== REJECTED QC
            $scope.initRejectQC = function(){
                $http.get('/getRejectedQC').then(function(response){
                    if(response.data.code == '00'){
                        $scope.hideRejectedQC = true
                        if(response.data.data.length > 0)
                            $scope.zoneParamDisplay[2].isDisabled = true
                        else
                            $scope.zoneParamDisplay[2].isDisabled = false

                        $scope.rejectedQCError = response.data.data
                    }
                })
            }

            // ===================================================== FAILED LOGIN
            $scope.failedLogin = function(){
                $http.get('/getFailedLogin').then(function(response){
                    if(response.data.code == '00'){
                        $scope.hideFailedLogin = true
                        if(response.data.data.length > 0)
                            $scope.zoneParamDisplay[5].isDisabled = true
                        else
                            $scope.zoneParamDisplay[5].isDisabled = false

                        $scope.failedLogin = response.data.data
                    }
                })
            }

            // ===================================================== COURIER WEIGHT FAIL
            $scope.weightFail = function(){
                $http.get('/getCourierWeightFail').then(function(response){
                    if(response.data.code == '00'){
                        console.log(response.data.data.length)
                        $scope.hideWeightCheck = true
                        if(response.data.data.length > 0)
                            $scope.zoneParamDisplay[3].isDisabled = true
                        else
                            $scope.zoneParamDisplay[3].isDisabled = false

                        $scope.failedCourierWeight = response.data.data
                    }
                })
            }

            // ===================================================== PICKER NOT LOGGED IN
            $scope.pickerNotLogged = function(){
                $http.get('/getPickerNotLogged').then(function(response){
                    if(response.data.code == '00'){
                        console.log(response.data.data.length)
                        $scope.hidePTLNotLoggedIn = true
                        if(response.data.data.length > 0)
                            $scope.zoneParamDisplay[6].isDisabled = true
                        else
                            $scope.zoneParamDisplay[6].isDisabled = false

                        $scope.PTLNotLogged = response.data.data
                    }
                })
            }

            // ===================================================== PICKER NOT RELEASED
            $scope.pickerNotReleasing = function(){
                $http.get('/getPickerNotReleasing').then(function(response){
                    if(response.data.code == '00'){
                        console.log(response.data.data.length)
                        $scope.hidePickerNotReleasing = true
                        if(response.data.data.length > 0)
                            $scope.zoneParamDisplay[7].isDisabled = true
                        else
                            $scope.zoneParamDisplay[7].isDisabled = false

                        $scope.PickerNotReleasing = response.data.data
                    }
                })
            }

            // ===================================================== PACKER NOT PACKIN IN X AMOUNT OF TIME
            // $scope.packerNotPacking = function(){
            //     $http.get('/getPackerNotPacking').then(function(response){
            //         if(response.data.code == '00'){
            //             console.log(response.data.data.length)
            //             $scope.hidePickerNotPacking = true
            //             if(response.data.data.length > 0)
            //                 $scope.zoneParamDisplay[8].isDisabled = true
            //             else
            //                 $scope.zoneParamDisplay[8].isDisabled = false

            //             $scope.PickerNotPacking = response.data.data
            //         }
            //     })
            // }

            // ===================================================== FUNCTIONS
            $scope.displayError = function(errMsg){
                if($scope.zoneParamDisplay[0].commm == errMsg){
                    if($scope.hideValue){
                        $scope.hideValue = false

                        $scope.performUpdate('updateStemComms', 0)
                    }else{
                        $scope.hideValue = true
                        $scope.initiate()
                    }
                }else if($scope.zoneParamDisplay[2].commm == errMsg){
                    if($scope.hideRejectedQC){
                        $scope.hideRejectedQC = false

                        $scope.performUpdate('updateQCRejectError', 2)
                    }else{
                        $scope.hideRejectedQC = true
                        $scope.initRejectQC()
                    }
                }else if($scope.zoneParamDisplay[5].commm == errMsg){
                    if($scope.hideFailedLogin){
                        $scope.hideFailedLogin = false
                        
                        $scope.performUpdate('updateFailed', 5)
                    }else{
                        $scope.hideFailedLogin = true
                        $scope.zoneParamDisplay[5].isDisabled = true
                        // $scope.initRejectQC()
                    }
                }else if($scope.zoneParamDisplay[3].commm == errMsg){
                    if($scope.hideWeightCheck){
                        $scope.hideWeightCheck = false
                        
                        $scope.performUpdate('updateCourierWeight', 3)
                    }else{
                        $scope.hideWeightCheck = true
                        $scope.weightFail()
                    }
                }else if($scope.zoneParamDisplay[6].commm == errMsg){
                    if($scope.hidePTLNotLoggedIn){
                        $scope.hidePTLNotLoggedIn = false
                        
                        // $scope.performUpdate('updateCourierWeight', 6)
                    }else{
                        $scope.hidePTLNotLoggedIn = true
                        $scope.pickerNotLogged()
                    }
                }else if($scope.zoneParamDisplay[7].commm == errMsg){
                    if($scope.hidePickerNotReleasing){
                        $scope.hidePickerNotReleasing = false
                        
                        // $scope.performUpdate('updateCourierWeight', 6)
                    }else{
                        $scope.hidePickerNotReleasing = true
                        $scope.pickerNotReleasing()
                    }
                }
                else if($scope.zoneParamDisplay[7].commm == errMsg){
                    if($scope.hidePickerNotPacking){
                        $scope.hidePickerNotPacking = false
                        
                        // $scope.performUpdate('updateCourierWeight', 6)
                    }else{
                        $scope.hidePickerNotPacking = true
                        $scope.packerNotPacking()
                    }
                }
            }

            $scope.performUpdate = function(endpoint, errorIndex){
                $http.post('/'+endpoint).then(function(response){
                    if(response.data.code == '00')
                        $scope.zoneParamDisplay[errorIndex].isDisabled = false
                    else
                        $scope.zoneParamDisplay[errorIndex].isDisabled = true
                })
            }

            $scope.initiate()
            $scope.initRejectQC()
            $scope.failedLogin()
            $scope.weightFail()
            $scope.pickerNotLogged()
            $scope.pickerNotReleasing()
            // $scope.packerNotPacking()
        } else {
            var confirm = $mdDialog.alert()
            .title('The user session was not found. Please log in before requesting the page.')
            .ok('Ok')

            $mdDialog.show(confirm).then(function() {
                $location.path('/')
            }, function() {});
        }
    })
});
