app.controller('QCRejectedViewController', function ($scope, $location, $http, toteInfoService, $filter) {
    $scope.tableHeader = {
		"CartonID"               : "Carton ID",
        "TransporterID"          : "Transporter ID",
        "DateRejected"           : "Date",
        //"CartonCalculatedWeight" : "Calculated Carton Weight",
        "ExpectedMinWeight"      : "Expected Minimum Weight",
        "ActualScaleWeight"      : "Actual Scale Weight",
        "ExpectedMaxWeight"      : "Expected Maximum Weight",
        "QcScaleUsed"            : "QC Scale",
        "RejectReason"           : "Reject Reason"
    }

    $scope.header = 'QC REJECT VIEW'
    $scope.paramHeader = 'QC REJECT VIEW'

    var count = 0
    var dataStore = []

    $scope.$watch('myDate',function(val){
		let dd = null;
        if(val){
			dd = val;
        }else{
			dd= new Date();
        }

		let theDate = $filter('date')(dd, "yyyy-MM-dd").substr(0,10)
		$http.get('/getWeightScaleRejects/' + theDate).then(function(response){
			if(response.data.Data.length > 0){
				$scope.zoneParamDisplay = response.data.Data;
				$scope.total = ' | Total Number of Cartons: ' + response.data.Data.length;
				$scope.header = 'QC REJECT VIEW | ' +  theDate
			}else{
				$scope.zoneParamDisplay = [];
				$scope.total = 0;
				$scope.header = 'QC REJECT VIEW | ' +  theDate;
				console.log('NOT SO HAPPY DAYS')
			}
		})
    })

    $scope.displayItems = function(transporterID, cartonID){
        if(!transporterID && cartonID){
            toteInfoService.setToteInfo(cartonID)
            $location.path('/toteInfo')
        }else{
            // $location.path('/qcRejectItemView/' + transporterID)
            toteInfoService.setToteInfo(cartonID)
            $location.path('/toteInfo')
        }
    }
})




