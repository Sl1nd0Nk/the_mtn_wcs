app.controller('PackingErrViewController', function ($scope, $location, $http, toteInfoService, loginService, $filter) {
    $scope.tableHeader = {
		"Date"  		: "Date",
        "User"    		: "Packer",
        "Station" 		: "Pack Station",
        "Supervisor"	: "Supervisor Cleared",
        "Error"      	: "Error"
    };

    $scope.header = 'PACKER ERROR VIEW';
    $scope.paramHeader = 'PACKER ERROR VIEW';

    var count = 0;
    var dataStore = [];

    $scope.ShowScreen = function(){
		$scope.$watch('myDate',function(val){
			let dd = null;
			if(val){
				dd = val;
			}else{
				dd= new Date();
			}

			let theDate = $filter('date')(dd, "yyyy-MM-dd").substr(0,10);
			$http.get('/getPackingErrors/' + theDate).then(function(response){
				if(response.data.Data.length > 0){
					$scope.zoneParamDisplay = response.data.Data;
					$scope.total = ' | Total Number of Errors: ' + response.data.Data.length;
					$scope.header = 'PACKER ERROR VIEW | ' +  theDate;
				}else{
					$scope.zoneParamDisplay = [];
					$scope.total = 0;
					$scope.header = 'PACKER ERROR VIEW | ' +  theDate;
					console.log('NOT SO HAPPY DAYS');
				}
			});
		});
	} /* ShowScreen */

	$scope.Permissions = function(){
		loginService.getLoggedInUser().then(function(response){
			if(response.status == 200){
				let UserData = response.data;

				if($scope.SetViewState(UserData, true)){
					$scope.UpdateLoggedInUser(UserData.user);
					$scope.NotAuth = false;
					$scope.ShowScreen();
				}
			}
		});
	} /* Permissions */

	$scope.SetViewState = function(Data, Init){
		if(Init)
			$scope.InitPage();

		if(Data.NoUser){
			$location.path('/');
			return false;
		}

		if(!Data.roleID || Data.roleID <= 1){
			$scope.UnAuthorised();
			return false;
		}

		return true;
	} /* SetViewState */

	$scope.InitPage = function(){
		$scope.NotAuth = false;
	} /* InitPage */

	$scope.UnAuthorised = function(){
		$scope.InitPage();
		$scope.NotAuth = true;
		return false;
	} /* UnAutherised */

	$scope.UpdateLoggedInUser = function(user){
		loggedInUser.innerHTML = '<i class="fa fa-user fa-fw"></i>' + user;
	} /* UpdateLoggedInUser */

    $scope.Permissions();
});




