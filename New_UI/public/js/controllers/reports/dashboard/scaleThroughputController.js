app.controller('scaleThroughputController', function ($scope, $location, $http, $mdDialog, $timeout, $filter, toteInfoService) {
    $scope.header = 'SCALE THROUGHPUT DASHBOARD'
    $scope.tableHeader = {
        zoneID:'Chute ID',
        morning0:'06h:00 - 07h:00',
        morning1:'07h:00 - 08h:00',
        morning2:'08h:00 - 09h:00',
        morning3:'09h:00 - 10h:00',
        morning4:'10h:00 - 11h:00',
        morning5:'11h:00 - 12h:00',
        morning6:'12h:00 - 13h:00',
        morning7:'13h:00 - 14h:00',
        morning8:'14h:00 - 15h:00',
        morning9:'15h:00 - 16h:00',
    };

    $http.get('/getLoggedInUser').then(function(response) {
        if( response.data.code == '00') {
            //========================SCALE THROUPUT
            $http.get('/getScaleLog').then(function(resp){
                if(resp.data.code == '00'){
                    $scope.zoneParamDisplay = [];
                    $scope.totes = [];
                    var timeOfDay = [0,0,0,0,0,0,0,0,0];
                    var timeZone = '';
                    var zoneObj = {
                        ZoneID      :'',
                        NumTotes    :0,
                        timeZone    :''
                    }

                    // console.log('_____________________________________________ | '+resp.data.data[0].scaleObjects.length)
                    getZoneOne()

                    function getZoneOne(){
                        for(var x = 0; x < resp.data.data.length; x++){
                            var timeZoneArr = [
                                {timeZone:'one',hourOfDay:6,totals:0},
                                {timeZone:'two',hourOfDay:7,totals:0}, {timeZone:'three',hourOfDay:8,totals:0}, 
                                {timeZone:'four',hourOfDay:9,totals:0}, {timeZone:'five',hourOfDay:10,totals:0}, 
                                {timeZone:'six',hourOfDay:11,totals:0}, {timeZone:'seven',hourOfDay:12,totals:0}, 
                                {timeZone:'eight',hourOfDay:13,totals:0},{timeZone:'nine',hourOfDay:14,totals:0}, 
                                {timeZone:'ten',hourOfDay:15,totals:0}  
                        ]
                            var i = 0
                            while(i < timeZoneArr.length){
                                var y = 0;
                                while(y < resp.data.data[x].scaleObjects.length){
                                    if(resp.data.data[x].scaleObjects[y].timeHours == timeZoneArr[i].hourOfDay){
                                        timeZoneArr[i].totals += 1
                                    }
                                    y++
                                }
                                i++
                                
                            }

                            zoneObj = {}
                            zoneObj.ZoneID = resp.data.data[x].scaleID
                            
                            zoneObj.NumTotes = angular.copy(timeZoneArr) 
                            $scope.totes.push(zoneObj)
                        }

                        $scope.zoneParamDisplay = $scope.totes
                    }
                }else{
                    var confirm = $mdDialog.alert()
                    .title('OOPS! Something went wrong. Please contact support.').ariaLabel('Lucky day').ok('Ok');
                    $mdDialog.show(confirm).then(function() {}, function() {});
                }
            });
        } else {
            var confirm = $mdDialog.alert()
            .title('The user session was not found. Please log in before requesting the page.')
            .ok('Ok')

            $mdDialog.show(confirm).then(function() {
                $location.path('/')
            }, function() {});
        }
    })
});
