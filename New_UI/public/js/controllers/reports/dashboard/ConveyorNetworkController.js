app.controller('ConveyorNetworkController', function ($scope, $location, $http, $mdDialog, $timeout, $filter, toteInfoService) {
    $scope.header = 'CONVEYOR NETWORK DASHBOARD';
    $scope.tableHeader = {
        zoneID:'Zone ID',
        numTote:'Number of Totes',
        totes: 'Totes',
        tID: 'Tote ID',
        reason: 'Reason',
        time: 'Time',
        chuteID: 'ChuteID',
        container: 'Container'
    };

    $scope.chuteDisplay = []

    $http.get('/getLoggedInUser').then(function(response) {
        if(response.data.code == '00') {
            //========================Pick-To-Zone Buffers
            $http.get('/getNetwork').then(function(resp){
                if(resp.data.code == '00'){
                    $scope.zoneParamDisplay = [];
                    $scope.totes = [];
                    $scope.zoneParamDisplay = resp.data.data;

                    $scope.getPTZNumTotes = function(zoneID){
                        var x = 0;
                        $scope.totes = [];
                        while(x < resp.data.data.length){
                            var y = 0;
                            while(y < resp.data.data[x].Totes.length){
                                if(resp.data.data[x].ZoneID == zoneID && !resp.data.data[x].processed){
                                    $scope.totes.push(resp.data.data[x].Totes[y].ToteID);
                                }
                                y++
                            }
                            x++
                        }

                        console.log('Totes: ' + $scope.totes);

                        var confirm = $mdDialog.alert()
                        .title('Totes In Zone ' + zoneID + ': ' + $scope.totes).ariaLabel('Lucky day').ok('Ok');
                        $mdDialog.show(confirm).then(function() {}, function() {});
                    };
                }else{
                    var confirm = $mdDialog.alert()
                    .title('OOPS! Something went wrong. Please contact support.').ariaLabel('Lucky day').ok('Ok');
                    $mdDialog.show(confirm).then(function() {}, function() {});
                }
            });

            //========================Pick-To-Light Reject Spur
            $http.get('/getPtlReject').then(function(resp){
                if(resp.data.code == '00'){
                    $scope.ptlRejectDisplay = [];
                    $scope.ptlRejectDisplay = resp.data.data;

                    $scope.showToteInfo = function(toteID){
                        var confirm = $mdDialog.confirm()
                        .title('Would you like to view Tote: ' + toteID + ' Information').ariaLabel('Lucky day').ok('Yes').cancel('No');
                        $mdDialog.show(confirm).then(function() {
                            toteInfoService.setToteInfo(toteID);
                            $location.path('/toteInfo');
                        }, function() {});
                    }
                }else{
                    var confirm = $mdDialog.alert()
                        .title('OOPS! Something went wrong. Please contact support.').ariaLabel('Lucky day').ok('Ok');
                    $mdDialog.show(confirm).then(function() {}, function() {});
                }
            });

            //========================Packing Chutes
            $http.get('/getTotesInChute').then(function(resp){
                if(resp.data.code == '00'){
                    $scope.chuteDisplay = resp.data.data;

                    $scope.totesInChute = function(schuteID){
	                console.log('____________________________________ SCHUTE ID. ' + schuteID)
                        $http.get('/getActualTotesInSchute/'+schuteID).then(function(schuteResp){
                            if(schuteResp.data.code == '00'){
				console.log('____________________________________ SCHUTE RESPONSE. ' + schuteResp.data.code)
                                var confirm = $mdDialog.alert()
                                .title('Totes in Schute ' + schuteID + ': ' + schuteResp.data.data).ariaLabel('Lucky day').ok('Ok');
                                $mdDialog.show(confirm).then(function() {}, function() {});
                            }else{
			 	console.log('____________________________________ SCHUTE RESPONSE. ' + schuteResp.data.code)
                                var confirm = $mdDialog.alert()
                                .title('OOPS! Something went wrong. Please contact support.').ariaLabel('Lucky day').ok('Ok');
                                $mdDialog.show(confirm).then(function() {}, function() {});
                            }
                        })
                    }
                }else{
                    var confirm = $mdDialog.alert()
                    .title('OOPS! Something went wrong. Please contact support.').ariaLabel('Lucky day').ok('Ok');
                    $mdDialog.show(confirm).then(function() {}, function() {});
                }
            });

            //======================== QC REJECT SPUR
            $http.get('/getQCRejectedTable').then(function(resp){
                if(resp.data.code == '00'){
                    console.log('**************************************************DO WE ACTUALLY MAKE IT HERE?****************************')
                    console.log($filter('date')(resp.data.data[0].dateRejected, 'shortTime'));
                    $scope.qcRejectedDisplay = [];
                    var tempArr = []
                    for(var x = 0; x < resp.data.data.length; x++){
                        resp.data.data[x].dateRejected = $filter('date')(resp.data.data[x].dateRejected, 'shortTime');
                        tempArr.push(resp.data.data[x]);
                    }
                    $scope.qcRejectedDisplay = tempArr;

                    $scope.viewCarton = function(cartonID){
                        if(cartonID){
                            var confirm = $mdDialog.confirm()
                            .title('Would you like to view Carton: ' + cartonID + ' Information').ariaLabel('Lucky day').ok('Yes').cancel('No');
                            $mdDialog.show(confirm).then(function() {
                                toteInfoService.setToteInfo(cartonID);
                                $location.path('/toteInfo');
                            }, function() {});
                        }else{
                            var confirm = $mdDialog.alert()
                            .title('OOPS! Something went wrong. Please contact support.').ariaLabel('Lucky day').ok('Ok');
                            $mdDialog.show(confirm).then(function() {}, function() {});
                        }
                    }
                }else{
                    var confirm = $mdDialog.alert()
                    .title('OOPS! Something went wrong. Please contact support.').ariaLabel('Lucky day').ok('Ok');
                    $mdDialog.show(confirm).then(function() {}, function() {});
                }
            });

            //======================== COURIER REJECT SPUR
            $http.get('/getCourierReject').then(function(resp){
                if(resp.data.code == '00'){
                    console.log($filter('date')(resp.data.data[0].Time, 'shortTime'));
                    $scope.courierRejectedDisplay = [];
                    var tempArr = []
                    for(var x = 0; x < resp.data.data.length; x++){
                        resp.data.data[x].Time = $filter('date')(resp.data.data[x].Time, 'shortTime');
                        tempArr.push(resp.data.data[x]);
                    }
                    $scope.courierRejectedDisplay = tempArr;

                    $scope.getCourierCarton = function(cartonID){
                        if(cartonID){
                            var confirm = $mdDialog.confirm()
                                .title('Would you like to view Carton: ' + cartonID + ' Information').ariaLabel('Lucky day').ok('Yes').cancel('No');
                            $mdDialog.show(confirm).then(function() {
                                toteInfoService.setToteInfo(cartonID);
                                $location.path('/toteInfo');
                            }, function() {});
                        }else{
                            var confirm = $mdDialog.alert()
                            .title('OOPS! Something went wrong. Please contact support.').ariaLabel('Lucky day').ok('Ok');
                            $mdDialog.show(confirm).then(function() {}, function() {});
                        }
                    };
                }else{
                    var confirm = $mdDialog.alert()
                    .title('OOPS! Something went wrong. Please contact support.').ariaLabel('Lucky day').ok('Ok');
                    $mdDialog.show(confirm).then(function() {}, function() {});
                }
            });

            //======================== TOTALS GRAPH!
            $http.get('/getTotals').then(function(resp){
                if(resp.data.code == '00'){
                    console.log('PTZ BUFFER: ' + resp.data.data.PTZBuffer + $scope.chuteDisplay);
                    $scope.labels = ['Pick-To-Zone', 'PTL Rejected', 'Packing Chute', 'QC Reject', 'Courier'];
                    $scope.series = ['Series A', 'Series B'];
                    $scope.data = [
                        [resp.data.data.courierTotal, resp.data.data.qcRejectTotal, resp.data.data.chuteTotal, resp.data.data.ptlReject, resp.data.data.PTZBuffer]
                        // [28, 48, 40, 19, 86, 27, 90]
                    ];
                    $scope.onClick = function (points, evt) {
                        console.log(points, evt);
                    };

                    $timeout(function () {
                        $scope.data = [
                            // [28, 48, 40, 19, 86, 27, 90],
                            [resp.data.data.PTZBuffer, resp.data.data.ptlReject, resp.data.data.chuteTotal, resp.data.data.qcRejectTotal, resp.data.data.courierTotal]
                        ];
                    }, 3000);
                }else{
                    var confirm = $mdDialog.alert()
                        .title('OOPS! Something went wrong. Please contact support.').ariaLabel('Lucky day').ok('Ok');
                    $mdDialog.show(confirm).then(function() {}, function() {});
                }
            });
        } else {
            var confirm = $mdDialog.alert()
            .title('The user session was not found. Please log in before requesting the page.')
            .ok('Ok')

            $mdDialog.show(confirm).then(function() {
                $location.path('/')
            }, function() {});
        }
    })
});
