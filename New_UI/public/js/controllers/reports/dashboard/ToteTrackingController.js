app.controller('ToteTrackingController', function ($scope, $location, $http, $mdDialog, $interval, $rootScope) {
    $scope.dataSource = {
        'chart': {
            'caption': 'Click status bar to view Picklists',
            'captionFontSize': '30',
            'captionPadding': '5',
            'baseFontSize': '16',
            'canvasBorderAlpha': '0',
            'showPlotBorder': '0',
            'placevaluesInside': '1',
            'valueFontColor': '#FFFFFF',
            'captionFontBold': '0',
            'bgColor': '#006C92',
            'divLineAlpha': '50',
            'plotSpacePercent': '10',
            'bgAlpha': '95',
            'canvasBgAlpha': '0',
            'outCnvBaseFontColor': '#FFFFFF',
            'showValues': '0',
            'baseFont': 'Open Sans',
            'paletteColors': '#FDCA03, #FDCA03, #FDCA03, #FDCA03, #FDCA03, #FDCA03',
            'theme': 'zune',

            // tool-tip customization
            'toolTipBorderColor': '#FFFFFF',
            'toolTipBorderThickness': '1',
            'toolTipBorderRadius': '2',
            'toolTipBgColor': '#000000',
            'toolTipBgAlpha': '70',
            'toolTipPadding': '12',
            'toolTipSepChar': ' - ',

            // axis customization
            'xAxisNameFontSize': '18',
            'yAxisNameFontSize': '18',
            'xAxisNamePadding': '10',
            'yAxisNamePadding': '10',
            'xAxisName': 'Status',
            'yAxisName': '',
            'xAxisNameFontBold': '0',
            'yAxisNameFontBold': '0',
            'showXAxisLine': '1',
            'xAxisLineColor': '#999999'

        },
        'data': [{
            'label': 'New',
            'value': 0
        }, {
            'label': 'Started',
            'value': 0
        }, {
            'label': 'Picked',
            'value': 0
        }, {
            'label': 'Packed',
            'value': 0
        }, {
            'label': 'Checked',
            'value': 0
        }, {
            'label': 'Shipped',
            'value': 0
        }]
    }

    $http.post('/API/pickListInfo/').then(function (response) {
        if(response.data.code == '00'){
            $scope.dataSource.data[0].value = response.data.data.statusNew;
            $scope.dataSource.data[1].value = response.data.data.statusStarted;
            $scope.dataSource.data[2].value = response.data.data.statusPicked;
            $scope.dataSource.data[3].value = response.data.data.statusPacked;
            $scope.dataSource.data[4].value = response.data.data.statusStored;
            $scope.dataSource.data[5].value = response.data.data.statusShipped;

            $scope.events = {
                dataplotclick: function (ev, props) {
                    $scope.$apply(function () {
                        $scope.colorValue = 'background-color:' + props.categoryLabel + ';';
                        $scope.selectedValue = 'You clicked on ' + props.categoryLabel + 'color column!'
                    })
                }
            }
        }else{
            var confirm = $mdDialog.confirm()
            .title('OOPS! Something went wrong. Please contact support.')
            .ariaLabel('Lucky day')
            .targetEvent($scope.dataSource.data[0].value)
            .ok('Ok')

            $mdDialog.show(confirm).then(function() {}, function() {});
        }
    });

    $scope.getNotification = function(){
		$http.post('/getNotifications').then(function(response){
			if(response){
				// console.log('___________________________________ | ' + response.data.total + ' |________________________________')
				$rootScope.notification = response.data.total
			}
		})
    }

	$scope.getCourierNoScan = function() {
        $http.get('/getCourtierNoRead').then(function(response) {
	    console.log(response.data.code)
            if(response.data.code == '00') {
                $scope.noRead = response.data.data
            } else {
                console.log('No courier no scan data found.')
            }
        })
    }
    
    // $interval(function() {
    //    $scope.getNotification()
    //	$scope.getCourierNoScan()
    // }, 10000);
})

angular.module('AngularChart', []).directive('chart', function () {
    return {
        restrict: 'E',
        template: '<div></div>',
        transclude: true,
        replace: true,
        scope: '=',
        link: function (scope, element, attrs) {
            console.log('oo', attrs, scope[attrs.formatter])
            var opt = {
                chart: {
                    renderTo: element[0],
                    type: 'line',
                    marginRight: 130,
                    marginBottom: 40
                },
                title: {
                    text: attrs.title,
                    x: -20 // center
                },
                subtitle: {
                    text: attrs.subtitle,
                    x: -20
                },
                xAxis: {
                    tickInterval: 1,
                    title: {
                        text: attrs.xname
                    }
                },
                plotOptions: {
                    lineWidth: 0.5
                },
                yAxis: {
                    title: {
                        text: attrs.yname
                    },
                    tickInterval: (attrs.yinterval) ?new Number(attrs.yinterval):null,
                    max: attrs.ymax,
                    min: attrs.ymin
                },
                tooltip: {
                    formatter: scope[attrs.formatter] || function () {
                        return '<b>' + this.y + '</b>'
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -10,
                    y: 100,
                    borderWidth: 0
                },
                series: [
                    {
                        name: 'Tokyo',
                        data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
                    }
                ]
            }


            // Update when charts data changes
            scope.$watch(function (scope) {
                return JSON.stringify({
                    xAxis: {
                        categories: scope[attrs.xdata]
                    },
                    series: scope[attrs.ydata]
                })
            }, function (news) {
                news = JSON.parse(news)
                if (!news.series) return
                angular.extend(opt, news)
                // console.log('opt.xAxis.title.text', opt)

                var chart = new Highcharts.Chart(opt)
            })
        }
    }
})
