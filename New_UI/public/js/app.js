/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-15 08:19:07
 * @Last Modified by: Zoran Popovic
 * @Last Modified time: 2018-05-29 12:29:10
 */

/**
* @app -this is where all the routes are defined. Depending on what item is selected, only that html page will load, along with its specified Controller.
*/

var app = angular.module('connxionApp', ['ngRoute', 'ngStorage', 'ng-ip-address', 'ui.bootstrap', 'dataGrid', 'chart.js', 'pagination', 'AngularChart', 'ng-fusioncharts', 'ngAnimate', 'ngMaterial', 'angularModalService', 'ngCookies', 'ngMessages', 'angular.filter'])

app.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/login/login.html',
            controller: 'LoginController'
        })
        .when('/login', {
            templateUrl: '/login/login.html',
            controller: 'LoginController'
        })

	.when('/trackShipped', {
            templateUrl: '/admin/trackShipped/trackShipped.html',
            controller: 'trackShippedController'
        })
        .when('/logout', {
            templateUrl: '/login/logout.html',
            controller: 'LoginController'
        })

        // OPERATIONS SECTION
        // REJECT STATION QC - blind count
        .when('/qcReject', {
            templateUrl: '/operations/qcRejected/qcReject.html',
            controller: 'QCRejectController'
        })
        .when('/courierReject', {
            templateUrl: '/operations/courierRejected/courierReject.html',
            controller: 'courierRejectController'
        })
        .when('/qcWeigh/:ToDo', {
            templateUrl: '/operations/qcRejected/qcCartonWeigh.html',
            controller: 'QCWeighController'
        })
        .when('/qcWeighed/:ToDo', {
            templateUrl: '/operations/qcRejected/processWeighedCarton.html',
            controller: 'QCWeighedController'
        })
        .when('/cartonScanning', {
            templateUrl: '/operations/qcRejected/cartonScanning.html',
            controller: 'QCRejectProcessController'
        })
        .when('/cartonScanning/:ToDo', {
            templateUrl: '/operations/qcRejected/cartonScanning.html',
            controller: 'QCRejectProcessController'
        })
        .when('/toteInfo', {
            templateUrl: '/operations/toteInfo/toteInfo.html',
            controller: 'toteInfoController'
        })
        .when('/qcRejectList', {
            templateUrl: '/operations/qcRejected/qcRejectsList.html',
            controller: 'QCRejectsListController'
        })
        .when('/pickListInfo', {
            templateUrl: '/operations/pickListInfo/pickList.html',
            controller: 'pickListController'
        })
        .when('/orderInfo', {
            templateUrl: '/operations/orderInfo/orderInfo.html',
            controller: 'orderInfoController'
        })
        .when('/palletHandover', {
            templateUrl: '/operations/palletHandover/palletHandover.html',
            controller: 'palletHandovers'
        })
        .when('/palletScanning/:ToDo', {
            templateUrl: '/operations/palletHandover/palletScanning.html',
            controller: 'palletScanningController'
        })
        .when('/palletComplete/:ToDo', {
            templateUrl: '/operations/palletHandover/palletComplete.html',
            controller: 'palletComplete'
        })
        .when('/nonConveyable', {
            templateUrl: '/operations/nonConveyable/nonConveyable.html',
            controller: 'nonConveyable'
        })

        // REJECT STATION - PTL
        .when('/ptlReject', {
            templateUrl: '/operations/ptlRejected/ptlReject.html',
            controller: 'PTLRejectController'
        })
        .when('/ptlRejectList', {
            templateUrl: '/operations/ptlRejected/ptlRejectList.html',
            controller: 'PTLRejectListController'
        })
        .when('/stationManager', {
            templateUrl: '/admin/stationParameters/stationManagement.html',
            controller: 'stationController'
        })

        // MANIFESTING STATION
        .when('/manifesting', {
            templateUrl: '/operations/manifesting/manifesting.html',
            controller: 'ManifestingController'
        })

        // PUT WALL
        .when('/putWallManifesting', {
            templateUrl: '/operations/manifesting/manifestingPutWall.html',
            controller: 'PutWallManifestingController'
        })
        // END OPERATIONS SECTION

        // SECURITY SECION
        // USER MANAGEMENT
        .when('/userMan', {
            templateUrl: '/security/userManagement/userList.html',
            controller: 'UserCntrl'
        })
        .when('/new/user', {
            templateUrl: '/security/userManagement/userForm.html',
            controller: 'NewUserController'
        })
        .when('/user/:userId', {
            templateUrl: '/security/userManagement/user.html',
            controller: 'EditUserController'
        })

        // ROLE MANAGEMENT
        .when('/roleMan', {
            templateUrl: '/security/roleManagement/roleList.html',
            controller: 'UserRoleController',
            resolve: {
                userRoles: function (UserRoles) {
                    return UserRoles.getUserRoles()
                }
            }
        })
        .when('/new/role', {
            templateUrl: '/security/roleManagement/roleForm.html',
            controller: 'NewUserRoleController'
        })
        .when('/role/:roleId', {
            templateUrl: '/security/roleManagement/role.html',
            controller: 'EditUserRoleController'
        })
        // END SECURITY SECTION

        // ADMIN SECTION
        // DEVICE MANAGEMENT
        .when('/deviceMan', {
            templateUrl: '/admin/deviceManagement/deviceList.html',
            controller: 'DeviceController',
            resolve: {
                devices: function (Devices) {
                    return Devices.getDevices()
                }
            }
        })
        .when('/deviceMan/new/device', {
            templateUrl: '/admin/deviceManagement/deviceForm.html',
            controller: 'NewDeviceController'
        })
        .when('/reprint', {
            templateUrl: '/admin/reprint/reprint.html',
            controller: 'reprintController'
        })
        .when('/salesChannelMgmt', {
            templateUrl: '/admin/salesChannelMgmt/salesChannelMgmt.html',
            controller: 'salesChannelMgmtController'
        })
        .when('/deviceMan/device/:deviceId', {
            templateUrl: '/admin/deviceManagement/device.html',
            controller: 'EditDeviceController'
        })
        .when('/printtopdf', {
            templateUrl: '/admin/PrintToPDF/PrintToPDF.html',
            controller: 'PrintToPDFController'
        })
         .when('/orderPriority', {
            templateUrl: '/admin/OrderPrioritising/OrderPrioritising.html',
            controller: 'orderPriorityController'
        })
          .when('/addBatch', {
            templateUrl: '/admin/WeightTolerance/WeightTolerance.html',
            controller: 'addBatchController'
        })

        // DEVICE MANAGEMENT - GROUPS
        .when('/deviceManGroup', {
            templateUrl: '/admin/deviceManagement/deviceListGroup.html',
            controller: 'DeviceGroupController',
            resolve: {
                deviceGroups: function (DeviceGroups) {
                    return DeviceGroups.getDeviceGroups()
                }
            }
        })
        .when('/new/deviceGroup', {
            templateUrl: '/admin/deviceManagement/deviceFormGroup.html',
            controller: 'NewDeviceGroupController'
        })
        .when('/deviceGroup/:deviceGroupId', {
            templateUrl: '/admin/deviceManagement/deviceGroup.html',
            controller: 'EditDeviceGroupController'
        })

        // SYSTEM PARAMETERS
        .when('/systemParameter', {
            templateUrl: '/admin/systemParameters/parameterList.html',
            controller: 'SystemParamController'
        })
        .when('/new/systemParameter', {
            templateUrl: '/admin/systemParameters/parameterForm.html',
            controller: 'NewSystemParamController'
        })
        .when('/systemParameter/:systemParameterId', {
            templateUrl: '/admin/systemParameters/parameter.html',
            controller: 'EditSystemParamController'
        })

        // SYSTEM PARAMETERS - GROUPS
        .when('/systemParameterGroup', {
            templateUrl: '/admin/systemParameters/parameterListGroup.html',
            controller: 'SystemParamGroupController',
            resolve: {
                systemParamGroups: function (SystemParamGroups) {
                    return SystemParamGroups.getSystemParamGroups()
                }
            }
        })
        .when('/new/systemParameterGroup', {
            templateUrl: '/admin/systemParameters/parameterFormGroup.html',
            controller: 'NewSystemParamGroupController'
        })
        .when('/systemParameterGroup/:systemParameterGroupId', {
            templateUrl: '/admin/systemParameters/parameterGroup.html',
            controller: 'EditSystemParamGroupController'
        })
        // END ADMIN SECTION

        // REPORTS SECTION
        // STANDARD REPORTS
        .when('/reports', {
            templateUrl: '/reports/reports/reportList.html',
            controller: 'ReportController'
        })

        // DASHBOARD
        .when('/toteTracking', {
            templateUrl: '/reports/dashboard/toteTracking.html',
            controller: 'ToteTrackingController'
        })
        .when('/conveyorNet', {
            templateUrl: '/reports/dashboard/conveyorNetwork.html',
            controller: 'ConveyorNetworkController'
        })
        .when('/throughput', {
            templateUrl: '/reports/dashboard/throughput.html',
            controller: 'ThroughputController'
        })
        .when('/putWallDash', {
            templateUrl: '/reports/dashboard/putWallDashboard.html',
            controller: 'putWallController'
        })
        .when('/packThroughput', {
            templateUrl: '/reports/dashboard/packThroughput.html',
            controller: 'packThroughputController'
        })
        .when('/courierThroughput', {
            templateUrl: '/reports/dashboard/courierThroughput.html',
            controller: 'courierThroughputController'
        })
        .when('/scaleThroughput', {
            templateUrl: '/reports/dashboard/scaleThroughPut.html',
            controller: 'scaleThroughputController'
        })
        .when('/alarmAndEvent', {
            templateUrl: '/reports/dashboard/alarmAndEventViewer.html',
            controller: 'alarmController'
        })
        .when('/qcRejectView', {
            templateUrl: '/reports/reports/qcRejectView.html',
            controller: 'QCRejectedViewController'
        })
        .when('/qcRejectItemView/:Todo', {
            templateUrl: '/reports/reports/qcRejectItemView.html',
            controller: 'QCRejectedItemViewController'
        })
        .when('/toteTrackingStatus', {
            templateUrl: '/reports/dashboard/toteTrackingStatus.html',
            controller: 'ToteTrackingController'
        })
        .when('/unpickaTote', {
            templateUrl: '/admin/unpickaTote/unpickStates.html',
            controller: 'unpickaTote_4' //UnpickPicklist
        })
        .when('/UnconsolidateOrders', {
            templateUrl: '/admin/unconsolidateOrders/unconsolidateStates.html',
            controller: 'unconsolidateController'
        })
        .when('/CancelledOrders', {
            templateUrl: '/admin/CancelledOrders/CancelledView.html',
            controller: 'CancelledController'
        })
        .when('/WeightAndDimensionUpdate', {
            templateUrl: '/admin/WeightUpdate/weightUpdate.html',
            controller: 'weightUpdateController'
        })
        .when('/packingErrView', {
            templateUrl: '/reports/reports/PackingErrView.html',
            controller: 'PackingErrViewController'
        })
        // END REPORTS SECTION

        .otherwise({
            redirectTo: '/'
        })
})

app.directive('focusMe', function($timeout, $parse) {
  return {
    link: function(scope, element, attrs) {
      var model = $parse(attrs.focusMe);
      scope.$watch(model, function(value) {
        if(value === true) {
          $timeout(function() {
            element[0].focus();
          });
        }
      });
    }
  };
});

