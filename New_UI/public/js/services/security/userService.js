/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-15 08:18:57
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-07-25 16:34:09
 */

/**
* @service Users -services are used as an intermediary between the controllers, and the methods used for db excecution. This is to keep the functionality and db integration separate.
* @function  getUsers - gets a list of all users from the db.
* @function  createUser - creates a new user.
* @function  getUser - gets the selected user based on its id.
* @function  editUser - edit the selected user. Saves it to db, by id.
* @function  deleteUser - deletes a user from the db, where id matches.
*/

app.service('Users', function ($http) {
    let userService = this

    userService.getUsers = function () {
        return $http.get('/API/user/')
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error finding users.')
        })
    }

    userService.createUser = function (user) {
        return $http.post('/API/user/', user)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error creating user.')
        })
    }

    userService.getUser = function (userId) {
        var url = '/API/user/' + userId
        return $http.get(url)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error finding this user.')
        })
    }

    userService.editUser = function (user) {
        console.log(user.id)
        return $http.put('/API/user/', user)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error editing this user.')
            console.log(response)
        })
    }

    userService.deleteUser = function (user) {
        return $http.delete('/API/user/', user)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error deleting this user.')
            console.log(response)
        })
    }
})
