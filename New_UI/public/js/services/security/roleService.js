/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-15 08:18:49
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-03-23 09:41:58
 */

/**
* @service UserRoles -services are used as an intermediary between the controllers, and the methods used for db excecution. This is to keep the functionality and db integration separate.
* @function  getUserRoles - gets a list of all user roles from the db.
* @function  createUserRole - creates a new user role.
* @function  getUserRole - gets the selected user role based on its id.
* @function  editUserRole - edit the selected user role. Saves it to db, by id.
* @function  deleteUserRole - deletes a user role from the db, where id matches.
*/

app.service('UserRoles', function ($http) {
    let userRoleService = this

    userRoleService.getUserRoles = function () {
        return $http.get('/API/userRole/')
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error finding user roles.')
        })
    }

    userRoleService.createUserRole = function (userRole) {
        return $http.post('/API/userRole/', userRole)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error creating user role.')
        })
    }

    userRoleService.getUserRole = function (userRoleId) {
        var url = '/API/userRole/' + userRoleId
        return $http.get(url)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error finding this user role.')
        })
    }

    userRoleService.editUserRole = function (userRole) {
        console.log(userRole.id)
        return $http.put('/API/userRole/', userRole)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error editing this user role.')
            console.log(response)
        })
    }

    userRoleService.deleteUserRole = function (userRole) {
        return $http.delete('/API/userRole/', userRole)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error deleting this user role.')
            console.log(response)
        })
    }
})
