/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-15 08:17:58
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-08-25 14:23:38
 */

/**
* @service
* @function
*/
app.service('orderInfoService', function ($q, $http, $cookies) {
    let orderInfoService = this
    let OrdID = null

    orderInfoService.setOrderInfo = function (value) {
        // console.log('IS VALUE BEING SET! ' + value)
        OrdID = value
    }

    orderInfoService.getOrderByID = function () {
        return $http.post('/API/getOrderDetails/' + OrdID)
        .then(function (response) {
			return response
        }, function (response) {
            window.alert('Errot getting order information.')
        })
    }

    orderInfoService.getOrder = function (orderID) {
        return $http.post('/API/getOrderDetails/' + orderID)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Errot getting order information.')
        })
    }
})
