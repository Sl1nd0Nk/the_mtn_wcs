/*
 * @Author: Ntokozo Majozi
 * @Date: 2017-03-15 08:17:58
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-08-23 20:18:14
 */

/**
* @service
* @function
*/
app.service('pickListService', function ($q, $http, $cookies) {
    let pickListService = this

    pickListService.getPickListInfo = function () {
        return $http.post('/API/pickListInfoNew/')
        .then(function (response) {
            return response
        }, function (response) {
            // window.alert('Errot getting tote information.')
        })
    }

    pickListService.getListByOrder = function (orderID) {
        return $http.post('/API/listByOrder/' + orderID)
        .then(function (response) {
            return response
        }, function (response) {
            // window.alert('Errot getting tote information.')
        })
    }
})
