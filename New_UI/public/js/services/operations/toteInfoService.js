/*
 * @Author: Ntokozo Majozi
 * @Date: 2017-83-22 08:17:58
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-08-22 13:48:37
 */

/**
* @service
* @function
*/
app.service('toteInfoService', function ($q, $http, $cookies) {
    let toteInfoService = this
    var tote = null

    toteInfoService.getToteInfo = function (toteID) {
        return $http.post('/API/toteInfo/'+ toteID)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Errot getting tote information.')
        })
    }

toteInfoService.getItemByOrder = function (orderID) {
        return $http.post('/API/getItemByOrder/' + orderID)
         .then(function (resp) {
             return resp
         }, function (resp) {
             window.alert('Errot getting items.')
         })
    }

    toteInfoService.setToteInfo = function (value) {
        // console.log('IS VALUE BEING SET! ' + value)
        tote = value
    }

    toteInfoService.getTote = function () {

        return $http.post('/API/toteInfo/' + tote)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Errot getting tote information.')
        })
    }
})
