/*
 * @Author: ntokozo Majozi
 * @Date: 2017-12-01 08:18:23
 */

app.service('RejectService', function ($http, $location, $mdDialog) {
    let rejectService = this

    var carton = null
    var cartonToWeigh = null

    rejectService.setCarton = function (value) {
        carton = value
    }

    rejectService.setWeightCartonID = function (value, transporterID) {
        return $http.get('/checkBeforeSetCarton/' + value).then(function(resp){
            if(resp.data.code == '00') {
               return value
            } else {
                var confirm = $mdDialog.confirm()
                .title('Carton ID: ' + value + ' Is invalid.')
                .textContent('Please ensure that a valid carton is being scanned.')
                .ariaLabel('Lucky day')
                .targetEvent(value)
                .ok('Ok')
    
                $mdDialog.show(confirm).then(function() {
                    $location.path('/qcWeigh/' + transporterID)
                }, function() {
                    
                });
            }
        })
    }

    rejectService.getQCStation = function(){
        return $http.get('/getQCStation/')
            .then(function (response){
                return response.data
            }, function (response){
                console.log("No station is found")
            })
    }

    rejectService.checkReject = function (cartonID) {
        return $http.get('/API/checkPickList/' + cartonID)
        .then(function (response) {
            return response.data
        }, function (response) {
            window.alert('Error finding pick lists.')
        })
    }



    rejectService.getPickList = function () {
        return $http.get('/API/getPickList/' + carton)
        .then(function (response) {
            return response.data
        }, function (response) {
            window.alert('Error finding pick lists.')
        })
    }

    rejectService.getCartonID = function () {
        return $http.get('/API/getCartonID/' + carton)
        .then(function (response) {
            return response.data.data
        }, function (response) {
            window.alert('Error finding carton ID.')
        })
    }

    rejectService.getWeighedCartinID = function () {
        return $http.get('/API/getCartonID/' + cartonToWeigh)
        .then(function (response) {
            return response.data.data
        }, function (response) {
            window.alert('Error finding carton ID.')
        })
    }

    rejectService.getWeighedCartonID = function () {
        return $http.get('/API/getPickList/' + cartonToWeigh)
        .then(function (response) {
            return response.data
        }, function (response) {
            window.alert('Error finding carton ID.')
        })
    }

    rejectService.getWeighedCartonID2 = function (cartonToWeigh2) {
        return $http.get('/API/getPickList/' + cartonToWeigh2)
        .then(function (response) {
            return response.data
        }, function (response) {
            window.alert('Error finding carton ID.')
        })
    }

    rejectService.getParameters = function () {
        return $http.get('/API/getParameters/')
        .then(function (response) {
            return response.data
        }, function (response) {
            window.alert('Error finding carton ID.')
        })
    }
})
