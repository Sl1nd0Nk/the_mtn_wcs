/*
 * @Author: Deon Wolmarans
 * @Date: 2017-07-25 10:42:02
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-07-29 15:35:33
 */

/**
* @service PrintingService - services are used as an intermediary between the controllers, and the methods used for db excecution. This is to keep the functionality and db integration separate.
* @function  postToPrint - sends detail to server of what needs printing.
*/

app.service('PrintingService', function ($http) {
    let PrintingService = this

    PrintingService.postToPrint = function (document) {
        var req = {
          method: 'POST',
          url: '/API/printDocuments/',
          headers: {
            'Content-Type':'application/json'
          },
          data: document
        }

        $http(req).success(function(response){return response}).error(function(response){return response})

        //var url = '/API/printDocuments/' + document
        //return $http.post(url)
        //.then(function (response) {
        //    return response
        //}, function (response) {
        //    window.alert('Error printing these documents.')
        //})
    }

    PrintingService.postZplToPrint = function (label) {
        var url = '/API/printZplDocuments/' + label
        return $http.post(url)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error printing this label.')
        })
    }
})
