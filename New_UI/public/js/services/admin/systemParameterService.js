/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-15 08:12:32
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-03-24 08:01:38
 */

/**
* @service SystemParams -services are used as an intermediary between the controllers, and the methods used for db excecution. This is to keep the functionality and db integration separate.
* @function  getSystemParams - gets a list of all the system parameters from the db.
* @function  createSystemParam - creates a new system parameter.
* @function  getSystemParam - gets the selected system paramter based on its id.
* @function  editSystemParam - edit the selected system parameter. Saves it to db, by id.
* @function  deleteSystemParam - deletes a system parameter from the db, where id matches.
*/

app.service('SystemParams', function ($http) {
    let systemParamsService = this

    systemParamsService.getSystemParams = function () {
        return $http.get('/API/systemParameter/')
            .then(function (response) {
                return response
            }, function (response) {
                window.alert('Error finding systemParams.')
            })
    }

    systemParamsService.createSystemParam = function (systemParam) {
        return $http.post('/API/systemParameter/', systemParam)
            .then(function (response) {
                return response
            }, function (response) {
                window.alert('Error creating systemParam.')
            })
    }

    systemParamsService.getSystemParam = function (systemParamId) {
        var url = '/API/systemParameter/' + systemParamId
        return $http.get(url)
            .then(function (response) {
                return response
            }, function (response) {
                window.alert('Error finding this systemParam.')
            })
    }

    systemParamsService.editSystemParam = function (systemParam) {
        var url = '/API/systemParameter/' + systemParam.id
        console.log(systemParam._id)
        return $http.put(url, systemParam)
            .then(function (response) {
                return response
            }, function (response) {
                window.alert('Error editing this systemParam.')
                console.log(response)
            })
    }

    systemParamsService.deleteSystemParam = function (systemParam) {
        // var url = '/API/systemParameter/' + systemParamId
        return $http.delete('/API/systemParameter/', systemParam.id)
            .then(function (response) {
                return response
            }, function (response) {
                window.alert('Error deleting this systemParam.')
                console.log(response)
            })
    }
})
