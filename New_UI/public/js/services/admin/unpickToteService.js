/*
 * @Author: Silindokuhle (Sli)
 * @Date: 2019-06-14
 *
 * 
 */

app.service('unpickToteService', function ($q, $http, $cookies)
{
	let unpickToteService = this

	unpickToteService.picklistTemp = function (data)
	{
		return $http.get('/API/unpickaTote' + data)
		.then(function (response)
		{
			return response;
		},
		function (response)
		{
			return response;
		})
	} /* unpickToteService.userloginTemp */
    
})
