/*
 * @Author: Silindokuhle (Sli)
 * @Date: 2019-06-14
 *
 * 
 */

app.service('WmsCancelUpdateService', function ($q, $http, $cookies)
{
	let WmsCancelUpdateService = this

	WmsCancelUpdateService.WmsUpdate = function (data)
	{
		return $http.get('/API/WmsCancelUpdate' + data)
		.then(function (response)
		{
			return response;
		},
		function (response)
		{
			return response;
		})
	} /* WmsCancelUpdateService.WmsUpdate */
})
