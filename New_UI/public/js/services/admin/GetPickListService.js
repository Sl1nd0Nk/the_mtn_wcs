/*
 * @Author: Silindokuhle (Sli)
 * @Date: 2019-06-14
 *
 * 
 */

app.service('GetPickListService', function ($q, $http, $cookies)
{
	let GetPickListService = this

	GetPickListService.GetPickList = function (data)
	{
		return $http.get('/API/Getpicklists' + data)
		.then(function (response)
		{
			return response;
		},
		function (response)
		{
			return response;
		})
	} /* GetPickListService.GetPickList */
})
