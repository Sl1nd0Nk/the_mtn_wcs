/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-15 08:05:59
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-03-20 10:06:49
 */

/**
* @service DeviceGroups -services are used as an intermediary between the controllers, and the methods used for db excecution. This is to keep the functionality and db integration separate.
* @function  getDeviceGroups - gets a list of all device groups from the db.
* @function  createDeviceGroup - creates a new device group.
* @function  getDeviceGroup - gets the selected device group based on its id.
* @function  editDeviceGroup - edit the selected device group. Saves it to db, by id.
* @function  deleteDeviceGroup - deletes a device group from the db, where id matches.
*/

app.service('DeviceGroups', function ($http) {
    let deviceGroupsService = this

    deviceGroupsService.getDeviceGroups = function () {
        return $http.get('/API/deviceGroup/')
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error finding device groups.')
        })
    }

    deviceGroupsService.createDeviceGroup = function (deviceGroup) {
        return $http.post('/API/deviceGroup/', deviceGroup)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error creating device group.')
        })
    }

    deviceGroupsService.getDeviceGroup = function (deviceGroupId) {
        var url = '/API/deviceGroup/' + deviceGroupId
        return $http.get(url)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error finding this device group.')
        })
    }

    deviceGroupsService.editDeviceGroup = function (deviceGroup) {
        // var url = '/API/deviceGroup/' + deviceGroup.id
        console.log(deviceGroup.id)
        return $http.put('/API/deviceGroup/', deviceGroup)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error editing this device group.')
            console.log(response)
        })
    }

    deviceGroupsService.deleteDeviceGroup = function (deviceGroup) {
        return $http.delete('/API/deviceGroup/', deviceGroup)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error deleting this device group.')
            console.log(response)
        })
    }
})
