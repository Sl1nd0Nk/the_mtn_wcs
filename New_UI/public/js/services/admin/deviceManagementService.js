/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-15 08:05:59
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-07-25 16:38:29
 */

/**
* @service Devices -services are used as an intermediary between the controllers, and the methods used for db excecution. This is to keep the functionality and db integration separate.
* @function  getDevices - gets a list of all devices from the db.
* @function  createDevice - creates a new device.
* @function  getDevice - gets the selected device based on its id.
* @function  editDevice - edit the selected device. Saves it to db, by id.
* @function  deleteDevice - deletes a device from the db, where id matches.
*/

app.service('Devices', function ($http) {
    let devicesService = this

    devicesService.getDevices = function () {
        return $http.get('/API/device/')
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error finding devices.')
        })
    }

    devicesService.createDevice = function (device) {
        return $http.post('/API/device/', device)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error creating device.')
        })
    }

    devicesService.getDevice = function (deviceId) {
        var url = '/API/device/' + deviceId
        return $http.get(url)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error finding this device.')
        })
    }

    devicesService.editDevice = function (device) {
        var url = '/API/device/' + device.id
        console.log(device.id)
        return $http.put(url, device)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error editing this device.')
            console.log(response)
        })
    }

    devicesService.deleteDevice = function (deviceId) {
        return $http.delete('/API/device/', deviceId)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error deleting this device.')
            console.log(response)
        })
    }
})
