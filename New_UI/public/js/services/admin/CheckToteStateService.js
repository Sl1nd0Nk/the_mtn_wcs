/*
 * @Author: Silindokuhle (Sli)
 * @Date: 2019-07-05
 *
 * 
 */

app.service('CheckToteStateService', function ($q, $http, $cookies)
{
	let CheckToteStateService = this

	CheckToteStateService.GetConsolidatedPicklist = function (data)
	{
		return $http.get('/API/ConsolidatedPicklist' + data)
		.then(function (response)
		{
			return response;
		},
		function (response)
		{
			return response;
		})
	} /* CheckToteStateService.GetConsolidatedPicklist */ 

	CheckToteStateService.QtyCheck = function (data)
	{
		return $http.get('/API/Toteqty' + data)
		.then(function (response)
		{
			return response;
		},
		function (response)
		{
			return response;
		})
	} /* CheckToteStateService.QtyCheck */ 

	CheckToteStateService.Unconsolidate = function (data)
	{
		return $http.get('/API/Unconsolidate' + data)
		.then(function (response)
		{
			return response;
		},
		function (response)
		{
			return response;
		})
	} /* CheckToteStateService.Unconsolidate */ 
})