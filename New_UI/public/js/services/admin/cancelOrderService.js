/*
 * @Author: Silindokuhle (Sli)
 * @Date: 2019-06-14
 *
 * 
 */

app.service('cancelOrderService', function ($q, $http, $cookies)
{
	let cancelOrderService = this

	cancelOrderService.CancelPicks = function (data)
	{
		return $http.get('/API/CancelPicklist' + data)
		.then(function (response)
		{
			return response;
		},
		function (response)
		{
			return response;
		})
	} /* unpickToteService.userloginTemp */ 
})
