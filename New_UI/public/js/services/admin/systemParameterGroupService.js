/*
 * @Author: Deon Wolmarans
 * @Date: 2017-03-14 09:01:32
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-03-24 09:05:13
 */

/**
* @service SystemParamGroups -services are used as an intermediary between the controllers, and the methods used for db excecution. This is to keep the functionality and db integration separate.
* @function  getSystemParamGroups - gets a list of all the system parameter groups from the db.
* @function  createSystemParamGroup - creates a new system parameter group.
* @function  getSystemParamGroup - gets the selected system paramter group based on its id.
* @function  editSystemParamGroup - edit the selected system parameter group. Saves it to db, by id.
* @function  deleteSystemParamGroup - deletes a system parameter group from the db, where id matches.
*/

app.service('SystemParamGroups', function ($http) {
    let systemParamGroupService = this

    systemParamGroupService.getSystemParamGroups = function () {
        return $http.get('/API/systemParameterGroup/')
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error finding systemParamGroups.')
        })
    }

    systemParamGroupService.createSystemParamGroup = function (systemParamGroup) {
        return $http.post('/API/systemParameterGroup/', systemParamGroup)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error creating systemParamGroup.')
        })
    }

    systemParamGroupService.getSystemParamGroup = function (systemParamGroupId) {
        var url = '/API/systemParameterGroup/' + systemParamGroupId
        return $http.get(url)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error finding this systemParamGroup.')
        })
    }

    systemParamGroupService.editSystemParamGroup = function (systemParamGroup) {
        console.log(systemParamGroup)
        return $http.put('/API/systemParameterGroup/', systemParamGroup)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error editing this systemParamGroup.')
            console.log(response)
        })
    }

    systemParamGroupService.deleteSystemParamGroup = function (systemParamGroupId) {
        var url = '/API/systemParameterGroup/' + systemParamGroupId
        return $http.delete(url)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error deleting this systemParamGroup.')
            console.log(response)
        })
    }
})
