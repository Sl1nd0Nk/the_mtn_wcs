/*
 * @Author: Clive Matlawa
 * @Date: 2017-09-12
 */

app.service('NonConvService', function ($http)
{
    let nonConvService = this

    nonConvService.getPickList = function (pickListId)
    {
			var url = '/API/NCpickList/' + pickListId
			return $http.get(url)
			.then(function (response)
			{
				return response
			},
			function (response)
			{
				return response
			})
    } /* nonConvService.getPickList */

    nonConvService.getSerials = function (serial)
    {
			var url = '/API/NCgetSerial/' + serial
			return $http.get(url)
			.then(function (response)
			{
				if(response.data.code == 'ETIMEDOUT')
				{
					response.data = null
				}
				return response
			},
			function (response)
			{
				return response
			})
    } /* nonConvService.getSerials */

    nonConvService.resetStation = function ()
    {
			 var url = '/API/NCpickListReset/'
			 return $http.get(url)
			 .then(function (response)
			 {
				 return response
			 },
			 function (response)
			 {
				 return response
			 })
		} /* nonConvService.resetStation */

    nonConvService.getCartons = function()
    {
			 var url = '/API/NCgetCartons/'
			 return $http.get(url)
			 .then(function (response)
			 {
				 return response
			 },
			 function (response)
			 {
				 return response
			 })
		} /* nonConvService.getCartons */

		nonConvService.updateCartonSize = function(newCartonSize)
		{
			 var url = '/API/NCupdateCartonSize/' + newCartonSize
			 return $http.get(url)
			 .then(function (response)
			 {
				 return response
			 },
			 function (response)
			 {
				 return response
			 })
		} /* nonConvService.updateCartonSize */

		nonConvService.updateScannedCarton = function(scannedCartonId)
		{
			 var url = '/API/NCupdateCartonToPicklist/' + scannedCartonId
			 return $http.get(url)
			 .then(function (response)
			 {
				 return response
			 },
			 function (response)
			 {
				 return response
			 })
		} /* nonConvService.updateScannedCarton */

		nonConvService.updateVerifyScannedDoc = function(scannedDocument)
		{
			 var url = '/API/NCupdateVerifyScannedDoc/' + scannedDocument
			 return $http.get(url)
			 .then(function (response)
			 {
				 return response
			 },
			 function (response)
			 {
				 return response
			 })
		} /* nonConvService.updateVerifyScannedDoc */

		nonConvService.updateScannedTransporter = function(scannedTransporterId)
		{
			 var url = '/API/NCupdateScannedTransporter/' + scannedTransporterId
			 return $http.get(url)
			 .then(function (response)
			 {
				 return response
			 },
			 function (response)
			 {
				 return response
			 })
		} /* nonConvService.updateVerifyScannedDoc */

		nonConvService.reprintCarton = function()
		{
			 var url = '/API/NCreprintCarton/'
			 return $http.get(url)
			 .then(function (response)
			 {
				 return response
			 },
			 function (response)
			 {
				 return response
			 })
		} /* nonConvService.reprintCarton */
})
