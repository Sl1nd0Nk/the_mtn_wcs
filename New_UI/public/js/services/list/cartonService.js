/*
 * @Author: Deon Wolmarans
 * @Date: 2017-07-07 11:51:18
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-07-07 11:55:43
 */

/**
* @service CartonService - services are used as an intermediary between the controllers, and the methods used for db excecution. This is to keep the functionality and db integration separate.
* @function  getCartons - gets a list of all Cartons from the db.
* @function  createCarton - creates a new Carton.
* @function  getCarton - gets the selected Carton based on its id.
* @function  editCarton - edit the selected Carton. Saves it to db, by id.
* @function  deleteCarton - deletes a Carton from the db, where id matches.
*/

app.service('CartonService', function ($http) {
    let cartonService = this

    cartonService.getCartons = function () {
        return $http.get('/API/cartons/')
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error finding cartons.')
        })
    }

    cartonService.createCarton = function (carton) {
        return $http.post('/API/cartons/', carton)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error creating carton.')
        })
    }

    cartonService.getCarton = function (cartonId) {
        var url = '/API/cartons/' + cartonId
        return $http.get(url)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error finding requested carton.')
        })
    }

    cartonService.editCarton = function (carton) {
        return $http.put('/API/cartons/', carton)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error editing this carton.')
        })
    }

    cartonService.deleteCarton = function (carton) {
        return $http.delete('/API/cartons/', carton)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error deleting this carton.')
        })
    }
})
