/*
 * @Author: Clive Matlawa
 * @Date: 2017-09-12
 */

app.service('PickListService', function ($http)
{
    let pickListService = this

    pickListService.getPickList = function (pickListId)
    {
			var url = '/API/pickList/' + pickListId
			return $http.get(url)
			.then(function (response)
			{
				return response
			},
			function (response)
			{
				return response
			})
    } /* pickListService.getPickList */

    pickListService.getSerials = function (serial)
    {
			var url = '/API/getSerial/' + serial
			return $http.get(url)
			.then(function (response)
			{
				if(response.data.code == 'ETIMEDOUT')
				{
					response.data = null
				}
				return response
			},
			function (response)
			{
				return response
			})
    } /* pickListService.getSerials */

    pickListService.printAutobag = function(toteID)
    {
	var url = '/API/printAutoBag/' + toteID

	return $http.get(url)
	.then(function(response)
	{
		return response
	},
	function(response)
	{
		return response
	})
     }

    pickListService.resetStation = function ()
    {
			 var url = '/API/pickListReset/'
			 return $http.get(url)
			 .then(function (response)
			 {
				 return response
			 },
			 function (response)
			 {
				 return response
			 })
		} /* pickListService.resetStation */

    pickListService.getCartons = function(cartonTote)
    {
			 var url = '/API/getCartons/' + cartonTote
			 return $http.get(url)
			 .then(function (response)
			 {
				 return response
			 },
			 function (response)
			 {
				 return response
			 })
		} /* PickListService.getCartons */

		pickListService.updateCartonSize = function(newCartonSize)
		{
			 var url = '/API/updateCartonSize/' + newCartonSize
			 return $http.get(url)
			 .then(function (response)
			 {
				 return response
			 },
			 function (response)
			 {
				 return response
			 })
		} /* pickListService.updateCartonSize */

		pickListService.updateScannedCarton = function(scannedCartonId)
		{
			 var url = '/API/updateCartonToPicklist/' + scannedCartonId
			 return $http.get(url)
			 .then(function (response)
			 {
				 return response
			 },
			 function (response)
			 {
				 return response
			 })
		} /* pickListService.updateScannedCarton */

		pickListService.updateVerifyScannedDoc = function(scannedDocument)
		{
			 var url = '/API/updateVerifyScannedDoc/' + scannedDocument
			 return $http.get(url)
			 .then(function (response)
			 {
				 return response
			 },
			 function (response)
			 {
				 return response
			 })
		} /* pickListService.updateVerifyScannedDoc */

		pickListService.updateScannedTransporter = function(scannedTransporterId)
		{
			 var url = '/API/updateScannedTransporter/' + scannedTransporterId
			 return $http.get(url)
			 .then(function (response)
			 {
				 return response
			 },
			 function (response)
			 {
				 return response
			 })
		} /* pickListService.updateVerifyScannedDoc */

		pickListService.reprintCarton = function()
		{
			 var url = '/API/reprintCarton/'
			 return $http.get(url)
			 .then(function (response)
			 {
				 return response
			 },
			 function (response)
			 {
				 return response
			 })
		} /* pickListService.reprintCarton */

    pickListService.getQCPickList = function (pickListId){
		var url = '/API/QCPickList/' + pickListId;
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.getQCPickList */

    pickListService.AddQCPickListData = function (ContainerID){
		var url = '/API/AddQCPickListData/' + ContainerID;
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.AddQCPickListData */

    pickListService.AddQCTrPickListData = function (ContainerID){
		var url = '/API/AddQCTrPickListData/' + ContainerID;
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.AddQCTrPickListData */

    pickListService.ClearQCPickListData = function (){
		var url = '/API/ClearQCPickListData/';
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.ClearQCPickListData */

    pickListService.QCFindScannedItem = function (Item){
		var url = '/API/QCFindScannedItem/' + Item;
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.QCFindScannedItem */

    pickListService.QCFindCartonAndWeigh = function (Item){
		var url = '/API/QCFindCartonAndWeigh/' + Item;
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.QCFindCartonAndWeigh */

    pickListService.QCCompareEnteredWeight = function (Item){
		var url = '/API/QCCompareEnteredWeight/' + Item;
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.QCCompareEnteredWeight */

    pickListService.CompleteQCPickListCarton = function (Data){
		var url = '/API/CompleteQCPickListCarton/' + Data;
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.CompleteQCPickListCarton */

    pickListService.AsideQCPickListCarton = function (Data){
		var url = '/API/AsideQCPickListCarton/' + Data;
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.AsideQCPickListCarton */

    pickListService.CompleteQCPickListTransporter = function (Data){
		var url = '/API/CompleteQCPickListTransporter/' + Data;
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.CompleteQCPickListTransporter */

    pickListService.TrRemoveConfirm = function (Data){
		var url = '/API/TrRemoveConfirm/' + Data;
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.TrRemoveConfirm */

   	pickListService.QCCheckIfWaitReceived = function (){
		var url = '/API/QCCheckIfWaitReceived/';
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.QCCheckIfWaitReceived */

    pickListService.getCourierContainer = function (Container){
		var url = '/API/CourierContainer/' + Container;
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.getCourierContainer */

    pickListService.getConveyorRejectedToteId = function (ToteId){
		var url = '/API/getConveyorRejectedToteId/' + ToteId;
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.getConveyorRejectedToteId */

    pickListService.ClearPtlRejectError = function (ToteId){
		var url = '/API/ClearPtlRejectError/' + ToteId;
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.ClearPtlRejectError */

   	pickListService.getCancelledOrderList = function (){
		var url = '/API/getCancelledOrderList/';
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.getCancelledOrderList */

    pickListService.CanReqPicklists = function (Data){
		var url = '/API/CanReqPicklists/' + Data;
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.CanReqPicklists */

    pickListService.PrintCancelManifest = function (Data){
		var url = '/API/PrintCancelManifest/' + Data;
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.PrintCancelManifest */

    pickListService.WADGetProduct = function (Data){
		var url = '/API/WADGetProduct/' + Data;
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.WADGetProduct */

    pickListService.WADUpdateProduct = function (Data){
		var url = '/API/WADUpdateProduct/' + Data;
		return $http.get(url).then(function (response){
			return response;
		}, function (response){
			return response;
		});
    } /* pickListService.WADUpdateProduct */
});
