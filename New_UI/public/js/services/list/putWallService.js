/*
 * @Author: Clive Matlawa
 * @Date: 2017-09-12
 */

app.service('PutWallService', function ($http){
    let putWallService = this

    putWallService.getPickList = function (pickListId){
			var url = '/API/pwpickList/' + pickListId
			return $http.get(url)
			.then(function (response){
				 return response
			}, function (response){
				 return response
			})
    } /* putWallService.getPickList */

    putWallService.getSerials = function (serial){
			var url = '/API/pwgetSerial/' + serial
			return $http.get(url)
			.then(function (response){
				if(response.data.code == 'ETIMEDOUT'){
					response.data = null
				}
				return response
			},function (response){
				return response
			})
    } /* putWallService.getSerials */

    putWallService.resetStation = function (){
			 var url = '/API/pwpickListReset/'
			 return $http.get(url)
			 .then(function (response){
				 return response
			 },function (response){
				 return response
			 })
		} /* putWallService.resetStation */

		putWallService.updateVerifyScannedDoc = function(scannedDocument){
			 var url = '/API/pwupdateVerifyScannedDoc/' + scannedDocument
			 return $http.get(url)
			 .then(function (response){
				 return response
			 },function (response){
				 return response
			 })
		} /* putWallService.updateVerifyScannedDoc */

		putWallService.reprintCarton = function(){
			 var url = '/API/pwreprintCarton/'
			 return $http.get(url)
			 .then(function (response){
				 return response
			 },function (response){
				 return response
			 })
		} /* putWallService.reprintCarton */

		putWallService.checkLocConfirmed = function(Location){
			 var url = '/API/pwcheckLocConfirmed/' + Location
			 return $http.get(url)
			 .then(function (response){
				 return response
			 },function (response){
				 return response
			 })
		} /* putWallService.checkLocConfirmed */
})
