/*
 * @Author: Deon Wolmarans
 * @Date: 2017-07-11 12:17:54
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-07-11 12:20:45
 */

/**
* @service TransporterService - services are used as an intermediary between the controllers, and the methods used for db excecution. This is to keep the functionality and db integration separate.
* @function  getTransporters - gets a list of all Transporters from the db.
* @function  createTransporter - creates a new Transporter.
* @function  getTransporter - gets the selected Transporter based on its id.
* @function  editTransporter - edit the selected Transporter. Saves it to db, by id.
* @function  deleteTransporter - deletes a Transporter from the db, where id matches.
*/

app.service('TransporterService', function ($http) {
    let transporterService = this

    transporterService.getTransporters = function () {
        return $http.get('/API/transporters/')
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error finding transporters.')
        })
    }

    transporterService.createTransporter = function (transporter) {
        return $http.post('/API/transporters/', transporter)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error creating transporter.')
        })
    }

    transporterService.getTransporter = function (transporterId) {
        var url = '/API/transporters/' + transporterId
        return $http.get(url)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error finding requested transporter.')
        })
    }

    transporterService.editTransporter = function (transporter) {
        return $http.put('/API/transporters/', transporter)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error editing this transporter.')
        })
    }

    transporterService.deleteTransporter = function (transporter) {
        return $http.delete('/API/transporters/', transporter)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error deleting this transporter.')
        })
    }
})
