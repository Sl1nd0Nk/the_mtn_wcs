/*
 * @Author: Deon Wolmarans
 * @Date: 2017-07-04 06:47:12
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-07-14 15:07:02
 */

/**
* @service PackListService - services are used as an intermediary between the controllers, and the methods used for db excecution. This is to keep the functionality and db integration separate.
* @function  getPackLists - gets a list of all pack lists from the db.
* @function  createPackList - creates a new pack list.
* @function  getPackList - gets the selected pack list based on its id.
* @function  editPackList - edit the selected packlist. Saves it to db, by id.
* @function  deletePackList - deletes a pack list from the db, where id matches.
*/

app.service('PackListService', function ($http) {
    let packListService = this

    packListService.getPackLists = function () {
        return $http.get('/API/packList/')
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error finding pack lists.')
        })
    }

    packListService.createPackList = function (packlist) {
        return $http.post('/API/packList/', packlist)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error creating pack list.')
        })
    }

    packListService.getCartonPackList = function (packListId) {
        var url = '/API/packListCarton/' + packListId
        return $http.get(url)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error finding requested pack list.')
        })
    }

    packListService.getTransporterPackList = function (packListId) {
        var url = '/API/packListTransporter/' + packListId
        return $http.get(url)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error finding requested pack list.')
        })
    }

    packListService.editPackList = function (packList) {
        return $http.put('/API/packList/', packList)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error editing this pack list.')
        })
    }

    packListService.deletePackList = function (packList) {
        return $http.delete('/API/packList/', packList)
        .then(function (response) {
            return response
        }, function (response) {
            window.alert('Error deleting this pack list.')
        })
    }
})
