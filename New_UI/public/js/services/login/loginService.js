/*
 * @Author: Clive Matlawa
 * @Date: 2017-09-12
 */

app.service('loginService', function ($q, $http, $cookies)
{
	let loginService = this

	loginService.userloginTemp = function (data)
	{
		return $http.get('/API/mongoUsers/' + data)
		.then(function (response)
		{
			return response;
		},
		function (response)
		{
			return response;
		})
	} /* loginService.userloginTemp */

	loginService.getStationDetail = function ()
	{
		var url = '/API/packStation/'
		return $http.get(url)
		.then(function (response)
		{
			return response
		},
		function (response)
		{
			return response;
		})
	} /* loginService.getStationDetail */

	loginService.getQCStationDetail = function(){
		var url = '/API/qcStation/';
		return $http.get(url).then(function(response){
			return response;
		}, function (response){
			return response;
		});
	} /* loginService.getQCStationDetail */

	loginService.getLoggedInUser = function(){
		var url = '/API/LoggedInUser/';
		return $http.get(url).then(function(response){
			return response;
		}, function (response){
			return response;
		});
	} /* loginService.getLoggedInUser */
});
