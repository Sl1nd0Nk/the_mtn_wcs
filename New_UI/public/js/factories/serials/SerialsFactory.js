/*
 * @Author: Deon Wolmarans
 * @Date: 2017-06-05 14:29:06
 * @Last Modified by: Ntokozo Majozi
 * @Last Modified time: 2018-01-11 15:09:23
 */

/**
* @factory SerialsFactory
* factory - this was used to get serial numbers from the db. this has now changed to use services, rather than a factory.
*/

app.factory('SerialsFactory', function ($http) {
    return {
        getSerials: function (serial) {
            return $http({
                method: 'GET',
                url: 'http://10.211.110.138:2021/warehouseExpert/serial?serial=' + serial
            })
        }
    }
})
