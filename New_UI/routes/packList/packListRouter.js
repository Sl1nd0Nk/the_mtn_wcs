/*
 * @Author: Ntokozo Majozi
 * @Date: 2017-07-04 09:36:12
 */

var apexLib = require('apex_lib/moduleIndex.js')
var Reprint = require('../../printDocs/MultiReprint')
var ReprintSingle = require('../../printDocs/ReprintLabel')
var express = require('express')
var router = express.Router()

var mongoClient = require('mongodb')
var ObjectID = mongoClient.ObjectID

// Get all the Pack lists.
router.get('/API/packList/', function (req, res) {
    apexLib.mongo.get.mongoMultiGet(global.mongo.db, 'packLists', '', function (error, doc) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(doc)
        }
    })
})

//Post re-print
router.post('/reprint/:ToDo', function(req, res) {
    console.log('Order ID: ' + req.params.ToDo)
    Reprint.multiPrint(req.params.ToDo, function(){})
})

router.post('/reprintSingle/:ToDo', function(req, res) {
    console.log('Carton ID: ' + req.params.ToDo)
    ReprintSingle.SinglePrint(req.params.ToDo, function(resp){
        res.send(resp)
    })
})

// Get a pack list per carton ID.
router.get('/API/packListCarton/:id', function (req, res) {
    apexLib.mongo.get.mongoSingleGet(global.mongo.db, 'packLists', { cartonId: req.params.id }, function (error, doc) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(doc)
        }
    })
})

// Get a pack list per transporter ID.
router.get('/API/packListTransporter/:id', function (req, res) {
    apexLib.mongo.get.mongoSingleGet(global.mongo.db, 'packLists', { transporterId: req.params.id }, function (error, doc) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(doc)
        }
    })
})

// Post a pack list.
router.post('/API/packList/', function (req, res) {
    var packlist = {
        cartonId: req.body.cartonId,
        carton: req.body.carton,
        transporterId: req.body.transporterId,
        cartons: req.body.cartons,
        _id: req.body._id
    }

    apexLib.mongo.insert.mongoSingleInsert(global.mongo.db, 'packLists', packlist, function (error, doc) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(doc)
        }
    })
})

// Update a pack list.
router.put('/API/packList/', function (req, res) {
    var updateDocId = req.body._id

    var packlist = {
        cartonId: req.body.cartonId,
        carton: req.body.carton,
        transporterId: req.body.transporterId,
        cartons: req.body.cartons,
        _id: req.body._id

    }

    apexLib.mongo.update.mongoSingleUpdate(global.mongo.db, 'packLists', { _id: new ObjectID(updateDocId) }, packlist, function (error, doc) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(doc)
        }
    })
})

// Delete a pack list.
router.delete('/API/packList/:id', function (req, res) {
    var id = req.params.id

    apexLib.mongo.delete.mongoSingleDelete(global.mongo.db, 'packLists', { _id: new ObjectID(id) }, function (error, doc) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(doc)
        }
    })
})

module.exports = router
