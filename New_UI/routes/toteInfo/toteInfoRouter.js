var express = require('express')
var PickList = require('pickListModel')

var router = express.Router()

var toteInfo = {}
toteInfo.items = []
toteInfo.orders = []

router.get('/fetchItems/:ToDo', function(req, res) {
    var barcode = req.params.ToDo

    PickList.findOne({toteID: barcode}, function(err, docs) {
        if(!err && docs) {
            res.send({code: '00', message: 'success', data: docs})
        } else {
            res.send({code: '01', message: 'Could not find the tote information for this tote.'})
        }
    })
})

router.post('/API/toteInfo/:ToDo', function (req, res) {
    if (toteInfo.orders.length > 0) {
        toteInfo.orders = []
        if (toteInfo.orders.length === 0) {
            clearItems()
        }
    } else {
        clearItems()
    }

    function clearItems () {
        if (toteInfo.items.length > 0) {
            toteInfo.items = []
            if (toteInfo.items.length < 1) {
                processTote()
            }
        } else {
            processTote()
        }
    }

    function processTote () {
        console.log('CLEAR PROCESS TOTE')
        var toteID = req.params.ToDo
        PickList.findOne({toteID: toteID}, function (err, data) {
            if (!err && data) {
                if (data.orders.length === 1) {
                    toteInfo.detailName = 'Single Order'
                    toteInfo.numTotes = data.orders[0].numberOfTotes
                    toteInfo.toteID = toteID
                    toteInfo.orderId = data.orders[0].orderID
                    toteInfo.items = data.orders[0].items
                    toteInfo.orderPicked = data.orders[0].picked
                    toteInfo.inUse = data.toteInUs
                    toteInfo.inUseBy = data.toteInUseBy
                    toteInfo.orders = data.orders
                    res.send({code: '00', message: 'success', data: toteInfo})
                } else if (data.orders.length > 1) {
                    console.log('found multiple orders')
                    toteInfo.detailName = 'Multi Order'
                    toteInfo.toteID = toteID
                    var x = 0
                    while (x < data.orders.length) {
                        toteInfo.orders.push(data.orders[x])
                        console.log('mix orders')
                        // if (data.orders[x].picked && !data.orders[x].packed) {
                        var p = 0
                        while (p < data.orders[x].items.length) {
                            console.log('push items')
                            toteInfo.items.push(data.orders[x].items[p])
                            p++
                        }
                        // }
                        x++
                    }
                    // console.log('**** | ', toteInfo.items, ' | ****')
                    res.send({code: '00', message: 'success', data: toteInfo})
                }
            } else {
                res.send({code: '01', message: 'FAIL'})
            }
        })
    }
})

router.post('/API/getItemByOrder/:ToDo', function (req, res) {
    var orderID = req.params.ToDo
    toteInfo.items = []
    PickList.find({'orders.orderID': orderID}, function (err, docs) {
        if (!err && docs) {
            var y = 0
            while (y < docs.length) {
                var x = 0
                while (x < docs[y].orders.length) {
                    if (docs[y].orders[x].orderID === orderID) {
                        var z = 0
                        while (z < docs[y].orders[x].items.length) {
                            console.log('______________________DO WE EVER MAKE IT HERE? ')
                            toteInfo.items.push(docs[y].orders[x].items[z])
                            z++
                        }
                    }
                    x++
                }
                y++
            }
            res.send({code: '00', message: 'success', data: toteInfo})
        } else {
            res.send({code: '01', message: 'success', data: docs})
        }
    })
})

module.exports = router
