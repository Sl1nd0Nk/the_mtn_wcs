var apexLib = require('apex_lib/moduleIndex.js')
var express = require('express')
var router = express.Router()

// Get the system parameter groups.
router.get('/API/systemParameterGroup/', function (req, res) {
    apexLib.mySQL.query.execute(global.mySQLClient, 'call sys_param_group_get();', '', function (error, rows) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(rows[0])
        }
    })
})

// Get a system parameter group per ID.
router.get('/API/systemParameterGroup/:id', function (req, res) {
    apexLib.mySQL.query.execute(global.mySQLClient, 'call sys_param_group_get_id(?);', req.params.id, function (error, rows) {
        if (error) {
            return res.status(405).send(error)
        } else {
            if (rows[0].length === 0) return res.status(405).send('No System Parameter Group Item Found.')
            else return res.status(200).send(rows[0])
        }
    })
})

// Post a system parameter group.
router.post('/API/systemParameterGroup/', function (req, res) {
    var name = req.body.name
    var description = req.body.description

    var parameters = [name, description]

    apexLib.mySQL.query.execute(global.mySQLClient, 'call sys_param_group_insert(?, ?);', parameters, function (error, rows) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send('Success')
        }
    })
})

// Put a system parameter group.
router.put('/API/systemParameterGroup/', function (req, res) {
    var id = req.body.id
    var name = req.body.name
    var description = req.body.description

    var parameters = [id, name, description]

    apexLib.mySQL.query.execute(global.mySQLClient, 'call sys_param_group_update(?, ?, ?);', parameters, function (error, rows) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send('Success')
        }
    })
})

// Delete a system parameter group.
router.delete('/API/systemParameterGroup/', function (req, res) {
    var id = req.body.id

    var parameters = [id]

    apexLib.mySQL.query.execute(global.mySQLClient, 'call sys_param_group_delete(?);', parameters, function (error, rows) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send('Success')
        }
    })
})

module.exports = router
