var apexLib     = require('apex_lib/moduleIndex.js')
var express     = require('express')
var router      = express.Router()
var Parameters  = require('parameterModel')

// Get the system parameters.
router.get('/getParameters/', function (req, res) {
    Parameters.find({}, function (err, pars) {
        if (!err && pars) {
            res.send({ code: '00', message: 'success', data: pars })
        } else {
            res.send({ code: '01', message: 'fail', data: err })
        }
    })
})

router.get('/API/getParameters/', function (req, res) {
    Parameters.find({}, function (err, data) {
        if (!err && data) {
            res.send({ code: '00', message: 'success', data: data })
        } else {
            res.send({ code: '01', message: 'fail', data: err })
        }
    })
})

router.post('/changeCourier/:ToDo', function (req, res) {
    var courierData = JSON.parse(req.params.ToDo)
    var foundCourier = false
    var foundRouteNumber = false

    Parameters.find(function (error, docs) {
        if (!error && docs) {
            if (docs.length > 0) {
                var y = 0
                while (y < docs[0].courierRoutes.length) {
                    if (docs[0].courierRoutes[y].routeNumber == courierData.newRoute && docs[0].courierRoutes[y].courierName !== courierData.oldCourier) {
                        foundRouteNumber = true
						foundCourier = true
                    }
                    y++
                }

                if (!foundRouteNumber) {
                    var x = 0
                    while (x < docs[0].courierRoutes.length) {
                        if (docs[0].courierRoutes[x].courierName == courierData.oldCourier) {
                            foundCourier = true

                            docs[0].courierRoutes[x].courierName = courierData.newCourier
                            docs[0].courierRoutes[x].routeNumber = courierData.newRoute

                            docs[0].save(function (saveError, saveDocs) {
                                if (!saveError && saveDocs) {
                                    res.send({ code: '00', message: 'success', data: saveDocs })
                                } else {
                                    res.send({ code: '01', message: 'failed to save the changes made. contact support.' })
                                }
                            })
                            break;
                        } 
                        x++
                    }
                } else {
                    res.send({ code: '01', message: 'Route number,' + courierData.newRoute + ' already exists.', data: error })
                }

                if (!foundCourier) {
                    res.send({ code: '01', message: 'fail, parameters not found.', data: error })
                }
            } else {
                res.send({ code: '01', message: 'fail, parameters not found.', data: error })
            }
        } else {
            res.send({ code: '01', message: 'fail, parameters not found.', data: error })
        }
    })
})

router.get('/updateCourierParam/:Todo', function (req, res) {
    var courierData = JSON.parse(req.params.Todo) 
    var alreadyExists = false
    var routeNumExists = false
    Parameters.find(function (error, docs) {
        if (!error && docs) {
            if (docs.length > 0) {
                var x = 0
                while (x < docs[0].courierRoutes.length) {
                    if (docs[0].courierRoutes[x].courierName == courierData.courierName) {
                        alreadyExists = true
                        break;
                    } 

                    x++
                }

                if (!alreadyExists) {
                    var y = 0
                    while (y < docs[0].courierRoutes.length) {
                        if (docs[0].courierRoutes[y].routeNumber == courierData.routeNumber) {
                            routeNumExists = true
                            break
                        }
                        y++
                    }
                }

                if (!alreadyExists && !routeNumExists) {
                    var courierRoutes = {}
                    courierRoutes.courierName = courierData.courierName
                    courierRoutes.routeNumber = courierData.routeNumber

                    docs[0].courierRoutes.push(courierRoutes)

                    docs[0].save(function (saveError, saveDocs) {
                        if (!saveError, saveDocs) {
                            res.send({ code: '00', message: 'success ' + courierData.courierName + ' already exists.', data: docs })
                        } else {
                            res.send({ code: '01', message: 'Failed to save courier company. ' + courierData.courierName, data: docs })
                        }
                    })
                } else {
                    res.send({ code: '01', message: 'Courier company already exists ' + courierData.courierName + ' already exists.', data: docs })
                }
            } else {
                res.send({ code: '01', message: 'Parameter Doc empty.', data: error })
            }
        } else {
            res.send({ code: '01', message: 'fail, parameters not found.', data: error })
        }
    })
})



router.get('/removeCourier/:ToDo', function (req, res) {
    console.log('________________________' + req.params.ToDo)
    Parameters.find(function (error, docs) {
        if (!error && docs) {
            var foundCourier = false

            var x = 0
            while (x < docs[0].courierRoutes.length) { 
                if (docs[0].courierRoutes[x].courierName == req.params.ToDo) {
                    var foundCourier = true

                    docs[0].courierRoutes.splice(x, 1)

                    docs[0].save(function (saveError, saveDocs) {
                        if (!saveError && saveDocs) {
                            res.send({ code: '00', message: 'Success', data: saveDocs })
                        } else {
                            res.send({ code: '01', message: 'Courier comany: ' + req.params.ToDo + ' not found. ' })
                        }
                    })
                    break
                }
                x++
            }

            if (!foundCourier) {
                res.send({ code: '01', message: 'Courier comany: ' + req.params.ToDo + ' not found. ' })
            }
        } else {
            res.send({ code: '01', message: 'Courier comany: ' + res.params.ToDo + ' not found. DB problem contact support.' })
        }
    })
})

router.post('/updateWeightData/:ToDo', function (req, res) {
    var weightData = JSON.parse(req.params.ToDo)
    Parameters.find(function (error, docs) {
        if (!error && docs) {
            if (docs.length > 0) {
                var foundWeights = false
                var x = 0
                while (x < docs[0].weightSettings.length) {
                    if (docs[0].weightSettings[x].StockSize == weightData.stockSizeOld) {
                        foundWeights = true

                        docs[0].weightSettings[x].UpperTolerance    = weightData.upper
                        docs[0].weightSettings[x].LowerTolerance    = weightData.lower
                        docs[0].weightSettings[x].weight            = weightData.weight
                        docs[0].weightSettings[x].useTransporter    = weightData.transporter
                        docs[0].weightSettings[x].width             = weightData.width
                        docs[0].weightSettings[x].height            = weightData.height

                        docs[0].save(function (saveError, saveDocs) {
                            if (!saveError && saveDocs) {
								console.log('successfully saved: ' + weightData.upper)
                                res.send({ code: '00', message: 'success', data: saveDocs })
                            } else {
								console.log('unsuccessfull at all. You need to save again!!')
                                res.send({ code: '01', message: 'failed to update. Contact support.' })
                            }
                        })
						break
                    }
                    x++
                }

                if (!foundWeights) {
                    res.send({ code: '01', message: 'Carton stocksize was not found.' })
                }
            }
        } else {
            res.send({ code: '01', message: 'Weight data information not found. Please contact support.' })
        }
    })
})

router.post('/addWeight/:Todo', function (req, res) {
    var addData = JSON.parse(req.params.Todo)
    Parameters.find(function (error, docs) {
        if (!error && docs) {
            if (docs.length > 0) {
                var foundStock = false 
                var x = 0
                while (x < docs[0].weightSettings.length) {
                    if (docs[0].weightSettings[x].StockSize == addData.StockSize) {
                        foundStock = true
                    }
                    break
                    x++
                }

                if (foundStock) {
                    res.send({ code: '01', message: 'This stocksize carton already exists.' })
                } else {
                    var addJson = {}

                    addJson.StockSize       = addData.StockSize 
                    addJson.UpperTolerance  = addData.UpperTolerance 
                    addJson.LowerTolerance  = addData.LowerTolerance
                    addJsonisCarton         = false, 
                    addJson.weight          = addData.weight
                    addJson.weightCapacity  = 2000, 
                    addJson.volume          = 1104000, 
                    addJson.volumeCapacity  = 1104000, 
                    addJson.useTransporter  = addData.useTransporter
                    addJson.useTransporterForBag = true, 
                    addJson.length          = 400, 
                    addJson.width           = addData.width
                    addJson.height          = addData.height

                    docs[0].weightSettings.push(addJson)

                    docs[0].save(function (saveError, saveDocs) {
                        if (!saveError && saveDocs) {
                            res.send({ code: '00', message: 'success', data: 'saveDocs' })
                        } else {
                            res.send({ code: '01', message: 'Failed to save new weight settings' })
                        }
                    })
                }
            } else {
                res.send({ code: '01', message: 'Parameters not found.' })
            }
        } else {
            res.send({ code: '01', message: 'Parameters not found. Please contact support.' })
        }
    })
})


router.post('/addOrderType/:ToDo', function(req, res) {

    var otData = JSON.parse(req.params.ToDo)

    Parameters.find(function (err, docs) {
        if (!err && docs) {
            if (docs.length > 0) {
                var found = false
                var i = 0
                while (i < docs[0].orderTypes.length) {
                    if (docs[0].orderTypes[i].orderType === otData.ordertype) {
                        found = true
                        break
                    }
                    i++
                }
                if (found) {
                    res.send({ code: '01', message: 'Order Type ' + otData.ordertype + ' already exists.' })
                } else {
                    var addJSON                 = {}
                    addJSON.orderType           = otData.ordertype
                    addJSON.maxOrdersPerTote    = otData.maxorders
                    docs[0].orderTypes.push(addJSON)
                    docs[0].save(function (saveError, saveDocs) {
                        if (!saveError && saveDocs) {
                            res.send({ code: '00', message: 'success', data: saveDocs })
                        } else {
                            res.send({ code: '01', message: 'This Order Type could not be saved.' })
                        }
                    })
                }
            } else {
                res.send({ code: '01', message: 'Parameters not found.' })
            }
        } else {
            res.send({ code: '01', message: 'Parameters not found. Please contact support.' })
        }
    })

})

router.post('/editOrderType/:ToDo', function (req, res) {

    var otData = JSON.parse(req.params.ToDo)

    Parameters.find(function (err, docs) {
        if (!err && docs) {
            if (docs.length > 0) {
                var found = false
                var i = 0
                while (i < docs[0].orderTypes.length) {
                    if (docs[0].orderTypes[i].orderType == otData.orderType) {
                        found = true
                        docs[0].orderTypes[i].maxOrdersPerTote = otData.maxOrdersPerTote
                        break
                    }
                    i++
                }
                if (found) {
                    docs[0].save(function (saveError, saveDocs) {
                        if (!saveError && saveDocs) {
                            res.send({ code: '00', message: 'success', data: saveDocs })
                        } else {
                            res.send({ code: '01', message: 'This Order Type could not be saved.' })
                        }
                    })
                } else {
                    res.send({ code: '01', message: 'Order Type ' + addData.ordertype + ' was not found.' })
                }
            }
        } else {
            res.send({ code: '01', message: 'Order Type data not found. Please contact support.' })
        }
    })

})

router.post('/deleteOrderType/:ToDo', function (req, res) {

    Parameters.find(function (err, docs) {
        if (!err && docs) {
            var found = false
            var i = 0
            while (i < docs[0].orderTypes.length) {
                if (docs[0].orderTypes[i].orderType == req.params.ToDo) {
                    found = true
                    docs[0].orderTypes.splice(i, 1)
                    break
                }
                i++
            }
            if (found) {
                docs[0].save(function (saveError, saveDocs) {
                    if (!saveError && saveDocs) {
                        res.send({ code: '00', message: 'Success', data: saveDocs })
                    } else {
                        res.send({ code: '01', message: 'Order Type ' + req.params.ToDo + ' could not be saved.' })
                    }
                })
            } else {
                res.send({ code: '01', message: 'Order Type ' + req.params.ToDo + ' was not found.' })
            }
        } else {
            res.send({ code: '01', message: 'Order Type data not found. Please contact support.' })
        }
    })

})

module.exports = router
