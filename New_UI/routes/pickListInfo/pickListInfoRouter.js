var express = require('express')
var PickList = require('pickListModel')
var CourierNoRead = require('courierNoRead')
var async = require('async');
var moment = require('moment')
var router = express.Router()

var statusCount = {}
statusCount.statusNewAdd = 0
statusCount.statusPickedAdd = 0
statusCount.statusPackedAdd = 0
statusCount.statusShippedAdd = 0

router.post('/API/pickListInfo/', function (req, res) {
    statusCount.statusNew = 0;
    statusCount.statusStarted = 0;
    statusCount.statusPicked = 0;
    statusCount.statusPacked = 0;
    statusCount.statusStored = 0;
    statusCount.statusShipped = 0;
    statusCount.statusCancelled = 0;
	statusCount.statusNewAdd = 0;
	statusCount.statusStartedAdd = 0;
	statusCount.statusPickedAdd = 0;
	statusCount.statusPackedAdd = 0;
	statusCount.statusShippedAdd = 0;
	statusCount.statusStoredAdd = 0;
	statusCount.statusCancelledAdd = 0;
	//let theData = new Date().toISOString().substr(0, 10);
    //let regVal = '\.*'+theData+'\.';
    let MyDate = moment(new Date()).format('YYYY-MM-DD');

    PickList.find({$or: [{Status: 'NEW'}, {Status: 'PICKED'}, {Status: 'STARTED'}, {Status: 'STORED'}, {Status: 'CANCELED'},{Status: 'SHIPPED', PackedDate: {$regex: MyDate, $options:'i'}}, {Status: 'PACKED', PackedDate: {$regex: MyDate, $options:'i'}}]}, function (err, docs) {
        if(err || !docs){
            return res.send({code: '01', message: 'fail', data: err})
        }

		let x = 0;
		while (x < docs.length) {
			if (docs[x].Status) {
				if (docs[x].Status === 'NEW') {
					statusCount.statusNew = statusCount.statusNew + 1;
					statusCount.statusNewAdd = statusCount.statusNewAdd + 1;
				} else if (docs[x].Status === 'STARTED') {
					statusCount.statusStarted = statusCount.statusStarted + 1;
					statusCount.statusStartedAdd = statusCount.statusStartedAdd + 1;
				} else if (docs[x].Status === 'PICKED') {
					statusCount.statusPicked = statusCount.statusPicked + 1;
					statusCount.statusPickedAdd = statusCount.statusPickedAdd + 1;
				} else if (docs[x].Status === 'PACKED') {
					statusCount.statusPacked = statusCount.statusPacked + 1;
					statusCount.statusPackedAdd = statusCount.statusPackedAdd + 1;
				} else if (docs[x].Status === 'STORED') {
					statusCount.statusStored = statusCount.statusStored + 1;
					statusCount.statusStoredAdd = statusCount.statusStoredAdd + 1;
				} else if (docs[x].Status === 'SHIPPED') {
					statusCount.statusShipped = statusCount.statusShipped + 1;
					statusCount.statusShippedAdd = statusCount.statusShippedAdd + 1;
				} else if (docs[x].Status === 'CANCELED') {
					statusCount.statusCancelled = statusCount.statusCancelled + 1;
					statusCount.statusCancelledAdd = statusCount.statusCancelledAdd + 1;
				} else {
					console.log('there is an other');
				}
			}
			x++;
		}

		let respData = {code: '00', message: 'success', data: statusCount, pickLists: docs};
		return res.send(respData);
    });
});

function CreateSendData(OrigPL, Cancelled){
	let Consolidated = false;

	if(OrigPL.PickListType == "WCS_MASTER"){
		Consolidated = true;
	}

	let Ords = [];
	if(OrigPL.orders.length == 1){
		Ords = OrigPL.orders;
	}

	let PToPush = {
			PickRegion: OrigPL.PickRegion,
			PickType: OrigPL.PickType,
			PickListType: OrigPL.PickListType,
			CreateDate: OrigPL.CreateDate,
			Status: Cancelled == null? OrigPL.Status: Cancelled,
			PackedDate: OrigPL.PackedDate,
			toteInUseBy: OrigPL.toteInUseBy,
			toteInUse: OrigPL.toteInUse,
			packType: OrigPL.packType,
			containerLocation: OrigPL.containerLocation,
			transporterID: OrigPL.transporterID,
			toteID: OrigPL.toteID,
			consolidated: Consolidated,
			orders: Ords
	};

	return PToPush
}

router.post('/API/pickListInfoNew/', function (req, res) {
	let Stack = {};
	let theDate = moment(new Date()).format('YYYY-MM-DD');

	let ReturnFields = {"PickRegion" : 1, "PickType" : 1, "PickListType" : 1,
					    "CreateDate" : 1, "orders": 1, "Status" : 1, "PackedDate" : 1,
					    "toteInUseBy" : 1, "toteInUse" : 1, "packType" : 1, "containerLocation" : 1,
					    "transporterID" : 1, "toteID" : 1};

	Stack.PicksNew = function(callback){
		PickList.find({Status: 'NEW'}, ReturnFields, function(err, nPL){
			if(err){
				return callback(err, {Count: 0, PLArr: null});
			}

			let PLArr = [];
			if(nPL.length <= 0){
				return callback(null, {Count: 0, PLArr: null});
			}

			let x = 0;
			while(x < nPL.length){
				if(nPL[x].orders.length > 1){
					let newPLorders = nPL[x].orders;
					let y = 0;
					while(y < newPLorders.length){
						if(newPLorders[y].WmsOrderStatus != 'CANCELED' && newPLorders[y].WmsOrderStatus != 'CANCELLED' && newPLorders[y].WmsPickListStatus != 'CANCELED' && newPLorders[y].WmsPickListStatus != 'CANCELLED'){
							let PToPush = CreateSendData(nPL[x], null);
							PToPush.orders.push(newPLorders[y]);
							PLArr.push(PToPush);
						}
						y++;
					}
				} else {
					let PToPush = CreateSendData(nPL[x], null);
					PLArr.push(PToPush);
				}
				x++;
			}

			return callback(null, {Count: PLArr.length, PLArr:PLArr});
		});
	}

	Stack.PicksStarted = function(callback){
		PickList.find({Status: 'STARTED'}, ReturnFields, function(err, staPL){
			if(err){
				return callback(err, {Count: 0, PLArr: null});
			}

			let PLArr = [];
			if(staPL.length <= 0){
				return callback(null, {Count: 0, PLArr: null});
			}

			let x = 0;
			while(x < staPL.length){
				if(staPL[x].orders.length > 1){
					let newPLorders = staPL[x].orders;
					let y = 0;
					while(y < newPLorders.length){
						if(newPLorders[y].WmsOrderStatus != 'CANCELED' && newPLorders[y].WmsOrderStatus != 'CANCELLED' && newPLorders[y].WmsPickListStatus != 'CANCELED' && newPLorders[y].WmsPickListStatus != 'CANCELLED'){
							let PToPush = CreateSendData(staPL[x], null);
							PToPush.orders.push(newPLorders[y]);
							PLArr.push(PToPush);
						}
						y++;
					}
				} else {
					let PToPush = CreateSendData(staPL[x], null);
					PLArr.push(PToPush);
				}
				x++;
			}

			return callback(null, {Count: PLArr.length, PLArr:PLArr});
		});
	}

	Stack.PicksPicked = function(callback){
		PickList.find({Status: 'PICKED'}, ReturnFields, function(err, piPL){
			if(err){
				return callback(err, {Count: 0, PLArr: null});
			}

			let PLArr = [];
			if(piPL.length <= 0){
				return callback(null, {Count: 0, PLArr: null});
			}

			let x = 0;
			while(x < piPL.length){
				if(piPL[x].orders.length > 1){
					let newPLorders = piPL[x].orders;
					let y = 0;
					while(y < newPLorders.length){
						if(newPLorders[y].WmsOrderStatus != 'CANCELED' && newPLorders[y].WmsOrderStatus != 'CANCELLED' && newPLorders[y].WmsPickListStatus != 'CANCELED' && newPLorders[y].WmsPickListStatus != 'CANCELLED'){
							let PToPush = CreateSendData(piPL[x], null);
							PToPush.orders.push(newPLorders[y]);
							PLArr.push(PToPush);
						}
						y++;
					}
				} else {
					let PToPush = CreateSendData(piPL[x], null);
					PLArr.push(PToPush);
				}
				x++;
			}

			return callback(null, {Count: PLArr.length, PLArr:PLArr});
		});
	}

	Stack.PicksPacked = function(callback){
		let MyDate = moment(new Date()).format('YYYY-MM-DD');
		PickList.find({Status: 'PACKED', PackedDate: {$regex: MyDate, $options:'i'}}, ReturnFields, function(err, paPL){
			return callback(err, paPL);
		});
	}

	Stack.PicksStored = function(callback){
		PickList.find({Status: 'STORED'}, ReturnFields, function(err, stoPL){
			return callback(err, stoPL);
		});
	}

	Stack.PicksShipped = function(callback){
		let MyDate = moment(new Date()).format('YYYY-MM-DD');
		PickList.find({Status: 'SHIPPED', PackedDate: {$regex: MyDate, $options:'i'}}, ReturnFields, function(err, shPL){
			return callback(err, shPL);
		});
	}

	Stack.PicksCancelled = function(callback){
		let MyDate = moment(new Date()).format('YYYY-MM-DD');
		PickList.find({'orders.WmsOrderStatus': 'CANCELED', 'orders.CancelDate': {$regex: MyDate, $options:'i'}}, ReturnFields, function(err, canPL){
			if(err){
				return callback(err, {Count: 0, PLArr: null});
			}

			let PLArr = [];
			if(canPL.length <= 0){
				return callback(null, {Count: 0, PLArr: null});
			}

			let x = 0;
			while(x < canPL.length){
				if(canPL[x].orders.length > 1){
					let newPLorders = canPL[x].orders;
					let y = 0;
					while(y < newPLorders.length){
						if(newPLorders[y].WmsOrderStatus == 'CANCELED' || newPLorders[y].WmsOrderStatus == 'CANCELLED' || newPLorders[y].WmsPickListStatus == 'CANCELED' || newPLorders[y].WmsPickListStatus == 'CANCELLED'){
							let PToPush = CreateSendData(canPL[x], 'CANCELLED');
							PToPush.orders.push(newPLorders[y]);
							PLArr.push(PToPush);
						}
						y++;
					}
				} else {
					let PToPush = CreateSendData(canPL[x], 'CANCELLED');
					PLArr.push(PToPush);
				}
				x++;
			}

			return callback(null, {Count: PLArr.length, PLArr:PLArr});
		});
	}

	async.parallel(Stack, function(err, Result){
		let NewstatusCount = {}
		NewstatusCount.statusNew = 0;
		NewstatusCount.statusStarted = 0;
		NewstatusCount.statusPicked = 0;
		NewstatusCount.statusPacked = 0;
		NewstatusCount.statusStored = 0;
		NewstatusCount.statusShipped = 0;
		NewstatusCount.statusCancelled = 0;

		NewstatusCount.statusNewAdd = 0;
		NewstatusCount.statusStartedAdd = 0;
		NewstatusCount.statusPickedAdd = 0;
		NewstatusCount.statusPackedAdd = 0;
		NewstatusCount.statusStoredAdd = 0;
		NewstatusCount.statusShippedAdd = 0;
		NewstatusCount.statusCancelledAdd = 0;

		if(err){
			console.log(err);
		} else {
			NewstatusCount.statusNew = NewstatusCount.statusNewAdd = Result.PicksNew.Count;
			NewstatusCount.statusStarted = NewstatusCount.statusStartedAdd = Result.PicksStarted.Count;
			NewstatusCount.statusPicked = NewstatusCount.statusPickedAdd = Result.PicksPicked.Count;
			NewstatusCount.statusPacked = NewstatusCount.statusPackedAdd = Result.PicksPacked.length;
			NewstatusCount.statusStored = NewstatusCount.statusStoredAdd = Result.PicksStored.length;
			NewstatusCount.statusShipped = NewstatusCount.statusShippedAdd = Result.PicksShipped.length;
			NewstatusCount.statusCancelled = NewstatusCount.statusCancelledAdd = Result.PicksCancelled.Count;
		}

		let respData = {data: NewstatusCount, PicksNew: Result.PicksNew.PLArr,
											  PicksStarted: Result.PicksStarted.PLArr,
											  PicksPicked: Result.PicksPicked.PLArr,
											  PicksPacked: Result.PicksPacked,
											  PicksStored: Result.PicksStored,
											  PicksShipped: Result.PicksShipped,
											  PicksCancelled: Result.PicksCancelled.PLArr};
		res.send(respData)
	});
})


router.post('/API/listByOrder/:ToDo', function (req, res) {
    var orderID = req.params.ToDo
    PickList.find({'orders.orderID': orderID}, function (err, data) {
        if (!err && data) {
            res.send({code: '00', message: 'success', data: data})
        } else {
            res.send({code: '01', message: 'fail', data: err})
        }
    })
})

router.get('/getCourtierNoRead', function(req, res) {
    CourierNoRead.find(function(error, docs) {
        if(!error && docs) {
            res.send({code:'00', message:'succses', data:docs.length})
        } else {
            res.send({code:'01', message:'fail. no data found'})
        }
    })
})

module.exports = router
