var express = require('express');
var PicklistModel = require('../../../db_schema/pickListModel');
var moment = require('moment');
var bodyParser = require('body-parser');
var router = express.Router();

router.get('/API/ConsolidatedPicklist:ToDo', function (req, res) { //'/API/Toteqty'
    
    if (req.params.ToDo)
    {
        var Data = JSON.parse(req.params.ToDo);
        
        PicklistModel.findOne({toteID: Data.toteID}, function(error, doc)
        {
            
            if (!doc || error || doc.length == 0) {
                console.log('Errors');
                return res.status(400).send(' Not in the system, please enter another tote ');
            } else {
                //console.log(doc + ' AS DOCKIEZZZ! \n');
                if (doc.orders.length != 0)
                {
                var consolidated = false;
                var refOrderID = doc.orders[0].orderID;
                
                console.log('My ref Order is ' +  refOrderID + ' UName ' + Data.user);
                for (var i = 0; i < doc.orders.length; i++)
                {
                    if (doc.orders[i].orderID != refOrderID)
                    {
                        consolidated = true;
                        doc.orders[i].unconsolidatedBy = Data.user;
                    }
                }

               let countPicked = 0;
               let countAll = 0;

                for (var i = 0; i < doc.orders.length; i++)
                {
                       if (doc.orders[i].picked == true)
                        {
                            countPicked++;   
                        }
                        countAll++;
                }
                //console.log(countPicked + '     *****    All ' + countAll);
                //Count picked and set picked .....
                if (countPicked != 0)
                {
                    if (countPicked == countAll)
                    {
                        doc.Status = 'PICKED'; 
                    } 
                }

                if (consolidated)
                {
                    doc.orders[0].unconsolidatedBy = Data.user;

                    doc.save(function (err, savedDoc) {
                        return res.status(200).send(doc);
                    });  

                } else {
                  //  console.log(doc.orders.length + ' IZ MY LENNNNGGTHER ');
                    return res.status(400).send('Does not contain consolidated orders');
                }
            } else {
                return res.status(400).send(' Has no orders, please enter another one');
              }
            }
        });

    } else {
        return res.status(400).send('Enter a tote first');
    }   
});

router.get('/API/Toteqty:ToDo', function (req, res) {
    //console.log(' NN ' + tote);

    console.log("*");

    if (req.params.ToDo)
    {

        var tote = req.params.ToDo;
        console.log(' NN' + tote);
        PicklistModel.find({toteID: tote}, function(error, doc)
        {
            if (!doc || error) {
                console.log('Errors2');
                return res.status(400).send(' Not in the system, please enter another tote ');
            } else if (doc.length == 0) {

                var zeros = tote.substring(0, 4);
                var number = tote.substring(4, 7);
                var totePass = false;
                //Validate tote
                    if (tote.length == 7)
                    { 
                        if (tote.substring(0, 1) == '0')
                        {
                            if (number.length == 3)
                            {   
                                if (parseInt(number) / parseInt(number) == 1)
                                {
                                // console.log(' Pass Test ! ');
                                    totePass = true; 
                                }
                            }
                        } else {
                            return res.status(400).send('Invalid, Please enter a valid tote');
                        }

                    } else {
                        return res.status(400).send('Invalid, Please enter a tote with correct length ');   
                    }
                
                    if (totePass)
                    {
                        return res.status(200).send({message: 'Empty'});
                    } else {
                        return res.status(400).send('Invalid, Please enter a valid tote');
                    }
            } else if (doc.length != 0)
            {
                return res.status(400).send('Not empty, please enter an empty tote');   
            }
        });

    } else {
        return res.status(400).send('Enter a tote first');
    }
});

router.get('/API/Unconsolidate:ToDo', function (req, res) {
    
    if (req.params.ToDo) {

        var MyData = JSON.parse(req.params.ToDo);
        console.log(' The new Tote -> ' + MyData.oldTote);
        PicklistModel.findOne({toteID: MyData.oldTote}, function(error, doc)
        {
            let i = 0;
            let  pickListMongoSchema1 = {};
	    let myStatus = doc.Status;

                while (i < doc.orders.length) {

                if (doc.orders[i].orderID == MyData.orderID)
                {

		    if (doc.orders[i].picked == true)
		    {
			myStatus = 'PICKED';
		    }		  

                    pickListMongoSchema1 = new PicklistModel({
                    toteID: MyData.newTote,
                    transporterID: doc.transporterID,
                    destination: doc.destination,
                    containerLocation: doc.containerLocation,
                    orders: doc.orders[i], 
                    Status : myStatus,
                    FromTote : doc.toteID,
                    PickUpdateRequired: doc.PickUpdateRequired,
                    PackUpdateRequired:doc.PackUpdateRequired,
                    ShipUpdateRequired: doc.ShipUpdateRequired,
                    CancelRequired: doc.CancelRequired,
                    CreateDate: doc.CreateDate,
                    PickListType: doc.PickListType,
                    PickType: doc.PickType,
                    PickRegion: doc.PickRegion,
                    Consolidate: doc.Consolidate,
                    SmallOrder: doc.SmallOrder
                });
                doc.orders.splice(i, 1);
                break;
                }

                i++;
            }
            
            doc.save(function(err, savedDoc) {
                //console.log(savedDoc);
                  //Save the new picklist 
                  pickListMongoSchema1.save( function(error,Resp){
                    if(error){
                        console.log(error);
                        return res.status(400).send({message: 'Could not be unconsolidated', order: MyData.orderID});   
                    } else {
                        return res.status(200).send(Resp);
                    }
                })
            });  

        });
       // console.log(' My New Tote !!! ' + MyData.newTote);
    } else {
        return res.status(400).send('Enter a tote to confirm');   
    }
});

module.exports = router;
