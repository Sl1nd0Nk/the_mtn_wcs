var express         = require('express');
var alarmDBModel    = require('../../module/DBAcces/AlarmDBAccessModule');
var reportDBModule  = require('../../module/reportsDBAccess/reportsDBAccess')
// var Params          = require('../../config/alarmAndEvent.json');
var WeightScaleRejects = require('WeightScaleRejectsModel')
var UserErr            = require('UserPackErrorModel');

var router = express.Router();


// ===================SUB-SYSTEM COMMS
router.get('/subSystemComs', function (req, res) {
    alarmDBModel.getSubSystemFails(function(dataResp){
        res.send(dataResp)
    })
});

router.post('/updateStemComms', function(req, res){
    alarmDBModel.updateSubsystem(function(dataResp){
        res.send(dataResp)
    })
});

// ===================REJECTED QC
router.get('/getRejectedQC', function(req, res){
    alarmDBModel.getRejectedQC(function(dataResp){
        res.send(dataResp)
    })
})

router.post('/updateQCRejectError', function(req, res){
    alarmDBModel.updateQCReject(function(dataResp){
        res.send(dataResp)
    })
})

// ===================FAILED LOGIN
router.get('/getFailedLogin', function(req, res){
    alarmDBModel.getFailedLogin(function(dataResp){
        res.send(dataResp)
    })
})

router.post('/updateFailed', function(req, res){
    res.send({code:'00', message:'success'})
})

// =================== COURIER WEIGHT FAIL
router.get('/getCourierWeightFail', function(req, res){
    alarmDBModel.courierWeightFail(function(dataResp){
        res.send(dataResp)
    })
})

router.post('/updateCourierWeight', function(req, res){
    alarmDBModel.updateCourierWeight(function(dataResp){
        res.send(dataResp)
    })
})

// =================== PICKER NOT LOGGED
router.get('/getPickerNotLogged', function(req, res){
    alarmDBModel.pickerNotLogged(function(dataResp){
        res.send(dataResp)
    })
})

// =================== PICKER NOT LOGGED
router.get('/getPickerNotReleasing', function(req, res){
    alarmDBModel.pickerNotReleasing(function(dataResp){
        res.send(dataResp)
    })
})

// =================== PACKER NOT PACKING
router.get('/getPackerNotPacking', function(req, res){
    alarmDBModel.pickerNotReleasing(function(dataResp){
        res.send(dataResp)
    })
})

// *************************************************************************************************************************

// =================== QC REJECT VIEW
router.get('/getRejectView', function(req, res){
    reportDBModule.qcRejectView(function(dataResp){
        res.send(dataResp)
    })
})

router.get('/getWeightScaleRejects/:id', function(req, res){
	let Date = req.params.id;
	let start = '^';
	let end = '/';

	let queryVal = new RegExp(start+Date);
	console.log('Date ' + queryVal);

    WeightScaleRejects.find({'DateRejected':{$regex:queryVal}}).sort({DateRejected: 1}).exec(function(err, Data){
		if(!err){
			if(Data){
				res.send({Data: Data});
		    } else {
				res.send({Msg: 'No Data Found'});
			}
		} else {
			console.log('getWeightScaleRejects error: ' + err);
			res.send({Msg: 'Error while trying to retrieve data'});
		}
    })
})

router.get('/getPackingErrors/:id', function(req, res){
	let Date = req.params.id;
	let start = '^';
	let end = '/';

	let queryVal = new RegExp(start+Date);
	console.log('Date ' + queryVal);

	UserErr.find({'Date':{$regex:queryVal}}).sort({Date: 1}).exec(function(err, Data){
		if(!err){
			if(Data){
				res.send({Data: Data});
			} else {
				res.send({Msg: 'No Data Found'});
			}
		} else {
			console.log('getPackingErrors error: ' + err);
			res.send({Msg: 'Error while trying to retrieve data'});
		}
	});
});

router.get('/getPicklistItem/:ToDo', function(req, res){
    reportDBModule.qcRejectGetItems(req.params.ToDo, function(dataResp){
        res.send(dataResp)
    })
})

// *************************************************************************************************************************

// =================================NOTIFICATIONS
router.post('/getNotifications', function(req, res){
    var count = 0

    // console.log('************************DO WE MAKE IT IN HERE? ***********************************')

    start()

    function start(){
        alarmDBModel.getSubSystemFails(function(dataResp){
            if(dataResp.code == '00' && dataResp.data.length > 0){
                console.log('***** | SUBSYSTEM' + dataResp.data.length)
                count += 1
                rejectedQC()
            }else{
                console.log('subsystems')
                rejectedQC()
            }
        })
    }

    function rejectedQC(){
        alarmDBModel.getRejectedQC(function(dataResp){
            if(dataResp.code == '00' && dataResp.data.length > 0){
                console.log('***** | REJECTED QC' + dataResp.data.length)
                count += 1
                failedLogin()
            }else{
                console.log('rejected qc')
                failedLogin()
            }
        })
    }

    function failedLogin(){
        alarmDBModel.getFailedLogin(function(dataResp){
            if(dataResp.code == '00' && dataResp.data.length > 0){
                console.log('***** | FAILED LOGIN ' + dataResp.data.length)
                count += 1
                notReleasing()
            }else{
                console.log('failedLogin')
                notReleasing()
            }
        })
    }

    function notReleasing(){
        alarmDBModel.pickerNotReleasing(function(dataResp){
            if(dataResp.code == '00' && dataResp.data.length > 0){
                console.log('***** | PICKER NOT RELEASING' + dataResp.data.length)
                count += 1
                notLoggedIn()
            }else{
                console.log('picker not releasing')
                notLoggedIn()
            }
        })
    }

    function notLoggedIn (){
        alarmDBModel.pickerNotLogged(function(dataResp){
            if(dataResp.code == '00' && dataResp.data.length > 0){
                console.log('***** | PICKER NOT LOGGED ' + dataResp.data.length)
                count += 1
                courierWight()
            }else{
                console.log('picker not logged in')
                courierWight()
            }
        })
    }

    function courierWight(){
        alarmDBModel.courierWeightFail(function(dataResp){
            if(dataResp.code == '00' && dataResp.data.length > 0){
                console.log('***** | COURIER WEIGHT FAIL ' + dataResp.data.length)
                count += 1
                callCallback()
            }else{
                console.log('courier weight fail')
                callCallback()
            }
        })
    }

    var callCallback = function(){
        res.send({total:count})
    }
})

module.exports = router;

// *************************************************************************************************************************
