var express = require('express');
var PicklistModel = require('../../../db_schema/pickListModel');
var UnpickUpdate = require('UnpickUpdatesModel');
var moment = require('moment');
var bodyParser = require('body-parser');
var router = express.Router();

router.get('/API/unpickaTote:ToDo', function (req, res) {

    var tote = req.params.ToDo;

    if (!tote)
    {
        res.status(400).send('Enter toteID first');
    }

        PicklistModel.find({toteID: tote}, function(error, doc)
        {

                if (doc && !error && doc.length > 0) {
                console.log(' Stored session '+ req.session.sessionTote + '   LENGTH   ' + doc.length);

                if (doc[0].orders.length != 0)
                {
                var toteStatus = doc[0].Status;
                var mainOrder = doc[0].orders[0].orderID;
                var orderStatus = 'pass';

                if (toteStatus.toUpperCase() == 'NEW' || toteStatus.toUpperCase() == 'STARTED' || toteStatus.toUpperCase() == 'PICKED')
                {///You might have to make some changes here SLI, because after recalculating once the tote is picked this will reject it.
                    console.log(toteStatus + ' My tote'); //Validation

                    for (var i = 0; i < doc[0].orders.length; i++) {
                        if (doc[0].orders[i].orderID != mainOrder) {
                            orderStatus = 'fail';
                        }
                    }

                    if (orderStatus != 'fail') {
                        req.session.sessionTote = doc[0].toteID;
                        console.log( ' Success' + ' Session ' + req.session.sessionTote);
                        res.status(200).send({Data: doc, sessionTote: req.session.sessionTote});
                    } else {
                            console.log('1');
                            res.status(400).send({tote: tote, message: 'Contains more than one Order, please unconsolidate '});
                    }
                   //
                } else {
                        console.log('2');
                        res.status(400).send({tote: tote, message: 'In ' + toteStatus + ' please scan another tote '});
                }
             } else {
                    console.log('3');
                    res.status(400).send({tote: tote, message: ' Currently has no orders in it '});
             }
            } else {
                    console.log('4');
                    res.status(400).send({tote: tote, message: ' is not in the system, please enter another tote '});
            }
        });
});

router.get('/API/Getpicklists:ToDo', function(req, res) {

    if (req.params.ToDo)
    {
        var MyData =  JSON.parse(req.params.ToDo);
        //console.log(MyData.picklists[0] + ' *** RAM \n\n');
        //console.log(MyData.orderID + ' *** RAM \n\n');
        //console.log(MyData.orderID + ' *** RAM \n\n');
        //console.log(MyData.orderID + ' *** RAM \n\n');
        console.log(MyData.toteID + ' RAMZy ');
        PicklistModel.find({'toteID' : MyData.toteID}, function(error, doc)
        {

            //var j = 0;
            var k = 0;
            var foundCount = 0;
            var lostPicklists = [];
            var foundPicklists = [];
            let foundOrders = [];

            if (doc && !error && doc.length != 0) {
                //console.log(' -> ' + doc[0].orders[0].pickListID + '  ***88***  ' + MyData.picklists[0]);
                for (var i = 0; i <  doc[0].orders.length;i++)
                {

                        for (var j = 0; j <  MyData.picklists.length; j++)
                        {
                        console.log(' -> ' + doc[0].orders[i].pickListID + '  *** MATCH ***  ' + j + ' ** ' + MyData.picklists[j]);

                            if (doc[0].orders[i].pickListID == MyData.picklists[j])
                            {
                                foundCount++;
                                foundPicklists[k] = MyData.picklists[j];
                                foundOrders[k] = doc[0].orders[i].orderID;
                                console.log(doc[0].orders[i].orderID + " **** TstT");
                                k++;
                            }
                        }
                }

            res.status(200).send({orderID: foundOrders, picklists: foundPicklists, toteID: doc[0].toteID});

            } else {
                res.status(400).send('Picklists not found or OrderID for picklists has changed');
            }
        });
    }
});

function SeparatePlsFromTote(PlArr, index, user, callback){
    if(index >= PlArr.length){
        return callback();
    }

    //console.log(PlArr[0] + '      ********* THE END ********** ');
    var newPicklist = new PicklistModel(PlArr[index]);
    newPicklist.save(function(err, savedDoc){
		CreateUnPickUpdate(savedDoc, user);
        index++;
        return SeparatePlsFromTote(PlArr, index, user, callback);
    });
} /* SeparatePlsFromTote */

function CreateUnPickLineUpdate(doc, index, user, callback){
	if(index >= doc.orders[0].items.length){
		return callback();
	}

	let NewRec = new UnpickUpdate({
		PickListID: doc.orders[0].pickListID,
		WcsId: doc.FromTote,
		PickListLine: doc.orders[0].items[index].pickListOrderLine,
		OrderId: doc.orders[0].orderID,
		Required: true,
		UserId: user,
		CreateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
	});

	NewRec.save(function(err, savedDoc){
		console.log('Marked picklist: ' + doc.orders[0].pickListID + ' | Line: ' + doc.orders[0].items[index].pickListOrderLine + ' | Unpick Required to true');
		index++;
		return CreateUnPickLineUpdate(doc, index, user, callback);
	});
} /* CreateUnPickLineUpdate */

function CreateUnPickUpdate(doc, user){
	CreateUnPickLineUpdate(doc, 0, user, function(){
		return;
	});
} /* CreateUnPickUpdate */

router.get('/API/CancelPicklist:ToDo', function(req, res) {
	let user = req.session.username;
    var comp = [];
    if (req.params.ToDo)
    {
        var MyData = JSON.parse(req.params.ToDo);

        console.log(MyData);

        let cancelledCount = 0;
        PicklistModel.findOne({toteID : MyData.toteID}, function(error, doc)
        {
            if(error){
                return res.status(400).send('Picklists not found or OrderID for picklists has changed');
            }

            if(!doc){
                return res.status(400).send('Picklists not found or OrderID for picklists has changed');
            }

            let w = 0;
            let cancelledCount = 0;
            let PlArr = [];

            while(w < MyData.picklistID.length){
                if(doc.orders.length > 1){
                    let x = 0;
                    while(x < doc.orders.length){
                        if(doc.orders[x].pickListID == MyData.picklistID[w]){
                            let PlOrder = {
                                packType: doc.packType,
                                orders: doc.orders[x],
                                Status : 'CANCELED',
                                FromTote : MyData.toteID,
                                PickUpdateRequired: {type: Boolean, default: false},
                                PackUpdateRequired: {type: Boolean, default: false},
                                ShipUpdateRequired: {type: Boolean, default: false},
                                CancelRequired: true,
                                CreateDate: doc.CreateDate,
                                PickType: doc.PickType,
                                PickRegion: doc.PickRegion,
                            }

                            PlArr.push(PlOrder);
                            doc.orders.splice(x, 1);
                            cancelledCount++;
                            break;
                        }
                        x++;
                    }
                } else {
                    doc.Status = 'CANCELED';
                    doc.FromTote = MyData.toteID;
                    doc.toteID = null;
                    doc.CancelRequired = true;
                    CreateUnPickUpdate(doc, user);
                }
                w++;
            }

            let countPicked = 0;
            let countAll = 0;
            for (let i = 0; i < doc.orders.length; i++)
            {
                for (let j = 0; j < doc.orders[i].items.length; j++)
                {
                    if (doc.orders[i].items[j].picked == true)
                    {
                        countPicked++;
                    }
                    countAll++;
                }
            }

            if (countPicked == countAll && doc.Status != 'CANCELLED' && doc.Status != 'CANCELED')
            {
                doc.Status = 'PICKED';
            }

            console.log(' -> PICKED TOTAL <- ' + countPicked + ' ALL TOTAL ' + countAll);

            if(PlArr.length <= 0){
                doc.save(function(err, savedDoc){
                    return res.status(200).send({Data: savedDoc});
                });
                return;
            }

            SeparatePlsFromTote(PlArr, 0, user, function(){
                doc.save(function(err, savedDoc){
                    if(savedDoc.Status == 'PICKED' || doc.Status == 'CANCELLED' || doc.Status == 'CANCELED'){
                        return res.status(200).send({Data: savedDoc});
                    }

                    return res.status(200).send({Data: savedDoc, count: cancelledCount});
                });
            });
        });
    }
});

router.post('/testP', function(req, res) {

    var data = {
        orderID: req.body.orders,
        PickListID: req.body.pickLists,
        toteID: req.body.toteID
    }
    //console.log(' Order ' + data.orderID);
    PicklistModel.find({'$and' : [{toteID: data.toteID}, {'orders.orderID': data.orderID}]}, function(error, doc)
    {
        if (doc && !error) {
            for (var i = 0; i < doc.orders.length; i++) {
                if (doc.orders[i].PickListID == data.PickListID[i])
                {
                    console.log(' FOUND ' + data.PickListID[i]);
                    res.send('EHOLOweni');
                }
            }
        }
    });
});

router.get('/Retrieve', function(req, res) {

    PicklistModel.findOne({toteID : '0000121'}
        , function(error, doc) {
        //console.log(doc);
        var myJson = JSON.stringify(doc);

        console.log(myJson);
    });
    res.status(200).send(" I'm not a robot ");
});

module.exports = router;
