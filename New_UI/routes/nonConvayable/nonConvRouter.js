var apexLib = require('apex_lib/moduleIndex.js')
var PickListModel = require('pickListModel')
var PackStationModel = require('packStationModel')
var Params = require('parameterModel')
var PwLocs = require('putWallModel')
var express = require('express')
var moment = require('moment')
var router = express.Router()
var redis = require('redis');
var Pubclient = redis.createClient();
var serialUri = require('ui').serialUrl;
var CartonUrl = require('ui').CartonUrl;
var CanReq    = require('CancelRequestModel');
var rest      = require('restler');

// Get a pick list per ID.
router.get('/API/NCpickList/:id', requireLogin, function (req, res){
	var Tote = req.params.id;
	var user = req.session.username;
	var IpAddr = req.session.ip;
	var Msg = null;

  GetStation(IpAddr, function (Station){
		if (!Station){
			Msg = 'Scan Non Convable Container ID: User ' + user + ' failed scan ID from an unknown station';
			WriteToFile(Msg);
			return res.status(200).send({NoStation:true});
		} else {
			if(Tote == "undefined" || !Tote){
				Tote = Station.currentTote;
			}

			Msg = 'Request from station ' + Station.stationNumber + ' with Conatiner ID ' + Tote;
			WriteToFile(Msg);

			if(Tote == "undefined" || !Tote){
				return res.status(200).send({ScanMode:false});
			}

			PickListModel.findOne({toteID: Tote}, function (error, doc){
				if (error || !doc){
					Msg = 'Container ID not found for station ' + Station.stationNumber + ' scanned container ID ' + Tote;
					WriteToFile(Msg);
					return res.status(405).send('No data found for scanned container ID ' + Tote);
				}

				if(doc.Status == 'CANCELED'){
					Msg = 'Container ' + Tote + ' has a cancelled order';
					WriteToFile(Msg);
					return res.status(405).send(Msg);
				}

				if(doc.orders[0].WmsOrderStatus == 'CANCELED'){
					Msg = 'Container ' + Tote + ' has a cancelled order';
					WriteToFile(Msg);
					return res.status(405).send(Msg);
				}

				CanReq.findOne({'OrderId': doc.orders[0].orderID}, function(err, Req){
					if(Req){
						doc.orders[0].WmsOrderStatus = 'CANCELED';
						doc.orders[0].WmsPickListStatus = 'CANCELED';
						doc.orders[0].CancelDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
						doc.orders[0].CancelInstruction = 'Fetch Tote From Pack Station: ' + Station.stationNumber;

						doc.save(function(err, sd){
							Msg = 'Container ' + Tote + ' has a cancelled order';
							WriteToFile(Msg);
							return res.status(405).send(Msg);
						});

						return;
					}

					Params.find({}, function(err, Parameters){
						if(err || !Parameters || Parameters.length <= 0){
							Msg = "Failed to find parameters in the system ";
							WriteToFile(Msg);
							return res.status(405).send('Failed to validate scanned container ' + Tote);
						}

						if(!Parameters[0].NonConveyableContainerPrefix){
							Msg = "Failed to find trolleySettings in parameters";
							WriteToFile(Msg);
							return res.status(405).send('Failed to validate scanned container ' + Tote);
						}

						var Prefix = Parameters[0].NonConveyableContainerPrefix;

						var Err = ValidateScannedTote(doc, Station.stationNumber, Prefix);

						if(Err){
							return res.status(405).send('Container ' + Tote + ': ' + Err);
						}

						doc.toteInUse = true;
						doc.toteInUseBy = Station.stationNumber.toString();
						doc.containerLocation = null;

						var CurrentOrderToProcess = GetNextOrderToProcess(doc);

						CheckIfPickListIsUnique(doc, CurrentOrderToProcess, function(PickListCount){
							Msg = 'PickListID ' + doc.orders[CurrentOrderToProcess].pickListID + ' number of occurrences = ' + PickListCount;
							WriteToFile(Msg);
							if(PickListCount > 1){
								Msg = 'Container ' + Tote + ' has a duplicate picklist';
								WriteToFile(Msg);
								return res.status(405).send(Msg);
							}

							doc.orders[CurrentOrderToProcess].cartonSize = Prefix;
							doc.save(function(error, savedDoc){
								CreateResponseData(savedDoc, Station.stationNumber, CurrentOrderToProcess, user, function(Details){
									if(Details.error){
										return res.status(405).send(Details);
									}

									if(Details.ScanMode == false){
										return res.status(200).send(Details);
									}

									if(Station.currentTote != savedDoc.toteID){
										Station.currentTote = savedDoc.toteID;

										Station.save(function(err, savedStn){
											Msg = 'Container ' + Tote + ' added to station ' + savedStn.stationNumber + ' by user ' + user;
											WriteToFile(Msg);
											return res.status(200).send(Details);
										});
									} else {
										return res.status(200).send(Details);
									}
								});
							});
						});
					});
				});
		  });
	  }
  });
});

router.get('/API/NCreprintCarton/', requireLogin, function (req, res){
	var user = req.session.username;
	var IpAddr = req.session.ip;
	var Msg = null;

	GetStation(IpAddr, function (doc){
		if (!doc){
			Msg = 'Document Reprint: User ' + user + ' failed attempt to reprint documents from an unknown station';
			WriteToFile(Msg);
			return res.status(200).send({NoStation:true});
		} else {
			var Tote = doc.currentTote;

			if(!Tote){
				return res.status(200).send({ScanMode:false});
			}

			PickListModel.findOne({toteID: Tote}, function (error, PickList){
				var CurrentOrderToProcess = GetNextOrderToProcess(PickList);
				GetAllItemsAndCartonsForOrder(PickList, CurrentOrderToProcess, function(OrdDetails){
					if(OrdDetails.error){
						return callback(OrdDetails);
					} else {
						DetermineCartonNo(PickList, CurrentOrderToProcess, function(savedDoc){
							if(savedDoc.error){
								return res.status(405).send(Details.error);
							} else {
								SendDocsLabelPrint(savedDoc, CurrentOrderToProcess, savedDoc.orders[CurrentOrderToProcess].UseTransporter, OrdDetails, function(Data){
									Msg = 'Document Reprint: User ' + user + ' reprinted labels/documents for order ' + savedDoc.orders[CurrentOrderToProcess].orderID + ' at station ' + doc.stationNumber;
									WriteToFile(Msg);

									CreateResponseData(savedDoc, doc.stationNumber, CurrentOrderToProcess, user, function(Details){
										if(Details.error){
											return res.status(405).send(Details.error);
										} else
											return res.status(200).send(Details);
									});
								});
							}
						});
					}
				});
			});
		}
	});
});

router.get('/API/NCupdateVerifyScannedDoc/:id', requireLogin, function (req, res){
	var user = req.session.username;
	var IpAddr = req.session.ip;
	var ScannedDoc = req.params.id;
	var Msg = null;

	GetStation(IpAddr, function (doc){
		if (!doc){
			Msg = 'Document Validation: User ' + user + ' failed attempt to validate order document from an unknown station';
			WriteToFile(Msg);
			return res.status(200).send({NoStation:true});
		} else {
			var Tote = doc.currentTote;

			if(!Tote){
				return res.status(200).send({ScanMode:false});
			}

			PickListModel.findOne({toteID: Tote}, function (error, PickList){
				if (error || !PickList){
					Msg = 'Document Validation: Cannot find picklist for Container ' + Tote + ' associated with station ' + doc.stationNumber;
					WriteToFile(Msg);
					return res.status(200).send({error:"Cannot find a picklist for Container " + Tote});
				} else {
			        var CurrentOrderToProcess = GetNextOrderToProcess(PickList);
                    if(CurrentOrderToProcess != null){
					  	PickList.orders[CurrentOrderToProcess].orderVerified = true;

						PickList.save(function(err, savedDoc){
							Msg = 'Document Validation: order ' + ScannedDoc + ' successfully validated against printed documents';
							WriteToFile(Msg);

							CreateResponseData(savedDoc, doc.stationNumber, CurrentOrderToProcess, user, function(Details){
								if(Details.error){
									return res.status(405).send(Details.error);
								} else
									return res.status(200).send(Details);
							});
					    });
					} else {
						WriteToFile("Unable to calculate CurrentOrderToProcess at Document verification - station " + doc.stationNumber );
						return res.status(405).send("Unable to find order at station, call administrator");
					}
				}
			});
		}
	});
});

router.get('/API/NCpickListReset/', requireLogin, function (req, res){
	var user = req.session.username;
	var IpAddr = req.session.ip;
	var Msg = null;

	GetStation(IpAddr, function (doc){
		if (!doc){
			Msg = 'Container Reset: User ' + user + ' failed attempt to reset Container from an unknown station';
			WriteToFile(Msg);
			return res.status(200).send({NoStation:true});
		} else {
			var Tote = doc.currentTote;

			if(!Tote){
				return res.status(200).send({ScanMode:false});
			}

			Msg = 'Container Reset: User ' + user + ' attempting to reset Container ' + Tote + ' at station ' + doc.stationNumber;
			WriteToFile(Msg);

			PickListModel.findOne({toteID: Tote}, function (error, PickList){
				if (error || !PickList){
					Msg = 'Container Reset: Cannot find picklist for Container ' + Tote + ' associated with station ' + doc.stationNumber;
					WriteToFile(Msg);
					return res.status(200).send({error:"Cannot find a picklist for Container " + Tote});
				} else {
					ResetPicklistForTote(PickList, user, doc.stationNumber, function(Details){
						if(Details.error){
							return res.status(405).send(Details.error);
						} else {
							ClearToteFromStation(doc.stationNumber, function(Details){
								Msg = 'Container Reset: Container ' + Tote + ' successfully reset and removed from station ' + doc.stationNumber;
								WriteToFile(Msg);
								return res.status(200).send(Details);
							});
						}
					});
				}
			});
		}
	});
});

// Get the serial record from the db.
router.get('/API/NCgetSerial/:id', requireLogin, function (req, res){
	var user = req.session.username;
	var ScannedSerial = req.params.id;
	var IpAddr = req.session.ip;
	var Msg = null;

	GetStation(IpAddr, function (Station){
		if (!Station){
			Msg = 'Serial Scan: User ' + user + ' failed attempt to validate serial number from an unknown station';
			WriteToFile(Msg);
			return res.status(200).send({NoStation:true});
		} else {
			if(!Station.currentTote){
				return res.status(200).send({ScanMode:false});
			}

			//PickListModel.findOne({'orders.items.serials': ScannedSerial}, function (error, MongoSerial)
            PickListModel.find({'orders.items.serials': {$regex: ScannedSerial, $options:'i'}}, function (error, MongoSerial){
                var SerialGood = false;
                var FoundIndex = null;
				if(error || !MongoSerial || MongoSerial.length <= 0){
                    SerialGood = true;
                } else {
					var x = 0;
					while(x < MongoSerial.length){
						if(MongoSerial[x].Status != 'RETURNED'){
							FoundIndex = x;
							break;
						}
						x++;
					}

					if(FoundIndex == null){
						Msg = 'Serial number ' + ScannedSerial + ' Found in a returned order so its good to be re-despatched';
						WriteToFile(Msg);
						SerialGood = true;
					}
				}

				if(SerialGood){
					var url = serialUri + ScannedSerial

					apexLib.rest.get(url, null, function (response){
						if (response instanceof Error){
						    Msg = 'Serial Scan: Error while trying to connect to WMS for serial validation | ' + response;
							WriteToFile(Msg);
							res.status(405).send('Error: Serial validation service is down, please call supervisor');
							return;
						}

						var dbResult = response;
						if(dbResult && dbResult != null){
							var serial = dbResult.serial;
							var sku = dbResult.SKU;
							var serialStatus = dbResult.serialStatus;
							var despatched = dbResult.dispatched;
							var Quantity = 1;

							if (!serial || serial == ''){
								Msg = 'Serial Scan: Station: ' + Station.stationNumber + ' | User ' + user + ': serial validation failed in WMS for scanned serial ' + ScannedSerial + ' [WMS returned]: ' + serial;
								WriteToFile(Msg);
								res.status(405).send('Scanned serial number ' + ScannedSerial + ' could not be validated');
								return;
							}

							/*if (ScannedSerial != serial && (("892700000" + ScannedSerial) != serial))
							{
								Quantity = dbResult.serialCount;
								Msg = 'Serial Scan: User ' + user + ': mismatch between scanned serial number and result returned from WMS ->[scanned]: ' + ScannedSerial + ' [WMS returned]:' + serial;
								WriteToFile(Msg);
								res.status(405).send('Scanned serial number could not be validated');
								return;
							}*/

							if ((ScannedSerial == serial) || (("892700000" + ScannedSerial) == serial) || (("892700008" + ScannedSerial) == serial)){
								Msg = 'User: ' + user + ' scanned serial: ' + ScannedSerial + ' | WMS returned: ' + serial + ' at station: ' + Station.stationNumber;
								WriteToFile(Msg);
							} else {
								Quantity = dbResult.serialCount;
								Msg = 'Serial Scan: Station: ' + Station.stationNumber + ' | User ' + user + ': mismatch between scanned serial number and result returned from WMS ->[scanned]: ' + ScannedSerial + ' [WMS returned]:' + serial;
								WriteToFile(Msg);
								res.status(405).send('Scanned serial number ' + ScannedSerial + ' could not be validated');
								return;
							}

							if(dbResult.serialCount){
								Quantity = dbResult.serialCount;
								Msg = 'Serial Scan: User ' + user + ' scanned serial ' + serial + ' at station ' + Station.stationNumber + ' serial quantity ' + Quantity;
								WriteToFile(Msg);
							}

							if(despatched == 1){
								Msg = 'Serial Scan: User ' + user + ' failed attempt to despatch an already despached serial ' + serial + ' at station ' + Station.stationNumber;
								WriteToFile(Msg);
								res.status(405).send('Scanned serial number ' + serial + ' is already despatched');
								return;
							} else {
								if(serialStatus === 'BROKEN'){
									Msg = 'Serial Scan: User ' + user + ' failed attempt to despatch a serial ' + serial + ' with serial status ' + serialStatus + ' at station ' + Station.stationNumber;
									WriteToFile(Msg);
									res.status(405).send('Scanned container serial number ' + serial + ' is no longer a fullpack');
									return;
								} else {
									UpdateSerialToPickList(user, Station, sku, Quantity, serial, function(Details){
										if(Details.error){
											res.status(405).send(Details.error);
										} else {
											res.status(200).send(Details);
										}
									});
								}
							}
						} else {
							CheckIfNonSerialised(ScannedSerial, Station.currentTote, Station.stationNumber, user, function(Details){
								if(Details.error){
									res.status(405).send(Details.error);
								} else {
									CreateResponseData(Details.PickList, Station.stationNumber, Details.index, user, function(Details){
										if(Details.error){
											return res.status(405).send(Details.error);
										} else {
											return res.status(200).send(Details);
										}
									});
								}
							});
						}
					});
				} else {
					if(MongoSerial[FoundIndex].toteID == Station.currentTote){
						Msg = 'Serial Scan: Station: ' + Station.stationNumber + ' | User ' + user + ' scanned serial number ' + ScannedSerial + ' which is already scanned into tote ' + Station.currentTote;
						WriteToFile(Msg);
						res.status(405).send('Scanned serial number ' + ScannedSerial + ' has already been scanned');
					} else {
						Msg = 'Serial Scan: Station: ' + Station.stationNumber + ' | User ' + user + ' scanned serial number ' + ScannedSerial + ' which is already despatched to order ' + MongoSerial[FoundIndex].orders[0].orderID;
						WriteToFile(Msg);
						res.status(405).send('Scanned serial number ' + ScannedSerial + ' is already despatched');
					}
				}
			});
		}
	});
});

/*=================== Functions =================================================*/

function CheckIfNonSerialised(Barcode, Tote, Station, user, callback){
	PickListModel.findOne({'toteID': Tote, 'orders.items.barcode': Barcode}, function (error, PickList){
		if (error || !PickList){
			Msg = 'Serial Scan: User ' + user + ' scanned serial number ' + Barcode + ' and could not be validated in WMS at station ' + Station;
			WriteToFile(Msg);
			return callback({error:'Scanned serial number ' + Barcode + ' could not be validated'});
		} else {
		  var CurrentOrderToProcess = GetNextOrderToProcess(PickList);
		  var x = 0;
		  var itemFound = false;
		  var RoomforOneMore = false;
		  var Serialised = false;

		  while(x < PickList.orders[CurrentOrderToProcess].items.length){
				itemFound = false;
				if(PickList.orders[CurrentOrderToProcess].items[x].barcode == Barcode){
					if(!PickList.orders[CurrentOrderToProcess].items[x].serialised){
						itemFound = true;

						var Qtypacked = PickList.orders[CurrentOrderToProcess].items[x].qtyPacked;
						if(Qtypacked == null){
							Qtypacked = 0;
						}

						var QtyRem = pickQty - Qtypacked;
						if(QtyRem > 0){
							RoomforOneMore = true;
							break;
						}
					} else {
						Serialised = true;
						break;
					}
				}
				x++;
			}

			if(x < PickList.orders[CurrentOrderToProcess].items.length){
				if(Serialised){
					Msg = 'Non Serialised Scan: User ' + user + ' scanned sku barcode ' + Barcode + ' which is not for a non-serialised product';
					WriteToFile(Msg);
					return callback({error:'Scanned product barcode ' + Barcode + ' is not for a non-serialised product'});
				} else {
					if(itemFound && RoomforOneMore){
						var Qtypacked = PickList.orders[CurrentOrderToProcess].items[x].qtyPacked;
						if(Qtypacked == null){
							Qtypacked = 0;
						}

						Qtypacked++;
						PickList.orders[CurrentOrderToProcess].items[x].qtyPacked = Qtypacked;

						Msg = 'Non Serialised Scan: Station: ' + Station + ' | User ' + user + ' added 1 to non-serialised product ' + PickList.orders[CurrentOrderToProcess].items[x].itemCode + ' in order ' + PickList.orders[CurrentOrderToProcess].orderID;
						WriteToFile(Msg);

						if(QtyPacked == PickList.orders[CurrentOrderToProcess].items[x].pickQty){
							PickList.orders[CurrentOrderToProcess].items[x].packedBy = Data.User;
							PickList.orders[CurrentOrderToProcess].items[x].packed = true;
						}

						var AllItemsPacked = true;
						x = 0;
						while(x < PickList.orders[CurrentOrderToProcess].items.length){
							if(PickList.orders[CurrentOrderToProcess].items[x].packed == false){
								AllItemsPacked = false;
								break;
							}
							y++;
						}

						if(AllItemsPacked){
							PickList.orders[CurrentOrderToProcess].packed = true;
							Msg = "Station: " + Station + " | Order " + PickList.orders[CurrentOrderToProcess].orderID + " in Container " + PickList.toteID + " is fully packed";
							WriteToFile(Msg);
						}

						PickList.save(function(err, savedDoc){
							if(!err && savedDoc){
							  return callback({PickList:savedDoc, index:CurrentOrderToProcess});
							} else {
								return callback({error:'Failed to commit scanned data'});
							}
						});
					} else {
						Msg = 'Non Serialised Scan: Station: ' + Station + ' | User ' + user + ' scanned sku barcode ' + Barcode + ' which is fully packed';
						WriteToFile(Msg);
						return callback({error:'Already packed enough stock of this product to the order'});
					}
				}
			} else {
				Msg = 'Non Serialised Scan: Station: ' + Station + ' | User ' + user + ' scanned sku barcode ' + Barcode + ' which is not valid for any item in order ' + PickList.orders[CurrentOrderToProcess].orderID;
				WriteToFile(Msg);
				return callback({error:'Scanned product barcode ' + Barcode + ' is not valid for this order'});
			}
		}
	});
} /* CheckIfNonSerialised */

function ValidateScannedTote(PickList, Station, Prefix)
{
	var valid = null;
	if(PickList.toteID.length == 7){
		if(PickList.toteID[0] == Prefix){
			if(PickList.toteInUse && Station != parseInt(PickList.toteInUseBy)){
				valid = 'Container in use at Station ' + PickList.toteInUseBy;
			} else {
				if(PickList.orders.length > 0){
					var index = 0;
					while(index < PickList.orders.length){
						var itemIndex = 0;
						while(itemIndex < PickList.orders[index].items.length){
							if(PickList.orders[index].items[itemIndex].picked == false){
								valid = 'Not all items in Container are picked';
								break;
							}

							if(PickList.orders[index].items[itemIndex].pickQty != PickList.orders[index].items[itemIndex].qty){
								valid = 'Quantity mismatch between item quantity and picked quantity for sku ' + PickList.orders[index].items[itemIndex].sku;
								break;
							}
							itemIndex++;
						}

						if(valid != null)
							break;

						index++;
					}

					var PackedOrderCount = 0;
					index = 0;
					while(index < PickList.orders.length){
						if(PickList.orders[index].packComplete){
							PackedOrderCount++;
						}

						index++;
					}

					if(PackedOrderCount >= PickList.orders.length){
						valid = 'All orders in container are already packed';
					}
				} else {
					valid = 'PickList for this container does not contain any orders';
				}
			}
		} else {
			valid = 'Not a non convable container';
		}
	} else {
		valid = 'Invalid Id Container ID scanned';
	}

	return valid;
} /* ValidateScannedTote */

function UpdateSerialToPickList(user, Station, sku, Quantity, serial, callback){
	var Tote = Station.currentTote;
	var Msg = null;

	PickListModel.findOne({toteID: Tote}, function (error, PickList){
		if (error || !PickList){
			Msg = 'Serial scan: Cannot find picklist for Container associated with station ' + Station.stationNumber;
			WriteToFile(Msg);
			return callback({error:"Cannot find a picklist for Container " + Tote});
		} else {
			var CurrentOrderToProcess = GetNextOrderToProcess(PickList);
			UpdateSerial(PickList, user, sku, Quantity, serial, Station.stationNumber, CurrentOrderToProcess, function(Details){
				return callback(Details);
			});
		}
	});
} /* UpdateSerialToPickList */

function GetStation(IpAddr, callback){
	PackStationModel.findOne({stationIP: IpAddr}, function (error, doc){
		if(error || !doc){
			return callback(null);
		} else {
			var now = new Date();
			doc.updated_at = now;
			doc.active = true;
			doc.save(function(error, savedDoc){
				return callback(savedDoc);
			});
		}
	});
} /* GetStation */

function UpdateSerial(PickList, user, sku, Quantity, serial, Station, CurrentOrderToProcess, callback){
	var OriginalQty = Quantity;
	var OrdIndex = CurrentOrderToProcess;
	var QtyPacked = null;
	var Msg = null;
	var itemIndex = 0;

	while(itemIndex < PickList.orders[OrdIndex].items.length){
		var QtyPacked = PickList.orders[OrdIndex].items[itemIndex].qtyPacked;
		if(QtyPacked == null){
			QtyPacked = 0;
		}

		var QtyRem = PickList.orders[OrdIndex].items[itemIndex].pickQty - QtyPacked;

		if(sku == PickList.orders[OrdIndex].items[itemIndex].sku && QtyRem >= Quantity && !PickList.orders[OrdIndex].items[itemIndex].packed){
			break;
		}
		itemIndex++;
	}

	if(itemIndex < PickList.orders[OrdIndex].items.length){
		QtyPacked += Quantity;
		//PickList.orders[OrdIndex].items[itemIndex].qtyPacked = QtyPacked;
		PickList.orders[OrdIndex].items[itemIndex].serials.push(serial);
		PickList.orders[OrdIndex].items[itemIndex].qtyPacked = PickList.orders[OrdIndex].items[itemIndex].serials.length;

		Msg = "Station: " + Station + " | User " + user + " added serial number " + serial + " from Container " + PickList.toteID + " to order " + PickList.orders[OrdIndex].orderID;
		WriteToFile(Msg);

		if(QtyPacked == PickList.orders[OrdIndex].items[itemIndex].pickQty){
			PickList.orders[OrdIndex].items[itemIndex].packedBy = user;
			PickList.orders[OrdIndex].items[itemIndex].packed = true;
		}
		Quantity = 0;

		var AllItemsPacked = true;
		itemIndex = 0;
		while(itemIndex < PickList.orders[OrdIndex].items.length){
			if(PickList.orders[OrdIndex].items[itemIndex].packed == false){
				AllItemsPacked = false;
				break;
			}
			itemIndex++;
		}

		if(AllItemsPacked){
			PickList.orders[OrdIndex].packed = true;
			Msg = "Station: " + Station + " | Order " + PickList.orders[OrdIndex].orderID + " in Container " + PickList.toteID + " is fully packed";
			WriteToFile(Msg);
		}

		PickList.save(function(error, savedData){
			if(error){
				Msg = "Error updating serial number " + serial + " to picklist in Container " + PickList.toteID;
				WriteToFile(Msg);
				return callback({error: Msg});
			} else {
				CreateResponseData(savedData, Station, OrdIndex, user, function(Details){
					return callback(Details);
				});
			}
		});
	} else {
		if(OriginalQty == Quantity){
			Msg = "Scanned serial number " + serial + " is not part of this order";
			WriteToFile(Msg);
			return callback({error:Msg});
		} else {
			Msg = "Serial number quantity supplied is greater than what is required";
			WriteToFile(Msg);
			return callback({error:Msg});
		}
	}
} /* UpdateSerial */

function ResetPicklistForTote(PickList, user, Station, callback){
	var Details = PickList;
	var x = 0;
	while(x < Details.orders.length){
		var y = 0;
		while(y < Details.orders[x].items.length){
			Details.orders[x].items[y].packedBy = null;
			Details.orders[x].items[y].serials = [];
			Details.orders[x].items[y].packed = false;
			Details.orders[x].items[y].qtyPacked = null;
			y++;
		}

		Details.orders[x].packed = false;
		Details.orders[x].picklistWeight = null;
		Details.orders[x].toteNumber = null;
		Details.orders[x].cartonID = null;
		x++;
	}

	Details.ShipUpdateRequired = false;
	Details.PickUpdateRequired = false;
	Details.PackUpdateRequired = false;
	Details.Status = 'PICKED';
	Details.PackedDate = null;
	Details.toteInUse = false;
	Details.toteInUseBy = "";
	Details.transporterID = null;

	Details.save(function(error, DocSaved){
		if(error || !DocSaved){
			Msg = "Failed to reset Container " + Details.toteID + " at station " + Station + ". Reason: " + error;
			WriteToFile(Msg);
			return callback({error:Msg});
		} else {
			return callback({NoStation: false, ScanMode: false});
		}
	});
} /* ResetPicklistForTote */

function CreateResponseData(savedDoc, Station, CurrentOrderToProcess, user, callback){
	PickListModel.count({'orders.orderID':savedDoc.orders[CurrentOrderToProcess].orderID}, function( err, TotesPerOrder){
		var NumOrders = savedDoc.orders.length;

		var toteTypeDetailName = GetToteType(NumOrders, TotesPerOrder);

		GenerateitemsToPack(savedDoc, CurrentOrderToProcess, function(itemsToDisplay){
			DetermineNextStepInProcess(savedDoc, CurrentOrderToProcess, function(Details){
				if(Details.error){
					return callback(Details);
				} else {
					if(Details.Next == 'DONE') {
						if(savedDoc.orders.length > 1) {
							savedDoc.orders[CurrentOrderToProcess].packComplete = true;
							savedDoc.save(function(err, savedDoc){
								CurrentOrderToProcess = GetNextOrderToProcess(savedDoc);

								if(CurrentOrderToProcess == null){
									ClearToteFromStation(Station, function(Details){
										return callback(Details);
									});
								} else {
									CreateResponseData(savedDoc, Station, CurrentOrderToProcess, user, callback);
								}
							});
						} else {
							if(savedDoc.orders.length == 1){
								savedDoc.orders[CurrentOrderToProcess].packComplete = true;
								savedDoc.save(function(err, savedDoc){
									PackCompleteOrder(savedDoc, CurrentOrderToProcess, Station, function(Details){
										return callback(Details);
									});
                                })
							} else {
								return callback({error:'No orders found in Container ' + savedDoc.toteID});
							}
						}
					} else if(Details.Next == 'CARTON') {
						savedDoc.orders[CurrentOrderToProcess].cartonID = savedDoc.toteID;
						savedDoc.save(function(err, savedDoc){
							CreateResponseData(savedDoc, Station, CurrentOrderToProcess, user, callback);
						});
					} else {
						var Details = {
							signedInStation: Station,
							toteTypeDetailName: toteTypeDetailName,
							scannedTote: savedDoc.toteID,
							useCartonSize: savedDoc.orders[CurrentOrderToProcess].cartonSize,
							scannedCartonIdDisplay: savedDoc.orders[CurrentOrderToProcess].cartonID,
							cartonSizeUsed: savedDoc.orders[CurrentOrderToProcess].cartonSize,
							currentOrderId: savedDoc.orders[CurrentOrderToProcess].orderID,
							currentOrderInToteProcess: (CurrentOrderToProcess+1),
							totalOrdersInToteToProcess: NumOrders,
							itemsToDisplay: itemsToDisplay,
							ToteMode:'MAN',
							Next: Details.Next
						}

						return callback(Details);
					}
				}
			});
		});
	});
} /* CreateResponseData */

function PackCompleteOrder(savedDoc, CurrentOrderToProcess, Station, callback){
  	var now = new Date();
	savedDoc.FromTote = savedDoc.toteID;
	savedDoc.toteID = savedDoc.orders[CurrentOrderToProcess].cartonID;
	savedDoc.PackedDate = moment(now).format('YYYY-MM-DD HH:mm');
	savedDoc.orders[CurrentOrderToProcess].packComplete = true;
	savedDoc.ShipUpdateRequired = true;
	savedDoc.PackUpdateRequired = true;
	savedDoc.Status = 'SHIPPED';
	savedDoc.toteInUse = false;
	savedDoc.PickListType = null;
	savedDoc.containerLocation = null;
	savedDoc.transporterID = null;
	savedDoc.save(function(err, savedDoc){
		ClearToteFromStation(Station, function(Details){
			return callback(Details);
		});
	});
} /* PackCompleteOrder */

function ClearToteFromStation(Station, callback){
	PackStationModel.findOne({stationNumber: Station}, function (error, doc){
		if(error || !doc){
			return callback({ScanMode:false});
		} else {
			var now = new Date();
			doc.updated_at = now;
			doc.currentTote = null;
			doc.currentTransporter = null;
			doc.save(function(error, doc){
				return callback({ScanMode:false});
			});
		}
	});
} /* ClearToteFromStation */

function GetNextOrderToProcess(PickList){
	var x = 0;
	while(x < PickList.orders.length){
		if(PickList.orders[x].packComplete == false){
			break;
		}
		x++;
	}

	if(x < PickList.orders.length){
		return (x);
	}
	else
	  return null;
} /* GetNextOrderToProcesse */

function SendDocsLabelPrint(PickList, CurrentOrderToProcess, UseTransporter, OrderData, callback){
	var Next = '';
	var OrdItems = OrderData.OrdItems;
	var Cartons  = OrderData.Cartons;

	var CartonNo = PickList.orders[CurrentOrderToProcess].toteNumber;
	var NoOfCartons = PickList.orders[CurrentOrderToProcess].numberOfTotes;

	var First = false;
	var Last = false;
	var Expect = '';
	if(CartonNo == 1 && CartonNo == NoOfCartons){
		First = true;
		Last = true;
		Next = 'DOC';
		Expect = 'Documents/label';
	} else if(CartonNo == 1 && CartonNo != NoOfCartons){
		First = true;
		Last = false;
		Expect = 'label';
		if(UseTransporter){
			Next = 'TRANS';
		} else {
			Next = 'DONE';
		}
	} else if(CartonNo > 1 && CartonNo == NoOfCartons){
		First = false;
		Last = true;
		Next = 'DOC';
		Expect = 'Documents/label';
	} else if(CartonNo > 1 && CartonNo != NoOfCartons){
		First = false;
		Last = false;
		Expect = 'label';
		if(UseTransporter){
			Next = 'TRANS';
		} else {
			Next = 'DONE';
		}
	}

	var Station = parseInt(PickList.toteInUseBy);
	PackStationModel.findOne({stationNumber: Station}, function (error, doc){
		if(error || !doc){
			return callback({error:"Error while finding station for Tote " + PickList.toteID});
		} else {
			var PrintData = {
				orderId: PickList.orders[CurrentOrderToProcess].orderID,
				printerName: doc.desktopPrinterQName,
				labelprinterName: doc.labelPrinterQName,
				items: OrdItems,
				ErpItems: PickList.orders[CurrentOrderToProcess].items,
				cartonId: PickList.orders[CurrentOrderToProcess].cartonID,
				courierId: PickList.orders[CurrentOrderToProcess].courierID,
				station: doc.stationNumber,
				CartonNo: CartonNo,
				NoOfCartons: NoOfCartons,
				CartionWeight: PickList.orders[CurrentOrderToProcess].picklistWeight,
				FirstCarton: First,
				LastCarton: Last,
				orderType: PickList.orders[CurrentOrderToProcess].orderType,
				Cartons: Cartons
			}

			Pubclient.publish('PACKING_' + doc.stationNumber, JSON.stringify(PrintData), function(err){
				 if(err){
					 return callback({error:"Error printing " + Expect + " for order " + PickList.orders[CurrentOrderToProcess].orderID});
				 } else {
					 PickList.orders[CurrentOrderToProcess].printed = true
					 PickList.save(function(err, savedDoc){
						 return callback({Next:Next, PickList:savedDoc});
					 });
				 }
			});
		}
	});

} /* SendDocsLabelPrint */

function DetermineCartonNo(savedDoc, CurrentOrderToProcess, callback){
	var Msg = null;
	let body = {
		cartonID:   savedDoc.orders[CurrentOrderToProcess].cartonID,
		orderID:    savedDoc.orders[CurrentOrderToProcess].orderID,
		picklistID: savedDoc.orders[CurrentOrderToProcess].pickListID
	}

	rest.postJson(CartonUrl, body).on('complete', function(result){
		if(result instanceof Error){
			Msg = "Error finding all picklists for order " + savedDoc.orders[CurrentOrderToProcess].orderID + " to calculate carton number";
			WriteToFile(Msg);
			return callback({error:"Problem encountered while calculating the order carton number - please call supervisor"});
		}

		if(result.Err){
			Msg = "Error finding all picklists for order " + savedDoc.orders[CurrentOrderToProcess].orderID + " to calculate carton number";
			WriteToFile(Msg);
			return callback({error:"Problem encountered while calculating the order carton number - please call supervisor"});
		}

		let CartonNo = result.CartonNo;
		let NoOfCartons = result.NoOfCartons;

        savedDoc.orders[CurrentOrderToProcess].toteNumber = CartonNo;
        savedDoc.orders[CurrentOrderToProcess].numberOfTotes = NoOfCartons;

		savedDoc.save(function(err, Data){
			Msg = "Order " + Data.orders[CurrentOrderToProcess].orderID + " | cartonID " +
						Data.orders[CurrentOrderToProcess].cartonID + " | carton number " + Data.orders[CurrentOrderToProcess].toteNumber +
						" of " + Data.orders[CurrentOrderToProcess].numberOfTotes + " was scanned on station " + Data.toteInUseBy;
			WriteToFile(Msg);

			return callback(Data);
		});
	});
} /* DetermineCartonNo */

function UpdateTotalToEachCarton(PickLists, index, Total, orderID, callback){
	if(index < PickLists.length){
		var ordIndx = 0;
		while(ordIndx < PickLists[index].orders.length){
			if(PickLists[index].orders[ordIndx].orderID == orderID){
				PickLists[index].orders[ordIndx].numberOfTotes = Total;
			}
			ordIndx++;
		}

		PickLists[index].save(function(err, SavedData){
			index++;
			UpdateTotalToEachCarton(PickLists, index, Total, orderID, callback);
		});
	} else {
		return callback();
	}
} /* UpdateTotalToEachCarton */

function DetermineNextStepInProcess(savedDoc, CurrentOrderToProcess, callback){
	if(!savedDoc.orders[CurrentOrderToProcess].packed){
		return callback({Next: 'BARCODE/SERIAL'});
	} else if(savedDoc.orders[CurrentOrderToProcess].packed &&
		     (savedDoc.orders[CurrentOrderToProcess].cartonID == null ||
		      savedDoc.orders[CurrentOrderToProcess].cartonID == '')){
		return callback({Next: 'CARTON'});
	} else if(!savedDoc.orders[CurrentOrderToProcess].printed){
		ParamGetCartonDetails(savedDoc, CurrentOrderToProcess, function(CartonDetails){
			if(CartonDetails.error){
			   return callback(CartonDetails);
			} else {
			   CalculateCartonWeight(savedDoc, CurrentOrderToProcess, CartonDetails.EmptyCartonWeight, function(CartonWeight){
				  savedDoc.orders[CurrentOrderToProcess].picklistWeight = CartonWeight;
				  savedDoc.orders[CurrentOrderToProcess].useTransporter = CartonDetails.UseTransporter;
                  savedDoc.orders[CurrentOrderToProcess].transporterWeight = null;
                  savedDoc.orders[CurrentOrderToProcess].minWeightLimit = (CartonWeight - CartonDetails.MinTolerance);
                  savedDoc.orders[CurrentOrderToProcess].maxWeightLimit = (CartonWeight + CartonDetails.MaxTolerance);
                  var Msg = "Container " + savedDoc.orders[CurrentOrderToProcess].cartonID + " weight " + CartonWeight + " | min weight " + (CartonWeight - CartonDetails.MinTolerance) + " | max weight " + (CartonWeight + CartonDetails.MaxTolerance);
                  WriteToFile(Msg);
				  savedDoc.save(function(err, savedDoc) {
					GetAllItemsAndCartonsForOrder(savedDoc, CurrentOrderToProcess, function(OrdDetails){
						if(OrdDetails.error){
							return callback(OrdDetails);
						} else {
							DetermineCartonNo(savedDoc, CurrentOrderToProcess, function(savedDoc){
								if(savedDoc.error){
									return callback(CartonDetails);
								} else {
									SendDocsLabelPrint(savedDoc, CurrentOrderToProcess, CartonDetails.UseTransporter, OrdDetails, function(Data){
										return callback(Data);
									});
								}
							});
						}
					});
				});
            });
			}
		});
	} else if(!savedDoc.orders[CurrentOrderToProcess].packComplete) {
		if(savedDoc.orders[CurrentOrderToProcess].numberOfTotes == savedDoc.orders[CurrentOrderToProcess].toteNumber){
			if(!savedDoc.orders[CurrentOrderToProcess].orderVerified){
				return callback({Next: 'DOC'});
			}
		}

		return callback({Next: 'DONE'});
	}
} /* DetermineNextStepInProcess */

function GetAllItemsAndCartonsForOrder(savedDoc, CurrentOrderToProcess, callback){
	var Msg = null;
	PickListModel.find({'orders.orderID': savedDoc.orders[CurrentOrderToProcess].orderID}, function(err, PickLists){
		if(err || !PickLists || PickLists.length <= 0){
			Msg = "Error finding all picklists for order " + Data.orders[CurrentOrderToProcess].orderID;
			WriteToFile(Msg);
			return callback({error:"Problem encountered fetching all picklists for order - please call supervisor"});
		} else {
			var Cartons = [];
			var OrdItems = [];
			var PL = 0;
			var TotalCartons = 0;
			while(PL < PickLists.length){
				var OrdIndex = 0;

				while(OrdIndex < PickLists[PL].orders.length){
					if(PickLists[PL].orders[OrdIndex].orderID == savedDoc.orders[CurrentOrderToProcess].orderID){
						var itemIndx = 0;
						TotalCartons++;
						while(itemIndx < PickLists[PL].orders[OrdIndex].items.length){
							OrdItems.push(PickLists[PL].orders[OrdIndex].items[itemIndx]);
							Cartons.push(PickLists[PL].orders[OrdIndex].cartonID);
							itemIndx++;
						}
					}
					OrdIndex++;
				}
				PL++;
			}

			var ReturnData ={
				Cartons: Cartons,
				OrdItems: OrdItems
			}

			UpdateTotalToEachCarton(PickLists, 0, TotalCartons, savedDoc.orders[CurrentOrderToProcess].orderID, function(){
				return callback(ReturnData);
			})
		}
	});
} /* GetAllItemsAndCartonsForOrder */

function CalculateCartonWeight(savedDoc, CurrentOrderToProcess, EmptyCartonWeight, callback){
	var x = 0;
	var Total = EmptyCartonWeight;
        while(x < savedDoc.orders[CurrentOrderToProcess].items.length){
		var lineWeight = (savedDoc.orders[CurrentOrderToProcess].items[x].eachWeight *
		                  savedDoc.orders[CurrentOrderToProcess].items[x].qtyPacked);
		Total += lineWeight;
		x++;
	}

	return callback(Total);
} /* CalculateCartonWeight */

function ParamGetCartonDetails(PickList, CurrentOrderToProcess, callback){
	var Msg = null;
	Params.find({}, function(err, Parameters){
		if(err || !Parameters || Parameters.length <= 0) {
			Msg = "Failed to find parameters in the system ";
			WriteToFile(Msg);
			return callback({error:Msg});
		} else {
			var CartonPrefix = PickList.orders[CurrentOrderToProcess].cartonID[0];

			if(Parameters[0].weightSettings && Parameters[0].weightSettings.length > 0){
				var paramIndex = 0;
				while(paramIndex < Parameters[0].weightSettings.length){
					if(Parameters[0].weightSettings[paramIndex].StockSize == parseInt(CartonPrefix)){
						break;
					}
					paramIndex++;
				}

				if(paramIndex < Parameters[0].weightSettings.length){
					var CartonDetails = {
						UseTransporter: Parameters[0].weightSettings[paramIndex].useTransporter,
						EmptyCartonWeight: Parameters[0].weightSettings[paramIndex].weight,
                        MinTolerance: Parameters[0].weightSettings[paramIndex].LowerTolerance,
                        MaxTolerance: Parameters[0].weightSettings[paramIndex].UpperTolerance
					}

					return callback(CartonDetails);
				} else {
					Msg = "Failed to find Container weightSettings to match order Container field for Container " +
					       PickList.orders[CurrentOrderToProcess].cartonID + " of order " + PickList.orders[CurrentOrderToProcess].orderID;
					WriteToFile(Msg);
					return callback({error:Msg});
				}
			} else {
				Msg = "Failed to find weightSettings parameter in the system ";
				WriteToFile(Msg);
				return callback({error:Msg});
			}
		}
	});
} /* ParamGetCartonDetails */

function GenerateitemsToPack(savedDoc, CurrentOrderToProcess, callback){
	var itemIndex = 0;
	var skus = [];
	var itemsCodes = [];
	var itemDescription = [];
	var pickQty = [];
	var qtyPacked = [];
	var SkuSerials = [];
	while(itemIndex < savedDoc.orders[CurrentOrderToProcess].items.length){
		var y = 0;
		while(y < skus.length){
			if(savedDoc.orders[CurrentOrderToProcess].items[itemIndex].sku == skus[y]){
				break;
			}
			y++;
		}

		if(y < skus.length){
			pickQty[y] += savedDoc.orders[CurrentOrderToProcess].items[itemIndex].pickQty;

			var QtyPacked = savedDoc.orders[CurrentOrderToProcess].items[itemIndex].qtyPacked;
			if(QtyPacked == null)
			  QtyPacked = 0;

			qtyPacked[y] += QtyPacked;

			var n = 0;
		    while(n < savedDoc.orders[CurrentOrderToProcess].items[itemIndex].serials.length){
			 	SkuSerials[y].push(savedDoc.orders[CurrentOrderToProcess].items[itemIndex].serials[n]);
			 	n++;
			}
		} else {
			skus.push(savedDoc.orders[CurrentOrderToProcess].items[itemIndex].sku);
			itemsCodes.push(savedDoc.orders[CurrentOrderToProcess].items[itemIndex].itemCode);
			itemDescription.push(savedDoc.orders[CurrentOrderToProcess].items[itemIndex].itemDescription);
			pickQty.push(savedDoc.orders[CurrentOrderToProcess].items[itemIndex].pickQty);

			var QtyPacked = savedDoc.orders[CurrentOrderToProcess].items[itemIndex].qtyPacked;
			if(QtyPacked == null)
			  QtyPacked = 0;

			qtyPacked.push(QtyPacked);
			var arr = savedDoc.orders[CurrentOrderToProcess].items[itemIndex].serials;
            SkuSerials.push(arr.slice(0, arr.length));
		}
		itemIndex++;
	}

  var itemsToDisplay = [];
  var f = 0;
  while(f < skus.length){
		var line = {
			itemCode: itemsCodes[f],
			itemDescription: itemDescription[f],
			pickQty: pickQty[f],
			qtyPacked: qtyPacked[f],
			SkuSerials: SkuSerials[f]
		}

		itemsToDisplay.push(line);
		f++;
	}

	return callback(itemsToDisplay);
} /* GenerateitemsToPack */

function CheckIfPickListIsUnique(doc, CurrentOrderToProcess, callback){
  PickListModel.find({'orders.pickListID': doc.orders[CurrentOrderToProcess].pickListID}, function(err, PickLists){
     if(err || !PickLists || PickLists.length <= 0){
       return callback(1);
     } else {
       var Total = 0;
       var x = 0;
       while(x < PickLists.length){
         var y = 0;
         while(y < PickLists[x].orders.length){
           if(PickLists[x].orders[y].pickListID == doc.orders[CurrentOrderToProcess].pickListID){
             Total++;
           }
	       y++;
         }
         x++;
       }

       return callback(Total);
     }
  });
} /* CheckIfPickListIsUnique */

function GetToteType(NumOrders, TotesPerOrder){
	var toteTypeDetailName = null;
	if(NumOrders > 1){
		toteTypeDetailName = 'MULTI ORDER CONTAINER'
	} else {
		if(TotesPerOrder > 1) {
			toteTypeDetailName = 'MULTI CONTAINER ORDER'
		} else {
			toteTypeDetailName = 'SINGLE ORDER CONTAINER'
		}
	}

	return toteTypeDetailName;
} /* GetToteType */

function WriteToFile(Msg){
	if(Msg){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' ' + Msg);
	}
} /* WriteToFile */

function requireLogin (req, res, next){
  if (!req.session.user){
     return res.status(200).send({NoUser:true})
  } else {
    next();
  }
};

/*=================== Functions End =============================================*/

module.exports = router
