/*
 * @Author: Deon Wolmarans
 * @Date: 2017-07-11 12:09:36
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-07-12 09:05:36
 */

var apexLib = require('apex_lib/moduleIndex.js')
var express = require('express')
var router = express.Router()

// Get all the Transporters.
router.get('/API/transporters/', function (req, res) {
    apexLib.mongo.get.mongoMultiGet(global.mongo.db, 'transporters', '', function (error, doc) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(doc)
        }
    })
})

// Get a transporter per ID.
router.get('/API/transporters/:id', function (req, res) {
    apexLib.mongo.get.mongoSingleGet(global.mongo.db, 'transporters', { transporterID: req.params.id }, function (error, doc) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(doc)
        }
    })
})

// Post a transporter.
router.post('/API/transporters/', function (req, res) {
    var transporter = {
        transporterID: req.body.transporterID,
        courierID: req.body.courierID,
        destination: req.body.destination,
        currentStation: req.body.currentStation,
        cartons: req.body.cartons
    }

    apexLib.mongo.insert.mongoSingleInsert(global.mongo.db, 'transporters', transporter, function (error, doc) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(doc)
        }
    })
})

// Update a transporter.
router.put('/API/transporters/', function (req, res) {
    var updateDocId = req.body.transporterID

    var transporter = {
        transporterID: req.body.transporterID,
        courierID: req.body.courierID,
        destination: req.body.destination,
        currentStation: req.body.currentStation,
        cartons: req.body.cartons
    }

    apexLib.mongo.update.mongoSingleUpdate(global.mongo.db, 'transporters', { transporterID: updateDocId }, transporter, function (error, doc) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(doc)
        }
    })
})

// Delete a transporter.
router.delete('/API/transporters/:id', function (req, res) {
    var id = req.params.id

    apexLib.mongo.delete.mongoSingleDelete(global.mongo.db, 'transporters', { transporterID: id }, function (error, doc) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(doc)
        }
    })
})

module.exports = router
