/*
 * @Author: Clive Matlawa
 * @Date: 2017-09-12
 */
var apexLib = require('apex_lib/moduleIndex.js')
var PickListModel = require('pickListModel')
var PackStationModel = require('packStationModel')
var Params = require('parameterModel')
var PwLocs = require('putWallModel')
var express = require('express')
var moment = require('moment')
var router = express.Router()
var redis = require('redis');
var Pubclient = redis.createClient();
var request     = require('request');
var serialUrl = require('ui').serialUrl
var pullWallUrl = require('ui').pullWall
var AlarmMod = require('../../module/alarmAndEvents/AlarmAndEventModule')



// Get a pick list per ID.
router.get('/API/pwpickList/:id', requireLogin, function (req, res){
	var Tote = req.params.id;
	var user = req.session.username;
	var IpAddr = req.session.ip;
	var Msg = null;

  GetStation(IpAddr, function (Station){
		if (!Station){
			Msg = 'Scan Tote: User ' + user + ' failed scan tote from an unknown station';
			WriteToFile(Msg);
			return res.status(200).send({NoStation:true});
		} else {
			if(Tote == "undefined" || !Tote){
				Tote = Station.currentTote;
			}

			Msg = 'Request from station ' + Station.stationNumber + ' with tote ' + Tote;
			WriteToFile(Msg);

			if(Tote == "undefined" || !Tote){
				return res.status(200).send({ScanMode:false});
			}

			PickListModel.findOne({toteID: Tote}, function (error, doc){
				if (error || !doc){
					Msg = 'Tote not found for station ' + Station.stationNumber + ' scanned tote ' + Tote;
					WriteToFile(Msg);
					return res.status(405).send('No data found for scanned tote ' + Tote);
				} else {
					var Err = ValidateScannedTote(doc, Station.stationNumber)

					if(Err){
						return res.status(405).send('Tote ' + Tote + ': ' + Err);
					} else {
						ToteValidForPutWalls(doc, Station.stationNumber, function(Valid){
							var ToteMode = null;
							if(Valid){
								ToteMode = 'PUT';

								CheckIfPickListIsUnique(doc, 0, function(Result){
									if(Result.unique){
										CheckIfWaitingForConfirmation(Station.stationNumber, function(ConfirmData){
											if(ConfirmData){
												var Instr = null;
												if(ConfirmData.Serial){
													if(ConfirmData.Serial == ''){
														Instr = 'Put item into location ' + Details.Location + ' with the light on. Push the button to confirm';
													} else {
													  Instr = 'Put item ' + ConfirmData.Serial + ' into location ' + ConfirmData.Location + ' with the light on. Push the button to confirm';
													}
												} else if(ConfirmData.Document){
													Instr = 'Put Scanned Document ' + ConfirmData.orderID + ' into location ' + ConfirmData.Location + ' with the light on. Push the button to confirm';
												}

												LightupLoc(ConfirmData.Location, '_ON_GREEN', Station.stationNumber, function(Response){
													if(Response.error){
														return res.status(405).send('Error while trying to switch on light for location ' + ConfirmData.Location);
													} else {
														return res.status(200).send({ToteMode:ToteMode,
																												 ScanMode: true,
																												 signedInStation:Station.stationNumber,
																												 Next: 'PUT_ITEM',
																												 Location: ConfirmData.Location,
																												 Instruction: Instr});
													}
												});
											} else {
												DetermineNextStepInProcess(doc, 0, user, Station.stationNumber, function(Details){
													if(Details.ScanMode == false){
														return res.status(200).send(Details);
													} else {
														var savedDoc = Details.savedDoc;
														if(Details.Error){
															return res.status(405).send(Details);
														} else {
															if(savedDoc.orders.length > 0){
																AddToteToStation(savedDoc, Station, user, function(returnData){
																	if(returnData.Error){
																		return res.status(405).send(returnData.Error);
																	} else {
																		savedDoc.toteInUse = true;
																		savedDoc.toteInUseBy = Station.stationNumber.toString();
																		savedDoc.containerLocation = null;

																		savedDoc.save(function(err, ready){
																			return res.status(200).send({ToteMode:ToteMode,
																																	 ScanMode: true,
																																	 signedInStation:Station.stationNumber,
																																	 Next: Details.Next});
																		});
																	}
																});
															} else {
																DestroyMasterPickList(savedDoc, Station.stationNumber, function(){
																  return res.status(200).send({ScanMode:false});
															  });
															}
														}
													}
												});
											 }
										 })
									 } else {
										 Msg = 'Tote ' + Tote + ' has a duplicate picklist';
										 WriteToFile(Msg);
										 return res.status(405).send(Msg);
									 }
							 });
							} else {
							  AddToteToStation(doc, Station, user, function(returnData){
								  if(returnData.Error){
									  return res.status(405).send(returnData.Error);
								  } else {
										return res.status(200).send({ToteMode:'MAN'});
								  }
							  });
							}
						});
					}
				}
			});
		}
	});
});

router.get('/API/pwreprintCarton/', requireLogin, function (req, res)
{
	var user = req.session.username;
	var IpAddr = req.session.ip;
	var Msg = null;

	GetStation(IpAddr, function (doc)
	{
		if (!doc)
		{
			Msg = 'Document Reprint: User ' + user + ' failed attempt to reprint documents from an unknown station';
			WriteToFile(Msg);
			return res.status(200).send({NoStation:true});
		}
		else
		{
			var Tote = doc.currentTote;

			if(!Tote)
			{
				return res.status(200).send({ScanMode:false});
			}

			PickListModel.findOne({toteID: Tote}, function (error, PickList)
			{
				var CurrentOrderToProcess = GetNextOrderToProcess(PickList);
				GetAllItemsAndCartonsForOrder(PickList, CurrentOrderToProcess, function(OrdDetails)
				{
					if(OrdDetails.error)
					{
						return callback(OrdDetails);
					}
					else
					{
						DetermineCartonNo(PickList, CurrentOrderToProcess, function(savedDoc)
						{
							if(savedDoc.error)
							{
								return res.status(405).send(Details.error);
							}
							else
							{
								SendDocsLabelPrint(savedDoc, CurrentOrderToProcess, savedDoc.orders[CurrentOrderToProcess].UseTransporter, OrdDetails, function(Data)
								{
									Msg = 'Document Reprint: User ' + user + ' reprinted labels/documents for order ' + savedDoc.orders[CurrentOrderToProcess].orderID + ' at station ' + doc.stationNumber;
									WriteToFile(Msg);

									/*CreateResponseData(savedDoc, doc.stationNumber, CurrentOrderToProcess, user, function(Details)
									{
										if(Details.error)
										{
											return res.status(405).send(Details.error);
										}
										else
											return res.status(200).send(Details);
									});*/
								});
							}
						});
					}
				});
			});
		}
	});
});

router.get('/API/pwupdateVerifyScannedDoc/:id', requireLogin, function (req, res){
	var user = req.session.username;
	var IpAddr = req.session.ip;
	var ScannedDoc = req.params.id;
	var Msg = null;

	GetStation(IpAddr, function (doc){
		if (!doc){
			Msg = 'Document Validation: User ' + user + ' failed attempt to validate order document from an unknown station';
			WriteToFile(Msg);
			return res.status(200).send({NoStation:true});
		} else {
			var Tote = doc.currentTote;

			if(!Tote){
				return res.status(200).send({ScanMode:false});
			}

			PickListModel.findOne({toteID: Tote}, function (error, PickList){
				if (error || !PickList){
					Msg = 'Document Validation: Cannot find picklist for tote ' + Tote + ' associated with station ' + doc.stationNumber;
					WriteToFile(Msg);
					return res.status(200).send({error:"Cannot find a picklist for tote " + Tote});
				} else {
					var x = 0;
					while(x < PickList.orders.length){
						if(PickList.orders[x].orderID == ScannedDoc){
							break;
						}
						x++;
				  }

				  if(x < PickList.orders.length){
						FindPutWallLocationUsingOrderID(PickList.orders[x].orderID, doc.stationNumber, function(Lane){
							if(Lane){
								LightupLoc(Lane, '_ON_GREEN', doc.stationNumber, function(Response){
									if(Response.error){
										return res.status(405).send('Error while trying to switch on light for location ' + Lane);
									} else {
										var LocationData = {
											Document: PickList.orders[x].orderID,
											PickList: PickList.orders[x].pickListID,
											ToteID: PickList.toteID
										}

										UpdateLocationToBeInUse(Lane, PickList.orders[x].orderID, PickList.orders[x].courierID, LocationData, function(){
											res.status(200).send({ToteMode:'PUT',
																						ScanMode: true,
																						signedInStation:doc.stationNumber,
																						Next: 'PUT_ITEM',
																						Location: Lane,
																						Instruction: 'Put Scanned Document ' + PickList.orders[x].orderID + ' into location ' + Lane + ' with the light on. Push the button to confirm'});
										});
									}
								});
							}
						});
					} else {
						return res.status(405).send('Scanned Document Number is not valid for any order in tote');
					}
				}
			});
		}
	});
});

router.get('/API/pwpickListReset/', requireLogin, function (req, res)
{
	var user = req.session.username;
	var IpAddr = req.session.ip;
	var Msg = null;

	GetStation(IpAddr, function (doc)
	{
		if (!doc)
		{
			Msg = 'Tote Reset: User ' + user + ' failed attempt to reset tote from an unknown station';
			WriteToFile(Msg);
			return res.status(200).send({NoStation:true});
		}
		else
		{
			var Tote = doc.currentTote;

			if(!Tote)
			{
				return res.status(200).send({ScanMode:false});
			}

			Msg = 'Tote Reset: User ' + user + ' attempting to reset tote ' + Tote + ' at station ' + doc.stationNumber;
			WriteToFile(Msg);

			PickListModel.findOne({toteID: Tote}, function (error, PickList)
			{
				if (error || !PickList)
				{
					Msg = 'Tote Reset: Cannot find picklist for tote ' + Tote + ' associated with station ' + doc.stationNumber;
					WriteToFile(Msg);
					return res.status(200).send({error:"Cannot find a picklist for tote " + Tote});
				}
				else
				{
					ResetPicklistForTote(PickList, user, doc.stationNumber, function(Details)
					{
						if(Details.error)
						{
							return res.status(405).send(Details.error);
						}
						else
						{
							LightAllLocToClear(PickList, 0, doc.stationNumber, function(){
								ClearToteFromStation(doc.stationNumber, function(Details)
								{
									Msg = 'Tote Reset: Tote ' + Tote + ' successfully reset and removed from station ' + doc.stationNumber;
									WriteToFile(Msg);
									return res.status(200).send(Details);
								});
							});
						}
					});
				}
			});
		}
	});
});

router.get('/API/pwcheckLocConfirmed/:id', function (req, res){
	var putWallLoc = req.params.id;

	PwLocs.findOne({'putAddress': putWallLoc}, function(err, PWLOC){
		if(!err && PWLOC){
			if(PWLOC.active){
				Msg = 'Location ' + putWallLoc + ' Not yet confirmed';
				WriteToFile(Msg);
				return res.status(200).send({Confirmed:false});
			} else {
				Msg = 'Location ' + putWallLoc + ' confirmed';
				WriteToFile(Msg);
				return res.status(200).send({Confirmed:true});
			}
		} else {
			Msg = 'Location ' + putWallLoc + ' Not yet confirmed';
			WriteToFile(Msg);
			return res.status(200).send({Confirmed:false});
		}
	});
});

// Get the serial record from the db.
router.get('/API/pwgetSerial/:id', requireLogin, function (req, res)
{
	var user = req.session.username;
	var ScannedSerial = req.params.id;
	var IpAddr = req.session.ip;
	var Msg = null;

	GetStation(IpAddr, function (Station){
		if (!Station){
			Msg = 'Serial Scan: User ' + user + ' failed attempt to validate serial number from an unknown station';
			WriteToFile(Msg);
			return res.status(200).send({NoStation:true});
		} else {
			if(!Station.currentTote){
				return res.status(200).send({ScanMode:false});
			}

      PickListModel.find({'orders.items.serials': {$regex: ScannedSerial, $options:'i'}}, function (error, MongoSerial){
		    var SerialGood = false;
		    var FoundIndex = null;
				if(error || !MongoSerial || MongoSerial.length <= 0){
					SerialGood = true;
				} else {
					var x = 0;
					while(x < MongoSerial.length){
						if(MongoSerial[x].Status != 'RETURNED'){
							FoundIndex = x;
							break;
						}
						x++;
					}

					if(FoundIndex == null){
						Msg = 'Serial number ' + ScannedSerial + ' Found in a returned order so its good to be re-despatched';
						WriteToFile(Msg);
						SerialGood = true;
					}
				}

				if(SerialGood){
					var url = serialUrl + ScannedSerial

					apexLib.rest.get(url, null, function (response){
						if (response instanceof Error){
							AlarmMod.addSubSystemCommsError()
						  	Msg = 'Serial Scan: Error while trying to connect to WMS for serial validation | ' + response;
							WriteToFile(Msg);
							res.status(405).send('Error: Serial validation service is down, please call supervisor');
							return;
						}

						var dbResult = response;
						if(dbResult && dbResult != null){
							var serial = dbResult.serial;
							var sku = dbResult.SKU;
							var serialStatus = dbResult.serialStatus;
							var despatched = dbResult.dispatched;
							var Quantity = 1;

							if (!serial || serial == ''){
								Msg = 'Serial Scan: Station: ' + Station.stationNumber + ' | User ' + user + ': serial validation failed in WMS for scanned serial ' + ScannedSerial + ' [WMS returned]: ' + serial;
								WriteToFile(Msg);
								res.status(405).send('Scanned serial number ' + ScannedSerial + ' could not be validated');
								return;
							}

							/*if (ScannedSerial != serial && (("892700000" + ScannedSerial) != serial))
							{
								Quantity = dbResult.serialCount;
								Msg = 'Serial Scan: User ' + user + ': mismatch between scanned serial number and result returned from WMS ->[scanned]: ' + ScannedSerial + ' [WMS returned]:' + serial;
								WriteToFile(Msg);
								res.status(405).send('Scanned serial number could not be validated');
								return;
							}*/

							if ((ScannedSerial == serial) || (("892700000" + ScannedSerial) == serial) || (("892700008" + ScannedSerial) == serial)){
								Msg = 'User: ' + user + ' scanned serial: ' + ScannedSerial + ' | WMS returned: ' + serial + ' at station: ' + Station.stationNumber;
								WriteToFile(Msg);
							} else {
								Quantity = dbResult.serialCount;
								Msg = 'Serial Scan: Station: ' + Station.stationNumber + ' | User ' + user + ': mismatch between scanned serial number and result returned from WMS ->[scanned]: ' + ScannedSerial + ' [WMS returned]:' + serial;
								WriteToFile(Msg);
								res.status(405).send('Scanned serial number ' + ScannedSerial + ' could not be validated');
								return;
							}

							if(dbResult.serialCount){
								Quantity = dbResult.serialCount;
								Msg = 'Serial Scan: User ' + user + ' scanned serial ' + serial + ' at station ' + Station.stationNumber + ' serial quantity ' + Quantity;
								WriteToFile(Msg);
							}

							if(despatched == 1){
								Msg = 'Serial Scan: User ' + user + ' failed attempt to despatch an already despached serial ' + serial + ' at station ' + Station.stationNumber;
								WriteToFile(Msg);
								res.status(405).send('Scanned serial number ' + serial + ' is already despatched');
								return;
							} else {
								if(serialStatus === 'BROKEN'){
									Msg = 'Serial Scan: User ' + user + ' failed attempt to despatch a serial ' + serial + ' with serial status ' + serialStatus + ' at station ' + Station.stationNumber;
									WriteToFile(Msg);
									res.status(405).send('Scanned container serial number ' + serial + ' is no longer a fullpack');
									return;
								} else {
									UpdateSerialToPickList(user, Station, sku, Quantity, serial, function(Details){
										if(Details.error){
											res.status(405).send(Details.error);
										} else {
											var LocationData = {
												Sku: Details.Sku,
												Serial: Details.Serial,
												Quantity: Details.Quantity,
												PickList: Details.PickList,
												User: Details.User,
				                ToteID: Details.ToteID
											}

											UpdateLocationToBeInUse(Details.Location, Details.Order, Details.Courier, LocationData, function(){
											});


											res.status(200).send({ToteMode:'PUT',
																						ScanMode: true,
																						signedInStation:Station.stationNumber,
																						Next: 'PUT_ITEM',
																						Location: Details.Location,
																						Instruction: 'Put item ' + Details.Serial + ' into location ' + Details.Location + ' with the light on. Push the button to confirm'});
										}
									});
								}
							}
						} else {
							CheckIfNonSerialised(ScannedSerial, Station.currentTote, Station.stationNumber, user, function(Details){
								if(Details.error){
									res.status(405).send(Details.error);
								} else {
									var LocationData = {
										Sku: Details.Sku,
										Serial: Details.Serial,
										Quantity: Details.Quantity,
										PickList: Details.PickList,
										User: Details.User,
										ToteID: Details.ToteID
									}

									UpdateLocationToBeInUse(Details.Location, Details.Order, Details.Courier, LocationData, function(){
										res.status(200).send({ToteMode:'PUT',
																					ScanMode: true,
																					signedInStation:Station.stationNumber,
																					Next: 'PUT_ITEM',
																					Location: Details.Location,
																					Instruction: 'Put item into location ' + Details.Location + ' with the light on. Push the button to confirm'});
									});
								}
							});
						}
					});
				} else {
					if(MongoSerial[FoundIndex].toteID == Station.currentTote){
						Msg = 'Serial Scan: Station: ' + Station.stationNumber + ' | User ' + user + ' scanned serial number ' + ScannedSerial + ' which is already scanned into tote ' + Station.currentTote;
						WriteToFile(Msg);
						res.status(405).send('Scanned serial number ' + ScannedSerial + ' has already been scanned');
					} else {
						Msg = 'Serial Scan: Station: ' + Station.stationNumber + ' | User ' + user + ' scanned serial number ' + ScannedSerial + ' which is already despatched to order ' + MongoSerial[FoundIndex].orders[0].orderID;
						WriteToFile(Msg);
						res.status(405).send('Scanned serial number ' + ScannedSerial + ' is already despatched');
					}
				}
			});
		}
	});
});

/*=================== Functions =================================================*/

function GetAllItemsAndCartonsForOrder(savedDoc, CurrentOrderToProcess, callback)
{
	var Msg = null;
	PickListModel.find({'orders.orderID': savedDoc.orders[CurrentOrderToProcess].orderID}, function(err, PickLists)
	{
		if(err || !PickLists || PickLists.length <= 0)
		{
			Msg = "Error finding all picklists for order " + Data.orders[CurrentOrderToProcess].orderID;
			WriteToFile(Msg);
			return callback({error:"Problem encountered fetching all picklists for order - please call supervisor"});
		}
		else
		{
			var Cartons = [];
			var OrdItems = [];
			var PL = 0;
			while(PL < PickLists.length)
			{
				var OrdIndex = 0;

				while(OrdIndex < PickLists[PL].orders.length)
				{
					if(PickLists[PL].orders[OrdIndex].orderID == savedDoc.orders[CurrentOrderToProcess].orderID)
					{
						var itemIndx = 0;
						while(itemIndx < PickLists[PL].orders[OrdIndex].items.length)
						{
							OrdItems.push(PickLists[PL].orders[OrdIndex].items[itemIndx]);
							Cartons.push(PickLists[PL].orders[OrdIndex].cartonID);
							itemIndx++;
						}
					}
					OrdIndex++;
				}
				PL++;
			}

			var ReturnData =
			{
				Cartons: Cartons,
				OrdItems: OrdItems
			}

			return callback(ReturnData);
		}
	});
} /* GetAllItemsAndCartonsForOrder */

function UpdateLocationToBeInUse(Location, Order, Courier, LocationData, callback){
	PwLocs.findOne({'putAddress': Location}, function(err, PWLOC){
		if(!err && PWLOC){
			PWLOC.orderID = Order;
			PWLOC.courier = Courier;
			PWLOC.active = true;
			PWLOC.Data = JSON.stringify(LocationData);
			PWLOC.save(function(){
				return callback();
			});
		} else {
			return callback();
		}
	});
} /* UpdateLocationToBeInUse */

function AddToteToStation(savedDoc, Station, user, callback){
	if(Station.currentTote != savedDoc.toteID){
	 Station.currentTote = savedDoc.toteID;

	 Station.save(function(err, savedStn){
		 if(!err && savedStn){
			 Msg = 'Tote ' + savedDoc.toteID + ' added to station ' + savedStn.stationNumber + ' by user ' + user;
			 WriteToFile(Msg);
			 return callback({Error:null});
		 } else {
			 Msg = 'Error adding Tote ' + savedDoc.toteID + ' added to station ' + savedStn.stationNumber + ' by user ' + user;
			 WriteToFile(Msg);
			 return callback({Error:'Failed to add tote ' + savedDoc.toteID + ' to station'});
		 }
	 });
	} else {
		return callback({Error:null});
	}
} /* AddToteToStation */

function ToteValidForPutWalls(doc, Station, callback){
        //return callback(false);
	if(doc.packType != 'PUT'){
		GetAutobaggerMaxVolume(function(Param){
			if(Param.volumeCapacity){
				CheckIfOrdersInToteAreValidforPutWall(doc, 0, Param.volumeCapacity, function(Result){
					if(Result){
						PwLocs.findOne({'packStation': Station}, function(err, PWLOC){
							if(err || !PWLOC){
								return callback(false);
							} else {
								return callback(true);
							}
						});
					} else {
						return callback(false);
					}
				});
			} else {
				return callback(false);
			}
		});
	} else {
		PwLocs.findOne({'packStation': Station}, function(err, PWLOC){
			if(err || !PWLOC){
				return callback(false);
			} else {
				return callback(true);
			}
		});
	}
} /* ToteValidForPutWalls */

function GetAutobaggerMaxVolume(callback){
	Params.find({}, function(err, Parameters){
		if(err || !Parameters || Parameters.length <= 0){
			Msg = "Failed to find parameters in the system ";
			WriteToFile(Msg);
			return callback({error:Msg});
		} else {
			var CartonPrefix = 1;

			if(Parameters[0].weightSettings && Parameters[0].weightSettings.length > 0){
				var paramIndex = 0;
				while(paramIndex < Parameters[0].weightSettings.length){
					if(Parameters[0].weightSettings[paramIndex].StockSize == CartonPrefix){
						break;
					}
					paramIndex++;
				}

				if(paramIndex < Parameters[0].weightSettings.length){

					return callback({volumeCapacity:Parameters[0].weightSettings[paramIndex].volumeCapacity});
				} else {
					Msg = "Failed to find Carton weightSettings to match order carton field for carton " +
					       PickList.orders[CurrentOrderToProcess].cartonID + " of order " + PickList.orders[CurrentOrderToProcess].orderID;
					WriteToFile(Msg);
					return callback({error:Msg});
				}
			} else {
				Msg = "Failed to find weightSettings parameter in the system ";
				WriteToFile(Msg);
				return callback({error:Msg});
			}
		}
	});
} /* GetAutobaggerMaxVolume */

function CheckIfOrdersInToteAreValidforPutWall(doc, index, volumeCapacity, callback){
	if(index < doc.orders.length){
		var PickListVolume = CalcPLVol(doc.orders[index].items);
		if(PickListVolume <= volumeCapacity){
			PickListModel.count({'orders.orderID':doc.orders[index].orderID}, function( err, PLsPerOrder){
				if(PLsPerOrder == 1){
					index++;
					CheckIfOrdersInToteAreValidforPutWall(doc, index, volumeCapacity, callback);
				} else {
					return callback(false);
				}
		  });
		} else {
			return callback(false);
		}
	} else {
		return callback(true);
	}
} /* CheckIfOrdersInToteAreValidforPutWall */

function CalcPLVol(items){
	var PickListVolume = 0;
	var weight = 0;
	var volume = 0;

	if(items.length > 0){
		var x = 0;
		while(x < items.length){
			if(items[x].eachVolume)
			  volume += ((items[x].qty * items[x].eachVolume) / 1)

			x++;
		}

		PickListVolume = volume;
	}

	return PickListVolume;
} /* CalcPLVol */

function CheckIfNonSerialised(Barcode, Tote, Station, user, callback){
	PickListModel.findOne({'toteID': Tote, 'orders.items.barcode': Barcode}, function (error, PickList){
		if (error || !PickList){
			Msg = 'Serial Scan: User ' + user + ' scanned serial number ' + Barcode + ' and could not be validated in WMS at station ' + Station;
			WriteToFile(Msg);
			return callback({error:'Scanned serial number ' + Barcode + ' could not be validated'});
		} else {
		  var CurrentOrderToProcess = 0;

      while(CurrentOrderToProcess < PickList.orders.length){
				var x = 0;
				var itemFound = false;
				var RoomforOneMore = false;
				var Serialised = false;

				while(x < PickList.orders[CurrentOrderToProcess].items.length){
					itemFound = false;
					if(PickList.orders[CurrentOrderToProcess].items[x].barcode == Barcode){
						if(!PickList.orders[CurrentOrderToProcess].items[x].serialised){
							itemFound = true;

							var Qtypacked = PickList.orders[CurrentOrderToProcess].items[x].qtyPacked;
							if(Qtypacked == null){
								Qtypacked = 0;
							}

							var QtyRem = pickQty - Qtypacked;
							if(QtyRem > 0){
								RoomforOneMore = true;
								break;
							}
						} else {
							Serialised = true;
							break;
						}
					}
					x++;
				}

				if(itemFound && RoomforOneMore){
					break;
				}

				if(Serialised){
				  break;
				}
				CurrentOrderToProcess++;
			}

			if(x < PickList.orders[CurrentOrderToProcess].items.length){
				if(Serialised){
					Msg = 'Non Serialised Scan: User ' + user + ' scanned sku barcode ' + Barcode + ' which is not for a non-serialised product';
					WriteToFile(Msg);
					return callback({error:'Scanned product barcode ' + Barcode + ' is not for a non-serialised product'});
				} else {
					if(itemFound && RoomforOneMore){
						FindAndLightAppropriateLane(PickList.orders[CurrentOrderToProcess].orderID, Station, function(Lane){
							if(Lane.Err){
								Msg = 'Station: ' + Station + ' ran out of working put wall locations to use';
								WriteToFile(Msg);

								return callback({error:Lane.Err})
							}

							return callback({Location: Lane.Lane,
															 Order: PickList.orders[CurrentOrderToProcess].orderID,
															 Sku: PickList.orders[CurrentOrderToProcess].sku,
															 Serial: '',
															 Quantity: 1,
															 Courier: PickList.orders[CurrentOrderToProcess].courierID,
															 User: user,
															 ToteID: PickList.toteID,
															 PickList: PickList.orders[CurrentOrderToProcess].pickListID});
						});
					} else {
						Msg = 'Non Serialised Scan: Station: ' + Station + ' | User ' + user + ' scanned sku barcode ' + Barcode + ' which is fully packed';
						WriteToFile(Msg);
						return callback({error:'Already packed enough stock of this product to the order'});
					}
				}
			} else {
				Msg = 'Non Serialised Scan: Station: ' + Station + ' | User ' + user + ' scanned sku barcode ' + Barcode + ' which is not valid for any item in order ' + PickList.orders[CurrentOrderToProcess].orderID;
				WriteToFile(Msg);
				return callback({error:'Scanned product barcode ' + Barcode + ' is not valid for this order'});
			}
		}
	});
} /* CheckIfNonSerialised */

function ValidateScannedTote(PickList, Station){
	var valid = null;
	if(PickList.toteID.length == 7){
		if(parseInt(PickList.toteID[0]) == 0 || parseInt(PickList.toteID[0]) == 1){
			if(PickList.toteInUse && Station != parseInt(PickList.toteInUseBy)){
				valid = 'Tote in use at Station ' + PickList.toteInUseBy;
			} else {
				if(PickList.orders.length > 0){
					var index = 0;
					while(index < PickList.orders.length){
						var itemIndex = 0;
						while(itemIndex < PickList.orders[index].items.length){
							if(PickList.orders[index].items[itemIndex].picked == false){
								valid = 'Not all items in Tote are picked';
								break;
							}

							if(PickList.orders[index].items[itemIndex].pickQty != PickList.orders[index].items[itemIndex].qty){
								valid = 'Quantity mismatch between item quantity and picked quantity for sku ' + PickList.orders[index].items[itemIndex].sku;
								break;
							}
							itemIndex++;
						}

						if(valid != null)
							break;

						index++;
					}

					var PackedOrderCount = 0;
					index = 0;
					while(index < PickList.orders.length){
						if(PickList.orders[index].packComplete){
							PackedOrderCount++;
						}

						index++;
					}

					if(PackedOrderCount >= PickList.orders.length){
						valid = 'All orders in tote are already packed';
					}
				} else {
					valid = 'PickList for this tote does not contain any orders';
				}
			}
		} else {
			valid = 'Not a tote';
		}
	} else {
		valid = 'Invalid Id for Tote';
	}

	return valid;
} /* ValidateScannedTote */

function UpdateSerialToPickList(user, Station, sku, Quantity, serial, callback){
	var Tote = Station.currentTote;
	var Msg = null;

	PickListModel.findOne({toteID: Tote}, function (error, PickList){
		if (error || !PickList){
			Msg = 'Serial scan: Cannot find picklist for tote associated with station ' + Station.stationNumber;
			WriteToFile(Msg);
			return callback({error:"Cannot find a picklist for tote " + Tote});
		} else {
			UpdateSerial(PickList, user, sku, Quantity, serial, Station.stationNumber, 0, function(Details){
				return callback(Details);
			});
		}
	});
} /* UpdateSerialToPickList */

function GetStation(IpAddr, callback){
	PackStationModel.findOne({stationIP: IpAddr}, function (error, doc){
		if(error || !doc){
			return callback(null);
		} else {
			var now = new Date();
			doc.updated_at = now;
			doc.active = true;
			doc.save(function(error, savedDoc){
				return callback(savedDoc);
			});
		}
	});
} /* GetStation */

function UpdateSerial(PickList, user, sku, Quantity, serial, Station, CurrentOrderToProcess, callback){
	var OriginalQty = Quantity;
	var OrdIndex = CurrentOrderToProcess;
	var QtyPacked = null;
	var Msg = null;

  if(OrdIndex < PickList.orders.length){
		var itemIndex = 0;
		while(itemIndex < PickList.orders[OrdIndex].items.length){
			QtyPacked = PickList.orders[OrdIndex].items[itemIndex].qtyPacked;
			if(QtyPacked == null){
				QtyPacked = 0;
			}

			var QtyRem = PickList.orders[OrdIndex].items[itemIndex].pickQty - QtyPacked;

			if(sku == PickList.orders[OrdIndex].items[itemIndex].sku && QtyRem >= Quantity && !PickList.orders[OrdIndex].items[itemIndex].packed){
				break;
			}
			itemIndex++;
		}

		if(itemIndex < PickList.orders[OrdIndex].items.length){
			FindAndLightAppropriateLane(PickList.orders[OrdIndex].orderID, Station, function(Lane){
				if(Lane.Err){
					Msg = 'Station: ' + Station + ' ran out of working put wall locations to use';
					WriteToFile(Msg);

					return callback({error:Lane.Err})
				}

				return callback({Location: Lane.Lane,
				                 Order: PickList.orders[OrdIndex].orderID,
				                 Sku: sku,
				                 Serial: serial,
				                 Quantity: Quantity,
				                 Courier: PickList.orders[OrdIndex].courierID,
				                 User: user,
				                 ToteID: PickList.toteID,
				                 PickList: PickList.orders[OrdIndex].pickListID});
			});
		} else {
			OrdIndex++;
			UpdateSerial(PickList, user, sku, Quantity, serial, Station, OrdIndex, callback);
		}
	} else {
		Msg = "Scanned serial number " + serial + " is not part of this order";
		WriteToFile(Msg);
		return callback({error:Msg});
	}
} /* UpdateSerial */

function FindAndLightAppropriateLane(orderID, Station, callback){
	FindPutWallLocationUsingOrderID(orderID, Station, function(Lane){
		if(Lane){
			LightupLoc(Lane, '_ON_GREEN', Station, function(Response){
				if(Response.error){
					UpdateLightError(Lane, Station, function(){
						FindAndLightAppropriateLane(orderID, Station, callback);
					});
				} else {
					return callback({Lane:Lane});
				}
			});
		} else {
			FindEmptyLocationUsingStation(Station, function(Lane){
				if(Lane){
					LightupLoc(Lane, '_ON_GREEN', Station, function(Response){
						if(Response.error){
							UpdateLightError(Lane, Station, function(){
								FindAndLightAppropriateLane(orderID, Station, callback);
							});
						} else {
							return callback({Lane:Lane});
						}
					});
				} else {
					return callback({Err:'No more locations available to use'});
				}
			});
		}
	});
} /* FindAndLightAppropriateLane */

function UpdateLightError(Lane, Station, callback){
	PwLocs.findOne({'putAddress': Lane}, function(err, PWLOC){
		if(!err && PWLOC){
			PWLOC.putLightError = true;
			PWLOC.save(function(){
				return callback();
			});
		} else {
			return callback();
		}
	});
} /* UpdateLightError */

function LightupLoc(Lane, color, Station, callback){
	if(Station % 2 == 0){
	  var Url = pullWallUrl.urlRightController
	} else {
		var Url = pullWallUrl.urlLeftController
	}
	request({
		method:'POST',
		url: Url,
		body:Lane + color,
		headers: {'Content-Type': 'text/xml'}
	},function(error, response, body){
		if(error){
			Msg = 'error: ' + error;
			WriteToFile(Msg);
			return callback({error: Msg});
		} else if(!body){
			Msg = 'there is no body.';
			WriteToFile(Msg);
			return callback({error: Msg});
		} else {
			Msg = 'body: ' + body;
			WriteToFile(Msg);
			return callback({error: null});
		}
	});
} /* LightupLoc */

function LightAllLocToClear(PickList, index, Station, callback){
	if(index < PickList.orders.length){
		FindPutWallLocationUsingOrderID(PickList.orders[index].orderID, Station, function(Lane){
			if(Lane){
				LightupLoc(Lane, '_ON_RED', Station, function(Response){
					ClearPutWallLocation(Lane, function(){
						index++;
						LightAllLocToClear(PickList, index, Station, callback);
					});
				});
			} else {
				index++;
				LightAllLocToClear(PickList, index, Station, callback);
			}
		});
	} else {
		return callback();
	}
}

function FindPutWallLocationUsingOrderID(OrderID, Station, callback){
	PwLocs.findOne({'packStation': Station, 'orderID': OrderID}, function(err, PWLOC){
		if(!err && PWLOC){
			return callback(PWLOC.putAddress);
		} else {
			return callback(null);
		}
	});
} /* FindPutWallLocationUsingOrderID */

function ClearPutWallLocation(Lane, callback){
	PwLocs.findOne({'putAddress': Lane}, function(err, PWLOC){
		if(!err && PWLOC){
			PWLOC.orderID = null;
			PWLOC.courier = null;
			PWLOC.putLightError = false;
			PWLOC.pullLightError = false;
			PWLOC.putComplete =false;
			PWLOC.Data = null;
			PWLOC.save(function(err, savedDoc){
				return callback();
			});
		} else {
			return callback();
		}
	});
} /* FindPutWallLocationUsingOrderID */

function FindEmptyLocationUsingStation(Station, callback){
	console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ | ' + Station + ' | ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
	PwLocs.find({'packStation': Station, 'orderID': null}, function(err, PWLOC){
		if(!err && PWLOC.length > 0){
			var x = 0;
			while(x < PWLOC.length){
				if(PWLOC[x].putLightError == true){
					console.log('TRUE 1')
				} else if(PWLOC[x].pullLightError == true){
					console.log('TRUE 2')
				} else {
					console.log('SUPPOSED TO RETURN')
					return callback(PWLOC[x].putAddress);
				}
				x++;
			}

			if(x >= PWLOC.length){
				return callback(null);
			}
		} else {
			return callback(null);
		}
	});
} /* FindEmptyLocationUsingStation */

function ResetPicklistForTote(PickList, user, Station, callback)
{
	//DeAssociatePackedOrders(PickList, user, Station, function(Details)
	//{
		//if(Details.error)
		//{
		///	return callback(Details);
		//}
		//else
		//{
			var Details = PickList;
			var x = 0;
			while(x < Details.orders.length)
			{
				var y = 0;
				while(y < Details.orders[x].items.length)
				{
					Details.orders[x].items[y].packedBy = null;
					Details.orders[x].items[y].serials = [];
					Details.orders[x].items[y].packed = false;
					Details.orders[x].items[y].qtyPacked = null;
					y++;
				}

				Details.orders[x].packed = false;
				Details.orders[x].picklistWeight = null;
				Details.orders[x].toteNumber = null;
				Details.orders[x].cartonID = null;
				x++;
			}

			Details.ShipUpdateRequired = false;
			Details.PickUpdateRequired = false;
			Details.PackUpdateRequired = false;
			Details.Status = 'PICKED';
			Details.PackedDate = null;
			Details.toteInUse = false;
			Details.toteInUseBy = "";
			Details.transporterID = null;

			Details.save(function(error, DocSaved)
			{
				if(error || !DocSaved)
				{
				  Msg = "Failed to reset tote " + Details.toteID + " at station " + Station + ". Reason: " + error;
				  WriteToFile(Msg);
					return callback({error:Msg});
				}
				else
				{
					return callback({NoStation: false, ScanMode: false});
				}
			});
		//}
	//});
} /* ResetPicklistForTote */

function DeAssociatePackedOrders(PickList, user, Station, callback)
{
	var OrdIndex = 0;
	var FailedToUnhook = false;

	while(OrdIndex < PickList.orders.length)
	{
		if(PickList.orders[OrdIndex].packComplete)
		{
			var ItemIndex = 0;

			while(ItemIndex < PickList.orders[OrdIndex].items.length)
			{
				if(PickList.orders[OrdIndex].items[ItemIndex].packed == false ||
				   (PickList.orders[OrdIndex].items[ItemIndex].pickQty != PickList.orders[OrdIndex].items[ItemIndex].qtyPacked))
				{
					FailedToUnhook = true;
					break;
				}
				ItemIndex++;
			}

			if(FailedToUnhook)
			{
				break;
			}
		}
		OrdIndex++;
	}

	if(FailedToUnhook)
	{
		if(PickList.orders.length > 1)
		{
		  return callback({error:'Failed to unhook order ' + PickList.orders[OrdIndex].orderID + ' from Master Picklist in Tote ' + PickList.toteID});
		}
	}
	else
	{
		if(PickList.orders.length > 1)
		{
                   CalculateTransporterWeight(PickList, function(TransporterDetails)
                   {
			UnhookPackedOrders(PickList, 0, user, Station, TransporterDetails, function(Details)
			{
				return callback(Details);
			});
                   });
		}
		else
		{
			return callback(PickList);
		}
	}
} /* DeAssociatePackedOrders */

function UnhookPackedOrder(PickList, Ordindex, user, Station, Lane, callback){
	var Msg = null;

	var Orders = PickList.orders[Ordindex];

	var now = new Date();
	var OrderDate = PickList.CreateDate;

	if(!OrderDate){
	  OrderDate =  moment(now).format('YYYY-MM-DD HH:mm');
	}

	var Subpicklist = new PickListModel ({
			toteID: 'PW_LOC_' + Lane,
			transporterID: null,
			destination: PickList.destination,
			packType: PickList.packType,
			toteInUse: false,
			toteInUseBy: PickList.toteInUseBy,
			orders: Orders,
			Status: 'STORED',
			PackUpdateRequired: false,
			ShipUpdateRequired: false,
			PackedDate: null,
			containerLocation: null,
			PickListType: null,
			FromTote: PickList.toteID,
			CreateDate: OrderDate
	})

	Subpicklist.save(function (error, doc){
		if (error){
			Msg = 'UnhookPackedOrder: Failed to unhook order ' + Subpicklist.orders[0].orderID + ' | ' + error ;
			WriteToFile(Msg);
			return callback(PickList);
		} else {
			Msg = 'UnhookPackedOrder: Successfully unhooked order ' + doc.orders[0].orderID + ' from tote ' + doc.FromTote + ' at station ' + Station + ' | user: ' + user;
			WriteToFile(Msg);

			PickList.orders.splice(Ordindex, 1);
			PickList.save(function(error1, SavedDoc){
				return callback(SavedDoc);
			});
		}
	});
} /* UnhookPackedOrder */

function DestroyMasterPickList(savedDoc, Station, callback){
	var Msg = null;
	var toteID = savedDoc.toteID;
  PickListModel.remove({'toteID': savedDoc.toteID}, function(err){
		Msg = 'WCS Master Picklist: deleted Master picklist in tote ' + toteID + ' at station ' + Station;
		WriteToFile(Msg)
		return callback();
	});
} /* DestroyMasterPickList */

function PackCompleteOrder(savedDoc, CurrentOrderToProcess, user, Station, Lane, callback){
  var now = new Date();
	savedDoc.FromTote = savedDoc.toteID;
	savedDoc.toteID = 'PW_LOC_' + Lane;
	savedDoc.ShipUpdateRequired = false;
	savedDoc.PackUpdateRequired = false;
	savedDoc.Status = 'STORED';
	savedDoc.toteInUse = false;
	savedDoc.PickListType = null;
	savedDoc.containerLocation = null;
	savedDoc.transporterID = null;
	savedDoc.save(function(err, savedDoc){
		ClearToteFromStation(Station, function(Details){
			return callback(Details);
		});
	});
} /* PackCompleteOrder */

function ClearToteFromStation(Station, callback){
	PackStationModel.findOne({stationNumber: Station}, function (error, doc){
		if(error || !doc){
			return callback({ScanMode:false});
		} else {
			var now = new Date();
			doc.updated_at = now;
			doc.currentTote = null;
			doc.currentTransporter = null;
			doc.save(function(error, doc){
				return callback({ScanMode:false});
			});
		}
	});
} /* ClearToteFromStation */

function SendDocsLabelPrint(PickList, CurrentOrderToProcess, OrderData, callback){
	var OrdItems = OrderData.OrdItems;
	var Cartons  = OrderData.Cartons;

	var CartonNo = PickList.orders[CurrentOrderToProcess].toteNumber;
	var NoOfCartons = PickList.orders[CurrentOrderToProcess].numberOfTotes;

	var First = true;
	var Last = true;

	var Station = parseInt(PickList.toteInUseBy);
	PackStationModel.findOne({stationNumber: Station}, function (error, doc){
		if(error || !doc){
			return callback({error:"Error while finding station for Tote " + PickList.toteID, PickList: PickList});
		} else {
			var PrintData = {
				orderId: PickList.orders[CurrentOrderToProcess].orderID,
				printerName: doc.desktopPrinterQName,
				labelprinterName: doc.labelPrinterQName,
				items: OrdItems,
				ErpItems: PickList.orders[CurrentOrderToProcess].items,
				cartonId: PickList.orders[CurrentOrderToProcess].cartonID,
				courierId: PickList.orders[CurrentOrderToProcess].courierID,
				station: doc.stationNumber,
				CartonNo: CartonNo,
				NoOfCartons: NoOfCartons,
				CartionWeight: PickList.orders[CurrentOrderToProcess].picklistWeight,
				FirstCarton: First,
				LastCarton: Last,
				orderType: PickList.orders[CurrentOrderToProcess].orderType,
				Cartons: Cartons,
				NoLabel: true
			}

			Pubclient.publish('PACKING_' + doc.stationNumber, JSON.stringify(PrintData), function(err){
				if(err){
				  return callback({error:"Error printing " + Expect + " for order " + PickList.orders[CurrentOrderToProcess].orderID, PickList:PickList});
				} else {
				  PickList.orders[CurrentOrderToProcess].printed = true
				  PickList.save(function(err, savedDoc){
					  return callback({PickList:savedDoc});
				  });
				}
			});
		}
	});

} /* SendDocsLabelPrint */

function DetermineNextStepInProcess(savedDoc, index, user, Station, callback){
	if(index < savedDoc.orders.length){
		if(savedDoc.orders[index].orderVerified){
			FindPutWallLocationUsingOrderID(savedDoc.orders[index].orderID, Station, function(Lane){
				if(Lane){
					if(savedDoc.orders.length > 1){
						UnhookPackedOrder(savedDoc, index, user, Station, Lane, function(Doc){
							DetermineNextStepInProcess(Doc, index, user, Station, callback);
						});
					} else {
						PackCompleteOrder(savedDoc, index, user, Station, Lane, function(Details){
							return callback(Details);
						});
					}
				}
			});
		} else {
			if(savedDoc.orders[index].packed){
				if(!savedDoc.orders[index].printed){
					ParamGetCartonDetails(savedDoc, index, function(CartonDetails){
						if(CartonDetails.error){
							return callback({Error: CartonDetails.error, savedDoc: savedDoc});
						} else {
							CalculateCartonWeight(savedDoc, index, CartonDetails.EmptyCartonWeight, function(CartonWeight){
								savedDoc.orders[index].picklistWeight = CartonWeight;
								savedDoc.orders[index].transporterWeight = null;
								savedDoc.orders[index].minWeightLimit = (CartonWeight - CartonDetails.MinTolerance);
								savedDoc.orders[index].maxWeightLimit = (CartonWeight + CartonDetails.MaxTolerance);
								savedDoc.orders[index].toteNumber = 1;
								var Msg = "Carton " + savedDoc.orders[index].cartonID + " weight " + CartonWeight + " | min weight " + (CartonWeight - CartonDetails.MinTolerance) + " | max weight " + (CartonWeight + CartonDetails.MaxTolerance);
								WriteToFile(Msg);
								savedDoc.save(function(err, savedDoc){
									if(!err && savedDoc){
										GetAllItemsAndCartonsForOrder(savedDoc, index, function(OrdDetails){
											if(OrdDetails.error){
												return callback({Error: OrdDetails.error, savedDoc: savedDoc});
											} else {
												SendDocsLabelPrint(savedDoc, index, OrdDetails, function(Data){
													return callback({Next: 'DOC', savedDoc: Data.PickList});
												});
											}
										});
									}
								});
							});
						}
					});
				} else {
					return callback({Next: 'DOC', savedDoc: savedDoc});
				}
			} else {
				index++;
				DetermineNextStepInProcess(savedDoc, index, user, Station, callback);
			}
		}
	} else {
		return callback({Next: 'BARCODE/SERIAL', savedDoc: savedDoc});
	}
} /* DetermineNextStepInProcess */

function CalculateCartonWeight(savedDoc, CurrentOrderToProcess, EmptyCartonWeight, callback){
	var x = 0;
	var Total = EmptyCartonWeight;
  while(x < savedDoc.orders[CurrentOrderToProcess].items.length){
		var lineWeight = (savedDoc.orders[CurrentOrderToProcess].items[x].eachWeight *
		                  savedDoc.orders[CurrentOrderToProcess].items[x].qtyPacked);
		Total += lineWeight;
		x++;
	}

	return callback(Total);
} /* CalculateCartonWeight */

function ParamGetCartonDetails(PickList, CurrentOrderToProcess, callback)
{
	var Msg = null;
	Params.find({}, function(err, Parameters){
		if(err || !Parameters || Parameters.length <= 0){
			Msg = "Failed to find parameters in the system ";
			WriteToFile(Msg);
			return callback({error:Msg});
		} else {
			var CartonPrefix = 1;

			if(Parameters[0].weightSettings && Parameters[0].weightSettings.length > 0){
				var paramIndex = 0;
				while(paramIndex < Parameters[0].weightSettings.length){
					if(Parameters[0].weightSettings[paramIndex].StockSize == CartonPrefix){
						break;
					}
					paramIndex++;
				}

				if(paramIndex < Parameters[0].weightSettings.length){
					var CartonDetails = {
						EmptyCartonWeight: Parameters[0].weightSettings[paramIndex].weight,
						MinTolerance: Parameters[0].weightSettings[paramIndex].LowerTolerance,
						MaxTolerance: Parameters[0].weightSettings[paramIndex].UpperTolerance
					}

					return callback(CartonDetails);
				} else {
					Msg = "Failed to find Carton weightSettings to match order carton field for carton " +
					       PickList.orders[CurrentOrderToProcess].cartonID + " of order " + PickList.orders[CurrentOrderToProcess].orderID;
					WriteToFile(Msg);
					return callback({error:Msg});
				}
			} else {
				Msg = "Failed to find weightSettings parameter in the system ";
				WriteToFile(Msg);
				return callback({error:Msg});
			}
		}
	});
} /* ParamGetCartonDetails */

function CheckIfWaitingForConfirmation(Station, callback){
	PwLocs.findOne({'packStation': Station, 'active': true}, function(err, PWLOC){
		if(!err && PWLOC){
			if(PWLOC.Data){
				var LocData = JSON.parse(PWLOC.Data);

				var ReturnData = {
					orderID: PWLOC.orderID,
					Location: PWLOC.putAddress
				}

				if(LocData.Serial){
						ReturnData.Serial = LocData.Serial;
				}

				if(LocData.Document){
					ReturnData.Document = LocData.Document;
				}

				return callback(ReturnData);

			} else {
				return callback(null);
			}
		} else {
			return callback(null);
		}
	});
} /* CheckIfWaitingForConfirmation */

function CheckIfPickListIsUnique(doc, index, callback){
	if(index < doc.orders.length){
		PickListModel.find({'orders.pickListID': doc.orders[index].pickListID}, function(err, PickLists){
			 if(err || !PickLists || PickLists.length <= 0){
				 return callback({unique: false, error: 'Failed to check picklist for uniqueness: ' + err, savedDoc: doc});
			 } else {
				 var Total = 0;
				 var x = 0;
				 while(x < PickLists.length){
					 var y = 0;
					 while(y < PickLists[x].orders.length){
						 if(PickLists[x].orders[y].pickListID == doc.orders[index].pickListID){
							 Total++;
						 }
						 y++;
					 }
					 x++;
				 }

				 if(Total > 1){
				   return callback({unique: false, error: 'Order ' + doc.orders[index].orderID + ' in tote is a duplicate', savedDoc: doc});
				 } else {
					 doc.orders[index].cartonSize = 1;
					 doc.save(function(err, savedDoc){
						 index++;
						 if(!err || savedDoc){
  						 CheckIfPickListIsUnique(savedDoc, index, callback);
						 } else {
							 CheckIfPickListIsUnique(doc, index, callback)
						 }
					 });
				 }
			 }
		});
	} else {
		return callback({unique: true, error: null, savedDoc: doc});
	}
} /* CheckIfPickListIsUnique */

function WriteToFile(Msg){
	if(Msg){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' ' + Msg);
	}
} /* WriteToFile */

function requireLogin (req, res, next){
  if (!req.session.user){
    return res.status(200).send({NoUser:true})
  } else {
    next();
  }
};

/*=================== Functions End =============================================*/

module.exports = router
