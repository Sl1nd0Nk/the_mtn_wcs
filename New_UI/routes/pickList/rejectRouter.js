var PickList = require('pickListModel')
var CourierReject = require('courierRejectModel');
var QCReject = require('rejectedQC')
var express = require('express')
var router = express.Router()
var restler = require('restler')
var qcRejectStation = require('qcRejectStationModel')
var param       = require('parameterModel')

router.get('/API/getPickList/:ToDo', function(req, res) {
    var cartonID = ''
    if(req.params.ToDo.substr(0, 1) == 'N'){cartonID=req.params.ToDo.substr(3)}else{cartonID=req.params.ToDo}
    console.log('******************************** CARTON ID ******************* | ' + cartonID)

    PickList.findOne({"orders.cartonID": cartonID}, function(err, data) {
        if (!err && data) {
            res.send({code: '00', message: 'success', data: data})
        } else {
            res.send({code: '01', message: 'fail', data: err})
        }
    })
})

router.get('/getPickListByTote/:ToDo', function(req, res) {
    PickList.findOne({"toteID": req.params.ToDo}, function(err, data) {
        if (!err && data) {
            res.send({code: '00', message: 'success', data: data})
        } else {
            res.send({code: '01', message: 'fail', data: err})
        }
    })
})

router.post('/resetReject/:ToDo', function(req, res){
    PickList.findOne({"toteID": req.params.ToDo}, function(err, data) {
        deleteExistingSession(req);
        if (!err && data) {
            var x = 0;
            let cartonID ;
            while(x < data.orders.length){
                data.orders[x].rejected = true;
                data.orders[x].rejectedReason = null;
                cartonID = data.orders[x].cartonID;
                x++
            }

            data.save(function(saveErr, saveDocs){
                if(!saveErr && saveDocs){
                    res.send({code: '00', message: 'success', data: data})
                     
                }else{
                    res.send({code: '03', message: 'failed.to.save', data: data})
                }
            });
        } else {
            res.send({code: '01', message: 'fail', data: err})
        }
    })
});

router.post('/resetQCReject/:ToDo', function(req, res){
    PickList.findOne({"orders.cartonID": req.params.ToDo}, function(err, data) {
        deleteExistingSession(req);
        if (!err && data) {
            var x = 0;
            let cartonID ;
            while(x < data.orders.length){
                data.orders[x].rejected = true;
                data.orders[x].rejectedReason = null;
                cartonID = data.orders[x].cartonID;
                x++
            }

            data.save(function(saveErr, saveDocs){
                if(!saveErr && saveDocs){
                    res.send({code: '00', message: 'success', data: data})
                     
                }else{
                    res.send({code: '03', message: 'failed.to.save', data: data})
                }
            });
        } else {
            res.send({code: '01', message: 'fail', data: err})
        }
    })
});



router.post('/insertNewQCSession/:ToDo', function(req,res){
    let qcData = JSON.parse(req.params.ToDo);
    qcData.active = true;
    let ipAddress = GetIpAddress(req);
    qcData.stationIP = ipAddress
    delete qcData._id;
    qcRejectStation.update({"stationIP" : ipAddress}, qcData, function(err, savedDoc){
        if(!err && savedDoc){
            console.log("Created new session" + JSON.stringify(savedDoc))
        }else{
            console.log("Err" + err)
        }
    })

});




router.get('/API/checkPickList/:ToDo', function (req, res) {
    if (req.params.ToDo.substr(0,1) == '7') {
        PickList.find({transporterID:req.params.ToDo}, function (error, docs) {
            if (!error && docs) {
                res.send({code: '00', message: 'success', data: docs})
            } else {
                res.send({code: '01', message: 'fail', data: error})
            }
        })
    } else {
        let ipAddress = GetIpAddress(req);
        
                PickList.findOne({"orders.cartonID": req.params.ToDo, "orders.rejected" : true, "orders.actualWeight1" :{$ne : null}}, function(err, data) {
                    if (!err && data) {       
                        res.send({code: '00', message: 'success', data: data})
                    } else {
                        console.log('WE DID NOT FIND IT: ' + data)
                        res.send({code: '01', message: 'fail', data: err})
                    }
                })
       
    }
})

router.post('/updateQCStation/:ToDo', function(req,res){
    let qcData = req.params.ToDo;
    let ipAddress = GetIpAddress(req);
    let serailArr = [];
    qcRejectStation.findOne({"stationIP" : ipAddress}, function(err, data){
        if(!err && data){
            let a = 0;
            while(a < data.orders.length){
                let b = 0;
                while(b < data.orders[a].items.length){
                    let c = 0;
                    if(data.orders[a].items[b].serialised == true) {
                    while(c < data.orders[a].items[b].qty){
                        //data.orders[a].items[b].RescannedSerials[c] = qcData.orders[a].items[b].RescannedSerials[c]
                        //console.log(data.orders[a].items[b].RescannedSerials[c]);

                                if(data.orders[a].items[b].serials[c] == qcData){
                                                       
                                         if(data.orders[a].items[b].RescannedSerials.length > 0){
                                            for(var q = 0; q < data.orders[a].items[b].RescannedSerials.length; q++){
                                                var serialnumber = data.orders[a].items[b].RescannedSerials[q];
                                                console.log(serialnumber.toString());
                                                serailArr.push(serialnumber.toString());
                                            }
                                              
                                            serailArr.push(qcData); 
                                         }else{
                                            serailArr.push(qcData); 
                                         }
                                         
                                         
                                          data.orders[a].items[b].RescannedSerials = serailArr;
                                             data.orders[a].items[b].checked = data.orders[a].items[b].RescannedSerials.length ;
                                                data.save(function(err, savedDoc){
                                                    if(!err && savedDoc){
                                                        //console.log(savedDoc.orders[a].items[b].RescannedSerials)
                                                      
                                                        res.send({result : savedDoc})
                                                    }else{
                                                        console.log("Err" +  err);  
                                                    }
                                                })
                                    }else{
                                           console.log("Serial Not founds");   
                                    }
                                     c++;
                       }
                                 
                    }else{

                        if(data.orders[a].items[b].barcode == qcData ){
                            if(data.orders[a].items[b].checked < data.orders[a].items[b].qty){

                            if(data.orders[a].items[b].RescannedSerials.length > 0){
                                for(var q = 0; q < data.orders[a].items[b].RescannedSerials.length; q++){
                                        var serialnumber = data.orders[a].items[b].RescannedSerials[q];
                                        console.log(serialnumber.toString());
                                        serailArr.push(serialnumber.toString());
                                    }
                                              
                            serailArr.push(qcData); 
                            }else{
                            serailArr.push(qcData); 
                            }
                                         
                            data.orders[a].items[b].RescannedSerials = serailArr
                              data.orders[a].items[b].checked = data.orders[a].items[b].RescannedSerials.length;
                                    data.save(function(err, savedDoc){
                                        if(!err && savedDoc){
                                            //console.log(savedDoc.orders[a].items[b].RescannedSerials)
                                          
                                            res.send({result : savedDoc})
                                        }else{
                                            console.log("Err" +  err);  
                                        }
                                    })  
                            }
                           
                       }
                     }
                    
                        
                    b++;
                }
                a++;
            }

            

         
        }
    })

  
})

router.get('/getQCStation/', function(req,res){
    
    let ipAddress = GetIpAddress(req);
    qcRejectStation.findOne({"stationIP" : ipAddress}, function(err, data){

        if(!err && data){
                if(data.active == true){
                  res.send({code: '00', message: 'success', data: data})  
              }else{
                 res.send({code: '03', message: 'Reject station Not Updated', data: data})
              }
             
            } else {
             res.send({code: '04', message: 'Reject station Not Updated', data: data})
            }
        });
})


router.post('/updateRejected/:ToDo', function(req, res) {
    var updateInfo = JSON.parse(req.params.ToDo)
    console.log('UPDATE INFO: Carton ID: ' + updateInfo.cartonID + ' | Weight: ' + updateInfo.weight)
    PickList.findOne({"orders.cartonID": updateInfo.cartonID}, function(err, data) {
        if (!err && data) {
            var x = 0
            var cartonUpdated = false
            while (x < data.orders.length) {
                console.log('carton ID: ' + data.orders[x].cartonID + ' | ' + updateInfo.cartonID)
                if (data.orders[x].cartonID == updateInfo.cartonID) {
                    data.orders[x].rejected = false
		    data.orders[x].picklistWeight = data.orders[x].actualWeight1; 
                    console.log('MIN: ' + data.orders[x].minWeightLimit + ' | MAX: ' + data.orders[x].maxWeightLimit + ' | Actual: ' + (data.orders[x].actualWeight1) - 115)
			let carton = updateInfo.cartonID;
                  	param.find({}, function(err, paramArr){
				let q = 0;
				while(q < paramArr[0].weightSettings.length){
				if(paramArr[0].weightSettings[q].StockSize == carton.substr(0,1)){

					data.orders[x].minWeightLimit = (data.orders[x].actualWeight1) - paramArr[0].weightSettings[q].LowerTolerance
					data.orders[x].maxWeightLimit = (data.orders[x].actualWeight1) + paramArr[0].weightSettings[q].UpperTolerance
			
				}
			    q++;
		     	}	
			})
                    cartonUpdated = true
                    
                   // if(updateInfo.weight) {
                     //  data.orders[x].picklistWeight = updateInfo.weight
                   // }
                    if(updateInfo.cartonComplete){
                        deleteExistingSession(req);
                    }
                    // UPDATED CODE
                    data.save(function(savedErr, savedData) {
                        if(!savedErr && savedData) {
                            res.send({code: '00', message: 'success', data: data})
                        } else {
                            res.send({code: '03', message: 'Reject Not Updated', data: data})
                        }
                    })
                    break
                }
                x++
            }
        } else {
            res.send({code: '01', message: 'fail', data: err})
        }
    })
})

router.post('/updateTransporter/:ToDo', function(req, res) {
    var transporterID = req.params.ToDo
    var totalWeight = 0
    PickList.find({transporterID:transporterID}, function(error, docs) {
        if(!error && docs) {
            var x = 0
            while (x < docs.length){
                totalWeight = totalWeight + docs[x].orders[0].picklistWeight
                x++
            }
            if(totalWeight > 0){
                var y = 0
                while(y < docs.length){
                    var c = 0
                    while(c < docs[y].orders.length) {
                        docs[y].orders[c].actualWeight1 = totalWeight + 920
                        docs[y].orders[c].transporterWeight = totalWeight + 920
                        docs[y].orders[c].minWeightLimit = (totalWeight + 920) - 115
                        docs[y].orders[c].maxWeightLimit = (totalWeight + 920) + 115
                        c++
                    }

                    docs[y].save(function(saveErr, saveDocs){
                        if(!saveErr && saveDocs) {
                            console.log(saveDocs.transporterID + '** saved ** ' + saveDocs.orders[0].actualWeight1)
                        } else {
                            console.log('** not saved **')
                        }
                    })
                    y++
                }

                res.send({code:'00', message:'success', data:docs})
            }
        } else {
            res.send({code:'01', message:'fail'})
        }
    })
})

router.get('/checkBeforeSetCarton/:ToDo', function (req, res) {
    var cartonID = ''
    if(req.params.ToDo.substr(0, 1) == 'N'){cartonID=req.params.ToDo.substr(3)}else{cartonID=req.params.ToDo}
    console.log('******************************** CARTON ID ******************* | ' + cartonID)
    PickList.findOne({"orders.cartonID": cartonID}, function(err, data) {
        if (!err && data) {
            res.send({code: '00', message: 'found data'})
        } else {
            res.send({code: '01', message: 'fail', data: err})
        }
    })
})

router.get('/updateQcReject/:ToDo', function(req, res){
    QCReject.findOne({processed:req.params.ToDo}, function(error, data){
        if(!error && data){
            data.processed = true;

            data.save(function(saveErr, saveData){
                if(!saveErr && saveData){
                    res.send({code:'00', message:'success'})
                } else {
                    res.send({code:'03', message:'fail'})
                }
            })
        } else {
            res.send({code:'01', message:'fail'})
        }
    })
})


// ==========================================COURIER REJECT!!!
router.post('/courierRejected/:ToDo', function (req, res) {
    CourierReject.findOne({"CartonId": req.params.ToDo}, function(err, data) {
        if (!err && data) {
            res.send({code: '00', message: 'success', data:data})
        } else {
            res.send({code: '01', message: 'fail', data: err})
        }
    })
})

router.post('/courierRejectedReset/:ToDo', function (req, res) {
    CourierReject.findOne({"CartonId": req.params.ToDo}, function(err, data) {
        if (!err && data) {
            data.processed = true;

            data.save(function(saveErr, saveDocs){
                if(!saveErr && saveDocs){
                    res.send({code: '00', message: 'success', data:saveDocs})
                }else{
                    res.send({code: '03', message: 'fail'})
                }
            });
        } else {
            res.send({code: '01', message: 'fail', data: err})
        }
    })
})

function GetIpAddress(req){
        var ClientIP = req.headers['x-forwarded-for'] ||
                                req.connection.remoteAddress ||
                                req.socket.remoteAddress ||
                                req.connection.socket.remoteAddress;

        if (ClientIP.substr(0, 7) == "::ffff:"){
            ClientIP = ClientIP.substr(7)
        }
        return ClientIP;
    } /* GetIpAddress */


    function deleteExistingSession( req){
        let ipAddress = GetIpAddress(req);
        let qcData = {

            active :  false,
            stationIP : ipAddress
        }
        qcRejectStation.remove({"stationIP" : ipAddress}, function(err, resp){
            if(!err && resp){
                console.log("deleted the session")
                qcRejectStation.create(qcData, function(err, resp){

                 
                })
            }else{
                console.log("err" + err);
            }
        })
    }

module.exports = router
