var apexLib = require('apex_lib/moduleIndex.js')
var PickListModel = require('pickListModel')
var QcRejectView = require('WeightScaleRejectsModel')
var CourierRejects = require('CourierRejectsModel');
var PackStationModel = require('packStationModel')
var QcStationModel = require('qcRejectStationModel')
var CanReq = require('CancelRequestModel');
var WeightDims = require('WeightDimsUpdateModel');
var Params = require('parameterModel')
var PwLocs = require('putWallModel')
var sm = require('serialModel');
var express = require('express')
var moment = require('moment')
var router = express.Router()
var redis = require('redis');
var Pubclient = redis.createClient();
var serialUri = require('ui').serialUrl
var AmarmMod = require('../../module/alarmAndEvents/AlarmAndEventModule')
var AutobagModule = require('../../module/autoBag/autobagModule')
var net = require('net');
var rest = require('restler');
var WeightDimsUrl = require('ui').WeightDimsUrl;
var WeightDimsUpdateUrl = require('ui').WeightDimsUpdateUrl;

router.get('/API/WADUpdateProduct/:id', requireLogin, function (req, res){
	let data = JSON.parse(req.params.id);
	let Product = data.Product;
	let Weight = data.Weight;
	let Height = data.Height;
	let Length = data.Length;
	let Width = data.Width;
	let FromWeight = data.FromWeight;
	let FromHeight = data.FromHeight;
	let FromLength = data.FromLength;
	let FromWidth = data.FromWidth;
	let Desc = data.Desc;
	let Uom = data.Uom;
	let Client = '45146';
	let user = req.session.username;
	let roleID = req.session.roleID;
	let IpAddr = req.session.ip;
	let Msg = null;

	let body = {
		Sku: Product,
		Uom: Uom,
		Consignee: Client,
		Weight: Weight,
		Height: Height,
		Length: Length,
		Width: Width,
		User: user
	}

	rest.postJson(WeightDimsUpdateUrl, body).on('complete', function(result){
		if(result instanceof Error){
			WriteToFile('Weight And Dimension Update Err: ' + result);
			CreateWeightDimLogRecord(Product, Desc, Uom, FromWeight, Weight, FromLength, Length, FromWidth, Width, FromHeight, Height, user, result, function(){
				return res.status(405).send({user: user, roleID: roleID, Err: 'Failed to update weight and dimensions for product: ' + Product});
			});
			return;
		}

		let Response = JSON.parse(result);

		if(Response.Err){
			WriteToFile('Weight And Dimension Update Err: ' + Response.Message);
			CreateWeightDimLogRecord(Product, Desc, Uom, FromWeight, Weight, FromLength, Length, FromWidth, Width, FromHeight, Height, user, Response.Message, function(){
				return res.status(200).send({user: user, roleID: roleID, Err: Response.Err});
			});
			return;
		}

		CreateWeightDimLogRecord(Product, Desc, Uom, FromWeight, Weight, FromLength, Length, FromWidth, Width, FromHeight, Height, user, Response.Message, function(){
			UpdateUndispatchedSerialsToWeight(Product, Uom, Weight, user, function(){
				return res.status(200).send({user: user, roleID: roleID, Msg: 'The details of ' + Product + ' for UOM ' + Uom + ' were successfully updated'});
			});
		});
	});
});

router.get('/API/WADGetProduct/:id', requireLogin, function (req, res){
	let data = JSON.parse(req.params.id);
	let Product = data.Product;
	let Client = '45146';
	let user = req.session.username;
	let roleID = req.session.roleID;
	let IpAddr = req.session.ip;
	let Msg = null;

	let body = {
		scannedItem: Product,
		consignee: Client
	}

	rest.postJson(WeightDimsUrl, body).on('complete', function(result){
		if(result instanceof Error){
			WriteToFile('Weight And Dimension Request Err: ' + result);
			return res.status(405).send({user: user, roleID: roleID, Err: 'Failed to find weight and dimensions for product: ' + Product});
		}

		let Response = JSON.parse(result);

		if(Response.Err){
			WriteToFile('Weight And Dimension Request Err: ' + Response.Err);
			return res.status(200).send({user: user, roleID: roleID, Err: Response.Err});
		}

		return res.status(200).send({user: user, roleID: roleID, ProdData: Response});
	});
});

router.get('/API/QCPickList/:id', requireLogin, function (req, res){
	let data = JSON.parse(req.params.id);
	let Container = data.ContainerID;
	let Scanned = data.Scanned;
	let user = req.session.username;
	let IpAddr = req.session.ip;
	let Msg = null;

	if(Container[0] == '7'){
		PickListModel.find({transporterID: Container}, function (error, docArr){
			if(error || !docArr || docArr.length <= 0){
				return res.status(405).send({Err: 'Failed to find scanned transporter'});
			}

			let TrDocument = docArr[0];

			if(Scanned){
				UpdateAttemptsForEach(docArr, 0, function(){});
			}

			return res.status(200).send({Document: TrDocument});
		});

		return;
	}

	PickListModel.findOne({toteID: Container}, function (error, doc){
		if(error || !doc){
			return res.status(405).send({Err: 'Failed to find scanned parcel'});
		}

		if(doc.Status == 'CANCELED'){
			return res.status(200).send({Document: doc});
		}

		if(doc.orders[0].QcVerificationAttempts && doc.orders[0].QcVerificationAttempts < 3){
			doc.orders[0].QcVerificationAttempts++;
		}

		if(!doc.orders[0].QcVerificationAttempts && Scanned){
			doc.orders[0].QcVerificationAttempts = 1;
		}

		CanReq.findOne({'OrderId': doc.orders[0].orderID}, function(err, Req){
			if(Req){
				doc.orders[0].WmsOrderStatus = 'CANCELED';
				doc.orders[0].WmsPickListStatus = 'CANCELED';
				doc.orders[0].CancelDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
				doc.orders[0].CancelInstruction = 'Fetch Parcel From QC Area';
				doc.Status = 'CANCELED';
			}

			doc.save(function (err, saveDoc){
				return res.status(200).send({Document: saveDoc});
			});
		});
	});
});

function CreateWeightDimLogRecord(Product, Desc, Uom, FromWeight, Weight, FromLength, Length, FromWidth, Width, FromHeight, Height, user, Result, callback){
	let NewRec = new WeightDims({
		UserID: user,
		Sku: Product,
		SkuDesc: Desc,
		Uom: Uom,
		FromWeight: FromWeight,
		ToWeight: Weight,
		FromLength: FromLength,
		ToLength: Length,
		FromWidth: FromWidth,
		ToWidth: Width,
		FromHeight: FromHeight,
		ToHeight: Height,
		Date: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
		Result: Result
	});

	NewRec.save(function(err, savedDoc){
		return callback();
	});
} /* CreateWeightDimLogRecord */

function UpdateUndispatchedSerialsToWeight(Product, Uom, Weight, user, callback){
	sm.update({"dispatched" : false, "SKU" : Product, "UOM" : Uom}, {"serialWeight": Weight, "userID" : user}, {multi: true}, function(err, res){
		if(err){
			WriteToFile('Failed to multi update serial weight. Err: ' + err);
		}

		return callback();
	});
} /* UpdateUndispatchedSerialsToWeight */

function UpdateAttemptsForEach(docArr, index, callback){
	if(index >= docArr.length){
		return callback();
	}

	let doc = docArr[index];

	if(doc.Status == 'CANCELED'){
		index++;
		return UpdateAttemptsForEach(docArr, index, callback);
	}

	if(doc.orders[0].QcVerificationAttempts && doc.orders[0].QcVerificationAttempts < 3){
		doc.orders[0].QcVerificationAttempts++;
	}

	if(!doc.orders[0].QcVerificationAttempts){
		doc.orders[0].QcVerificationAttempts = 1;
	}

	CanReq.findOne({'OrderId': doc.orders[0].orderID}, function(err, Req){
		if(Req){
			doc.orders[0].WmsOrderStatus = 'CANCELED';
			doc.orders[0].WmsPickListStatus = 'CANCELED';
			doc.orders[0].CancelDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			doc.orders[0].CancelInstruction = 'Fetch Parcel From QC Area';
			doc.Status = 'CANCELED';
		}

		doc.save(function (err, saveDoc){
			index++;
			UpdateAttemptsForEach(docArr, index, callback);
		});
	});
} /* UpdateAttemptsForEach */

function CreateCartonItems(Order){
	let x = 0;
	let Items = [];
	while(x < Order.items.length){
		let item = {
		   ItemCode: Order.items[x].itemCode,
		   ItemDescription: Order.items[x].itemDescription,
		   QtyOriginal: Order.items[x].serialised == true? Order.items[x].serials.length : Order.items[x].qty,
		   QtyPacked: Order.items[x].serialised == true? Order.items[x].serials.length : Order.items[x].qtyPacked,
		   QtyVerified: 0,
		   Serials: Order.items[x].serials,
		   VerifiedSerials: [],
		   Sku: Order.items[x].sku,
		   Barcode: Order.items[x].barcode,
		   Serialised: Order.items[x].serialised,
		   PackedBy: Order.items[x].packedBy
		};

		Items.push(item);
		x++;
	}

	return Items;
} /* CreateCartonItems */

function CheckCartonCheckComplete(Items){
	let x = 0;
	while(x < Items.length){
		if(Items[x].QtyPacked != Items[x].QtyVerified){
			return false;
		}
		x++;
	}

	return true;
} /* CheckCartonCheckComplete */

function GetCartonsTolerancesFromParams(callback){
	let CartonTolerances = [];

	Params.find({}, function(err, Parameters){
		if(err || !Parameters || Parameters.length <= 0){
			return callback(CartonTolerances);
		}

		if(!Parameters[0].weightSettings || Parameters[0].weightSettings.length <= 0){
			return callback(CartonTolerances);
		}

		let len = Parameters[0].weightSettings.length;
		let x = 0;
		while(x < len){
			let CartonData = {
				StockSize: Parameters[0].weightSettings[x].StockSize,
				UseTransporter: Parameters[0].weightSettings[x].useTransporter,
				EmptyCartonWeight: Parameters[0].weightSettings[x].weight,
				MinTolerance: Parameters[0].weightSettings[x].LowerTolerance,
				MaxTolerance: Parameters[0].weightSettings[x].UpperTolerance
			};

			CartonTolerances.push(CartonData);
			x++;
		}

		return callback(CartonTolerances);
	});
} /* GetCartonsTolerancesFromParams */

function GetTolerancesFromParams(Perfix, callback){
	Params.find({}, function(err, Parameters){
		if(err || !Parameters || Parameters.length <= 0){
			return callback(null);
		}

		if(!Parameters[0].weightSettings || Parameters[0].weightSettings.length <= 0){
			return callback(null);
		}

		let len = Parameters[0].weightSettings.length;
		let x = 0;
		while(x < len){
			if(Parameters[0].weightSettings[x].StockSize == Perfix){
				return callback({
					StockSize: Parameters[0].weightSettings[x].StockSize,
					EmptyTrWeight: Parameters[0].weightSettings[x].weight,
					MinTolerance: Parameters[0].weightSettings[x].LowerTolerance,
					MaxTolerance: Parameters[0].weightSettings[x].UpperTolerance
				});
			}
			x++;
		}

		return callback(null);
	});
} /* GetTolerancesFromParams */

function AllChecked(Cartons){
	let x = 0;
	while(x < Cartons.length){
		if(!Cartons[x].WeightCheckComplete){
			return false;
		}
		x++;
	}

	return true;
} /* AllChecked */

function QCCalculateTransporterWeight(Transporter, Cartons, callback){
	let StnTr = Transporter;

	let CartonPrefix = StnTr[0];
	if(StnTr[1] != '0'){
		CartonPrefix = StnTr[0] + StnTr[1];
	}

	let TrStockSize = parseInt(CartonPrefix);

	GetTolerancesFromParams(TrStockSize, function(Resp){
		if(!Resp){
			return callback({Err: 'Failed To Get Transporter Settings from Parameters'});
		}

		let TrWeight = Resp.EmptyTrWeight;
		let x = 0;
		while(x < Cartons.length){
			if(!Cartons[x].CartonProblem){
				TrWeight += Cartons[x].MeasuredWeight;
			}
			x++;
		}

		let TrMin = TrWeight - Resp.MinTolerance;
		let TrMax = TrWeight + Resp.MaxTolerance;

		return callback({
			TrWeight: TrWeight,
			TrMin: TrMin,
			TrMax: TrMax
		});
	});
} /* QCCalculateTransporterWeight */

function CompleteCartonInTrToPickList(TrId, Cartons, index, TrWeightData, user, callback){
	if(index >= Cartons.length){
		return callback();
	}

	PickListModel.findOne({toteID: Cartons[index].CartonID}, function (error, doc){
		if(error || !doc){
			index++;
			return CompleteCartonInTrToPickList(TrId, Cartons, index, TrWeightData, user, callback);
		}

		doc.orders[0].picklistWeight = Cartons[index].MeasuredWeight;
		doc.orders[0].actualWeight1 = TrWeightData.TrWeight;
		doc.orders[0].transporterID = TrId;
		doc.orders[0].transporterWeight = TrWeightData.TrWeight;
		doc.orders[0].minWeightLimit = TrWeightData.TrMin;
		doc.orders[0].maxWeightLimit = TrWeightData.TrMax;
		doc.orders[0].rejected = false;
		doc.orders[0].rejectedReason = null;
		doc.orders[0].actualWeight2 = null;
		doc.orders[0].QcChecked = Cartons[index].CartonChecked;
		doc.orders[0].QcCheckOK = (Cartons[index].CartonProblem)? false: true;
		doc.orders[0].LastQcCheckTime = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
		doc.orders[0].QcCheckResult = Cartons[index].CartonReason;

		UpdateQcRejectView(Cartons[index], user);

		doc.save(function(err, savedDoc){
			index++;
			return CompleteCartonInTrToPickList(TrId, Cartons, index, TrWeightData, user, callback);
		});
	});
} /* CompleteCartonInTrToPickList */

function UpdateQcRejectView(Carton, user){
	QcRejectView.findOne({'CartonID': Carton.CartonID}, function(err, Record){
		if(err || !Record){
			return;
		}

		let MyDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');

		Record.qcChecked = Carton.CartonChecked;
		Record.qcCheckedDate = MyDate;
		Record.qcCheckedBy = user;

		let qcCheckResult = Carton.CartonReason;
		Record.qcCheckResult.push({Date: MyDate, Result: qcCheckResult});

		Record.save(function(err, savedDoc){
			return;
		});
	});
} /* UpdateQcRejectView */

function CompleteCartonToPickList(Carton, user, callback){
	GetTolerancesFromParams(parseInt(Carton.CartonID[0]), function(Resp){
		if(!Resp){
			return callback({Err: 'Failed To Get Parcel Settings from Parameters'});
		}

		PickListModel.findOne({toteID: Carton.CartonID}, function (error, doc){
			if(error || !doc){
				return callback({Err: 'Failed To Find Parcel in picklists'});
			}

			if(!Carton.CartonProblem)
				doc.orders[0].picklistWeight = (doc.orders[0].actualWeight1 != null)? doc.orders[0].actualWeight1: doc.orders[0].picklistWeight;

			if(!doc.orders[0].actualWeight1){
				doc.orders[0].actualWeight1 = doc.orders[0].picklistWeight;
			}

			if(!Carton.CartonProblem){
 				doc.orders[0].minWeightLimit = doc.orders[0].picklistWeight - Resp.MinTolerance;
				doc.orders[0].maxWeightLimit = doc.orders[0].picklistWeight + Resp.MaxTolerance;
			}

			doc.orders[0].transporterID = null;
			doc.orders[0].transporterWeight = null;
			doc.orders[0].rejected = false;
			doc.orders[0].rejectedReason = null;
			doc.orders[0].actualWeight2 = null;
			doc.orders[0].QcChecked = Carton.CartonChecked;
			doc.orders[0].QcCheckOK = (Carton.CartonProblem)? false: true;
			doc.orders[0].LastQcCheckTime = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			doc.orders[0].QcCheckResult = Carton.CartonReason;

			UpdateQcRejectView(Carton, user);

			doc.save(function(err, savedDoc){
				return callback({Err: null});
			});
		});
	});
} /* CompleteCartonToPickList */

function AddAsNoReadToQcRejectViewIfNotExist(doc, callback){
	QcRejectView.findOne({'CartonID': doc.toteID}, function(err, Record){
		if(err || Record){
			return callback({Record:Record});
		}

		let newRejectView = new QcRejectView({
			TransporterID: (!doc.transporterID)? '': doc.transporterID,
			CartonID: doc.toteID,
			RejectReason: 'No Read At Inline Scale',
			ExpectedMinWeight: doc.orders[0].minWeightLimit,
			ExpectedMaxWeight: doc.orders[0].maxWeightLimit,
			CartonCalculatedWeight: doc.orders[0].picklistWeight,
			ActualScaleWeight: null,
			QcScaleUsed: null,
			DateRejected: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
			processed: false,
			qcChecked: false,
			qcCheckedBy: null,
			qcCheckResult: []
		});

		newRejectView.save(function(err, savedDoc){
			return callback({Record:savedDoc});
		});
	});
} /* AddAsNoReadToQcRejectViewIfNotExist */

function UpdateStationWithScaleValue(weight, stationNumber){
	QcStationModel.findOne({stationNumber: stationNumber}, function (error, QcStation){
		if (error || !QcStation){
			return;
		}

		QcStation.TempWeightReads.push(weight);
		QcStation.save(function (error, savedData){
			return;
		});
	});
} /* UpdateStationWithScaleValue */

function ExtractAndEvaluateReceivedWeight(data, stationNumber){
	let RawData = data.toString();
	let ExtractedRange = RawData.substr(50, 30);

	console.log('ExtractedRange: ' + ExtractedRange);

	ExtractedRange = ExtractedRange.trim();
	let g = ExtractedRange.indexOf('g');

	if(g > 0){
		let Strweight = ExtractedRange.substring(1, g);
		Strweight = Strweight.trim();
		console.log('Strweight: ' + Strweight);
		if(Strweight){
			let weight = parseFloat(Strweight);
			console.log('weight: ' + weight);
			UpdateStationWithScaleValue(weight, stationNumber);
		}
	}
} /* ExtractAndEvaluateReceivedWeight */

function GetWeightFromScale(ScaleIP, ScalePort, stationNumber){
	let Count = 0;
	let client = new net.Socket();
	client.setTimeout(1000);
	client.setEncoding('utf8');
	client.connect(ScalePort, ScaleIP, function (err) {
		if(err){
			console.log('Scale for QC Station ' + stationNumber + ' Failed to connect. Err: ' + err);
		}else{
			console.log('Scale for QC Station ' + stationNumber + ' connected');
		}
	});

	client.on('data', function (data){
		console.log('Scale for QC Station ' + stationNumber + ' Received ' + data.toString('utf8').substr(50, 30));
		if(data.toString('utf8').substr(50, 30) !== ''){
			ExtractAndEvaluateReceivedWeight(data, stationNumber);
			client.destroy();
		}
	});

	client.on('close', function(){
		console.log('Scale Connection for QC Station ' + stationNumber + ' Closed');
	});

    client.on('timeout', function () {
        console.log('Scale Connection for QC Station ' + stationNumber + ' timed out');
    });

	client.on('error', function(err){
		console.log('Scale Connection for QC Station ' + stationNumber + ' Error: ' + err);
	});
} /* GetWeightFromScale */

function CreateSingleContainerCourierRejectRec(user, MyDate, doc, callback){
	let RejectReason = [];
	RejectReason.push({
		Date: MyDate,
		Reason: 'Decision Point No Read',
		Checked: true,
		CheckedBy: user
	});

	let NewRec = new CourierRejects({
		TransporterID           : doc.transporterID,
		CartonID                : doc.toteID,
		RejectReason            : RejectReason
	});

	NewRec.save(function(err, savedDoc){
		return callback(err, savedDoc);
	});
} /* CreateSingleContainerCourierRejectRec */

function CreateCourierRejectViewForEachInTr(docArr, index, user, MyDate, callback){
	let doc = docArr[index];
	CreateSingleContainerCourierRejectRec(user, MyDate, doc, function(err, savedDoc){
		if((index+1) == docArr.length){
			return callback(err, savedDoc);
		}

		index++;
		return CreateCourierRejectViewForEachInTr(docArr, index, user, MyDate, callback);
	});
} /* CreateCourierRejectViewForEachInTr */

router.get('/API/AddQCTrPickListData/:id', requireLogin, function (req, res){
	let Container = req.params.id;
	let user = req.session.username;
	let IpAddr = req.session.ip;
	let Msg = null;

	if(Container[0] != '7'){
		return res.status(405).send({Err: 'Not a Transporter'});
	}

	PickListModel.find({transporterID: Container}, function (error, docArr){
		if(error || !docArr || docArr.length <= 0){
			return res.status(405).send({Err: 'Failed to find scanned transporter'});
		}

		QcStationModel.findOne({stationIP: IpAddr}, function (error, QcStation){
			if (error || !QcStation){
				return res.status(200).send({NoStation: true, ScanMode: false});
			}

			if(QcStation.ContainerID == Container && QcStation.Cartons && QcStation.Cartons.length > 0){
				let ReturnData = {
					StationData: QcStation
				}
				return res.status(200).send(ReturnData);
			}

			GetCartonsTolerancesFromParams(function(CartonTolerances){
				QcStation.active = true;
				QcStation.updated_at = new Date();
				QcStation.ContainerID = Container;

				let z = 0;
				let Cartons = [];
				while(z < docArr.length){
					let doc = docArr[z];
					AddAsNoReadToQcRejectViewIfNotExist(doc, function(Resp){});
					let x = 0;
					while(x < doc.orders.length){
						let CartonItems = CreateCartonItems(doc.orders[x]);
						let CartonPrefix = doc.orders[x].cartonID[0];
						let ICartonPrefix = parseInt(CartonPrefix);
						let y = 0;
						while(y < CartonTolerances.length){
							if(ICartonPrefix == CartonTolerances[y].StockSize){
								break;
							}
							y++;
						}

						let CartonData = {
							CartonID: doc.orders[x].cartonID,
							OrderID: doc.orders[x].orderID,
							CartonWeight: doc.orders[x].picklistWeight,
							MinWeight: (y < CartonTolerances.length)? (doc.orders[x].picklistWeight - CartonTolerances[y].MinTolerance): doc.orders[x].minWeightLimit,
							MaxWeight: (y < CartonTolerances.length)? (doc.orders[x].picklistWeight + CartonTolerances[y].MaxTolerance): doc.orders[x].maxWeightLimit,
							PicklistID: doc.orders[x].pickListID,
							WeightFailReason: doc.orders[x].rejectedReason,
							CartonChecked: false,
							CartonProblem: false,
							CartonCancelled: doc.orders[x].WmsOrderStatus == 'CANCELED'? true: false,
							CartonReason: doc.orders[x].WmsOrderStatus == 'CANCELED'? 'Order Is Cancelled' : '',
							transporterID: doc.transporterID,
							Items: CartonItems
						};

						Cartons.push(CartonData);
						x++;
					}
					z++;
				}

				QcStation.Cartons = Cartons;
				QcStation.LastScannedResult = null;
				QcStation.save(function (error, savedData){
					let ReturnData = {
						StationData: savedData
					}
					return res.status(200).send(ReturnData);
				});
			});
		});
	});
});

router.get('/API/AddQCPickListData/:id', requireLogin, function (req, res){
	let Container = req.params.id;
	let user = req.session.username;
	let IpAddr = req.session.ip;
	let Msg = null;

	if(Container[0] == '7'){
		return res.status(405).send({Err: 'Not a Parcel'});
	}

	PickListModel.findOne({toteID: Container}, function (error, doc){
		if(error || !doc){
			return res.status(405).send({Err: 'Failed to find scanned parcel'});
		}

		QcStationModel.findOne({stationIP: IpAddr}, function (error, QcStation){
			if (error || !QcStation){
				return res.status(200).send({NoStation: true, ScanMode: false});
			}

			if(QcStation.ContainerID == Container && QcStation.Cartons && QcStation.Cartons.length > 0 && QcStation.Cartons[0].CartonID == doc.orders[0].cartonID){
				let ReturnData = {
					StationData: QcStation
				}
				return res.status(200).send(ReturnData);
			}

			AddAsNoReadToQcRejectViewIfNotExist(doc, function(Resp){
				QcStation.active = true;
				QcStation.updated_at = new Date();
				QcStation.ContainerID = Container;

				let x = 0;
				let Cartons = [];
				while(x < doc.orders.length){
					let Aweight = doc.orders[x].actualWeight1;
					let RejectMsg = doc.orders[x].rejectedReason;
					if(Resp && Resp.Record){
						if(Resp.Record.RejectReason){
							RejectMsg = Resp.Record.RejectReason;
						}

						if(Resp.Record.ActualScaleWeight && !Aweight){
							Aweight = Resp.Record.ActualScaleWeight;
						}
					}

					let CartonItems = CreateCartonItems(doc.orders[x]);
					let CartonData = {
						CartonID: doc.orders[x].cartonID,
						OrderID: doc.orders[x].orderID,
						CartonWeight: doc.orders[x].picklistWeight,
						MeasuredWeight: Aweight,
						MinWeight: doc.orders[x].minWeightLimit,
						MaxWeight: doc.orders[x].maxWeightLimit,
						PicklistID: doc.orders[x].pickListID,
						WeightFailReason: RejectMsg,
						CartonChecked: false,
						CartonProblem: false,
						CartonCancelled: doc.orders[x].WmsOrderStatus == 'CANCELED'? true: false,
						CartonReason: doc.orders[x].WmsOrderStatus == 'CANCELED'? 'Order Is Cancelled' : '',
						transporterID: doc.transporterID,
						Items: CartonItems
					};

					Cartons.push(CartonData);
					x++;
				}

				QcStation.Cartons = Cartons;
				QcStation.Inprogress = Container;
				QcStation.LastScannedResult = null;
				QcStation.save(function (error, savedData){
					let ReturnData = {
						StationData: savedData
					}
					return res.status(200).send(ReturnData);
				});
			});
		});
	});
});

router.get('/API/QCFindScannedItem/:id', requireLogin, function (req, res){
	let data = JSON.parse(req.params.id);
	let Item = data.ScannedItem;
	let CartonID = data.CartonID;
	let IpAddr = req.session.ip;
	let user = req.session.username;
	let Msg = null;

	QcStationModel.findOne({stationIP: IpAddr}, function (error, QcStation){
		if(error || !QcStation){
			return res.status(200).send({Err: 'Failed to find scanned parcel'});
		}

		let x = 0;
		while(x < QcStation.Cartons.length){
			if(QcStation.Cartons[x].CartonID == CartonID){
				break;
			}
			x++;
		}

		if(x >= QcStation.Cartons.length){
			return res.status(200).send({Err: 'Failed to find scanned parcel'});
		}

		let y = 0;
		let NonSerialError = null;
		while(y < QcStation.Cartons[x].Items.length){
			if((QcStation.Cartons[x].Items[y].Barcode && QcStation.Cartons[x].Items[y].Barcode == Item) || (QcStation.Cartons[x].Items[y].Sku && QcStation.Cartons[x].Items[y].Sku == Item)){
				if(QcStation.Cartons[x].Items[y].Serialised){
					return res.status(200).send({Err: 'Barcode for a serialised product scanned. Please scan serial number instead'});
				}

				if(QcStation.Cartons[x].Items[y].QtyVerified >= QcStation.Cartons[x].Items[y].QtyPacked){
					NonSerialError = 'Non Serialised Stock All Confirmed';
				} else {
					QcStation.Cartons[x].Items[y].QtyVerified += 1;
					QcStation.Cartons[x].CartonChecked = CheckCartonCheckComplete(QcStation.Cartons[x].Items);
					QcStation.active = true;
					QcStation.updated_at = new Date();
					QcStation.save(function (error, savedData){
						let ReturnData = {
							StationData: savedData
						}
						return res.status(200).send(ReturnData);
					});

					return;
				}
			} else {
				if(QcStation.Cartons[x].Items[y].Serials && QcStation.Cartons[x].Items[y].Serials.length > 0){
					let index1 = QcStation.Cartons[x].Items[y].Serials.indexOf(Item);
					let index2 = QcStation.Cartons[x].Items[y].Serials.indexOf('892700000' + Item);
					let index3 = QcStation.Cartons[x].Items[y].Serials.indexOf('892700008' +Item);

					let index = index1;
					if(index < 0){
						index = index2;
					}

					if(index < 0){
						index = index3;
					}

					if(index >= 0){
						let vIndex = QcStation.Cartons[x].Items[y].VerifiedSerials.indexOf(QcStation.Cartons[x].Items[y].Serials[index]);
						if(vIndex >= 0){
							return res.status(200).send({Err: 'Scanned Serial number already verified'});
						}

						QcStation.active = true;
						QcStation.updated_at = new Date();
						QcStation.Cartons[x].Items[y].VerifiedSerials.push(QcStation.Cartons[x].Items[y].Serials[index]);
						QcStation.Cartons[x].Items[y].QtyVerified = QcStation.Cartons[x].Items[y].VerifiedSerials.length;
						QcStation.Cartons[x].CartonChecked = CheckCartonCheckComplete(QcStation.Cartons[x].Items);
						QcStation.save(function (error, savedData){
							let ReturnData = {
								StationData: savedData
							}
							return res.status(200).send(ReturnData);
						});

						return;
					}
				}
			}
			y++;
		}

		if(NonSerialError){
			return res.status(200).send({Err: NonSerialError});
		}

		return res.status(200).send({Err: 'Scanned Barcode/Serial is not found in this parcel'});
	});
});

router.get('/API/QCFindCartonAndWeigh/:id', requireLogin, function (req, res){
	let data = JSON.parse(req.params.id);
	let CartonID = data.TrCartonId;
	let Container = data.Container;
	let IpAddr = req.session.ip;
	let user = req.session.username;
	let Msg = null;

	QcStationModel.findOne({stationIP: IpAddr}, function (error, QcStation){
		if(error || !QcStation){
			return res.status(200).send({Err: 'Scanned Parcel ID is invalid'});
		}

		let x = 0;
		if (CartonID.indexOf('NTM') >= 0){
			let TempCtn = CartonID.replace('NTM', '');
			CartonID = TempCtn;
		}

		while(x < QcStation.Cartons.length){
			if(QcStation.Cartons[x].CartonID == CartonID){
				break;
			}
			x++;
		}

		if(x >= QcStation.Cartons.length){
			return res.status(200).send({Err: 'Scanned Parcel ID does not belong in Transporter ' + Container});
		}

		if(QcStation.Cartons[x].CartonChecked){
			if(QcStation.Cartons[x].MeasuredWeight >= QcStation.Cartons[x].MinWeight && QcStation.Cartons[x].MeasuredWeight <= QcStation.Cartons[x].MaxWeight){
				return res.status(200).send({Err: 'Scanned Parcel ' + CartonID + ' Already Checked. Successfully Passed Weight Check'});
			}

			return res.status(200).send({Err: 'Scanned Parcel ' + CartonID + ' Already Checked'});
		}

		QcStation.Inprogress = CartonID;

		if(QcStation.ScaleManualMode){
			QcStation.save(function (error, savedData){
				let ReturnData = {
					StationData: savedData,
					AskForWait: true
				}
				return res.status(200).send(ReturnData);
			});

			return;
		}

		QcStation.Cartons[x].MeasuredWeight = null;
		QcStation.AverageWeight = null;
		QcStation.ScaleQueryCounts = 0;
		GetWeightFromScale(QcStation.ScaleIP, QcStation.ScalePort, QcStation.stationNumber);
		QcStation.save(function (error, savedData){
			let ReturnData = {
				StationData: savedData
			}
			return res.status(200).send(ReturnData);
		});
	});
});

router.get('/API/PrintCancelManifest/:id', requireLogin, function (req, res){
	let data = JSON.parse(req.params.id);
	let OrderID = data.Order;
	let Queue = data.Queue;
	let IpAddr = req.session.ip;
	let user = req.session.username;
	let Msg = null;

	PickListModel.find({'orders.orderID': OrderID}, function (error, docArr){
		if(error || !docArr || docArr.length <= 0){
			return res.status(405).send({Err: 'Failed to find picklists for order'});
		}

		let x = 0;
		let Cartons = [];
		let Items = [];
		let CourierId = null;
		let OrderType = null;

		while(x < docArr.length){
			if(docArr[x].orders[0].cartonID && docArr[x].orders[0].packed){
				let y = 0;
				while(y < docArr[x].orders[0].items.length){
					Cartons.push(docArr[x].orders[0].cartonID);
					Items.push(docArr[x].orders[0].items[y]);
					y++;
				}

				if(!CourierId){
					CourierId = docArr[x].orders[0].courierID;
				}

				if(!OrderType){
					OrderType = docArr[x].orders[0].orderType;
				}
			}
			x++;
		}

		let Reprint = null;
		CanReq.findOne({'OrderId': OrderID}, function(err, Req){
			if(Req){
				if(Req.ManifestPrinted){
					Reprint = true;
				}

				let PrintData = {
					orderId: OrderID,
					printerName: Queue,
					courierId: CourierId,
					orderType: OrderType,
					Cartons: Cartons,
					items: Items,
					Reprint: Reprint
				};

				Pubclient.publish('CANCEL_QUEUE', JSON.stringify(PrintData), function(err){
					if(err){
						return res.status(200).send({Msg: 'Error printing documents for order ' + OrderID});
					}

					Req.ManifestPrinted = true;
					Req.PrintedBy = user;
					Req.save(function(err, sd){
						return res.status(200).send({Msg: 'Manifest for order ' + OrderID + ' printed.'});
					});
				});

				return;
			}

			return res.status(200).send({Msg: 'Error printing documents for order ' + OrderID});
		});
	});
});

router.get('/API/getCancelledOrderList/', requireLogin, function (req, res){
	let IpAddr = req.session.ip;
	let user = req.session.username;
	let Msg = null;

	CanReq.find({'Status': {$ne: 'SHIPPED'}}, function(err, ReqArr){
		if(err){
			return res.status(200).send({Err: 'Err'});
		}

		if(!ReqArr || ReqArr.length <= 0){
			return res.status(200).send({Orders: null});
		}

		return res.status(200).send({Orders: ReqArr});
	});
});

router.get('/API/CanReqPicklists/:id', requireLogin, function (req, res){
	let data = JSON.parse(req.params.id);
	let OrderID = data.OrderID;
	let IpAddr = req.session.ip;
	let user = req.session.username;
	let Msg = null;

	PickListModel.find({'orders.orderID': OrderID}, function (error, docArr){
		if(error || !docArr || docArr.length <= 0){
			return res.status(405).send({Err: 'Failed to find picklists for order'});
		}

		let Picklists = [];
		let ReadyForManifest = null;
		let TotesCleared = true;
		let x = 0;
		while(x < docArr.length){
			if(docArr[x].orders.length > 1){
				let y = 0;
				while(y < docArr[x].orders.length){
					if(docArr[x].orders[y].orderID == OrderID){
						let ShowScanSerials = 'Tote needs to be unconsolidated ';
						if(docArr[x].orders[y].cartonID && !ReadyForManifest && TotesCleared){
							ReadyForManifest = 'Print Cancellation Manifest';
						}

						if(!docArr[x].orders[y].packed && docArr[y].toteID){
							ShowScanSerials = 'and then unpicked';
							ReadyForManifest = null;
							TotesCleared = false;
						}

						Picklists.push({OrderId: docArr[x].orders[y].orderID,
										PickListID: docArr[x].orders[y].pickListID,
										Status: docArr[x].orders[y].WmsOrderStatus? docArr[x].orders[y].WmsOrderStatus : docArr[x].Status,
										Consolidated: true,
										Container: docArr[x].toteID,
										Details: docArr[x].orders[y].CancelInstruction, ShowScanSerials: ShowScanSerials});
					}
					y++;
				}
			} else {
				let ShowScanSerials = null;

				if(!docArr[x].orders[0].packed && docArr[x].toteID && docArr[x].PickType != 'FULLPICK'){
					ShowScanSerials = 'Tote needs to be unpicked';
					ReadyForManifest = null;
					TotesCleared = false;
				}

				if(docArr[x].orders[0].cartonID && !ReadyForManifest && TotesCleared){
					ReadyForManifest = 'Print Cancellation Manifest';
				}

				Picklists.push({OrderId: docArr[x].orders[0].orderID,
								PickListID: docArr[x].orders[0].pickListID,
								Status: docArr[x].Status,
								Consolidated: false,
								Container: docArr[x].toteID,
								Details: docArr[x].orders[0].CancelInstruction, ShowScanSerials: ShowScanSerials});
			}
			x++;
		}

		CanReq.findOne({'OrderId': OrderID}, function(err, Req){
			if(Req){
				if(Req.ManifestPrinted && TotesCleared){
					ReadyForManifest = 'Reprint Cancellation Manifest';
				}
			}

			if(!ReadyForManifest){
				return res.status(200).send({Picklists: Picklists, ReadyForManifest: ReadyForManifest});
			}

			PackStationModel.find({}, function (error, StnArr){
				if(error || !StnArr || StnArr.length <= 0){
					return res.status(200).send({Picklists: Picklists, ReadyForManifest: ReadyForManifest});
				}

				let PrintQs = [];
				let b = 0;
				while(b < StnArr.length){
					PrintQs.push({Station: 'Pack Station: ' + StnArr[b].stationNumber, QName: StnArr[b].desktopPrinterQName});
					b++;
				}

				return res.status(200).send({Picklists: Picklists, ReadyForManifest: ReadyForManifest, PrintQs: PrintQs});
			});
		});
	});
});

router.get('/API/QCCheckIfWaitReceived/', requireLogin, function (req, res){
	let IpAddr = req.session.ip;
	let user = req.session.username;
	let Msg = null;

	QcStationModel.findOne({stationIP: IpAddr}, function (error, QcStation){
		if(error || !QcStation){
			return res.status(200).send({Err: 'Err'});
		}

		if(QcStation.TempWeightReads.length < 5){
			QcStation.ScaleQueryCounts++;
			GetWeightFromScale(QcStation.ScaleIP, QcStation.ScalePort, QcStation.stationNumber);
		} else {
			let x = 0;
			let TotalWeight = 0;
			while(x < 5){
				if(!isNaN(QcStation.TempWeightReads[x])){
					TotalWeight += QcStation.TempWeightReads[x];
				}
				x++;
			}

			let Average = TotalWeight / 5;
			QcStation.AverageWeight = Average;
		}

		QcStation.save(function (error, savedData){
			let ReturnData = {
				StationData: savedData
			}
			return res.status(200).send(ReturnData);
		});
	});
});

router.get('/API/QCCompareEnteredWeight/:id', requireLogin, function (req, res){
	let data = JSON.parse(req.params.id);
	let CartonID = data.TrCartonId;
	let Container = data.Container;
	let EnteredWeight = data.EnteredWeight;
	let IpAddr = req.session.ip;
	let user = req.session.username;
	let Msg = null;

	QcStationModel.findOne({stationIP: IpAddr}, function (error, QcStation){
		if(error || !QcStation){
			return res.status(200).send({Err: 'Scanned Parcel ID is invalid'});
		}

		let x = 0;
		if (CartonID.indexOf('NTM') >= 0){
			let TempCtn = CartonID.replace('NTM', '');
			CartonID = TempCtn;
		}

		while(x < QcStation.Cartons.length){
			if(QcStation.Cartons[x].CartonID == CartonID){
				break;
			}
			x++;
		}

		if(x >= QcStation.Cartons.length){
			return res.status(200).send({Err: 'Scanned Parcel ID does not belong in Transporter ' + Container});
		}

		if(isNaN(EnteredWeight)){
			return res.status(200).send({Err: 'Invalid Weight Entered (' + EnteredWeight + ') for Parcel ' + CartonID});
		}

		QcStation.active = true;
		QcStation.updated_at = new Date();
		QcStation.Cartons[x].MeasuredWeight = parseFloat(EnteredWeight);
		QcStation.Cartons[x].CartonReason = 'Manual Scale Weight Failed';
		QcStation.LastScannedResult = 'Parcel ' + CartonID + ' entered weight ' + QcStation.Cartons[x].MeasuredWeight + ' out of tolerance';
		let WeightOk = false;

		if(QcStation.Cartons[x].MeasuredWeight >= QcStation.Cartons[x].MinWeight && QcStation.Cartons[x].MeasuredWeight <= QcStation.Cartons[x].MaxWeight){
			QcStation.Cartons[x].CartonChecked = true;
			QcStation.Cartons[x].WeightCheckComplete = true;
			QcStation.Cartons[x].CartonProblem = false;
			QcStation.Inprogress = null;
			QcStation.Cartons[x].CartonReason = 'Manual Scale Weight Passed';
			QcStation.LastScannedResult = 'Parcel ' + CartonID + ' entered weight ' + QcStation.Cartons[x].MeasuredWeight + ' is within tolerance';
			WeightOk = true;
		}

		QcStation.TempWeightReads = [];
		QcStation.AverageWeight = null;
		QcStation.ScaleQueryCounts = 0;
		QcStation.save(function (error, savedData){
			let ReturnData = {
				StationData: savedData,
				WeightOk: WeightOk
			}
			return res.status(200).send(ReturnData);
		});
	});
});

router.get('/API/CompleteQCPickListCarton/:id', requireLogin, function (req, res){
	let data = JSON.parse(req.params.id);
	let CartonID = data.CartonID;
	let IpAddr = req.session.ip;
	let user = req.session.username;
	let Msg = null;

	QcStationModel.findOne({stationIP: IpAddr}, function (error, QcStation){
		if(error || !QcStation){
			return res.status(200).send({Err: 'Failed to find parcel to complete'});
		}

		let x = 0;
		while(x < QcStation.Cartons.length){
			if(QcStation.Cartons[x].CartonID == CartonID){
				break;
			}
			x++;
		}

		if(x >= QcStation.Cartons.length){
			return res.status(200).send({Err: 'Failed to find parcel to complete'});
		}

		QcStation.Cartons[x].CartonReason = 'Parcel Content Successfully Verified';
		QcStation.Inprogress = null;

		if(QcStation.Cartons.length == 1 && QcStation.Cartons[0].CartonID == QcStation.ContainerID){
			CompleteCartonToPickList(QcStation.Cartons[0], user, function(){
				QcStation.active = true;
				QcStation.updated_at = new Date();
				QcStation.ContainerID = null;
				QcStation.Cartons = null;
				QcStation.TempWeightReads = [];
				QcStation.AverageWeight = null;
				QcStation.ScaleQueryCounts = 0;
				QcStation.LastScannedResult = null;
				QcStation.save(function (error, savedData){
					let ReturnData = {
						StationData: savedData,
						User: user
					}
					return res.status(200).send(ReturnData);
				});
			});
			return;
		}

		QcStation.Cartons[x].WeightCheckComplete = true;
		QcStation.LastScannedResult = null;
		QcStation.active = true;
		QcStation.TempWeightReads = [];
		QcStation.AverageWeight = null;
		QcStation.ScaleQueryCounts = 0;
		QcStation.updated_at = new Date();
		QcStation.save(function (error, savedData){
			let ReturnData = {
				StationData: savedData
			}
			return res.status(200).send(ReturnData);
		});
	});
});

router.get('/API/CompleteQCPickListTransporter/:id', requireLogin, function (req, res){
	let data = JSON.parse(req.params.id);
	let Transporter = data.Transporter;
	let IpAddr = req.session.ip;
	let user = req.session.username;
	let Msg = null;

	QcStationModel.findOne({stationIP: IpAddr}, function (error, QcStation){
		if(error || !QcStation){
			return res.status(200).send({Err: 'Failed to find parcel to complete'});
		}

		if(QcStation.ContainerID == Transporter){
			if(AllChecked(QcStation.Cartons)){
				QCCalculateTransporterWeight(Transporter, QcStation.Cartons, function(Resp){
					if(Resp.Err){
						return res.status(200).send({Err: Resp.Err});
					}

					CompleteCartonInTrToPickList(QcStation.ContainerID, QcStation.Cartons, 0, Resp, user, function(){
						QcStation.ContainerID = null;
						QcStation.Cartons = null;
						QcStation.LastScannedResult = null;
						QcStation.active = true;
						QcStation.Inprogress = null;
						QcStation.TempWeightReads = [];
						QcStation.AverageWeight = null;
						QcStation.ScaleQueryCounts = 0;
						QcStation.updated_at = new Date();
						QcStation.save(function (error, savedData){
							let ReturnData = {
								StationData: savedData
							}
							return res.status(200).send(ReturnData);
						});
					});
				});
			}
			return;
		}

		QcStation.ContainerID = null;
		QcStation.Cartons = null;
		QcStation.LastScannedResult = null;
		QcStation.active = true;
		QcStation.Inprogress = null;
		QcStation.TempWeightReads = [];
		QcStation.AverageWeight = null;
		QcStation.ScaleQueryCounts = 0;
		QcStation.updated_at = new Date();
		QcStation.save(function (error, savedData){
			let ReturnData = {
				StationData: savedData
			}
			return res.status(200).send(ReturnData);
		});
	});
});

router.get('/API/TrRemoveConfirm/:id', requireLogin, function (req, res){
	let data = JSON.parse(req.params.id);
	let Transporter = data.Transporter;
	let Carton = data.Carton;
	let IpAddr = req.session.ip;
	let user = req.session.username;
	let Msg = null;

	QcStationModel.findOne({stationIP: IpAddr}, function (error, QcStation){
		if(error || !QcStation){
			return res.status(200).send({Err: 'Failed to find parcel to complete'});
		}

		if(QcStation.ContainerID == Transporter){
			let x = 0;
			while(x < QcStation.Cartons.length){
				if(QcStation.Cartons[x].CartonID == Carton){
					break;
				}
				x++;
			}

			if(x < QcStation.Cartons.length){
				QcStation.Cartons.splice(x, 1);
				QcStation.Inprogress = null;
				QcStation.updated_at = new Date();

				QcStation.save(function (error, savedData){
					let ReturnData = {
						StationData: savedData
					}

					PickListModel.findOne({'orders.cartonID': Carton}, function (error, doc){
						if(error || !doc){
							return res.status(200).send(ReturnData);
						}

						doc.orders[0].transporterID = null;
						doc.transporterID = null;
						doc.save(function(err, sd){
							return res.status(200).send(ReturnData);
						});
					});
				});
				return;
			}

			let ReturnData = {
				StationData: QcStation
			}
			return res.status(200).send(ReturnData);
		}

		let ReturnData = {
			StationData: QcStation
		}
		return res.status(200).send(ReturnData);
	});
});

router.get('/API/AsideQCPickListCarton/:id', requireLogin, function (req, res){
	let data = JSON.parse(req.params.id);
	let CartonID = data.CartonID;
	let IpAddr = req.session.ip;
	let user = req.session.username;
	let Msg = null;

	QcStationModel.findOne({stationIP: IpAddr}, function (error, QcStation){
		if(error || !QcStation || !QcStation.Cartons || QcStation.Cartons.length <= 0){
			return res.status(200).send({Err: 'Failed to find parcel to set aside'});
		}

		let x = 0;
		while(x < QcStation.Cartons.length){
			if(QcStation.Cartons[x].CartonID == CartonID){
				break;
			}
			x++;
		}

		if(x >= QcStation.Cartons.length){
			return res.status(200).send({Err: 'Failed to find parcel to set aside'});
		}

		QcStation.Cartons[x].CartonReason = 'Parcel Set Aside. Not All Contents Where Verified';
		QcStation.Cartons[x].CartonProblem = true;

		if(QcStation.Cartons.length == 1 && QcStation.Cartons[0].CartonID == QcStation.ContainerID){
			CompleteCartonToPickList(QcStation.Cartons[0], user, function(){
				QcStation.active = true;
				QcStation.updated_at = new Date();
				QcStation.ContainerID = null;
				QcStation.Cartons = null;
				QcStation.Inprogress = null;
				QcStation.TempWeightReads = [];
				QcStation.AverageWeight = null;
				QcStation.ScaleQueryCounts = 0;
				QcStation.LastScannedResult = null;
				QcStation.save(function (error, savedData){
					let ReturnData = {
						StationData: savedData,
						User: user
					}
					return res.status(200).send(ReturnData);
				});
			});
			return;
		}

		QcStation.Cartons[x].WeightCheckComplete = true;
		QcStation.Inprogress = null;
		QcStation.LastScannedResult = null;
		QcStation.TempWeightReads = [];
		QcStation.AverageWeight = null;
		QcStation.ScaleQueryCounts = 0;
		QcStation.active = true;
		QcStation.updated_at = new Date();
		QcStation.save(function (error, savedData){
			let ReturnData = {
				StationData: savedData
			}
			return res.status(200).send(ReturnData);
		});
	});
});

router.get('/API/ClearQCPickListData/', requireLogin, function (req, res){
	let user = req.session.username;
	let IpAddr = req.session.ip;
	let Msg = null;

	QcStationModel.findOne({stationIP: IpAddr}, function (error, QcStation){
		if(error || !QcStation){
			return res.status(200).send({NoStation: true, ScanMode: false});
		}

		QcStation.active = true;
		QcStation.updated_at = new Date();
		QcStation.ContainerID = null;
		QcStation.Cartons = null;
		QcStation.Inprogress = null;
		QcStation.TempWeightReads = [];
		QcStation.AverageWeight = null;
		QcStation.ScaleQueryCounts = 0;
		QcStation.LastScannedResult = null;
		QcStation.save(function (error, savedData){
			let ReturnData = {
				StationData: savedData,
				User: user
			}
			return res.status(200).send(ReturnData);
		});
	});
});

router.get('/API/CourierContainer/:id', requireLogin, function (req, res){
	let Container = req.params.id;
	let IpAddr = req.session.ip;
	let user = req.session.username;
	let Msg = null;

	if(Container[0] == '7'){
		PickListModel.find({transporterID: Container}, function (error, docArr){
			if(error || !docArr || docArr.length <= 0){
				return res.status(200).send({Err: 'Failed to find scanned container'});
			}

			let doc = docArr[0];
			CourierRejects.findOne({'CartonID': doc.toteID, 'TransporterID': Container}, function(err, Crv){
				if(err){
					return res.status(200).send({Err: 'Failed to find scanned container'});
				}

				if(!Crv){
					let MyDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
					CreateCourierRejectViewForEachInTr(docArr, 0, user, MyDate, function(err, savedDoc){
						let ShowHistory = false;
						let History = [];

						let ReturnData = {
							ShowHistory: ShowHistory,
							History: History,
							Container: Container,
							CurrentError: savedDoc.RejectReason[savedDoc.RejectReason.length -1].Reason
						}

						return res.status(200).send(ReturnData);
					});

					return;
				}

				Crv.RejectReason[Crv.RejectReason.length -1].Checked = true;
				Crv.RejectReason[Crv.RejectReason.length -1].CheckedBy = user;
				Crv.save(function(err, savedDoc){
					let ShowHistory = false;
					let History = [];
					if(savedDoc.RejectReason.length > 1){
						ShowHistory = true;
						let x = 0;
						while(x < savedDoc.RejectReason.length -1){
							History.push(savedDoc.RejectReason[x]);
							x++;
						}
					}

					let ReturnData = {
						ShowHistory: ShowHistory,
						History: History,
						Container: Container,
						CurrentError: savedDoc.RejectReason[savedDoc.RejectReason.length -1].Reason
					}

					return res.status(200).send(ReturnData);
				});
			});
		});
		return;
	}

	PickListModel.findOne({toteID: Container}, function (error, doc){
		if(error || !doc){
			return res.status(200).send({Err: 'Failed to find scanned container'});
		}

		if(doc.transporterID){
			return res.status(200).send({Err: 'Parcel should be in transporter ' + doc.transporterID});
		}

		CourierRejects.findOne({'CartonID': doc.toteID}, function(err, Crv){
			if(err){
				return res.status(200).send({Err: 'Failed to find scanned container'});
			}

			if(!Crv){
				let MyDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
				CreateSingleContainerCourierRejectRec(user, MyDate, doc, function(err, savedDoc){
					let ShowHistory = false;
					let History = [];

					let ReturnData = {
						ShowHistory: ShowHistory,
						History: History,
						Container: Container,
						CurrentError: savedDoc.RejectReason[savedDoc.RejectReason.length -1].Reason
					}

					return res.status(200).send(ReturnData);
				});

				return;
			}

			Crv.RejectReason[Crv.RejectReason.length -1].Checked = true;
			Crv.RejectReason[Crv.RejectReason.length -1].CheckedBy = user;
			Crv.RejectReason[Crv.RejectReason.length -1].Date = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			Crv.save(function(err, savedDoc){
				let ShowHistory = false;
				let History = [];
				if(savedDoc.RejectReason.length > 1){
					ShowHistory = true;
					let x = 0;
					while(x < savedDoc.RejectReason.length -1){
						History.push(savedDoc.RejectReason[x]);
						x++;
					}
				}

				let ReturnData = {
					ShowHistory: ShowHistory,
					History: History,
					Container: Container,
					CurrentError: savedDoc.RejectReason[savedDoc.RejectReason.length -1].Reason
				}

				return res.status(200).send(ReturnData);
			});
		});
	});
});

router.get('/API/getConveyorRejectedToteId/:id', requireLogin, function (req, res){
	let ToteId = req.params.id;
	let IpAddr = req.session.ip;
	let user = req.session.username;
	let Msg = null;

	if(ToteId[0] != '0' || ToteId.length != 7){
		return res.status(200).send({Err: 'Scanned Id is not a valid tote Id'});
	}

	PickListModel.findOne({toteID: ToteId}, function (error, doc){
		if(error || !doc){
			return res.status(200).send({Err: 'Failed to find scanned tote'});
		}

		if(!doc.orders[0].rejected){
			if(doc.Status == 'PICKED'){
				return res.status(200).send({Err: 'Tote ' + ToteId + ' is all picked and should be heading to packing. Please place back on the conveyor'});
			}

			return res.status(200).send({Err: 'Tote ' + ToteId + ' still in picking and should have not rejected. Please place back on the conveyor'});
		}

		let ReturnData = {
			Container: ToteId,
			CurrentError: (!doc.orders[0].rejectedReason)? 'Rejected due to unknown reason': doc.orders[0].rejectedReason
		}

		return res.status(200).send(ReturnData);
	});
});

router.get('/API/ClearPtlRejectError/:id', requireLogin, function (req, res){
	let ToteId = req.params.id;
	let IpAddr = req.session.ip;
	let user = req.session.username;
	let Msg = null;

	if(ToteId[0] != '0' || ToteId.length != 7){
		return res.status(200).send({Err: 'Scanned Id is not a valid tote Id'});
	}

	PickListModel.findOne({toteID: ToteId}, function (error, doc){
		if(error || !doc){
			return res.status(200).send({Err: 'Failed to find scanned tote'});
		}

		let x = 0;
		while(x < doc.orders.length){
			doc.orders[x].rejectedReason = null;
			doc.orders[x].rejected = false;
			x++;
		}

		doc.save(function(err, savedDoc){
			let ReturnData = {
				Container: ToteId
			}

			return res.status(200).send(ReturnData);
		});
	});
});

// Get a pick list per ID.
router.get('/API/pickList/:id', requireLogin, function (req, res){
	var Tote = req.params.id;
	var user = req.session.username;
	var IpAddr = req.session.ip;
	var Msg = null;

  GetStation(IpAddr, function (Station){
		if (!Station){
			Msg = 'Scan Tote: User ' + user + ' failed scan tote from an unknown station';
			WriteToFile(Msg);
			return res.status(200).send({NoStation:true});
		} else {
			if(Tote == "undefined" || !Tote){
				Tote = Station.currentTote;
			}

			Msg = 'Request from station ' + Station.stationNumber + ' with tote ' + Tote;
			WriteToFile(Msg);

			if(Tote == "undefined" || !Tote){
				return res.status(200).send({ScanMode:false});
			}

			PickListModel.findOne({toteID: Tote}, function (error, doc){
				if (error || !doc){
					Msg = 'Tote not found for station ' + Station.stationNumber + ' scanned tote ' + Tote;
					WriteToFile(Msg);
					return res.status(405).send('No data found for scanned tote ' + Tote);
				} else {
					var Err = ValidateScannedTote(doc, Station.stationNumber)

					if(Err){
						return res.status(405).send('Tote ' + Tote + ': ' + Err);
					} else {
						ToteValidForPutWalls(doc, Station.stationNumber, function(Valid){
							var ToteMode = null;
							if(Valid){
								ToteMode = 'PUT';
								if(Station.currentTote != Tote){
									Station.currentTote = Tote;

									Station.save(function(err, savedStn){
										Msg = 'Tote ' + Tote + ' added to station ' + savedStn.stationNumber + ' by user ' + user;
										WriteToFile(Msg);
										return res.status(200).send({ScanMode: true, ToteMode:ToteMode});
									});
								} else {
									return res.status(200).send({ScanMode: true, ToteMode:ToteMode});
								}

							} else {
								doc.toteInUse = true;
								doc.toteInUseBy = Station.stationNumber.toString();
								doc.containerLocation = null;

								var CurrentOrderToProcess = GetNextOrderToProcess(doc);

								CheckIfPickListIsUnique(doc, CurrentOrderToProcess, function(PickListCount){
									Msg = 'PickListID ' + doc.orders[CurrentOrderToProcess].pickListID + ' number of occurrences = ' + PickListCount;
									WriteToFile(Msg);
									if(PickListCount == 1){
										CalculateCartonSize(doc, CurrentOrderToProcess, function(CartonSize){
											doc.orders[CurrentOrderToProcess].cartonSize = CartonSize;
											doc.save(function(error, savedDoc){
												CreateResponseData(savedDoc, Station.stationNumber, CurrentOrderToProcess, user, function(Details){
													if(Details.error){
														return res.status(405).send(Details);
													} else {
														if(Details.ScanMode == false){
															return res.status(200).send(Details);
														} else {
															if(Station.currentTote != savedDoc.toteID){
																Station.currentTote = savedDoc.toteID;

																Station.save(function(err, savedStn){
																	Msg = 'Tote ' + Tote + ' added to station ' + savedStn.stationNumber + ' by user ' + user;
																	WriteToFile(Msg);
																	return res.status(200).send(Details);
																});
															} else {
																return res.status(200).send(Details);
															}
														}
													}
												});
											});
										});
									} else {
										Msg = 'Tote ' + Tote + ' has a duplicate picklist';
										WriteToFile(Msg);
										return res.status(405).send(Msg);
									}
								});
							}
					  });
				  }
			  }
		  });
	  }
  });
});

router.get('/API/printAutoBag/:ToDo', function(req, res) {
	var theToteID = req.params.ToDo

	console.log('___________________________ THE TOTE ID: ' + theToteID)

	AutobagModule.printBagLabel(theToteID, function(response) {
		res.send(response)
	})
})

router.get('/API/getCartonID/:ToDo', function (req, res) {
    return res.status(200).send({code: '00', meddage: 'success', data: req.params.ToDo})
})

router.get('/API/getCartons/:ToDo', requireLogin, function (req, res)
{
	var user = req.session.username;
	var cartonTote = req.params.ToDo;
	var IpAddr = req.session.ip;
	var Msg = null;

	// var thePickList = getPickList(cartonTote)
	getPickList(cartonTote, function(thePackType) {
		GetStation(IpAddr, function (doc)
		{
			if (!doc)
			{
				Msg = 'get Cartons: User ' + user + ' failed attempt to get carton sizes from an unknown station';
				WriteToFile(Msg);
				return res.status(200).send({NoStation:true});
			}
			else
			{
				Params.find({}, function(err, Parameters)
				{
					if(err || !Parameters[0] || Parameters.length <= 0)
					{
						return callback(3);
					}
					else
					{
						var len = Parameters[0].weightSettings.length;

						if(Parameters[0].weightSettings && len > 0)
						{
							var Cartons = [];
							var v = 0;
							var n = -1;
							while(v < Parameters[0].weightSettings.length)
							{
								console.log('____________________________________________ | ' + doc.autobagPrinter)
								if(doc.autobagPrinter) {
									if(Parameters[0].weightSettings[v].isCarton || (Parameters[0].weightSettings[v].StockSize == 1 && thePackType == 'PUT'))
									{
										var line =
										{
											id: Parameters[0].weightSettings[v].StockSize,
											value: Parameters[0].weightSettings[v].StockSize.toString()
										}

										n++;
										Cartons[n] = line;
									}
								} else {
									if(Parameters[0].weightSettings[v].isCarton)
									{
										var line =
										{
											id: Parameters[0].weightSettings[v].StockSize,
											value: Parameters[0].weightSettings[v].StockSize.toString()
										}

										n++;
										Cartons[n] = line;
									}
								}

								v++;
							}

							return res.status(200).send(Cartons);
						}
						else
						{
							return res.status(200).send(null);
						}
					}
				});
			}
		});
	})
});

router.get('/API/getCartons_old/', requireLogin, function (req, res)
{
	var user = req.session.username;
	var IpAddr = req.session.ip;
	var Msg = null;

	GetStation(IpAddr, function (doc)
	{
		if (!doc)
		{
			Msg = 'get Cartons: User ' + user + ' failed attempt to get carton sizes from an unknown station';
			WriteToFile(Msg);
			return res.status(200).send({NoStation:true});
		}
		else
		{
			Params.find({}, function(err, Parameters)
			{
				if(err || !Parameters[0] || Parameters.length <= 0)
				{
					return callback(3);
				}
				else
				{
					var len = Parameters[0].weightSettings.length;

					if(Parameters[0].weightSettings && len > 0)
					{
						var Cartons = [];
						var v = 0;
						var n = -1;
						while(v < Parameters[0].weightSettings.length)
						{
							if(Parameters[0].weightSettings[v].isCarton)
							{
								var line =
								{
									id: Parameters[0].weightSettings[v].StockSize,
									value: Parameters[0].weightSettings[v].StockSize.toString()
								}

								n++;
								Cartons[n] = line;
							}

							v++;
						}

						return res.status(200).send(Cartons);
					}
					else
					{
						return res.status(200).send(null);
					}
				}
			});
		}
	});
});

router.get('/API/updateCartonSize/:id', requireLogin, function (req, res)
{
	var user = req.session.username;
	var IpAddr = req.session.ip;
	var CartonSize = req.params.id;
	var Msg = null;

  if(CartonSize != "undefined")
	{
		GetStation(IpAddr, function (Station)
		{
			if (!Station)
			{
				Msg = 'Update Carton Size: User ' + user + ' failed attempt to update carton size from an unknown station';
				WriteToFile(Msg);
				return res.status(200).send({NoStation:true});
			}
			else
			{
				if(!Station.currentTote)
				{
					return res.status(200).send({ScanMode:false});
				}

				PickListModel.findOne({toteID: Station.currentTote}, function (error, doc)
				{
					if(error || !doc)
					{
						Msg = 'Update Carton Size: Cannot find picklist for tote associated with station ' + Station.stationNumber;
						WriteToFile(Msg);
						return callback({error:"Cannot find a picklist for tote " + Station.currentTote});
					}
					else
					{
						var CurrentOrderToProcess = GetNextOrderToProcess(doc);
						var CurrCartonSize = doc.orders[CurrentOrderToProcess].cartonSize;
						doc.orders[CurrentOrderToProcess].cartonSize = parseInt(CartonSize);

						doc.save(function(err, savedDoc)
						{
							Msg = 'Update Carton Size: User ' + user + ' updated carton size for order ' + savedDoc.orders[CurrentOrderToProcess].orderID + ' from ' + CurrCartonSize + ' to ' + CartonSize;
							WriteToFile(Msg);

							CreateResponseData(savedDoc, Station.stationNumber, CurrentOrderToProcess, user, function(Details)
							{
								if(Details.error)
								{
									return res.status(405).send(Details.error);
								}
								else
								  return res.status(200).send(Details);
							});
						});
					}
				});
			}
		});
	}
	else
	{
		return res.status(405).send("No Carton size selected");
	}
});

router.get('/API/updateCartonToPicklist/:id', requireLogin, function (req, res)
{
	var user = req.session.username;
	var IpAddr = req.session.ip;
	var CartonID = req.params.id;
	var Msg = null;

	GetStation(IpAddr, function (Station)
	{
		if (!Station)
		{
			Msg = 'Update Carton Size: User ' + user + ' failed attempt to update carton size from an unknown station';
			WriteToFile(Msg);
			return res.status(200).send({NoStation:true});
		}
		else
		{
			if(!Station.currentTote)
			{
				return res.status(200).send({ScanMode:false});
			}

			PickListModel.findOne({toteID: Station.currentTote}, function (error, doc)
			{
				if(error || !doc)
				{
					Msg = 'Update Carton: Cannot find picklist for tote associated with station ' + Station.stationNumber;
					WriteToFile(Msg);
					return callback({error:"Cannot find a picklist for tote " + Station.currentTote});
			  }
			  else
			  {
					PickListModel.findOne({'orders.cartonID': CartonID}, function (error, CartonCheck)
					{
						if(error || !CartonCheck)
						{
							var CurrentOrderToProcess = GetNextOrderToProcess(doc);
							doc.orders[CurrentOrderToProcess].cartonID = CartonID;

							doc.save(function(err, savedDoc)
							{
								Msg = 'Update Carton: User ' + user + ' updated carton ' + CartonID + ' to order ' + savedDoc.orders[CurrentOrderToProcess].orderID;
								WriteToFile(Msg);

								CreateResponseData(savedDoc, Station.stationNumber, CurrentOrderToProcess, user, function(Details)
								{
									if(Details.error)
									{
										return res.status(405).send(Details.error);
									}
									else
										return res.status(200).send(Details);
								});
							});
						}
						else
						{
							return res.status(405).send('Scanned carton is not unique ' + CartonID);
						}
					});
				}
			});
		}
	});
});

router.get('/API/reprintCarton/', requireLogin, function (req, res)
{
	var user = req.session.username;
	var IpAddr = req.session.ip;
	var Msg = null;

	GetStation(IpAddr, function (doc)
	{
		if (!doc)
		{
			Msg = 'Document Reprint: User ' + user + ' failed attempt to reprint documents from an unknown station';
			WriteToFile(Msg);
			return res.status(200).send({NoStation:true});
		}
		else
		{
			var Tote = doc.currentTote;

			if(!Tote)
			{
				return res.status(200).send({ScanMode:false});
			}

			PickListModel.findOne({toteID: Tote}, function (error, PickList)
			{
				var CurrentOrderToProcess = GetNextOrderToProcess(PickList);
				GetAllItemsAndCartonsForOrder(PickList, CurrentOrderToProcess, function(OrdDetails)
				{
					if(OrdDetails.error)
					{
						return callback(OrdDetails);
					}
					else
					{
						DetermineCartonNo(PickList, CurrentOrderToProcess, function(savedDoc)
						{
							if(savedDoc.error)
							{
								return res.status(405).send(Details.error);
							}
							else
							{
								SendDocsLabelPrint(savedDoc, CurrentOrderToProcess, savedDoc.orders[CurrentOrderToProcess].UseTransporter, OrdDetails, function(Data)
								{
									Msg = 'Document Reprint: User ' + user + ' reprinted labels/documents for order ' + savedDoc.orders[CurrentOrderToProcess].orderID + ' at station ' + doc.stationNumber;
									WriteToFile(Msg);

									CreateResponseData(savedDoc, doc.stationNumber, CurrentOrderToProcess, user, function(Details)
									{
										if(Details.error)
										{
											return res.status(405).send(Details.error);
										}
										else
											return res.status(200).send(Details);
									});
								});
							}
						});
					}
				});
			});
		}
	});
});

router.get('/API/updateScannedTransporter/:id', requireLogin, function (req, res)
{
	var user = req.session.username;
	var IpAddr = req.session.ip;
	var TransporterID = req.params.id;
	var Msg = null;

	GetStation(IpAddr, function (doc)
	{
		if (!doc)
		{
			Msg = 'Update Transporter: User ' + user + ' failed attempt to update transporter to order from an unknown station';
			WriteToFile(Msg);
			return res.status(200).send({NoStation:true});
		}
		else
		{
			var Tote = doc.currentTote;

			if(!Tote)
			{
				return res.status(200).send({ScanMode:false});
			}

			PickListModel.findOne({toteID: Tote}, function (error, PickList)
			{
				if (error || !PickList)
				{
					Msg = 'Update Transporter: Cannot find picklist for tote ' + Tote + ' associated with station ' + doc.stationNumber;
					WriteToFile(Msg);
					return res.status(200).send({error:"Cannot find a picklist for tote " + Tote});
				}
				else
				{
				      var CurrentOrderToProcess = GetNextOrderToProcess(PickList);
                                      if(CurrentOrderToProcess != null){
				        PickList.orders[CurrentOrderToProcess].transporterID = TransporterID;

					PickList.save(function(err, savedDoc)
					{
						Msg = 'Update Transporter: order ' + savedDoc.orders[CurrentOrderToProcess].orderID + ' successfully added to transporter ' + savedDoc.orders[CurrentOrderToProcess].transporterID;
						WriteToFile(Msg);

						CreateResponseData(savedDoc, doc.stationNumber, CurrentOrderToProcess, user, function(Details)
						{
							if(Details.error)
							{
								return res.status(405).send(Details.error);
							}
							else
								return res.status(200).send(Details);
						});
					});
                                    } else {
                                      WriteToFile("Unable to calculate the CurrentOrderToProcess at transporter scan - Station " + doc.stationNumber);
                                      return res.status(405).send("Failed to find order on station, call administrator");
                                    }
				}
			});
		}
	});
});

router.get('/API/updateVerifyScannedDoc/:id', requireLogin, function (req, res)
{
	var user = req.session.username;
	var IpAddr = req.session.ip;
	var ScannedDoc = req.params.id;
	var Msg = null;

	GetStation(IpAddr, function (doc)
	{
		if (!doc)
		{
			Msg = 'Document Validation: User ' + user + ' failed attempt to validate order document from an unknown station';
			WriteToFile(Msg);
			return res.status(200).send({NoStation:true});
		}
		else
		{
			var Tote = doc.currentTote;

			if(!Tote)
			{
				return res.status(200).send({ScanMode:false});
			}

			PickListModel.findOne({toteID: Tote}, function (error, PickList)
			{
				if (error || !PickList)
				{
					Msg = 'Document Validation: Cannot find picklist for tote ' + Tote + ' associated with station ' + doc.stationNumber;
					WriteToFile(Msg);
					return res.status(200).send({error:"Cannot find a picklist for tote " + Tote});
				}
				else
				{
			             var CurrentOrderToProcess = GetNextOrderToProcess(PickList);
                                     if(CurrentOrderToProcess != null){
					PickList.orders[CurrentOrderToProcess].orderVerified = true;

					PickList.save(function(err, savedDoc)
					{
						Msg = 'Document Validation: order ' + ScannedDoc + ' successfully validated against printed documents';
						WriteToFile(Msg);

						CreateResponseData(savedDoc, doc.stationNumber, CurrentOrderToProcess, user, function(Details)
						{
							if(Details.error)
							{
								return res.status(405).send(Details.error);
							}
							else
								return res.status(200).send(Details);
						});
					});
                                    } else {
                                      WriteToFile("Unable to calculate CurrentOrderToProcess at Document verification - station " + doc.stationNumber );
                                      return res.status(405).send("Unable to find order at station, call administrator");
                                    }
				}
			});
		}
	});
});

router.get('/API/pickListReset/', requireLogin, function (req, res)
{
	var user = req.session.username;
	var IpAddr = req.session.ip;
	var Msg = null;

	GetStation(IpAddr, function (doc)
	{
		if (!doc)
		{
			Msg = 'Tote Reset: User ' + user + ' failed attempt to reset tote from an unknown station';
			WriteToFile(Msg);
			return res.status(200).send({NoStation:true});
		}
		else
		{
			var Tote = doc.currentTote;

			if(!Tote)
			{
				return res.status(200).send({ScanMode:false});
			}

			Msg = 'Tote Reset: User ' + user + ' attempting to reset tote ' + Tote + ' at station ' + doc.stationNumber;
			WriteToFile(Msg);

			PickListModel.findOne({toteID: Tote}, function (error, PickList)
			{
				if (error || !PickList)
				{
					Msg = 'Tote Reset: Cannot find picklist for tote ' + Tote + ' associated with station ' + doc.stationNumber;
					WriteToFile(Msg);
					return res.status(200).send({error:"Cannot find a picklist for tote " + Tote});
				}
				else
				{
					ResetPicklistForTote(PickList, user, doc.stationNumber, function(Details)
					{
						if(Details.error)
						{
							return res.status(405).send(Details.error);
						}
						else
						{
							ClearToteFromStation(doc.stationNumber, function(Details)
							{
								Msg = 'Tote Reset: Tote ' + Tote + ' successfully reset and removed from station ' + doc.stationNumber;
								WriteToFile(Msg);
								return res.status(200).send(Details);
							});
						}
					});
				}
			});
		}
	});
});

// Get the serial record from the db.
router.get('/API/getSerial/:id', requireLogin, function (req, res)
{
	var user = req.session.username;
	var ScannedSerial = req.params.id;
	var IpAddr = req.session.ip;
	var Msg = null;

	GetStation(IpAddr, function (Station)
	{
		if (!Station)
		{
			Msg = 'Serial Scan: User ' + user + ' failed attempt to validate serial number from an unknown station';
			WriteToFile(Msg);
			return res.status(200).send({NoStation:true});
		}
		else
		{
			if(!Station.currentTote)
			{
				return res.status(200).send({ScanMode:false});
			}

			//PickListModel.findOne({'orders.items.serials': ScannedSerial}, function (error, MongoSerial)
                        PickListModel.find({'orders.items.serials': {$regex: ScannedSerial, $options:'i'}}, function (error, MongoSerial)
			{
                           var SerialGood = false;
                           var FoundIndex = null;
				if(error || !MongoSerial || MongoSerial.length <= 0)
				{
                                  SerialGood = true;
                                }
                                else
                                {
                                  var x = 0;
                                  while(x < MongoSerial.length)
                                  {
                                    if(MongoSerial[x].Status != 'RETURNED')
                                    {
                                      FoundIndex = x;
                                      break;
                                    }
                                    x++;
                                  }

                                  if(FoundIndex == null)
                                  {
                                      Msg = 'Serial number ' + ScannedSerial + ' Found in a returned order so its good to be re-despatched';
                                      WriteToFile(Msg);
                                      SerialGood = true;
                                  }
                                }

                                if(SerialGood)
                                {
					var url = serialUri + ScannedSerial

					apexLib.rest.get(url, null, function (response)
					{
						if (response instanceof Error)
						{
							AmarmMod.addSubSystemCommsError()
						  	Msg = 'Serial Scan: Error while trying to connect to WMS for serial validation | ' + response;
							WriteToFile(Msg);
							res.status(405).send('Error: Serial validation service is down, please call supervisor');
							return;
						}

						var dbResult = response;
						if(dbResult && dbResult != null)
						{
							var serial = dbResult.serial;
							var sku = dbResult.SKU;
							var serialStatus = dbResult.serialStatus;
							var despatched = dbResult.dispatched;
							var Quantity = 1;

							if (!serial || serial == '')
							{
								Msg = 'Serial Scan: Station: ' + Station.stationNumber + ' | User ' + user + ': serial validation failed in WMS for scanned serial ' + ScannedSerial + ' [WMS returned]: ' + serial;
								WriteToFile(Msg);
								res.status(405).send('Scanned serial number ' + ScannedSerial + ' could not be validated');
								return;
							}

							/*if (ScannedSerial != serial && (("892700000" + ScannedSerial) != serial))
							{
								Quantity = dbResult.serialCount;
								Msg = 'Serial Scan: User ' + user + ': mismatch between scanned serial number and result returned from WMS ->[scanned]: ' + ScannedSerial + ' [WMS returned]:' + serial;
								WriteToFile(Msg);
								res.status(405).send('Scanned serial number could not be validated');
								return;
							}*/

							if ((ScannedSerial == serial) || (("892700000" + ScannedSerial) == serial) || (("892700008" + ScannedSerial) == serial))
							{
								Msg = 'User: ' + user + ' scanned serial: ' + ScannedSerial + ' | WMS returned: ' + serial + ' at station: ' + Station.stationNumber;
								WriteToFile(Msg);
							}
							else
							{
								Quantity = dbResult.serialCount;
								Msg = 'Serial Scan: Station: ' + Station.stationNumber + ' | User ' + user + ': mismatch between scanned serial number and result returned from WMS ->[scanned]: ' + ScannedSerial + ' [WMS returned]:' + serial;
								WriteToFile(Msg);
								res.status(405).send('Scanned serial number ' + ScannedSerial + ' could not be validated');
								return;
							}

							if(dbResult.serialCount)
							{
								Quantity = dbResult.serialCount;
								Msg = 'Serial Scan: User ' + user + ' scanned serial ' + serial + ' at station ' + Station.stationNumber + ' serial quantity ' + Quantity;
								WriteToFile(Msg);
							}

							if(despatched == 1)
							{
								Msg = 'Serial Scan: User ' + user + ' failed attempt to despatch an already despached serial ' + serial + ' at station ' + Station.stationNumber;
								WriteToFile(Msg);
								res.status(405).send('Scanned serial number ' + serial + ' is already despatched');
								return;
							}
							else
							{
								if(serialStatus === 'BROKEN')
								{
									Msg = 'Serial Scan: User ' + user + ' failed attempt to despatch a serial ' + serial + ' with serial status ' + serialStatus + ' at station ' + Station.stationNumber;
									WriteToFile(Msg);
									res.status(405).send('Scanned container serial number ' + serial + ' is no longer a fullpack');
									return;
								}
								else
								{
									UpdateSerialToPickList(user, Station, sku, Quantity, serial, function(Details)
									{
										if(Details.error)
										{
											res.status(405).send(Details.error);
										}
										else
										{
											res.status(200).send(Details);
										}
									});
								}
							}
						}
						else
						{
							CheckIfNonSerialised(ScannedSerial, Station.currentTote, Station.stationNumber, user, function(Details)
							{
								if(Details.error)
								{
									res.status(405).send(Details.error);
								}
								else
								{
									CreateResponseData(Details.PickList, Station.stationNumber, Details.index, user, function(Details)
									{
										if(Details.error)
										{
											return res.status(405).send(Details.error);
										}
										else
											return res.status(200).send(Details);
									});
								}
							});
						}
					});
				}
				else
				{
					if(MongoSerial[FoundIndex].toteID == Station.currentTote)
					{
						Msg = 'Serial Scan: Station: ' + Station.stationNumber + ' | User ' + user + ' scanned serial number ' + ScannedSerial + ' which is already scanned into tote ' + Station.currentTote;
						WriteToFile(Msg);
						res.status(405).send('Scanned serial number ' + ScannedSerial + ' has already been scanned');
					}
					else
					{
						Msg = 'Serial Scan: Station: ' + Station.stationNumber + ' | User ' + user + ' scanned serial number ' + ScannedSerial + ' which is already despatched to order ' + MongoSerial[FoundIndex].orders[0].orderID;
						WriteToFile(Msg);
						res.status(405).send('Scanned serial number ' + ScannedSerial + ' is already despatched');
					}
				}
			});
		}
	});
});

/*=================== Functions =================================================*/
function CalculateCartonSize(pickList, CurrentOrderToProcess, callback)
{
	Params.find({}, function(err, Parameters)
	{
		if(err || !Parameters || Parameters.length <= 0)
		{
			return callback(3);
		}
		else
		{
			if(Parameters[0].weightSettings && Parameters[0].weightSettings.length > 0)
			{
				var x = 0;
				var total = 0;
				while(x < pickList.orders[CurrentOrderToProcess].items.length)
				{
					if(pickList.orders[CurrentOrderToProcess].items[x].picked)
					{
						total += (pickList.orders[CurrentOrderToProcess].items[x].pickQty *
						          pickList.orders[CurrentOrderToProcess].items[x].eachVolume);
					}
					x++;
				}

				var v = 0;
				var BestFit = null;
				while(v < Parameters[0].weightSettings.length)
				{
					if(total < Parameters[0].weightSettings[v].volumeCapacity && Parameters[0].weightSettings[v].isCarton)
					{
						if(!BestFit)
						{
							BestFit = v;
						}
						else
						{
							if(Parameters[0].weightSettings[BestFit].volumeCapacity > Parameters[0].weightSettings[v].volumeCapacity)
							{
								BestFit = v;
							}
						}
					}
					v++;
				}

				if(BestFit)
				{
					return callback(Parameters[0].weightSettings[BestFit].StockSize);
				}
				else
				{
					return callback(3);
				}
			}
			else
			{
				return callback(3);
			}
		}
	});

} /* CalculateCartonSize */

function CheckIfNonSerialised(Barcode, Tote, Station, user, callback)
{
	PickListModel.findOne({'toteID': Tote, 'orders.items.barcode': Barcode}, function (error, PickList)
	{
		if (error || !PickList)
		{
			Msg = 'Serial Scan: User ' + user + ' scanned serial number ' + Barcode + ' and could not be validated in WMS at station ' + Station;
			WriteToFile(Msg);
			return callback({error:'Scanned serial number ' + Barcode + ' could not be validated'});
		}
		else
		{
		  var CurrentOrderToProcess = GetNextOrderToProcess(PickList);
		  var x = 0;
		  var itemFound = false;
		  var RoomforOneMore = false;
		  var Serialised = false;

		  while(x < PickList.orders[CurrentOrderToProcess].items.length)
		  {
				itemFound = false;
				if(PickList.orders[CurrentOrderToProcess].items[x].barcode == Barcode)
				{
					if(!PickList.orders[CurrentOrderToProcess].items[x].serialised)
					{
						itemFound = true;

						var Qtypacked = PickList.orders[CurrentOrderToProcess].items[x].qtyPacked;
						if(Qtypacked == null)
						{
							Qtypacked = 0;
						}

						var QtyRem = pickQty - Qtypacked;
						if(QtyRem > 0)
						{
							RoomforOneMore = true;
							break;
						}
					}
					else
					{
						Serialised = true;
						break;
					}
				}
				x++;
			}

			if(x < PickList.orders[CurrentOrderToProcess].items.length)
			{
				if(Serialised)
				{
					Msg = 'Non Serialised Scan: User ' + user + ' scanned sku barcode ' + Barcode + ' which is not for a non-serialised product';
					WriteToFile(Msg);
					return callback({error:'Scanned product barcode ' + Barcode + ' is not for a non-serialised product'});
				}
				else
				{
					if(itemFound && RoomforOneMore)
					{
						var Qtypacked = PickList.orders[CurrentOrderToProcess].items[x].qtyPacked;
						if(Qtypacked == null)
						{
							Qtypacked = 0;
						}

						Qtypacked++;
						PickList.orders[CurrentOrderToProcess].items[x].qtyPacked = Qtypacked;

						Msg = 'Non Serialised Scan: Station: ' + Station + ' | User ' + user + ' added 1 to non-serialised product ' + PickList.orders[CurrentOrderToProcess].items[x].itemCode + ' in order ' + PickList.orders[CurrentOrderToProcess].orderID;
						WriteToFile(Msg);

						if(QtyPacked == PickList.orders[CurrentOrderToProcess].items[x].pickQty){
							PickList.orders[CurrentOrderToProcess].items[x].packedBy = Data.User;
							PickList.orders[CurrentOrderToProcess].items[x].packed = true;
						}

						var AllItemsPacked = true;
						x = 0;
						while(x < PickList.orders[CurrentOrderToProcess].items.length){
							if(PickList.orders[CurrentOrderToProcess].items[x].packed == false){
								AllItemsPacked = false;
								break;
							}
							y++;
						}

						if(AllItemsPacked){
							PickList.orders[CurrentOrderToProcess].packed = true;
							Msg = "Station: " + Station + " | Order " + PickList.orders[CurrentOrderToProcess].orderID + " in tote " + PickList.toteID + " is fully packed";
							WriteToFile(Msg);
						}

						PickList.save(function(err, savedDoc)
						{
							if(!err && savedDoc){
							  return callback({PickList:savedDoc, index:CurrentOrderToProcess});
							} else {
								return callback({error:'Failed to commit scanned data'});
							}
						});
					}
					else
					{
						Msg = 'Non Serialised Scan: Station: ' + Station + ' | User ' + user + ' scanned sku barcode ' + Barcode + ' which is fully packed';
						WriteToFile(Msg);
						return callback({error:'Already packed enough stock of this product to the order'});
					}
				}
			}
			else
			{
				Msg = 'Non Serialised Scan: Station: ' + Station + ' | User ' + user + ' scanned sku barcode ' + Barcode + ' which is not valid for any item in order ' + PickList.orders[CurrentOrderToProcess].orderID;
				WriteToFile(Msg);
				return callback({error:'Scanned product barcode ' + Barcode + ' is not valid for this order'});
			}
		}
	});
} /* CheckIfNonSerialised */

function ValidateScannedTote(PickList, Station)
{
	var valid = null;
	if(PickList.toteID.length == 7)
	{
		if(parseInt(PickList.toteID[0]) == 0 || parseInt(PickList.toteID[0]) == 1)
		{
			if(PickList.toteInUse && Station != parseInt(PickList.toteInUseBy))
			{
				valid = 'Tote in use at Station ' + PickList.toteInUseBy;
			}
			else
			{
				if(PickList.orders.length > 0)
				{
					var index = 0;
					while(index < PickList.orders.length)
					{
						var itemIndex = 0;
						while(itemIndex < PickList.orders[index].items.length)
						{
							if(PickList.orders[index].items[itemIndex].picked == false)
							{
								valid = 'Not all items in Tote are picked';
								break;
							}

							if(PickList.orders[index].items[itemIndex].pickQty != PickList.orders[index].items[itemIndex].qty)
							{
								valid = 'Quantity mismatch between item quantity and picked quantity for sku ' + PickList.orders[index].items[itemIndex].sku;
								break;
							}
							itemIndex++;
						}

						if(valid != null)
							break;

						index++;
					}

					var PackedOrderCount = 0;
					index = 0;
					while(index < PickList.orders.length)
					{
						if(PickList.orders[index].packComplete)
						{
							PackedOrderCount++;
						}

						index++;
					}

					if(PackedOrderCount >= PickList.orders.length)
					{
						valid = 'All orders in tote are already packed';
					}
				}
				else
				{
					valid = 'PickList for this tote does not contain any orders';
				}
			}
		}
		else
		{
			valid = 'Not a tote';
		}
	}
	else
	{
		valid = 'Invalid Id for Tote';
	}

	return valid;
} /* ValidateScannedTote */

function UpdateSerialToPickList(user, Station, sku, Quantity, serial, callback)
{
	var Tote = Station.currentTote;
	var Msg = null;

	PickListModel.findOne({toteID: Tote}, function (error, PickList)
	{
		if (error || !PickList)
		{
			Msg = 'Serial scan: Cannot find picklist for tote associated with station ' + Station.stationNumber;
			WriteToFile(Msg);
			return callback({error:"Cannot find a picklist for tote " + Tote});
		}
		else
		{
			var CurrentOrderToProcess = GetNextOrderToProcess(PickList);
			UpdateSerial(PickList, user, sku, Quantity, serial, Station.stationNumber, CurrentOrderToProcess, function(Details)
			{
				return callback(Details);
			});
		}
	});
} /* UpdateSerialToPickList */

function GetStation(IpAddr, callback)
{
	PackStationModel.findOne({stationIP: IpAddr}, function (error, doc)
	{
		if(error || !doc)
		{
			return callback(null);
		}
		else
		{
			var now = new Date();
			doc.updated_at = now;
			doc.active = true;
			doc.save(function(error, savedDoc)
			{
				return callback(savedDoc);
			});
		}
	});
} /* GetStation */

function UpdateSerial(PickList, user, sku, Quantity, serial, Station, CurrentOrderToProcess, callback)
{
	var OriginalQty = Quantity;
	var OrdIndex = CurrentOrderToProcess;
	var QtyPacked = null;
	var Msg = null;
	var itemIndex = 0;

	while(itemIndex < PickList.orders[OrdIndex].items.length)
	{
		var QtyPacked = PickList.orders[OrdIndex].items[itemIndex].qtyPacked;
		if(QtyPacked == null)
		{
			QtyPacked = 0;
		}

		var QtyRem = PickList.orders[OrdIndex].items[itemIndex].pickQty - QtyPacked;

		if(sku == PickList.orders[OrdIndex].items[itemIndex].sku && QtyRem >= Quantity && !PickList.orders[OrdIndex].items[itemIndex].packed)
		{
			break;
		}
		itemIndex++;
	}

	if(itemIndex < PickList.orders[OrdIndex].items.length)
	{
		QtyPacked += Quantity;
		PickList.orders[OrdIndex].items[itemIndex].qtyPacked = QtyPacked;
		PickList.orders[OrdIndex].items[itemIndex].serials.push(serial);

		Msg = "Station: " + Station + " | User " + user + " added serial number " + serial + " from tote " + PickList.toteID + " to order " + PickList.orders[OrdIndex].orderID;
		WriteToFile(Msg);

		if(QtyPacked == PickList.orders[OrdIndex].items[itemIndex].pickQty)
		{
			PickList.orders[OrdIndex].items[itemIndex].packedBy = user;
			PickList.orders[OrdIndex].items[itemIndex].packed = true;
		}
		Quantity = 0;

		var AllItemsPacked = true;
		itemIndex = 0;
		while(itemIndex < PickList.orders[OrdIndex].items.length)
		{
			if(PickList.orders[OrdIndex].items[itemIndex].packed == false)
			{
				AllItemsPacked = false;
				break;
			}
			itemIndex++;
		}

		if(AllItemsPacked)
		{
			PickList.orders[OrdIndex].packed = true;
			Msg = "Station: " + Station + " | Order " + PickList.orders[OrdIndex].orderID + " in tote " + PickList.toteID + " is fully packed";
			WriteToFile(Msg);
		}

		PickList.save(function(error, savedData)
		{
			if(error)
			{
				Msg = "Error updating serial number " + serial + " to picklist in tote " + PickList.toteID;
				WriteToFile(Msg);
				return callback({error: Msg});
			}
			else
			{
				CreateResponseData(savedData, Station, OrdIndex, user, function(Details)
				{
					return callback(Details);
				});
			}
		});
	}
	else
	{
		if(OriginalQty == Quantity)
		{
			Msg = "Scanned serial number " + serial + " is not part of this order";
			WriteToFile(Msg);
			return callback({error:Msg});
		}
		else
		{
			Msg = "Serial number quantity supplied is greater than what is required";
			WriteToFile(Msg);
			return callback({error:Msg});
		}
	}
} /* UpdateSerial */

function ResetPicklistForTote(PickList, user, Station, callback)
{
	DeAssociatePackedOrders(PickList, user, Station, function(Details)
	{
		if(Details.error)
		{
			return callback(Details);
		}
		else
		{
			var x = 0;
			while(x < Details.orders.length)
			{
				var y = 0;
				while(y < Details.orders[x].items.length)
				{
					Details.orders[x].items[y].packedBy = null;
					Details.orders[x].items[y].serials = [];
					Details.orders[x].items[y].packed = false;
					Details.orders[x].items[y].qtyPacked = null;
					y++;
				}

				Details.orders[x].packed = false;
				Details.orders[x].picklistWeight = null;
				Details.orders[x].toteNumber = null;
				Details.orders[x].cartonID = null;
				x++;
			}

			Details.ShipUpdateRequired = false;
			Details.PickUpdateRequired = false;
			Details.PackUpdateRequired = false;
			Details.Status = 'PICKED';
			Details.PackedDate = null;
			Details.toteInUse = false;
			Details.toteInUseBy = "";
			Details.transporterID = null;

			Details.save(function(error, DocSaved)
			{
				if(error || !DocSaved)
				{
				  Msg = "Failed to reset tote " + Details.toteID + " at station " + Station + ". Reason: " + error;
				  WriteToFile(Msg);
					return callback({error:Msg});
				}
				else
				{
					return callback({NoStation: false, ScanMode: false});
				}
			});
		}
	});
} /* ResetPicklistForTote */

function DeAssociatePackedOrders(PickList, user, Station, callback)
{
	var OrdIndex = 0;
	var FailedToUnhook = false;

	while(OrdIndex < PickList.orders.length)
	{
		if(PickList.orders[OrdIndex].packComplete)
		{
			var ItemIndex = 0;

			while(ItemIndex < PickList.orders[OrdIndex].items.length)
			{
				if(PickList.orders[OrdIndex].items[ItemIndex].packed == false ||
				   (PickList.orders[OrdIndex].items[ItemIndex].pickQty != PickList.orders[OrdIndex].items[ItemIndex].qtyPacked))
				{
					FailedToUnhook = true;
					break;
				}
				ItemIndex++;
			}

			if(FailedToUnhook)
			{
				break;
			}
		}
		OrdIndex++;
	}

	if(FailedToUnhook)
	{
		if(PickList.orders.length > 1)
		{
		  return callback({error:'Failed to unhook order ' + PickList.orders[OrdIndex].orderID + ' from Master Picklist in Tote ' + PickList.toteID});
		}
	}
	else
	{
		if(PickList.orders.length > 1)
		{
                   CalculateTransporterWeight(PickList, function(TransporterDetails)
                   {
			UnhookPackedOrders(PickList, 0, user, Station, TransporterDetails, function(Details)
			{
				return callback(Details);
			});
                   });
		}
		else
		{
			return callback(PickList);
		}
	}
} /* DeAssociatePackedOrders */

function UnhookPackedOrders(PickList, Ordindex, user, Station, TransporterDetails, callback)
{
	var Msg = null;

	if(Ordindex < PickList.orders.length)
	{
		if(PickList.orders[Ordindex].packComplete)
		{
			var Orders = PickList.orders[Ordindex];

                        var now = new Date();
                        var OrderDate = PickList.CreateDate;

                        if(!OrderDate)
                        {
                           OrderDate =  moment(now).format('YYYY-MM-DD HH:mm');
                        }

			var Subpicklist = new PickListModel
			({
					toteID: PickList.orders[Ordindex].cartonID,
					transporterID: PickList.orders[Ordindex].transporterID,
					destination: PickList.destination,
					packType: PickList.packType,
					toteInUse: false,
					toteInUseBy: PickList.toteInUseBy,
					orders: Orders,
					Status: 'PACKED',
					PackUpdateRequired: true,
					ShipUpdateRequired: false,
					PackedDate: moment(now).format('YYYY-MM-DD HH:mm'),
					containerLocation: null,
					PickListType: null,
					FromTote: PickList.toteID,
                                        CreateDate: OrderDate
			})

                        if(TransporterDetails.TotalWeight && Orders.useTransporter)
                        {
			  Subpicklist.orders[0].transporterWeight = TransporterDetails.TotalWeight;
                          Subpicklist.orders[0].minWeightLimit = TransporterDetails.MinTolerance;
                          Subpicklist.orders[0].maxWeightLimit = TransporterDetails.MaxTolerance;
                          Msg = 'Carton ' + PickList.orders[Ordindex].cartonID + ' Transporter weight ' + TransporterDetails.TotalWeight + ' | min weight limit ' + TransporterDetails.MinTolerance + ' | max weight limit ' + TransporterDetails.MaxTolerance;
                          WriteToFile(Msg);
			}

			Subpicklist.save(function (error, doc)
			{
				if (error)
				{
				  Msg = 'UnhookPackedOrders: Failed to unhook order ' + Subpicklist.orders[0].orderID + ' | ' + error ;
				  WriteToFile(Msg);

				  Ordindex++;
				  UnhookPackedOrders(PickList, Ordindex, user, Station, TransporterDetails, callback);
				}
				else
				{
				  Msg = 'UnhookPackedOrders: Successfully unhooked order ' + doc.orders[0].orderID + ' from tote ' + doc.FromTote + ' at station ' + Station + ' | user: ' + user;
				  WriteToFile(Msg);

				  PickList.orders.splice(Ordindex, 1);
				  PickList.save(function(error1, SavedDoc)
				  {
				     UnhookPackedOrders(SavedDoc, Ordindex, user, Station, TransporterDetails, callback);
				  });
				}

			});
	  }
	  else
	  {
			Ordindex++;
			UnhookPackedOrders(PickList, Ordindex, user, Station, TransporterDetails, callback);
		}
	}
	else
	{
		return callback(PickList);
	}
} /* UnhookPackedOrders */

function CreateResponseData(savedDoc, Station, CurrentOrderToProcess, user, callback)
{
	PickListModel.count({'orders.orderID':savedDoc.orders[CurrentOrderToProcess].orderID}, function( err, TotesPerOrder)
	{
		var NumOrders = savedDoc.orders.length;

		var toteTypeDetailName = GetToteType(NumOrders, TotesPerOrder);

		GenerateitemsToPack(savedDoc, CurrentOrderToProcess, function(itemsToDisplay)
		{
			DetermineNextStepInProcess(savedDoc, CurrentOrderToProcess, function(Details)
			{
				if(Details.error)
				{
					return callback(Details);
				}
				else
				{
					if(Details.Next == 'DONE')
					{
						if(savedDoc.orders.length > 1)
						{
							savedDoc.orders[CurrentOrderToProcess].packComplete = true;
							savedDoc.save(function(err, savedDoc)
							{
								CurrentOrderToProcess = GetNextOrderToProcess(savedDoc);

								if(CurrentOrderToProcess == null)
								{
                                    CalculateTransporterWeight(savedDoc, function(TransporterDetails)
                                    {
									UnhookPackedOrders(savedDoc, 0, user, Station, TransporterDetails, function(savedDoc)
									{
										if(savedDoc.orders.length > 0)
										{
											return callback({error:'Failed to un-associate all sub-orders for consolidated tote ' + savedDoc.toteID});
										}
										else
										{
											DestroyMasterPickList(savedDoc, Station, function()
											{
												ClearToteFromStation(Station, function(Details)
												{
													return callback(Details);
												});
											});
										}
									});
                                                                    });
								}
								else
								{
									CreateResponseData(savedDoc, Station, CurrentOrderToProcess, user, callback);
								}
							});
						}
						else
						{
							if(savedDoc.orders.length == 1)
							{
								savedDoc.orders[CurrentOrderToProcess].packComplete = true;
								savedDoc.save(function(err, savedDoc){
                                                                	CalculateTransporterWeight(savedDoc, function(TransporterDetails){
									PackCompleteOrder(savedDoc, CurrentOrderToProcess, Station, TransporterDetails, function(Details)
									{
										return callback(Details);
									});
								   })
                                                                })
							}
							else
							{
								return callback({error:'No orders found in tote ' + savedDoc.toteID});
							}
						}
					}
					else
					{
						// console.log('***************** | ' + Details.Next + ' | *********************')
						var Details =
						{
							signedInStation: Station,
							toteTypeDetailName: toteTypeDetailName,
							scannedTote: savedDoc.toteID,
							useCartonSize: savedDoc.orders[CurrentOrderToProcess].cartonSize,
							scannedCartonIdDisplay: savedDoc.orders[CurrentOrderToProcess].cartonID,
							cartonSizeUsed: savedDoc.orders[CurrentOrderToProcess].cartonSize,
							currentOrderId: savedDoc.orders[CurrentOrderToProcess].orderID,
							currentOrderInToteProcess: (CurrentOrderToProcess+1),
							totalOrdersInToteToProcess: NumOrders,
							itemsToDisplay: itemsToDisplay,
							ToteMode:'MAN',
							Next: Details.Next
						}

						return callback(Details);
					}
				}
			});
		});
	});
} /* CreateResponseData */

function CalculateTransporterWeight(savedDoc, callback)
{
  var Msg = null;
  Params.find({}, function(err, Parameters)
  {
    if(err || !Parameters || Parameters.length <= 0)
    {
       Msg = "Failed to find parameters in the system ";
       WriteToFile(Msg);
       return callback({Error:Msg});
    }
    else
    {
       var CartonPrefix = 7;

       if(Parameters[0].weightSettings && Parameters[0].weightSettings.length > 0)
       {
          var paramIndex = 0;
          while(paramIndex < Parameters[0].weightSettings.length)
          {
             if(Parameters[0].weightSettings[paramIndex].StockSize == CartonPrefix)
             {
                break;
             }
             paramIndex++;
          }

          if(paramIndex < Parameters[0].weightSettings.length /*&& savedDoc.orders[0].cartonID.substr(0,1) == '3'*/)
          {
             var TotalWeight = Parameters[0].weightSettings[paramIndex].weight;

             var x = 0;
             while(x < savedDoc.orders.length)
             {
		Msg = savedDoc.toteID + " CARTONID: " + savedDoc.orders[x].cartonID + '**********************************************************************' + savedDoc.orders[x].packComplete
		WriteToFile(Msg);
               if(savedDoc.orders[x].packComplete && parseInt(savedDoc.orders[x].cartonID[0]) == 3)
               {
		 console.log('*****************************************: ' + savedDoc.orders[x].cartonID + '*******************************************')
		 Msg = "CARTONID: " + savedDoc.orders[x].cartonID
		 WriteToFile(Msg);
                 TotalWeight += savedDoc.orders[x].picklistWeight;
               }
               x++;
             }

             var MinTolerance = TotalWeight - Parameters[0].weightSettings[paramIndex].LowerTolerance;
             var MaxTolerance = TotalWeight + Parameters[0].weightSettings[paramIndex].UpperTolerance;

             var TransporterWeights =
             {
               MinTolerance: MinTolerance,
               MaxTolerance: MaxTolerance,
               TotalWeight: TotalWeight
             }

             return callback(TransporterWeights);
          }
          else
          {
            Msg = "Failed to find Transporter weightSettings ";
            WriteToFile(Msg);
            return callback({Error:Msg});
          }
       }
       else
       {
          Msg = "Failed to find weightSettings parameter in the system ";
          WriteToFile(Msg);
          return callback({Error:Msg});
       }
    }
  });

} /* CalculateTransporterWeight */

function DestroyMasterPickList(savedDoc, Station, callback)
{
	var Msg = null;
	var toteID = savedDoc.toteID;
  PickListModel.remove({'toteID': savedDoc.toteID}, function(err)
  {
		Msg = 'WCS Master Picklist: deleted Master picklist in tote ' + toteID + ' at station ' + Station;
		WriteToFile(Msg)
		return callback();
	});
} /* DestroyMasterPickList */

function PackCompleteOrder(savedDoc, CurrentOrderToProcess, Station, TransporterDetails, callback)
{

	if(TransporterDetails.TotalWeight && savedDoc.orders[0].useTransporter){
		savedDoc.orders[0].transporterWeight = TransporterDetails.TotalWeight;
		savedDoc.orders[0].minWeightLimit = TransporterDetails.MinTolerance;
		savedDoc.orders[0].maxWeightLimit = TransporterDetails.MaxTolerance;
		Msg = 'Carton ' + savedDoc.orders[0].cartonID + ' Transporter weight ' + TransporterDetails.TotalWeight + ' | min weight limit ' + TransporterDetails.MinTolerance + ' | max weight limit ' + TransporterDetails.MaxTolerance;
		WriteToFile(Msg);
	}

  	var now = new Date();
	savedDoc.FromTote = savedDoc.toteID;
	savedDoc.toteID = savedDoc.orders[CurrentOrderToProcess].cartonID;
	savedDoc.PackedDate = moment(now).format('YYYY-MM-DD HH:mm');
	savedDoc.orders[CurrentOrderToProcess].packComplete = true;
	savedDoc.ShipUpdateRequired = false;
	savedDoc.PackUpdateRequired = true;
	savedDoc.Status = 'PACKED';
	//savedDoc.toteInUseBy = null;
	savedDoc.toteInUse = false;
	savedDoc.PickListType = null;
	savedDoc.containerLocation = null;
	savedDoc.transporterID = savedDoc.orders[CurrentOrderToProcess].transporterID;
	//savedDoc.orders[CurrentOrderToProcess].transporterID = null;
	savedDoc.save(function(err, savedDoc)
	{
		ClearToteFromStation(Station, function(Details)
		{
			return callback(Details);
		});
	});
} /* PackCompleteOrder */

function ClearToteFromStation(Station, callback)
{
	PackStationModel.findOne({stationNumber: Station}, function (error, doc)
	{
		if(error || !doc)
		{
			return callback({ScanMode:false});
		}
		else
		{
			var now = new Date();
			doc.updated_at = now;
			doc.currentTote = null;
			doc.currentTransporter = null;
			doc.save(function(error, doc)
			{
				return callback({ScanMode:false});
			});
		}
	});
} /* ClearToteFromStation */

function GetNextOrderToProcess(PickList)
{
	var x = 0;
	while(x < PickList.orders.length)
	{
		if(PickList.orders[x].packComplete == false)
		{
			break;
		}
		x++;
	}

	if(x < PickList.orders.length)
	{
		return (x);
	}
	else
	  return null;
} /* GetNextOrderToProcesse */

function SendDocsLabelPrint(PickList, CurrentOrderToProcess, UseTransporter, OrderData, callback)
{
	var Next = '';
	var OrdItems = OrderData.OrdItems;
	var Cartons  = OrderData.Cartons;

	var CartonNo = PickList.orders[CurrentOrderToProcess].toteNumber;
	var NoOfCartons = PickList.orders[CurrentOrderToProcess].numberOfTotes;

	var First = false;
	var Last = false;
	var Expect = '';
	if(CartonNo == 1 && CartonNo == NoOfCartons)
	{
		First = true;
		Last = true;
		Next = 'DOC';
		Expect = 'Documents/label';
	}
	else if(CartonNo == 1 && CartonNo != NoOfCartons)
	{
		First = true;
		Last = false;
		Expect = 'label';
		if(UseTransporter)
		{
			Next = 'TRANS';
		}
		else
		{
			Next = 'DONE';
		}
	}
	else if(CartonNo > 1 && CartonNo == NoOfCartons)
	{
		First = false;
		Last = true;
		Next = 'DOC';
		Expect = 'Documents/label';
	}
	else if(CartonNo > 1 && CartonNo != NoOfCartons)
	{
		First = false;
		Last = false;
		Expect = 'label';
		if(UseTransporter)
		{
			Next = 'TRANS';
		}
		else
		{
			Next = 'DONE';
		}
	}

	var Station = parseInt(PickList.toteInUseBy);
	PackStationModel.findOne({stationNumber: Station}, function (error, doc)
	{
		if(error || !doc)
		{
			return callback({error:"Error while finding station for Tote " + PickList.toteID});
		}
		else
		{
			var PrintData =
			{
				orderId: PickList.orders[CurrentOrderToProcess].orderID,
				printerName: doc.desktopPrinterQName,
				labelprinterName: doc.labelPrinterQName,
				items: OrdItems,
				ErpItems: PickList.orders[CurrentOrderToProcess].items,
				cartonId: PickList.orders[CurrentOrderToProcess].cartonID,
				courierId: PickList.orders[CurrentOrderToProcess].courierID,
				station: doc.stationNumber,
				CartonNo: CartonNo,
				NoOfCartons: NoOfCartons,
				CartionWeight: PickList.orders[CurrentOrderToProcess].picklistWeight,
				FirstCarton: First,
				LastCarton: Last,
				orderType: PickList.orders[CurrentOrderToProcess].orderType,
				Cartons: Cartons
			}

			Pubclient.publish('PACKING_' + doc.stationNumber, JSON.stringify(PrintData), function(err)
			{
				 if(err)
				 {
					 return callback({error:"Error printing " + Expect + " for order " + PickList.orders[CurrentOrderToProcess].orderID});
				 }
				 else
				 {
					 PickList.orders[CurrentOrderToProcess].printed = true
					 PickList.save(function(err, savedDoc)
					 {
						 return callback({Next:Next, PickList:savedDoc});
					 });
				 }
			});
		}
	});

} /* SendDocsLabelPrint */

function DetermineCartonNo(savedDoc, CurrentOrderToProcess, callback)
{
	var Msg = null;
	if(savedDoc.orders[CurrentOrderToProcess].toteNumber)
	{
		return callback(savedDoc);
	}
	else
	{
		PickListModel.find({'orders.orderID': savedDoc.orders[CurrentOrderToProcess].orderID}, function(err, PickLists)
		{
			if(err || !PickLists || PickLists.length <= 0)
			{
				Msg = "Error finding all picklists for order " + savedDoc.orders[CurrentOrderToProcess].orderID + " to calculate carton number";
				WriteToFile(Msg);
				return callback({error:"Problem encountered while calculating the order carton number - please call supervisor"});
			}
			else
			{
				var Total = 0;

				var t = 0;
				while(t < PickLists.length)
				{
					var n = 0;
					while(n < PickLists[t].orders.length)
					{
					if(PickLists[t].orders[n].orderID == savedDoc.orders[CurrentOrderToProcess].orderID)
					{
						Total++;
					}
					n++;
					}
					t++;
				}

				Msg = "Order " + savedDoc.orders[CurrentOrderToProcess].orderID + " has " + Total + " cartons";
				WriteToFile(Msg);

				if(Total > 1)
				{
					var ActualOrderCount = 1;
					PL = 0;
					while(PL < PickLists.length)
					{
						var OrdIndex = 0;

						while(OrdIndex < PickLists[PL].orders.length)
						{
							if((PickLists[PL].orders[OrdIndex].orderID == savedDoc.orders[CurrentOrderToProcess].orderID))
							{
								if(PickLists[PL].orders[OrdIndex].numberOfTotes > ActualOrderCount)
								{
									ActualOrderCount = PickLists[PL].orders[OrdIndex].numberOfTotes;
								}
							}
							OrdIndex++;
						}
						PL++;
					}

					if(ActualOrderCount > 1)
					{
						savedDoc.orders[CurrentOrderToProcess].numberOfTotes = ActualOrderCount;
						//Total = ActualOrderCount;
					}
					else
					{
						savedDoc.orders[CurrentOrderToProcess].numberOfTotes = Total;
					}

					PL = 0;
					var highestNum = 1;
					while(PL < PickLists.length)
					{
						var OrdIndex = 0;

						while(OrdIndex < PickLists[PL].orders.length)
						{
							if(PickLists[PL].orders[OrdIndex].orderID == savedDoc.orders[CurrentOrderToProcess].orderID &&
								 PickLists[PL].orders[OrdIndex].pickListID != savedDoc.orders[CurrentOrderToProcess].pickListID)
							{
								if(PickLists[PL].orders[OrdIndex].toteNumber)
								{
									if(PickLists[PL].orders[OrdIndex].toteNumber == highestNum)
									{
										highestNum++;
										PL = -1;
										break;
									}
								}
							}
							OrdIndex++;
						}
						PL++;
					}

					if(highestNum <= Total)
					{
						savedDoc.orders[CurrentOrderToProcess].toteNumber = highestNum;
						savedDoc.save(function(err, Data)
						{
							Msg = "Order " + Data.orders[CurrentOrderToProcess].orderID + " | cartonID " +
										Data.orders[CurrentOrderToProcess].cartonID + " | carton number " + Data.orders[CurrentOrderToProcess].toteNumber +
										" of " + Data.orders[CurrentOrderToProcess].numberOfTotes + " was scanned on station " + Data.toteInUseBy;
							WriteToFile(Msg);

							return callback(Data);
						});
					}
					else
					{
						Msg = "Order " + savedDoc.orders[CurrentOrderToProcess].orderID + " | cartonID " +
									savedDoc.orders[CurrentOrderToProcess].cartonID + " encountered a problem while calculating carton number. [calculated number = " +
									highestNum + " of " + Total + " cartons]";
						WriteToFile(Msg);
						return callback({error:"Problem encountered while calculating the order carton number - please call supervisor"});
					}
				}
				else
				{
					savedDoc.orders[CurrentOrderToProcess].toteNumber = Total;

					savedDoc.save(function(err, Data)
					{
						Msg = "Order " + Data.orders[CurrentOrderToProcess].orderID + " | cartonID " +
									Data.orders[CurrentOrderToProcess].cartonID + " | carton number " + Data.orders[CurrentOrderToProcess].toteNumber +
									" of " + Data.orders[CurrentOrderToProcess].numberOfTotes + " was scanned on station " + Data.toteInUseBy;
						WriteToFile(Msg);

						return callback(Data);
					});
				}
			}
		});
	}
} /* DetermineCartonNo */

function UpdateTotalToEachCarton(PickLists, index, Total, orderID, callback)
{
	if(index < PickLists.length)
	{
		var ordIndx = 0;
		while(ordIndx < PickLists[index].orders.length)
		{
			if(PickLists[index].orders[ordIndx].orderID == orderID)
			{
				PickLists[index].orders[ordIndx].numberOfTotes = Total;
			}
			ordIndx++;
		}

		PickLists[index].save(function(err, SavedData)
		{
			index++;
			UpdateTotalToEachCarton(PickLists, index, Total, orderID, callback);
		});
	}
	else
	{
		return callback();
	}
} /* UpdateTotalToEachCarton */

function DetermineNextStepInProcess(savedDoc, CurrentOrderToProcess, callback)
{
	if(!savedDoc.orders[CurrentOrderToProcess].packed)
	{
		return callback({Next: 'BARCODE/SERIAL'});
	}
  else if(savedDoc.orders[CurrentOrderToProcess].packed &&
		     (savedDoc.orders[CurrentOrderToProcess].cartonID == null ||
		      savedDoc.orders[CurrentOrderToProcess].cartonID == ''))
	{
		return callback({Next: 'CARTON'});
	}
	else if(!savedDoc.orders[CurrentOrderToProcess].printed)
  {
		ParamGetCartonDetails(savedDoc, CurrentOrderToProcess, function(CartonDetails)
		{
			if(CartonDetails.error)
			{
				return callback(CartonDetails);
			}
			else
			{
			     CalculateCartonWeight(savedDoc, CurrentOrderToProcess, CartonDetails.EmptyCartonWeight, function(CartonWeight)
                             {
				savedDoc.orders[CurrentOrderToProcess].picklistWeight = CartonWeight;
				savedDoc.orders[CurrentOrderToProcess].useTransporter = CartonDetails.UseTransporter;
                                savedDoc.orders[CurrentOrderToProcess].transporterWeight = null;
                                savedDoc.orders[CurrentOrderToProcess].minWeightLimit = (CartonWeight - CartonDetails.MinTolerance);
                                savedDoc.orders[CurrentOrderToProcess].maxWeightLimit = (CartonWeight + CartonDetails.MaxTolerance);
                                var Msg = "Carton " + savedDoc.orders[CurrentOrderToProcess].cartonID + " weight " + CartonWeight + " | min weight " + (CartonWeight - CartonDetails.MinTolerance) + " | max weight " + (CartonWeight + CartonDetails.MaxTolerance);
                                WriteToFile(Msg);
				savedDoc.save(function(err, savedDoc)
				{
					GetAllItemsAndCartonsForOrder(savedDoc, CurrentOrderToProcess, function(OrdDetails)
					{
						if(OrdDetails.error)
						{
							return callback(OrdDetails);
						}
						else
						{
							DetermineCartonNo(savedDoc, CurrentOrderToProcess, function(savedDoc)
							{
								if(savedDoc.error)
								{
									return callback(CartonDetails);
								}
								else
								{
									SendDocsLabelPrint(savedDoc, CurrentOrderToProcess, CartonDetails.UseTransporter, OrdDetails, function(Data)
									{
										return callback(Data);
									});
								}
							});
						}
					});
				});
                           });
			}
		});
	}
	else if(!savedDoc.orders[CurrentOrderToProcess].packComplete)
	{
		if(savedDoc.orders[CurrentOrderToProcess].numberOfTotes == savedDoc.orders[CurrentOrderToProcess].toteNumber)
		{
			if(!savedDoc.orders[CurrentOrderToProcess].orderVerified)
			{
				return callback({Next: 'DOC'});
			}
		}

		if(savedDoc.orders[CurrentOrderToProcess].useTransporter &&
		  (savedDoc.orders[CurrentOrderToProcess].transporterID == null || savedDoc.orders[CurrentOrderToProcess].transporterID == ''))
		{
			return callback({Next: 'TRANS'});
		}
		else
		{
			return callback({Next: 'DONE'});
		}
	}
} /* DetermineNextStepInProcess */

function GetAllItemsAndCartonsForOrder(savedDoc, CurrentOrderToProcess, callback)
{
	var Msg = null;
	PickListModel.find({'orders.orderID': savedDoc.orders[CurrentOrderToProcess].orderID}, function(err, PickLists)
	{
		if(err || !PickLists || PickLists.length <= 0)
		{
			Msg = "Error finding all picklists for order " + Data.orders[CurrentOrderToProcess].orderID;
			WriteToFile(Msg);
			return callback({error:"Problem encountered fetching all picklists for order - please call supervisor"});
		}
		else
		{
			var Cartons = [];
			var OrdItems = [];
			var PL = 0;
			while(PL < PickLists.length)
			{
				var OrdIndex = 0;

				while(OrdIndex < PickLists[PL].orders.length)
				{
					if(PickLists[PL].orders[OrdIndex].orderID == savedDoc.orders[CurrentOrderToProcess].orderID)
					{
						var itemIndx = 0;
						while(itemIndx < PickLists[PL].orders[OrdIndex].items.length)
						{
							OrdItems.push(PickLists[PL].orders[OrdIndex].items[itemIndx]);
							Cartons.push(PickLists[PL].orders[OrdIndex].cartonID);
							itemIndx++;
						}
					}
					OrdIndex++;
				}
				PL++;
			}

			var ReturnData =
			{
				Cartons: Cartons,
				OrdItems: OrdItems
			}

			return callback(ReturnData);
		}
	});
} /* GetAllItemsAndCartonsForOrder */

function CalculateCartonWeight(savedDoc, CurrentOrderToProcess, EmptyCartonWeight, callback)
{
	var x = 0;
	var Total = EmptyCartonWeight;
        while(x < savedDoc.orders[CurrentOrderToProcess].items.length)
        {
		var lineWeight = (savedDoc.orders[CurrentOrderToProcess].items[x].eachWeight *
		                  savedDoc.orders[CurrentOrderToProcess].items[x].qtyPacked);
		Total += lineWeight;
		x++;
	}

	return callback(Total);
} /* CalculateCartonWeight */

function ParamGetCartonDetails(PickList, CurrentOrderToProcess, callback)
{
	var Msg = null;
	Params.find({}, function(err, Parameters)
	{
		if(err || !Parameters || Parameters.length <= 0)
		{
			Msg = "Failed to find parameters in the system ";
			WriteToFile(Msg);
			return callback({error:Msg});
		}
		else
		{
			var CartonPrefix = PickList.orders[CurrentOrderToProcess].cartonID[0];

			if(Parameters[0].weightSettings && Parameters[0].weightSettings.length > 0)
			{
				var paramIndex = 0;
				while(paramIndex < Parameters[0].weightSettings.length)
				{
					if(Parameters[0].weightSettings[paramIndex].StockSize == parseInt(CartonPrefix))
					{
						break;
					}
					paramIndex++;
				}

				if(paramIndex < Parameters[0].weightSettings.length)
				{
					var CartonDetails =
					{
						UseTransporter: Parameters[0].weightSettings[paramIndex].useTransporter,
						EmptyCartonWeight: Parameters[0].weightSettings[paramIndex].weight,
                                                MinTolerance: Parameters[0].weightSettings[paramIndex].LowerTolerance,
                                                MaxTolerance: Parameters[0].weightSettings[paramIndex].UpperTolerance
					}

					return callback(CartonDetails);
				}
				else
				{
					Msg = "Failed to find Carton weightSettings to match order carton field for carton " +
					       PickList.orders[CurrentOrderToProcess].cartonID + " of order " + PickList.orders[CurrentOrderToProcess].orderID;
					WriteToFile(Msg);
					return callback({error:Msg});
				}
			}
			else
			{
				Msg = "Failed to find weightSettings parameter in the system ";
				WriteToFile(Msg);
				return callback({error:Msg});
			}
		}
	});
} /* ParamGetCartonDetails */

function GenerateitemsToPack(savedDoc, CurrentOrderToProcess, callback)
{
	var itemIndex = 0;
	var skus = [];
	var itemsCodes = [];
	var itemDescription = [];
	var pickQty = [];
	var qtyPacked = [];
	var SkuSerials = [];
	while(itemIndex < savedDoc.orders[CurrentOrderToProcess].items.length)
	{
		var y = 0;
		while(y < skus.length)
		{
			if(savedDoc.orders[CurrentOrderToProcess].items[itemIndex].sku == skus[y])
			{
				break;
			}
			y++;
		}

		if(y < skus.length)
		{
			pickQty[y] += savedDoc.orders[CurrentOrderToProcess].items[itemIndex].pickQty;

			var QtyPacked = savedDoc.orders[CurrentOrderToProcess].items[itemIndex].qtyPacked;
			if(QtyPacked == null)
			  QtyPacked = 0;

			qtyPacked[y] += QtyPacked;

			var n = 0;
		  while(n < savedDoc.orders[CurrentOrderToProcess].items[itemIndex].serials.length)
			{
			 	SkuSerials[y].push(savedDoc.orders[CurrentOrderToProcess].items[itemIndex].serials[n]);
			 	n++;
			}
		}
		else
		{
			skus.push(savedDoc.orders[CurrentOrderToProcess].items[itemIndex].sku);
			itemsCodes.push(savedDoc.orders[CurrentOrderToProcess].items[itemIndex].itemCode);
			itemDescription.push(savedDoc.orders[CurrentOrderToProcess].items[itemIndex].itemDescription);
			pickQty.push(savedDoc.orders[CurrentOrderToProcess].items[itemIndex].pickQty);

			var QtyPacked = savedDoc.orders[CurrentOrderToProcess].items[itemIndex].qtyPacked;
			if(QtyPacked == null)
			  QtyPacked = 0;

			qtyPacked.push(QtyPacked);
			var arr = savedDoc.orders[CurrentOrderToProcess].items[itemIndex].serials;
      SkuSerials.push(arr.slice(0, arr.length));
		}
		itemIndex++;
	}

  var itemsToDisplay = [];
  var f = 0;
  while(f < skus.length)
  {
		var line =
		{
			itemCode: itemsCodes[f],
			itemDescription: itemDescription[f],
			pickQty: pickQty[f],
			qtyPacked: qtyPacked[f],
			SkuSerials: SkuSerials[f]
		}

		itemsToDisplay.push(line);
		f++;
	}

	return callback(itemsToDisplay);
} /* GenerateitemsToPack */

function CheckIfPickListIsUnique(doc, CurrentOrderToProcess, callback)
{
  PickListModel.find({'orders.pickListID': doc.orders[CurrentOrderToProcess].pickListID}, function(err, PickLists)
  {
     if(err || !PickLists || PickLists.length <= 0)
     {
       return callback(1);
     }
     else
     {
       var Total = 0;
       var x = 0;
       while(x < PickLists.length)
       {
         var y = 0;
         while(y < PickLists[x].orders.length)
         {
           if(PickLists[x].orders[y].pickListID == doc.orders[CurrentOrderToProcess].pickListID)
           {
             Total++;
           }
	   y++;
         }
         x++;
       }

       return callback(Total);
     }
  });
} /* CheckIfPickListIsUnique */

function ToteValidForPutWalls(doc, Station, callback){
        //return callback(false);
	if(doc.packType != 'PUT'){
		GetAutobaggerMaxVolume(function(Param){
			if(Param.volumeCapacity){
				CheckIfOrdersInToteAreValidforPutWall(doc, 0, Param.volumeCapacity, function(Result){
					if(Result){
						PwLocs.findOne({'packStation': Station}, function(err, PWLOC){
							if(err || !PWLOC){
								return callback(false);
							} else {
								return callback(true);
							}
						});
					} else {
						return callback(false);
					}
				});
			} else {
				return callback(false);
			}
		});
	} else {
		PwLocs.findOne({'packStation': Station}, function(err, PWLOC){
			if(err || !PWLOC){
				return callback(false);
			} else {
				return callback(true);
			}
		});
	}
} /* ToteValidForPutWalls */

function GetAutobaggerMaxVolume(callback){
	Params.find({}, function(err, Parameters){
		if(err || !Parameters || Parameters.length <= 0){
			Msg = "Failed to find parameters in the system ";
			WriteToFile(Msg);
			return callback({error:Msg});
		} else {
			var CartonPrefix = 1;

			if(Parameters[0].weightSettings && Parameters[0].weightSettings.length > 0){
				var paramIndex = 0;
				while(paramIndex < Parameters[0].weightSettings.length){
					if(Parameters[0].weightSettings[paramIndex].StockSize == CartonPrefix){
						break;
					}
					paramIndex++;
				}

				if(paramIndex < Parameters[0].weightSettings.length){

					return callback({volumeCapacity:Parameters[0].weightSettings[paramIndex].volumeCapacity});
				} else {
					Msg = "Failed to find Carton weightSettings to match order carton field for carton " +
					       PickList.orders[CurrentOrderToProcess].cartonID + " of order " + PickList.orders[CurrentOrderToProcess].orderID;
					WriteToFile(Msg);
					return callback({error:Msg});
				}
			} else {
				Msg = "Failed to find weightSettings parameter in the system ";
				WriteToFile(Msg);
				return callback({error:Msg});
			}
		}
	});
} /* GetAutobaggerMaxVolume */

function CheckIfOrdersInToteAreValidforPutWall(doc, index, volumeCapacity, callback){
	if(index < doc.orders.length){
		var PickListVolume = CalcPLVol(doc.orders[index].items);
		if(PickListVolume <= volumeCapacity){
			PickListModel.count({'orders.orderID':doc.orders[index].orderID}, function( err, PLsPerOrder){
				if(PLsPerOrder == 1){
					index++;
					CheckIfOrdersInToteAreValidforPutWall(doc, index, volumeCapacity, callback);
				} else {
					return callback(false);
				}
		  });
		} else {
			return callback(false);
		}
	} else {
		return callback(true);
	}
} /* CheckIfOrdersInToteAreValidforPutWall */

function CalcPLVol(items){
	var PickListVolume = 0;
	var weight = 0;
	var volume = 0;

	if(items.length > 0){
		var x = 0;
		while(x < items.length){
			if(items[x].eachVolume)
			  volume += ((items[x].qty * items[x].eachVolume) / 1)

			x++;
		}

		PickListVolume = volume;
	}

	return PickListVolume;
} /* CalcPLVol */

function GetToteType(NumOrders, TotesPerOrder){
	var toteTypeDetailName = null;
	if(NumOrders > 1){
		toteTypeDetailName = 'MULTI ORDER TOTE';
	} else {
		if(TotesPerOrder > 1){
			toteTypeDetailName = 'MULTI TOTE ORDER';
		} else {
			toteTypeDetailName = 'SINGLE ORDER TOTE';
		}
	}

	return toteTypeDetailName;
} /* GetToteType */

function WriteToFile(Msg){
	if(Msg){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' ' + Msg);
	}
} /* WriteToFile */

function requireLogin (req, res, next){
	if (!req.session.user){
		return res.status(200).send({NoUser:true});
	} else {
		next();
	}
} /* requireLogin */

function getPickList(toteID, callback){
	PickListModel.findOne({toteID: toteID}, function(error, docs){
		if(!error && docs) {
			callback(docs.packType);
		}
	});
} /* getPickList */

/*=================== Functions End =============================================*/

module.exports = router
