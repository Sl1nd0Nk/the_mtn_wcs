/*
 * @Author: Deon Wolmarans
 * @Date: 2017-07-07 11:44:21
 * @Last Modified by: Deon Wolmarans
 * @Last Modified time: 2017-07-07 11:48:22
 */
var apexLib = require('apex_lib/moduleIndex.js')
var express = require('express')
var router = express.Router()

var mongoClient = require('mongodb')
var ObjectID = mongoClient.ObjectID

// Get all the Cartons.
router.get('/API/cartons/', function (req, res) {
    apexLib.mongo.get.mongoMultiGet(global.mongo.db, 'cartons', '', function (error, doc) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(doc)
        }
    })
})

// Get a carton per ID.
router.get('/API/cartons/:id', function (req, res) {
    apexLib.mongo.get.mongoSingleGet(global.mongo.db, 'cartons', { cartonID: req.params.id }, function (error, doc) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(doc)
        }
    })
})

// Post a carton.
router.post('/API/cartons/', function (req, res) {
    var carton = {
        cartonID: req.body.cartonID,
        orderID: req.body.orderID
    }

    apexLib.mongo.insert.mongoSingleInsert(global.mongo.db, 'cartons', carton, function (error, doc) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(doc)
        }
    })
})

// Update a carton.
router.put('/API/cartons/', function (req, res) {
    var updateDocId = req.body.toteID

    var carton = {
        cartonID: req.body.cartonID,
        orderID: req.body.orderID
    }

    apexLib.mongo.update.mongoSingleUpdate(global.mongo.db, 'cartons', { cartonID: updateDocId }, carton, function (error, doc) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(doc)
        }
    })
})

// Delete a carton.
router.delete('/API/cartons/:id', function (req, res) {
    var id = req.params.id

    apexLib.mongo.delete.mongoSingleDelete(global.mongo.db, 'cartons', { _id: new ObjectID(id) }, function (error, doc) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(doc)
        }
    })
})

module.exports = router
