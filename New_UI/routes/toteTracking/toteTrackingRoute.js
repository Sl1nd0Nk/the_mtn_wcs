var express         = require('express');
var PickToZone      = require('conveyorNetworkModel');
var PTLReject       = require('ptlRejectModel');
var PackStation     = require('packStationModel');
var QCRejected      = require('rejectedQC');
var CourierRejected = require('courierRejectModel');
var PickList        = require('pickListModel');
var ChuteModel      = require('chuteModel')
var CourierDivert   = require('courierDivertModel')
var ScaleThroughPut = require('scaleThroughModel')
var PickThrouput    = require('pickThroughputModel')
var CartonShipped   = require('cartonShippedModel')
var putWallModel    = require('putWallDashModel')
var PutWallModel    = require('putWallDashModel')

var moment          = require('moment')


var router = express.Router();

router.get('/getNetwork', function (req, res) {
    PickToZone.find({'Totes.processed':false}, function(error, docs){
        if(!error && docs){
            res.send({code:'00', message:'success', data:docs});
        }else{
            res.send({code:'01', message:'fail.Picklist.ntf', data:error});
        }
    });
});

router.get('/getCartonShipped/:ToDo', function(req, res) {
	let Date = req.params.ToDo;
	let start = '^';
	let end = '/';

	let queryVal = new RegExp(start+Date);
	console.log('Date ' + queryVal);
	PickList.find({'PackedDate':{$regex:queryVal}}).sort({PackedDate: 1}).exec(function(err, Data){
		if(!err){
			if(Data){
				res.send({Data: Data});
		    } else {
				res.send({Msg: 'No Data Found'});
			}
		} else {
			console.log('getCartonShipped error: ' + err);
			res.send({Msg: 'Error while trying to retrieve data'});
		}
	});
    /*CartonShipped.find(function(error, data) {
        if(!error && data) {
            getTodaysShipped(data, req.params.ToDo, function(respData) {
		console.log('CARTON SHIPPED RESPONSE DATA: ')
                res.send({code:'00', message:'success', data:respData})
            })
        } else {
            res.send({code:'01', message:'Cortons were not found.'})
        }
    })*/
})

router.get('/fetchCarton/:ToDo', function(req, res) {
	PickList.findOne({'orders.cartonID':req.params.ToDo},function(err, Data){
		if(!err){
			if(Data){
				res.send({Data: Data});
		    } else {
				res.send({Msg: 'No Data Found'});
			}
		} else {
			console.log('fetchCarton error: ' + err);
			res.send({Msg: 'Error while trying to retrieve data'});
		}
	});
    /*CartonShipped.findOne({cartonID: req.params.ToDo}, function(err, docs) {
        if(!err && docs) {
            res.send({code:'00', message: 'Success', data: docs})
        } else {
            res.send({code:'01', message: 'Carton ID: ' + req.params.ToDo + ' was not found in the db.'})
        }
    })*/
})

router.get('/getPickThroughput', function(req, res) {
    PickThrouput.find(function(error, docs) {
        if(!error && docs) {
            res.send({code:'00', message:'success', data:docs})
        } else {
            res.send({code:'01', message:'Could not find pick throughput data.'})
        }
    })
})

router.get('/getQCRejectedTable', function(req, res) {
    QCRejected.find(function(error, data) {
        if(!error && data) {
            res.send({code:'00', meassage:'success', data:data})
        } else {
            res.send({code:'01', message:'Rejected totes were not found.'})
        }
    })
})

router.get('/getPtlReject', function (req, res) {
    PTLReject.find({Reset:false}, function(error, docs){
        if(!error && docs){
            res.send({code:'00', message:'success', data:docs});
        }else{
            res.send({code:'01', message:'fail.PTLReject.ntf', data:error});
        }
    });
});

router.get('/getTotesInChute', function(req, res){
    getTotesInChute(function(response){
        res.send(response)
    })
})

router.get('/getChuteInfo', function(req, res){
    ChuteModel.find({}, function(error, docs){
        if(!error && docs){
            res.send({code:'00', message:'success', data:docs});
        }else{
            res.send({code:'01', message:'fail.Chute.ntf', data:error});
        }
    })
})

router.get('/getScaleInfo', function(req, res){
    ChuteModel.find({}, function(error, docs){
        if(!error && docs){
            res.send({code:'00', message:'success', data:docs});
        }else{
            res.send({code:'01', message:'fail.Chute.ntf', data:error});
        }
    })
})

router.get('/getCourierDivert', function(req, res){
    CourierDivert.find({}, function(error, docs){
        if(!error && docs){
            res.send({code:'00', message:'success', data:docs});
        }else{
            res.send({code:'01', message:'fail.Chute.ntf', data:error});
        }
    })
})

function getTotesInChute(callback){
    PickList.find({containerLocation:{$ne:null},  $where:"this.containerLocation.length >0"}, function(error, data){
        if(!error && data){
            var Schute1 = 0; var Schute2 = 0; var Schute3 = 0; var Schute4 = 0; var Schute5 = 0; var Schute6 = 0; var Schute7 = 0
            var Schute8 = 0; var Schute9 = 0; var Schute10 = 0; var Schute11 = 0; var Schute12 = 0; var Schute13 = 0; var Schute14 = 0
            var Schute15 = 0; var Schute16 = 0; var Schute17 = 0; var Schute18 = 0

            for(var x = 0; x < data.length; x++){
                if(data[x].containerLocation == 1){Schute1 += 1} else if(data[x].containerLocation == 2){Schute2 = Schute2 + 1
                }else if(data[x].containerLocation == 3){Schute3 = Schute3 + 1}else if(data[x].containerLocation == 4){Schute4 = Schute4 + 1
                }else if(data[x].containerLocation == 5){Schute5 = Schute5 + 1}else if(data[x].containerLocation == 6){Schute6 = Schute6 + 1
                }else if(data[x].containerLocation == 7){Schute7 = Schute7 + 1}else if(data[x].containerLocation == 8){Schute8 = Schute8 + 1
                }else if(data[x].containerLocation == 9){Schute9 = Schute9 + 1}else if(data[x].containerLocation == 10){Schute10 = Schute10 + 1
                }else if(data[x].containerLocation == 11){Schute11 = Schute11 + 1}else if(data[x].containerLocation == 12){Schute12 = Schute12 + 1
                }else if(data[x].containerLocation == 13){Schute13 = Schute13 + 1}else if(data[x].containerLocation == 14){Schute14 = Schute14 + 1
                }else if(data[x].containerLocation == 15){Schute15 = Schute15 + 1}else if(data[x].containerLocation == 16){Schute16 = Schute16 + 1
                }else if(data[x].containerLocation == 17){Schute17 = Schute17 + 1}else if(data[x].containerLocation == 18){Schute18 = Schute18 + 1}
            }

            var schuteObj = {};var schuteTwoObj = {};var schuteThreeObj = {};var schuteFourObj = {};var schuteFiveObj = {};var schuteSixObj = {};
            var schuteSevenObj = {};var schuteEightObj = {};var schuteNineObj = {};var schute10Obj = {};var schute11Obj = {};var schute12Obj = {};
            var schute13Obj = {};var schute14Obj = {};var schute15Obj = {};var schute16Obj = {};var schute17Obj = {};var schute18Obj = {};

            var displayArr = []

            schuteObj = {ZoneID: 1,numTotes: Schute1}
            displayArr.push(schuteObj)

            schuteTwoObj = {ZoneID: 2,numTotes: Schute2}
            displayArr.push(schuteTwoObj)

            schuteThreeObj = {ZoneID: 3,numTotes: Schute3}
            displayArr.push(schuteThreeObj)

            schuteFourObj = {ZoneID: 4,numTotes: Schute4}
            displayArr.push(schuteFourObj)

            schuteFiveObj = {ZoneID: 5,numTotes: Schute5}
            displayArr.push(schuteFiveObj)

            schuteSixObj = {ZoneID: 6,numTotes: Schute6}
            displayArr.push(schuteSixObj)

            schuteSevenObj = {ZoneID: 7,numTotes: Schute7}
            displayArr.push(schuteSevenObj)

            schuteEightObj = {ZoneID: 8,numTotes: Schute8}
            displayArr.push(schuteEightObj)

            schuteNineObj = {ZoneID: 9,numTotes: Schute9}
            displayArr.push(schuteNineObj)

            schute10Obj = {ZoneID: 10,numTotes: Schute10}
            displayArr.push(schute10Obj)

            schute11Obj = {ZoneID: 11,numTotes: Schute11}
            displayArr.push(schute11Obj)

            schute12Obj = {ZoneID: 12,numTotes: Schute12}
            displayArr.push(schute12Obj)

            schute13Obj = {ZoneID: 13,numTotes: Schute13}
            displayArr.push(schute13Obj)

            schute14Obj = {ZoneID: 14,numTotes: Schute14}
            displayArr.push(schute14Obj)

            schute15Obj = {ZoneID: 15,numTotes: Schute15}
            displayArr.push(schute15Obj)

            schute16Obj = {ZoneID: 16,numTotes: Schute16}
            displayArr.push(schute16Obj)

            schute17Obj = {ZoneID: 17,numTotes: Schute17}
            displayArr.push(schute17Obj)

            schute18Obj = {ZoneID: 18,numTotes: Schute18}
            displayArr.push(schute18Obj)

            callback({code:'00', message:'success', data:displayArr});
        }else{
            callback({code:'01', message:'fail.PackStation.ntf', data:error});
        }
    });
}

router.get('/getActualTotesInSchute/:ToDo', function(req, res){
    PickList.find({containerLocation:req.params.ToDo},function(error, docs){
        if(!error && docs){
            var totes = []
            for(var x =0; x < docs.length; x++){
                totes.push(docs[x].toteID)
            }
            res.send({code:'00', message:'success', data:totes});
        }else{
            res.send({code:'01', message:'fail.PickList.ntf', data:error});
        }
    });
});

router.get('/getScaleLog', function(req, res){
    ScaleThroughPut.find(function(error, docs){
        if(!error && docs){
            res.send({code:'00', message:'success', data:docs});
        }else{
            res.send({code:'01', message:'fail.ScaleThrouput.ntf', data:error});
        }
    });
});

router.get('/getCourierReject', function(req, res){
    CourierRejected.find({processed:false},function(error, docs){
        if(!error && docs){
            res.send({code:'00', message:'success', data:docs});
        }else{
            res.send({code:'01', message:'fail.Courier.ntf', data:error});
        }
    });
});

router.get('/getTotals', function (req, res) {
    var pickToZoneTotal = 0;
    var ptlRejectTotal  = 0;
    var ChuteTotal      = 0;
    var QCRejectTotal   = 0;
    var CourierTotal    = 0;
    var compressedChutes = [];

    PickToZone.find(function(error, data){
        if(!error && data){
            for(var x = 0; x < data.length; x++){
                // console.log('=== PTZ FOUND ===' + data[x].Totes.length);
                pickToZoneTotal +=  data[x].Totes.length
            }

            getPTLRejectTotal();
        }
    });

    function getPTLRejectTotal(){
        // console.log('=== PTL INIT ===');
        PTLReject.find({Reset:false},function(ptlError, ptlData){
            if(!ptlError && ptlData){
                // console.log('=== PTL FOUND ===');
                ptlRejectTotal = ptlData.length;
                getChuteTotal();
            }
        })
    }

    function getChuteTotal(){
        getTotesInChute(function(response){
            for(var x = 0; x < response.data.length; x++){
                ChuteTotal += response.data[x].numTotes
            }
            console.log( 'SchuteTotal: ' + ChuteTotal)
            getQcRejectTotal();
        })
    }

    function getQcRejectTotal(){
        QCRejected.find({processed:false}, function(qcError, qcData){
            if(!qcError && qcData){
                // console.log('=== QC FOUND ===');
                QCRejectTotal = qcData.length;
                getCourierRejectTotal();
            }
        });
    }

    function getCourierRejectTotal(){
        CourierRejected.find({processed:false}, function(courierError, courierData){
            if(!courierError && courierData){
                // console.log('=== COURIER FOUND ===');
                CourierTotal =  courierData.length;
                sendResponse();
            }
        });
    }

    function sendResponse(){
        // console.log('=== SEND RESP FOUND ===');
        var respData = {
            PTZBuffer:pickToZoneTotal,
            ptlReject : ptlRejectTotal,
            chuteTotal: ChuteTotal,
            qcRejectTotal: QCRejectTotal,
            courierTotal: CourierTotal
        };

        res.send({code:'00', message:'success', data:respData});
    }
});

router.get('/getPutWallData', function(req, res) {
    PutWallModel.find(function(error, docs) {
        if(!error && docs) {
            res.send({code:'00', message:'success', data:docs})
        } else {
            res.send({code:'01', message:'Put wall information not found.'})
        }
    })
})

function getTodaysShipped_old(shippedData, theData, callback) {
    var todayArr = []
    var x = 0
    for(var x = 0; x < shippedData.length; x++) {
	if(shippedData[x].dateTime.substr(0, 1) == '0') {
     		console.log('THE DATA: ' + shippedData[x].dateTime.substr(1, 1) + ' | ' + theData)
        }
	// console.log('THE DATA: ' + shippedData[x].dateTime.substr(0, 1) + ' | ' + theData)
        if(shippedData[x].dateTime.substr(0, 2) == theData) {
            todayArr.push(shippedData[x])
        }
    }

    callback(todayArr)
}

function getTodaysShipped(shippedData, theData, callback) {
    var todayArr = []
    var x = 0

    for(var x = 0; x < shippedData.length; x++) {
        if(shippedData[x].dateTime.substr(0, 1) == '0') {
	    console.log('THE DATA: ' + shippedData[x].dateTime.substr(0, 10) + ' | ' + theData)
            if(shippedData[x].dateTime.substr(0, 10) == theData) {
		console.log('THE DATA THAT YOU PUSH')
                todayArr.push(shippedData[x])
            }
        } else if(shippedData[x].dateTime.substr(0, 10) == theData) {
            todayArr.push(shippedData[x])
        }
    }

    callback(todayArr)
}

module.exports = router;
