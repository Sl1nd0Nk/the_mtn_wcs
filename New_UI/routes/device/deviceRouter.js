var apexLib = require('apex_lib/moduleIndex.js')
var express = require('express')
var router = express.Router()

// Get the devices.
router.get('/API/device/', function (req, res) {
    apexLib.mySQL.query.execute(global.mySQLClient, 'call device_get();', '', function (error, rows) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(rows[0])
        }
    })
})

// Get device item per ID.
router.get('/API/device/:id', function (req, res) {
    apexLib.mySQL.query.execute(global.mySQLClient, 'call device_get_id(?);', req.params.id, function (error, rows) {
        if (error) {
            return res.status(405).send(error)
        } else {
            if (rows[0].length === 0) return res.status(405).send('No Device Item Found.')
            else return res.status(200).send(rows[0])
        }
    })
})

// Post a device.
router.post('/API/device/', function (req, res) {
    var name = req.body.name
    var value = req.body.value
    var description = req.body.description
    var roleID = req.body.roleID
    var groupID = req.body.groupID

    var parameters = [name, value, description, roleID, groupID]

    apexLib.mySQL.query.execute(global.mySQLClient, 'call device_insert(?, ?, ?, ?, ?);', parameters, function (error, rows) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send('Success')
        }
    })
})

// Update a device.
router.put('/API/device/', function (req, res) {
    var id = req.body.id
    var name = req.body.name
    var value = req.body.value
    var description = req.body.description
    var roleID = req.body.roleID
    var groupID = req.body.groupID

    var parameters = [id, name, value, description, roleID, groupID]

    apexLib.mySQL.query.execute(global.mySQLClient, 'call device_update(?, ?, ?, ?, ?, ?);', parameters, function (error, rows) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send('Success')
        }
    })
})

// Delete a device.
router.delete('/API/device/:id', function (req, res) {
    // var id = req.body.id

    apexLib.mySQL.query.execute(global.mySQLClient, 'call device_delete(?);', req.params.id, function (error, rows) {
        if (error) {
            return res.status(405).send(error)
        } else {
            if (rows.affectedRows > 0) return res.status(200).send('Success')
            else return res.status(405).send('No record to delete.')
        }
    })
})

module.exports = router
