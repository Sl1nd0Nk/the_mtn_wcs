var apexLib = require('apex_lib/moduleIndex.js')
var express = require('express')
var router = express.Router()

// Get the device groups.
router.get('/API/deviceGroup/', function (req, res) {
    apexLib.mySQL.query.execute(global.mySQLClient, 'call device_group_get();', '', function (error, rows) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(rows[0])
        }
    })
})

// Get device group item per ID.
router.get('/API/deviceGroup/:id', function (req, res) {
    apexLib.mySQL.query.execute(global.mySQLClient, 'call device_group_get_id(?);', req.params.id, function (error, rows) {
        if (error) {
            return res.status(405).send(error)
        } else {
            if (rows[0].length === 0) return res.status(405).send('No Device Group Item Found.')
            else return res.status(200).send(rows[0])
        }
    })
})

// Post a device group.
router.post('/API/deviceGroup/', function (req, res) {
    var name = req.body.name
    var description = req.body.description

    var parameters = [name, description]

    apexLib.mySQL.query.execute(global.mySQLClient, 'call device_group_insert(?, ?);', parameters, function (error, rows) {
        if (error) {
            return res.status(405).send(error.message)
        } else {
            return res.status(200).send('Success')
        }
    })
})

// Update a device group.
router.put('/API/deviceGroup/', function (req, res) {
    var id = req.body.id
    var name = req.body.name
    var description = req.body.description

    var parameters = [id, name, description]

    apexLib.mySQL.query.execute(global.mySQLClient, 'call device_group_update(?, ?, ?);', parameters, function (error, rows) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send('Success')
        }
    })
})

// Delete a device group.
router.delete('/API/deviceGroup/', function (req, res) {
    var id = req.body.id

    apexLib.mySQL.query.execute(global.mySQLClient, 'call device_group_delete(?);', id, function (error, rows) {
        if (error) {
            return res.status(405).send(error)
        } else {
            if (rows.affectedRows > 0) return res.status(200).send('Success')
            else return res.status(405).send('No record to delete.')
        }
    })
})

module.exports = router
