var express = require('express')
var PackStation = require('packStationModel')

var router = express.Router()

router.get('/getStation', function (req, res) {
    PackStation.find({}, function(error, data) {
        if(!error && data) {
            res.send({code:'00', message:'success', data:data})
        } else {
            res.send({code:'01', message:'fail', data:error})
        }
    })
})

router.post('/updateStation/:ToDo', function(req, res) {
    var stationID = req.params.ToDo
    
    PackStation.findOne({stationNumber:stationID}, function(error, docs) {
        if(!error && docs) {
            console.log('THE CHUTE THAT WE ARE LOOKING FOR: ' + docs.schuteNumber)
            PackStation.find({schuteNumber:docs.schuteNumber}, function(err, chuteDocs){
                if(!err && chuteDocs){
                    var x = 0
                    while(x < chuteDocs.length){
                        if(chuteDocs[x].active){chuteDocs[x].active = false; chuteDocs[x].updated_at = Date.now()} else {chuteDocs[x].active = true; chuteDocs[x].updated_at = Date.now()}
                        chuteDocs[x].save(function(sErr, sDocs){
                            if(!sErr && sDocs){
                                console.log('*** SAVED ***')
                            }else{
                                console.log('### NOT SAVED ###')
                            }
                        })
                        x++
                    }

                    res.send({code:'00', message:'success', data:chuteDocs})
                }else{
                    console.log('IS IT AN ERRORED RESPONSE?')
                    res.send({code:'03', message:'save.fail', data:err})
                }
            })
        } else {
            console.log('**** | **** ' + stationID + ' | ERR: ' + error + ' | DOCS: ' + docs)
            res.send({code:'01', message:'fail', data:error})
        }
    })
})

router.post('/updateAll/:ToDo', function(req, res) {
    var option = req.params.ToDo

    PackStation.find({}, function(error, data) {
        if(!error && data) {
            var x = 0
            while (x < data.length) {
                if(option == 'deactivate'){
                    data[x].active = false

                    data[x].save()
                }else{
                    data[x].active = true
                    
                    data[x].save()
                }
                x++
            }
            res.send({code:'00', message:'success', data:data})
        } else {
            res.send({code:'01', message:'fail'})
        }
    })
})

module.exports = router
