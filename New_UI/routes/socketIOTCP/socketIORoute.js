var express = require('express')
var SocketModule = require('../../module/socketIO');

var router = express.Router()

router.post('/getManualWeight', function (req, res) {
    SocketModule.getWeight(function(response){
        res.send(response)
    })
})

module.exports = router
