/*
 * @Author: Deon Wolmarans
 * @Date: 2017-07-25 10:45:46
 * @Last Modified by: Ntokozo Majozi
 * @Last Modified time: 2018-01-11 10:55:28
 */

var apexLib = require('apex_lib/moduleIndex.js')
var express = require('express')
var router = express.Router()
var redis = require('redis');
var invoiceUrl = require('ui').invoiceUrl

var Pubclient = redis.createClient();

// Send the order and printer name to the server.
router.post('/API/printDocuments', function (req, res) {
    	console.log('We just entered the printing end-point')
       var documentToPrint = req.body
       console.log(documentToPrint);
       //var invoiceJson = JSON.parse(documentToPrint)
    var url = invoiceUrl

    /*apexLib.rest.post(url, documentToPrint, function (response) {
        res.status(200).send(response)
    })*/

    console.log('Clive you got :::::::PACKING_' + documentToPrint.station);
    Pubclient.publish('PACKING_' + documentToPrint.station, JSON.stringify(documentToPrint));
    res.status(200).send('Document Printing');
})

// Send the order and zpl printer name to the server.
router.post('/API/printZplDocuments/:id', function (req, res) {
    var labelToPrintId = req.params.id
    // var labelJson = JSON.parse(labelToPrintId)
    var url = invoiceUrl

    apexLib.rest.post(url, labelToPrintId, function (response) {
        res.status(200).send(response)
    })
})

module.exports = router
