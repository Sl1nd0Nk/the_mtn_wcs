var express     = require('express')
var PackStation = require('packStationModel')
var PickList    = require('pickListModel')
var CanReq      = require('CancelRequestModel');
var moment      = require('moment')
var redis       = require('redis')
var Pubclient   = redis.createClient();
var CartonUrl   = require('ui').CartonUrl;
var rest        = require('restler');
var serialLib   = require('classSerial');
var PackUpdate  = require('PackUpdatesModel');
var ShipUpdate  = require('ShipUpdatesModel');
var serls       = new serialLib();
var router      = express.Router()

router.get('/getPalletStation/', requireLogin, function (req, res) {
	let user = req.session.username;
	return res.status(200).send({User: user});
});

router.get('/getLoadInfo/:ToDo', requireLogin, function (req, res) {
	let user = req.session.username;
	let Msg = 'User: ' + user;

    PickList.findOne({toteID:req.params.ToDo, PickType:'FULLPICK'}, function(error, docs){
		Msg += ' | Pallet: ' + req.params.ToDo;

        if(error || !docs){
			Msg += '| ERROR: ' + error;
			WriteToFile(Msg);
            return res.send({code:'01', message:'Picklist not found.', data:null});
        }

		Msg += '| Order: ' + docs.orders[0].orderID + '| Picklist: ' + docs.orders[0].pickListID;

		if(docs.Status == 'CANCELED'){
			Msg += '| Picklist is cancelled.';
			WriteToFile(Msg);
			return res.send({code:'05', message:'Picklist is cancelled.'});
		}

		if(docs.orders[0].WmsOrderStatus == 'CANCELED'){
			Msg += '| Picklist is cancelled.';
			WriteToFile(Msg);
			return res.send({code:'05', message:'Picklist is cancelled.'});
		}

		CanReq.findOne({'OrderId': docs.orders[0].orderID}, function(err, Req){
			if(Req){
				docs.Status = 'CANCELED';
				docs.orders[0].WmsOrderStatus = 'CANCELED';
				docs.orders[0].WmsPickListStatus = 'CANCELED';
				docs.orders[0].CancelDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
				docs.orders[0].CancelInstruction = 'Fetch Pallet From Full Pallet Handover Station';

				docs.save(function(err, sd){
					Msg += '| Picklist is cancelled.';
					WriteToFile(Msg);
					return res.send({code:'05', message:'Picklist is cancelled.'});
				});

				return;
			}

			if(docs.Status != 'PICKED'){
				if(docs.Status == 'PACKED'){
					return res.send({code:'00', message:'PACKED', data:docs});
					//Msg += '| already PACKED';
					//WriteToFile(Msg);
					//return res.send({code:'05', message:'Picklist is already PACKED.'});
				}

				if(docs.Status == 'SHIPPED'){
					Msg += '| already SHIPPED';
					WriteToFile(Msg);
					return res.send({code:'05', message:'Picklist is already SHIPPED.'});
				}

				Msg += '| is not PICKED';
				WriteToFile(Msg);
				return res.send({code:'05', message:'Picklist is not PICKED.'});
			}

			let CanPack = true;
			let PicklistQty = 0;
			let PalletQty = 0;

			if(docs.orders[0].items[0].qty != docs.orders[0].items[0].pickQty){
				PicklistQty = docs.orders[0].items[0].qty;
				PalletQty = docs.orders[0].items[0].pickQty;
				CanPack = false;
			}

			if(CanPack && docs.orders[0].items[0].serialised && docs.orders[0].items[0].qty != docs.orders[0].items[0].serials.length){
				PicklistQty = docs.orders[0].items[0].qty;
				PalletQty = docs.orders[0].items[0].serials.length;
				CanPack = false;
			}

			if(!CanPack){
				Msg += '| Qty mismatch picklist qty ('+ PicklistQty +') and pallet qty ('+ PalletQty +')';
				WriteToFile(Msg);
				return res.send({code:'05', message:'Qty mismatch between picklist qty and pallet qty'});
			}

			docs.Status = 'PACKED';
			docs.PackUpdateRequired = true;
			docs.PackedDate = moment(new Date()).format('YYYY-MM-DD HH:mm');
			docs.orders[0].items[0].packed = true;
			docs.orders[0].items[0].qtyPacked = docs.orders[0].items[0].qty;
			docs.orders[0].packed = true;
			docs.PackUpdateRequired = true;
			docs.orders[0].packComplete = true;
			docs.FromTote = req.params.ToDo;
			docs.orders[0].cartonID = req.params.ToDo;
			docs.orders[0].items[0].packedBy = user;

			docs.save(function(saveError, saveDocs){
				if(saveError || !saveDocs){
					Msg += '| Could not print documents - Error: ' + saveError;
					WriteToFile(Msg);
					return res.send({code:'07', message:'Could not print documents', data: null});
				}

				GetAllItemsAndCartonsForOrder(saveDocs, 0, function(OrdDetails){
					if(OrdDetails.error){
						Msg += '| ' + OrdDetails.error;
						WriteToFile(Msg);
						return res.send({code:'07', message:OrdDetails.error});
					}

					DetermineCartonNo(saveDocs, 0, function(responseDoc){
						if(responseDoc.error){
							Msg += '| ' + responseDoc.error;
							WriteToFile(Msg);
							return res.send({code:'07', message:responseDoc.error});
						}

						CreatePackUpdate(saveDocs);
						serls.UpdateSerialsToHost(saveDocs.orders[0], function(){});

						SendDocsLabelPrint(responseDoc, 0, false, OrdDetails, function(data){
							if(data){
								Msg += '| Successfully Packed.';
								WriteToFile(Msg);
								return res.send({code:'00', message:'success', data:saveDocs});
							}

							Msg += '| Failed to print the document';
							WriteToFile(Msg);
							return res.send({code:'03', message:'Failed to print the document', data:null});
						});
					});
				});
			});
		});
    });
});

router.get('/updateShipped/:ToDo', requireLogin, function (req, res) {
	let user = req.session.username;
	let Msg = 'User: ' + user;

	PickList.findOne({toteID:req.params.ToDo, PickType:'FULLPICK'}, function(error, docs){
		Msg += ' | Pallet: ' + req.params.ToDo;

		if(error || !docs){
			Msg += '| ERROR: ' + error;
			WriteToFile(Msg);
			res.send({code:'01', message:'Fail. Invalid document number. Please reset, and start again', data:null});
		}

		Msg += '| Order: ' + docs.orders[0].orderID + '| Picklist: ' + docs.orders[0].pickListID;

		docs.orders[0].shipped = true;
		docs.ShipUpdateRequired = true;
		docs.Status = 'SHIPPED';
		docs.dispatched = true;
		docs.transporterID = null;
		docs.dispatchedDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');

		CreateShipUpdate(docs);

		docs.save(function(saveError, saveData){
			if(!saveError && saveData){
				Msg += '| Successfully Shipped.';
				WriteToFile(Msg);
				res.send({code:'00', message:'success', data:saveData})
			}else{
				Msg += '| Failed To Ship Pallet - Error: ' + saveError;
				WriteToFile(Msg);
				res.send({code:'03', message:'Fail. Could not update picklist. Please reset, and start again', data:null})
			}
		});
	});
});

router.get('/getLoadInfoDisplay/:ToDo', function (req, res) {
	PickList.findOne({toteID:req.params.ToDo}, function(error, docs){
		if(!error && docs){
			res.send({code:'00', message:'success', data:docs})
		}else{
			res.send({code:'01', message:'Fail. Could not find piclist', data:error})
		}
	})
})

router.get('/reset/:ToDo', function (req, res) {
	var jsonPiclistInfo = JSON.parse(req.params.ToDo)
	PickList.findOne({toteID:jsonPiclistInfo.loadID}, function(error, docs){
		if(!error && docs){
			docs.Status = 'PICKED'

			docs.save(function(saveError, saveData) {
				if(!saveError && saveData){
					res.send({code:'00', message:'success', data:saveData})
				}else{
					res.send({code:'01', message:'failed to save.', data:docs})
				}
			})
		}else{
			res.send({code:'01', message:'Fail. Could not find piclist', data:docs})
		}
	})
})

function GetAllItemsAndCartonsForOrder(savedDoc, CurrentOrderToProcess, callback){
	var Msg = null;
	PickList.find({'orders.orderID': savedDoc.orders[CurrentOrderToProcess].orderID}, function(err, PickLists){
		if(err || !PickLists || PickLists.length <= 0){
			Msg = "Error finding all picklists for order " + Data.orders[CurrentOrderToProcess].orderID;
			WriteToFile(Msg);
			return callback({error:"Problem encountered fetching all picklists for order - please call supervisor"});
		} else {
			var Cartons = [];
			var OrdItems = [];
			var PL = 0;
			while(PL < PickLists.length){
				var OrdIndex = 0;

				while(OrdIndex < PickLists[PL].orders.length){
					if(PickLists[PL].orders[OrdIndex].orderID == savedDoc.orders[CurrentOrderToProcess].orderID){
						var itemIndx = 0;
						while(itemIndx < PickLists[PL].orders[OrdIndex].items.length){
							OrdItems.push(PickLists[PL].orders[OrdIndex].items[itemIndx]);
							Cartons.push(PickLists[PL].orders[OrdIndex].cartonID);
							itemIndx++;
						}
					}
					OrdIndex++;
				}
				PL++;
			}

			var ReturnData ={
				Cartons: Cartons,
				OrdItems: OrdItems
			}

			return callback(ReturnData);
		}
	});
}

function SendDocsLabelPrint(PickList, CurrentOrderToProcess, UseTransporter, OrderData, callback){
	var Next = '';
	var OrdItems = OrderData.OrdItems;
	var Cartons  = OrderData.Cartons;

	var CartonNo = PickList.orders[CurrentOrderToProcess].toteNumber;
	var NoOfCartons = PickList.orders[CurrentOrderToProcess].numberOfTotes;

	var First = false;
	var Last = false;
	var Expect = '';
	if(CartonNo == 1 && CartonNo == NoOfCartons){
		First = true;
		Last = true;
		Next = 'DOC';
		Expect = 'Documents/label';
	} else if(CartonNo == 1 && CartonNo != NoOfCartons){
		First = true;
		Last = false;
		Expect = 'label';
		if(UseTransporter){
			Next = 'TRANS';
		} else {
			Next = 'DONE';
		}
	} else if(CartonNo > 1 && CartonNo == NoOfCartons){
		First = false;
		Last = true;
		Next = 'DOC';
		Expect = 'Documents/label';
	} else if(CartonNo > 1 && CartonNo != NoOfCartons){
		First = false;
		Last = false;
		Expect = 'label';
		if(UseTransporter)
		{
			Next = 'TRANS';
		} else {
			Next = 'DONE';
		}
	}

    // var Station = parseInt(PickList.toteInUseBy);
    var Station = 41
	PackStation.findOne({stationNumber: Station}, function (error, doc){
		if(error || !doc){
			return callback({error:"Error while finding station for Tote " + PickList.toteID});
		} else {
			var PrintData = {
				orderId: PickList.orders[CurrentOrderToProcess].orderID,
				printerName: doc.desktopPrinterQName,
				labelprinterName: doc.labelPrinterQName,
				items: OrdItems,
				ErpItems: PickList.orders[CurrentOrderToProcess].items,
				cartonId: PickList.orders[CurrentOrderToProcess].cartonID,
				courierId: PickList.orders[CurrentOrderToProcess].courierID,
				station: doc.stationNumber,
				CartonNo: CartonNo,
				NoOfCartons: NoOfCartons,
				CartionWeight: PickList.orders[CurrentOrderToProcess].picklistWeight,
				FirstCarton: First,
				LastCarton: Last,
				orderType: PickList.orders[CurrentOrderToProcess].orderType,
				Cartons: Cartons
			}

			Pubclient.publish('PACKING_' + doc.stationNumber, JSON.stringify(PrintData), function(err){
				 if(err){
					 return callback({error:"Error printing " + Expect + " for order " + PickList.orders[CurrentOrderToProcess].orderID});
				 } else {
					 PickList.orders[CurrentOrderToProcess].printed = true
					 PickList.save(function(err, savedDoc){
						 return callback({Next:Next, PickList:savedDoc});
					 });
				 }
			});
		}
	});

} /* SendDocsLabelPrint */

function getNumTotes(loadID){
    PickList.find({toteID:loadID}, function(error, data){
        if(!error && data){
            return data.length
        }else{
            return ''
        }
    })
}

function claculateCartonNumber(loadID){
    PickList.find({toteID:loadID}, function(error, data){
        if(!error && data){

        }else{

        }
    })
}

function WriteToFile(Msg){
	if(Msg){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' ' + Msg);
	}
} /* WriteToFile */

function DetermineCartonNo(savedDoc, CurrentOrderToProcess, callback){
	var Msg = null;
	let body = {
		cartonID:   savedDoc.orders[CurrentOrderToProcess].cartonID,
		orderID:    savedDoc.orders[CurrentOrderToProcess].orderID,
		picklistID: savedDoc.orders[CurrentOrderToProcess].pickListID
	}

	rest.postJson(CartonUrl, body).on('complete', function(result){
		if(result instanceof Error){
			Msg = "Error finding all picklists for order " + savedDoc.orders[CurrentOrderToProcess].orderID + " to calculate carton number";
			WriteToFile(Msg);
			return callback({error:"Problem encountered while calculating the order carton number - please call supervisor"});
		}

		if(result.Err){
			Msg = "Error finding all picklists for order " + savedDoc.orders[CurrentOrderToProcess].orderID + " to calculate carton number";
			WriteToFile(Msg);
			return callback({error:"Problem encountered while calculating the order carton number - please call supervisor"});
		}

		let CartonNo = result.CartonNo;
		let NoOfCartons = result.NoOfCartons;

        savedDoc.orders[CurrentOrderToProcess].toteNumber = CartonNo;
        savedDoc.orders[CurrentOrderToProcess].numberOfTotes = NoOfCartons;

		savedDoc.save(function(err, Data){
			Msg = "Order " + Data.orders[CurrentOrderToProcess].orderID + " | cartonID " +
						Data.orders[CurrentOrderToProcess].cartonID + " | carton number " + Data.orders[CurrentOrderToProcess].toteNumber +
						" of " + Data.orders[CurrentOrderToProcess].numberOfTotes + " was scanned on station " + Data.toteInUseBy;
			WriteToFile(Msg);

			return callback(Data);
		});
	});
}

function CreatePackUpdate(mPL){
	CreateRecordForEachLine(mPL, 0, function(){
		return;
	});
} /* CreatePackUpdate */

function CreateRecordForEachLine(mPL, lineNdx, callback){
	if(lineNdx >= mPL.orders[0].items.length){
		return callback();
	}

	let Order = mPL.orders[0];
	let Line = Order.items[lineNdx];

	PackUpdate.findOne({'PickListID': Order.pickListID, 'PickListLine': Line.pickListOrderLine, 'OrderId': Order.orderID}, function(err, uPL){
		if(err){
			lineNdx++;
			return CreateRecordForEachLine(mPL, lineNdx, callback);
		}

		if(uPL){
			lineNdx++;
			return CreateRecordForEachLine(mPL, lineNdx, callback);
		}

		let NewRec = new PackUpdate({
			PickListID: Order.pickListID,
			PickListLine: Line.pickListOrderLine,
			WcsId: Order.cartonID,
			OrderId: Order.orderID,
			Required: true,
			CreateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
		});

		NewRec.save(function(err, savedDoc){
			WriteToFile('Marked picklist: ' + Order.pickListID + ' PickListLine: ' + Line.pickListOrderLine + ' | Order: ' + Order.orderID + ' | Carton: ' + Order.cartonID + ' PackUpdate to true');
			lineNdx++;
			return CreateRecordForEachLine(mPL, lineNdx, callback);
		});
	});
} /* CreateRecordForEachLine */

function CreateShipUpdate(mPL){
	ShipUpdate.findOne({'OrderId': mPL.orders[0].orderID}, function(err, uPL){
		if(err){
			return;
		}

		if(uPL){
			return;
		}

		let NewRec = new ShipUpdate({
			OrderId: mPL.orders[0].orderID,
			Required: true,
			CreateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
		});

		NewRec.save(function(err, savedDoc){
			WriteToFile('Marked Order: ' + mPL.orders[0].orderID + ' ShipUpdate to true');
			return;
		});
	});
} /* CreateShipUpdate */

function requireLogin (req, res, next){
	if (!req.session.user){
		return res.status(200).send({NoUser:true});
	} else {
		next();
	}
} /* requireLogin */

module.exports = router
