/*
 * @Author: Clive Matlawa
 * @Date: 2017-09-12
 */

var apexLib = require('apex_lib/moduleIndex.js')
var PackStationModel = require('packStationModel')
var QcStationModel = require('qcRejectStationModel')
var UserModel = require('userModel')
var moment = require('moment')
var express = require('express')
var RoleCheck	= require('../../module/roleAndSessionCheck/roleCheck')

var router = express.Router()

/***************** New Point ************************************************/

router.get('/API/getShowAllUsers/', function (req, res) {
	RoleCheck.checkUserRole(req.session.username, function(response) {
		if(response.code == '00') {
			if(response.data.roleID == 2 || response.data.roleID == 3) {
				UserModel.find(function(error, docs){
					if(!error && docs){
						res.send({code:'00', message:'success', data:docs})
					}else{
						res.send({code:'01', message:'fail. Users not found', data:error})
					}
				})
			} else {
				res.send({code:'01', message:'You do not have access to this page.'})
			}
		} else {
			res.send({code:'01', message:'User role were not found. You do not have access to this page.'})
		}
	})
})

router.get('/checkRole/:ToDo', function(req, res) {
	roleRequired = req.params.ToDo
	var role = 0
	if(roleRequired=='one') { role = 1 } else if (roleRequired=='two') { role = 2 } else if ( roleRequired=='three' ) { role = 3}

	if(role == 1) {
		res.send({code:'00', meesage:'success'})
	} else {
		RoleCheck.checkUserRole(req.session.username, function(response) {
			if(response.code == '00') {
				if(response.data.roleID == role || response.data.roleID == 2) {
					res.send({code: '00', message: 'success'})
				} else {
					res.send({code: '01', message: 'You do not have permission to access this page.'})
				}
			} else {
				res.send({code:'01', message:'User role were not found. You do not have access to this page.'})
			}
		})
	}
})

router.get('/getLoggedInUser/ToDo', function(req, res) {
	if(!req.session.user) {
		res.send({code:'01', message:'User session not found'})
	} else {
		res.send({code:'00', message:'success'})
	}
})

router.post('/addUser/:ToDo', function(req, res) {
	var userData = JSON.parse(req.params.ToDo)

	var roleID = 1
	if(userData.selected == 'Admin') {roleID = 2} else if(userData.selected == 'Supervisor') {roleID = 3} else {roleID = 1}

	UserModel.findOne({username: userData.username}, function(error, docs) {
		if(!error && docs) {
			res.send({code:'01', message:'User ' + userData.username + ' already exists.'})
		} else {
			var userModel = new UserModel({
				name 	 : userData.name,
				surname  : userData.surname,
				password : userData.password,
				username : userData.username,
				roleID 	 : roleID,
				uniqueID : "",
				failedLogin : 0
			})

			userModel.save(function(saveErr, saveData) {
				if(!saveErr && saveData) {
					res.send({code:'00', message:'success', data:saveData})
				} else {
					res.send({code:'01', message:'This user could not be saved properly.'})
				}
			})
		}
	})
})


/* Uses username/password to validate user */
router.get('/API/mongoUsers/:id', function (req, res){
  if(req.params.id){
	var data = JSON.parse(req.params.id);
	var name = data.userName;
	var password = data.userPassword;
	var Msg = null;

	UserModel.findOne({username: name, password: password}, function(error, doc)
	{
		if(error || !doc)
		{
			Msg = 'Failed attempt by user ' + name + ' to log onto the system';
			failedLogin(name)
			WriteToFile(Msg);
			return res.status(405).send('Unable to find user');
		}
		else
		{
			var Token = req.headers['x-forwarded-for'] ||
									req.connection.remoteAddress ||
									req.socket.remoteAddress ||
									req.connection.socket.remoteAddress;

			if (Token.substr(0, 7) == "::ffff:")
			{
					Token = Token.substr(7)
			}

			doc.uniqueID = Token;

			doc.save(function()
			{
				Msg = 'User ' + name + ' successfully logged onto the system';
				clearFailed(name)
				WriteToFile(Msg);

				var endResult =
				{
					Token: Token,
					UserID: doc.name
				}
				req.session.user = doc.name;
				req.session.ip = Token;
				req.session.username = doc.username;
				req.session.roleID = doc.roleID;
				res.locals.user = doc.name;
				delete req.session.errorcontrol;
				return res.status(200).send(endResult)
			});
		}
	});
   }
   else
   {
      return res.status(405).send('Unable to find user');
   }
});

/* Get a station by IP. */
router.get('/API/packStation/',requireLogin, function (req, res)
{
	var station = req.session.ip;

	PackStationModel.findOne({stationIP: station}, function (error, doc)
	{
		if (error || !doc)
		{
			return res.status(200).send({NoStation: true, ScanMode: false})
		}
		else
		{
			var now = new Date();
			doc.active = true
			doc.updated_at = now;
			doc.save(function (error, savedData)
			{
				var ReturnData =
				{
					stationNumber: savedData.stationNumber,
					currentTote: savedData.currentTote,
					user: req.session.user
				}
				return res.status(200).send(ReturnData)
			})
		}
	})
})

router.get('/getLoggedInUser', function(req, res) {
	if(!req.session.user){
		res.send({code:'01', message:'User session not found.'})
	} else {
		console.log('SESSION: ' + JSON.stringify(req.session))
		res.send({code:'00', message:'success'})
	}
})

router.post('/removeUser/:ToDo', function(req, res) {
	var username = req.params.ToDo

	console.log('Username: ' + username)
	UserModel.remove({username: username}, function(error, docs) {
		if(!error && docs) {
			res.send({code:'00', message:'success', data:docs})
		} else {
			res.send({code:'01', message:'Failed to remove the user'})
		}
	})
})

router.post('/updateUser/:ToDo', function(req, res) {
	var userData = JSON.parse(req.params.ToDo)
	var roleID = 1
	if(userData.selected == 'Admin') {roleID = 2} else if(userData.selected == 'Supervisor') {roleID = 3} else {roleID = 1}

	UserModel.findOne({username:userData.oldUsername}, function(error, data) {
		if(!error && data) {
			data.name 		= userData.name,
			data.surname 	= userData.surname,
			data.password 	= userData.password,
			data.username 	= userData.username,
			data.roleID 	= roleID

			data.save(function(err, docs) {
				if(!err && docs) {
					res.send({code:'00', message:'success', docs})
				} else {
					res.send({code:'01', message:'Failed to save user updates.'})
				}
			})
		} else {
			res.send({code:'01', message:'User details were not found.'})
		}
	})
})

/*=================== Functions =================================================*/

function requireLogin (req, res, next){
  if(!req.session.user){
     return res.status(200).send({NoUser:true})
  } else {
    next();
  }
} /* requireLogin */

function WriteToFile(Msg)
{
	if(Msg)
	{
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' ' + Msg);
	}
} /* WriteToFile */

function failedLogin(username){
	if(username){
		UserModel.findOne({username:username}, function(error, data){
			if(!error && data){
				data.failedLogin += 1

				data.save()
			}
		})
	}
}

function clearFailed(name){
	if(name){
		UserModel.findOne({username:name}, function(error, data){
			if(!error && data){
				data.failedLogin = 0

				data.save()
			}
		})
	}
}

/* Get a station by IP. */
router.get('/API/qcStation/', requireLogin, function (req, res){
	let station = req.session.ip;

	console.log(station);

	QcStationModel.findOne({stationIP: station}, function (error, doc){
		if (error || !doc){
			return res.status(200).send({NoStation: true, ScanMode: false})
		} else {
			let now = new Date();
			doc.active = true;
			doc.updated_at = now;
			doc.save(function (error, savedData){
				let ReturnData = {
					stationNumber   : savedData.stationNumber,
					currentContainer: savedData.ContainerID,
					user            : req.session.user
				}
				return res.status(200).send(ReturnData);
			});
		}
	});
});

router.get('/API/LoggedInUser/', requireLogin, function (req, res){
	let Ip = req.session.ip;
	let user = req.session.user;
	let roleID = req.session.roleID;

	let ReturnData = {
		Ip: Ip,
		user: user,
		roleID: roleID
	}
	return res.status(200).send(ReturnData);
});
/*=================== Functions End =============================================*/

module.exports = router


