/*
 * @Author: Morne Ausmeier
 * @Date: 2017-03-10 14:27:30
 * @Last Modified by: Morne Ausmeier
 * @Last Modified time: 2017-03-20 16:49:14
 */

var apexLib = require('apex_lib/moduleIndex.js')
var express = require('express')
var router = express.Router()

// Get all the User roles.
router.get('/API/userRole/', function (req, res) {
    apexLib.mySQL.query.execute(global.mySQLClient, 'call user_role_get();', '', function (error, rows) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send(rows[0])
        }
    })
})

// Get a user role per ID.
router.get('/API/userRole/:id', function (req, res) {
    apexLib.mySQL.query.execute(global.mySQLClient, 'call user_role_get_id(?);', req.params.id, function (error, rows) {
        if (error) {
            return res.status(405).send(error)
        } else {
            if (rows[0].length === 0) return res.status(405).send('No User Role Found.')
            else return res.status(200).send(rows[0])
        }
    })
})

// Post a user role.
router.post('/API/userRole/', function (req, res) {
    var name = req.body.name
    var description = req.body.description

    var parameters = [name, description]

    apexLib.mySQL.query.execute(global.mySQLClient, 'call user_role_insert(?, ?);', parameters, function (error, rows) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send('Success')
        }
    })
})

// Update a user role.
router.put('/API/userRole/', function (req, res) {
    var id = req.body.id
    var name = req.body.name
    var description = req.body.description

    var parameters = [id, name, description]

    apexLib.mySQL.query.execute(global.mySQLClient, 'call user_role_update(?, ?, ?);', parameters, function (error, rows) {
        if (error) {
            return res.status(405).send(error)
        } else {
            return res.status(200).send('Success')
        }
    })
})

// Delete a user.
router.delete('/API/userRole/', function (req, res) {
    var id = req.body.id

    apexLib.mySQL.query.execute(global.mySQLClient, 'call user_role_delete(?);', id, function (error, rows) {
        if (error) {
            return res.status(405).send(error)
        } else {
            if (rows.affectedRows > 0) return res.status(200).send('Success')
            else return res.status(405).send('No record to delete.')
        }
    })
})

module.exports = router
