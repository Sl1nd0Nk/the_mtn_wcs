var express = require('express')
var PickList = require('pickListModel')

var router = express.Router()

router.post('/API/getOrderDetails/:ToDo', function (req, res) {
    var orderID = req.params.ToDo
    console.log('*************ORDER ID: ', orderID)
    PickList.find({'orders.orderID': orderID}, function (err, data) {
        if (!err && data) {
			let OrderData = [];
			if(data.length > 0){
				let x = 0;
				while(x < data.length){
					let PL = data[x];
					let Consolidated  = 'NO';
					let order = null;

					if(PL.orders.length > 1){
						Consolidated  = 'YES';
						let y = 0;
						while(y < PL.orders.length){
							if(PL.orders[y].orderID == orderID){
								order = PL.orders[y];
								break;
							}
							y++;
						}
					} else {
						order = PL.orders[0];
					}

					let toteID = PL.toteID;
					let CartonNr = '';
					if(order.cartonID){
						toteID = PL.FromTote;
						CartonNr = order.toteNumber;
					}

					let OrdData = {
						ToteUsed: toteID,
						CartonID: order.cartonID,
						CartonNr: CartonNr,
						Consolidated: Consolidated,
						TransporterUsed: order.transporterID,
						OrderID: order.orderID,
						PickListID: order.pickListID,
						ItemsCount: order.items.length,
						OrderType: order.orderType,
						Courier: order.courierID,
						Status: PL.Status,
						CreatedDate: PL.CreateDate,
						PackedDate: PL.PackedDate,
						Station: PL.toteInUseBy,
						Items: order.items
					}

					OrderData.push(OrdData);

					x++;
				}

				res.send({data: OrderData})
			} else {
				res.send({data: 'Order Not Found'});
			}
            //res.send({code: '00', message: 'success', data: data})
        } else {
            res.send({data: err});
        }
    })
})

module.exports = router
