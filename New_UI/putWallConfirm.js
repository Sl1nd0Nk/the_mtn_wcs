var express        = require("express");
var bodyParser     = require("body-parser");
var mongoose       = require("mongoose");
var moment         = require('moment')
var redis          = require('redis');

var mongoUrl       = require('ui').mongo.url;
var packStnLib	   = require('classPackStation');
var PwLib          = require('classPutwallLocs');
var logLib	       = require('classLogging');
var serialLib 	   = require('classSerial');
var paramLib       = require('classParams');
var prnLib		   = require('classPrinting');
var pickListLib	   = require('classPicklist');
var pwStnLib	   = require('classPullwallStn');

var packStn        = new packStnLib();
var PwLocs         = new PwLib();
var log            = new logLib();
var serls          = new serialLib();
var param          = new paramLib();
var prn            = new prnLib();
var pickList       = new pickListLib();
var PwStn          = new pwStnLib();

var Pubclient      = redis.createClient();
var app            = express();

/********************* DB Connection ***********************************/

mongoose.connect(mongoUrl, function(err){
	if(err){
		log.WriteToFile(null, 'MONGO CONNECT ERROR ' + err);
	} else {
		log.WriteToFile(null,'Connected To Mongo');
		app.listen(8899, function(){
			log.WriteToFile(null,'PUT WALL CONFIRM LISTENING ON PORT 8899');
		});
	}
}); /* mongoose.connect */

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.text())

app.post('/buttons', function(req, res){
	let body = req.body;
	let light = body.split('-');
	log.WriteToFile(null, 'Listener received... ' + light[0]);
	DeactivateReceivedLight(light[0], function(){
		log.WriteToFile(null, 'Done');
	});

	res.end();
});

function ProcessInvoiceDocVerify(PWLOC, PL, Station, Data, callback){
	DocumentVerifyUpdateToPickList(PWLOC, PL, Data, function(Resp){
		Station.pickList = Resp.PL;
		packStn.Update(Station, function(Resp){
			let Msg = 'Document Validation: order ' + Data.Document + ' successfully validated against printed documents';
			log.WriteToFile(null, Msg);

			PwLocs.UpdateLocationToBeConfirmed(PWLOC, false, function(Resp){
				return callback();
			});
		});
	});
} /* ProcessInvoiceDocVerify */

function FindPackStationAndProcessReceivedData(PWLOC, Data, callback){
	packStn.GetStation({'stationNumber': Data.PackStation}, function(Resp){
		if(Resp.Err){
			log.WriteToFile(null,'Failed to find station for data | Err ' + Resp.Err);
			return callback();
		}

		let Station = Resp.Station;
		let PL = Station.pickList;
		if(!Data.Serial){
			if(!Data.Document){
				log.WriteToFile(null, 'location ' + location + '. Invalid Data received');
				return callback();
			}

			ProcessInvoiceDocVerify(PWLOC, PL, Station, Data, function(){
				return callback();
			});

			return;
		}

		if(Data.Serial != '' && Data.Serial.length > 0){
			SerialUpdateToPickList(PWLOC, PL, Data, function(){
				return callback();
			});

			return;
		}

		NonSerialUpdateToPickList(PWLOC, PL, Data, function(Resp){
			Station.pickList = Resp.PL;
			packStn.Update(Station, function(Resp){
				PwLocs.UpdateLocationToBeConfirmed(PWLOC, false, function(Resp){
					return callback();
				});
			});
		});
	});
} /* FindPackStationAndProcessReceivedData */

function CheckPullAddress(location, callback){
	PwLocs.FindOne({'pullAddress': location}, function(Resp){
		let PWLOC = Resp.pwLoc;
		if(Resp.Err){
			log.WriteToFile(null, Resp.Err);
			return callback();
		}

		if(!PWLOC){
			log.WriteToFile(null, 'Invalid location ' + location + ' received');
			return callback();
		}

		log.WriteToFile(null, 'Found Location ' + location);

		if(!PWLOC.orderID){
			log.WriteToFile(null, 'No order at location ' + location);
			return callback();
		}

		PwStn.FindOne({'Location': location}, function(Resp){
			let PWSTN = Resp.pwStn;
			if(Resp.Err){
				log.WriteToFile(null, 'Failed to find putwall station for location ' + location + '. Err: ' + Resp.Err);
				return callback();
			}

			if(!PWSTN){
				log.WriteToFile(null, 'Failed to find putwall station for location ' + location);
				return callback();
			}

			pickList.FindOne({'orders.orderID': PWLOC.orderID}, function(Resp){
				let PL = Resp.PL;
				if(Resp.Err){
					log.WriteToFile(null, 'Failed to find picklist for order ' + PWLOC.orderID + ' in location ' + location + '. Err: ' + Resp.Err);
					return callback();
				}

				if(!PL){
					log.WriteToFile(null, 'Failed to find picklist for order ' + PWLOC.orderID + ' in location ' + location);
					return callback();
				}

				param.GetGeneratedCartonID(function(ParamData){
					if(ParamData.error) {
						log.WriteToFile(null, 'Failed to find parameters for carton id generation. Err: ' + ParamData.error);
						return callback();
					}

					let JustPrint = true;
					if(!PL.toteID){
						JustPrint = false;
					} else {
						if(parseInt(PL.toteID[0]) != 1){
							JustPrint = false;
						}
					}

					if(JustPrint == false){
						PL.toteID = ParamData.CartonID;
						PL.orders[0].cartonID = ParamData.CartonID;

						log.WriteToFile(null, 'Picklist for order ' + PWLOC.orderID + ' has a count of ' + PL.orders.length);
						log.WriteToFile(null, 'Printing Bagger For Carton ' + PL.toteID);

						pickList.Update(PL, function(Resp){
							prn.PrintToAutobagger(PWSTN.printer, PWLOC.packStation, PL, function(Result){
								if(Result.Msg){
									log.WriteToFile(null, Result.Msg);
									return callback();
								}

								log.WriteToFile(null, 'Bagger Printed for carton ' + PL.toteID + ' order ' + PL.orders[0].orderID);
								PwLocs.ClearPutWallLoc(PWLOC, function(Resp){
									PWSTN.Location = null;
									PwStn.Update(PWSTN, function(Resp){
										return callback();
									});
								});
							});
						});
					} else {
						prn.PrintToAutobagger(PWSTN.printer, PWLOC.packStation, PL, function(Result){
							if(Result.Msg){
								log.WriteToFile(null, Result.Msg);
								return callback();
							}

							PwLocs.ClearPutWallLoc(PWLOC, function(Resp){
								PWSTN.Location = null;
								PwStn.Update(PWSTN, function(Resp){
									return callback();
								});
							});
						});
					}
				});
			});
		});
	});
} /* CheckPullAddress */

function DeactivateReceivedLight(location, callback){
	PwLocs.FindOne({'putAddress': location}, function(Resp){
		let PWLOC = Resp.pwLoc;

		if(Resp.Err || !PWLOC){
			CheckPullAddress(location, function(){
				return callback();
			});
			return;
		}

		if(!PWLOC.active){
			log.WriteToFile(null, 'location ' + location + ' is not active');
			return callback();
		}

		if(!PWLOC.Data){
			log.WriteToFile(null, 'location ' + location + ' has no data');
			return callback();
		}

		let Data = JSON.parse(PWLOC.Data);

		FindPackStationAndProcessReceivedData(PWLOC, Data, function(){
			return callback();
		});
	});
} /* DeactivateReceivedLight */

function DocumentVerifyUpdateToPickList(PWLOC, PL, Data, callback){
	let x = 0;
	while(x < PL.orders.length){
		if(PL.orders[x].orderID == Data.Document){
			break;
		}
		x++;
	}

	if(x < PL.orders.length){
		PL.orders[x].orderVerified = true;
	}

	return callback({PL: PL});

} /* DocumentVerifyUpdateToPickList */

function NonSerialUpdateToPickList(PWLOC, PL, Data, callback){
	let x = 0;
	while(x < PL.orders.length){
		if(PL.orders[x].orderID == PWLOC.orderID){
			break;
		}
		x++;
	}

	if(x < PL.orders.length){
		let y = 0;
		let QtyPacked = null;
		while(y < PL.orders[x].items.length){
			QtyPacked = PL.orders[x].items[y].qtyPacked;
			if(QtyPacked == null){
				QtyPacked = 0;
			}

			let QtyRem = PL.orders[x].items[y].pickQty - QtyPacked;
			if(Data.Sku == PL.orders[x].items[y].sku && QtyRem >= Data.Quantity && !PL.orders[x].items[y].packed){
				break;
			}
		}

		if(y < PL.orders[x].items.length){
			Qtypacked = PL.orders[x].items[y].qtyPacked;
			if(Qtypacked == null){
				Qtypacked = 0;
			}

			Qtypacked += Data.Quantity;
			PL.orders[x].items[y].qtyPacked = Qtypacked;
			PL.orders[x].picklistWeight += Data.SerialWeight;

			Msg = 'Non Serialised Scan: Station: ' + PWLOC.packStation + ' | User ' + Data.User + ' added 1 to non-serialised product ' + PL.orders[x].items[y].itemCode + ' in order ' + PL.orders[x].orderID;
			log.WriteToFile(null, Msg);

			if(QtyPacked == PL.orders[x].items[y].pickQty){
				PL.orders[x].items[y].packedBy = Data.User;
				PL.orders[x].items[y].packed = true;
			}

			let AllItemsPacked = true;
			y = 0;
			while(y < PL.orders[x].items.length){
				if(PL.orders[x].items[y].packed == false){
					AllItemsPacked = false;
					break;
				}
				y++;
			}

			if(AllItemsPacked){
				PL.orders[x].packed = true;
				Msg = "Station: " + PWLOC.packStation + " | Order " + PL.orders[x].orderID + " in tote " + PL.toteID + " is fully packed";
				log.WriteToFile(null, Msg);
			}
		}
	}

	return callback({PL: PL});
} /* NonSerialUpdateToPickList */

function SerialUpdateToPickList(PWLOC, PL, Data, callback){
	let QtyPacked = null;
	let x = 0;
	while(x < PL.orders.length){
		if(PL.orders[x].orderID == PWLOC.orderID){
			break;
		}
		x++;
	}

	if(x >= PL.orders.length){
		return callback();
	}

	let y = 0;
	while(y < PL.orders[x].items.length){
		QtyPacked = PL.orders[x].items[y].qtyPacked;
		if(QtyPacked == null){
			QtyPacked = 0;
		}

		let QtyRem = PL.orders[x].items[y].pickQty - QtyPacked;

		if(Data.Sku == PL.orders[x].items[y].sku && QtyRem >= Data.Quantity && !PL.orders[x].items[y].packed){
			let InfoMsg = "Station: " + PWLOC.packStation + " | Tote " + Data.ToteID + " | Order " + PWLOC.orderID + " | SKU: " + Data.Sku + " | Remaining Qty: " + QtyRem;
			log.WriteToFile(null, InfoMsg);
			break;
		}

		y++;
	}

	if(y >= PL.orders[x].items.length){
		return callback();
	}

	packStn.GetStation({'stationNumber': PWLOC.packStation}, function(Resp){
		if(Resp.Err){
			log.WriteToFile(null,'Failed to find station for data | Err ' + Resp.Err);
			return callback();
		}

		let Station = Resp.Station;

		QtyPacked += Data.Quantity;
		Station.pickList.orders[x].items[y].qtyPacked = QtyPacked;
		Station.pickList.orders[x].items[y].serials.push(Data.Serial);
		Station.pickList.orders[x].picklistWeight += Data.SerialWeight;

		let ItemIndex = y;

		if(QtyPacked == Station.pickList.orders[x].items[y].pickQty){
			Station.pickList.orders[x].items[y].packedBy = Data.User;
			Station.pickList.orders[x].items[y].packed = true;
		}

		let AllItemsPacked = true;
		y = 0;
		while(y < Station.pickList.orders[x].items.length){
			if(Station.pickList.orders[x].items[y].packed == false){
				AllItemsPacked = false;
				break;
			}
			y++;
		}

		if(AllItemsPacked){
			Station.pickList.orders[x].packed = true;
		}

		packStn.Update(Station, function(Resp){
			if(Resp.Err){
				let ErrMsg = "Station: " + PWLOC.packStation + " | Failed to update serial to station. Err: " + Resp.Err;
				log.WriteToFile(null, ErrMsg);
			} else {
				let Msg = "Station: " + PWLOC.packStation + " | User " + Data.User + " added serial number " + Data.Serial + " from tote " + Data.ToteID + " to order " + PWLOC.orderID;
				log.WriteToFile(null, Msg);
			}

			if(AllItemsPacked){
				let AllPackedMsg = "Station: " + PWLOC.packStation + " | Order " + Station.pickList.orders[x].orderID + " in tote " + Station.pickList.toteID + " is fully packed";
				log.WriteToFile(null, AllPackedMsg);
			}

			serls.GetSerial(Data.Serial, function(Resp){
				let SerialRec = Resp.Ser;
				serls.UpdateScannedSerialToDispatched(SerialRec, Station.pickList.orders[x], ItemIndex, null, Data.User, function(UpdateResp){
					if(UpdateResp.Err){
						log.WriteToFile(null,'Error while updating serial number ' + UpdateResp.serial + ' in serial table');
					} else {
						log.WriteToFile(null,'Successfully dispatched serial ' + UpdateResp.SavedDoc.serial + ' in serial table');
					}

					PwLocs.UpdateLocationToBeConfirmed(PWLOC, false, function(Resp){
						return callback();
					});
				});
			});
		});
	});
} /* SerialUpdateToPickList */
