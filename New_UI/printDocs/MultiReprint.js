var redis = require('redis');
var mongoose       = require('mongoose');
mongoose.Promise = require('bluebird');

var Pubclient = redis.createClient();

var mongooseSchema = mongoose.Schema // MongoDB Schema class object.

var MultiPrint = function () {}

MultiPrint.prototype.multiPrint = function (uiOrderInfo) {
  var packListMongoSchema = mongooseSchema(
    {
      cartonId: {type: mongooseSchema.Types.String, default: null},
      carton: {type: mongooseSchema.Types.String, default: null},
      transporterId: {type: mongooseSchema.Types.String, default: null},
      cartons: [
          {
            orderID: {type: mongooseSchema.Types.String, default: null},
            cartonID: {type: mongooseSchema.Types.String, default: null},
            courierID: {type: mongooseSchema.Types.String, default: null},
            packingWeight: {type: mongooseSchema.Types.Number, default: null},
            actualWeight: {type: mongooseSchema.Types.String, default: null},
            rejected: {type: mongooseSchema.Types.Boolean, default: false},
            complete: {type: mongooseSchema.Types.Boolean, default: false},
            weight: {type: mongooseSchema.Types.Number, default: null},
            items:
            [
              {
                  itemCode: {type: mongooseSchema.Types.String, default: null},
                  itemDescription: {type: mongooseSchema.Types.String, default: null},
                  eachWeight: {type: mongooseSchema.Types.Number, default: null},
                  qty: {type: mongooseSchema.Types.Number, default: null},
                  pickQty: {type: mongooseSchema.Types.Number, default: null},
                  location: {type: mongooseSchema.Types.String, default: null},
                  pickZone: {type: mongooseSchema.Types.String, default: null},
                  picked: {type: mongooseSchema.Types.Boolean, default: false},
                  packed: {type: mongooseSchema.Types.Boolean, default: false},
                  shortPick: {type: mongooseSchema.Types.Boolean, default: false},
                  serials: [],
                  barcode: {type: mongooseSchema.Types.String, default: null},
                  sku: {type: mongooseSchema.Types.String, default: null},
                  serialised: {type: mongooseSchema.Types.Boolean, default: false}
              }
            ]
          }
        ]
      }
    )
    
    // var packList = mongoose.model('packList', packListMongoSchema)
    var packList = require('packListModel')
    
    var pickListMongoSchema = mongooseSchema(
        {
            toteID: {type: mongooseSchema.Types.String, default: null},
            transporterID: {type: mongooseSchema.Types.String, default: null},
            destination: {type: mongooseSchema.Types.String, default: null},
            packType: {type: mongooseSchema.Types.String, default: null},
            toteInUse: {type: mongooseSchema.Types.Boolean, default: false},
            toteInUseBy: {type: mongooseSchema.Types.String, default: null},
            orders:
            [
                {
                    orderID: {type: mongooseSchema.Types.String, default: null},
                    cartonID: {type: mongooseSchema.Types.String, default: null},
                    toteNumber: {type: mongooseSchema.Types.Number, default:null},
                    numberOfTotes: {type: mongooseSchema.Types.Number, default: 0},
                    pickListID: {type: mongooseSchema.Types.String, default: null},
                    courierID: {type: mongooseSchema.Types.String, default: null},
                    orderType: {type: mongooseSchema.Types.String, default: null},
                    priorityLevel: {type: mongooseSchema.Types.String, default: null},
                    picklistWeight: {type: mongooseSchema.Types.Number, default: null},
                    picklistVolume: {type: mongooseSchema.Types.Number, default: null},
                    picked: {type: mongooseSchema.Types.Boolean, default: false},
                    packed: {type: mongooseSchema.Types.Boolean, default: false},
                    shipped: {type: mongooseSchema.Types.Boolean, default: false},
                    rejected: {type: mongooseSchema.Types.Boolean, default: false},
                    rejectedReason: {type: mongooseSchema.Types.String, default: null},
                    cartonSize: {type: mongooseSchema.Types.Number, default: -1},
                    items:
                    [
                        {
                            itemCode: {type: mongooseSchema.Types.String, default: null},
                            itemDescription: {type: mongooseSchema.Types.String, default: null},
                            eachWeight: {type: mongooseSchema.Types.Number, default: null},
                            qty: {type: mongooseSchema.Types.Number, default: null},
                            pickQty: {type: mongooseSchema.Types.Number, default: null},
                            qtyPacked: {type: mongooseSchema.Types.Number, default: null},
                            location: {type: mongooseSchema.Types.String, default: null},
                            pickZone: {type: mongooseSchema.Types.String, default: null},
                            picked: {type: mongooseSchema.Types.Boolean, default: false},
                            packed: {type: mongooseSchema.Types.Boolean, default: false},
                            shortPick: {type: mongooseSchema.Types.Boolean, default: false},
                            serials: [],
                            barcode: {type: mongooseSchema.Types.String, default: null},
                            sku: {type: mongooseSchema.Types.String, default: null},
                            serialised: {type: mongooseSchema.Types.Boolean, default: false},
                            pickedBy: {type: String, default: null},
                            packedBy: {type: String, default: null},
                            unitPrice: {type: Number, default: null},
                            uom: {type: String, default: null},
                            orderLine: {type: Number, default: null},
                            pickListOrderLine: {type: Number, default: null},
                            eachVolume: {type: Number, default: null}
                        }
                    ]
                }
            ],
        PackedDate: {type: mongooseSchema.Types.String, default: null},
        Status : {type: mongooseSchema.Types.String, default: null},
        FromTote : {type: mongooseSchema.Types.String, default: null},
        PickUpdateRequired: {type: mongooseSchema.Types.Boolean, default: false},
        PackUpdateRequired: {type: mongooseSchema.Types.Boolean, default: false},
        PickListType: {type: mongooseSchema.Types.String, default: null}
        }
    )
    
    // var pickList = mongoose.model('pickList', pickListMongoSchema)
    var pickList = require('pickListModel')
    
    function ProcessReprint(CartonArr)
    {
            var Arr = [];
    
            Arr.push(CartonArr);
    
            if(Arr.length > 0)
            {
      var PackList = false;
      var cursor = packList.find({'cartons.cartonID':{$in:Arr}}).cursor();
    
      cursor.on('data', function(detail)
      {
        console.log(detail);
        var Testdata =
        {
          orderId: detail.cartons[0].orderID,
          printerName: "qdp2018",
          labelprinterName: "qlp2018",
          items: detail.cartons[0].items,
          cartonId: detail1.orders[0].cartonID,
          courierId: detail.cartons[0].courierID,
          station: 36,
          CartonNo:1,
          NoOfCartons:1,
          CartionWeight:0, //detail.cartons[0].weight,
          LastCarton:true,
          FirstCarton:true,
          orderType: null
        }
    
        console.log("Publishing.... to Station 36");
        Pubclient.publish('PACKING_36', JSON.stringify(Testdata));
    
      });
    
      cursor.on('end', function(detail)
      {
        console.log("DONE");
        if(PackList === false)
        {
          var cursor1 = pickList.find({'orders.orderID':{$in:Arr}}).cursor();
                            var Count = 1;
    
          cursor1.on('data', function(detail1)
          {
                              cursor1.pause();
                              pickList.find({'orders.orderID': detail1.orders[0].orderID}, function(err, docs)
                              {
                                if(docs)
                                {
                                   //console.log(docs);
                                   var items = [];
                                   var Cartons = [];
                                   var HighestNumber = 0
                                   if(docs.length === 1)
                                   {
                                     //items = detail1.orders[0].items;
                                      var fff = 0
                                      while(fff < detail1.orders[0].items.length)
                                      {
                                         items.push(detail1.orders[0].items[fff])
                                         Cartons.push(detail1.orders[0].cartonID)
                                         fff++
                                      }
    
                                   }
                                   else
                                   {
                                        var docCount = 0;
                                        //var HighestNumber = 0
                                        while(docCount < docs.length)
                                        {
                                           // if(toteFromPicklists.orders[0].pickListID === docs[docCount].orders[0].pickListID && toteFromPicklists.toteID != docs[docCount].toteID)
                                           // {
                                           //     DuplicateOrder = true
                                           // } 
                                                                               
                                            //var ndx = 0;
                                            //while(ndx < docs.data.length)  
                                            //{
                                               var jdx  = 0
                                               while(jdx < docs[docCount].orders[0].items.length)
                                               {
                                                  if(docs[docCount].orders[0].items[jdx].qtyPacked != null)
                                                  {
                                                     items.push(docs[docCount].orders[0].items[jdx]);
                                                     Cartons.push(docs[docCount].orders[0].cartonID);
                                                  }
    
                                                  if(docs[docCount].orders[0].toteNumber > 0 && docs[docCount].orders[0].toteNumber > HighestNumber)
                                                  { 
                                                    HighestNumber = docs[docCount].orders[0].toteNumber;
                                                  }
    
                                                  jdx++
                                                }
                                              //  ndx++
                                            //} 
    
                                             docCount++
                                        }
            }
    
                                    var toteNum = HighestNumber + 1;
                                    var Last = false;
                                    var first = false;
    
                                    if(detail1.orders[0].toteNumber > 0)
                                    {
                                      toteNum = detail1.orders[0].toteNumber;
                                    }
                                    else
                                    {
                                      detail1.orders[0].toteNumber = toteNum; 
                                      detail1.save();
                                    }
    
     
                                    if(toteNum == docs.length)
                                    {
                                       Last = true;
                                    }
                                    if(detail1.orders[0].toteNumber == 1)
                                    {
                                       first = true;
                                    }
                                    //console.log(detail1);
                                    console.log("Printing No " + Count++);
                                    var Testdata =
                                    {
                                            orderId: detail1.orders[0].orderID,
                                            printerName: "qdp2018",
                                            labelprinterName: "qlp2018",
                                            items: items,
                                            ErpItems: detail1.orders[0].items,
                                            cartonId: detail1.orders[0].cartonID,
                                            courierId: detail1.orders[0].courierID,
                                            station: 36,
                                            CartonNo:toteNum,
                                            NoOfCartons: docs.length,
                                            CartionWeight:0, //detail1.orders[0].picklistWeight,
                                            LastCarton:Last,
                                            FirstCarton:first,
                                            Cartons: Cartons,
                                             PackedDate:detail1.PackedDate,
                                            orderType: null
                                    }
    
                                    console.log("Publishing from Picklist .... to Station 36");
                                    Pubclient.publish('PACKING_36', JSON.stringify(Testdata));
            
                                    setTimeout(function()
                                    {
                                console.log("===== 5 seconds later ========");
                                console.log(" ");
                                             //GetPickedPacked();
                                            cursor1.resume();
                               }, 5000);
                                   
    
                                 }
                                });
                                    /*var Last = false;
                                    var first = false;
                                    if(detail1.orders[0].toteNumber == detail1.orders[0].numberOfTotes)
                                    {
                                       Last = true;
                                    }
                                    if(detail1.orders[0].toteNumber == 1)
                                    {
                                       first = true;
                                    }
            console.log(detail1);
            var Testdata =
            {
              orderId: detail1.orders[0].orderID,
              printerName: "qdp2018",
              labelprinterName: "qlp2018",
              items: detail1.orders[0].items,
              cartonId: detail1.orders[0].cartonID,
              courierId: detail1.orders[0].courierID,
              station: 36,
              CartonNo:detail1.orders[0].toteNumber,
              NoOfCartons:detail1.orders[0].numberOfTotes,
              CartionWeight:0, //detail1.orders[0].picklistWeight,
              LastCarton:Last,
              FirstCarton:first,
              orderType: null
            }
    
            console.log("Publishing from Picklist .... to Station 36");
            Pubclient.publish('PACKING_36', JSON.stringify(Testdata));*/
    
          });
    
          cursor1.on('end', function()
                      {
            console.log("Done 1");
          });
        }
      });
          }
          else
          {
            console.log("Nothing To Print");
          }
    }

    console.log('Connected To Mongo');
    
    console.log("Entered Carton: " + uiOrderInfo)
    ProcessReprint(uiOrderInfo);
} 

module.exports = new MultiPrint()



