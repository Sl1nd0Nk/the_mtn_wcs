var request     = require('request');
var moment      = require('moment');
var formidable  = require("formidable");

var logLib      = require('classLogging');
var log         = new logLib();

var pullWallUrl = require('ui').pullWall;
/*======================================= End Of Private ===========================================================*/

module.exports = class AIOI {
    constructor(){}

	LightupLoc(Lane, color, StationNumber, callback){
		var Msg = null;

		if(StationNumber % 2 == 0){
		  	var Url = pullWallUrl.urlRightController;
		} else {
			var Url = pullWallUrl.urlLeftController;
		}

		request({
			method:'POST',
			url: Url,
			body:Lane + color,
			headers: {'Content-Type': 'text/xml'}
		},function(error, response, body){
			if(error){
				Msg = 'error: ' + error;
				log.WriteToFile(null, Msg);
				return callback({error: Msg});
			} else if(!body){
				Msg = 'there is no body.';
				log.WriteToFile(null, Msg);
				return callback({error: Msg});
			} else {
				Msg = 'body: ' + body;
				log.WriteToFile(null, Msg);
				return callback({error: null});
			}
		});
	} /* LightupLoc */
} /* AIOI */