var packStation = require('packStationModel');
var moment      = require('moment');
var formidable  = require("formidable");

function _Find(KeyValuePair, callback){
	packStation.find(KeyValuePair, function(err, PackingStnArr){
		if(err){
			return callback({Err: err});
		} else {
			if(PackingStnArr.length > 0){
				return callback({PackingStnArr: PackingStnArr});
			} else {
				return callback({PackingStnArr: null});
			}
		}
	});
} /* _Find */

function _FindOne(KeyValuePair, callback){
	var ReturnFields = {'pickList':1, 'ToteType':1, 'NumOrders':1,
						'CurrentOrder':1, 'currentTransporter':1,
						'autobagPrinterQName':1, 'labelPrinterQName':1,
						'stationNumber':1};

	packStation.find(KeyValuePair).limit(1).exec(function(err, StationArr){
		if(err){
			return callback({Err: err});
		} else {
			var Station = StationArr[0];
			if(Station){
				return callback({Station: Station});
			} else {
				return callback({Station: null});
			}
		}
	});
} /* _FindOne */

function _Update(Obj, callback){
	Obj.updated_at = new Date();
	Obj.markModified('pickList');
	Obj.save(function(err, SaveDoc){
		return callback({Err: err, SavedDoc: SaveDoc});
	});
} /* _Update */

function _CreateNewStation(Object, callback){
	packStation.create(Object, function(err, SavedDoc){
		if(err){
			return callback({Err: err});
		} else {
			if(SavedDoc){
				return callback({SavedDoc: SavedDoc});
			} else {
				return callback({SavedDoc: null});
			}
		}
	});
} /* _CreateNewStation */

function _CheckIfOrderFullyPacked(Order){
	var x = 0;
	while(x < Order.items.length){
		if(Order.items[x].packed != true){
			break;
		}
		x++;
	}

	if(x < Order.items.length){
		return false;
	} else {
		return true;
	}
} /* _CheckIfOrderFullyPacked */

function _ResetPackStationValues(Station){
	Station.pickList = null;
	Station.currentTransporter = null;
	Station.NumOrders = null;
	Station.CurrentOrder = null;
	Station.ToteType = null;
	Station.OrderIndex = null;
	Station.ProcessOnPutWalls = null;
	Station.CartonSizes = null;
	Station.ProcUsingDesktopAutoBag = null;
	Station.ProcessOnPutWalls = null;

	return Station;
} /* _ResetPackStationValues */

function _DetermineStationOrderData(Station, callback){
	if(Station.pickList){
		if(Station.pickList.orders.length > 0){
			Station.NumOrders = Station.pickList.orders.length;

			let cnt = 0;
			while(cnt < Station.pickList.orders.length){
				if(Station.pickList.orders[cnt].packComplete == false){
					break;
				}
				cnt++;
			}

			if(cnt < Station.pickList.orders.length){
				Station.CurrentOrder = cnt+1;
				Station.OrderIndex = cnt;
			} else {
				Station.CurrentOrder = Station.NumOrders;
				Station.OrderIndex = Station.NumOrders -1;
			}

			_Update(Station, function(Resp){
				if(Resp.Err){
					return callback({Station: Station});
				} else {
					return callback({Station: Resp.SavedDoc});
				}
		   });
	   } else {
		 	return callback({Station: Station});
	   }
   } else {
	   return callback({Station: Station});
   }
} /* _DetermineStationOrderData */

module.exports = class PackStation {
    constructor(){}

    New(Object, callback){
		_CreateNewStation(Object, function(Resp){
			return callback(Resp);
		});
	} /* New */

	Find(KeyValuePair, callback){
		_Find(KeyValuePair, function(Resp){
			return callback(Resp);
		});
	} /* Find */

	FindOne(KeyValuePair, callback){
		_FindOne(KeyValuePair, function(Resp){
			return callback(Resp);
		});
	} /* FindOne */

	Update(Obj, callback){
		_Update(Obj, function(Resp){
			return callback(Resp);
		});
	} /* Update */

	UpdateStationType(Station, Valid, callback){
		Station.ProcessOnPutWalls = Valid;

		if(Valid == true){
			var PL = Station.pickList;
			var x = 0;
			while(x < PL.orders.length){
				PL.orders[x].cartonSize = 1;
				x++;
			}

			Station.pickList = PL;
		}

		_Update(Station, function(Resp){
			return callback({Station: Resp.SavedDoc});
		});
	} /* UpdateStationType */

	CheckIfCartonUsedAtAnyStation(Carton, callback){
		_Find({'pickList.orders.cartonID':Carton}, function(Resp){
			if(Resp.Err){
				//log.WriteToFile(null, Resp.Err);
				return callback({Msg: 'Internal Server Error'});
			} else {
				var PLArr = Resp.PackingStnArr;
				if(PLArr){
					if(PLArr.length <= 0){
						return callback({Msg: null});
					} else {
						return callback({Msg: 'Scanned Carton ' + Carton + ' has been used before'});
					}
				} else {
					return callback({Msg: null});
				}
			}
		});
	} /* CheckIfCartonUsedAtAnyStation */

	CheckIfToteInprogressAtAnyStation(Tote, callback){
		_Find({'pickList.toteID':Tote}, function(Resp){
			if(Resp.Err){
				//log.WriteToFile(null, Resp.Err);
				return callback({Msg: 'Internal Server Error'});
			} else {
				var PLArr = Resp.PackingStnArr;
				if(PLArr){
					if(PLArr.length <= 0){
						return callback({Msg: null});
					} else {
						return callback({Msg: 'Tote is inprogress at pack station ' + PLArr[0].stationNumber});
					}
				} else {
					return callback({Msg: null});
				}
			}
		});
	} /* CheckIfToteInprogressAtAnyStation */

	CheckIfOrderFullyPacked(Order){
		return _CheckIfOrderFullyPacked(Order);
	} /* CheckIfOrderFullyPacked */

	ResetPackStationValues(Station){
		return _ResetPackStationValues(Station);
	} /* ResetPackStationValues */

	GetStation(KeyValuePair, callback){
		_FindOne(KeyValuePair, function(Resp){
			//if(Resp.Station){
			//	_DetermineStationOrderData(Resp.Station, function(Resp){
			//		return callback(Resp);
			//	});
			//} else {
				return callback(Resp);
			//}
		});
	} /* GetStation */
} /* PackStation */
