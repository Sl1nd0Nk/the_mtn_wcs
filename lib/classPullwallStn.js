var PwStn       = require('pullWallModel');
var moment      = require('moment');
var formidable  = require("formidable");

function _Find(KeyValuePair, callback){
	PwStn.find(KeyValuePair, function(err, pwStnArr){
		if(err){
			return callback({Err: err});
		} else {
			if(pwStnArr.length > 0){
				return callback({pwStnArr: pwStnArr});
			} else {
				return callback({pwStnArr: null});
			}
		}
	});
} /* _Find */

function _FindOne(KeyValuePair, callback){
	PwStn.findOne(KeyValuePair, function(err, pwStn){
		if(err){
			return callback({Err: err});
		} else {
			if(pwStn){
				return callback({pwStn: pwStn});
			} else {
				return callback({Err: 'Not Found', pwStn: null});
			}
		}
	});
} /* _FindOne */

function _Update(Obj, callback){
	Obj.save(function(err, savedDoc){
		if(err){
			return callback({Err: err});
		} else {
			if(savedDoc){
				return callback({SavedDoc: savedDoc});
			} else {
				return callback({Err: 'Update Failed', SavedDoc: null});
			}
		}
	});
} /* _Update */

module.exports = class PullWallStn {
    constructor(){}

	Find(KeyValuePair, callback){
		_Find(KeyValuePair, function(Resp){
			return callback(Resp);
		});
	} /* Find */

	FindOne(KeyValuePair, callback){
		_FindOne(KeyValuePair, function(Resp){
			return callback(Resp);
		});
	} /* FindOne */

	Update(Obj, callback){
		_Update(Obj, function(Resp){
			return callback(Resp);
		});
	} /* Update */

	FormHTMLReplaceList(){
		var list = {
			ScannedStnID: "<!-- -->",
			ScannedTransporter: "<!-- -->",
			ScannedParcel:"<!-- -->",
			Instruction:"<!-- -->",
			StnID:"<!--",
			EndStnID:"-->",
			TransID:"<!--",
			EndTransID:"-->",
			Parcel:"<!--",
			EndParcel:"-->",
			Confirm:"<!--",
			ConTR:"",
			EndConfirm:"-->",
			ShowAlert: "//",
			endPoint: "login",
			Idle:"<!--",
			EndIdle:"-->",
			Logout:"<!--",
			EndLogout:"-->",
			Input:"<!--",
			EndInput:"-->",
			ShowBBox:"<!--",
			EndShowBBox:"-->",
			RelBtn:"<!--",
			EndRelBtn:"-->"
		}

		return list;
	} /* FormHTMLReplaceList */

} /* PullWallStn */
