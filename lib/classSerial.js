var serial      = require('serialModel');
var moment      = require('moment');
var formidable  = require("formidable");
var async       = require('async');

function _Find(KeyValuePair, callback){
	serial.find(KeyValuePair, function(err, SerArr){
		if(err){
			return callback({Err: err});
		} else {
			if(SerArr.length > 0){
				return callback({SerArr: SerArr});
			} else {
				return callback({SerArr: null});
			}
		}
	});
} /* _Find */

function _Update(Obj, callback){
	Obj.save(function(err, savedDoc){
		if(err){
			return callback({Err: err});
		} else {
			if(savedDoc){
				return callback({SavedDoc: savedDoc});
			} else {
				return callback({Err: 'Update Failed', SavedDoc: null});
			}
		}
	});
} /* _Update */

function _FindSerialCountForEachParent(task, callback){
	let SerialRecNo = task.RecNo;
	serial.find({'parentRecNo': SerialRecNo, 'dispatched': false}).exec(function(err, ChildSerialArr){
		if(!err){
			return callback({ChildSerialArr: null});
		} else {
			return callback({ChildSerialArr: ChildSerialArr});
		}
	});
} /* _FindSerialCountForEachParent */

function _ResetEachSerial(SerArr, index, callback){
	if(index < SerArr.length){
		let SerialRec = SerArr[index];

		SerialRec.dispatched = false;
		SerialRec.salesOrderID = null;
		SerialRec.salesOrderLineID = null;
		SerialRec.cartonID = null;
		SerialRec.serialStatus = 'AVAILABLE';
		SerialRec.userID = null;

		_Update(SerialRec, function(Resp){
			index++;
			_ResetEachSerial(SerArr, index, callback);
		});
	} else {
		return callback();
	}
} /* _ResetEachSerial */

function _GetSerialCountForSerial(SerialRecNo, ReturnFields, UOM, callback){
	serial.find({'parentRecNo': SerialRecNo, 'dispatched': false}, ReturnFields).exec(function(err, SerialRecArr){
		if(!err){
			if(SerialRecArr.length > 0){
				if(SerialRecArr[0].UOM == UOM){
					return callback({SerialCount: SerialRecArr.length});
				} else {
					var q = async.queue(FindSerialCountForEachParent, SerialRecArr.length);
					var SerialCount = 0;

					q.push(SerialRecArr, function (Resp) {
						if(Resp.ChildSerialArr){
							let SerialData = Resp.ChildSerialArr;
							if(SerialData.length > 0){
								if(SerialData[0].UOM == UOM){
									SerialCount += SerialData.length;
								} else {
									q.push(SerialData);
								}
							}
						}
					});

					q.drain = function() {
						return callback({SerialCount: SerialCount});
					}
				}
			} else {
				return callback({SerialCount: 1});
			}
		} else {
			return callback({Err: 'Failed To Find Child Serials using SerialRecNo ' + SerialRecNo + ', please call supervisor'});
		}
	});
} /* _GetSerialCountForSerial */

function _UpdateEachSerial(SerArr, index, OrderData, itemIndex, callback){
	if(index < SerArr.length){
		let SerialRec = SerArr[index];

		SerialRec.cartonID = OrderData.cartonID;
		SerialRec.cartonNo = OrderData.toteNumber;
		SerialRec.noOfCartons = OrderData.numberOfTotes;
		SerialRec.resyncRequired = true;
		SerialRec.dispatched = true;

		SerialRec.salesOrderID = OrderData.orderID;
		SerialRec.salesOrderLineID = OrderData.items[itemIndex].orderLine;
		SerialRec.serialStatus = 'DISPATCHED';
		SerialRec.userID = OrderData.items[itemIndex].packedBy;

		_Update(SerialRec, function(Resp){
			index++;
			_UpdateEachSerial(SerArr, index, OrderData, itemIndex, callback);
		});
	} else {
		return callback();
	}
} /* _UpdateEachSerial */

function _FindSerialByItem(OrderData, index, callback){
	if(index < OrderData.items.length){
		let OrdSerials = OrderData.items[index].serials;
		if(OrdSerials.length > 0){
			_Find({'serial': {$in: OrdSerials}}, function(Resp){
				if(Resp.Err){
					//let Msg = "Failed to find serials for order " + OrderData.orderID + " | picklistID " + OrderData.pickListID;
					//log.WriteToFile(null, Msg);
					index++;
					_FindSerialByItem(OrderData, index, callback);
				} else {
					if(Resp.SerArr){
						_UpdateEachSerial(Resp.SerArr, 0, OrderData, index, function(){
							index++;
							_FindSerialByItem(OrderData, index, callback);
						});
					} else {
						//let Msg = "No serials to update to host for order " + OrderData.orderID + " | picklistID " + OrderData.pickListID;
						//log.WriteToFile(null, Msg);
						index++;
						_FindSerialByItem(OrderData, index, callback);
					}
				}
			});
		} else {
			index++;
			_FindSerialByItem(OrderData, index, callback);
		}
	} else {
		return callback();
	}
} /* _FindSerialByItem */

function _FindSerialNonSyncedByItem(OrderData, index, callback){
	if(index < OrderData.items.length){
		let OrdSerials = OrderData.items[index].serials;
		if(OrdSerials.length > 0){
			_Find({'serial': {$in: OrdSerials}, 'resyncRequired' : false}, function(Resp){
				if(Resp.Err){
					//let Msg = "Failed to find serials for order " + OrderData.orderID + " | picklistID " + OrderData.pickListID;
					//log.WriteToFile(null, Msg);
					index++;
					_FindSerialNonSyncedByItem(OrderData, index, callback);
				} else {
					if(Resp.SerArr){
						_UpdateEachSerial(Resp.SerArr, 0, OrderData, index, function(){
							index++;
							_FindSerialNonSyncedByItem(OrderData, index, callback);
						});
					} else {
						//let Msg = "No serials to update to host for order " + OrderData.orderID + " | picklistID " + OrderData.pickListID;
						//log.WriteToFile(null, Msg);
						index++;
						_FindSerialNonSyncedByItem(OrderData, index, callback);
					}
				}
			});
		} else {
			index++;
			_FindSerialNonSyncedByItem(OrderData, index, callback);
		}
	} else {
		return callback();
	}
} /* _FindSerialNonSyncedByItem */

module.exports = class ClassSerial {
    constructor(){}

	Find(KeyValuePair, callback){
		_Find(KeyValuePair, function(Resp){
			return callback(Resp);
		});
	} /* Find */

	FindOne(KeyValuePair, callback){
		serial.findOne(KeyValuePair, function(err, Ser){
			if(err){
				return callback({Err: err});
			} else {
				if(Ser){
					return callback({Ser: Ser});
				} else {
					return callback({Err: 'Not Found', Ser: null});
				}
			}
		});
	} /* FindOne */

	Update(Obj, callback){
		_Update(Obj, function(Resp){
			return callback(Resp);
		});
	} /* Update */

	GetSerial(ScannedSerial, callback){
		let QueryData = [ScannedSerial, "892700000" + ScannedSerial, "892700008" + ScannedSerial];
		let Query = {'serial': {$in: QueryData}};
		let ReturnFields = {'_id':1, 'serial':1, 'UOM':1, 'SKU':1, 'serialWeight':1, 'serialStatus':1, 'parentRecNo':1, 'RecNo': 1, 'dispatched':1, 'serialCount':1, 'salesOrderID':1, 'loadID':1, 'cartonID':1};
		serial.find(Query, ReturnFields).limit(1).exec(function(err, SerArr){
			if(!err){
				let Ser = SerArr[0];
				if(Ser){
					return callback({Ser: Ser});
				} else {
					return callback({Ser: null});
				}
			} else {
				return callback({Err: err});
			}
		});
	} /* GetSerial */

	UpdateScannedSerialToDispatched(SerialRec, Order, itemIndex, Station, user, callback){
		SerialRec.dispatched = true;
		SerialRec.salesOrderID = Order.orderID;
		SerialRec.salesOrderLineID = Order.items[itemIndex].orderLine;
		SerialRec.cartonID = Order.pickListID;
		SerialRec.serialStatus = 'DISPATCHED';
		SerialRec.userID = user;

		_Update(SerialRec, function(Resp){
			return callback(Resp);
		});
	} /* UpdateScannedSerialToDispatched */

	ResetSerials(OrderID, PickListID, callback){
		_Find({'salesOrderID':OrderID, 'cartonID':PickListID}, function(Resp){
			if(Resp.Err){
				let Msg = "Failed to Reset serials for order " + OrderID + " | picklistID " + PickListID;
				//log.WriteToFile(null, Msg);
				return callback();
			} else {
				if(Resp.SerArr){
					_ResetEachSerial(Resp.SerArr, 0, function(){
						return callback();
					});
				} else {
					let Msg = "No serials to reset for order " + OrderID + " | picklistID " + PickListID;
					//log.WriteToFile(null, Msg);
					return callback();
				}
			}
		});
	} /* ResetSerials */

	UpdateSerialsToHost(OrderData, callback){
		if(OrderData){
			_FindSerialByItem(OrderData, 0, function(Resp){
				return callback();
			});
		} else {
			//let Msg = "All serials for order " + OrderData.orderID + " | picklistID " + OrderData.pickListID + " marked as required to sync to host";
			//log.WriteToFile(null, Msg);
			return callback();
		}
	} /* UpdateSerialsToHost */

	ReUpdateSerialsToHost(OrderData, callback){
		if(OrderData){
			_FindSerialNonSyncedByItem(OrderData, 0, function(Resp){
				return callback();
			});
		} else {
			//let Msg = "All serials for order " + OrderData.orderID + " | picklistID " + OrderData.pickListID + " marked as required to sync to host";
			//log.WriteToFile(null, Msg);
			return callback();
		}
	} /* UpdateSerialsToHost */
} /* ClassSerial */
