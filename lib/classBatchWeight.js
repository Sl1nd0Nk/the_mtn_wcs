var Detail   = require('batchWeightTolerance');

var logging  = require('classLogging');

var log      = new logging();

function _FindOne(KeyValuePair, callback){
	Detail.findOne(KeyValuePair, function(err, batchWeight){
		if(err){
			return callback({Err: err, batchWeight: null});
		} else {
			if(batchWeight){
				return callback({Err: null, batchWeight: batchWeight});
			} else {
				return callback({Err: null, batchWeight: null});
			}
		}
	});
} /* _FindOne */

function _Find(KeyValuePair, callback){
	Detail.find(KeyValuePair, function(err, batchWeightArr){
		if(err){
			return callback({Err: err});
		} else {
			if(batchWeightArr.length > 0){
				return callback({Err: null, batchWeightArr: batchWeightArr});
			} else {
				return callback({Err: null, batchWeightArr: null});
			}
		}
	});
} /* _Find */

function _New(Object, callback){
	var NewDetail = new Detail(Object);

	NewDetail.save(function (err, savedDoc){
		return callback({Err:err, SavedDoc: savedDoc});
	});
} /* _New */

function _FormDetail(RawData){
	var NewDetail = {
			RangeStart 	: RawData.RangeStart,
			RangeEnd 	: RawData.RangeEnd,
			BatchWeight : RawData.batchWeight}

	return NewDetail;
} /* _FormDetail */

function _NewDetail(Object, callback){
	_New(Object, function(Resp){
		return callback(Resp);
	});
} /* _NewDetail */

function _Update(Object, callback){
	Object.save(function(err, savedDoc){
		return callback({Err: err, SavedDoc: savedDoc});
	});
} /* _Update */

module.exports = class WaveDetail {
    constructor(){}

	FindOne(KeyValuePair, callback){
		_FindOne(KeyValuePair, function(Resp){
			return callback(Resp);
		});
	} /* FindOne */

	Find(KeyValuePair, callback){
		_Find(KeyValuePair, function(Resp){
			return callback(Resp);
		});
	} /* Find */

	Update(Object, callback){
		_Update(Object, function(Resp){
			return callback(Resp);
		});
	} /* Update */

	New(Object, callback){
		_NewDetail(Object, function(Resp){
			return callback(Resp);
		});
	} /* New */

	// GetLowestStatus(KeyValuePair, callback){
	// 	_Find(KeyValuePair, function(Resp){
	// 		if(Resp.Err){
	// 			return callback(Resp);
	// 		} else {
	// 			if(Resp.DetailArr.length > 0){
	// 				var x = 0;
	// 				var Data = Resp.DetailArr;
	// 				var LowestState = dec.plStatus.PICKED.value;
	// 				while(x < Data.length){
	// 					if(Data[x].Status < LowestState){
	// 						LowestState = Data[x].Status;
	// 					}
	// 					x++;
	// 				}

	// 				return callback({LowestStatus: LowestState});
	// 			} else {
	// 				return callback({Err: 'No Data found for given inputs [' + JSON.stringify(KeyValuePair) + ']'});
	// 			}
	// 		}
	// 	});
	// } /* GetLowestStatus */

	Delete(KeyValuePair, callback){
		Detail.remove(KeyValuePair, function(err){
			let Err = null
			if(!err){
				Err = err;
			}

			return callback({Err: Err})
		});
	}

} /* class WaveDetail */