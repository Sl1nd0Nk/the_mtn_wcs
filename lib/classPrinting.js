var redis       = require('redis');
var moment      = require('moment');
var formidable  = require("formidable");

var Pubclient   = redis.createClient();

/*======================================= End Of Private ===========================================================*/

module.exports = class Printing {
    constructor(){}

	PrintDocuments(Data, NoLabel, orderIndex, callback){
		var Station = Data.Station;
		var PL = Station.pickList;

		var LabelPrn = Station.labelPrinterQName;
		if(Station.ProcUsingDesktopAutoBag == true){
			LabelPrn = Station.autobagPrinterQName;
		}

		if(PL){
			var OrdItems = Data.OrdItems;
			var Cartons  = Data.Cartons;

			var CartonNo = PL.orders[orderIndex].toteNumber;
			var NoOfCartons = PL.orders[orderIndex].numberOfTotes;

			var First = false;
			var Last = false;

			if(CartonNo == 1 && CartonNo == NoOfCartons){
				First = true;
				Last = true;
			} else if(CartonNo == 1 && CartonNo != NoOfCartons){
				First = true;
				Last = false;
			} else if(CartonNo > 1 && CartonNo == NoOfCartons){
				First = false;
				Last = true;
			} else if(CartonNo > 1 && CartonNo != NoOfCartons){
				First = false;
				Last = false;
			}

			var PrintData = {
				orderId: PL.orders[orderIndex].orderID,
				printerName: Station.desktopPrinterQName,
				labelprinterName: LabelPrn,
				items: OrdItems,
				ErpItems: PL.orders[orderIndex].items,
				cartonId: PL.orders[orderIndex].cartonID,
				courierId: PL.orders[orderIndex].courierID,
				station: Station.stationNumber,
				CartonNo: CartonNo,
				NoOfCartons: NoOfCartons,
				CartionWeight: PL.orders[orderIndex].picklistWeight,
				FirstCarton: First,
				LastCarton: Last,
				orderType: PL.orders[orderIndex].orderType,
				Cartons: Cartons,
				NoLabel: NoLabel
			}

			Pubclient.publish('PACKING_' + Station.stationNumber, JSON.stringify(PrintData), function(err){
				 if(err){
					 return callback({Msg:'Error printing documents/label for order ' + PL.orders[orderIndex].orderID});
				 } else {
					return callback({Station: Station});
				 }
			});
		}
	} /* PrintDocuments */

	PrintDocumentsPdf(PL, Data, NoLabel, STN, callback){
		if(PL){
			var OrdItems = Data.OrdItems;
			var Cartons  = Data.Cartons;

			var CartonNo = PL.orders[Data.OrderIndex].toteNumber;
			var NoOfCartons = PL.orders[Data.OrderIndex].numberOfTotes;

			var First = false;
			var Last = false;

			if(CartonNo == 1 && CartonNo == NoOfCartons){
				First = true;
				Last = true;
			} else if(CartonNo == 1 && CartonNo != NoOfCartons){
				First = true;
				Last = false;
			} else if(CartonNo > 1 && CartonNo == NoOfCartons){
				First = false;
				Last = true;
			} else if(CartonNo > 1 && CartonNo != NoOfCartons){
				First = false;
				Last = false;
			}

			var PrintData = {
				orderId: PL.orders[Data.OrderIndex].orderID,
				printerName: null,
				labelprinterName: null,
				items: OrdItems,
				ErpItems: PL.orders[Data.OrderIndex].items,
				cartonId: PL.orders[Data.OrderIndex].cartonID,
				courierId: PL.orders[Data.OrderIndex].courierID,
				station: STN,
				CartonNo: CartonNo,
				NoOfCartons: NoOfCartons,
				CartionWeight: PL.orders[Data.OrderIndex].picklistWeight,
				FirstCarton: First,
				LastCarton: Last,
				orderType: PL.orders[Data.OrderIndex].orderType,
				Cartons: Cartons,
				NoLabel: NoLabel,
				PdfPrint: PL.orders[Data.OrderIndex].orderID + '.pdf'
			}

			Pubclient.publish('PACKING_' + STN, JSON.stringify(PrintData), function(err){
				 if(err){
					 return callback({Msg:'Error printing documents for order ' + PL.orders[Data.OrderIndex].orderID});
				 } else {
					return callback({File: PL.orders[Data.OrderIndex].orderID + '.pdf'});
				 }
			});
		}
	} /* PrintDocumentsPdf */

	PrintToAutobagger(printer, packStation, savedDoc, callback){
		var PrintData = {
			orderId: savedDoc.orders[0].orderID,
			printerName: null,
			labelprinterName: printer,
			items: null,
			ErpItems: null,
			cartonId: savedDoc.orders[0].cartonID,
			courierId: savedDoc.orders[0].courierID,
			station: packStation,
			CartonNo: savedDoc.orders[0].toteNumber,
			NoOfCartons: savedDoc.orders[0].numberOfTotes,
			CartionWeight: savedDoc.orders[0].picklistWeight,
			FirstCarton: true,
			LastCarton: true,
			orderType: savedDoc.orders[0].orderType,
			Cartons: null,
			NoDoc: true
		}

		Pubclient.publish('PACKING_' + packStation, JSON.stringify(PrintData), function(err){
			if(err){
				return callback({Msg: "Error printing label for order " + savedDoc.orders[0].orderID, PickList:savedDoc});
			} else {
				return callback({PickList: savedDoc});
			}
		});
	} /* PrintToAutobagger */

} /* Printing */