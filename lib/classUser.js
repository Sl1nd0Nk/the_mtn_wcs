var user        = require('userModel');
var moment      = require('moment');
var formidable  = require("formidable");

function _CreateUser(Object, callback){
	user.create(Object, function(err, SavedDoc){
		if(err){
			return callback({Err: err});
		} else {
			if(SavedDoc){
				return callback({SavedDoc: SavedDoc});
			} else {
				return callback({SavedDoc: null});
			}
		}
	});
} /* _CreateUser */

module.exports = class User {
    constructor(){}

    New(Object, callback){
		_CreateUser(Object, function(Resp){
			return callback(Resp);
		});
	} /* New */

	Find(KeyValuePair, callback){
		user.find(KeyValuePair, function(err, UserArr){
			if(err){
				return callback({Err: err});
			} else {
				if(UserArr.length > 0){
					return callback({UserArr: UserArr});
				} else {
					return callback({UserArr: null});
				}
			}
		});
	} /* Find */

	FindOne(KeyValuePair, callback){
		user.findOne(KeyValuePair, function(err, User){
			if(err){
				return callback({Err: err});
			} else {
				if(User){
					return callback({User: User});
				} else {
					return callback({User: null});
				}
			}
		});
	} /* FindOne */

	FormNewUserObj(req, callback){
		var form = new formidable.IncomingForm();

		form.parse(req, function (err, fields, files){
			if((fields.uid != null) &&
			   (fields.firstName != null) &&
			   (fields.LastName != null) &&
			   (fields.psw != null)){
				   var UserData = {
					     UserID : fields.uid,
					     FirstName : fields.firstName,
                         LastName : fields.LastName,
                         Password: fields.psw
				   }

				return callback({UserData: UserData});
			} else {
				return callback({UserData: null});
			}
		});
	} /* FormNewUserObj */

	GetLoginDetails(req, callback){
		var form = new formidable.IncomingForm();

		form.parse(req, function (err, fields, files){
			if((fields.uid != null) &&
			   (fields.uid != "") &&
			   (fields.psw != null) &&
			   (fields.psw != "")){
				return callback({Username: fields.uid, Password: fields.psw});
			} else {
				return callback({Username: null});
			}
		});
	} /* GetLoginDetails */
}