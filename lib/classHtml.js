var moment    = require('moment');
var mustache  = require('mustache');
var fs        = require('fs');

var UrlTemplate  = require('ui').BaseSystem;
var UrlArr = UrlTemplate.split('{{}}');
var ext;
var page;

var Template = {
	default: {
		name: 'default',
		page: null
	},
	frame: [{
		name: null,
		page: null
	}]
}

function _CreateDefaultView(user, Ip){

	var View = {
		user: user,
		url: UrlArr[0]+ Ip + UrlArr[1],
		ToteProcess: "",
		PackProcess: "none",
		changeCartonSizeScreen: "none",
		Station: "none",
		NoStation: "",
		btnReset: "none",
		btnReprint: "none",
		btnChangeCarton: "none",
		btnViewSerials: "none",
		orderPriority :"none"
	};

	return View;
}

function _CreateDefaultPwView(user, Ip){
	var View = {
		user: user,
		url: UrlArr[0]+ Ip + UrlArr[1],
		PackProcess: "",
		PwInstruction: "none",
		btnPwReset: "none",
		ScanMode: "none",
		clcendpoint: "none",
		CheckConfimation : ""
	};

	return View;
}

function _CreateDefaultPdfView(user, Ip){
	var View = {
		user: user,
		url: UrlArr[0]+ Ip + UrlArr[1],
		dispdfview: "none",
		PdfDoc: "",
		showPdf: "<!--",
		showPdfEnd: "-->",
		disploading: "none",
		CheckDocLoadedStart: "<!--",
		CheckDocLoadedEnd: "-->",
		PrivUser: "none",
		NoPrivUser: "none",
		orderPriority :"none",
		batchWeight: "none"
	};

	return View;
}

function _DetermineNextStepToProcess(Order, view){
	var NewView = view;
	var x = 0;
	var MoreToScan = false;
	while(x < Order.items.length){
		if(Order.items[x].qtyPacked < Order.items[x].pickQty){
			MoreToScan = true;
			break;
		}
		x++;
	}

	if(MoreToScan){
		NewView.InputAction = "Scan Serial/Barcode";
		NewView.endpoint = "serial";
		NewView.btnViewSerials = "";
	} else {
		if(Order.packed && (Order.cartonID == null || Order.cartonID == '')){
			NewView.InputAction = "Scan Carton ID";
			NewView.endpoint = "carton";
			NewView.btnViewSerials = "";
		} else {
			NewView.btnReset = "none";
			NewView.btnChangeCarton = "none";
			NewView.toteDetailHeading = "none";
			NewView.cartonToUseHeading = "none";
			NewView.cartonSizeHeading = "";
			NewView.scannedCatonIdHeading = "";

			NewView.scannedCartonIdDisplay = Order.cartonID;
			NewView.cartonSizeUsed = Order.cartonSize;

			if(!Order.packComplete){
				NewView.btnReprint = "";

				if(Order.numberOfTotes == Order.toteNumber){
					if(!Order.orderVerified){
						NewView.InputAction = "Scan Document Barcode";
						NewView.endpoint = "document";
					} else {
						if(Order.useTransporter &&
						  (Order.transporterID == null || Order.transporterID == '')){
							NewView.InputAction = "Scan Transporter";
							NewView.endpoint = "transporter";
						}
					}
				} else {
					if(Order.useTransporter &&
					  (Order.transporterID == null || Order.transporterID == '')){
						NewView.InputAction = "Scan Transporter";
						NewView.endpoint = "transporter";
					}
				}
			}
		}
	}

	return NewView;
} /* _DetermineNextStepToProcess */

function _diff_minutes(dt2, dt1){
  var diff =(dt2.getTime() - dt1.getTime()) / 1000;
  diff /= 60;
  return Math.abs(Math.round(diff));
} /* _diff_minutes */

function _WEB_CallPageRedirect(res, Url){
	res.statusCode = 302;
	res.setHeader('Location',Url);
	res.end();
} /* _WEB_CallPageRedirect */

function _GetHeaderIpAddr(req){
	//console.log(JSON.stringify(req.headers));
	//var hostUrl = req.headers.host;
	var hostUrl = req.headers.referer;
        if(hostUrl){
		var hostIpArr1 = hostUrl.split('://');
		var hostIpArr = hostIpArr1[1].split(':');
		return hostIpArr[0];
	}

	return '';
} /* _GetHeaderIpAddr */

function _ShowScanTote(ViewData, user, Ip){
	var Station = ViewData.Station;
	var view = _CreateDefaultView(user, Ip);
	view.Station = "";
	view.NoStation = "none";
	view.batchWeight = "none"
	view.signedInStation = "PACKING STATION " + Station.stationNumber;

	if(ViewData.Msg){
		view.alert = true;
		view.alertMsg = ViewData.Msg;
	}

	return view;
} /* _ShowScanTote */

module.exports = class Html {

    constructor(){
		var data = {};
		data.page = fs.readFileSync('./view/PutWallPacking.html', "utf8");
		data.name = 'putWallPacking';

		Template.default.page = fs.readFileSync('./view/index.html', "utf8");
		Template.frame.push(data);

		data = {};
		data.page = fs.readFileSync('./view/Packing.html', "utf8");
		data.name = 'normalPacking';
		Template.frame.push(data);

		data = {};
		data.page = fs.readFileSync('./view/batchWeights.html', "utf8");
		data.name = 'batchWeights';
		Template.frame.push(data);

		data = {};
		data.page = fs.readFileSync('./view/orderPriority.html', "utf8");
		data.name = 'orderPriority';
		Template.frame.push(data);

		data = {};
		data.page = fs.readFileSync('./view/PrintToPDF.html', "utf8");
		data.name = 'PrintToPDF';
		Template.frame.push(data);

		mustache.parse(Template.default.page);
	} /* constructor */

	GetPage(FrameName){
		if(!FrameName){
			return Template.default.page;
		} else {
			var x = 0;
			while(x < Template.frame.length){
				if(Template.frame[x].name == FrameName){
					return Template.frame[x].page;
				}

				x++;
			}

			return "";
		}
	} /* GetPage */

    ShowNoStation(ErrorData, user, Ip){
		//console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' ' + ErrorData.Err );

		return _CreateDefaultView(user, Ip);
	} /* ShowNoStation */

	ShowScanTote(ViewData, user, Ip){
		return _ShowScanTote(ViewData, user, Ip);
	} /* ShowScanTote */

	ProcessTote(ViewData, user, Ip){
		var Station = ViewData.Station;
		var PL = Station.pickList;

		if(PL){
			var view = _CreateDefaultView(user, Ip);
			view.ToteProcess = "none";
			view.PackProcess = "";
			view.orderScanContent = "";
			view.toteDetailHeading = "";
			view.cartonToUseHeading = "";
			view.cartonSizeHeading = "none";
			view.scannedCatonIdHeading = "none";
			view.btnReset = "";
			view.btnChangeCarton = "";
			view.batchWeight = "none"

			view.toteTypeDetailName = Station.ToteType;

			view.scannedTote = PL.toteID;
			if(PL.orders){
				if(PL.orders.length > 0){
					if(Station.OrderIndex < PL.orders.length){
						view.useCartonSize = PL.orders[Station.OrderIndex].cartonSize;
						view.currentOrderId = PL.orders[Station.OrderIndex].orderID;

						var SerItems = [];
						var x = 0;
						while(x < PL.orders[Station.OrderIndex].items.length){
							var y = 0;
							while(y < PL.orders[Station.OrderIndex].items[x].serials.length){
								var dd = {
									prodCode: PL.orders[Station.OrderIndex].items[x].itemCode,
									serial: PL.orders[Station.OrderIndex].items[x].serials[y]
								}

								SerItems.push(dd);
								y++;
							}
							x++;
						}

						view.items = PL.orders[Station.OrderIndex].items;
						view.serItems = SerItems;
						view.optitems = Station.CartonSizes;

						view = _DetermineNextStepToProcess(PL.orders[Station.OrderIndex], view);
					}
				}
			}

			view.currentOrderInToteProcess = Station.CurrentOrder;
			view.totalOrdersInToteToProcess = Station.NumOrders;

			view.signedInStation = "PACKING STATION " + Station.stationNumber;

			if(ViewData.Msg){
				view.prompt = true;
				view.promptMsg = ViewData.Msg;
			}

			if(Station.UserError){
				if(ViewData.SuperMsg){
					view.SuperMsg = ViewData.SuperMsg;
				}
				view.UserMsg = Station.UserError;
				view.ShowIncomingModal = true;
				view.supendpoint = "superorverride";
			}

			return view;
		} else {
			return _ShowScanTote(ViewData, user, Ip);
		}
	} /* ProcessTote */

	ProcessPwTote(ViewData, user, Ip){
		var Station = ViewData.Station;
		var PL = Station.pickList;
		if(PL){
			var Next = ViewData.Next;
			var ConfirmData = ViewData.ConfirmData;
			var view = _CreateDefaultPwView(user, Ip);
			view.PackProcess = "";

			view.signedInStation = "PUTWALL PACKING STATION " + Station.stationNumber;

			if(ViewData.Msg){
				view.prompt = true;
				view.promptMsg = ViewData.Msg;

				view.ScanMode = "";

				if(Next == 'DOC'){
					view.InputAction = "Scan Document Barcode";
					view.endpoint = "pwdocument";
				} else if(Next == 'BARCODE/SERIAL'){
					view.InputAction = "Scan Serial/Barcode";
					view.endpoint = "pwserial";
					view.btnPwReset = "";
				}
			} else {
				if(Station.UserError){
					if(ViewData.SuperMsg){
						view.SuperMsg = ViewData.SuperMsg;
					}
					view.ShowIncomingModal = true;
					view.UserMsg = Station.UserError;
					view.ScanMode = "";
					view.supendpoint = "superorverride";

					if(Next == 'DOC'){
						view.InputAction = "Scan Document Barcode";
						view.endpoint = "pwdocument";
					} else if(Next == 'BARCODE/SERIAL'){
						view.InputAction = "Scan Serial/Barcode";
						view.endpoint = "pwserial";
						view.btnPwReset = "";
					}
				} else {
					if(ConfirmData){
						var Instr = null;
						if(ConfirmData.Serial){
							if(ConfirmData.Serial == ''){
								Instr = 'Put item into location ' + ConfirmData.Location + ' with the light on. Push the button to confirm';
							} else {
								Instr = 'Put item ' + ConfirmData.Serial + ' into location ' + ConfirmData.Location + ' with the light on. Push the button to confirm';
							}
						} else if(ConfirmData.Document){
							Instr = 'Put Scanned Document ' + ConfirmData.orderID + ' into location ' + ConfirmData.Location + ' with the light on. Push the button to confirm';
						}

						view.instruction = Instr;
						view.PwInstruction = "";
						view.Location = ConfirmData.Location;
						view.CheckConfimation = "CheckLocConfirmed();";
						view.clcendpoint = "checklocconfirm";

					} else {
						view.ScanMode = "";

						if(Next == 'DOC'){
							view.InputAction = "Scan Document Barcode";
							view.endpoint = "pwdocument";
						} else if(Next == 'BARCODE/SERIAL'){
							view.InputAction = "Scan Serial/Barcode";
							view.endpoint = "pwserial";
							view.btnPwReset = "";
						}
					}
				}
			}

			return view;
		} else {
			return _ShowScanTote(ViewData, user, Ip);
		}
	} /* ProcessPwTote */

	ProcessPdfData(ViewData, user, Ip){
		var view = _CreateDefaultPdfView(user, Ip);

		if(ViewData){
			if(ViewData.NoAccess){
				view.NoPrivUser = "";
			} else {
				view.PrivUser = "";

				if(ViewData.Msg){
					view.alert = true;
					view.batchWeight = "none"
					view.alertMsg = ViewData.Msg;
				}

				if(ViewData.File){
					view.disploading = "";
					view.CheckDocLoadedStart = "";
					view.CheckDocLoadedEnd = "";
					view.batchWeight = "none"
					view.pdfFile = ViewData.File;
				}

				if(ViewData.Loaded){
					view.dispdfview = "";
					view.showPdf = "";
					view.showPdfEnd = "";
					view.batchWeight = "none"
					view.PdfDoc = ViewData.Loaded;
				}

				if(ViewData.orderPriority1){
					var priorityArr = [];
					var priorityData = ViewData.orderPriority;
					let x = 0;
					let y = 0;
					var priorityObj;

					if(ViewData.success){
						view.updateSuccess = true;
					}

					if(priorityData != null){
						while(y < priorityData.length){
							let x = 0;
							while(x < priorityData[y].orders.length){
								priorityObj = {
									PicklistID 	: priorityData[y].orders[x].pickListID,
									OrderID 	: priorityData[y].orders[x].orderID,
									Priority 	: priorityData[y].priorityLevel,
									Status 		: priorityData[y].Status,
									pl 			: priorityData[y].orders[x].pickListID
								};

								priorityArr.push(priorityObj);
								x++;
							}
							y++;
						}

						view.orderPriority  	= "block";
						view.ToteProcess 		= "none";
						view.PackProcess 		= "none";
						view.orderScanContent 	= "none";
						view.toteDetailHeading 	= "none";
						view.cartonToUseHeading = "none";
						view.cartonSizeHeading 	= "none";
						view.scannedCatonIdHeading = "none";
						view.btnReset = "";
						view.btnChangeCarton = "";
						view.priorityDateValue = ViewData.selectedDate;
						view.orderPriorityItems = priorityArr;
						var priorityData1 = ViewData.orderPriority;

						var req = ViewData.req;
						console.log(req.query.selectedPickList);
						if(ViewData.PicklistID){
							let a = 0;

							while(a < priorityData1.length){
								let b = 0;
								while(b < priorityData1[a].orders.length){
									if(priorityData1[a].orders[b].pickListID == req.query.selectedPickList){
										view.openPriorityModal = true;
										view.txtOrderID = priorityData1[a].orders[b].orderID;
										view.txtPickListID = ViewData.PicklistID
										view.txtPriorityLevel = priorityData1[a].priorityLevel;
										view.txtPriorityStatus = priorityData1[a].Status;
										delete ViewData.PicklistID;
									}
									b++;
								}
								a++;
							}
						}
					} else {
						console.log("Not displaying Anything");
						view.orderPriority      = "block";
						view.ToteProcess        = "none";
						view.PackProcess        = "none";
						view.orderScanContent   = "block";
						view.toteDetailHeading  = "none";
						view.cartonToUseHeading = "none";
						view.cartonSizeHeading  = "none";
						view.scannedCatonIdHeading = "none";
						view.btnReset = "";
						view.btnChangeCarton = "";
					}
				}

				if(ViewData.add1){
					view.batchWeight = "block";
					view.ToteProcess = "none";
					view.PackProcess = "none";
					view.orderScanContent = "none";
					view.toteDetailHeading = "none";
					view.cartonToUseHeading = "none";
					view.cartonSizeHeading = "none";
					view.scannedCatonIdHeading = "none";
					view.btnReset = "";
					view.btnChangeCarton = "";
					view.batchWeight = ViewData.addBatch;
				}

				if(ViewData.addNewBatch){
					view.ShowAddBatchModal = true;
					view.batchStep = "Add New Batch";
				}

				if(ViewData.deleteBatch){
					view.ShowDeleteBatchModal = true;
					view.recordID = ViewData.deleteBatch;
				}

				if(ViewData.editBatch){
					view.ShowEditBatchModal = true;
					view.recordID = ViewData.editBatch._id;
					view.RangeStart = ViewData.editBatch.RangeStart
					view.RangeEnd = ViewData.editBatch.RangeEnd
					view.batchWeight = ViewData.editBatch.batchWeight
				}
			}
		}

		return view;
	} /* ProcessPdfData */

	requireLogin (req, res, next){
	  if(!req.session){
		  var Ip = _GetHeaderIpAddr(req);
		  _WEB_CallPageRedirect(res, UrlArr[0]+ Ip + UrlArr[1] + 'login');
	  } else {
		  if (!req.session.user){
			var Ip = _GetHeaderIpAddr(req);
			_WEB_CallPageRedirect(res, UrlArr[0]+ Ip + UrlArr[1] + 'login');
		  } else {
			if(!req.session.expiration){
				var now = new Date();
				req.session.expiration = now;
				next();
			} else {
				var now = new Date();
				var ex = new Date(req.session.expiration);
				var diff = _diff_minutes(ex, now);
				if(diff <= 10){
					req.session.expiration = now;
					next();
				} else {
					req.session.expiration = null;
					var Ip = _GetHeaderIpAddr(req);
					_WEB_CallPageRedirect(res, UrlArr[0]+ Ip + UrlArr[1] + 'login');
				}
			}
		  }
	  }
	} /* requireLogin */

	WEB_CallPageRedirect(res, Url){
		_WEB_CallPageRedirect(res, Url);
	} /* WEB_CallPageRedirect */

	GetHeaderIpAddr(req){
		return _GetHeaderIpAddr(req);
	} /* GetHeaderIpAddr */

	GetIpAddress(req){
                let ClientIP;

		if(req.headers['x-forwarded-for']){
			ClientIP = req.headers['x-forwarded-for'];
		} else if(req.connection.remoteAddress){
			ClientIP = req.connection.remoteAddress;
		} else if(req.socket.remoteAddress){
			ClientIP = req.socket.remoteAddress;
		} else if(req.connection.socket.remoteAddress){
			ClientIP = req.connection.socket.remoteAddress;
		}

		/*var ClientIP = req.headers['x-forwarded-for'] ||
					   req.connection.remoteAddress ||
					   req.socket.remoteAddress ||
					   req.connection.socket.remoteAddress;*/

		if (ClientIP.substr(0, 7) == "::ffff:"){
			ClientIP = ClientIP.substr(7)
		}

		return ClientIP;
	} /* GetIpAddress */
}
