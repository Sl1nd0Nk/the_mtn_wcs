var pickList    = require('pickListModel');
var moment      = require('moment');
var formidable  = require("formidable");
var async       = require('async');

var qGetPLForTrolley  = async.queue(GetPLForTrolley, 1);

qGetPLForTrolley.drain = function(){
}

function GetPLForTrolley(task, callback){
	let KeyValuePair = task.KeyValuePair;
	let ScannedToteID = task.ScannedToteID;
	let UserID = task.UserID;
	let TrolleyID = task.TrolleyID;

	pickList.findOne(KeyValuePair, function(err, PL){
		if(err){
			return callback(err, null);
		} else {
			if(PL){
				PL.toteID = ScannedToteID;
				PL.toteInUse = true;
				PL.toteInUseBy = UserID;
				PL.containerLocation = TrolleyID;

				_Update(PL, function(Resp){
					return callback(Resp.Err, Resp.SavedDoc);
				});
			} else {
				return callback('Not Found', null);
			}
		}
	});
} /* GetPLForTrolley */

var qDetCartNo  = async.queue(DetCartNo, 1);

qDetCartNo.drain = function(){
}

function DetCartNo(task, callback){
	let savedDoc = task.PL;
	let OrderIndex = task.index;

	_Find({'orders.orderID': savedDoc.orders[OrderIndex].orderID}, function(Resp){
		if(Resp.Err){
			return callback(null,{Msg: 'Failed to determine carton number, please call administrator'});
		} else {
			var PickLists = Resp.pickListArr;
			if(PickLists){
				if(PickLists.length > 0){
					var NoOfCartons = 0;
					var NextCartonNum = null;
					var t = 0;
					var Cartons = [];
					var OrdItems = [];

					while(t < PickLists.length){
						var n = 0;
						while(n < PickLists[t].orders.length){
							if(PickLists[t].orders[n].orderID == savedDoc.orders[OrderIndex].orderID){
								NoOfCartons++;
							}
							n++;
						}
						t++;
					}

					t = 0;
					while(t < PickLists.length){
						var n = 0;
						while(n < PickLists[t].orders.length){
							if(PickLists[t].orders[n].orderID == savedDoc.orders[OrderIndex].orderID){
								if(PickLists[t].orders[n].toteNumber != null && PickLists[t].orders[n].toteNumber <= NoOfCartons){
									if(NextCartonNum == null){
										NextCartonNum = PickLists[t].orders[n].toteNumber;
									} else {
										if(PickLists[t].orders[n].toteNumber > NextCartonNum){
											NextCartonNum = PickLists[t].orders[n].toteNumber;
										}
									}
								}

								var itemIndx = 0;
								if(PickLists[t].orders[n].pickListID == savedDoc.orders[OrderIndex].pickListID){
									while(itemIndx < savedDoc.orders[OrderIndex].items.length){
										OrdItems.push(savedDoc.orders[OrderIndex].items[itemIndx]);
										Cartons.push(savedDoc.orders[OrderIndex].cartonID);
										itemIndx++;
									}
								} else {
									while(itemIndx < PickLists[t].orders[n].items.length){
										OrdItems.push(PickLists[t].orders[n].items[itemIndx]);
										Cartons.push(PickLists[t].orders[n].cartonID);
										itemIndx++;
									}
								}
							}
							n++;
						}
						t++;
					}

					if(NextCartonNum == null){
						NextCartonNum = 1;
					} else {
						if(NextCartonNum < NoOfCartons){
							NextCartonNum += 1;
						}
					}

					return callback(null,{CartonNo: NextCartonNum, NoOfCartons: NoOfCartons, Cartons: Cartons, OrdItems: OrdItems});

				} else {
					return callback(null,{Msg: 'Failed to determine carton number, please call administrator'});
				}
			} else {
				return callback(null,{Msg: 'Failed to determine carton number, please call administrator'});
			}
		}
	});
} /* DetCartNo */

/*Get the first 200 records*/
function _getTop200(KeyValuePair, callback){
	pickList.find(KeyValuePair, function(err, pickListArr){
		if(err){
			return callback({Err: err});
		} else {
			if(pickListArr.length > 0){

				return callback({pickListArr: pickListArr});
			} else {
				return callback({Err: 'No records Found', pickListArr: null});
			}
		}
	}).limit(200);
} /* _getTop200 */

function _New(Object, callback){
} /* _CreateNewStation */

function _Find(KeyValuePair, callback){
	pickList.find(KeyValuePair, function(err, pickListArr){
		if(err){
			return callback({Err: err});
		} else {
			if(pickListArr.length > 0){
				return callback({pickListArr: pickListArr});
			} else {
				return callback({pickListArr: null});
			}
		}
	});
} /* _Find */

function _FindStream(KeyValuePair){
	let cursor = pickList.find(KeyValuePair).cursor();

	return cursor;
} /* _Find */

function _FindOne(KeyValuePair, callback){
	pickList.findOne(KeyValuePair, function(err, PL){
		if(err){
			return callback({Err: err});
		} else {
			if(PL){
				return callback({PL: PL});
			} else {
				return callback({Err: 'Not Found', PL: null});
			}
		}
	});
} /* _FindOne */

function _Count(KeyValuePair, callback){
	pickList.count(KeyValuePair, function(err, TotesPerOrder){
		if(err){
			return callback({Err: err});
		} else {
			if(TotesPerOrder){
				return callback({TotesPerOrder: TotesPerOrder});
			} else {
				return callback({Err: 'Err While Count', TotesPerOrder: null});
			}
		}
	});
} /* _Count */

function _Update(Obj, callback){
	Obj.save(function(err, savedDoc){
		if(err){
			return callback({Err: err});
		} else {
			if(savedDoc){
				return callback({SavedDoc: savedDoc});
			} else {
				return callback({Err: 'Update Failed', SavedDoc: null});
			}
		}
	});
} /* _Update */

function _CalcPLVol(items){
	var PickListVolume = 0;
	var weight = 0;
	var volume = 0;

	if(items.length > 0){
		var x = 0;
		while(x < items.length){
			if(items[x].eachVolume)
			  volume += ((items[x].qty * items[x].eachVolume) / 1)

			x++;
		}

		PickListVolume = volume;
	}

	return PickListVolume;
} /* _CalcPLVol */

function ValidateEachOrder(task, volumeCapacity, callback){
	var PickListVolume = _CalcPLVol(task.items);

	if(PickListVolume <= volumeCapacity){
		_Count({'orders.orderID':task.orderID}, function(Resp){
			var PLsPerOrder = Resp.TotesPerOrder;
			if(PLsPerOrder == 1){
				return callback(null, true);
			} else {
				return callback(null, false);
			}
	  });
	} else {
		return callback(null, false);
	}
} /* ValidateEachOrder */

function _CheckIfOrdersInToteAreValidforPutWall(doc, index, volumeCapacity, callback){
	if(index < doc.orders.length){
var PickListVolume = _CalcPLVol(doc.orders[index].items);
		if(PickListVolume <= volumeCapacity){
			_Count({'orders.orderID':doc.orders[index].orderID}, function(Resp){
				var PLsPerOrder = Resp.TotesPerOrder;
				if(PLsPerOrder == 1){
					index++;
					_CheckIfOrdersInToteAreValidforPutWall(doc, index, volumeCapacity, callback);
				} else {
					return callback(false);
				}
		  });
		} else {
			return callback(false);
		}
	} else {
		return callback(true);
	}
} /* _CheckIfOrdersInToteAreValidforPutWall */

module.exports = class PickingList {
    constructor(){}

    New(Object, callback){
		_New(Object, function(Resp){
			return callback(Resp);
		});
	} /* New */

	getTop200(KeyValuePair, callback){
		_getTop200(KeyValuePair, function(Resp){
			return callback(Resp);
		});
	} /* getTop200 */

	Find(KeyValuePair, callback){
		_Find(KeyValuePair, function(Resp){
			return callback(Resp);
		});
	} /* Find */

	FindStream(KeyValuePair){
		return _FindStream(KeyValuePair);
	} /* Find */

	FindOne(KeyValuePair, callback){
		_FindOne(KeyValuePair, function(Resp){
			return callback(Resp);
		});
	} /* FindOne */

	Count(KeyValuePair, callback){
		_Count(KeyValuePair, function(Resp){
			return callback(Resp);
		});
	} /* Count */

	Update(Obj, callback){
		_Update(Obj, function(Resp){
			return callback(Resp);
		});
	} /* Update */

	Delete(KeyValuePair, callback){
		pickList.remove(KeyValuePair, function(){
			return callback();
		});
	} /* Delete */

	FormNewPickList(Obj){
		return new pickList(Obj);
	} /* FormNewPickList */

	GetToteType(NumOrders, OrderID, callback){
		_Count({'orders.orderID':OrderID}, function(Resp){
			var TotesPerOrder = Resp.TotesPerOrder;
			if(!TotesPerOrder){
				TotesPerOrder = 1;
			}

			var toteTypeDetailName = null;
			if(NumOrders > 1){
				toteTypeDetailName = 'MULTI ORDER TOTE';
			} else {
				if(TotesPerOrder > 1){
					toteTypeDetailName = 'MULTI TOTE ORDER';
				} else {
					toteTypeDetailName = 'SINGLE ORDER TOTE';
				}
			}

			return callback(toteTypeDetailName);
		});
	} /* GetToteType */

	MarkPLInUse(PL, StnNumber, callback){
		PL.toteInUse = true;
		PL.toteInUseBy = StnNumber;
		PL.containerLocation = null;

		var x = 0;
		while(x < PL.orders.length){
			var y = 0;
			while(y < PL.orders[x].items.length){
				if(PL.orders[x].items[y].qtyPacked == null){
					PL.orders[x].items[y].qtyPacked = 0;
				}
				y++;
			}
			x++;
		}

		_Update(PL, function(PResp){
			if(PResp.Err){
				//log.WriteToFile(null, 'Failed to add tote ' + PL.toteID + ' to station ' + StnNumber);
			} else {
				//log.WriteToFile(null, 'Successfully added tote ' + PL.toteID + ' to station ' + StnNumber);
			}

			return callback();
		});
	} /* MarkPLInUse */

	CheckIfCartonIsUnique(Carton, callback){
		_FindOne({'orders.cartonID':Carton}, function(Resp){
			if(Resp.Err){
				if(Resp.Err == 'Not Found'){
					return callback(Resp);
				} else {
					return callback({Msg: 'Failed to check if carton is unique on the system, call administrator'});
				}
			} else {
				return callback({Msg: 'Scanned Carton ' + Carton + ' has been used before'});
			}
		});
	} /* CheckIfCartonIsUnique */

	DetermineCartonNo(savedDoc, OrderIndex, callback){
		let Data = {
			PL: savedDoc,
			index: OrderIndex
		}

		/*qDetCartNo.push(Data, function (err, RespData) {
			return callback(RespData);
		});*/

		DetCartNo(Data, function(err, RespData){
			return callback(RespData);
		});
	} /* DetermineCartonNo */

	CheckIfOrderValidForDeskAutobag(PL, index, volumeCapacity, callback){
		var PickListVolume = _CalcPLVol(PL.orders[index].items);
		if(PickListVolume <= volumeCapacity){
			_Count({'orders.orderID':PL.orders[index].orderID}, function(Resp){
				var PLsPerOrder = Resp.TotesPerOrder;
				if(PLsPerOrder == 1){
					return callback(true);
				} else {
					return callback(false);
				}
		  });
		} else {
			return callback(false);
		}
	} /* CheckIfOrderValidForDeskAutobag */

	CheckIfOrdersInToteAreValidforPutWall(doc, index, volumeCapacity, callback){
		_CheckIfOrdersInToteAreValidforPutWall(doc, index, volumeCapacity,function(Result){
			return callback(Result);
		});
	} /* CheckIfOrdersInToteAreValidforPutWall */

	GetPickListForPickTrolley(Data, callback){
		qGetPLForTrolley.push(Data, function (err, RespData) {
			let Resp = {};
			Resp.Err = err;
			Resp.PL = RespData;

			return callback(Resp);
		});
	} /* GetPickListForPickTrolley */

} /* PickingList */
