var PwLocs    = require('putWallModel');
var moment      = require('moment');
var formidable  = require("formidable");

var aioiLib     = require('classAIOI');
var aioi        = new aioiLib();

function _New(Object, callback){
} /* _CreateNewStation */

function _Find(KeyValuePair, callback){
	PwLocs.find(KeyValuePair, function(err, pwLocsArr){
		if(err){
			return callback({Err: err});
		} else {
			if(pwLocsArr.length > 0){
				return callback({pwLocsArr: pwLocsArr});
			} else {
				return callback({pwLocsArr: null});
			}
		}
	});
} /* _Find */

function _FindOne(KeyValuePair, callback){
	PwLocs.findOne(KeyValuePair, function(err, pwLoc){
		if(err){
			return callback({Err: err});
		} else {
			if(pwLoc){
				return callback({pwLoc: pwLoc});
			} else {
				return callback({Err: 'Not Found', pwLoc: null});
			}
		}
	});
} /* _FindOne */

function _Update(Obj, callback){
	Obj.save(function(err, savedDoc){
		if(err){
			return callback({Err: err});
		} else {
			if(savedDoc){
				return callback({SavedDoc: savedDoc});
			} else {
				return callback({Err: 'Update Failed', SavedDoc: null});
			}
		}
	});
} /* _Update */

function _FindPutWallLocationUsingOrderID(OrderID, StationNumber, callback){
	_FindOne({'packStation': StationNumber, 'orderID': OrderID}, function(Resp){
        if(!Resp.Err && Resp.pwLoc){
			return callback(Resp.pwLoc.putAddress);
		} else {
			return callback(null);
		}
	});
} /* _FindPutWallLocationUsingOrderID */

function _FindEmptyLocationUsingStation(StationNumber, callback){
	_Find({'packStation': StationNumber, 'orderID': null}, function(Resp){
		if(!Resp.Err && Resp.pwLocsArr){
			var PWLOC = Resp.pwLocsArr;
			var x = 0;
			while(x < PWLOC.length){
				if(PWLOC[x].putLightError == true){
				} else if(PWLOC[x].pullLightError == true){
				} else {
					return callback(PWLOC[x].putAddress);
				}
				x++;
			}

			if(x >= PWLOC.length){
				return callback(null);
			}
		} else {
			return callback(null);
		}
	});
} /* _FindEmptyLocationUsingStation */

function _UpdateLightError(Lane, OrderID, StationNumber, callback){
	_FindOne({'packStation': StationNumber, 'orderID': OrderID}, function(Resp){
		if(!Resp.Err && Resp.pwLoc){
			var PWLOC = Resp.pwLoc;
			PWLOC.putLightError = true;
			_Update(PWLOC, function(Resp){
				return callback();
			});
		} else {
			return callback();
		}
	});
} /* _UpdateLightError */

function _FindAndLightAppropriateLane(orderID, StationNumber, callback){
	_FindPutWallLocationUsingOrderID(orderID, StationNumber, function(Lane){
		if(Lane){
			aioi.LightupLoc(Lane, '_ON_GREEN', StationNumber, function(Response){
				if(Response.error){
					_UpdateLightError(Lane, orderID, StationNumber, function(){
						_FindAndLightAppropriateLane(orderID, StationNumber, callback);
					});
				} else {
					return callback({Lane:Lane});
				}
			});
		} else {
			_FindEmptyLocationUsingStation(StationNumber, function(Lane){
				if(Lane){
					aioi.LightupLoc(Lane, '_ON_GREEN', StationNumber, function(Response){
						if(Response.error){
							_UpdateLightError(Lane, orderID, StationNumber, function(){
								_FindAndLightAppropriateLane(orderID, StationNumber, callback);
							});
						} else {
							return callback({Lane:Lane});
						}
					});
				} else {
					return callback({Err:'No more locations available to use'});
				}
			});
		}
	});
} /* _FindAndLightAppropriateLane */

module.exports = class PullWallLocs {
    constructor(){}

    New(Object, callback){
		_New(Object, function(Resp){
			return callback(Resp);
		});
	} /* New */

	Find(KeyValuePair, callback){
		_Find(KeyValuePair, function(Resp){
			return callback(Resp);
		});
	} /* Find */

	FindOne(KeyValuePair, callback){
		_FindOne(KeyValuePair, function(Resp){
			return callback(Resp);
		});
	} /* FindOne */

	Update(Obj, callback){
		_Update(Obj, function(Resp){
			return callback(Resp);
		});
	} /* Update */

	LightupLoc(Lane, color, StationNumber, callback){
		aioi.LightupLoc(Lane, color, StationNumber, function(Response){
			return callback(Response);
		});
	} /* LightupLoc */

	CheckIfWaitingForConfirmation(StationNumber, callback){
		_FindOne({'packStation': StationNumber, 'active': true}, function(Resp){
			if(!Resp.Err && Resp.pwLoc){
				if(Resp.pwLoc.Data){
					var LocData = JSON.parse(Resp.pwLoc.Data);

					var ReturnData = {
						orderID: Resp.pwLoc.orderID,
						Location: Resp.pwLoc.putAddress
					}

					if(LocData.Serial){
						ReturnData.Serial = LocData.Serial;
					}

					if(LocData.Document){
						ReturnData.Document = LocData.Document;
					}

					return callback(ReturnData);

				} else {
					return callback(null);
				}
			} else {
				return callback(null);
			}
		});
	} /* CheckIfWaitingForConfirmation */

	FindAndLightAppropriateLane(orderID, StationNumber, callback){
		_FindAndLightAppropriateLane(orderID, StationNumber, function(Resp){
			return callback(Resp);
		});
	} /* FindAndLightAppropriateLane */

	FindPutWallLocationUsingOrderID(OrderID, StationNumber, callback){
		_FindPutWallLocationUsingOrderID(OrderID, StationNumber, function(Lane){
			return callback(Lane);
		});
	} /* FindPutWallLocationUsingOrderID */

	UpdateLocationToBeInUse(Location, Order, Courier, LocationData, callback){
		_FindOne({'putAddress': Location}, function(Resp){
			if(!Resp.Err && Resp.pwLoc){
				var Obj = Resp.pwLoc;

				Obj.orderID = Order;
				Obj.courier = Courier;
				Obj.active = true;
				Obj.Data = JSON.stringify(LocationData);

				_Update(Obj, function(Resp){
					return callback();
				});
			} else {
				return callback();
			}
		});
	} /* UpdateLocationToBeInUse */

	UpdateLocationToBeConfirmed(Obj, Complete, callback){
		var now = new Date();

		Obj.Data = null;
		Obj.active = false;

		if(Complete == true){
			Obj.Complete = Complete;
			Obj.date = now;
		}

		_Update(Obj, function(Resp){
			return callback();
		});
	} /* UpdateLocationToBeConfirmed */

	ClearPutWallLoc(Obj, callback){
		Obj.orderID = null;
		Obj.courier = null;
		Obj.Complete = false;
		Obj.date = null;
		Obj.active = false;
		Obj.Data = null;
		Obj.putLightError = false;
		Obj.pullLightError = false;
		Obj.putComplete =false;

		_Update(Obj, function(Resp){
			return callback();
		});
	} /* ClearPutWallLoc */

	CalculatePwCartonWeight(savedDoc, CurrentOrderToProcess, EmptyCartonWeight, callback){
		var x = 0;
		var Total = EmptyCartonWeight;
		while(x < savedDoc.orders[CurrentOrderToProcess].items.length){
			var lineWeight = (savedDoc.orders[CurrentOrderToProcess].items[x].eachWeight *
							  savedDoc.orders[CurrentOrderToProcess].items[x].qtyPacked);
			Total += lineWeight;
			x++;
		}

		return callback(Total);
	} /* CalculatePwCartonWeight */

        FindCompleteLoc(KeyValuePair, callback){
	PwLocs.find(KeyValuePair).sort({'date':1}).exec( function(err, pwLocsArr){
		if(err){
			return callback({Err: err});
		} else {
			if(pwLocsArr.length > 0){
				return callback({pwLocsArr: pwLocsArr});
			} else {
				return callback({pwLocsArr: null});
			}
		}
	});
        }
} /* PullWallLocs */
