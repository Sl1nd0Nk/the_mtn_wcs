var param       = require('parameterModel');
var moment      = require('moment');
var formidable  = require("formidable");

function _Find(KeyValuePair, callback){
	param.find(KeyValuePair, function(err, paramArr){
		if(err){
			return callback({Err: err});
		} else {
			if(paramArr.length > 0){
				return callback({paramArr: paramArr});
			} else {
				return callback({paramArr: null});
			}
		}
	});
} /* _Find */

function _Update(Obj, callback){
	Obj.save(function(err, savedDoc){
		if(err){
			return callback({Err: err});
		} else {
			if(savedDoc){
				return callback({SavedDoc: savedDoc});
			} else {
				return callback({Err: 'Update Failed', SavedDoc: null});
			}
		}
	});
} /* _Update */

module.exports = class Parameter {
    constructor(){}

	New(Object, callback){
		var NewDetail = new param(Object);

		NewDetail.save(function (err, savedDoc){
			return callback({Err:err, SavedDoc: savedDoc});
		});
	} /* _New */

	Find(KeyValuePair, callback){
		_Find(KeyValuePair, function(Resp){
			return callback(Resp);
		});
	} /* Find */

	FindOne(KeyValuePair, callback){
		param.findOne(KeyValuePair, function(err, Param){
			if(err){
				return callback({Err: err});
			} else {
				if(Param){
					return callback({Param: Param});
				} else {
					return callback({Err: 'Not Found', Param: null});
				}
			}
		});
	} /* FindOne */

	Update(Obj, callback){
		_Update(Obj, function(Resp){
			return callback(Resp);
		});
	} /* Update */

	GetGeneratedCartonID(callback){
		let Msg = null;
		_Find({}, function(Resp){
			let Parameters = Resp.paramArr;
			if(Resp.Err || !Parameters || Parameters.length <= 0){
				Msg = "Failed to find parameters in the system to generate carton ID for desktop autobagger";
				//log.WriteToFile(null, Msg);
				return callback({error:Msg});
			} else {
				if(Parameters[0].autoBagsSeqNo){
					let GenCartonID = 10000000 + Parameters[0].autoBagsSeqNo;
					Parameters[0].autoBagsSeqNo += 1;

					Parameters[0].save(function(err3, savedParam){
						if(!err3 && savedParam){
							return callback({CartonID: GenCartonID.toString()});
						} else {
							Msg = "Failed to update new parameter sequence number ";
							//log.WriteToFile(null, Msg);
							return callback({error:Msg});
						}
					});
				} else {
					Msg = "Failed to find autobagger carton sequence number parameter in the system ";
					//log.WriteToFile(null, Msg);
					return callback({error:Msg});
				}
			}
		});
	} /* GetGeneratedCartonID */

	ParamGetPwCartonDetails(PickList, CurrentOrderToProcess, callback){
		let Msg = null;
		_Find({}, function(Resp){
			let Parameters = Resp.paramArr;
			if(Resp.Err || !Parameters || Parameters.length <= 0){
				Msg = "Failed to find parameters in the system ";
				//log.WriteToFile(null, Msg);
				return callback({error:Msg});
			} else {
				let CartonPrefix = 1;

				if(Parameters[0].weightSettings && Parameters[0].weightSettings.length > 0){
					let paramIndex = 0;
					while(paramIndex < Parameters[0].weightSettings.length){
						if(Parameters[0].weightSettings[paramIndex].StockSize == CartonPrefix){
							break;
						}
						paramIndex++;
					}

					if(paramIndex < Parameters[0].weightSettings.length){
						let CartonDetails = {
							EmptyCartonWeight: Parameters[0].weightSettings[paramIndex].weight,
							MinTolerance: Parameters[0].weightSettings[paramIndex].LowerTolerance,
							MaxTolerance: Parameters[0].weightSettings[paramIndex].UpperTolerance
						}

						return callback(CartonDetails);
					} else {
						Msg = "Failed to find Carton weightSettings to match order carton field for carton " +
							   PickList.orders[CurrentOrderToProcess].cartonID + " of order " + PickList.orders[CurrentOrderToProcess].orderID;
						//log.WriteToFile(null, Msg);
						return callback({error:Msg});
					}
				} else {
					Msg = "Failed to find weightSettings parameter in the system ";
					//log.WriteToFile(null, Msg);
					return callback({error:Msg});
				}
			}
		});
	} /* ParamGetPwCartonDetails */

	CalcCartonSizesForOrdOnStation(Station, callback){
        let CartonSizeArr = [];

		_Find({}, function(Resp){
			let Parameters = Resp.paramArr;
			if(!Resp.Err && Parameters.length > 0){
				if(Parameters[0].weightSettings && Parameters[0].weightSettings.length > 0){
					let u = 0;
                                        let BestFit = null;
					while(u < Station.pickList.orders.length){
						let x = 0;
						let total = 0;
						while(x < Station.pickList.orders[u].items.length){
							if(Station.pickList.orders[u].items[x].picked){
								total += (Station.pickList.orders[u].items[x].pickQty *
										  Station.pickList.orders[u].items[x].eachVolume);
							}
							x++;
						}

						let v = 0;
						//let BestFit = null;
						while(v < Parameters[0].weightSettings.length){
							if(total < Parameters[0].weightSettings[v].volumeCapacity && Parameters[0].weightSettings[v].isCarton){
								if(!BestFit){
									BestFit = v;
								} else {
									if(Parameters[0].weightSettings[BestFit].volumeCapacity > Parameters[0].weightSettings[v].volumeCapacity){
										BestFit = v;
									}
								}
							}
							v++;
						}

						if(BestFit){
							Station.pickList.orders[u].cartonSize = Parameters[0].weightSettings[BestFit].StockSize;
							Station.pickList.orders[u].useTransporter = Parameters[0].weightSettings[BestFit].useTransporter;
							Station.pickList.orders[u].picklistWeight = 0;
							Station.pickList.orders[u].minWeightLimit = Parameters[0].weightSettings[BestFit].LowerTolerance;
							Station.pickList.orders[u].maxWeightLimit = Parameters[0].weightSettings[BestFit].UpperTolerance;
							Station.pickList.orders[u].transporterWeight = null;
							Station.pickList.orders[u].transporterID = null;
							Station.pickList.orders[u].packComplete = false;
							Station.pickList.orders[u].orderVerified = false;
							Station.pickList.orders[u].printed = false;
							Station.pickList.orders[u].cartonID = null;
						}
						u++;
					}

					u = 0;
					while(u < Parameters[0].weightSettings.length){
						if(Parameters[0].weightSettings[u].isCarton != null){
							if(Parameters[0].weightSettings[u].isCarton == true){
								if(u == BestFit){
									CartonSizeArr.push({Size: Parameters[0].weightSettings[u].StockSize,
														Selected: "selected", Transporter: Parameters[0].weightSettings[u].useTransporter,
														weight: Parameters[0].weightSettings[u].weight,
														minWeightLimit: Parameters[0].weightSettings[u].LowerTolerance,
														maxWeightLimit: Parameters[0].weightSettings[u].UpperTolerance});
								} else {
									CartonSizeArr.push({Size: Parameters[0].weightSettings[u].StockSize,
														Selected: "",
														Transporter: Parameters[0].weightSettings[u].useTransporter,
														weight: Parameters[0].weightSettings[u].weight,
														minWeightLimit: Parameters[0].weightSettings[u].LowerTolerance,
														maxWeightLimit: Parameters[0].weightSettings[u].UpperTolerance});
								}
							}
						}
						u++;
					}

					return callback({Station: Station, CartonSizeArr: CartonSizeArr});
				}
			}
		});
	} /* CalcCartonSizesForOrdOnStation */

	GetAutobaggerMaxVolume(callback){
		_Find({}, function(Resp){
			let Parameters = Resp.paramArr;
			if(Resp.Err || !Parameters || Parameters.length <= 0){
				let Msg = "Failed to find parameters in the system ";
				//log.WriteToFile(null, Msg);
				return callback({error:Msg});
			} else {
				let CartonPrefix = 1;

				if(Parameters[0].weightSettings && Parameters[0].weightSettings.length > 0){
					let paramIndex = 0;
					while(paramIndex < Parameters[0].weightSettings.length){
						if(Parameters[0].weightSettings[paramIndex].StockSize == CartonPrefix){
							break;
						}
						paramIndex++;
					}

					if(paramIndex < Parameters[0].weightSettings.length){
						return callback({volumeCapacity:Parameters[0].weightSettings[paramIndex].volumeCapacity});
					} else {
						Msg = "Failed to find Carton weightSettings to match order carton field for carton " +
							   PickList.orders[CurrentOrderToProcess].cartonID + " of order " + PickList.orders[CurrentOrderToProcess].orderID;
						//log.WriteToFile(null, Msg);
						return callback({error:Msg});
					}
				} else {
					Msg = "Failed to find weightSettings parameter in the system ";
					//log.WriteToFile(null, Msg);
					return callback({error:Msg});
				}
			}
		});
	} /* GetAutobaggerMaxVolume */

	GetTransporterWeightDetails(Station, callback){
		let Msg = null;
	  	_Find({}, function(Resp){
			let Parameters = Resp.paramArr;
			if(Resp.Err || !Parameters || Parameters.length <= 0){
				Msg = "Failed to find parameters in the system ";
				//log.WriteToFile(null, Msg);
				return callback({Msg: Msg, Station: Station});
			} else {
				let StnTr = Station.currentTransporter;

				let CartonPrefix = StnTr[0];
				if(StnTr[1] != '0'){
					CartonPrefix = StnTr[0] + StnTr[1];
				}

		   		let TrStockSize = parseInt(CartonPrefix);

		   		if(Parameters[0].weightSettings && Parameters[0].weightSettings.length > 0){
			  		let paramIndex = 0;
			  		while(paramIndex < Parameters[0].weightSettings.length){
				 		if(Parameters[0].weightSettings[paramIndex].StockSize == TrStockSize){
							break;
				 		}
				 		paramIndex++;
			  		}

			  		if(paramIndex < Parameters[0].weightSettings.length){
				 		let TotalWeight = Parameters[0].weightSettings[paramIndex].weight;

				 		let x = 0;
				 		while(x < Station.pickList.orders.length){
				   			if(Station.pickList.orders[x].packComplete == true &&
					  		   Station.pickList.orders[x].useTransporter == true){
					 			TotalWeight += Station.pickList.orders[x].picklistWeight;
				   			}
				   			x++;
				 		}

				 		let MinTolerance = TotalWeight - Parameters[0].weightSettings[paramIndex].LowerTolerance;
				 		let MaxTolerance = TotalWeight + Parameters[0].weightSettings[paramIndex].UpperTolerance;

				 		let TransporterWeights = {
				 			MinTolerance: MinTolerance,
				   			MaxTolerance: MaxTolerance,
				   			TotalWeight: TotalWeight
				 		}

				 		return callback(TransporterWeights);
			  		} else {
						Msg = "Failed to find Transporter weightSettings ";
						//log.WriteToFile(null, Msg);
						return callback({Msg: Msg, Station: Station});
			  		}
		   		} else {
			  		Msg = "Failed to find weightSettings parameter in the system ";
			  		//log.WriteToFile(null, Msg);
			  		return callback({Msg: Msg, Station: Station});
		   		}
			}
	  	});
	} /* GetTransporterWeightDetails */

	GetTransporterTolerance(transporter, callback){
		_Find({}, function(Resp){
			let Parameters = Resp.paramArr;
			if(Resp.Err || !Parameters || Parameters.length <= 0){
		   		//log.WriteToFile(null, "Failed to find parameters in the system ");
		   		return callback({Error:"Failed to find parameters in the system "});
			} else {
				let StnTr = transporter;

				let CartonPrefix = StnTr[0];
				if(StnTr[1] != '0'){
					CartonPrefix = StnTr[0] + StnTr[1];
				}

		   		let TrStockSize = parseInt(CartonPrefix);

		   		if(Parameters[0].weightSettings && Parameters[0].weightSettings.length > 0){
			  		let paramIndex = 0;
			  		while(paramIndex < Parameters[0].weightSettings.length){
				 		if(Parameters[0].weightSettings[paramIndex].StockSize == TrStockSize){
							break;
				 		}
				 		paramIndex++;
			  		}

			  		if(paramIndex < Parameters[0].weightSettings.length){
						let TransporterWeights = {
				   			MinTolerance: Parameters[0].weightSettings[paramIndex].LowerTolerance,
				   			MaxTolerance: Parameters[0].weightSettings[paramIndex].UpperTolerance,
				   			EmptyTransporter: Parameters[0].weightSettings[paramIndex].weight
				 		}

				 		return callback(TransporterWeights);
			  		} else {
						//log.WriteToFile(null, "Failed to find Transporter weightSettings ");
						return callback({Error:"Failed to find Transporter weightSettings "});
			  		}
		   		} else {
			  		//log.WriteToFile(null, "Failed to find weightSettings parameter in the system ");
			  		return callback({Error:"Failed to find weightSettings parameter in the system "});
		   		}
			}
	  	});
	} /* CalculateTransporterWeight */

} /* Parameter */
