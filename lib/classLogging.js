var moment = require('moment');
//var TrLog  = require('trLogging');

module.exports = class WriteToLog {
    constructor(){}

	WriteToFile(Process, Msg){
		if(Msg){
			if(Process){
				console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' |' + Process + '| ' + Msg);
			} else {
				console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' |' + Msg);
			}
		}
	} /* WriteToFile */

	WriteTransactionToDB(Key, In, Out, Direction, Status, callback){
		var Tr = new TrLog({
					 TransactionID: Key,
					 Request: In,
					 Response: Out,
					 Direction: Direction,
					 Status: Status})

		Tr.save(function(err, saveDoc){
			return callback();
		});
	} /* WriteTransactionToDB */
}