var moment   = require('moment');
var apexLib  = require('apex_lib/moduleIndex.js');

module.exports = class WriteToLog {
    constructor(){}

	WriteToFile(Process, Msg){
		if(Msg){
			console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' |' + Process + '| ' + Msg + '|\n--------------------\n');
		}
	} /* WriteToFile */

	/* Function that inserts information into the SCCONNECTTRANSACTION table that indicates the result of an import. This table resides in the WHE system DB. */
	insertSCConnectTransaction (transactionType, pluginID, transactionStatus, transactionSet, transactionBOType, transactionBOKey, transactionData, transactionError, callback) {
		/* Input parameters to be sent to the stored proc. */
		var inputParams = {
			'transactionType': transactionType,
			'pluginID': pluginID,
			'transactionStatus': transactionStatus,
			'transactionSet': transactionSet,
			'transactionBOType': transactionBOType,
			'transactionBOKey': transactionBOKey,
			'transactionData': transactionData,
			'transactionError': transactionError
		};

		apexLib.msSQL.storedProc.execute(global.msSQLClient, 'apx_transaction_insert', null, inputParams, null, function (error, recordSet, returnValue){
			return callback();
		});
	} /* insertSCConnectTransaction */

	UpdateSCConnectTransaction (tmpID, pluginID, transactionStatus, transactionBOType, transactionBOKey, transactionError, transactionData, callback){
		/* Input parameters to be sent to the stored proc. */
		var inputParams = {
			'pluginID': pluginID,
			'transactionStatus': transactionStatus,
			'transactionBOType': transactionBOType,
			'transactionBOKey': transactionBOKey,
			'transactionError': transactionError,
			'transactionData': transactionData,
			'tmpID': tmpID
		}

		apexLib.msSQL.storedProc.execute(global.msSQLClient, 'apx_transaction_update', null, inputParams, null, function (error, recordSet, returnValue){
			return callback();
		})
	} /* UpdateSCConnectTransaction */
}
