var express    = require('express');
var bodyParser = require('body-parser');
var config     = require('wms_api');
var logging    = require('./lib/classLogging');

//require('body-parser-xml')(bodyParser);

var ProcName         = config.SERVICES.PACKLIST.NAME;
var log              = new logging();
var packListRouter   = require('./routes/warehouseExpert/packingList/packListUpdateRouter.js');
var apexLib          = require('apex_lib/moduleIndex.js');

apexLib.msSQL.client(config.MSSQL.User, config.MSSQL.Pwd, config.MSSQL.Port, config.MSSQL.Url, config.MSSQL.Database, function (err, client){
    if (err) {
        log.WriteToFile(ProcName, 'Unable to connect to SQL Server');
    } else {
		log.WriteToFile(ProcName, 'Connect To MsSql DB');
        global.msSQLClient = client;
    }
});

var app = express();

/* Allow JSON to be parsed by the body parser. */
app.use(bodyParser.json());

/* Allow URL encoded messages to be accepted by bodyparser. */
app.use(bodyParser.urlencoded({ extended: false }));

/* Allow plain text to be parsed by bodyparser. */
app.use(bodyParser.text());

/* Set the sales order router. */
app.use(packListRouter);

/* Catch 404 and forward to error handler. */
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/* Create the server. */
var server = app.listen(config.SERVICES.PACKLIST.PORT, function (){
    log.WriteToFile(ProcName, 'Server listening on port ' + server.address().port);
});

module.exports = app;
