var express  = require('express');
var moment   = require('moment');
var apexLib  = require('apex_lib/moduleIndex.js');
var mongoose = require('mongoose');
var Serial   = require('serialModel');
var config   = require('wms_api');
var async    = require('async');
var Params   = require('parameterModel')

var BatchSize = config.SERIAL_FETCH.BATCH_SIZE;
var QueryClause = config.SERIAL_FETCH.BATCH_CLAUSE;
var MyWarehouse = config.SERIAL_FETCH.THISWAREHOUSE;
var NextWarehouse = config.SERIAL_FETCH.NEXTWAREHOUSE;

var UpdateRecs = [];

apexLib.msSQL.client(config.MSSQL.User, config.MSSQL.Pwd, config.MSSQL.Port, config.MSSQL.Url, config.MSSQL.Database, function (err, Sqlclient) {
    if (err) {
        WriteToFile('Unable to connect to SQL Server');
    } else {
		WriteToFile('Connect To MsSql DB');
        global.msSQLClient = Sqlclient;

		mongoose.connect(config.mongoDB.url, function (error, Mclient) {
			if (error) {
				WriteToFile('MongoDB: Connection Error: ' + error);
			} else {
				WriteToFile('MongoDB: Connection Established.');
				WaitForTrigger();
			}
		}); /* Mongo Connection */
    }
});

function WriteToFile(Msg){
	if(Msg){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' ' + Msg);
	}
} /* WriteToFile */

function CheckIfNextToRun(callback){
	Params.find({}, function(err, Parameters){
		if(err || !Parameters || Parameters.length <= 0){
			WriteToFile('No parameters found');
			return callback(false);
		}

		if(!Parameters[0].serialSync){
			WriteToFile('No serial sync setting');
			return callback(false);
		}

		if(Parameters[0].serialSync != 'PULL'){
			WriteToFile('Serial push currently running.');
			return callback(false);
		}

		WriteToFile('Serial pull can be ran');
		return callback(true);
	});
} /* CheckIfNextToRun */

function SwitchSerialSyncStatus(callback){
	Params.find({}, function(err, Parameters){
		if(err || !Parameters || Parameters.length <= 0){
			WriteToFile('No parameters found');
			return callback();
		}

		if(!Parameters[0].serialSync){
			WriteToFile('No serial sync setting');
			return callback();
		}

		Parameters[0].serialSync = 'PUSH';
		Parameters[0].save(function(err, sd){
			return callback();
		});
	});
} /* SwitchSerialSyncStatus */

function CheckIfNextToPull(callback){
	let SelQuery = "select * from apx_wh_sys_param where wh_param_code='WcsWarehouseMode'";

	apexLib.msSQL.query.execute(global.msSQLClient, SelQuery, null, function (error, recordSet){
		if (error) {
			return callback(true);
		}

		if(recordSet.length <= 0){
			return callback(true);
		}

		if(recordSet[0].wh_param_value != 'MULTI'){
			return callback(true);
		}

		SelQuery = "select * from apx_wh_sys_param where wh_param_code='WcsWarehouseSeq'";
		apexLib.msSQL.query.execute(global.msSQLClient, SelQuery, null, function (error, recordSet){
			if (error) {
				return callback(true);
			}

			if(recordSet.length <= 0){
				return callback(true);
			}

			if(recordSet[0].wh_param_value != MyWarehouse){
				return callback(false);
			}

			WriteToFile(MyWarehouse + ' warehouse is next to pull from serial table');
			return callback(true);
		});
	});
} /* CheckIfNextToPull */

function SwitchSerialPullWarehouse(callback){
	let UpdateQuery = "Update apx_wh_sys_param set wh_param_value = '" + NextWarehouse + "' where wh_param_code='WcsWarehouseSeq'";
	apexLib.msSQL.query.execute(global.msSQLClient, UpdateQuery, null, function (error, recordSet){
		if (error) {
			WriteToFile('RetrieveSerialsFromSql says: Error while updating apx_wh_sys_param table ' + error.message);
		} else {
			WriteToFile('Warehouse switched to ' + NextWarehouse);
		}

		return callback();
	});
} /* SwitchSerialPullWarehouse */

function WaitForTrigger(){
	setTimeout(function(){
		CheckIfNextToRun(function(Resp){
			if(!Resp){
				return WaitForTrigger();
			}

			CheckIfNextToPull(function(Resp){
				if(!Resp){
					SwitchSerialSyncStatus(function(){
						return WaitForTrigger();
					});
					return;
				}

				RetrieveSerialsFromSql(function(){
					SwitchSerialSyncStatus(function(){
						SwitchSerialPullWarehouse(function(){
							return WaitForTrigger();
						});
					});
				});
			});
		});
	}, 30000);
} /* WaitForTrigger */

function RetrieveSerialsFromSql(callback){
	var SelQuery = "select distinct Top (" + BatchSize + ") [id],[serial],[loadID],[SKU],[ASNID],[UOM],[dispatched],[received],[orderID]" +
      			   " ,[userid],[serialweight],[activated],[parentNodeID],[serialStatus],[salesOrderID],[salesOrderLineID],[cartonID],[cartonNo]" +
      			   " ,[noOfCartons],[createDate],[lastEditDate] from apx_serial " + QueryClause;

	apexLib.msSQL.query.execute(global.msSQLClient, SelQuery, null, function (error, recordSet){
		if (error) {
			WriteToFile('RetrieveSerialsFromSql says: Error while querying apx_serial table ' + error.message);
			return callback();
		}

		UpdateRecs = [];

		if(recordSet.length <= 0){
			WriteToFile('Nothing To Process');
			FindUntriggered(function(){
				return callback();
			});

			return;
		}

		var q = async.queue(ProcessBulkResultNew, 1000);

		q.push(recordSet, function(Resp){
			if(Resp.Err){
				WriteToFile('Error while processing serial ' + Resp.Serial + ' | Error: ' + Resp.Err);
			}
		});

		q.drain = function() {
			/*let x = 0;
			let InClause = '(';
			while(x < UpdateRecs.length){
				InClause += UpdateRecs[x];
				if(x+1 < UpdateRecs.length){
					InClause += ',';
				}
				x++;
			}
			InClause += ')';*/

			SetFlagFalseIntr(UpdateRecs, 0, function(){
				WriteToFile('All Done processing');
				FindUntriggered(function(){
					return callback();
				});
			});
		}
	});
} /* RetrieveSerialsFromSql */

function FindUntriggered(callback){
	return callback();
  	let SelQuery = "select * from apx_serial where received = 1 and dispatched  = 0 and (substring(convert(nvarchar(20), createDate,114), 0, 6)  = substring(convert(nvarchar(20), lastEditDate,114), 0, 6))";

	apexLib.msSQL.query.execute(global.msSQLClient, SelQuery, null, function (error, recordSet){
		if (error) {
			WriteToFile('FindUntriggered says: Error while querying apx_serial table ' + error.message);
			return callback();
		}

		if(recordSet.length <= 0){
			WriteToFile('No Untriggered serials found');
			return callback();
		}

		let updtQuery = "update apx_serial set JPsyncRequired = 1, MDsyncRequired = 1 where received = 1 and dispatched  = 0 and (substring(convert(nvarchar(20), createDate,114), 0, 6)  = substring(convert(nvarchar(20), lastEditDate,114), 0, 6))";

		apexLib.msSQL.query.execute(global.msSQLClient, updtQuery, null, function (error, recordSet1){
			if (error) {
				WriteToFile('FindUntriggered says: Error while updating apx_serial table ' + error.message);
			} else {
				WriteToFile(recordSet.length + ' untriggered serials updated to sync required');
			}

			return callback();
		});
	});
} /* FindUntriggered */

function ProcessBulkResultNew(row, callback){
	Serial.find({'serial': row.serial}).limit(1).exec(function(err, SerialRecArr){
		if(err){
			return callback({Serial: row.serial, Err: err});
		}

		if(SerialRecArr.length <= 0 || !SerialRecArr[0]){
			AddNewSerial(row, function(){
				return callback({Serial: row.serial, Err: null, Process: 'ADD'});
			});

			return;
		}

		let SerialRec = SerialRecArr[0];
		UpdateSerialRecord(SerialRec, row, function(){
			return callback({Serial: row.serial, Err: null, Process: 'UPDATE'});
		});
	});
} /* ProcessBulkResultNew */

function SetFlagFalse(RecNo, callback){
	var UpdateQuery = "Update apx_serial set MDsyncRequired = 0 where id = " + parseInt(RecNo);
	apexLib.msSQL.query.execute(global.msSQLClient, UpdateQuery, null, function (error, recordSet){
		if (error) {
			WriteToFile('RetrieveSerialsFromSql says: Error while updating apx_serial table ' + error.message);
		} else {
			WriteToFile('Update Success');
		}

		return callback();
	});
} /* SetFlagFalse */

function SetFlagFalseBulk(InClause, callback){
	WriteToFile('Update Bulk Clause: ' + InClause);
	var UpdateQuery = "Update apx_serial set MDsyncRequired = 0 where id in " + InClause;
	apexLib.msSQL.query.execute(global.msSQLClient, UpdateQuery, null, function (error, recordSet){
		if (error) {
			WriteToFile('RetrieveSerialsFromSql says: Error while updating apx_serial table ' + error.message);
		} else {
			WriteToFile('Update Bulk Success');
		}

		return callback();
	});
} /* SetFlagFalseBulk */

function SetFlagFalseIntr(UpdateRecs, index, callback){
	if(index >= UpdateRecs.length){
		return callback();
	}

	let RecNo = UpdateRecs[index];
	let UpdateQuery = "Update apx_serial set MDsyncRequired = 0 where id = " + parseInt(RecNo);
	apexLib.msSQL.query.execute(global.msSQLClient, UpdateQuery, null, function (error, recordSet){
		if (error) {
			WriteToFile('RetrieveSerialsFromSql says: Error while updating apx_serial table ' + error.message);
		} else {
			WriteToFile('Update Id ' + RecNo + ' Success');
		}

		index++;
		return SetFlagFalseIntr(UpdateRecs, index, callback);
	});
} /* SetFlagFalseIntr */

function FindSerialCountForEachParent(task, callback){
	var SerialRecNo = task.RecNo;
	Serial.find({'parentRecNo': SerialRecNo, 'dispatched': false},function(err, ChildSerialArr){
		if(!err){
			return callback({ChildSerialArr: null});
		} else {
			return callback({ChildSerialArr: ChildSerialArr});
		}
	});
} /* FindSerialCountForEachParent */

function GetSerialChildCount(RecNo, callback){
	let SelQuery = "select Count(*) as SerialCount from apx_serial_hierarchy as Tbl1 join" +
	               " (select MAX(depth) as Cnt from apx_serial_hierarchy where id_ancestor = '" + RecNo + "') as Tbl2" +
	               " on Tbl1.depth = Tbl2.Cnt" +
				   " where id_ancestor ='" + RecNo  + "'";

	apexLib.msSQL.query.execute(global.msSQLClient, SelQuery, null, function (error, CountRecordSet){
		if (error) {
			WriteToFile('RetrieveSerialsFromSql says: Error while querying for serial count ' + error.message);
			return callback({Count: 1});
		}

		if(CountRecordSet.length > 0){
			return callback({Count: CountRecordSet[0].SerialCount});
		}

		return callback({Count: 1});
	});
} /* GetSerialChildCount */

function AddNewSerial(row, callback){
	GetSerialChildCount(row.id, function(Resp){
		let SerialCount = Resp.Count;

		let now = new Date();
		let SerialRec = new Serial({
			activated: row.activated,
			ASNID: row.ASNID,
			cartonID: row.cartonID,
			cartonNo: row.cartonNo,
			dispatched: row.dispatched,
			RecNo: row.id,
			loadID: row.loadID,
			noOfCartons: row.noOfCartons,
			orderID : row.orderID,
			parentRecNo : row.parentNodeID,
			received: row.received,
			salesOrderID: row.salesOrderID,
			salesOrderLineID: row.salesOrderLineID,
			serial: row.serial,
			serialStatus: row.serialStatus,
			serialWeight: row.serialweight,
			SKU: row.SKU,
			UOM: row.UOM,
			userID: row.userid,
			serialCount: SerialCount,
			last_update_to_wcs: now,
			last_update_to_wms: null
		});

		SerialRec.save(function(err, docs){
			if(!err){
				WriteToFile('SUCCESS: ' + docs.RecNo);
				/*SetFlagFalse(docs.RecNo, function(){
					WriteToFile('Rec ' + docs.RecNo + ' sync set to false');
					return callback();
				});*/

				UpdateRecs.push(docs.RecNo);
				return callback();
			}

			WriteToFile('FAIL: ' + row.serial + ' | ' + err);
			return callback();
		});
	});
} /* AddNewSerial */

function UpdateSerialRecord(SerialRec, row, callback){
	if(!SerialRec || !row){
		return callback();
	}

	GetSerialChildCount(row.id, function(Resp){
		let SerialCount = Resp.Count;

		let now = new Date();
		SerialRec.activated = row.activated;
		SerialRec.ASNID = row.ASNID;
		SerialRec.cartonID = row.cartonID;
		SerialRec.cartonNo = row.cartonNo;
		SerialRec.dispatched = row.dispatched;
		SerialRec.RecNo = row.id;
		SerialRec.loadID = row.loadID;
		SerialRec.noOfCartons = row.noOfCartons;
		SerialRec.orderID = row.orderID;
		SerialRec.parentRecNo = row.parentNodeID;
		SerialRec.received = row.received;
		SerialRec.salesOrderID = row.salesOrderID;
		SerialRec.salesOrderLineID = row.salesOrderLineID;
		SerialRec.serial = row.serial;
		SerialRec.serialStatus = row.serialStatus;
		SerialRec.serialWeight = row.serialweight;
		SerialRec.SKU = row.SKU;
		SerialRec.UOM = row.UOM;
		SerialRec.userID = row.userid;
		SerialRec.serialCount = SerialCount;
		SerialRec.last_update_to_wcs = now;

		SerialRec.save(function(err, SavedRec){
			if(!err){
				WriteToFile('Updated Serial number ' + SavedRec.serial);
				/*SetFlagFalse(SavedRec.RecNo, function(){
					WriteToFile('Rec ' + SavedRec.RecNo + ' sync set to false');
					return callback();
				});*/

				UpdateRecs.push(SavedRec.RecNo);
				return callback();
			}

			WriteToFile('Failed to update Serial number ' + row.serial);
			return callback();
		});
	});
} /* UpdateSerialRecord */
