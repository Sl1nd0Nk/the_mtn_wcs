var express    = require('express');
var bodyParser = require('body-parser');
var config     = require('wms_api');
var logging    = require('classLogging');
var mongoose   = require('mongoose');

//require('body-parser-xml')(bodyParser);

var ProcName         = config.SERVICES.ORDER_HEADER.NAME;
var log              = new logging();
var Router           = require('./routes/warehouseExpert/orderHeader/wcsSalesOrderHeader.js');
var SeqRouter        = require('./routes/warehouseExpert/carton/cartonsequence.js');
var apexLib          = require('apex_lib/moduleIndex.js');

apexLib.msSQL.client(config.MSSQL.User, config.MSSQL.Pwd, config.MSSQL.Port, config.MSSQL.Url, config.MSSQL.Database, function (err, client){
    if (err) {
        log.WriteToFile(ProcName, 'Unable to connect to SQL Server');
    } else {
		log.WriteToFile(ProcName, 'Connect To MsSql DB');
        global.msSQLClient = client;

		mongoose.connect(config.mongoDB.url, function (error, client) {
			if (error) {
				log.WriteToFile(ProcName, 'MongoDB: Connection Error: ' + error);
			} else {
				log.WriteToFile(ProcName, 'MongoDB: Connection Established.');
			}
		}); /* Mongo Connection */
    }
});

var app = express();

/* Allow JSON to be parsed by the body parser. */
app.use(bodyParser.json());

/* Allow URL encoded messages to be accepted by bodyparser. */
app.use(bodyParser.urlencoded({ extended: false }));

/* Allow plain text to be parsed by bodyparser. */
app.use(bodyParser.text());

/* Set the sales order router. */
app.use(Router);
app.use(SeqRouter);

/* Catch 404 and forward to error handler. */
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/* Create the server. */
var server = app.listen(config.SERVICES.ORDER_HEADER.PORT, function (){
	log.WriteToFile(ProcName, 'Server listening on port ' + server.address().port);
});

module.exports = app;
