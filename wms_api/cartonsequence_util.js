var config      = require('wms_api');
var logging     = require('classLogging');
var moment      = require('moment');
var mongoose    = require('mongoose');
var async       = require('async');
var pickList    = require('pickListModel');
var cartseq     = require('CartonSequenceModel');

mongoose.connect(config.mongoDB.url, function (error, client) {
	if (error) {
		console.log('MongoDB: Connection Error: ' + error);
	} else {
		console.log('MongoDB: Connection Established.');
		return FindAnyPartialShippedOrders();
	}
}); /* Mongo Connection */

function CreateCartSeq(PL, callback){
	cartseq.findOne({'PicklistId': PL.orders[0].pickListID}, function(err, Carton){
		if(err){
			return callback();
		}

		if(Carton){
			return callback();
		}

		let NewRec = new cartseq({
			OrderId: PL.orders[0].orderID,
			PicklistId: PL.orders[0].pickListID,
			CartonId: PL.orders[0].cartonID,
			CartonNo: PL.orders[0].toteNumber,
			NoOfCartons: PL.orders[0].numberOfTotes
		});

		NewRec.save(function(err, savedDoc){
			return callback();
		});
	});
} /* CreateCartSeq */

function ProcessEachPlForOrder(PlArr, a, callback){
	if(a >= PlArr.length){
		return callback();
	}

	if(PlArr[a].Status == 'PACKED' || PlArr[a].Status == 'SHIPPED'){
		CreateCartSeq(PlArr[a], function(){
			a++;
			return ProcessEachPlForOrder(PlArr, a, callback);
		});

		return;
	}

	a++;
	return ProcessEachPlForOrder(PlArr, a, callback);
} /* ProcessEachPlForOrder */

function ProcEachOrderInPL(PL, y, callback){
	if(y >= PL.orders.length){
		return callback();
	}

	let OrderID = PL.orders[y].orderID;
	pickList.find({'orders.orderID': OrderID}, function (err, PlArr){
		if(err){
			y++;
			return ProcEachOrderInPL(PL, y, callback);
		}

		if(!PlArr || PlArr.length <= 0){
			y++;
			return ProcEachOrderInPL(PL, y, callback);
		}

		ProcessEachPlForOrder(PlArr, 0, function(){
			y++;
			return ProcEachOrderInPL(PL, y, callback);
		});
	});
} /* ProcEachOrderInPL */

function ProcessEachPL(docArr, x, callback){
	if(x >= docArr.length){
		return callback();
	}

	let PL = docArr[x];
	ProcEachOrderInPL(PL, 0, function(){
		x++;
		return ProcessEachPL(docArr, x, callback);
	});
} /* ProcessEachPL */

function FindAnyPartialShippedOrders(){
	pickList.find({'Status': 'PICKED'}, function (err, docArr){
		if(err){
			console.log(err);
			return;
		}

		if(!docArr || docArr.length <= 0){
			console.log('None Found');
			return;
		}

		ProcessEachPL(docArr, 0, function(){
			console.log('Finished');
			return;
		});
	});
} /* FindAnyPartialShippedOrders */