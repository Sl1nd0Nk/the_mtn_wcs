var express  = require('express');
var moment   = require('moment');
var apexLib  = require('apex_lib/moduleIndex.js');
var mongoose = require('mongoose');
var Serial   = require('serialModel');
var config   = require('wms_api');
var async    = require('async');
var Params   = require('parameterModel')

apexLib.msSQL.client(config.MSSQL.User, config.MSSQL.Pwd, config.MSSQL.Port, config.MSSQL.Url, config.MSSQL.Database, function (err, Sqlclient) {
    if (err) {
        WriteToFile('Unable to connect to SQL Server');
    } else {
		WriteToFile('Connect To MsSql DB');
        global.msSQLClient = Sqlclient;

		mongoose.connect(config.mongoDB.url, function (error, Mclient) {
			if (error) {
				WriteToFile('MongoDB: Connection Error: ' + error);
			} else {
				WriteToFile('MongoDB: Connection Established.');
				WaitForTrigger();
			}
		}); /* Mongo Connection */
    }
});

function WriteToFile(Msg){
	if(Msg){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' ' + Msg);
	}
} /* WriteToFile */

function CheckIfNextToRun(callback){
	Params.find({}, function(err, Parameters){
		if(err || !Parameters || Parameters.length <= 0){
			WriteToFile('No parameters found');
			return callback(false);
		}

		if(!Parameters[0].serialSync){
			WriteToFile('No serial sync setting');
			return callback(false);
		}

		if(Parameters[0].serialSync != 'PUSH'){
			WriteToFile('Serial pull currently running.');
			return callback(false);
		}

		WriteToFile('Serial push can be ran');
		return callback(true);
	});
} /* CheckIfNextToRun */

function SwitchSerialSyncStatus(callback){
	Params.find({}, function(err, Parameters){
		if(err || !Parameters || Parameters.length <= 0){
			WriteToFile('No parameters found');
			return callback();
		}

		if(!Parameters[0].serialSync){
			WriteToFile('No serial sync setting');
			return callback();
		}

		Parameters[0].serialSync = 'PULL';
		Parameters[0].save(function(err, sd){
			return callback();
		});
	});
} /* SwitchSerialSyncStatus */

function WaitForTrigger(){
	setTimeout(function(){
		CheckIfNextToRun(function(Resp){
			if(!Resp){
				return WaitForTrigger();
			}

			FindSerialsToUpdateToWms(function(){
				SwitchSerialSyncStatus(function(){
					return WaitForTrigger();
				});
			});
		});
	}, 30000);
} /* WaitForTrigger */

function FindSerialsToUpdateToWms(callback){
	Serial.find({'resyncRequired': true}).limit(10000).exec(function(err, SerialRecArr){
		if(err){
			WriteToFile('Error while querying for serials to push ' + err);
			return callback();
		}

		if(SerialRecArr.length <= 0){
			WriteToFile('No Serials to push ');
			return callback();
		}

		let Parents = [];
		let y = 0;
		while(y < SerialRecArr.length){
			if((SerialRecArr[y].dispatched == true || SerialRecArr[y].serialStatus == 'BROKEN') &&
				SerialRecArr[y].parentRecNo != null && SerialRecArr[y].parentRecNo != '0'){
				let z = 0;
				let FoundInList = false;
				while(z < Parents.length){
					if(Parents[z].parentRecNo == SerialRecArr[y].parentRecNo){
						FoundInList = true;
						break;
					}

					z++;
				}

				if(FoundInList == false){
					Parents.push({parentRecNo: SerialRecArr[y].parentRecNo, ChildSer: SerialRecArr[y].serial});
				}
			}
			y++;
		}

		if(Parents.length <= 0){
			ProcessSerialList(SerialRecArr, 0, function(){
				WriteToFile('Done updating ' + SerialRecArr.length + ' serials to wms');
				return callback();
			});

			return;
		}

		var qUpdateParent  = async.queue(MarkParentsAsBroken, Parents.length);

		qUpdateParent.push(Parents, function (){});

		qUpdateParent.drain = function(){
			ProcessSerialList(SerialRecArr, 0, function(){
				WriteToFile('Done updating ' + SerialRecArr.length + ' serials to wms');
				return callback();
			});
		}
	});
} /* FindSerialsToUpdateToWms */

function ProcessSerialList(SerialRecArr, index, callback){
	if(index >= SerialRecArr.length){
		return callback();
	}

	let DispValue = 0;
	if(SerialRecArr[index].dispatched == true){
		DispValue = 1;
	}

	let UpdateQuery = "Update apx_serial set userid='" + SerialRecArr[index].userID + "', " +
					  "serialStatus='" + SerialRecArr[index].serialStatus + "', " +
					  "salesOrderLineID=" + SerialRecArr[index].salesOrderLineID + ", " +
					  "salesOrderID='" + SerialRecArr[index].salesOrderID + "', " +
					  "noOfCartons=" + SerialRecArr[index].noOfCartons + ", " +
					  "cartonNo=" + SerialRecArr[index].cartonNo + ", " +
					  "dispatched=" + DispValue + ", " +
					  "cartonID='" + SerialRecArr[index].cartonID + "', " +
					  "syncSource='MD', JPsyncRequired=1 where id = " + parseInt(SerialRecArr[index].RecNo);

	apexLib.msSQL.query.execute(global.msSQLClient, UpdateQuery, null, function (error, recordSet){
		if (error) {
			WriteToFile('PushSerials says: Error while updating apx_serial table ' + error.message);
			return callback();
		}

		if(SerialRecArr[index].dispatched == true && SerialRecArr[index].serialCount > 1){
			UpdateChildSerials(SerialRecArr[index], function(){
				WriteToFile('Child Serials updated ');
				WriteToFile(SerialRecArr[index].serial + ' Update Success');
				SerialRecArr[index].resyncRequired = false;
				SerialRecArr[index].last_update_to_wms = new Date();
				SerialRecArr[index].save(function(err, savedDoc){
					index++;
					return ProcessSerialList(SerialRecArr, index, callback);
				});
			});
		} else {
			WriteToFile(SerialRecArr[index].serial + ' Update Success');
			SerialRecArr[index].resyncRequired = false;
			SerialRecArr[index].last_update_to_wms = new Date();
			SerialRecArr[index].save(function(err, savedDoc){
				index++;
				return ProcessSerialList(SerialRecArr, index, callback);
			});
		}
	});
} /* ProcessSerialList */

function MarkParentsAsBroken(task, callback){
	let parentRecNo = task.parentRecNo;
	let Ser = task.ChildSer;

	Serial.findOne({'RecNo': parentRecNo}, function(err, ParenSerRec){
		if(err){
			WriteToFile("Failed to find serial " + Ser + "'s parent to mark as BROKEN | Err: " + err);
			return callback();
		}

		if(!ParenSerRec){
			WriteToFile("Serial " + Ser + "'s parent not found to mark as BROKEN");
			return callback();
		}

		if(ParenSerRec.dispatched == true || ParenSerRec.serialStatus == 'BROKEN'){
			return callback();
		}

		ParenSerRec.serialStatus = 'BROKEN';
		ParenSerRec.resyncRequired = true;
		ParenSerRec.save(function(err, savedDoc){
			return callback();
		});
	});
} /* MarkParentsAsBroken */

function UpdateChildren(task, callback){
	let ChildSer = task;

	ChildSer.save(function(err, savedDoc){
		if(err){
			WriteToFile('Failed to update serial ' + ChildSer.serial + ' | Err: ' + err);
		} else {
			if(savedDoc){
				WriteToFile('Successfully updated child serial ' + savedDoc.serial + ' to resync to WMS');
			}
		}

		return callback(null, null);
	});
} /* UpdateChildren */

function FindAndUpdateFromWms(Parent, callback){
	let UpdateQuery = "Update apx_serial set userid='" + Parent.userID + "', " +
					  "serialStatus='DISPATCHED', " +
					  "salesOrderLineID=" + Parent.salesOrderLineID + ", " +
					  "salesOrderID='" + Parent.salesOrderID + "', " +
					  "noOfCartons=" + Parent.noOfCartons + ", " +
					  "cartonNo=" + Parent.cartonNo + ", " +
					  "dispatched=true, " +
					  "cartonID='" + Parent.cartonID + "', " +
					  "syncSource='MD', JPsyncRequired=1 where parentNodeID = " + parseInt(Parent.RecNo);

	apexLib.msSQL.query.execute(global.msSQLClient, UpdateQuery, null, function (error, recordSet){
		if (error) {
			WriteToFile('PushSerials says: Error while updating apx_serial table ' + error.message);
		}

		return callback();
	});
} /* FindAndUpdateFromWms */

function UpdateChildSerials(Parent, callback){
	if(parseInt(Parent.RecNo) <= 0){
		WriteToFile("Serial " + Parent.serial + " RecNo is null");
		return callback();
	}

	Serial.find({'parentRecNo': Parent.RecNo}, function(err, ChildRecs){
		if(err){
			WriteToFile("Failed to find child serials for parent serial " + Parent.serial + " | Err " + err);
			return callback();
		}

		if(ChildRecs.length <= 0){
			WriteToFile("No child serials found for parent serial " + Parent.serial);
			FindAndUpdateFromWms(Parent, function(){
				return callback();
			});

			return;
		}

		let x = 0;
		while(x < ChildRecs.length){
			ChildRecs[x].cartonID = Parent.cartonID;
			ChildRecs[x].cartonNo = Parent.cartonNo;
			ChildRecs[x].noOfCartons = Parent.noOfCartons;
			ChildRecs[x].resyncRequired = true;
			ChildRecs[x].dispatched = Parent.dispatched;

			ChildRecs[x].salesOrderID = Parent.salesOrderID;
			ChildRecs[x].salesOrderLineID = Parent.salesOrderLineID;
			ChildRecs[x].serialStatus = 'DISPATCHED';
			ChildRecs[x].userID = Parent.userID;
			x++;
		}

		var qUpdateChildren  = async.queue(UpdateChildren, ChildRecs.length);

		qUpdateChildren.push(ChildRecs, function (err, data) {});

		qUpdateChildren.drain = function(){
			WriteToFile("Finished updating child serials for parent serial " + Parent.serial);
			return callback();
		}
	});
} /* UpdateChildSerials */
