var express  = require('express');
var moment   = require('moment');
var apexLib  = require('apex_lib/moduleIndex.js');
var mongoose = require('mongoose');
var config   = require('wms_api');
var async    = require('async');
var CanReq   = require('CancelRequestModel');
var pickList = require('pickListModel');

apexLib.msSQL.client(config.MSSQL.User, config.MSSQL.Pwd, config.MSSQL.Port, config.MSSQL.Url, config.MSSQL.Database, function (err, Sqlclient) {
    if (err) {
        WriteToFile('Unable to connect to SQL Server');
    } else {
		WriteToFile('Connect To MsSql DB');
        global.msSQLClient = Sqlclient;

		mongoose.connect(config.mongoDB.url, function (error, Mclient) {
			if (error) {
				WriteToFile('MongoDB: Connection Error: ' + error);
			} else {
				WriteToFile('MongoDB: Connection Established.');
				WaitForTrigger();
			}
		}); /* Mongo Connection */
    }
});

function WriteToFile(Msg){
	if(Msg){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' ' + Msg);
	}
} /* WriteToFile */

function WaitForTrigger(){
	setTimeout(function(){
		CheckWmsCancelRequest(function(){
			CheckForWmsRequestUpdates(function(){
				CheckIfWcsCancellationRequestFulfilled(function(){
					return WaitForTrigger();
				});
			});
		});
	}, 30000);
} /* WaitForTrigger */

function CheckIfWcsCancellationRequestFulfilled(callback){
	CanReq.find({'Required': false, 'Status': 'PENDING'}, function(err, ReqArr){
		if(err){
			WriteToFile(err);
			return callback();
		}

		if(!ReqArr || ReqArr.length <= 0){
			return callback();
		}

		ProcessEachWcsRequest(ReqArr, 0, function(){
			return callback();
		});
	});
} /* CheckIfWcsCancellationRequestFulfilled */

function CancelAnyShipped(PlArr, index, callback){
	if(index >= PlArr.length){
		return callback();
	}

	let PL = PlArr[index];
	if(PL.Status != 'SHIPPED'){
		index++;
		return CancelAnyShipped(PlArr, index, callback);
	}

	PL.Status = 'CANCELED';
	PL.orders[0].WmsOrderStatus = 'CANCELED';
	PL.orders[0].WmsPickListStatus = 'CANCELED';
	PL.orders[0].CancelDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
	PL.orders[0].CancelInstruction = 'Fetch Parcel From Courier: ' + PL.orders[0].courierID;

	PL.save(function(err, PL){
		index++;
		return CancelAnyShipped(PlArr, index, callback);
	});
} /* CancelAnyShipped */

function ProcessEachWcsRequest(ReqArr, x, callback){
	if(x >= ReqArr.length){
		return callback();
	}

	let Req = ReqArr[x];

	pickList.find({'orders.orderID': Req.OrderId}, function(err, PlArr){
		if(err){
			WriteToFile(err);
			x++;
			return ProcessEachWcsRequest(ReqArr, x, callback);
		}

		if(!PlArr || PlArr.length <= 0){
			x++;
			return ProcessEachWcsRequest(ReqArr, x, callback);
		}

		let y = 0;
		let AllShipped = true;
		while(y < PlArr.length){
			if(PlArr[y].Status != 'SHIPPED'){
				AllShipped = false;
				break;
			}
			y++;
		}

		if(AllShipped){
			Req.Status = 'SHIPPED';
			Req.Required = true;
			Req.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			Req.save(function(err, savedDoc){
				x++;
				return ProcessEachWcsRequest(ReqArr, x, callback);
			});

			return;
		}

		y = 0;
		let AllCancelled = true;
		while(y < PlArr.length){
			if(PlArr[y].orders.length == 1){
				if(PlArr[y].Status != 'CANCELED' && PlArr[y].Status != 'CANCELLED'){
					AllCancelled = false;
					break;
				}
			} else {
				let z = 0;
				while(z < PlArr[y].orders.length){
					if(PlArr[y].orders[z].orderID == Req.OrderId){
						if(PlArr[y].orders[z].WmsOrderStatus != 'CANCELED' && PlArr[y].orders[z].WmsOrderStatus != 'CANCELLED'){
							AllCancelled = false;
							break;
						}
					}
					z++;
				}

				if(!AllCancelled){
					break;
				}
			}
			y++;
		}

		if(AllCancelled){
			y = 0;
			let ManifestRequired = false;
			while(y < PlArr.length){
				if(PlArr[y].toteID){
					ManifestRequired = true;
					break;
				}
				y++;
			}

			Req.ManifestRequired = ManifestRequired;
			Req.ManifestPrinted = false;
			Req.Status = 'CANCELED';
			Req.Required = true;
			Req.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			Req.save(function(err, savedDoc){
				x++;
				return ProcessEachWcsRequest(ReqArr, x, callback);
			});

			return;
		}

		CancelAnyShipped(PlArr, 0, function(){
			x++;
			return ProcessEachWcsRequest(ReqArr, x, callback);
		});
	});
} /* ProcessEachWcsRequest */

function CheckForWmsRequestUpdates(callback){
	CanReq.find({'Required': true}, function(err, ReqArr){
		if(err){
			WriteToFile(err);
			return callback();
		}

		if(!ReqArr || ReqArr.length <= 0){
			return callback();
		}

		WriteToFile(ReqArr.length + ' request updates found to send to WMS');
		ProcessEachReqUpdate(ReqArr, 0, function(){
			WriteToFile('Request updates to WMS complete' );
			return callback();
		});
	});
} /* CheckForWmsRequestUpdates */

function ProcessEachReqUpdate(ReqArr, x, callback){
	if(x >= ReqArr.length){
		return callback();
	}

	let Req = ReqArr[x];

	UpdateWmsRequestStatus(Req.Status, Req.OrderId, function(){
		Req.Required = false;
		Req.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
		Req.save(function(err, savedDoc){
			x++;
			return ProcessEachReqUpdate(ReqArr, x, callback);
		});
	});
} /* ProcessEachReqUpdate */

function CheckWmsCancelRequest(callback){
	apexLib.msSQL.query.execute(global.msSQLClient, "Select * from apx_order_cancel_request where WmsStatus='WCS PENDING' and WcsStatus='REQUEST'", null, function (err, Records) {
		if (err) {
			WriteToFile(err);
			return callback();
		}

		if(!Records || Records.length <= 0){
			return callback();
		}

		WriteToFile(Records.length + ' new cancellation requests updates from WMS to WCS found');
		ProcessEachRecordFound(Records, 0, function(){
			WriteToFile('Cancellation request to WCS complete' );
			return callback();
		});
	});
} /* CheckWmsCancelRequest */

function ProcessEachRecordFound(Records, x, callback){
	if(x >= Records.length){
		return callback();
	}

	let Record = Records[x];

	CanReq.findOne({'OrderId': Record.OrderID}, function(err, Req){
		if(err){
			WriteToFile(err);
			x++;
			return ProcessEachRecordFound(Records, x, callback);
		}

		if(Req){
			UpdateWmsRequestStatus('PENDING', Record.OrderID, function(){
				x++;
				return ProcessEachRecordFound(Records, x, callback);
			});

			return;
		}

		pickList.findOne({'orders.orderID': Record.OrderID}, function(err, PL){
			if(!PL){
				x++;
				return ProcessEachRecordFound(Records, x, callback);
			}

			let NewRec = new CanReq({
				OrderId: Record.OrderID,
				Status: 'PENDING',
				UserID: Record.EDITUSER,
				Required: false,
				CreateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
			});

			NewRec.save(function(err, savedDoc){
				console.log('New Cancellation Request Created For Order: ' + Record.OrderID + ' in WCS');
				UpdateWmsRequestStatus('PENDING', Record.OrderID, function(){
					x++;
					return ProcessEachRecordFound(Records, x, callback);
				});
			});
		});
	});
} /* ProcessEachRecordFound */

function UpdateWmsRequestStatus(Status, OrderID, callback){
	apexLib.msSQL.query.execute(global.msSQLClient, "UPDATE apx_order_cancel_request SET WcsStatus='" + Status + "' where OrderID='" + OrderID + "'", null, function (err, recordset) {
		if (err) {
			WriteToFile(err);
		}

		return callback();
	});
} /* UpdateWmsRequestStatus */