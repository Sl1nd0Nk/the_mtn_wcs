var express = require('express')
var router = express.Router()
var path = require('path')

// GET index page.
router.get('/', function (req, res, next) {
    var join = path.join(__dirname, 'index.html')
    res.sendFile(join)
})

module.exports = router
