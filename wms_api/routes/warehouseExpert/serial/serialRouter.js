var express    = require('express');
var apexLib    = require('apex_lib/moduleIndex.js');
var router     = express.Router();
var logging    = require('../../../lib/classLogging');
var config     = require('wms_api');
var ProcName   = config.SERVICES.SERIAL.NAME;
var log        = new logging();

var messageArray = [];

router.get('/warehouseExpert/serial', function (req, res) {
    try {
        var serial = req.query.serial;
        log.WriteToFile(ProcName, 'Querying Serial: ' + serial);

        /* Parameter to send to sql server. */
        var storedProcParams = {
            serial: serial
        };

        /* Call stored proc to find the specified serial number. */
        apexLib.msSQL.storedProc.execute(global.msSQLClient, 'apx_serial_get', null, storedProcParams, null, function (error, recordSet, returnValue) {
            if (error) {
				/* Error occurred during the stored proc process. */
                res.status(406).send(responseMessageCreator(0, 60002, 'Stored Procedure Unable to Complete:\n' + 'Error:  ' + error));
            } else {
                if (recordSet[0].length === 1) {
					/* Return serial record set. */
                    return res.status(200).send(recordSet[0][0]);
                } else {
					/* Returns null indicating serial does not exist. */
                    return res.status(200).send(null);
                }
            }
        });
    } catch (error) {
        return res.status(406).send(responseMessageCreator(0, 60001, 'An unhandled error has occurred ' + error));
    }
});

function responseMessageCreator (success, reasonCode, reasonText) {
    return '<RESPONSE><SUCCESS>' + success + '</SUCCESS>' +
  '<REASONCODE>' + reasonCode + '</REASONCODE>' +
  '<REASONTEXT>' + reasonText + '</REASONTEXT></RESPONSE>';
} /* responseMessageCreator */

router.post('/warehouseExpert/serial/update', function (req, res){
    var jsonData = req.body.data;

    log.WriteToFile(ProcName, 'Received Serial Update data\n: ' + jsonData);

    var orderID = jsonData.orderID;
    var cartonID = jsonData.cartonID;
    var noOfCartons = jsonData.noOfCartons;
    var cartonNumber = jsonData.cartonNo;

    /* If these fields aren't populated. */
    if (!orderID || !cartonID || !noOfCartons || !cartonNumber) {
        return res.status(400).send('Please ensure that all fields are populated.');
    }

    /* If no items were sent through. */
    if (jsonData.items.length === 0) {
        return res.status(400).send('Please ensure that order lines are provided.');
    }

    /* Iterate through the order lines.b */
    for (let i = 0; i < jsonData.items.length; i++) {
        var sku = jsonData.items[i].sku;
        var qty = jsonData.items[i].qty;
        var orderLine = jsonData.items[i].orderLine;

        /* If these fields aren't populated. */
        if (!sku || !qty || !orderLine) {
            return res.status(400).send('Please ensure that all fields are populated within the order line.');
        }

        /* If the orderLine property doesn't contain any serials. */
        if (jsonData.items[i].serials.length === 0) {
            return res.status(400).send('Please ensure that each orderLine contains serials.');
        }
    }

    var totalSerials = 0;
    var totalSerialsUpdated = 0;

    /* Get the amount of serials within this carton. */
    for (let i = 0; i < jsonData.items.length; i++) {
        totalSerials += jsonData.items[i].serials.length;
    }

    ProcessItems(orderID, cartonID, cartonNumber, noOfCartons, jsonData.items, 0, function(error){
        if (!error) {
            return res.status(200).send('Serials Updated.');
        } else {
            return res.status(400).send(error);
        }
    });
});

/* Iterates through items and updates each serial number. */
function ProcessItems(orderID, cartonID, cartonNumber, noOfCartons, items, itemIndex, callback){
    if(itemIndex < items.length){
        var sku = items[itemIndex].sku;
        var orderLine = items[itemIndex].orderLine;

        ProcessSerials(orderID, cartonID, cartonNumber, noOfCartons, sku, orderLine, items[itemIndex].serials, 0, function(error){
            /*if (error){
                return callback(error);
            } else {*/
                itemIndex++;
                ProcessItems(orderID, cartonID, cartonNumber, noOfCartons, items, itemIndex, callback);
            //}
        });
    } else {
        return callback(null);
    }
} /* ProcessItems */

function ProcessSerials(orderID, cartonID, cartonNumber, noOfCartons, sku, orderLine, serials, serialIndex, callback){
    if(serialIndex < serials.length){
        var serial = serials[serialIndex];
        updateSerial1(orderID, orderLine, cartonID, cartonNumber, noOfCartons, sku, serial, function (error) {
            /*if (error) {
                return callback(error);
            }*/

            serialIndex++;
            ProcessSerials(orderID, cartonID, cartonNumber, noOfCartons, sku, orderLine, serials, serialIndex, callback);
        });
    } else {
        return callback(null);
    }
} /* ProcessSerials */

function updateSerial1 (salesOrderID, orderLineID, cartonID, cartonNo, noOfCartons, sku, serial, callback) {
    var storedProcParams = {
        salesOrderID: salesOrderID,
        salesOrderLineID: orderLineID,
        cartonID: cartonID,
        cartonNo: cartonNo,
        noOfCartons: noOfCartons,
        SKU: sku,
        serial: serial
    };

    apexLib.msSQL.storedProc.executeWithTransaction(global.msSQLClient, 'apx_serial_so_associate', null, storedProcParams, null, null, function (error, recordSet, returnValue,inputParams, transactionObject) {
        if (!error) {
            if (recordSet.length > 0) {
                if (recordSet[0][0].ErrorNumber > 0) {
                    log.WriteToFile(ProcName, 'Serial Update Failed: ' + recordSet[0][0].ErrorMessage + 'for serial:' + serial);
                    return callback(null);
                } else {
                    log.WriteToFile(ProcName, 'inserted serial number ' + serial);
                    return callback(null);
                }
            } else {
                return callback(null);
            }
        } else {
            log.WriteToFile(ProcName, error.message);
            return callback(error);
        }
    });
} /* updateSerial1 */

/* Calls a stored procedure (apx_serial_so_associate) to update a serial. */
function updateSerial (salesOrderID, orderLineID, cartonID, cartonNo, noOfCartons, sku, serial, transactionObject, callback) {
    var storedProcParams = {
        salesOrderID: salesOrderID,
        salesOrderLineID: orderLineID,
        cartonID: cartonID,
        cartonNo: cartonNo,
        noOfCartons: noOfCartons,
        SKU: sku,
        serial: serial
    };

    apexLib.msSQL.storedProc.executeWithTransaction(global.msSQLClient, 'apx_serial_so_associate', null, storedProcParams, null, transactionObject, function (error, recordSet, returnValue,inputParams, transactionObject) {
        if (!error) {
            if (recordSet.length > 0) {
                if (recordSet[0][0].ErrorNumber > 0) {
                    log.WriteToFile(ProcName, 'Serial Update Failed: ' + recordSet[0][0].ErrorMessage + 'for serial:' + transactionObject.serial);
                    return callback(recordSet[0][0].ErrorMessage, recordSet[0][0].ErrorNumber, transactionObject);
                } else {
                    return callback(null, 1, transactionObject);
                }
            }
        } else {
            log.WriteToFile(ProcName, 'Serial Update Failed: ' + error.message);
            return callback(error, 0, transactionObject);
        }
    });
} /* updateSerial */

module.exports = router;
