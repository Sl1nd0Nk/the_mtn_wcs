var express = require('express');
var apexLib = require('apex_lib/moduleIndex.js');
var router = express.Router();

router.post('/warehouseExpert/salesOrder/shipment', function (req, res){
    try {
        var orderID = req.body.orderID;

        apexLib.msSQL.storedProc.executeWithTransaction(global.msSQLClient,
                    'apx_so_shipment_require', null, {'orderID': orderID}, null, null, function (error, recordSet, returnValue){
                if (error){
                    res.status(400).send('Sales Order Require Failed: ' + error);
                } else {
                    res.status(200).send('Order ' + orderID + ' required success');
                }
        });
    } catch (error){
        return res.status(400).send(error);
    }
});

module.exports = router;
