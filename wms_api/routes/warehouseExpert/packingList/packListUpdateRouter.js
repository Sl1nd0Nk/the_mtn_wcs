var express = require('express');
var apexLib = require('apex_lib/moduleIndex.js');
var router = express.Router();
var config     = require('wms_api');
var logging    = require('../../../lib/classLogging');

var ProcName   = config.SERVICES.PACKLIST.NAME;
var log        = new logging();

/* Update a pack list with WCS courier area information. */
router.post('/warehouseExpert/packList/weight1', function (req, res) {
    try {
        var cartonID = req.body.cartonID; /* The carton ID. */
        var transporterID = req.body.transporterID; /* The transporter ID of this carton. It can be NULL. */
        var actualWeight1 = req.body.actualWeight1; /* Weight Scale 1 value from WCS. */
        var theoreticalWeight = req.body.theoreticalWeight; /* The theoretical weight of a carton. */
        var salesOrderID = req.body.salesOrderID;
        var TransTheoryWeight = req.body.transporterTheoryWeight; /* The theoretical weight of the transporter should it exist */

        var inputParams = {
            'packListID': cartonID,
            'transporterID': transporterID,
            'actualWeight1': actualWeight1,
            'theoreticalWeight': theoreticalWeight,
            'salesOrderID': salesOrderID,
            'theoTransporterWeight': TransTheoryWeight
        };

        log.WriteToFile(ProcName, 'PackList weight 1 data: ' + JSON.stringify(inputParams));

        /* Execute the pick list weight update stored procedure. */
        apexLib.msSQL.storedProc.execute(global.msSQLClient, 'apx_pack_list_header_weight1_update', null, inputParams, null, function (error, recordSet, returnValue) {
            if (!error) {
                if (recordSet[0][0].ErrorNumber > 0) {
                    log.WriteToFile(ProcName, 'Pack List Weight 1 Update Failed for CartonID: ' + cartonID + ' ' + error);
                    return res.status(400).send('Fail');
                } else {
                    log.WriteToFile(ProcName, 'Pack List Weight 1 Update Succeeded for CartonID: ' + cartonID);
                    return res.status(200).send('Success');
                }
            }
        });
    } catch (error) {
    }
});

/* Update a pack list with WCS courier area information. */
router.post('/warehouseExpert/packList/weight2', function (req, res) {
    try {
        var cartonID = req.body.cartonID; // The carton ID.
        var actualWeight2 = req.body.actualWeight2; // Weight Scale 1 value from WCS.

        var inputParams = {
            'packListID': cartonID,
            'actualWeight2': actualWeight2,
        }

        log.WriteToFile(ProcName, 'PackList weight 2 data: ' + JSON.stringify(inputParams));

        /* Execute the pick list weight update stored procedure. */
        apexLib.msSQL.storedProc.execute(global.msSQLClient, 'apx_pack_list_header_weight2_update', null, inputParams, null, function (error, recordSet, returnValue) {
            if (!error) {
                if (recordSet[0][0].ErrorNumber > 0) {
                    log.WriteToFile(ProcName, 'Pack List Weight 2 Update Failed for CartonID: ' + cartonID + ' ' + error);
                    return res.status(400).send('Fail');
                } else {
                    log.WriteToFile(ProcName, 'Pack List Weight 2 Update Succeeded for CartonID: ' + cartonID);
                    return res.status(200).send('Success');
                }
            }
        });
    } catch (error) {
    }
});

module.exports = router;

