var express     = require('express');
var parseString = require('xml2js').parseString;
var config      = require('wms_api');
var logging     = require('classLogging');
var router      = express.Router();
var moment      = require('moment');
var async       = require('async');
var pickList    = require('pickListModel');
var cartseq     = require('CartonSequenceModel');

var ProcName   = config.SERVICES.ORDER_HEADER.NAME;
var log        = new logging();

var q  = async.queue(ProcNewCarton, 1);

/* Post sales order data in raw xml format. */
router.post('/warehouseExpert/carton/cartonsequence', function (req, res) {
	q.push({req: req, res: res}, function (err) {
		log.WriteToFile(ProcName, 'finished processing Carton Seq');
	});
});

q.drain = function() {
    log.WriteToFile(ProcName, 'all Carton Seq have been processed');
}

function ProcNewCarton(task, callback){
	let req = task.req;
	let res = task.res;

	let body = req.body;
	let cartonID = body.cartonID;
	let orderID = body.orderID;
	let picklistID = body.picklistID;

	console.log('cartonID: ' + cartonID);
	console.log('orderID: ' + orderID);
	console.log('picklistID: ' + picklistID);

	cartseq.find({'OrderId': orderID}, function(err, CartonsArr){
		if(err){
			console.log('Err: ' + err);
			res.status(406).send({Err: err});
			return callback();
		}

		if(!CartonsArr || CartonsArr.length <= 0){
			console.log('No Carton Data. Adding it in');
			AddThisCarton(cartonID, orderID, picklistID, 1, null, function(Resp){
				res.status(200).send(Resp);
				return callback();
			});

			return;
		}

		let x = 0;
		while(x < CartonsArr.length){
			if(CartonsArr[x].CartonId == cartonID && CartonsArr[x].OrderId == orderID){
				break;
			}
			x++;
		}

		if(x < CartonsArr.length){
			res.status(200).send({CartonNo: CartonsArr[x].CartonNo, NoOfCartons: CartonsArr[x].NoOfCartons});
			return callback();
		}

		x = 0;
		let CartonNo = 0;
		let NoOfCartons = null;
		let pickListFound = false;
		while(x < CartonsArr.length){
			if(!NoOfCartons){
				NoOfCartons = CartonsArr[x].NoOfCartons;
			}

			if(CartonsArr[x].PicklistId == picklistID){
				CartonNo = CartonsArr[x].CartonNo;
				pickListFound = true;
				break;
			}

			if(CartonsArr[x].CartonNo > CartonNo){
				CartonNo = CartonsArr[x].CartonNo;
			}

			x++;
		}

		if(!pickListFound){
			CartonNo += 1;
		}

		AddThisCarton(cartonID, orderID, picklistID, CartonNo, NoOfCartons, function(Resp){
			res.status(200).send(Resp);
			return callback();
		});
	});
} /* ProcNewCarton */

function AddThisCarton(cartonID, orderID, picklistID, CartonNo, NoOfCartons, callback){
	if(!NoOfCartons){
		pickList.find({'orders.orderID': orderID}, function (err, docArr){
			if(err){
				return callback({Err: err});
			}

			if(!docArr || docArr.length <= 0){
				return callback({Err: 'No picklists found'});
			}

			let x = 0;
			let TotalCount = 0;
			while(x < docArr.length){
				let y = 0;
				while(y < docArr[x].orders.length){
					if(docArr[x].orders[y].orderID == orderID && docArr[x].Status != 'CANCELLED' && docArr[x].Status != 'CANCELED'){
						TotalCount++;
					}
					y++;
				}
				x++;
			}

			NoOfCartons = TotalCount;

			let NewRec = new cartseq({
				OrderId: orderID,
				PicklistId: picklistID,
				CartonId: cartonID,
				CartonNo: CartonNo,
				NoOfCartons: NoOfCartons
			});

			NewRec.save(function(err, savedDoc){
				return callback({CartonNo: CartonNo, NoOfCartons: NoOfCartons});
			});
		});

		return;
	}

	let NewRec = new cartseq({
		OrderId: orderID,
		PicklistId: picklistID,
		CartonId: cartonID,
		CartonNo: CartonNo,
		NoOfCartons: NoOfCartons
	});

	NewRec.save(function(err, savedDoc){
		return callback({CartonNo: CartonNo, NoOfCartons: NoOfCartons});
	});
} /* AddThisCarton */

module.exports = router;
