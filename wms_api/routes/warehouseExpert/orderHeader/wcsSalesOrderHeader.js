var express     = require('express');
/* Used to parse the xml body to a javascript object. */
var parseString = require('xml2js').parseString;
/* Path to the apex library. */
var apexLib     = require('apex_lib/moduleIndex.js');
var config      = require('wms_api');
var logging     = require('classLogging');
var router      = express.Router();
var moment      = require('moment');

var ProcName   = config.SERVICES.ORDER_HEADER.NAME;
var log        = new logging();

/* End-point used to get a sales order header. */
router.post('/warehouseExpert/salesOrder/header', function (req, res){
    try {
        var orderID = req.body.orderID;
        log.WriteToFile(ProcName, 'Incoming Order Header Request: ' + orderID);

        /* Retrieve the so header. */
        apexLib.msSQL.storedProc.execute(global.msSQLClient, 'apx_so_header_retrieve', null, {'orderID': orderID}, null, function (error, recordSet, returnValue) {
            if (error) {
				log.WriteToFile(ProcName, orderID + ' - Error: ' + error);
                return res.status(406).send('An Error Occurred' + error.message);
            } else {
				/* Make sure that data was retrieved. */
				if (recordSet[0].length > 0){
					/* Get the invoice number for this sales order. */
					getInvoiceNumber(orderID, recordSet[0][0].ORDERTYPE, function (error, result) {
						if (error){
							log.WriteToFile(ProcName, orderID + ' - Error: ' + error);
							res.status(400).send(error);
						} else {
							var invoiceNo = result;

							var SOHeaderJson = {
								'invoiceNumber': invoiceNo,
								'customerOrderNo': recordSet[0][0].HOSTORDERID,
								'orderType': recordSet[0][0].ORDERTYPE,
								'customerName': recordSet[0][0].CUSTOMERNAME,
								'customerAccount': recordSet[0][0].CUSTOMERACCNO,
								'specialInstructions': recordSet[0][0].NOTES,
								'custVatNo': recordSet[0][0].VATNUMBER,
								'storeName': recordSet[0][0].ShortName,
								'storeMessage': recordSet[0][0].CustomMessage,
								'billTo': {
									'street1': recordSet[0][0].BILLTOSTREET1,
									'street2': recordSet[0][0].BILLTOSTREET2,
									'street3': recordSet[0][0].BILLTOSTREET3,
									'street4': recordSet[0][0].BILLTOSTREET4,
									'city': recordSet[0][0].BILLTOCITY,
									'zip': recordSet[0][0].BILLTOZIP,
									'contact1Name': recordSet[0][0].BILLTOCONTACT1NAME,
									'contact2Name': recordSet[0][0].BILLTOCONTACT2NAME,
									'contact1Phone': recordSet[0][0].BILLTOCONTACT1PHONE,
									'contact2Phone': recordSet[0][0].BILLTOCONTACT2PHONE,
									'contact1Email': recordSet[0][0].BILLTOCONTACT1EMAIL
								},
								'shipTo': {
									'street1': recordSet[0][0].SHIPTOSTREET1,
									'street2': recordSet[0][0].SHIPTOSTREET2,
									'street3': recordSet[0][0].SHIPTOSTREET3,
									'street4': recordSet[0][0].SHIPTOSTREET4,
									'city': recordSet[0][0].SHIPTOCITY,
									'zip': recordSet[0][0].SHIPTOZIP,
									'contact1Name': recordSet[0][0].SHIPTOCONTACT1NAME,
									'contact2Name': recordSet[0][0].SHIPTOCONTACT2NAME,
									'contact1Phone': recordSet[0][0].SHIPTOCONTACT1PHONE,
									'contact2Phone':  recordSet[0][0].SHIPTOCONTACT2PHONE,
									'contact1Email': recordSet[0][0].SHIPTOCONTACT1EMAIL
								},
								'referenceOrd': recordSet[0][0].REFERENCEORD,
								'hardCopyInvoice': recordSet[0][0].HARDCOPYINVOICE,
								'terms': recordSet[0][0].TERMS,
								'hardCopyManifest': recordSet[0][0].HARDCOPYMANIFEST
							};

							log.WriteToFile(ProcName, orderID + ' - Return Data: ' + JSON.stringify(SOHeaderJson));
							return res.status(200).send(JSON.stringify(SOHeaderJson));
						}
					})
				} else {
					log.WriteToFile(ProcName, orderID + ' - Error: NO DATA FOUND');
					return res.status(406).send(orderID + ' - Error: NO DATA FOUND');
				}
			}
        })
    } catch (error) {
		log.WriteToFile(ProcName, orderID + ' - Error: ' + error);
		return res.status(406).send('An Error Occurred' + error.message);
    }
});

/* Get an invoice number for the specific sales order. */
function getInvoiceNumber (orderID, orderType, callback) {
    var storedProcName = null;
    if (orderType === 'DEANWHWMS' || orderType === 'INTTRFNWH' || orderType === 'RTV') {
        storedProcName = 'apx_invoice_erp_generate';
    } else if (orderType === 'EDINWHWMS'){
        storedProcName = 'apx_invoice_retail_generate';
    } else {
        storedProcName = 'apx_invoice_other_generate';
    }

    /* Get the sales order invoice. */
    apexLib.msSQL.storedProc.execute(global.msSQLClient, storedProcName, null, {'orderID': orderID}, null, function (error, recordSet, returnValue){
        if(error){
            return callback(error, null);
        } else {
            return callback(null, recordSet[0][0].INVOICENO);
        }
    });
} /* getInvoiceNumber */

module.exports = router;
