/*require('appdynamics').profile({
    controllerHostName: '10.20.88.95',
    controllerPort: 8090,
    accountName: 'customer1',
    accountAccessKey: '91f9b93a-0a53-4caa-8bde-f4f8328773dc',
    applicationName: 'Midrand Prod WCS',
    tierName: 'NodeJS',
    nodeName: 'Pick List Generator',
    debug: true,
logging:{
   'logfiles': [{
   'root_directory':'/tmp/appd',
   'filename':'echo_%N.log',
   'level': 'DEBUG',
   'max_size': 10000,
   'max_files': 1
}]
}
});*/

var express      = require('express');
var router       = express.Router();
var moment       = require('moment');
var apexLib      = require('apex_lib/moduleIndex.js');
var mongoose     = require('mongoose');
var pickList     = require('pickListModel');
var config       = require('wms_api');
var PackedSerial = require('PackedSerialUpdatesModel');
var ShipUpdate   = require('ShipUpdatesModel');
var serialLib    = require('classSerial');
var CanReq       = require('CancelRequestModel');

var serls        = new serialLib();

var ThisWarehouse = config.THISWAREHOUE;
WriteToFile('This Warehouse [' + ThisWarehouse + ']');

apexLib.msSQL.client(config.MSSQL.User, config.MSSQL.Pwd, config.MSSQL.Port, config.MSSQL.Url, config.MSSQL.Database, function (err, client) {
    if (err) {
        WriteToFile('Unable to connect to SQL Server');
    } else {
		WriteToFile('Connect To MsSql DB');
        global.msSQLClient = client;

		mongoose.connect(config.mongoDB.url, function (error, client) {
			if (error) {
				WriteToFile('MongoDB: Connection Error: ' + error);
			} else {
				WriteToFile('MongoDB: Connection Established.');
				WaitForTrigger();
			}
		}); /* Mongo Connection */
    }
});

function WriteToFile(Msg){
	if(Msg){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' ' + Msg);
	}
} /* WriteToFile */

function WaitForTrigger(){
	setTimeout(function(){
		onHeartbeatCallback();
	}, 30000);
} /* WaitForTrigger */

function FindPickListInMongoDB(pickListID, callback){
	pickList.find({'orders.pickListID': pickListID}, function(error, PLST){
		if(error){
			return callback({Error: error});
		} else {
			if(PLST.length > 0){
				return callback({PickList: PLST[0]});
		  	} else {
				return callback({PickList: null});
			}
		}
	});
} /* FindPickListInMongoDB */

function CheckIfPickListIsValidToSend(OrderID, recordSet2, callback){
	pickList.find({'orders.orderID': OrderID}, function(error, ORDER){
		if(error){
			WriteToFile("Error while finding order " + OrderID + " in MongoDB | Error: " + error);
			return callback({Good: false});
		}

		/*var Ordindex = 0;
		var ExtraPickListFound = false;
		while(Ordindex < ORDER.length){
			if(ORDER[Ordindex].Status == 'PICKED' || ORDER[Ordindex].Status == 'PACKED' || ORDER[Ordindex].Status == 'SHIPPED'){
				ExtraPickListFound = true;
				break;
			}
			Ordindex++;
		}*/

		if(ORDER.length <= 0){
			return callback({Good: true, toteNumber: (1)});
		}

		let Ordindex = 0;
		let OrderLineData = [];
		while(Ordindex < ORDER.length){
			let p = 0;
			while(p < ORDER[Ordindex].orders.length){
				if(ORDER[Ordindex].orders[p].orderID == OrderID){
					let q = 0;
					while(q < ORDER[Ordindex].orders[p].items.length){
						let r = 0;
						while(r < OrderLineData.length){
							if(OrderLineData[r].OrderLine == ORDER[Ordindex].orders[p].items[q].orderLine){
								OrderLineData[r].OrderLineQty += ORDER[Ordindex].orders[p].items[q].qty;
								break;
							}
							r++;
						}

						if(r >= OrderLineData.length){
							OrderLineData.push({OrderLine: ORDER[Ordindex].orders[p].items[q].orderLine, OrderLineQty: ORDER[Ordindex].orders[p].items[q].qty});
						}
						q++;
					}
				}
				p++;
			}
			Ordindex++;
		}

		if(OrderLineData.length <= 0){
			return callback({Good: true, toteNumber: (Ordindex+1)});
		}

		let s = 0;
		let ProblemFound = false;
		let PickListOrderLineData = [];
		while(s < recordSet2.length){
			let u = 0;
			while(u < PickListOrderLineData.length){
				if(PickListOrderLineData[u].OrderLine == recordSet2[s].ORDERLINE){
					PickListOrderLineData[u].OrderLineQty += recordSet2[s].QTY;
					break;
				}
				u++;
			}

			if(u >= PickListOrderLineData.length){
				PickListOrderLineData.push({OrderLine: recordSet2[s].ORDERLINE, OrderLineQty: recordSet2[s].QTY, AllocatedQty: recordSet2[s].QTYALLOCATED, PickList: recordSet2[s].PICKLIST, OrderId: recordSet2[s].ORDERID});
			}
			s++;
		}

		s = 0;
		while(s < PickListOrderLineData.length){
			let t = 0;
			while(t < OrderLineData.length){
				if(PickListOrderLineData[s].OrderLine == OrderLineData[t].OrderLine){
					if((PickListOrderLineData[s].OrderLineQty + OrderLineData[t].OrderLineQty) > PickListOrderLineData[s].AllocatedQty){
						WriteToFile("PickList " + PickListOrderLineData[s].PickList + " for order " + PickListOrderLineData[s].OrderId + " orderLine " + PickListOrderLineData[s].OrderLine + ": Qty Mismatch between orderline and picklists.");
						ProblemFound = true;
						break;
					}
				}
				t++;
			}

			if(ProblemFound){
				break;
			}
			s++;
		}

		if(ProblemFound){
			return callback({Good: false});
		}

		return callback({Good: true, toteNumber: (Ordindex+1)});
	});
} /* CheckIfPickListIsValidToSend */

function StageRetrievedPickLists(pickListArr, index, callback){
	if(index >= pickListArr.length){
		return callback();
	}

	let inputParams = {
		'pickList': pickListArr[index].PICKLIST,
		'interface_status': 1,
		'warehouse': pickListArr[index].WAREHOUSE,
		'orderID': pickListArr[index].ORDERID
	}

	apexLib.msSQL.storedProc.execute(global.msSQLClient, 'apx_picklist_staging_insert', null, inputParams, null, function(error, recordSetInsert, returnValue1){
		if(error){
			WriteToFile("Error while trying to stage picklist " + pickListArr[index].PICKLIST + " | Err: " + error);
		} else {
			WriteToFile("Staged PickList " + pickListArr[index].PICKLIST + " in WCSpickList table");
		}

		index++;
		StageRetrievedPickLists(pickListArr, index, callback);
	});
} /* StageRetrievedPickLists */

function ValidatePicklistQtyPerOrderLine(Recs){
	let p = 0;
	let DiscrepFound = false;
	while(p < Recs.length){
		let OrderLine = Recs[p].ORDERLINE;
		let OrderLineQty = Recs[p].QTYALLOCATED;
		let PLineQty = Recs[p].QTY;
		let r = p+1;
		while(r < Recs.length){
			if(Recs[r].ORDERLINE == OrderLine){
				if((Recs[r].QTY + PLineQty) > OrderLineQty){
					DiscrepFound = true;
					break;
				}
			}
			r++;
		}

		if(DiscrepFound){
			return false;
		}

		if(PLineQty > OrderLineQty){
			return false;
		}

		p++;
	}

	return true;
} /* ValidatePicklistQtyPerOrderLine */

function SendValidPickListToWCS(pickListArr, index, callback){
	if(index >= pickListArr.length){
		return callback();
	}

	var inputParams2 ={
		'pickList': pickListArr[index].PICKLIST
	}

	apexLib.msSQL.storedProc.execute(global.msSQLClient, 'apx_picklists_retrieve_test', null, inputParams2, null, function (error2, recordSet2, returnValue2){
		var Warehouse = null;

		if(recordSet2[0].length <= 0){
			WriteToFile('pickList Header ' + pickListArr[index].PICKLIST + ' details not found or not in correct status');
			index++;
			return SendValidPickListToWCS(pickListArr, index, callback);
		}

		Warehouse = recordSet2[0][0].WAREHOUSE;
		if(Warehouse == null){
			Warehouse = '311WAREHOUSE';
		}

		/* include allocatedQty to stored Proc */
		let QtysValid = ValidatePicklistQtyPerOrderLine(recordSet2[0]);

		if(!QtysValid){
			WriteToFile('pickList ' + pickListArr[index].PICKLIST + ' -> Qty mismatch between orderline and picklist');
			index++;
			return SendValidPickListToWCS(pickListArr, index, callback);
		}

		if(Warehouse.toUpperCase() != ThisWarehouse.toUpperCase()){
			WriteToFile('pickList Header ' + pickListArr[index].PICKLIST + ' orderID not for this warehouse [' + ThisWarehouse + ']');
			index++;
			return SendValidPickListToWCS(pickListArr, index, callback);
		}

		FindPickListInMongoDB(recordSet2[0][0].PICKLIST, function(FnData){
			if(FnData.Error){
				WriteToFile("Failed to find picklist in MongoDB - Error: " + FnData.Error);
				index++;
				return SendValidPickListToWCS(pickListArr, index, callback);
			}

			if(FnData.PickList != null){
				WriteToFile('PickList ' + recordSet2[0][0].PICKLIST + ' already in WCS');
				index++;
				return SendValidPickListToWCS(pickListArr, index, callback);
			}

			CheckIfPickListIsValidToSend(recordSet2[0][0].ORDERID, recordSet2[0], function(Valid){
				if(Valid.Good == false){
					index++;
					return SendValidPickListToWCS(pickListArr, index, callback);
				}

				CanReq.findOne({'OrderId': recordSet2[0][0].ORDERID}, function(err, Req){
					let NeedToCancel = false;
					if(err){
						WriteToFile(err);
					}

					if(Req){
						NeedToCancel = true;
					}

					let pickListItems = [];
					for(let j = 0; j < recordSet2[0].length; j++){
						let serialised = false;

						if(recordSet2[0][j].CLASSNAME === 'SERIAL'){
						  serialised = true;
						}

						let zone = parsePickZone(recordSet2[0][j].FROMLOCATION);

						pickListItems.push({
							itemCode: recordSet2[0][j].SKUSHORTDESC,
							itemDescription: recordSet2[0][j].SKUSHORTDESC,
							eachWeight: recordSet2[0][j].GROSSWEIGHT,
							location: recordSet2[0][j].FROMLOCATION,
							qty: recordSet2[0][j].QTY,
							serialised: serialised,
							pickZone: zone,
							sku: recordSet2[0][j].SKU,
							barcode: recordSet2[0][j].EANUPC,
							uom: recordSet2[0][j].UOM,
							unitPrice: recordSet2[0][j].UNITPRICE,
							orderLine: recordSet2[0][j].ORDERLINE,
							pickListOrderLine: recordSet2[0][j].PICKLISTORDERLINE,
							eachVolume: recordSet2[0][j].VOLUME
						});
					}

					let PickRegion = recordSet2[0][0].PICKREGION;
					if(PickRegion == '311VSU'){
						PickRegion = '311PTL';
					}

					let newPicklist = new pickList({
						packType: 'MAN',
						destination: recordSet2[0][0].FROMLOCATION,
						orders:{
							orderID: recordSet2[0][0].ORDERID,
							toteNumber: null,
							numberOfTotes: 1,
							WmsOrderStatus: NeedToCancel == true? 'CANCELED': null,
							WmsPickListStatus: NeedToCancel == true? 'CANCELED': null,
							CancelDate: NeedToCancel == true? moment(new Date()).format('YYYY-MM-DD HH:mm:ss'): null,
							CancelInstruction: NeedToCancel == true? 'Cancelled Before Tote Assignment': null,
							pickListID: recordSet2[0][0].PICKLIST,
							courierID: recordSet2[0][0].ROUTE,
							/*priorityLevel: recordSet2[0][0].ORDERPRIORITY,*/
							orderType: recordSet2[0][0].ORDERTYPE,
							cartonSize: 3,
							items: pickListItems
						},
						Status: NeedToCancel == true? 'CANCELED': 'NEW',
						CreateDate: moment().format('YYYY-MM-DD HH:mm'),
						PickType: recordSet2[0][0].PICKTYPE,
						PickRegion: PickRegion,
						priorityLevel: recordSet2[0][0].ORDERPRIORITY,
						startTime: moment().format('YYYY-MM-DD HH:mm')
					});

					newPicklist.save(function(err, savedDoc){
						if(err){
							WriteToFile("Error while trying to save New picklist with picklistID " + recordSet2[0][0].PICKLIST + " | Error: " + err);
							index++;
							return SendValidPickListToWCS(pickListArr, index, callback);
						}

						if(!savedDoc){
							WriteToFile("Error while trying to save New picklist with picklistID " + recordSet2[0][0].PICKLIST + " | Error: (savedDoc = null)");
							index++;
							return SendValidPickListToWCS(pickListArr, index, callback);
						}

						WriteToFile("New picklist with picklistID: " + recordSet2[0][0].PICKLIST + " | orderID: " + recordSet2[0][0].ORDERID + " added to WCS");

						let inputParams3 = {
							'pickList': savedDoc.orders[0].pickListID,
							'interface_status': 2
						}

						apexLib.msSQL.storedProc.execute(global.msSQLClient, 'apx_picklist_staging_update', null, inputParams3, null, function (error3, recordSet3, returnValue3){
							if(error3 || returnValue3 !== 0){
								WriteToFile('Failed to update staging');
							}

							index++;
							SendValidPickListToWCS(pickListArr, index, callback);
						});
					});
				});
			});
		});
	});
} /* SendValidPickListToWCS */

function onHeartbeatCallback(){
	WriteToFile('Checking for pickLists to Retrieve ');
	var inputParams = {
		'Warehouse': ThisWarehouse
	};

	apexLib.msSQL.storedProc.execute(global.msSQLClient, 'apx_intial_picklists_retrieve', null, inputParams, null, function (error, recordSet, returnValue){
		if(error){
			WriteToFile('apx_picklists_retrieve failed: ' + error.message);
			CheckFullPickPicklistsWCS(function(){
				WaitForTrigger();
			});
		} else {
			if(recordSet[0].length > 0){
				WriteToFile('[' + recordSet[0].length + '] pickLists found for staging');
				StageRetrievedPickLists(recordSet[0], 0, function(){
					SendValidPickListToWCS(recordSet[0], 0, function(){
						CheckFullPickPicklistsWCS(function(){
							//CheckPickListsToCancel(function(){
								CheckFailedShipmentOrders(function(){
									WaitForTrigger();
								});
							//});
						});
					});
				});
			} else {
				WriteToFile('No pickLists to Retrieve ');
				CheckFullPickPicklistsWCS(function(){
					//CheckPickListsToCancel(function(){
						CheckFailedShipmentOrders(function(){
							WaitForTrigger();
						});
					//});
				});
			}
		}
  });
} /* onHeartbeatCallback */

function CheckFailedShipmentOrders(callback){
	WriteToFile('Checking failed Orders to repush in WMS');
	let SelQuery = "Select ORDERID from OUTBOUNDORHEADER where STATUS = 'SHIPPED' AND SHIPMENTERROR is not null AND (SHIPMENTRETRIES < 50 OR SHIPMENTRETRIES is null)";

	apexLib.msSQL.query.execute(global.msSQLClient, SelQuery, null, function (error, recordSet){
		if (error) {
			WriteToFile('CheckFailedShipmentOrders check says: Error while querying OUTBOUNDORHEADER table ' + error.message);
			return callback();
		}

		if(recordSet.length <= 0){
			WriteToFile('No failed Orders found to repush in WMS');
			return callback();
		}

		WriteToFile('['+ recordSet.length + '] Orders found to repush in WMS');
		FindOrdersInMongoForRetrigger(recordSet, 0, function(RetClause){
			return callback();
		});
	});
} /* CheckFailedShipmentOrders */

function ResetWMSOrderHeader(OrderID, callback){
	let Query = "Update OUTBOUNDORHEADER set SHIPMENTERROR = null where ORDERID = '" + OrderID + "'";
	apexLib.msSQL.query.execute(global.msSQLClient, Query, null, function (error1, recordSet1) {
		if (error1) {
			WriteToFile('CheckFailedShipmentOrders check says: Error while updating OUTBOUNDORHEADER table ' + error1.message);
		}

		return callback();
	});
} /* ResetWMSOrderHeader */

function UpdateSerForEachPL(PLArr, ndx, callback){
	if(ndx >= PLArr.length){
		return callback();
	}

	let PL = PLArr[ndx];
	serls.ReUpdateSerialsToHost(PL.orders[0], function(){
		ndx++;
		return UpdateSerForEachPL(PLArr, ndx, callback);
	});
} /* UpdateSerForEachPL */

function FindAndUpdatePickListSerials(OrderID, callback){
	pickList.find({'orders.orderID': OrderID, 'Status': 'SHIPPED'}, function(err, PLArr){
		if(err){
			return callback();
		}

		if(!PLArr || PLArr.length <= 0){
			return callback();
		}

		UpdateSerForEachPL(PLArr, 0, function(){
			return callback();
		});
	});
} /* FindAndUpdatePickListSerials */

function FindOrdersInMongoForRetrigger(recordSet, ndx, callback){
	if(ndx >= recordSet.length){
		return callback(null);
	}

	let OrderID = recordSet[ndx].ORDERID;
	FindAndUpdatePickListSerials(OrderID, function(){
		ShipUpdate.findOne({'OrderId': OrderID}, function(err, OrdRec){
			if(err){
				WriteToFile('Failed to find Order To Retrigger To WMS. Err: ' + err);
				ndx++;
				return FindOrdersInMongoForRetrigger(recordSet, ndx, callback);
			}

			if(!OrdRec){
				//WriteToFile('Failed to find Order To Retrigger To WMS');
				ndx++;
				return FindOrdersInMongoForRetrigger(recordSet, ndx, callback);
			}

			if(OrdRec.Required){
				ndx++;
				return FindOrdersInMongoForRetrigger(recordSet, ndx, callback);
			}

			OrdRec.Required = true;
			OrdRec.Result = null;
			OrdRec.save(function(err, sd){
				ResetWMSOrderHeader(OrderID, function(){
					ndx++;
					return FindOrdersInMongoForRetrigger(recordSet, ndx, callback);
				});
			});
		});
	});
} /* FindOrdersInMongoForRetrigger */

function CheckPickListsToCancel(callback){
	WriteToFile('Checking pickLists to cancel from WMS');
	var SelQuery = "Select * from apx_wcs_to_cancel where WCSSTATUS = 'WCS_PENDING'";

	apexLib.msSQL.query.execute(global.msSQLClient, SelQuery, null, function (error, recordSet){
		if (error) {
			WriteToFile('CheckPickListsToCancel check says: Error while querying apx_wcs_to_cancel table ' + error.message);
			return callback();
		} else {
			if(recordSet.length > 0){
				WriteToFile('[' + recordSet.length + '] pickLists to cancel found in WMS');
				UpdateEachPickListToCancelled(recordSet, 0, function(){
					return callback();
				});
			} else {
				WriteToFile('No pickLists to cancel found in WMS');
				return callback();
			}
		}
	});
} /* CheckPickListsToCancel */

function UpdateEachPickListToCancelled(recordSet, index, callback){
	if(index < recordSet.length){
		var pickListID = recordSet[index].PICKLIST;

		pickList.find({'orders.pickListID': pickListID}, function(error, ORDER){
			if(error){
				WriteToFile("Error while finding Picklist " + pickListID + " in MongoDB | Error: " + error);
				index++;
				UpdateEachPickListToCancelled(recordSet, index, callback);
			} else {
				if(ORDER.length > 0){
					CancelEachPickList(ORDER, 0, pickListID, function(){
						UpdateWMSTableWithStatus('WCS_CANCELLED', pickListID, function(){
							index++;
							UpdateEachPickListToCancelled(recordSet, index, callback);
						});
					});
				} else {
				  UpdateWMSTableWithStatus('WCS_NOT_FOUND', pickListID, function(){
					index++;
					UpdateEachPickListToCancelled(recordSet, index, callback);
				  });
				}
			}
		});
	} else {
		return callback();
	}
} /* UpdateEachPickListToCancelled */

function UpdateWMSTableWithStatus(NewStatus, pickListID, callback){
	var Query = "Update apx_wcs_to_cancel set WCSSTATUS = '" + NewStatus + "' where PICKLIST = '" + pickListID + "'";

	apexLib.msSQL.query.execute(global.msSQLClient, Query, null, function (error1, recordSet1) {
		if (error1) {
			WriteToFile('CheckPickListsToCancel check says: Error while updating apx_wcs_to_cancel table ' + error.message);
		}

		return callback();
	});
} /* UpdateWMSTableWithStatus */

function CancelEachPickList(ORDER, ndx, pickListID, callback){
	if(ndx < ORDER.length){
		var PL = ORDER[ndx];

		if(PL.orders.length > 1){
			var x = 0;
			while(x < PL.orders.length){
				if(PL.orders[x].pickListID == pickListID){
					break;
				}

				x++;
			}

			if(x < PL.orders.length){
				var orders = [];
				orders.push(PL.orders[x]);
				var newPicklist = new pickList({
									toteID: null,
									packType: PL.packType,
									destination: PL.destination,
									orders: orders,
									Status: 'CANCELED',
									CreateDate: PL.CreateDate,
									PickType: PL.PickType,
									PickRegion: PL.PickRegion});

				newPicklist.save(function(err, savedDoc){
					if(!err && savedDoc){
						WriteToFile('Successfully unconsolidated and cancelled picklist ' + pickListID);
					} else {
						WriteToFile('Failed unconsolidated and cancelled picklist ' + pickListID);
					}

					var RemainingOrders = [];
					var y = 0;
					while(y < PL.orders.length){
						if(y != x){
							RemainingOrders.push(PL.orders[y]);
						}
						y++;
					}

					PL.orders = RemainingOrders;

					PL.save(function(err, savedDoc){
						if(!err && savedDoc){
							WriteToFile('Successfully retained consolidated tote with ID ' + savedDoc.toteID);
						} else {
							WriteToFile('Failed retained consolidated tote with ID ' + PL.toteID);
						}

						ndx++;
						CancelEachPickList(ORDER, ndx, pickListID, callback);
					});
				});
			} else {
				ndx++;
				CancelEachPickList(ORDER, ndx, pickListID, callback);
			}
		} else {
			PL.Status = 'CANCELED';
			PL.toteID = null;

			PL.save(function(err, savedDoc){
				if(!err && savedDoc){
					WriteToFile('Successfully cancelled pickList ' + pickListID);
				} else {
					WriteToFile('Failed to cancel pickList ' + pickListID);
				}

				ndx++;
				CancelEachPickList(ORDER, ndx, pickListID, callback);
			});
		}
	} else {
		return callback();
	}
} /* CancelEachPickList */

function CheckFullPickPicklistsWCS(callback){
	WriteToFile('Checking Full pickLists in WCS');
	pickList.find({'PickType': 'FULLPICK', Status: 'NEW'}, function(error, ORDER){
		if(error){
			WriteToFile("Error while finding order any New Full Pallet Picklists in MongoDB | Error: " + error);
			return callback();
		}

		if(ORDER.length <= 0){
			WriteToFile('No Full pickLists Found in WCS');
			return callback();
		}

		WriteToFile('[' + ORDER.length + '] Full pickLists Found in WCS');
		CheckAndUpdatePickListsAccordingly(ORDER, 0, function(){
			return callback();
		});
	});
} /* CheckFullPickPicklistsWCS */

function CancelOrderPicklist(PL, callback){
	PL.Status = 'CANCELED';
	PL.orders[0].WmsOrderStatus = 'CANCELED';
	PL.orders[0].WmsPickListStatus = 'CANCELED';
	PL.orders[0].CancelDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
	PL.orders[0].CancelInstruction = 'Cancelled Before Assignment';

	PL.save(function(err, savedDoc){
		return callback();
	});
} /* CancelOrderPicklist */

function CheckAndUpdatePickListsAccordingly(ORDER, index, callback){
	if(index >= ORDER.length){
		return callback();
	}

	let OrderID = ORDER[index].orders[0].orderID;
	CanReq.findOne({'OrderId': OrderID}, function(err, Req){
		let OkToProceed = true;

		if(err){
			WriteToFile(err);
		}

		if(Req){
			OkToProceed = false;
			CancelOrderPicklist(ORDER[index], function(){
				index++;
				CheckAndUpdatePickListsAccordingly(ORDER, index, callback);
			});
			return;
		}

		if(OkToProceed){
			CheckPickListStatusInWMS(ORDER[index], function(){
				index++;
				CheckAndUpdatePickListsAccordingly(ORDER, index, callback);
			});
		}
	});
} /* CheckAndUpdatePickListsAccordingly */

function CheckPickListStatusInWMS(PL, callback){
	var SelQuery = "Select * from PICKDETAIL where PICKLIST = '" + PL.orders[0].pickListID + "'";

	apexLib.msSQL.query.execute(global.msSQLClient, SelQuery, null, function (error, recordSet) {
		if (error) {
			WriteToFile('FullPicklist check says: Error while querying PICKHEADER table ' + error.message);
			return callback();
		} else {
			if(recordSet.length > 0){
				var FromLoad = recordSet[0].FROMLOAD;

				SelQuery = "Select serial from apx_serial where received=1 and dispatched=0 and loadID = '" + FromLoad + "'";

			    apexLib.msSQL.query.execute(global.msSQLClient, SelQuery, null, function (error1, recordSet1) {
					if (error1) {
						WriteToFile('FullPicklist check says: Error while querying apx_serial table ' + error1.message);
						return callback();
					} else {
						var LoadSerials = [];
						if(recordSet1.length > 0){
							for(var x = 0; x < recordSet1.length; x++){
								LoadSerials.push(recordSet1[x].serial);
							}
						}

						var Changed = false;
						var Status = recordSet[0].STATUS;

						if(Status == 'COMPLETE'){
							PL.toteID = recordSet[0].TOLOAD;
							PL.Status = 'PICKED';
							PL.PickUpdateRequired = true;
							PL.orders[0].picked = true;
							PL.orders[0].items[0].pickQty = recordSet[0].PICKEDQTY;
							PL.orders[0].items[0].picked = true;
							PL.orders[0].items[0].pickedBy = recordSet[0].EDITUSER;
							PL.orders[0].items[0].serials = LoadSerials;
							Changed = true;
						} else if (Status == 'CANCELED'){
							PL.Status = recordSet[0].STATUS;
							Changed = true;
						}

						if(Changed){
							PL.save(function(err, savedDoc){
								if(!err && savedDoc) {
									WriteToFile('Successfully updated picklist ' + savedDoc.orders[0].pickListID + ' for order ' + savedDoc.orders[0].orderID);
								} else {
									WriteToFile('Error while updating picklist ' + PL.orders[0].pickListID + ' for order ' + PL.orders[0].orderID + ' | Error: ' + err);
								}

								return callback();
							});
						} else {
							return callback();
						}
					}
				});
			} else {
				WriteToFile('FullPicklist check says: Error while querying PICKHEADER table ');
				return callback();
			}
		}
	});
} /* CheckPickListStatusInWMS */

function parsePickZone (pickLocation){
	if(pickLocation != null){
		if(pickLocation.substring(0, 1) === 'V'){
			var pickStringType = pickLocation.substr(7, 2);
			WriteToFile('VSU PICK ZONE: ' + pickStringType);
			var pickInteger = parseInt(pickStringType);
		} else if(pickLocation.substring(0, 2) === 'DR' || pickLocation.substring(0, 3) === 'RET' || pickLocation.substring(0, 1) === 'X' || pickLocation.substring(0, 1) === 'Y'){
			var pickInteger = 17;
		}else {
			var pickStringType = pickLocation.substring(3, 5);
			var pickInteger = parseInt(pickStringType);
		}
		return pickInteger.toString();
	} else {
		return 0;
	}
} /* parsePickZone */
