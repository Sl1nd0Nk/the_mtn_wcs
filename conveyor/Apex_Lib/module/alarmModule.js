var PickerNotLoggedIn = require('../model/ptlLoggedInLogs')

var moment = require('moment')

var AlarmMod = function(){}

AlarmMod.prototype.upDateLoggedInUser = function(toteID, zoneID){
    var pickerNotLoggedIn = new PickerNotLoggedIn({
        toteID      : toteID,
        ptlZoneID   : zoneID,
        Date        : moment().format('DD-MM-YYYY, h:mm a'),
        TimeIn      : Date.now()
    })

    pickerNotLoggedIn.save()
}

module.exports = new AlarmMod()