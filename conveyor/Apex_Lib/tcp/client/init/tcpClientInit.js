/**
* Initialise TCP client connection.
* @param  {string} host The host to connect to.
* @param  {number} port The port to connect to.
* @param  {Function} onErrorEvent Callback triggers on error.
* @param  {Function} onCloseEvent Callback triggers on close of the connection.
* @param  {Function} onDataEvent Callback triggers on data received.
* @param  {Function} onInitCallback Callback triggers on initialise.
*/
function tcpClientInit (host, port, onErrorEvent, onCloseEvent, onDataEvent, onInitCallback) {
    var net = require('net')
    var netModule = new net.Socket()

    netModule.on('error', onErrorEvent)
    netModule.on('close', onCloseEvent)
    netModule.on('data', onDataEvent)

    // Create client connection.
    netModule.connect(port, host, function () {
        return onInitCallback(netModule) // Return the client connection via a callback.
    })
}

module.exports = tcpClientInit
