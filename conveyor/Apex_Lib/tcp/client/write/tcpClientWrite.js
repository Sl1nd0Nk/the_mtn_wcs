/*
 * @Author: Morne Ausmeier
 * @Date: 2017-02-22 21:41:42
 * @Last Modified by: Morne Ausmeier
 * @Last Modified time: 2017-02-22 21:43:09
 */

/**
* Write a message via the tcp protocol.
* @param  {Object} tcpClient The tcp client connection. See tcpClientInit.js
* @param  {type} message The message to write.
*/
function tcpClientWrite (tcpClient, message) {
    tcpClient.write(message)
}

module.exports = tcpClientWrite
