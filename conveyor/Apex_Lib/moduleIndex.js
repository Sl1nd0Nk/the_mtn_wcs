/*
 * @Author: Morne.Ausmeier
 * @Date: 2017-02-19 18:32:29
 * @Last Modified by: Morne Ausmeier
 */

var finsClient = require('./fins/init/finsInit.js')
var finsRead = require('./fins/read/finsRead.js')
var finsReadSync = require('./fins/read/finsRead.js')
var finsWrite = require('./fins/write/finsWrite.js')
var finsWriteSync = require('./fins/write/finsWriteSync')

var tcpClientInit = require('./tcp/client/init/tcpClientInit.js')
var tcpClientWrite = require('./tcp/client/write/tcpClientWrite.js')

/**
 * Index containing fins protocol functions.
 */
var fins = {client: finsClient, read: finsRead, readSync: finsReadSync, write: finsWrite, writeSync: finsWriteSync}
module.exports.fins = fins

/**
* Index containing tcp functions.
*/
var tcp = {client: tcpClientInit, write: tcpClientWrite}
module.exports.tcp = tcp
