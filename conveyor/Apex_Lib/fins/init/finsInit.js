﻿/*
 * @Author: Morne Ausmeier
 * @Date: 2017-02-23 08:29:56
 * @Last Modified by: Morne Ausmeier
 * @Last Modified time: 2017-05-03 23:16:19
 */

/**
* Initialise a connection to a device via the fins protocol.
* @param  {number} port The device port. Use 9600 if you are unsure.
* @param  {string} ip The ip of the device.
* @param  {number} timeout The timeout in milliseconds.
* @param  {Function} onReplyCallback Function that gets called when we recieve a reply from the fins device. This is used for reading and writing.
*/
function finsInit (port, ip, timeout, onReplyCallback) {
    var finsModule = require('../lib/index.js').FinsClient
    var options = { timeout: 1000 }
    var finsClient = null

    options.timeout = timeout // Set the timeout for the connection.

    finsClient = finsModule(port, ip, options) // Instantiate the fins client.

    finsClient.on('reply', onReplyCallback) // Setting up the response listener.

    return finsClient
}

module.exports = finsInit
