/**
* Write to the specified registry synchronously address using the Fins UDP protocol.
* @param {object} finsClient The fins client object. See the finsInit module to create a connection.
* @param {string} address The registry address to start writing from.
* @param {number[]} values The value/s to write. Specify either a single number e.g. 1234 or an array of numbers e.g. [1, 2, 3, 4]
*/
function finsWriteSync (finsClient, address, values) {
    return finsClient.write(address, values)
}

module.exports = finsWriteSync
