﻿/*
 * @Author: Morne Ausmeier
 * @Date: 2017-02-20 07:35:12
 * @Last Modified by: Morne Ausmeier
 * @Last Modified time: 2017-07-05 09:49:40
 */

/**
* Module that writes data to a device registry via the fins protocol.
* For development purposes
*   PLC IP = 192.168.1.140
*   Registers = D100, D200, D151 (Can Read and Write to all registers)
*/

/**
* Write to the specified registry address using the Fins UDP protocol.
* @param {object} finsClient The fins client object. See the finsInit module to create a connection.
* @param {string} address The registry address to start writing from.
* @param {number[]} values The value/s to write. Specify either a single number e.g. 1234 or an array of numbers e.g. [1, 2, 3, 4]
* @param {Function} onWriteCallback The callback that triggers after the write has been completed.
*/
function finsWrite (finsClient, address, values, onWriteCallback) {
    finsClient.write(address, values, onWriteCallback)
}

module.exports = finsWrite
