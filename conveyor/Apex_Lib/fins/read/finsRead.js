﻿/*
 * @Author: Morne.Ausmeier
 * @Date: 2017-02-19 21:14:43
 * @Last Modified by: Morne Ausmeier
 * @Last Modified time: 2017-07-04 09:09:23
 */

/**
* For development purposes
*   PLC IP = 192.168.1.140
*   Registers = D100, D200, D151 (Can Read and Write to all registers)
*/

/**
* Reads the specified registry addresses using the fins UDP protocol.
* @param {object} finsClient The fins client object. See the finsInit module to create a connection.
* @param {string} startAddress The registry address to start reading from.
* @param {number} registryRange The range to read.
* @param {Function} onReadCallback Callback when the device has been read.
*/
function finsRead (finsClient, startAddress, registryRange, onReadCallback) {
    finsClient.read(startAddress, registryRange, onReadCallback)
}

module.exports = finsRead
