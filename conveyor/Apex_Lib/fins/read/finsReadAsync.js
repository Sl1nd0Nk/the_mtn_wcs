/*
 * @Author: Morne Ausmeier
 * @Date: 2017-07-04 09:09:36
 * @Last Modified by:   Morne Ausmeier
 * @Last Modified time: 2017-07-04 09:09:36
 */

/**
* Reads the specified registry addresses synchronously using the fins protocol.
* @param {object} finsClient The fins client object. See the finsInit module to create a connection.
* @param {string} startAddress The registry address to start reading from.
* @param {number} registryRange The range to read.
*/
function finsReadSync (finsClient, startAddress, registryRange) {
    return finsClient.read(startAddress, registryRange)
}

module.exports = finsReadSync
