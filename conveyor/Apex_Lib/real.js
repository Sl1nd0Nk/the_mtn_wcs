/*require('appdynamics').profile({
    controllerHostName: '10.20.88.95',
    controllerPort: 8090,
    accountName: 'customer1',
    accountAccessKey: '91f9b93a-0a53-4caa-8bde-f4f8328773dc',
    applicationName: 'Midrand Prod WCS',
    tierName: 'NodeJS',
    nodeName: 'Conveyor',
    debug: true,
logging:{
   'logfiles': [{
   'root_directory':'/tmp/appd',
   'filename':'echo_%N.log',
   'level': 'DEBUG',
   'max_size': 10000,
   'max_files': 1
}]
}
});*/

var express            = require('express');
var bodyParser         = require('body-parser');
var restler            = require('restler');
var moduleIndex        = require('./moduleIndex.js');
var mongoose           = require('mongoose');
var config             = require('conveyor');
var pickList           = require('pickListModel');
var parameters         = require('parameterModel');
var RejectedQC         = require('rejectedQCModel');
var packStations       = require('packStationModel');
var CourierReject      = require('courierRejectModel');
var PickToZone         = require('pickToZoneModel');
var PtlReject          = require('ptlRejectModel');
var ChuteModel         = require('chuteModel');
var CourierDivert      = require('courierDivertModel');
var WeightFailLog      = require('weightFailLog');
var ScaleThroughput    = require('scaleThroughModel');
var CourierNoRead      = require('courierNoRead');
var PickThrouput       = require('pickThroughputModel');
var CartonShipped      = require('cartonShippedModel');
var PutWallDash        = require('putWallDashModel');
var chutesQty          = require('chuteQtyModel');
var WeightScaleRejects = require('WeightScaleRejectsModel');
var CourierRejects     = require('CourierRejectsModel');
var ShipUpdate         = require('ShipUpdatesModel');
var CourierCarton      = require('CourierCartonUpdatesModel');
var WmsPackList        = require('WmsPackListUpdatesModel');
var CanReq             = require('CancelRequestModel');
var IncRoute		   = require('IncorrectCourierModel');

var mongooseSchema     = mongoose.Schema;
var app                = express();
var moment             = require('moment');
var async              = require('async');
var jsonParser         = bodyParser.json();

var net = require('net');

app.use(bodyParser.urlencoded({ extended: false }));

var weight1 = null; // Weigh Scale 1 value.
var weight2 = null; // Weigh Scale 2 value.
var weight3 = null;

var ScaleScanner1Read = false;
var ScaleScanner2Read = false;
var DecScannerRead = false;
var HatchScanner1Read = false;
var HatchScanner2Read = false;

var HatchWeight1 = null;
var HatchWeight2 = null;

var LastUsedChute  = 0;
var lastRead       = null;

var lastReadScale1 = null;
var lastReadScale2 = null;

var lastReadHatch1 = null;
var lastReadHatch2 = null;

/* Connect to the remote Fins device. Timeout after 3 seconds. Return the client object. */
var finsClientObject = moduleIndex.fins.client(config.plc.port, config.plc.url, config.plc.timeout, onReplyCallback);

var qProcDecisionPoint = async.queue(ProcDecisionPoint, 2);
var qProcWeightScale1  = async.queue(ProcWeightScale1, 2);
var qProcWeightScale2  = async.queue(ProcWeightScale2, 2);
var qProcCourierHatch1 = async.queue(ProcCourierHatch1, 2);
var qProcCourierHatch2 = async.queue(ProcCourierHatch2, 2);

qProcDecisionPoint.drain = function(){}
qProcWeightScale1.drain  = function(){}
qProcWeightScale2.drain  = function(){}
qProcCourierHatch1.drain = function(){}
qProcCourierHatch2.drain = function(){}

/* Weight Scale 1.
moduleIndex.tcp.client(config.weightScale.one.url,
					   config.weightScale.one.port,
					   onWeightScale1Error,
					   onWeightScale1Close,
					   onWeightScale1DataReceived,
					   onWeightScale1Connection);*/

/* Weight Scale 2.
moduleIndex.tcp.client(config.weightScale.two.url,
					   config.weightScale.two.port,
					   onWeightScale2Error,
					   onWeightScale2Close,
					   onWeightScale2DataReceived,
					   onWeightScale2Connection);*/

/* Weight Scale 3. */
moduleIndex.tcp.client(config.weightScale.three.url,
					   config.weightScale.three.port,
					   onWeightScale3Error,
					   onWeightScale3Close,
					   onWeightScale3DataReceived,
					   onWeightScale3Connection);

/* Weight Scale 4. */
moduleIndex.tcp.client(config.weightScale.four.url,
					   config.weightScale.four.port,
					   onWeightScale4Error,
					   onWeightScale4Close,
					   onWeightScale4DataReceived,
					   onWeightScale4Connection);

/* Connect to the MongoDB instance. */
mongoose.connect(config.mongoDB.url, { useMongoClient: true }, function (error, client) {
    if (error) {
        console.log('MongoDB: Connection Error: ' + error);
    } else {
        console.log('MongoDB: Connection Established.');

		//BACKDOOR_TestScanners(null, null, null, null, null, null, null, null, null);
    }
});

/* POST a release command to WCS. */
app.post(config.toteRelease.url,jsonParser, function (req, res) {
    try {
        var toteID = req.body.toteID;
        var location = req.body.location;

        if (location === '1') pickZone1Released = true;
        if (location === '2') pickZone2Released = true;
        if (location === '3') pickZone3Released = true;
        if (location === '4') pickZone4Released = true;
        if (location === '5') pickZone5Released = true;
        if (location === '6') pickZone6Released = true;
        if (location === '7') pickZone7Released = true;
        if (location === '8') pickZone8Released = true;
        if (location === '9') pickZone9Released = true;
        if (location === '10') pickZone10Released = true;
        if (location === '11') pickZone11Released = true;
        if (location === '12') pickZone12Released = true;
        if (location === '13') pickZone13Released = true;
        if (location === '14') pickZone14Released = true;

        console.log( toteID + ' ' + location )
        return res.status(500).send('Request Received.' + ' ToteID: ' + toteID + ' Location: ' + location);
    } catch (error) {
        console.log(error);
    }
});

/* Create the server. */
var server = app.listen(config.toteRelease.port, function (){
    console.log('Server listening on port ' + server.address().port);
});
// Used to enforce synchronous processing of PLC results.
var processingResultStarted = false;

// In-feed.
var sensor1Busy = false;

// PTL Zones.
var sensor2Busy = false;
var sensor3Busy = false;
var sensor4Busy = false;
var sensor5Busy = false;
var sensor6Busy = false;
var sensor7Busy = false;
var sensor8Busy = false;
var sensor9Busy = false;
var sensor10Busy = false;

// Pick Areas.
var sensor11Busy = false;
var sensor12Busy = false;
var sensor13Busy = false;
var sensor14Busy = false;
var sensor15Busy = false;
var sensor16Busy = false;
var sensor17Busy = false;
var sensor18Busy = false;
var sensor19Busy = false;
var sensor20Busy = false;
var sensor21Busy = false;
var sensor22Busy = false;
var sensor23Busy = false;
var sensor24Busy = false;

// Packing Stations.
var sensor25Busy = null;
var sensor26Busy = null;
var sensor27Busy = null;
var sensor28Busy = null;
var sensor29Busy = null;
var sensor30Busy = null;
var sensor31Busy = null;
var sensor32Busy = null;
var sensor33Busy = null;

// Courier.
var sensor34Busy = false;
var sensor35Busy = false;
var sensor36Busy = false;
var sensor37Busy = false;
var sensor38Busy = false;
var sensor39Busy = false;

var WCSHeartbeat = false;

// Pick Zone release.
var pickZone1Released = false;
var pickZone2Released = false;
var pickZone3Released = false;
var pickZone4Released = false;
var pickZone5Released = false;
var pickZone6Released = false;
var pickZone7Released = false;
var pickZone8Released = false;
var pickZone9Released = false;
var pickZone10Released = false;
var pickZone11Released = false;
var pickZone12Released = false;
var pickZone13Released = false;
var pickZone14Released = false;

/* Set an PLC reading interval. */
setInterval(onHeartbeatCallback, config.plc.ReadInterval);

// Callback function triggers after reading/writing to/from the Fins device. This callback contains the actual response data.
function onReplyCallback (replyMessage){
    let scannerData = [];

    if (replyMessage.values){

        while (replyMessage.values.length) {
			/* Splice the full PLC read range into increments of 10. */
            scannerData.push(replyMessage.values.splice(0, 10));
        }

        /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~PTL Zones~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

        /* In-Feed / Reject */
        if (scannerData[0][0] === 1 && !sensor1Busy) {
            sensor1Busy = true;
            let toteID = getToteBarcode(scannerData[0]);
            console.log(toteID + ' at scanner 1.');
            checkPickedTote(toteID, function(toteState){
				if(toteState == true){
					let msg = "Tote rejected because it cannot be packed.";
					moduleIndex.fins.writeSync(finsClientObject, 'D5000', [1, 20]);
					rejectTote(toteID, msg);
				}else{
					if((toteID[0] != '0' && toteID[0] != '1') || toteID.length != 7){
						console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss:SSS') + ' LEF1: Invalid Tote ID ' + toteID + ' Found on conveyor - does not match tote ID standards');
						moduleIndex.fins.writeSync(finsClientObject, address, [1, 20]);
					} else {
						findOrAssociatePicklist(toteID, 'D5000', scannerData[0][1], [0], function(){});
					}
				}
		 	});
        }

        if (scannerData[0][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5000', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]); // Ack and send destination.
            sensor1Busy = false;
        }

        /* Zone 1 & 2 */
        if (scannerData[1][0] === 1 && !sensor2Busy) {
            sensor2Busy = true;
            let toteID = getToteBarcode(scannerData[1]);
            console.log(toteID + ' At Zone 1/2');
            findOrAssociatePicklist(toteID, 'D5010', scannerData[1][1], [1,2], function(){});
        }

        if (scannerData[1][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5010', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]); // Ack and send destination.
            sensor2Busy = false;
        }

        /* Zone 3 & 4 */
        if (scannerData[2][0] === 1 && !sensor3Busy) {
            sensor3Busy = true;
            let toteID = getToteBarcode(scannerData[2]);
            console.log(toteID + ' At Zone 3/4');
            findOrAssociatePicklist(toteID, 'D5020', scannerData[2][1], [3,4], function(){});
        }

        if (scannerData[2][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5020', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]); // Ack and send destination.
            sensor3Busy = false;
        }

        /* Zone 5 & 6 */
        if (scannerData[3][0] === 1 && !sensor4Busy) {
            sensor4Busy = true;
            let toteID = getToteBarcode(scannerData[3]);
            console.log(toteID + ' At Zone 5/6');
            findOrAssociatePicklist(toteID, 'D5030', scannerData[3][1], [5,6], function(){});
        }

        if (scannerData[3][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5030', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]); // Ack and send destination.
            sensor4Busy = false;
        }

        /* Zone 7 */
        if (scannerData[4][0] === 1 && !sensor5Busy) {
            sensor5Busy = true;
            let toteID = getToteBarcode(scannerData[4]);
            console.log(toteID + ' At Zone 7');
            findOrAssociatePicklist(toteID, 'D5040', scannerData[4][1], [7], function(){});
        }

        if (scannerData[4][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5040', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]); // Ack and send destination.
            sensor5Busy = false;
        }

        /* Zone 8 */
        if (scannerData[5][0] === 1 && !sensor6Busy) {
            sensor6Busy = true;
            let toteID = getToteBarcode(scannerData[5]);
            console.log(toteID + ' At Zone 8');
            findOrAssociatePicklist(toteID, 'D5050', scannerData[5][1], [8], function(){});
        }

        if (scannerData[5][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5050', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]); // Ack and send destination.
            sensor6Busy = false;
        }

        /* Zone 9 & 10 */
        if (scannerData[6][0] === 1 && !sensor7Busy) {
            sensor7Busy = true;
            let toteID = getToteBarcode(scannerData[6]);
            console.log(toteID + ' At Zone 9/10');
            findOrAssociatePicklist(toteID, 'D5060', scannerData[6][1], [9,10], function(){});
        }

        if (scannerData[6][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5060', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]); // Ack and send destination.
            sensor7Busy = false;
        }

        /* Zone 11 & 12 */
        if (scannerData[7][0] === 1 && !sensor8Busy) {
            sensor8Busy = true;
            let toteID = getToteBarcode(scannerData[7]);
            console.log(toteID + ' At Zone 11/12');
            findOrAssociatePicklist(toteID, 'D5070', scannerData[7][1], [11,12], function(){});
        }

        if (scannerData[7][0] === 10){
            moduleIndex.fins.writeSync(finsClientObject, 'D5070', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]); // Ack and send destination.
            sensor8Busy = false;
        }

        /* Zone 13 & 14 */
        if (scannerData[8][0] === 1 && !sensor9Busy) {
            sensor9Busy = true;
            let toteID = getToteBarcode(scannerData[8]);
            console.log(toteID + ' At Zone 13/14');
            findOrAssociatePicklist(toteID, 'D5080', scannerData[8][1], [13,14], function(){});
        }

        if (scannerData[8][0] === 10){
            moduleIndex.fins.writeSync(finsClientObject, 'D5080', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]); // Ack and send destination.
            sensor9Busy = false;
        }

        /* To Man / Recirc */
        if (scannerData[9][0] === 1 && !sensor10Busy){
            sensor10Busy = true;
            let toteID = getToteBarcode(scannerData[9]);
            console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss:SSS') + ' LEF1:' + toteID + ' at Man/Recirc');
            findOrAssociatePicklist(toteID, 'D5090', scannerData[9][1], [null], function(){});
        } /*else {
            console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss:SSS') + ' LEF1: Man/Recirc ... sensor10Busy = (' + sensor10Busy +') | Value = (' + scannerData[9][0] + ')');
        }*/

        if (scannerData[9][0] === 10){
            moduleIndex.fins.writeSync(finsClientObject, 'D5090', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]); // Ack and send destination.
            console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss:SSS') + ' LEF1: Man/Recirc register cleared');
            sensor10Busy = false;
        }

        /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~PTL Pick Stop~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        /* Zone 1 stop */
        if (scannerData[10][0] === 8 && !sensor11Busy) {
            let toteID = getToteBarcode(scannerData[10]);
            console.log(toteID + ' At 1 Stop.');
            sensor11Busy = true;
            pickZone1Released = false;
            requestPicking(1, toteID);
        }

        if (pickZone1Released){
            pickZone1Released = false;
            moduleIndex.fins.writeSync(finsClientObject, 'D5100', [1, 2]); // Ack and send destination.
            sensor11Busy = false;
        }

        if (scannerData[10][0] === 10){
            moduleIndex.fins.writeSync(finsClientObject, 'D5100', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]); // Ack and send destination.
            sensor11Busy = false;
        }

        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // Zone 2 stop
        if (scannerData[11][0] === 8 && !sensor12Busy) {
            let toteID = getToteBarcode(scannerData[11])
            console.log(toteID + 'At 2 Stop.')
            sensor12Busy = true
            pickZone2Released = false
            requestPicking(2, toteID)
        }

        if (pickZone2Released) {
            pickZone2Released = false
	    	moduleIndex.fins.writeSync(finsClientObject, 'D5110', [1, 2]) // Ack and send destination.
            sensor12Busy = false
        }

        if (scannerData[11][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5110', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) // Ack and send destination.
            sensor12Busy = false
        }

        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // Zone 3 stop
        if (scannerData[12][0] === 8 && !sensor13Busy) {
            let toteID = getToteBarcode(scannerData[12])
            console.log(toteID + 'At 3 Stop.')
            sensor13Busy = true
            pickZone3Released = false
            requestPicking(3, toteID)
        }

        if (pickZone3Released) {
            pickZone3Released = false
            moduleIndex.fins.writeSync(finsClientObject, 'D5120', [1, 2]) // Ack and send destination.
            sensor13Busy = false
        }

        if (scannerData[12][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5120', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) // Ack and send destination.
            sensor13Busy = false
        }

        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // Zone 4 stop
        if (scannerData[13][0] === 8 && !sensor14Busy) {
            let toteID = getToteBarcode(scannerData[13])
            console.log(toteID + 'At 4 Stop.')
            sensor14Busy = true
            pickZone4Released = false
            requestPicking(4, toteID)
        }

        if (pickZone4Released) {
            pickZone4Released = false
            moduleIndex.fins.writeSync(finsClientObject, 'D5130', [1, 2]) // Ack and send destination.
            sensor14Busy = false
        }

        if (scannerData[13][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5130', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) // Ack and send destination.
            sensor14Busy = false
        }

        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // Zone 5 stop
        if (scannerData[14][0] === 8 && !sensor15Busy) {
            let toteID = getToteBarcode(scannerData[14])
            console.log(toteID + 'At 5 Stop.')
            sensor15Busy = true
            pickZone5Released = false

            requestPicking(5, toteID)
        }

        if (pickZone5Released) {
            pickZone5Released = false
            moduleIndex.fins.writeSync(finsClientObject, 'D5140', [1, 2]) // Ack and send destination.
            sensor15Busy = false
        }

        if (scannerData[14][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5140', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) // Ack and send destination.
            sensor15Busy = false
        }

        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // Zone 6 stop
        if (scannerData[15][0] === 8 && !sensor16Busy) {
            let toteID = getToteBarcode(scannerData[15])
            console.log(toteID + 'At 6 Stop.')
            sensor16Busy = true
            pickZone6Released = false

            requestPicking(6, toteID)
        }

        if (pickZone6Released) {
            pickZone6Released = false
            moduleIndex.fins.writeSync(finsClientObject, 'D5150', [1, 2]) // Ack and send destination.
            sensor16Busy = false
        }

       if (scannerData[15][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5150', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) // Ack and send destination.
            sensor16Busy = false
        }

        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // Zone 7 stop
        if (scannerData[16][0] === 8 && !sensor17Busy) {
            let toteID = getToteBarcode(scannerData[16])
            console.log(toteID + ' At 7 Stop.')
            sensor17Busy = true
            pickZone7Released = false
            requestPicking(7, toteID)
        }

        if (pickZone7Released) {
            pickZone7Released = false
            moduleIndex.fins.writeSync(finsClientObject, 'D5160', [1, 2]) // Ack and send destination.
            sensor17Busy = false
        }

        if (scannerData[16][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5160', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) // Ack and send destination.
            sensor17Busy = false
        }

        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // Zone 8 stop
        if (scannerData[17][0] === 8 && !sensor18Busy) {
            let toteID = getToteBarcode(scannerData[17])
            console.log(toteID + 'At 8 Stop.')
            sensor18Busy = true
            pickZone8Released = false
            requestPicking(8, toteID)
        }

        if (pickZone8Released) {
            pickZone8Released = false
            moduleIndex.fins.writeSync(finsClientObject, 'D5170', [1, 2]) // Ack and send destination.
            sensor18Busy = false
        }

        if (scannerData[17][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5170', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) // Ack and send destination.
            sensor18Busy = false
        }

        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // Zone 9 stop.
        if (scannerData[18][0] === 8 && !sensor19Busy) {
            let toteID = getToteBarcode(scannerData[18])
            console.log(toteID + 'At 9 Stop.')
            sensor19Busy = true
            pickZone9Released = false
            requestPicking(9, toteID)
        }

        if (pickZone9Released) {
            pickZone9Released = false
            moduleIndex.fins.writeSync(finsClientObject, 'D5180', [1, 2]) // Ack and send destination.
            sensor19Busy = false
        }

        if (scannerData[18][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5180', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) // Ack and send destination.
            sensor19Busy = false
        }

        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // Zone 10 stop.
        if (scannerData[19][0] === 8 && !sensor20Busy) {
            let toteID = getToteBarcode(scannerData[19])
            console.log(toteID + 'At 10 Stop.')
            sensor20Busy = true
            pickZone10Released = false
            requestPicking(10, toteID)
        }

        if (pickZone10Released) {
            pickZone10Released = false
            moduleIndex.fins.writeSync(finsClientObject, 'D5190', [1, 2]) // Ack and send destination.
            sensor20Busy = false
        }

        if (scannerData[19][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5190', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) // Ack and send destination.
            sensor20Busy = false
        }

        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // Zone 11 stop.
        if (scannerData[20][0] === 8 && !sensor21Busy) {
            let toteID = getToteBarcode(scannerData[20])
            console.log(toteID + 'At 11 Stop.')
            sensor21Busy = true
            pickZone11Released = false

            requestPicking(11, toteID)
        }

        if (pickZone11Released) {
            pickZone11Released = false
            moduleIndex.fins.writeSync(finsClientObject, 'D5200', [1, 2]) // Ack and send destination.
            sensor21Busy = false
        }

        if (scannerData[20][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5200', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) // Ack and send destination.
            sensor21Busy = false
        }

        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // Zone 12 stop.
        if (scannerData[21][0] === 8 && !sensor22Busy) {
            sensor22Busy = true
            let toteID = getToteBarcode(scannerData[21])
            console.log(toteID + ' at 12 Stop.')
            pickZone12Released = false

            requestPicking(12, toteID)
        }

        if (pickZone12Released) {
            pickZone12Released = false
            moduleIndex.fins.writeSync(finsClientObject, 'D5210', [1, 2]) // Ack and send destination.
            sensor22Busy = false
        }

        if (scannerData[21][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5210', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) // Ack and send destination.
            sensor22Busy = false
        }

        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // Zone 13 stop.
        if (scannerData[22][0] === 8 && !sensor23Busy) {
            sensor23Busy = true
            pickZone13Released = false
            let toteID = getToteBarcode(scannerData[22])
            console.log(toteID + ' at 13 Stop.')
            requestPicking(13, toteID)
        }

        if (pickZone13Released) {
            pickZone13Released = false
            moduleIndex.fins.writeSync(finsClientObject, 'D5220', [1, 2]) // Ack and send destination.
            sensor23Busy = false
        }

        if (scannerData[22][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5220', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) // Ack and send destination.
            sensor23Busy = false
        }

        if (pickZone14Released) {
            pickZone14Released = false
            moduleIndex.fins.writeSync(finsClientObject, 'D5230', [1, 2]) // Ack and send destination.
            sensor24Busy = false
        }

        if (scannerData[23][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5230', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) // Ack and send destination.
            sensor24Busy = false
        }

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Packing Stations~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		/* If scanner 25 has a tote. */
        if (scannerData[24][0] === 1){
            let toteID = getToteBarcode1('Packing', scannerData[24]);

            if(sensor25Busy != toteID){
				sensor25Busy = toteID;

				GetOrUpdateToteChuteLocation(toteID, 1, function(route){
					console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ----->>Packing: Tote [' + toteID + '] at scanner (1/2)');
					console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' -----<<Packing: Tote [' + toteID + '] heading to chute ' + route);
					moduleIndex.fins.writeSync(finsClientObject, 'D5240', [1, route]);
				});
			}
        }

		if (scannerData[24][0] === 10){
            moduleIndex.fins.writeSync(finsClientObject, 'D5240', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        }

		/* If scanner 26 has a tote. */
        if (scannerData[25][0] === 1){
            let toteID = getToteBarcode1('Packing', scannerData[25]);

            if(sensor26Busy != toteID){
				sensor26Busy = toteID;

				GetOrUpdateToteChuteLocation(toteID, 3, function(route){
					console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ----->>Packing: Tote [' + toteID + '] at scanner (3/4)');
					console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' -----<<Packing: Tote [' + toteID + '] heading to chute ' + route);
					moduleIndex.fins.writeSync(finsClientObject, 'D5250', [1, route]);
				});
			}
        }

        if (scannerData[25][0] === 10){
            moduleIndex.fins.writeSync(finsClientObject, 'D5250', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        }

		/* If scanner 27 has a tote. */
        if (scannerData[26][0] === 1){
            let toteID = getToteBarcode1('Packing', scannerData[26]);

            if(sensor27Busy != toteID){
				sensor27Busy = toteID;

				GetOrUpdateToteChuteLocation(toteID, 5, function(route){
					console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ----->>Packing: Tote [' + toteID + '] at scanner (5/6)');
					console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' -----<<Packing: Tote [' + toteID + '] heading to chute ' + route);
					moduleIndex.fins.writeSync(finsClientObject, 'D5260', [1, route]);
				});
			}
        }

        if (scannerData[26][0] === 10){
            moduleIndex.fins.writeSync(finsClientObject, 'D5260', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        }

		/* If scanner 28 has a tote. */
        if (scannerData[27][0] === 1){
            let toteID = getToteBarcode1('Packing', scannerData[27]);

            if(sensor28Busy != toteID){
				sensor28Busy = toteID;

				GetOrUpdateToteChuteLocation(toteID, 7, function(route){
					console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ----->>Packing: Tote [' + toteID + '] at scanner (7/8)');
					console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' -----<<Packing: Tote [' + toteID + '] heading to chute ' + route);
					moduleIndex.fins.writeSync(finsClientObject, 'D5270', [1, route]);
				});
			}
        }

        if (scannerData[27][0] === 10){
            moduleIndex.fins.writeSync(finsClientObject, 'D5270', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        }

		/* If scanner 29 has a tote. */
        if (scannerData[28][0] === 1){
            let toteID = getToteBarcode1('Packing', scannerData[28]);

            if(sensor29Busy != toteID){
				sensor29Busy = toteID;

				GetOrUpdateToteChuteLocation(toteID, 9, function(route){
					console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ----->>Packing: Tote [' + toteID + '] at scanner (9/10)');
					console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' -----<<Packing: Tote [' + toteID + '] heading to chute ' + route);
					moduleIndex.fins.writeSync(finsClientObject, 'D5280', [1, route]);
				});
			}
        }

        if (scannerData[28][0] === 10){
            moduleIndex.fins.writeSync(finsClientObject, 'D5280', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        }

		/* If scanner 30 has a tote. */
        if (scannerData[29][0] === 1){
            let toteID = getToteBarcode1('Packing', scannerData[29]);

            if(sensor30Busy != toteID){
				sensor30Busy = toteID;

				GetOrUpdateToteChuteLocation(toteID, 11, function(route){
					console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ----->>Packing: Tote [' + toteID + '] at scanner (11/12)');
					console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' -----<<Packing: Tote [' + toteID + '] heading to chute ' + route);
					moduleIndex.fins.writeSync(finsClientObject, 'D5290', [1, route]);
				});
			}
        }

        if (scannerData[29][0] === 10){
            moduleIndex.fins.writeSync(finsClientObject, 'D5290', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        }

		/* If scanner 31 has a tote. */
        if (scannerData[30][0] === 1){
            let toteID = getToteBarcode1('Packing', scannerData[30]);

            if(sensor31Busy != toteID){
				sensor31Busy = toteID;

				GetOrUpdateToteChuteLocation(toteID, 13, function(route){
					console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ----->>Packing: Tote [' + toteID + '] at scanner (13/14)');
					console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' -----<<Packing: Tote [' + toteID + '] heading to chute ' + route);
					moduleIndex.fins.writeSync(finsClientObject, 'D5300', [1, route]);
				});
			}
        }

        if (scannerData[30][0] === 10){
            moduleIndex.fins.writeSync(finsClientObject, 'D5300', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        }

		/* If scanner 32 has a tote. */
        if (scannerData[31][0] === 1){
            let toteID = getToteBarcode1('Packing', scannerData[31]);

            if(sensor32Busy != toteID){
				sensor32Busy = toteID;

				GetOrUpdateToteChuteLocation(toteID, 15, function(route){
					console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ----->>Packing: Tote [' + toteID + '] at scanner (15/16)');
					console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' -----<<Packing: Tote [' + toteID + '] heading to chute ' + route);
					moduleIndex.fins.writeSync(finsClientObject, 'D5310', [1, route]);
				});
			}
        }

        if (scannerData[31][0] === 10){
            moduleIndex.fins.writeSync(finsClientObject, 'D5310', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        }

		/* If scanner 33 has a tote. */
        if (scannerData[32][0] === 1){
            let toteID = getToteBarcode1('Packing', scannerData[32]);

            if(sensor33Busy != toteID){
				sensor33Busy = toteID;

				GetOrUpdateToteChuteLocation(toteID, 17, function(route){
					console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ----->>Packing: Tote [' + toteID + '] at scanner (17/18)');
					console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' -----<<Packing: Tote [' + toteID + '] heading to chute ' + route);
					moduleIndex.fins.writeSync(finsClientObject, 'D5320', [1, route]);
				});
			}
        }

        if (scannerData[32][0] === 10){
            moduleIndex.fins.writeSync(finsClientObject, 'D5320', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        }

        // ~~~~~~~~~~~~~~~~~~~~~~~Weight Scale 1 and 2 Scanners~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        var weight1ScannerData = scannerData[33].concat(scannerData[34])
        // Scanner at Weight Scale 1

        if(scannerData[33][0] === 7){
			let containerObj1 = getCourierContainerBarcode1('Scale 1', weight1ScannerData);
            let containerID1 = containerObj1.barcode;
            let barcodeArr = containerObj1.barcodeArr;
            ScaleScanner1Read = true;

            if(!containerID1){
				lastReadScale1 = null;
				containerID1 = null;
				moduleIndex.fins.writeSync(finsClientObject, 'D5330', [1, 2]);
				console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '------<<Scale 1: [1, 2] | Not a valid container');
				ScaleScanner1Read = false;
			} else {
				if(lastReadScale1 != containerID1){
					lastReadScale1 = containerID1;
					return GetWeightFromScale1(config.weightScale.one.url, config.weightScale.one.port, containerID1);
				}
			}
		}

        if (scannerData[33][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5330', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
            lastReadScale1 = null;
        }

        var weight2ScannerData = scannerData[35].concat(scannerData[36])
	    // Scanner at Weight Scale 2.

        if (scannerData[35][0] === 7) {
			let containerObj2 = getCourierContainerBarcode1('Scale 2', weight2ScannerData);
            let containerID2 = containerObj2.barcode;
            let barcodeArr = containerObj2.barcodeArr;
            ScaleScanner2Read = true;

            if(!containerID2){
				lastReadScale2 = null;
				containerID2 = null;
				moduleIndex.fins.writeSync(finsClientObject, 'D5340', [1, 2]);
				console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '------<<Scale 2: [1, 2] | Not a valid container');
				weight1 = null;
				sensor34Busy = false;
				ScaleScanner2Read = false;
			} else {
				if(lastReadScale2 != containerID2){
					lastReadScale2 = containerID2;
					return GetWeightFromScale2(config.weightScale.two.url, config.weightScale.two.port, containerID2);
				}
			}

		}

        if (scannerData[35][0] === 10){
	        moduleIndex.fins.writeSync(finsClientObject, 'D5340', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
	        lastReadScale2 = null;
        }

        // ~~~~~~~~~~~~~~~~~~~~~~Courier Decision Making.~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		var courierDecisionScannerData = scannerData[37].concat(scannerData[38]);

		if (scannerData[37][0] === 5){
			let containerObj = getCourierContainerBarcode1('DecPoint', courierDecisionScannerData);
            let containerID = containerObj.barcode;
            let barcodeArr = containerObj.barcodeArr;
            DecScannerRead = true;

			if(lastRead != containerID){
				if(containerID){
					lastRead = containerID;

					qProcDecisionPoint.push({containerID: containerID}, function(err, data){
						DecScannerRead = false;
						if(data == lastRead){
							lastRead = null;
						}
					});
				} else {
				   moduleIndex.fins.writeSync(finsClientObject, 'D5350', [1, 4]);
				   DecScannerRead = false;
				   console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '-----<<DecPoint: [1, 4] | No Read');
				}
			}
		}

        if (scannerData[37][0] === 10) {
            console.log('CLV_C: Courier decision cleared.');
            moduleIndex.fins.writeSync(finsClientObject, 'D5350', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
            sensor36Busy = false;
        }

        // ~~~~~~~~~~~~~~~~~~~~Courier Hatches~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        let courierHatch1ScannerData = scannerData[39].concat(scannerData[40])

        if (scannerData[39][0] === 6) {
            //let containerID = getCourierContainerBarcode1('Hatch [RTT]', courierHatch1ScannerData);
			let containerObj = getCourierContainerBarcode1('Hatch [RTT]', courierHatch1ScannerData);
            let containerID = containerObj.barcode;
            let barcodeArr = containerObj.barcodeArr;
            HatchScanner1Read = true;

			if(lastReadHatch1 != containerID){
				if(containerID){
					lastReadHatch1 = containerID;
					let weightToCheck = HatchWeight1;
					qProcCourierHatch1.push({containerID: containerID, weight4: weightToCheck, location: 'Hatch [RTT]'}, function(err, data){
						HatchWeight1 = null;
						HatchScanner1Read = false;
						if(data == lastReadHatch1){
							lastReadHatch1 = null;
						}
					});
				} else {
					HatchWeight1 = null
				   	moduleIndex.fins.writeSync(finsClientObject, 'D5360', [1, 1]);
				   	HatchScanner1Read = false;
				   	console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '-----<<Hatch [RTT]: [1, 1] | No Read');
				}
			}
        }

	    if(scannerData[39][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5360', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) // Ack and clear.
            sensor37Busy = false
        }

        let courierHatch2ScannerData = scannerData[41].concat(scannerData[42])

        //if (scannerData[41][0] === 6 && HatchWeight2) {
        if (scannerData[41][0] === 6){
            //let containerID = getCourierContainerBarcode1('Hatch [DSV]', courierHatch2ScannerData);
			let containerObj = getCourierContainerBarcode1('Hatch [DSV]', courierHatch2ScannerData);
			let containerID = containerObj.barcode;
			let barcodeArr = containerObj.barcodeArr;
			HatchScanner2Read = true;

			if(lastReadHatch2 != containerID){
				if(containerID){
					lastReadHatch2 = containerID;
					let weightToCheck = HatchWeight2;
					qProcCourierHatch2.push({containerID: containerID, weight4: weightToCheck, location: 'Hatch [DSV]'}, function(err, data){
						HatchWeight2 = null;
						HatchScanner2Read = false;
						if(data == lastReadHatch2){
							lastReadHatch2 = null;
						}
					});
				} else {
					HatchWeight2 = null
				   	moduleIndex.fins.writeSync(finsClientObject, 'D5370', [1, 1]);
				   	HatchScanner2Read = false;
				   	console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '-----<<Hatch [DSV]: [1, 1] | No Read');
				}
			}
        }

	    if(scannerData[41][0] === 10) {
            moduleIndex.fins.writeSync(finsClientObject, 'D5370', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) // Ack and clear.
            sensor38Busy = false
        }
      }

      processingResultStarted = false
}

/* FSE Heartbeat event. Reads the PLC registers. */
function onHeartbeatCallback () {
    if (WCSHeartbeat){
		moduleIndex.fins.writeSync(finsClientObject, 'D5440', [1]); // Ack and send destination.
    } else {
		moduleIndex.fins.writeSync(finsClientObject, 'D5440', [0]); // Ack and send destination.
	}

    WCSHeartbeat = !WCSHeartbeat;

    if(!processingResultStarted){
        moduleIndex.fins.readSync(finsClientObject, 'D4000', 450); // Read all addresses containing sensor information.
        processingResultStarted = true;
    }
} /* onHeartbeatCallback */

function UpdateEachPLInTrWeightFail(docs, index, weight, Reason, callback){
	if(index >= docs.length){
		return callback();
	}

	if(Reason){
		docs[index].orders[0].rejected = true;
		docs[index].orders[0].rejectedReason = Reason;
	}

	docs[index].orders[0].actualWeight1 = weight;
	docs[index].save(function(saveError, saveDoc){
		index++;
		UpdateEachPLInTrWeightFail(docs, index, weight, Reason, callback);
	});
} /* UpdateEachPLInTrWeightFail */

function CompareTransporterWeights(containerID, weight, callback){
    pickList.find({transporterID: containerID}, function(error, docs){
        if(error || !docs || docs.length <= 0){
            return callback({Reject: true, Reason: 'Failed to find cartons for transporter', containerID: containerID});
        }

		let x = 0;
		let result = true;
		let Reason = null;
		let Orders = [];
		while(x < docs.length){
			Orders.push(docs[x].orders[0].orderID);
			x++;
		}

		if(weight >= docs[0].orders[0].minWeightLimit && weight <= docs[0].orders[0].maxWeightLimit) {
			result = false;
		}

		if(result){
			Reason = 'Transporter Weight Not Within Tolerance';
		}

		CanReq.find({'OrderId': {$in: Orders}}, function(err, ReqArr){
			if(ReqArr.length > 0){
					Reason = 'Order in transporter is cancelled';
					result = true;
			}

			UpdateEachPLInTrWeightFail(docs, 0, weight, Reason, function(){});

			return callback({Reject: result, Reason: Reason, Lower: docs[0].orders[0].minWeightLimit, Upper: docs[0].orders[0].maxWeightLimit, containerID: containerID});
		});
    });
} /* CompareTransporterWeights */

function CompareWeights(containerID, weight, callback){
	pickList.findOne({'orders.cartonID': containerID}, function (err, doc){
		if(err || !doc){
			return callback({Reject: true, Reason: 'Failed to find picklist for carton', containerID: containerID});
		}

     	let result = true;
      	doc.orders[0].actualWeight1 = weight;

		let LowerLim = doc.orders[0].minWeightLimit;
		let UpperLim = doc.orders[0].maxWeightLimit;

       	if(LowerLim && UpperLim){
       		if((weight >= LowerLim) && (weight <= UpperLim)){
           		result = false;
           		doc.orders[0].rejected = false;
           		doc.orders[0].rejectedReason = null;
         	} else {
           		doc.orders[0].rejectedReason = 'Carton Weight Not Within Tolerance';
           		doc.orders[0].rejected = true;
         	}

         	doc.save(function(error, savedDoc){});

         	return callback({Reject: result, Reason: doc.orders[0].rejectedReason, Lower: LowerLim, Upper: UpperLim, containerID: containerID});
       	}

		parameters.find({}, function(err, Params){
			if(err || Params.length <= 0){
				doc.orders[0].rejected = true;
				doc.orders[0].rejectedReason = 'Failed to calculate weight tolerances for carton';
			} else {
				let len = Params[0].weightSettings.length;
				if(Params[0].weightSettings && len > 0){
					let v = 0;
					while(v < len){
						if(Params[0].weightSettings[v].StockSize == parseInt(containerID[0])){
							break;
						}
						v++;
					}

					if(v < len){
						let lowerLimit = doc.orders[0].picklistWeight - Params[0].weightSettings[v].LowerTolerance;
						let upperLimit = doc.orders[0].picklistWeight + Params[0].weightSettings[v].UpperTolerance;

						doc.orders[0].minWeightLimit = lowerLimit;
						doc.orders[0].maxWeightLimit = upperLimit;

						if((weight >= lowerLimit) && (weight <= upperLimit)){
							result = false;
							doc.orders[0].rejected = false;
							doc.orders[0].rejectedReason = null;
						} else {
							doc.orders[0].rejected = true;
							doc.orders[0].rejectedReason = 'Carton Weight Not Within Tolerance';
						}
					} else {
						doc.orders[0].rejected = true;
						doc.orders[0].rejectedReason = 'Cannot find weight setting for carton';
					}
				} else {
					doc.orders[0].rejected = true;
					doc.orders[0].rejectedReason = 'Failed to calculate weight tolerances (no weight settings)';
				}
			}

			if(doc.Status == 'CANCELED'){
				result = true;
				doc.orders[0].rejected = true;
				doc.orders[0].rejectedReason = 'Order is cancelled';
				doc.save(function(error, savedDoc){
					return callback({Reject: result, Reason: savedDoc.orders[0].rejectedReason, containerID: containerID});
				});

				return;
			}

			CanReq.findOne({'OrderId': doc.orders[0].orderID}, function(err, Req){
				if(Req){
					doc.Status = 'CANCELED';
					doc.orders[0].WmsOrderStatus = 'CANCELED';
					doc.orders[0].WmsPickListStatus = 'CANCELED';
					doc.orders[0].CancelDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
					doc.orders[0].CancelInstruction = 'Fetch Parcel From QC Area';
					doc.orders[0].rejected = true;
					result = true;
					doc.orders[0].rejectedReason = 'Order is cancelled';
				}

				doc.save(function(error, savedDoc){
					if(result == true){
						return callback({Reject: result, Reason: savedDoc.orders[0].rejectedReason, containerID: containerID});
					} else {
						return callback({Reject: result, Lower: savedDoc.orders[0].minWeightLimit, Upper: savedDoc.orders[0].maxWeightLimit, containerID: containerID });
					}
				});
			});
		});
  	});
} /* CompareWeights */

function QCUpdateEachTrCarton(task, callback){
	let Pdoc = task.Doc;
	let TransporterID = task.containerID;
	let reason = task.reason;
	let actualWeight = task.actualWeight;
	let Scale = task.Scale;

	WeightScaleRejects.findOne({CartonID: Pdoc.orders[0].cartonID}, function(error, doc){
		if(!error){
			if(doc){
				doc.TransporterID = TransporterID;
				doc.RejectReason = reason;
				doc.ActualScaleWeight = actualWeight;
				doc.QcScaleUsed = Scale;
				doc.DateRejected = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');

				doc.save(function(err, savedDoc){});
			} else {
				let NewRec = new WeightScaleRejects({
					TransporterID           : TransporterID,
					CartonID                : Pdoc.orders[0].cartonID,
					RejectReason            : reason,
					ExpectedMinWeight       : Pdoc.orders[0].minWeightLimit.toFixed(2),
					ExpectedMaxWeight       : Pdoc.orders[0].maxWeightLimit.toFixed(2),
					CartonCalculatedWeight  : Pdoc.orders[0].picklistWeight.toFixed(2),
					ActualScaleWeight       : actualWeight,
					QcScaleUsed				: Scale,
					DateRejected            : moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
					processed               : false
				});

				NewRec.save(function(err, savedDoc){});
			}
		}

		return callback();
	});
} /* QCUpdateEachTrCarton */

function UpdateQcRejectView(containerID, actualWeight, reason, Scale){
	if(containerID){
		if(parseInt(containerID[0]) != 7){
			WeightScaleRejects.findOne({CartonID: containerID}, function(error, doc){
				if(!error){
					if(doc){
						doc.TransporterID = '';
						doc.RejectReason = reason;
						doc.ActualScaleWeight = actualWeight;
						doc.QcScaleUsed = Scale;
						doc.DateRejected = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');

						doc.save(function(err, savedDoc){});
					} else {
						pickList.findOne({'orders.cartonID':containerID}, function(pickError, pickDoc){
							if(!pickError && pickDoc){
								let NewRec = new WeightScaleRejects({
									TransporterID           : '',
									CartonID                : containerID,
									RejectReason            : reason,
									ExpectedMinWeight       : pickDoc.orders[0].minWeightLimit.toFixed(2),
									ExpectedMaxWeight       : pickDoc.orders[0].maxWeightLimit.toFixed(2),
									CartonCalculatedWeight  : pickDoc.orders[0].picklistWeight.toFixed(2),
									ActualScaleWeight       : actualWeight,
									QcScaleUsed				: Scale,
									DateRejected            : moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
									processed               : false
								});

								NewRec.save(function(err, savedDoc){});
							}
						});
					}
				}
			});
		} else {
			pickList.find({'transporterID':containerID, 'Status': 'PACKED'}, function(pickError, pickDocs){
				if(!pickError && pickDocs.length > 0){
					var qQCUpdateEachTrCarton = async.queue(QCUpdateEachTrCarton, pickDocs.length);
					qQCUpdateEachTrCarton.drain = function(){}

					let x = 0;
					while(x < pickDocs.length){
						qQCUpdateEachTrCarton.push({Doc: pickDocs[x], containerID: containerID, actualWeight: actualWeight, reason: reason, Scale: Scale}, function(){});
						x++;
					}
				}
			});
		}
	}
} /* UpdateQcRejectView */

function UpdateQcCarton(plDoc, containerID, ReceivedWeight, Rejected, RejectMsg, callback){
	if(!plDoc){
		return callback();
	}

	plDoc.orders[0].QcChecked = (Rejected == true)? false: true;
	plDoc.orders[0].QcCheckOK = (Rejected == true)? false: true;
	plDoc.orders[0].LastQcCheckTime = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
	plDoc.orders[0].QcCheckResult = (Rejected == true)? RejectMsg: 'Inline Scale Weight Passed';

	plDoc.save(function(err, savedDoc){
		return callback();
	});
} /* UpdateQcCarton */

function UpdateSingleCarton(containerID, ReceivedWeight, Rejected, RejectMsg){
	pickList.findOne({'orders.cartonID':containerID}, function(plErr, plDoc){
		if(plErr || !plDoc){
			return;
		}

		UpdateQcCarton(plDoc, containerID, ReceivedWeight, Rejected, RejectMsg, function(){
			return;
		});
	});
} /* UpdateSingleCarton */

function RecursiveUpdateTransporterCartons(plDocArr, x, containerID, ReceivedWeight, Rejected, RejectMsg, callback){
	if(x >= plDocArr.length){
		return callback();
	}

	UpdateQcCarton(plDocArr[x], containerID, ReceivedWeight, Rejected, RejectMsg, function(){
		x++;
		return RecursiveUpdateTransporterCartons(plDocArr, x, containerID, ReceivedWeight, Rejected, RejectMsg, callback);
	});
} /* RecursiveUpdateTransporterCartons */

function UpdateTransporterCartons(containerID, ReceivedWeight, Rejected, RejectMsg){
	pickList.find({'transporterID':containerID, 'Status': 'PACKED'}, function(plErr, plDocArr){
		if(plErr || !plDocArr || plDocArr.length <= 0){
			return;
		}

		RecursiveUpdateTransporterCartons(plDocArr, 0, containerID, ReceivedWeight, Rejected, RejectMsg, function(){
			return;
		});
	});
} /* UpdateTransporterCartons */

function UpdateContainerAtQC(containerID, ReceivedWeight, Rejected, RejectMsg){
	if(!containerID){
		return;
	}

	if(parseInt(containerID[0]) != 7){
		return UpdateSingleCarton(containerID, ReceivedWeight, Rejected, RejectMsg);
	}

	return UpdateTransporterCartons(containerID, ReceivedWeight, Rejected, RejectMsg);
} /* UpdateContainerAtQC */

function GetBestChuteForPutwall(ChuteArr){
	let x = 0;
	let BestChuteNdx = null;
	while(x < ChuteArr.length){
		if(BestChuteNdx == null){
			BestChuteNdx = x;
		} else {
			if(ChuteArr[x].qty < ChuteArr[BestChuteNdx].qty){
				BestChuteNdx = x;
			}
		}
		x++;
	}

	return ChuteArr[BestChuteNdx].chuteNo;
} /* GetBestChuteForPutwall */

function GetBestChuteForNormalChute(ChuteArr){
	let x = 0;
	let BestChuteNdx = null;
	while(x < ChuteArr.length){
		if(ChuteArr[x].qty < ChuteArr[x].max){
			if(BestChuteNdx == null){
				BestChuteNdx = x;
			} else {
				if(ChuteArr[x].qty < ChuteArr[BestChuteNdx].qty){
					BestChuteNdx = x;
				}
			}
		}
		x++;
	}

	if(BestChuteNdx){
		return ChuteArr[BestChuteNdx].chuteNo;
	} else {
		x = ChuteArr.length-1;
		BestChuteNdx = null;
		while(x >= 0){
			if(BestChuteNdx == null){
				BestChuteNdx = x;
			} else {
				if(ChuteArr[x].qty < ChuteArr[BestChuteNdx].qty){
					BestChuteNdx = x;
				}
			}
			x--;
		}

		return ChuteArr[BestChuteNdx].chuteNo;
	}
} /* GetBestChuteForNormalChute */

function GetOrUpdateToteChuteLocation(tote, scanner, callback) {
  	if((parseInt(tote[0]) != 0 && parseInt(tote[0]) != 1) || tote.length != 7){
    	return callback(19);
  	} else {
		pickList.find({'toteID': tote}).limit(1).exec(function(err, PickListArr){
			let PickList = null;
			let IntRoute = null;

			if(PickListArr && PickListArr.length > 0){
				PickList = PickListArr[0];
			}

			if(PickList){
				if(PickList.containerLocation != null && PickList.containerLocation != ""){
					IntRoute = parseInt(PickList.containerLocation);
				}

				if(IntRoute && IntRoute >= scanner){
					return callback(IntRoute);
				} else {
					let isPutwall = false;
					if(PickList.packType == 'PUT'){
						isPutwall = true;
					}

					chutesQty.find({'chuteNo':{ $gte: scanner }, 'active': true, 'putwall': isPutwall}, function(err, ChutesArr){
						if(ChutesArr && ChutesArr.length > 0){
							let BestChute = null;

							if(isPutwall == true){
								BestChute = GetBestChuteForPutwall(ChutesArr);
							} else {
								BestChute = GetBestChuteForNormalChute(ChutesArr);
							}

							let StrChute = BestChute.toString();
							PickList.containerLocation = StrChute;
							PickList.Packing = true;
							PickList.save(function(err, savedDoc){});
							return callback(BestChute);
						} else {
							chutesQty.find({'chuteNo':{ $gte: scanner }, 'putwall': isPutwall}, function(err, ChutesArr){
								if(ChutesArr && ChutesArr.length > 0){
									let BestChute = null;

									if(isPutwall == true){
										BestChute = GetBestChuteForPutwall(ChutesArr);
									} else {
										BestChute = GetBestChuteForNormalChute(ChutesArr);
									}

									let StrChute = BestChute.toString();
									PickList.containerLocation = StrChute;
									PickList.Packing = true;
									PickList.save(function(err, savedDoc){});
									return callback(BestChute);
								} else {
									return callback(19);
								}
							});
						}
					});
				}
			} else {
				return callback(19);
			}
		});
	}
} /* GetOrUpdateToteChuteLocation */

function CalculateNextAvailableChute(PickList, Sindex, callback) {
  var ndx = Sindex -1
  Sindex = ndx
  var ChuteArr = [{active: false, qty:0, max:0, putwall:false},
                  {active: false, qty:0, max:0, putwall:false},
                  {active: false, qty:0, max:0, putwall:false},
                  {active: false, qty:0, max:0, putwall:false},
                  {active: false, qty:0, max:0, putwall:false},
                  {active: false, qty:0, max:0, putwall:false},
                  {active: false, qty:0, max:0, putwall:false},
                  {active: false, qty:0, max:0, putwall:false},
                  {active: false, qty:0, max:0, putwall:false},
                  {active: false, qty:0, max:0, putwall:false},
                  {active: false, qty:0, max:0, putwall:false},
                  {active: false, qty:0, max:0, putwall:false},
                  {active: false, qty:0, max:0, putwall:false},
                  {active: false, qty:0, max:0, putwall:false},
                  {active: false, qty:0, max:0, putwall:false},
                  {active: false, qty:0, max:0, putwall:false},
                  {active: false, qty:0, max:0, putwall:false},
                  {active: false, qty:0, max:0, putwall:false}]

  GetChuteQtyForIndex(Sindex, ChuteArr, function(ResultArr){
    var BestChute = null
    var x = ndx

    console.log('CLV: Processing tote ' + PickList.toteID + ' packType= ' + PickList.packType)

    if(PickList.packType == 'PUT'){
      while(x < ResultArr.length){
        if(ResultArr[x].putwall == true) {
            if(BestChute == null){
              BestChute = x
            } else {
              if(ResultArr[x].qty < ResultArr[BestChute].qty){
                BestChute = x
              }
            }
        }
        x++
      }
    }

    if(BestChute != null){
      var pch = (BestChute+1)
      console.log('CLV: PUT WALL suitable tote ' + PickList.toteID + ' going to chute ' + pch)
      return callback(pch)
    } else {
      BestChute = null
	  x = ndx
	  while(x < ResultArr.length){
	    if(ResultArr[x].active == true && ResultArr[x].putwall == false) {
		  if(ResultArr[x].qty < ResultArr[x].max){
		    if(BestChute == null){
			  BestChute = x
		    } else {
			  if(ResultArr[x].qty < ResultArr[BestChute].qty){
			    BestChute = x
			  }
		    }
		  }
	    }
	    x++
	  }

      if(BestChute == null){
         console.log('CLV: no suitable chute, using round robin')
         var Num = ndx+1
         BestChute = null
         while(Num < 19){
           if(LastUsedChute == null || LastUsedChute < 0){
             LastUsedChute = 0
           }

           if(ResultArr[Num-1].putwall == false){
             if(ResultArr[Num-1].qty < ResultArr[Num-1].max){
               LastUsedChute = Num
               if(LastUsedChute >= 18){
                 LastUsedChute = 0
               }
               BestChute = Num
               break;
             }
           }
           Num++
         }

         if(BestChute == null){
           BestChute = 19
           console.log('CLV: Tote ' + PickList.toteID + ' going to chute ' + BestChute)
         } else {
           console.log('CLV:  Tote ' + PickList.toteID + ' going to chute ' + BestChute)
         }

         return callback(BestChute)
      } else {
        var ch = (BestChute+1)
        console.log('CLV: CHUTE FOUND!! Tote ' + PickList.toteID + ' going to chute ' + ch)
        return callback(ch)
      }
    }
  })
}

function GetChuteQtyForIndex(index, ChuteArr, callback){
  if(index < ChuteArr.length) {
    packStations.find({'schuteNumber': (index+1)}, function(error, Stations){
       if(error || Stations.length <= 0) {
         console.log('CLV: Error while find chute no ' + (index+1) + ' pack stations')
         index++
         GetChuteQtyForIndex(index, ChuteArr, callback)
       } else {
         var x = 0
         var count = 0;
         var hasAutobag = false
         while(x < Stations.length){
           if(Stations[x].active) {
             count++
           }

           if(Stations[x].autobagPrinterQName != null){
             hasAutobag = true
           }
           x++
         }

         ChuteArr[index].max = 7

         if(count > 0){
           ChuteArr[index].active = true
         }

         ChuteArr[index].putwall = hasAutobag

         var ChuteNdx = index+1
         var StrChute = ChuteNdx.toString()

         pickList.find({'containerLocation': StrChute}, function(err, Totes){
           if(err){
             console.log('CLV: Error while find chute no ' + StrChute + ' no of totes')
             ChuteArr[index].qty = 0
           } else {
             if(Totes.length > 0) {
               ChuteArr[index].qty = Totes.length
             } else {
               ChuteArr[index].qty = 0
             }
           }

           ChuteArr[index].max = 7

           index++
           GetChuteQtyForIndex(index, ChuteArr, callback)
         })
       }
    })
  } else {
    console.log('CLV: Map--> ' + JSON.stringify(ChuteArr))
    return callback(ChuteArr)
  }
}

// Get the tote barcode.
function getToteBarcode (barcodeArray) {
    var barcode = ''
    for (let i = 2; i < 10; i++) {
        if (barcodeArray[i] !== 0) {
            var hex = parseInt(barcodeArray[i]).toString(16)
            var ascii = hex2Ascii(hex)
            barcode += ascii
        }
    }
    barcode = barcode.substring(0, 7)

    if (parseInt(barcode) === 0) return null

    return barcode
}

function getToteBarcode1(Location, barcodeArray) {
    var barcode = ''
    for(let i = 2; i < 10; i++) {
    	if(barcodeArray[i] !== 0) {
        	var hex = parseInt(barcodeArray[i]).toString(16);
            var ascii = hex2Ascii(hex);
            barcode += ascii;
        }
    }

    barcode = barcode.substring(0, 7);
    //console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ----->>' + Location + ': Barcode [' + barcode + ']');

    if(parseInt(barcode) === 0){
		return null;
	}

    return barcode;
} /* getToteBarcode1 */

// Convert hex to ascii. Returns ascii value.
function hex2Ascii (hexValue) {
    var hex = hexValue.toString() // Force conversion
    var str = ''
    for (var i = 0; i < hex.length; i += 2) {
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16))
    }
    return str
}

function GetNextValidTote(callback){
	pickList.find({'toteID': null, 'Status': 'NEW', 'priorityLevel': {$in: [10, null]}}, function (error, docNullTote){
		if(error || !docNullTote || docNullTote.length <= 0){
			return callback({ToteData: null});
		}

		let x = 0;
		while(x < docNullTote.length){
			let PL = docNullTote[x];
			if(PL.PickType == null || PL.PickType == 'PARTIAL'){
				if(PL.PickRegion == null || PL.PickRegion == '311PTL'){
					return callback({ToteData: PL});
				}
			}
			x++;
		}

		return callback({ToteData: null});
	}).sort({"startTime" : 1 });
} /* GetNextValidTote */

function GetNextValidPriorityTote(callback){
	pickList.find({'toteID': null, 'Status': 'NEW', 'priorityLevel': {$ne: null}, 'priorityLevel': {$lt: 10}}, function (error, docNullTote){
		if(error || !docNullTote || docNullTote.length <= 0){
			return callback({ToteData: null});
		}

		let x = 0;
		while(x < docNullTote.length){
			let PL = docNullTote[x];
			if(PL.PickType == null || PL.PickType == 'PARTIAL'){
				if(PL.PickRegion == null || PL.PickRegion == '311PTL'){
					return callback({ToteData: PL});
				}
			}
			x++;
		}

		return callback({ToteData: null});
	}).sort({"priorityLevel" : 1, "startTime" : 1 });
} /* GetNextValidPriorityTote */

// Find a picklist to associate with a tote.
// Also finds its next destination.
function findOrAssociatePicklist (toteID, address, scannerID, scannerArr, callback) {
    pickList.findOne({'toteID': toteID}, function (err, doc) {
        if(doc){
            if(address == 'D5000' && parseInt(doc.FromTote) >= 3){
                  doc.FromTote = null;
                  let msg = 'Maximum recirculation reached';
                  console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' LEF: TOTE REJECTING.. ' + toteID + ' MAX RECIRC REACHED');
                  moduleIndex.fins.writeSync(finsClientObject, address, [1, 20]);
                  doc.save(function(er, sdc){});
                  rejectTote(toteID, msg);
                  return callback();
             }

            let nextDestination = findNextDestination(toteID, doc, scannerID, scannerArr);

            moduleIndex.fins.writeSync(finsClientObject, address, [1, nextDestination.destination]); // Ack and send destination.
            if(address == 'D5090'){
               console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss:SSS') + ' LEF1: Tote ' + toteID + ' next destination is ' + nextDestination.destination);
            } else {
               console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss:SSS') + ' LEF: Tote ' + toteID + ' next destination is ' + nextDestination.destination);
	    	}

            return callback();
        } else {
            if(address == 'D5090'){
				console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss:SSS') + ' LEF1: Empty Tote ' + toteID + ' at Recirc -- Recirculating...');
                moduleIndex.fins.writeSync(finsClientObject, address, [1, 22]);
                return callback();
            }

            if((toteID[0] != '0' && toteID[0] != '1') || toteID.length != 7){
				console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss:SSS') + ' LEF1: Invalid Tote ID ' + toteID + ' Found on conveyor - does not match tote ID standards');
                moduleIndex.fins.writeSync(finsClientObject, address, [1, 22]);
                return callback();
			}

            parameters.findOne({'toteConsolidation.consolidationInProgress': false}, function (err, parameterDoc) {
            	if(err){
					console.log('Error while checking the consolidation in progress state');
					moduleIndex.fins.writeSync(finsClientObject, address, [1, 22]); // Ack and send destination.
					return callback();
				}

                if(!parameterDoc){
                  	console.log('Not associating tote ' + toteID + ' pickilist. Tote consolidation in progress');
                    moduleIndex.fins.writeSync(finsClientObject, address, [1, 22]); // Ack and send destination.

                    return callback();
                }

				if(toteID[0] == '1'){
					GetNextValidPriorityTote(function(NextToteData){
						if(!NextToteData.ToteData){
							/* If empty picklist found. */
							console.log('No Priority PickList Available to Assign');
							moduleIndex.fins.writeSync(finsClientObject, address, [1, 22]);
							return callback();
						}

						let docNullTote = NextToteData.ToteData;
						console.log('ASIGNING PRIORITY TOTE: ' + toteID);
						docNullTote.toteID = toteID;
						docNullTote.Status = 'STARTED';
						docNullTote.save(function(error1, savedDoc){
							if(!error1 && savedDoc) {
								let nextDestination = findNextDestination(toteID, savedDoc, scannerID, scannerArr);
								moduleIndex.fins.writeSync(finsClientObject, address, [1, nextDestination.destination]); // Ack and send destination.
								console.log('LEF: Tote ' + toteID + ' next destination is ' + nextDestination.destination);
							} else {
								console.log('Error while trying to associate picklist with tote ' + toteID);
								moduleIndex.fins.writeSync(finsClientObject, address, [1, 22]); // Ack and send destination.
							}

							return callback();
						});
					});
				} else {
					GetNextValidTote(function(NextToteData){
						if(!NextToteData.ToteData){
							/* If empty picklist found. */
							console.log('No PickList Available to Assign');
							moduleIndex.fins.writeSync(finsClientObject, address, [1, 22]);
							return callback();
						}

						let docNullTote = NextToteData.ToteData;
						console.log('WE ARE ASIGNING!!!: ' + toteID);
						docNullTote.toteID = toteID;
						docNullTote.Status = 'STARTED';
						docNullTote.save(function(error1, savedDoc){
							if(!error1 && savedDoc) {
								let nextDestination = findNextDestination(toteID, savedDoc, scannerID, scannerArr);
								moduleIndex.fins.writeSync(finsClientObject, address, [1, nextDestination.destination]); // Ack and send destination.
								console.log('LEF: Tote ' + toteID + ' next destination is ' + nextDestination.destination);
							} else {
								console.log('Error while trying to associate picklist with tote ' + toteID);
								moduleIndex.fins.writeSync(finsClientObject, address, [1, 22]); // Ack and send destination.
							}

							return callback();
						});
					});
				}
            });
        }
    });
}

function processChute(chuteID, toteID){
    if(chuteID !== null){
        ChuteModel.findOne({chuteID:chuteID}, function(error, docs){
            if(!error && docs){
                var x = 0
                while(x < docs.Totes.length){
                    if(toteID == docs.Totes[x].toteID){
                        docs.Totes[x].processed = true
                        break
                    }
                    x++
                }

                docs.save(function(error, data){
                    if(!error && data){
                        console.log('Tote ID: ' + toteID + ' Has been processed in Chute: ' + chuteID)
                    }else{
                        console.log('Tote ID: ' + toteID + ' Has been NOT processed from Chute: ' + chuteID)
                    }
                })
            }else{
                console.log('Tote ID: ' + toteID + ' Has been NOT processed from Chute: ' + chuteID)
            }
        })
    }
}

function updateToteDestination (toteID, destToUpdate) {
    pickList.findOne({'toteID': toteID}, function (err, doc) {
       if(!err && doc) {
         doc.destination = destToUpdate
         doc.containerLocation = null
         doc.save(function(error, savedDoc){
            if(!error && savedDoc)
              console.log('Tote ' + savedDoc.toteID + ' now heading to scanner ' + savedDoc.destination)
         })
       } else {
         console.log('Error while trying to update tote ' + toteID + ' to destination ' + destToUpdate)
       }
    });
}

// Update a picklist to fully picked.
function updatePicklistToPicked (toteID, callback) {

    let SomethingChanged = false
    pickList.findOne({'toteID': toteID}, function (err, doc) {
        if (!err && doc)
        {
          let x = 0
          let count = 0;
          //processChute(doc.containerLocation, toteID)
          while(x < doc.orders.length) {
            if (doc.orders[x].picked === false) {
               doc.orders[x].picked = true
               SomethingChanged = true
            }

            x++
          }

          doc.containerLocation = null
          if(SomethingChanged){
            doc.PickUpdateRequired = true
            doc.Status = 'PICKED'
            doc.FromTote = null;
          }

          doc.save(function(err, savedDoc){
             if(!err && savedDoc) {
              	return callback();
             }
          })
        } else {
			return callback();
		}
    });
}

// Find the next destination of a tote.
function findNextDestination (toteID, cDoc, scannerID, scannerArr) {
    let destResult = {fullyPicked: false, destination: null}

    let orders = cDoc.orders;
    let nextDest = 999;
    let ZoneFound = false;
    let NotPicked = false;

    // Iterate through all the orders.
    let i = 0;
    while (i < orders.length) {
        // Iterate through all the items in an order.
        let j = 0;
        while(j < orders[i].items.length) {
            let item = orders[i].items[j];

            if (item.picked == false) {
               NotPicked = true;
               if(scannerArr.length > 0){
                 let x = 0;
                 let Max = scannerArr.length;
                 let IntPickZone = parseInt(item.pickZone);
              	 destResult.destination = item.pickZone;
                 while(x < Max){
                   if(scannerArr[x] > 0 && IntPickZone == scannerArr[x]){
                     console.log('LEF: shortest route found for tote ' + toteID + ' at zone ' + scannerArr[x]);
                     destResult.fullyPicked = false;
                     destResult.destination = item.pickZone;
                     ZoneFound = true;
                     break;
                   }
                   x++;
                 }
               }
            }

           if(ZoneFound){
             break;
           }
           j++;
        }

       if(ZoneFound){
         break;
       }
       i++;
    }

    if(ZoneFound == false){
    	if(!NotPicked){
      		if(orders.length > 0 && i >= orders.length){
        		destResult.fullyPicked = true;
        	}
      	}
    } else {
	  let x = 0;
	  while(x < cDoc.orders.length){
		cDoc.orders[x].picked = false;
		x++;
	  }

	  cDoc.save(function(err, saveDoc){});
    }

    if(destResult.fullyPicked == true) {
        if(scannerID === 10){
           console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss:SSS') + ' LEF1: Fully picked --- ' + toteID);
           destResult.destination = 20;
           updatePicklistToPicked(toteID, function(){});
        } else {
           destResult.destination = 22;
        }
        return destResult;
    }

    if (scannerID === 10 && !destResult.fullyPicked) {
        destResult.destination = 22
        let Count = 0;
        if(cDoc.FromTote != null && !isNaN(cDoc.FromTote)){
            Count = parseInt(cDoc.FromTote);
        }

		Count += 1;
        cDoc.FromTote = Count.toString();
        cDoc.containerLocation = null;
        console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss:SSS') + ' LEF1: Not fully picked --- ' + toteID + ' | Recirc Count ' + Count);

        cDoc.save(function(err, saveDoc){});
    }

    return destResult;
}

function setToManifest (toteID) {
    pickList.findOne({'toteID': toteID}, function (err, doc) {
        if (!err && doc) { // If picklist found.
          doc.destination = 18
        } else {
          console.log('Error while trying to set tote ' + toteID + ' to manifest')
        }
    })
}

function requestPicking (location, toteID) {
    restler.post('http://localhost:3000/api/picktote', {
        data: { Location: location, ToteId: toteID }
    }).on('complete', function (result) {
       if(result instanceof Error) {
          console.log(result);
       } else {
         setToManifest(toteID)
       }
    });
} /* requestPicking */

// Get the container's barcode.
function getCourierContainerBarcode (barcodeData) {
    var barcode = ''
    for (let i = 2; i < barcodeData.length; i++) {
        if (barcodeData[i] !== 0) {
            var hex = parseInt(barcodeData[i]).toString(16)
            var ascii = hex2Ascii(hex)
            barcode += ascii
        }
    }

    // Labels can have multiple barcodes.
    // We split them via the ; char.
    var barcodeArray = barcode.split(';')

    console.log('Barcode Array: ' + barcodeArray)

    for (var i = 0; i < barcodeArray.length; i++) {
        if (barcodeArray[i].indexOf('NTM') !== -1) {
            barcode = barcodeArray[i].replace('NTM', '')
	    console.log('NTM Barcode Found: ' + barcodeArray[i])
	    break
        }
	if (barcodeArray[i].length === 8) {
	    barcode = barcodeArray[i]
	    console.log('Raw Barcode Found: ' + barcodeArray[i])
	    break
	}
	if (barcodeArray[i].indexOf('_') >= -1) {
	    barcode = ''
	    console.log('Underscore Barcode Found: ' + barcodeArray[i])
        }
    }

    // Finally we return an array of barcodes.
    return barcode
}

function getCourierContainerBarcode1(Location, barcodeData) {
    let barcode = '';

    for (let i = 2; i < barcodeData.length; i++) {
        if (barcodeData[i] !== 0) {
            var hex = parseInt(barcodeData[i]).toString(16);
            var ascii = hex2Ascii(hex);
            barcode += ascii;
        }
    }

    // Labels can have multiple barcodes.
    // We split them via the ; char.
    let barcodeArray = barcode.split(';');
    let barcodeArr = [];
    let Lbarcode = null;

	console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ----->>' + Location + ': Barcode Array [' + barcodeArray + ']');

    for (var i = 0; i < barcodeArray.length; i++) {
        if (barcodeArray[i].indexOf('NTM') !== -1) {
            if(!Lbarcode){
                Lbarcode = barcodeArray[i].replace('NTM', '');

            }
            barcodeArr.push(barcodeArray[i].replace('NTM', ''));

            console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ----->>' + Location + ': NTM Barcode Found [' + barcodeArray[i] + ']');
	    	//break;
        }

		if (barcodeArray[i].length === 8) {
            if(!Lbarcode){
                Lbarcode = barcodeArray[i];

            }
	    	barcodeArr.push(barcodeArray[i]);

	    	console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ----->>' + Location + ': Raw Barcode Found [' + barcodeArray[i] + ']');
	    	//break;
		}

		if (barcodeArray[i].indexOf('_') >= -1) {
            if(!Lbarcode){
               Lbarcode = '';
            }

	    	console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ----->>' + Location + ': Underscore Barcode Found [' + barcodeArray[i] + ']');
        }
    }

    // Finally we return an array of barcodes.
    return {barcode: Lbarcode, barcodeArr: barcodeArr};
} /* getCourierContainerBarcode1 */

function InLineScaleCheckCarton(ReplyRegister, Scale, containerID, ReceivedWeight, callback){
	CompareWeights(containerID, ReceivedWeight, function(Resp){
		if(Resp.Reject){
			let Rj = null;
			if(Resp.Reason){
				Rj = Resp.Reason;
				console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '------<<Scale ' + Scale + ': [1, 2] Carton ID: ' + Resp.containerID + ' Rejected. ' + Resp.Reason);
			} else {
				Rj = 'Carton Weight not within tolerance';
				console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '------<<Scale ' + Scale + ': [1, 2] | Carton ID: ' + Resp.containerID + ' Rejected. Weight not within tolerance. ' + ' Lower Limit [' + Resp.Lower + '] | Actual Weight [' + ReceivedWeight + '] | Upper Limit [' + Resp.Upper + ']');
			}

			UpdateQcRejectView(containerID, ReceivedWeight, Rj, 1);
			moduleIndex.fins.writeSync(finsClientObject, ReplyRegister, [1, 2]);
		} else {
			console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '------<<Scale ' + Scale + ': [1, 1] | Carton ID: ' + Resp.containerID + ' weight passed. ' + ' Lower Limit [' + Resp.Lower + '] | Actual Weight [' + ReceivedWeight + '] | Upper Limit [' + Resp.Upper + ']');
			moduleIndex.fins.writeSync(finsClientObject, ReplyRegister, [1, 1]);
		}

		UpdateContainerAtQC(containerID, ReceivedWeight, Resp.Reject, Resp.Reason);
		return callback();
	});
} /* InLineScaleCheckCarton */

function InLineScaleCheckTransporter(ReplyRegister, Scale, containerID, ReceivedWeight, callback){
	CompareTransporterWeights(containerID, ReceivedWeight, function (Resp) {
		if(Resp.Reject){
			let Rj = null;
			if(Resp.Reason){
				Rj = Resp.Reason;
				console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '------<<Scale ' + Scale + ': [1, 2] Transporter ID: ' + Resp.containerID + ' Rejected. ' + Resp.Reason);
			} else {
				Rj = 'Transporter Weight not within tolerance';
				console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '------<<Scale ' + Scale + ': [1, 2] | Transporter ID: ' + Resp.containerID + ' Rejected. Weight not within tolerance. ' + ' Lower Limit [' + Resp.Lower + '] | Actual Weight [' + ReceivedWeight + '] | Upper Limit [' + Resp.Upper + ']');
			}

			UpdateQcRejectView(containerID, ReceivedWeight, Rj, 1);
			moduleIndex.fins.writeSync(finsClientObject, ReplyRegister, [1, 2]);
		} else {
			console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '------<<Scale ' + Scale + ': [1, 1] | Carton ID: ' + Resp.containerID + ' weight passed. ' + ' Lower Limit [' + Resp.Lower + '] | Actual Weight [' + ReceivedWeight + '] | Upper Limit [' + Resp.Upper + ']');
			moduleIndex.fins.writeSync(finsClientObject, ReplyRegister, [1, 1]);
		}

		UpdateContainerAtQC(containerID, ReceivedWeight, Resp.Reject, Resp.Reason);
		return callback();
	});
} /* InLineScaleCheckTransporter */

function GetWeightFromScale1(ScaleIP, ScalePort, containerID){
	let Count = 0;
	let WeightReceived = false;
	let client = new net.Socket();
	client.setTimeout(1000);
	client.setEncoding('utf8');
	client.connect(ScalePort, ScaleIP, function (err) {
		if(err){
			console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + 'Scale 1 Failed to connect. Err: ' + err);
			ScaleScanner1Read = false;
		}else{
			//console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + 'Scale 1 connected');
		}
	});

	client.on('data', function (data){
		if(!WeightReceived){
			WeightReceived = true;
			let MyWeight1 = data.toString();
			MyWeight1 = fixWeight(MyWeight1);
			console.log('Weight 1 Received: ' + MyWeight1);
			client.destroy();

			if(isNaN(MyWeight1)){
				moduleIndex.fins.writeSync(finsClientObject, 'D5330', [1, 2]);
				let RejectMsg = 'Invalid weight [' + MyWeight1 + '] read from scale';
				lastReadScale1 = null;
				UpdateQcRejectView(containerID, 0, RejectMsg, 1);
				UpdateContainerAtQC(containerID, 0, true, RejectMsg);
				ScaleScanner1Read = false;
				console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '------<<Scale 1: [1, 2] | Container ID: ' + containerID + ' rejected. ' + RejectMsg);
			} else {
				qProcWeightScale1.push({containerID: containerID, weight1: MyWeight1}, function(err, data){
					ScaleScanner1Read = false;
				});
			}
		}
	});

	client.on('close', function(){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + 'Scale 1 Connection Closed');
	});

    client.on('timeout', function () {
        console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + 'Scale 1 Connection timed out');
        ScaleScanner1Read = false;
    });

	client.on('error', function(err){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + 'Scale 1 Connection Error: ' + err);
		ScaleScanner1Read = false;
	});
} /* GetWeightFromScale1 */

function GetWeightFromScale2(ScaleIP, ScalePort, containerID){
	let Count = 0;
	let WeightReceived = false;
	let client = new net.Socket();
	client.setTimeout(1000);
	client.setEncoding('utf8');
	client.connect(ScalePort, ScaleIP, function (err) {
		if(err){
			console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + 'Scale 2 Failed to connect. Err: ' + err);
			ScaleScanner2Read = false;
		}else{
			//console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + 'Scale 2 connected');
		}
	});

	client.on('data', function (data){
		if(!WeightReceived){
			WeightReceived = true;
			let MyWeight2 = data.toString();
			MyWeight2 = fixWeight(MyWeight2);
			console.log('Weight 1 Received: ' + MyWeight2);
			client.destroy();

			if(isNaN(MyWeight2)){
				moduleIndex.fins.writeSync(finsClientObject, 'D5340', [1, 2]);
				let RejectMsg = 'Invalid weight [' + MyWeight2 + '] read from scale';
				lastReadScale2 = null;
				UpdateQcRejectView(containerID, 0, RejectMsg, 2);
				UpdateContainerAtQC(containerID, 0, true, RejectMsg);
				ScaleScanner2Read = false;
				console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '------<<Scale 2: [1, 2] | Container ID: ' + containerID + ' rejected. ' + RejectMsg);
			} else {
				qProcWeightScale2.push({containerID: containerID, weight2: MyWeight2}, function(err, data){
					ScaleScanner2Read = false;
				});
			}
		}
	});

	client.on('close', function(){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + 'Scale 2 Connection Closed');
	});

    client.on('timeout', function () {
        console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + 'Scale 2 Connection timed out');
        ScaleScanner2Read = false;
    });

	client.on('error', function(err){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + 'Scale 2 Connection Error: ' + err);
		ScaleScanner2Read = false;
	});
} /* GetWeightFromScale2 */

function ProcWeightScale1(task, callback){
	let containerID = task.containerID;
	let ReceivedWeight1 = task.weight1;

	if(parseInt(containerID[0]) != 7){
		InLineScaleCheckCarton('D5330', 1, containerID, ReceivedWeight1, function(){
			return callback(null, containerID);
		});
		return;
	}

	InLineScaleCheckTransporter('D5330', 1, containerID, ReceivedWeight1, function(){
		return callback(null, containerID);
	});
} /* ProcWeightScale1 */

function ProcWeightScale2(task, callback){
	let containerID = task.containerID;
	let ReceivedWeight2 = task.weight2;

	if(parseInt(containerID[0]) != 7){
		InLineScaleCheckCarton('D5340', 2, containerID, ReceivedWeight2, function(){
			return callback(null, containerID);
		});
		return;
	}

	InLineScaleCheckTransporter('D5340', 2, containerID, ReceivedWeight2, function(){
		return callback(null, containerID);
	});
} /* ProcWeightScale2 */

function UpdateCourierView(PL, CheckedDate, Reason, callback){
	CourierRejects.findOne({'CartonID': PL.toteID}, function(err, Crv){
		if(err){
			return callback();
		}

		if(!Crv){
			let RejectReason = [];
			RejectReason.push({Date: CheckedDate, Reason: Reason, Checked: false, CheckedBy: null});

			let NewRec = new CourierRejects({
				TransporterID : PL.transporterID,
				CartonID      : PL.toteID,
				RejectReason  : RejectReason
			});

			NewRec.save(function(err, savedDoc){
				return callback();
			});
			return;
		}

		Crv.RejectReason.push({Date: CheckedDate, Reason: Reason, Checked: false, CheckedBy: null});
		Crv.save(function(err, savedDoc){
			return callback();
		});
	});
} /* UpdateCourierView */

function SetDecPointChecked(DecPointPL, Rejected, Reason, callback){
	DecPointPL.orders[0].CourierDecPointChecked = (Rejected == true)? false: true;
	DecPointPL.orders[0].CourierDecPointCheckedBy = (Rejected == true)? '': 'SYSTEM';
	DecPointPL.orders[0].CourierDecPointRejected = Rejected;
	DecPointPL.orders[0].CourierDecPointRejectReason = (Rejected == true)? Reason: '';

	let CheckedDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
	DecPointPL.orders[0].LastCourierDecPointTime = CheckedDate;

	DecPointPL.save(function(err, savedDoc){
		if(!Rejected){
			return callback();
		}

		UpdateCourierView(DecPointPL, CheckedDate, Reason, function(){
			return callback();
		});
	});
} /* SetDecPointChecked */

function UpdateCartonAtDecPoint(PL, Rejected, Reason, callback){
	pickList.findOne({toteID: PL.toteID}, function(err, DecPointPL){
		if(err || !DecPointPL){
			return callback();
		}

		SetDecPointChecked(DecPointPL, Rejected, Reason, function(){
			return callback();
		});
	});
} /* UpdateCartonAtDecPoint */

function SetDecPointTrChecked(DecPointPLArr, ndx, Rejected, Reason, callback){
	if(ndx >= DecPointPLArr.length){
		return callback();
	}

	let DecPointPL = DecPointPLArr[ndx];
	SetDecPointChecked(DecPointPL, Rejected, Reason, function(){
		ndx++;
		return SetDecPointTrChecked(DecPointPLArr, ndx, Rejected, Reason, callback);
	});
} /* SetDecPointTrChecked */

function UpdateTransporterAtDecPoint(PL, Rejected, Reason, callback){
	pickList.find({transporterID: PL.transporterID}, function(err, DecPointPLArr){
		if(err || !DecPointPLArr || DecPointPLArr.length <= 0){
			return callback();
		}

		SetDecPointTrChecked(DecPointPLArr, 0, Rejected, Reason, function(){
			return callback();
		});
	});
} /* UpdateTransporterAtDecPoint */

function UpdatePlAtDecPoint(PL, Rejected, Reason, callback){
	if(PL.transporterID){
		UpdateTransporterAtDecPoint(PL, Rejected, Reason, function(){
			return callback();
		});
		return
	}

	UpdateCartonAtDecPoint(PL, Rejected, Reason, function(){
		return callback();
	});
} /* UpdatePlAtDecPoint */

function CartonCanBeShipped(PL){
	if(!PL.orders[0].actualWeight1){
		let Msg = (!PL.transporterID)? 'Carton Has Not Been Weight At QC':'Transporter Has Not Been Weight At QC';
		return ({OkToShip: false, RejectReason: Msg});
	}

	if(!PL.orders[0].QcChecked){
		let Msg = (!PL.transporterID)? 'Carton Has Failed At QC And No Check Performed':'Transporter Has Failed At QC And No Check Performed';
		return ({OkToShip: false, RejectReason: Msg});
	}

	if(!PL.orders[0].QcCheckOK){
		let Msg = (!PL.transporterID)? 'Carton Has Failed At QC And QC Check Not Completed Successfully':'Transporter Has Failed At QC And QC Check Not Completed Successfully';
		return ({OkToShip: false, RejectReason: Msg});
	}

	return ({OkToShip: true});
} /* CartonCanBeShipped */

function DecideShipping(Type, err, containerID, PL, callback){
	if(err || !PL){
		moduleIndex.fins.writeSync(finsClientObject, 'D5350', [1, 4]);
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '-----<<DecPoint: [1, 4] - ' + Type + ' ID: ' + containerID + ' Rejected. Failed To Find Details For ' + Type + '. ' + err);
		return callback();
	}

	if(!PL.orders || PL.orders.length <= 0){
		moduleIndex.fins.writeSync(finsClientObject, 'D5350', [1, 4]);
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '-----<<DecPoint: [1, 4] - ' + Type + ' ID: ' + containerID + ' Rejected. Failed To Find Details For ' + Type + '. ');
		UpdatePlAtDecPoint(PL, true, 'Failed To Find Details For ' + Type, function(){});
		return callback();
	}

	let OkToShipObj = CartonCanBeShipped(PL);
	if(!OkToShipObj.OkToShip){
		moduleIndex.fins.writeSync(finsClientObject, 'D5350', [1, 4]);
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '-----<<DecPoint: [1, 4] - ' + Type + ' ID: ' + containerID + ' Rejected. ' + OkToShipObj.RejectReason);
		UpdatePlAtDecPoint(PL, true, OkToShipObj.RejectReason, function(){});
		return callback();
	}

	if(!PL.orders[0].courierID){
		moduleIndex.fins.writeSync(finsClientObject, 'D5350', [1, 4]);
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '-----<<DecPoint: [1, 4] - ' + Type + ' ID: ' + containerID + ' Rejected. No CourierID Found In PickList');
		UpdatePlAtDecPoint(PL, true, 'No CourierID Found In PickList', function(){});
		return callback();
	}

	if(PL.Status == 'CANCELED'){
		moduleIndex.fins.writeSync(finsClientObject, 'D5350', [1, 4]);
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '-----<<DecPoint: [1, 4] - ' + Type + ' ID: ' + containerID + ' Rejected. Order Is Cancelled');
		UpdatePlAtDecPoint(PL, true, 'Order Is Cancelled', function(){});
		return callback();
	}

	CanReq.findOne({'OrderId': PL.orders[0].orderID}, function(err, Req){
		if(Req){
			PL.Status = 'CANCELED';
			PL.orders[0].WmsOrderStatus = 'CANCELED';
			PL.orders[0].WmsPickListStatus = 'CANCELED';
			PL.orders[0].CancelDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			PL.orders[0].CancelInstruction = 'Fetch Parcel From Courier Reject Area';

			PL.save(function(err, PL){
				moduleIndex.fins.writeSync(finsClientObject, 'D5350', [1, 4]);
				console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '-----<<DecPoint: [1, 4] - ' + Type + ' ID: ' + containerID + ' Rejected. Order Is Cancelled');
				UpdatePlAtDecPoint(PL, true, 'Order Is Cancelled', function(){});
				return callback();
			});

			return;
		}

		GetCourierToRoute(PL.orders[0].courierID, function(Resp){
			moduleIndex.fins.writeSync(finsClientObject, 'D5350', [1, Resp.CourierRoute]);

			let Msg = moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '-----<<DecPoint: [1, ' + Resp.CourierRoute + '] - ' + Type + ' ID: ' + containerID;
			if(Resp.RejectReason){
				Msg = Msg + ' Rejected. ' + Resp.RejectReason;
				UpdatePlAtDecPoint(PL, true, Resp.RejectReason, function(){});
			} else {
				Msg = Msg + ' Routed to ' + PL.orders[0].courierID;

				UpdatePlAtDecPoint(PL, false, null, function(){
					SetToShip(containerID, false, null, PL.orders[0].actualWeight1);
				});
			}

			console.log(Msg);
			return callback();
		});
	});
} /* DecideShipping */

function GetCourierToRoute(courierID, callback){
	/* CourierRoute (4) = Reject */
	parameters.findOne({'courierRoutes.courierName': courierID},function(err, Par){
		if(err || !Par){
			console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' Failed To Find Courier Routes | ' + err);
			return callback({CourierRoute: 4, RejectReason: 'Courier Route Not Found'});
		}

		if(!Par.courierRoutes || Par.courierRoutes.length <= 0){
			console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' No Courier Routes Found In Parameters ');
			return callback({CourierRoute: 4, RejectReason: 'No Courier Routes In Parameters'});
		}

		let x = 0;
		while(x < Par.courierRoutes.length){
			if(courierID == Par.courierRoutes[x].courierName){
				break;
			}
			x++;
		}

		if(x < Par.courierRoutes.length){
			return callback({CourierRoute: Par.courierRoutes[x].routeNumber});
		}

		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' Failed To Find Courier Route For Courier Id: ' + courierID);
		return callback({CourierRoute: 4, RejectReason: 'No Courier Route For Courier Id: ' + courierID});
	});
} /* GetCourierToRoute */

function DecideForATransporter(containerID, callback){
	pickList.findOne({transporterID: containerID}, function(err, PL){
		DecideShipping('Transporter', err, containerID, PL, function(){
			return callback();
		});
	});
} /* DecideForATransporter */

function DecideForACarton(containerID, callback){
	pickList.findOne({toteID: containerID}, function(err, PL){
		DecideShipping('Carton', err, containerID, PL, function(){
			return callback();
		});
	});
} /* DecideForACarton */

function ProcDecisionPoint(task, callback){
	let containerID = task.containerID;

	if(containerID.substr(0,1) == '7'){
		DecideForATransporter(containerID, function(){
			return callback(null, containerID);
		});
		return;
	}

	DecideForACarton(containerID, function(){
		return callback(null, containerID);
	});
} /* ProcDecisionPoint */

function ProcCourierHatch1(task, callback){
	let containerID = task.containerID;
	let lweight = task.weight4;
	let Location = task.location;

	SetToShip(containerID, true, 'RTT', lweight);

	if(parseInt(containerID[0]) == 7){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '-----<<' + Location + ': [1, 1] | Transporter ' + containerID + ' seen at courier hatch 1');
	} else {
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '-----<<' + Location + ': [1, 1] | Carton ' + containerID + ' seen at courier hatch 1');
	}

	moduleIndex.fins.writeSync(finsClientObject, 'D5360', [1, 1]);
	return callback(null, containerID);
} /* ProcCourierHatch1 */

function ProcCourierHatch2(task, callback){
	let containerID = task.containerID;
	let lweight = task.weight4;
	let Location = task.location;

	SetToShip(containerID, true, 'DSV', lweight);

	if(parseInt(containerID[0]) == 7){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '-----<<' + Location + ': [1, 1] | Transporter ' + containerID + ' seen at courier hatch 2');
	} else {
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + '-----<<' + Location + ': [1, 1] | Carton ' + containerID + ' seen at courier hatch 2');
	}

	moduleIndex.fins.writeSync(finsClientObject, 'D5370', [1, 1]);
	return callback(null, containerID);
} /* ProcCourierHatch2 */

function fixWeight(weight){
    if(!weight || weight.length <= 0){
		return null;
	}

	let def = weight.match('Default');
	if(def.length > 0){
		weight = weight.substring(7, weight.length);
		weight = weight.trim();
	}

	let g = weight.indexOf('g');
	if(g > 0){
		weight = weight.substring(0, g);
	}

	return weight;
} /* fixWeight */

// Called when weight scale 1 error ocurred.
function onWeightScale1Error(err){
    console.log('Weight Scale 1: Connection Error- ' + err);
}

// Called when weight scale 1 connection closes.
function onWeightScale1Close(){
    console.log('Weight Scale 1: Connection Closed');
}

// Called when weight scale 1 has a reading
function onWeightScale1DataReceived(data){
    let MyWeight1 = data.toString();
    MyWeight1 = fixWeight(MyWeight1);
    weight1 = MyWeight1;
    console.log('Weight 1 Received: ' + weight1);
}

// Called when weight scale 1 connection established.
function onWeightScale1Connection(netModule){
    console.log('Weight Scale 1: Connection Established.');
}

// Called when weight scale 2 error ocurred.
function onWeightScale2Error(err){
    console.log('Weight Scale 2: Connection Error- ' + err);
}

// Called when weight scale 2 connection closes.
function onWeightScale2Close(){
    console.log('Weight Scale 2: Connection Closed');
}

// Called whenever weight scale 2 has a reading
function onWeightScale2DataReceived(data){
    let MyWeight2 = data.toString();
    MyWeight2 = fixWeight(MyWeight2);
    weight2 = MyWeight2;
    console.log('Weight Scale 2 Received:' + weight2);
}

// Called when weight scale 2 connection established.
function onWeightScale2Connection(netModule){
    console.log('Weight Scale 2: Connection Established.');
}

function onWeightScale3Error(err){
    console.log('Weight Scale 3: Connection Error- ' + err);
}

// Called when weight scale 4 error ocurred.
function onWeightScale4Error(err){
    console.log('Weight Scale 4: Connection Error- ' + err);
}

function onWeightScale3Close(){
    console.log('Weight Scale 3: Connection Closed');
}

// Called when weight scale 4 connection closes.
function onWeightScale4Close(){
    console.log('Weight Scale 4: Connection Closed');
}

function onWeightScale3DataReceived(data){
    HatchWeight1 = data.toString();
    HatchWeight1 = fixWeight(HatchWeight1);
    console.log('Weight 4 Received: ' + lastReadHatch1);
}

// Called when weight scale 4 has a reading
function onWeightScale4DataReceived(data){
    HatchWeight2 = data.toString();
    HatchWeight2 = fixWeight(HatchWeight2);
    console.log('Weight 4 Received: ' + lastReadHatch2);
}

function onWeightScale3Connection(netModule){
    console.log('Weight Scale 3: Connection Established.');
}

// Called when weight scale 4 connection established.
function onWeightScale4Connection(netModule){
    console.log('Weight Scale 4: Connection Established.');
}

function ProcessCartonShip(Carton, hatch, weight, callback){
	if(hatch){
		Carton.dispatched = true;
		Carton.transporterID = null;
		Carton.dispatchedDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
	}

	Carton.Status = 'SHIPPED';
	Carton.orders[0].shipped = true;
	Carton.orders[0].actualWeight2 = weight;
	Carton.ShipUpdateRequired = true;

	CreateShipUpdate(Carton);
	CreateCourierUpdate(Carton);
	CreateWmsPackListUpdate(Carton);
	Carton.save(function(err, savedDoc){
		return callback();
	});
} /* ProcessCartonShip */

function ShipEachCartonInTransporter(TransporterData, index, hatch, courier, weight, callback){
    if(index >= TransporterData.length){
    	return callback();
    }

    let Carton = TransporterData[index];

	if(courier && hatch && Carton.orders[0].courierID != courier){
		LogIncorrectRouting(Carton, courier);
	}

    ProcessCartonShip(Carton, hatch, weight, function(){
		index++;
		return ShipEachCartonInTransporter(TransporterData, index, hatch, courier, weight, callback);
	});
} /* ShipEachCartonInTransporter */

function ShipTransporterCartons(containerID, hatch, courier, weight){
	pickList.find({'transporterID': containerID}, function(err, TransporterData) {
		if(err){
		  console.log('Error while trying to find picklists for transporter ' + containerID + ' to ship. Err: ' + err);
		  return;
		}

		if(!TransporterData || TransporterData.length <= 0){
			console.log('Failed to find picklist for transporter ' + containerID + ' to ship.');
			return;
		}

		ShipEachCartonInTransporter(TransporterData, 0, hatch, courier, weight, function(){
			if(!hatch){
				console.log('Decision Point shipping completed for transporter ' + containerID);
			} else {
				console.log('Courier Hatch shipping completed for transporter ' + containerID);
			}

		   	return;
		});
	});
} /* ShipTransporterCartons */

function LogIncorrectRouting(Carton, courier){
	IncRoute.findOne({'CartonID': Carton.orders[0].cartonID}, function(err, Rec){
		if(err){
			return;
		}

		if(Rec){
			return;
		}

		let NewRec = new IncRoute({
			CartonID: Carton.orders[0].cartonID,
			TransporterID: Carton.orders[0].transporterID,
			OrderID: Carton.orders[0].orderID,
			DesignatedCourier: Carton.orders[0].courierID,
			RoutedCourier: courier,
			Required: true,
			Date: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
		});

		NewRec.save(function(err, savedDoc){
			return;
		});
	});
} /* LogIncorrectRouting */

function ShipCarton(containerID, hatch, courier, weight){
	pickList.findOne({'orders.cartonID': containerID}, function (err, Carton) {
		if(err){
			console.log('Error while trying to find picklist for carton ' + containerID + ' to ship. Err: ' + err);
			return;
		}

		if(!Carton){
			console.log('Failed to find picklist for carton ' + containerID + ' to ship.');
			return;
		}

		if(courier && hatch && Carton.orders[0].courierID != courier){
			LogIncorrectRouting(Carton, courier);
		}

		ProcessCartonShip(Carton, hatch, weight, function(){
			if(!hatch){
				console.log('Decision Point shipping completed for carton ' + containerID);
			} else {
				console.log('Courier Hatch shipping completed for carton ' + containerID);
			}

			return;
		});
	});
} /* ShipCarton */

function SetToShip(containerID, hatch, courier, weight){
	if(parseInt(containerID[0]) == 7){
		return ShipTransporterCartons(containerID, hatch, courier, weight);
	}

	return ShipCarton(containerID, hatch, courier, weight);
} /* SetToShip */

function CreateShipUpdate(mPL){
	ShipUpdate.findOne({'OrderId': mPL.orders[0].orderID}, function(err, uPL){
		if(err){
			return;
		}

		if(uPL){
			return;
		}

		let NewRec = new ShipUpdate({
			OrderId: mPL.orders[0].orderID,
			Required: true,
			CreateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
		});

		NewRec.save(function(err, savedDoc){
			console.log('Marked Order: ' + mPL.orders[0].orderID + ' ShipUpdate to true');
			return;
		});
	});
} /* CreateShipUpdate */

function CreateWmsPackListUpdate(mPL){
	WmsPackList.findOne({'OrderId': mPL.orders[0].orderID, 'CartonId': mPL.orders[0].cartonID}, function(err, uPL){
		if(err){
			return;
		}

		if(uPL){
			return;
		}

		let NewRec = new WmsPackList({
			OrderId: mPL.orders[0].orderID,
			CartonId: mPL.orders[0].cartonID,
			Required: true,
			CreateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
		});

		NewRec.save(function(err, savedDoc){
			console.log('Marked Carton ' + mPL.orders[0].cartonID + ' of Order: ' + mPL.orders[0].orderID + ' Wms Packlist Required to true');
			return;
		});
	});
} /* CreateWmsPackListUpdate */

function CreateCourierUpdate(mPL){
	CourierCarton.findOne({'OrderId': mPL.orders[0].orderID, 'CartonId': mPL.orders[0].cartonID}, function(err, uPL){
		if(err){
			return;
		}

		if(uPL){
			return;
		}

		let NewRec = new CourierCarton({
			OrderId: mPL.orders[0].orderID,
			CartonId: mPL.orders[0].cartonID,
			Required: true,
			CreateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
		});

		NewRec.save(function(err, savedDoc){
			console.log('Marked Carton ' + mPL.orders[0].cartonID + ' of Order: ' + mPL.orders[0].orderID + ' Carton Update Required to true');
			return;
		});
	});
} /* CreateCourierUpdate */

function checkPickedTote(toteID, callback){
	pickList.findOne({"toteID" : toteID}, function(err, docs){
		if(!err && docs){
		if(docs.Status == "PICKED"){
			return callback(true);
		}else{
			return callback(false);
		}
		}

		return callback(false);
	});
} /* checkPickedTote */

function rejectTote (toteID, reason) {
    pickList.findOne({"toteID" : toteID}, function(err, Resp){
		if(!err && Resp){
			let y = 0;
			while(y < Resp.orders.length){
				Resp.orders[y].rejectedReason = reason;
				Resp.orders[y].rejected = true;
				y++;
			}
			Resp.save();
		}
	});
} /* rejectTote */

function BACKDOOR_TestScanners(contScale1, weightScale1, contScale2, weightScale2, contDecPoint, contHatch1, weightHatch1, contHatch2, weightHatch2){
	if(contScale1 && weightScale1){
		qProcWeightScale1.push({containerID: contScale1, weight1: weightScale1}, function(err, data){});
	}

	if(contScale2 && weightScale2){
		qProcWeightScale2.push({containerID: contScale2, weight2: weightScale2}, function(err, data){});
	}

	if(contDecPoint){
		qProcDecisionPoint.push({containerID: contDecPoint}, function(err, data){});
	}

	if(contHatch1 && weightHatch1){
		qProcCourierHatch1.push({containerID: contHatch1, weight4: weightHatch1, location: 'Hatch [RTT]'}, function(err, data){});
	}

	if(contHatch2 && weightHatch2){
		qProcCourierHatch2.push({containerID: contHatch2, weight4: weightHatch2, location: 'Hatch [DSV]'}, function(err, data){});
	}
} /* BACKDOOR_TestScanners */


