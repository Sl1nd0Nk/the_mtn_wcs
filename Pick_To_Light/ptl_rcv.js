var net        = require('net');
var mongoose   = require('mongoose');
var moment     = require('moment');
var async      = require('async');
var rest       = require('restler');
var config     = require('../config/ptl');
var constants  = require('../config/ptl_constants');
var ptlZone    = require('../db_schema/ptlZoneModel');
var pickList   = require('../db_schema/pickListModel');
var User       = require('../db_schema/userModel');
var Param      = require('../lib/classParam');
var PickLineUpdate = require('PickLineUpdatesModel');

var sysParam   = new Param();

/////////=========== PTL CONSTANTS ================//////////
var SEQNO        = 1;
var STX          = '';
var ETX          = '';
var OWN_ID       = '050';
var HOST_ID      = '001';
var LIVE_ID      = 'LV';
var ACK          = 'QU';
var DISPLAY      = 'DI';
var MAX_DISP_LEN = 16;
var ESC          = '';
var ESC_SEQ      = ['0','1','2','3','4','5','6','7',
                    '8','A','B','C','D','E','G',
                    'K','L','P','S','T','Z','|'];

var ESC_OFF    = 0;
var ESC_ON     = 1;
var ESC_BLINK  = 2;

var ESC_3_START_POS = '..';
var ESC_3_END_POS   = '..';
var ESC_3_ROW_NUM   = '..';

var COMMISION_NO       = '9001';
var COMMISION_NO_CLEAR = '0000';

var CONNECT_TIMEOUT = config.PTL_COMMS.CONNECT_TIMEOUT;

var RECEIVE_IP   = config.PTL_COMMS.IP_ADDR;
var RECEIVE_PORT = config.PTL_COMMS.RECEIVE_PORT;

/////////=========== PTL RECEIVE =====================///////////

var PtlRcv = null;

mongoose.connect(config.mongoDB.url, function(err){
	if(err){
		WriteToFile('Error', 'MONGO CONNECT ERROR ' + err);
	} else {
		WriteToFile('Info', 'Connected To Mongo');
		AttemptToConnect();
	}
}); /* mongoose.connect */

function AttemptToConnect(){
	WriteToFile('Info', ' Attempting To Connect To [' + RECEIVE_IP + '] [' + RECEIVE_PORT + ']');
	PtlRcv = new net.Socket();
	PtlRcv.setTimeout(CONNECT_TIMEOUT);
	SEQNO = 1;
	PtlRcv.connect(RECEIVE_PORT, RECEIVE_IP, function(){
		WriteToFile('Info', ' Attempting To Connect To [' + RECEIVE_IP + '] [' + RECEIVE_PORT + ']');
	});

	PtlRcv.on('connect', function() {
		WriteToFile('Info', ' Connected To [' + RECEIVE_IP + '] [' + RECEIVE_PORT + ']');
		UpdateConnectedStatusToDb(true, function(){});
	});

	PtlRcv.on('timeout', function() {
		WriteToFile('Error', ' Timed Out While Trying To Connect To [' + RECEIVE_IP + '] [' + RECEIVE_PORT + ']');
		PtlRcv.destroy();
	});

	PtlRcv.on('end', function() {
		WriteToFile('Error', ' End Of Transmission Signal Received');
	});

	PtlRcv.on('close', function() {
		WriteToFile('Error', ' Connection Closed');
		PtlRcv = null;
		UpdateConnectedStatusToDb(false, function(){
			TimeOut();
		});
	});

	PtlRcv.on('error', function(err) {
		WriteToFile('Error', ' Connection Error - ' + err);
		PtlRcv.destroy();
	});

	PtlRcv.on('data', function(data) {
		UpdateConnectedStatusToDb(true, function(){});
		let RespStr = data.toString("utf8");
		WriteToFile('Info', ' Received [ ' + RespStr + ' ]');

		let UserLogin = RespStr.split('USER:');

		if(UserLogin.length > 1){
			let MsqTypeArr = RespStr.split(OWN_ID);

			if(MsqTypeArr.length > 1){
				MsqTypeArr = MsqTypeArr[1].split('LOGIN');
				if(MsqTypeArr.length > 1){
					let MsqType = MsqTypeArr[0];

					if(MsqType == 'BC'){
						let ZoneID = ExtractZoneIdFromMsg(MsqTypeArr[1]);
						let UserId = ExtractUserIdFromMsg(MsqTypeArr[1]);

						if(ZoneID && UserId.length){
							LogUserIntoPtlZone(ZoneID, UserId);
						}
					}
				}
			}
		} else {
			let EscSeqIndex = ExtractEscSeq(RespStr);

			if(EscSeqIndex >= 0 && EscSeqIndex < RespStr.length){
				PickStockFromLocation(RespStr, EscSeqIndex);
			} else {
				if(RespStr.length >= 15 && RespStr[9] == 'K' && RespStr[10] == 'Y' && RespStr[11] == 'P' && RespStr[12] == 'Z'){
					ConfirmZoneControllerKeyPress(RespStr);
				}
			}
		}

		let ReceivedSeq = RespStr[1] + RespStr[2];
		let SendData = STX + ReceivedSeq + OWN_ID + HOST_ID + ACK + ETX;
		WriteToFile('Info', ' Sending ACK [ ' + SendData + ' ]');
		PtlRcv.write(SendData);
	});
} /* AttemptToConnect */

function TimeOut(){
	setTimeout(function(){
		AttemptToConnect();
	}, 2000);
} /* TimeOut */

function UpdateConnectedStatusToDb(Status, callback){
	sysParam.FindOne({'ParameterName' : 'PTL'}, function(Resp){
		if(Resp.Err){
			WriteToFile('Error', ' Failed To Find Paramater PTL | Err [' + Resp.Err + ']');
			return callback();
		}

		if(!Resp.Param){
			WriteToFile('Error', ' Failed To Find Paramater PTL');
			return callback();
		}

		let PtlParam = Resp.Param;

		PtlParam.Fields.ReceiverConnected = Status

		sysParam.Update(PtlParam, function(Resp){
			return callback();
		});
	});
} /* UpdateConnectedStatusToDb */

function ConfirmZoneControllerKeyPress(RespStr){
	let Data = RespStr.substring(11, 15);
	let LocData = Data.substring(2, Data.length);
	let ZoneID = parseInt(LocData);

	ptlZone.findOne({'ZoneID': ZoneID}, function(err, ZoneData){
		if(err){
			WriteToFile('Error', ' Failed To Find Zone [ ' + ZoneID + ' ] For (DONE) Confirmation | Err [ ' + err + ' ]');
			return;
		}

		if(!ZoneData){
			WriteToFile('Error', ' Failed To Find Zone [ ' + ZoneID + ' ] For (DONE) Confirmation');
			return;
		}

		if(!ZoneData.ToteID){
			WriteToFile('Error', ' Failed To Confirm (DONE) Confirmation At Zone [ ' + ZoneID + ' ]. No Tote At Station.');
			return;
		}

		if(ZoneData.Status == 'DONE'){
			WriteToFile('Info', ' (DONE) Confimed At Zone [ ' + ZoneID + ' ] Successfully');
			ZoneData.ZoneCompleted = true;
		}

		ZoneData.Transmitted = false;

		ZoneData.save(function(err, savedDoc){
			return;
		});
	});
} /* ConfirmZoneControllerKeyPress */

function PickStockFromLocation(RespStr, EscSeqIndex){
	let Data = RespStr.substring(11, EscSeqIndex);
	let LocData = Data.substring(0, 16);
	let Value = Data.substring(20, Data.length);
	let RtnDataLoc = LocData.split(" ");
	let ZoneLoc = LocData[3] + LocData[4];
	let ZoneID = parseInt(ZoneLoc);
	let QtyPicked = parseInt(Value);

	ptlZone.findOne({'ZoneID': ZoneID}, function(err, ZoneData){
		if(err){
			WriteToFile('Error', ' Failed To Find Zone [ ' + ZoneID + ' ] To Pick From Location [ ' + RtnDataLoc[0] + ' ] | Err [ ' + err + ' ]');
			return;
		}

		if(!ZoneData){
			WriteToFile('Error', ' Failed To Find Zone [ ' + ZoneID + ' ] To Pick From Location [ ' + RtnDataLoc[0] + ' ]');
			return;
		}

		let ToteID = ZoneData.ToteID;
		let UserID = ZoneData.UserID;

		if(!QtyPicked || isNaN(QtyPicked)){
			WriteToFile('Error', ' Failed To Pick. Received PTL Qty [ ' + QtyPicked + ' ] At Zone [ ' + ZoneID + ' ] Location [ ' + RtnDataLoc[0] + ' ] Not Valid');
			return;
		}

		if(!ToteID){
			WriteToFile('Error', ' Failed To Pick. No Tote At Zone [ ' + ZoneID + ' ] To Pick From Location [ ' + RtnDataLoc[0] + ' ]');
			return;
		}

		pickList.findOne({'toteID': ToteID}, function(err, PLData){
			if(err){
				WriteToFile('Error', ' Failed To Find Picklist In Zone [ ' + ZoneID + ' ] To Pick From Location [ ' + RtnDataLoc[0] + ' ] | Err [ ' + err + ' ]');
				return;
			}

			if(!PLData){
				WriteToFile('Error', ' Failed To Find Picklist In Zone [ ' + ZoneID + ' ] To Pick From Location [ ' + RtnDataLoc[0] + ' ]');
				return;
			}

			WriteToFile('Info', ' [ ' + QtyPicked + ' ] Picked From Location [ ' + RtnDataLoc[0] + ' ] Into Tote [ ' + ToteID + ' ] By User [ ' + UserID + ' ]');
			PLData = PickQtyInPickList(PLData, RtnDataLoc[0], QtyPicked, UserID);
			ZoneData = PickQtyInZone(ZoneData, RtnDataLoc[0], QtyPicked);

			PLData.save(function(err, PSavedDoc){
				ZoneData.save(function(err, ZSavedDoc){
					return;
				});
			});
		});
	});
} /* PickStockFromLocation */

function PickQtyInZone(ZoneData, Loc, QtyPicked){
	let x = 0;
	let Zone = ZoneData;
	let RemainingQty = QtyPicked;

	while(x < Zone.PickLocations.length){
		if(Zone.PickLocations[x].Location == Loc){
			if(RemainingQty < 0){
				RemainingQty = 0;
			}

			if(RemainingQty >= Zone.PickLocations[x].PickQty){
				Zone.PickLocations[x].PickedQty = Zone.PickLocations[x].PickQty;
				RemainingQty -= Zone.PickLocations[x].PickQty;
			} else {
				Zone.PickLocations[x].PickedQty = RemainingQty;
				Zone.PickLocations[x].ShortPicked = true;
			}

			Zone.PickLocations[x].LocStatus = 'PICKED';
			Zone.PickLocations[x].Transmitted = false;
		}
		x++;
	}

	x = 0;
	let AllZoneLocsPicked = true;
	while(x < Zone.PickLocations.length){
		if(Zone.PickLocations[x].LocStatus != 'PICKED'){
			AllZoneLocsPicked = false;
			break;
		}
		x++;
	}

	if(AllZoneLocsPicked){
		Zone.Status = 'DONE';
		Zone.Transmitted = false;
	}

	return Zone;
} /* PickQtyInZone */

function CreatePickLineUpdate(uPL, ordNdx, lnIndx){
	PickLineUpdate.findOne({'PickListID': uPL.orders[ordNdx].pickListID,
					  'PickListLine': uPL.orders[ordNdx].items[lnIndx].pickListOrderLine,
					  'OrderId': uPL.orders[ordNdx].orderID}, function(err, PlUpdateRec){
		if(err){
			return;
		}

		if(PlUpdateRec){
			return;
		}

		let NewRec = new PickLineUpdate({
			PickListID: uPL.orders[ordNdx].pickListID,
			WcsId: uPL.toteID,
			PickListLine: uPL.orders[ordNdx].items[lnIndx].pickListOrderLine,
			OrderId: uPL.orders[ordNdx].orderID,
			Required: true,
			CreateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
		});

		NewRec.save(function(err, savedDoc){
			WriteToFile('info', 'Marked picklist: ' + uPL.orders[ordNdx].pickListID + ' | Line: ' + uPL.orders[ordNdx].items[lnIndx].pickListOrderLine + ' | PickLineRequired to true');
			return;
		});
	});
} /* CreatePickLineUpdate */

function PickQtyInPickList(PLData, Loc, QtyPicked, UserID){
	let PL = PLData;
	let Location = Loc;
	let RemainingQty = QtyPicked;

	let x = 0;
	while(x < PL.orders.length){
		let y = 0;
		while(y < PL.orders[x].items.length){
			if(PL.orders[x].items[y].location == Location){
				if(RemainingQty < 0){
					RemainingQty = 0;
				}

				if(RemainingQty >= PL.orders[x].items[y].qty){
					PL.orders[x].items[y].pickQty = PL.orders[x].items[y].qty;
					RemainingQty -= PL.orders[x].items[y].qty;
				} else {
					PL.orders[x].items[y].pickQty = RemainingQty;
					PL.orders[x].items[y].shortPick = true;
				}

				PL.orders[x].items[y].picked = true;
				PL.orders[x].items[y].pickedBy = UserID;
				PL.orders[x].items[y].PickLineRequired = true;
				CreatePickLineUpdate(PL, x, y);
			}
			y++;
		}

		y = 0;
		let AllLinesForOrderPicked = true;
		while(y < PL.orders[x].items.length){
			if(!PL.orders[x].items[y].picked){
				AllLinesForOrderPicked = false;
				break;
			}
			y++;
		}

		if(AllLinesForOrderPicked){
			PL.orders[x].picked = true;
		}
		x++;
	}

	x = 0;
	let AllOrdersPicked = true;
	while(x < PL.orders.length){
		if(!PL.orders[x].picked){
			AllOrdersPicked = false;
			break;
		}
		x++;
	}

	if(AllOrdersPicked){
		PL.Status = 'PICKED';
		PL.PickUpdateRequired = true;
	}

	return PL;
} /* PickQtyInPickList */

function LogUserIntoPtlZone(ZoneID, UserId){
	WriteToFile('Info', ' BC Login Message Received For Zone [ ' + ZoneID + ' ] | UserID [ ' + UserId + ' ]');
	let Stack = {};

	Stack.UserData = function(callback){
		User.findOne({'PTLID': UserId}, function(err, UserData){
			return callback(err, UserData);
		});
	}

	Stack.ZoneData = function(callback){
		ptlZone.findOne({'ZoneID': ZoneID}, function(err, ZoneData){
			return callback(err, ZoneData);
		});
	}

	async.parallel(Stack, function(err, Result){
		if(err){
			WriteToFile('Error', ' Failed To Find USER/ZONE | RFID Tag ID [ ' + UserId + ' ] | Zone [ ' + ZoneID + ' ] | Err [ ' + err + ' ]');
			return;
		}

		if(!Result.ZoneData){
			WriteToFile('Error', ' Failed To Find Zone [ ' + ZoneID + ' ] For Login | RFID Tag ID [ ' + UserId + ' ] To Login');
			return;
		}

		let UserName = null;
		if(!Result.UserData){
			WriteToFile('Error', ' Failed To Find User Using RFID Tag ID [ ' + UserId + ' ] For Login At Zone [ ' + ZoneID + ' ] | User (Admin) will be used instead');
			UserName = 'Admin';
		} else {
			UserName = Result.UserData.username;
		}

		let Zone = Result.ZoneData;

		if(!Zone.ToteID){
			WriteToFile('Error', ' Failed No Tote At Zone [ ' + ZoneID + ' ]');
			return;
		}

		Zone.UserID = UserName;
		Zone.Transmitted = false;

		Zone.save(function(err, savedDoc){
			WriteToFile('Info', ' UserID [ ' + UserName + ' ] Logging Into Zone [ ' + Result.ZoneData.ZoneID + ' ]');
		});
	});
} /* LogUserIntoPtlZone */

function ExtractZoneIdFromMsg(MsqType){
	let n = MsqType.indexOf('Z');
	let min = 0;
	let max = 2;
	let StrZone = '';

	while(min < max){
		let index = n + min + 1;
		StrZone += MsqType[index];
		min++;
	}

	let Zone = null;
	if(StrZone.length > 0){
		Zone = parseInt(StrZone);
	}

	return Zone;
} /* ExtractZoneIdFromMsg */

function ExtractUserIdFromMsg(MsqType){
	WriteToFile('Info', 'MsqType [ ' + MsqType + ' ]');
	if(!MsqType || MsqType.length <= 0){
		return null;
	}

	let DataArr = MsqType.split('USER');
	let UserId = '';
	if(DataArr.length > 1){
		let UserData = DataArr[1];
		let n = UserData.indexOf(':');
		let min = 0;
		let max = 4;

		while(min < max){
			let index = n + min + 1;
			UserId += UserData[index];
			min++;
		}
	}

	return UserId;
} /* ExtractUserIdFromMsg */

function ExtractEscSeq(RespStr){
	if(!RespStr || RespStr.length <= 0){
		return -1;
	}

	let x = 0;
	while(x < RespStr.length){
		if(RespStr[x] == ESC){
			WriteToFile('Info', ' [ESC SEQ] Found At Position[ ' + x + ' ]');
			return x;
		}
		x++;
	}

	return -1;
} /* ExtractEscSeq */

function WriteToFile(logtype, Message) {
	let now = new Date();
  	console.log(moment(now).format('YYYY-MM-DD HH:mm:ss') + ": [" + logtype + "] -> " + Message);
} /* WriteToFile */
