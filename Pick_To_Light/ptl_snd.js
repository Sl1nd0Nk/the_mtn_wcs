var net        = require('net');
var mongoose   = require('mongoose');
var moment     = require('moment');
var async      = require('async');
var rest       = require('restler');
var ptlZone    = require('../db_schema/ptlZoneModel');
var config     = require('../config/ptl');
var constants  = require('../config/ptl_constants');
var Param      = require('../lib/classParam');

var sysParam   = new Param();

/////////=========== PTL CONSTANTS ================//////////
var SEQNO        = 1;
var STX          = '';
var ETX          = '';
var OWN_ID       = '050';
var HOST_ID      = '001';
var LIVE_ID      = 'LV';
var ACK          = 'QU';
var DISPLAY      = 'DI';
var MAX_DISP_LEN = 16;
var ESC		 = '';
var ESC_SEQ      = ['0','1','2','3','4','5','6','7',
                    '8','A','B','C','D','E','G',
                    'K','L','P','S','T','Z','|'];

var ESC_OFF    = 0;
var ESC_ON     = 1;
var ESC_BLINK  = 2;

var ESC_3_START_POS = '..';
var ESC_3_END_POS   = '..';
var ESC_3_ROW_NUM   = '..';

var COMMISION_NO       = '9001';
var COMMISION_NO_CLEAR = '0000';

var LIVE_MSG_TR_INT = config.PTL_COMMS.LIVE_MSG_TR_INT;
var CONNECT_TIMEOUT = config.PTL_COMMS.CONNECT_TIMEOUT;
var MISSING_ZONE_CONTROLLERS = config.ZONE_MISSING_CONTROLLERS.Zones;

var SEND_IP      = config.PTL_COMMS.IP_ADDR;
var SEND_PORT    = config.PTL_COMMS.SEND_PORT;

var INT_MSG_QUEUE = [];

var EndPoint = config.ZONE_RELEASE.url;

/////////=========== PTL CONSTANTS END ============//////////

var PtlSend = null;

mongoose.connect(config.mongoDB.url, function(err){
	if(err){
		WriteToFile('Error', ' MONGO CONNECT ERROR ' + err);
	} else {
		WriteToFile('Info', ' Connected To Mongo');
		AttemptToConnect();
	}
}); /* mongoose.connect */

function AttemptToConnect(){
	sysParam.FindOne({'ParameterName' : 'PTL'}, function(Resp){
		if(Resp.Err){
			WriteToFile('Error', ' Failed To Find Paramater PTL | Err [' + Resp.Err + ']');
			TimeOut();
			return;
		}

		if(!Resp.Param){
			WriteToFile('Error', ' Failed To Find Paramater PTL');
			TimeOut();
			return;
		}

		if(!Resp.Param.Fields){
			WriteToFile('Error', ' PTL Paramater Not Configured');
			TimeOut();
			return;
		}

		if(!Resp.Param.Fields.ReceiverConnected){
			WriteToFile('Error', ' PTL Receiver Not Connected');
			TimeOut();
			return;
		}

		PtlSend = new net.Socket();
		PtlSend.setTimeout(CONNECT_TIMEOUT);
		SEQNO = 1;
		PtlSend.connect(SEND_PORT, SEND_IP, function(){
			WriteToFile('Info', ' Attempting To Connect To [' + SEND_IP + '] [' + SEND_PORT + ']');
		});

		PtlSend.on('connect', function() {
			RecoverPtlState();
			WriteToFile('Info', ' Connected To [' + SEND_IP + '] [' + SEND_PORT + ']');
                        SendLiveTelegram();
		});

		PtlSend.on('ready', function() {
			SendLiveTelegram();
		});

		PtlSend.on('timeout', function() {
			WriteToFile('Error', ' Timed Out While Trying To Connect To [' + SEND_IP + '] [' + SEND_PORT + ']');
			PtlSend.destroy();
		});

		PtlSend.on('end', function() {
			WriteToFile('Error', ' End Of Transmission Signal Received');
		});

		PtlSend.on('close', function() {
			WriteToFile('Error', ' Connection Closed');
			UpdateConnectedStatusToDb(false, function(){
				PtlSend = null;
				TimeOut();
			});
		});

		PtlSend.on('error', function(err) {
			WriteToFile('Error', ' Connection Error - ' + err);
			PtlSend.destroy();
		});

		PtlSend.on('data', function(data) {
			let RespStr = data.toString("utf8");
			//WriteToFile('Info', ' Received Response [ ' + RespStr + ' ]');

			RemoveMsgFromQueue(RespStr);

			if(INT_MSG_QUEUE.length > 0){
				SendQueuedMsg();
			} else {
				GetNextMsgToSend();
			}
		});
	});
} /* AttemptToConnect */

function TimeOut(){
	setTimeout(function(){
		AttemptToConnect();
	}, 2000);
} /* TimeOut */

function UpdateConnectedStatusToDb(Status, callback){
	sysParam.FindOne({'ParameterName' : 'PTL'}, function(Resp){
		if(Resp.Err){
			WriteToFile('Error', ' Failed To Find Paramater PTL | Err [' + Resp.Err + ']');
			return callback();
		}

		if(!Resp.Param){
			WriteToFile('Error', ' Failed To Find Paramater PTL');
			return callback();
		}

		let PtlParam = Resp.Param;

		PtlParam.Fields.ReceiverConnected = Status

		sysParam.Update(PtlParam, function(Resp){
			return callback();
		});
	});
} /* UpdateConnectedStatusToDb */

function RecoverPtlState(){
	INT_MSG_QUEUE = [];

	ptlZone.find({'Status': {$ne: null}}, function(err, ZoneData){
		if(err){
			return;
		}

		if(!ZoneData || ZoneData.length <= 0){
			return;
		}

		ResetEachZone(ZoneData, 0, function(){
			return;
		});
	});
} /* RecoverPtlState */

function ResetEachZone(ZoneData, x, callback){
	if(x >= ZoneData.length){
		return callback();
	}

	let Zone = ZoneData[x];
	let Update = false;
	if(Zone.Transmitted == true){
		Zone.Transmitted = false;
		if(!Update){
			Update = true;
		}
	}

	let y = 0;
	while(y < Zone.PickLocations.length){
		if(Zone.PickLocations[y].Transmitted == true){
			Zone.PickLocations[y].Transmitted = false;
			if(!Update){
				Update = true;
			}
		}
		y++;
	}

	if(!Update){
		x++;
		return ResetEachZone(ZoneData, x, callback);
	}

	Zone.save(function(err, savedDoc){
		x++;
		return ResetEachZone(ZoneData, x, callback);
	});
} /* ResetEachZone */

function GetNextMsgToSend(){
	let Stack = {};

	Stack.ZoneData = function(callback){
		ptlZone.find({'Status': {$ne: null}, 'Transmitted': false}, function(err, ZoneData){
			return callback(err, ZoneData);
		});
	}

	Stack.LocationData = function(callback){
		ptlZone.find({'Status': {$in: ['PICK','DONE']}}, function(err, LocationData){
			return callback(err, LocationData);
		});
	}

	Stack.ClearLocData = function(callback){
		ptlZone.find({'PickLocations.LocStatus': 'CLEAR', 'PickLocations.Transmitted': false}, function(err, ClearLocData){
			return callback(err, ClearLocData);
		});
	}

	Stack.Param = function(callback){
		sysParam.FindOne({'ParameterName' : 'PTL'}, function(Resp){
			return callback(Resp.Err, Resp.Param);
		});
	}

	async.parallel(Stack, function(err, Result){
		if(err){
			return SendLiveTelegram();
		}

		if(!Result.Param || !Result.Param.Fields || !Result.Param.Fields.ReceiverConnected){
			PtlSend.destroy();
			WriteToFile('Error', ' PTL Receiver Not Connected');
			TimeOut();
			return;
		}

		ProcessClearOldToteLocations(Result.ClearLocData);
		ProcessZoneControllerCommands(Result.ZoneData);
		ProcessZoneLocationCommands(Result.LocationData);

		if(INT_MSG_QUEUE.length <= 0){
			return SendLiveTelegram();
		}

		SendQueuedMsg();
	});
} /* GetNextMsgToSend */

function ProcessClearOldToteLocations(ClearLocData){
	if(!ClearLocData || ClearLocData.length <= 0){
		return;
	}

	let x = 0;
	while(x < ClearLocData.length){
		let y = 0;
		while(y < ClearLocData[x].PickLocations.length){
			if(!ClearLocData[x].PickLocations[y].Transmitted && ClearLocData[x].PickLocations[y].LocStatus == 'CLEAR'){
				ClearLocData[x] = QueueZoneLocClear(ClearLocData[x], y);
			}
			y++;
		}
		x++;
	}

	return;
} /* ProcessClearOldToteLocations */

function ProcessZoneControllerCommands(ZoneData){
	if(!ZoneData || ZoneData.length <= 0){
		return;
	}

	let x = 0;
	while(x < ZoneData.length){
		if(!ZoneData[x].UserID || ZoneData[x].Status == 'DONE'){
			if(ZoneData[x].ZoneCompleted || (InListMissingController(ZoneData[x]) && ZoneData[x].Status == 'DONE')){
				QueueZoneClear(ZoneData[x]);
			} else {
				QueueBlinkingZoneController(ZoneData[x]);
			}
		} else {
			QueueSteadyPick(ZoneData[x]);
		}

		x++;
	}
} /* ProcessZoneControllerCommands */

function InListMissingController(Zone){
	let ZoneID = Zone.ZoneID;

	let index = MISSING_ZONE_CONTROLLERS.indexOf(ZoneID);

	if(index >= 0){
		WriteToFile('Info', ' Zone [ ' + MISSING_ZONE_CONTROLLERS[index] + ' ] Has No Zone Controller');
		return true;
	}

	return false;
} /* InListMissingController */

function QueueZoneClear(Zone){
	let ZControl = FormatLocName(Zone.ZoneID);

	let data = {
		Location: ZControl,
		Value   : ' ',
		status  : 'DONE',
		Color   : '000'
	}

	Zone.UserID = null;
	Zone.Status = null;
	Zone.ToteInTime = null;
	Zone.Transmitted = false;
	Zone.ZoneCompleted = false;
	Zone.ZoneError = false;
	SendZCTRLTelegram(data, Zone);

  	let x = 0;
  	while(x < Zone.PickLocations.length){
		Zone = QueueZoneLocClear(Zone, x);
		x++;
  	}
} /* QueueZoneClear */

function QueueBlinkingZoneController(Zone){
	let ZControl = FormatLocName(Zone.ZoneID);
	let Value = 'LOGIN';
	let status = 'LOGIN';
	let Color = '220'; // ZControl Colour 'YELLOW' Blinking

	if(Zone.Status == 'DONE'){
		Value = 'DONE';
		Color = '020';
		status = 'PICKED';

		if(Zone.ZoneError){
			Color = '200';
			status = 'ERROR';
		}
	}

	let data = {
		Location: ZControl,
		Value   : Value,
		status  : status,
		Color   : Color
	}

	SendZCTRLTelegram(data, Zone);
} /* QueueBlinkingZoneController */

function QueueSteadyPick(Zone){
	let ZControl = FormatLocName(Zone.ZoneID);

	let data = {
		Location: ZControl, //Location
		Value   : 'PICK',   //Intruction
		status  : 'PICK',
		Color   : '110'     // ZControl Colour 'YELLOW' NON BLINKING
	}

	Zone.Status = 'PICK';

	SendZCTRLTelegram(data, Zone);
} /* QueueSteadyPick */

function ProcessZoneLocationCommands(LocationData){
	if(!LocationData || LocationData.length <= 0){
		return;
	}

	let x = 0;
	while(x < LocationData.length){
		let y = 0;
		while(y < LocationData[x].PickLocations.length){
			if(!LocationData[x].PickLocations[y].Transmitted){
				if(LocationData[x].PickLocations[y].LocStatus == 'NEW' && !LocationData[x].PickLocations[y].ShortPicked){
					LocationData[x] = QueueBlinkingQty(LocationData[x], y);
				} else {
					LocationData[x] = QueueSteadyQty(LocationData[x], y);
				}
			}
			y++;
		}
		x++;
	}

	return;
} /* ProcessZoneLocationCommands */

function QueueBlinkingQty(Zone, index){
	let Colour = '020';
	let Status = 'NEW';

	if(Zone.PickLocations[index].Location[0] == 'P'){
		Colour = '002';
	}

	let data = {
		Location: Zone.PickLocations[index].Location,    //Location
		Value   : Zone.PickLocations[index].PickQty,     //Intruction
		status  : Status,
		Color   : Colour,                                //BLINKING Colour
		UserID	: Zone.UserID
	}

	Zone.PickLocations[index].Transmitted = true;
	SendTelegram(data, Zone);

	return Zone;
} /* QueueBlinkingQty */

function QueueSteadyQty(Zone, index){
	let Colour = '010';
	let Status = 'PICKED';

	if(Zone.PickLocations[index].Location[0] == 'P'){
		Colour = '001';
	}

	let data = {
		Location: Zone.PickLocations[index].Location,    //Location
		Value   : Zone.PickLocations[index].PickQty,     //Intruction
		status  : Status,
		Color   : Colour,                                //BLINKING Colour
		UserID	: Zone.UserID
	}

	Zone.PickLocations[index].Transmitted = true;
	SendTelegram(data, Zone);

	return Zone;
} /* QueueSteadyQty */

function QueueZoneLocClear(Zone, index){
	let loc = Zone.PickLocations[index].Location;
	
	if(loc){
		let data = {
			Location: loc,
			Value   : ' ',
			status  : 'DONE',
			Color   : '000'
		}

    		Zone.PickLocations[index].Location = null;
    		Zone.PickLocations[index].PickQty = null;
    		Zone.PickLocations[index].PickedQty = null;
    		Zone.PickLocations[index].ShortPicked = false;
    		Zone.PickLocations[index].LocStatus = null;
		Zone.PickLocations[index].Transmitted = false;

		SendTelegram(data, Zone);
	} else {
		Zone.PickLocations[index].Location = null;
                Zone.PickLocations[index].PickQty = null;
                Zone.PickLocations[index].PickedQty = null;
                Zone.PickLocations[index].ShortPicked = false;
                Zone.PickLocations[index].LocStatus = null;
                Zone.PickLocations[index].Transmitted = false;
	}

	return Zone;
} /* QueueZoneLocClear */

function SendQueuedMsg(){
	let ZoneData = INT_MSG_QUEUE[0].Zone;

	if(!ZoneData){
		INT_MSG_QUEUE.splice(0, 1);
		return;
	}

	UpdateZoneToDB(ZoneData);
} /* SendQueuedMsg */

function UpdateZoneToDB(ZoneData){
	ptlZone.findOne({'ZoneID': ZoneData.ZoneID}, function(err, DBZoneData){
		if(err){
			WriteToFile('Error', err);
			return;
		}

		if(!ZoneData){
			WriteToFile('Error', 'Failed To Find Zone Using ZoneId [ ' + ZoneData.ZoneID + ' ]');
			return;
		}

		if(INT_MSG_QUEUE[0].Type == 'ZONE_CONTROLLER'){
			let ToteID = ZoneData.ToteID;

			if(!ZoneData.Status){
				ZoneData.ToteID = null;
				ReleaseToteFromStation(ToteID, ZoneData.ZoneID);
			} else {
				ZoneData.Transmitted = true;
			}
		}

		if(INT_MSG_QUEUE[0].Type == 'ZONE_LOCATION'){
			let x = 0;
			while(x < ZoneData.PickLocations.length){
				if(ZoneData.PickLocations[x].Location == null){
					ZoneData.PickLocations.splice(x, 1);
					break;
				}
				x++;
			}
		}

		DBZoneData.UserID = ZoneData.UserID;
		DBZoneData.Status = ZoneData.Status;
		DBZoneData.ToteID = ZoneData.ToteID;
		DBZoneData.ToteInTime = ZoneData.ToteInTime;
		DBZoneData.Transmitted = ZoneData.Transmitted;
		DBZoneData.ZoneCompleted = ZoneData.ZoneCompleted;
		DBZoneData.ZoneError = ZoneData.ZoneError;
		DBZoneData.PickLocations = ZoneData.PickLocations;

		DBZoneData.save(function(err, saveDoc){
			if(INT_MSG_QUEUE[0].Type == 'ZONE_CONTROLLER'){
				WriteToFile('Info', ' Sending Zone Controller Command [ ' + INT_MSG_QUEUE[0].Msg + ' ]');
			}

			if(INT_MSG_QUEUE[0].Type == 'ZONE_LOCATION'){
				WriteToFile('Info', ' Sending Location Command [ ' + INT_MSG_QUEUE[0].Msg + ' ]');
			}

			if(PtlSend){
				PtlSend.write(INT_MSG_QUEUE[0].Msg);
			}
		});
	});
} /* UpdateZoneToDB */

function ReleaseToteFromStation(ToteID, ZoneID){
	let StrZone = ZoneID.toString();
	//console.log(EndPoint);
	rest.post(EndPoint,{
		data: { toteID: ToteID, location: StrZone},
	}).on('complete', function(data, response){
		console.log(data);
		if(response){
			WriteToFile('Info', ' Released Tote [ ' + ToteID + ' ] from Zone [ ' + ZoneID + ' ]');
		} else {
			WriteToFile('Error',' Failed To Release Tote [ ' + ToteID + ' ] from Zone [ ' + ZoneID + ' ]');
		}

		return;
	});
} /* ReleaseToteFromStation */

function RemoveMsgFromQueue(RespStr){
	let RespMsgSeqNo = RespStr[1] + RespStr[2];

	//WriteToFile('Info', ' Received SeqNo To Remove [ ' + RespMsgSeqNo + ' ]');

	let x = 0;
	while(x < INT_MSG_QUEUE.length){
		if(INT_MSG_QUEUE[x].SeqNo == RespMsgSeqNo){
			break;
		}
		x++;
	}

	if(x < INT_MSG_QUEUE.length){
		INT_MSG_QUEUE.splice(x, 1);
	} else {
		INT_MSG_QUEUE.splice(0, 1);
	}
} /* RemoveMsgFromQueue */

function GetNextSeqNo() {
	if(SEQNO < 10) {
		strSeq = "0" + SEQNO.toString();
	} else {
		strSeq = SEQNO.toString();
	}

	SEQNO++;

	if(SEQNO > 99) {
		SEQNO = 1;
	}

	return strSeq;
} /* GetNextSeqNo */

function SendLiveTelegram() {
	setTimeout(function(){
		let NextSeqNo = GetNextSeqNo();
		let Msg = OWN_ID + HOST_ID + LIVE_ID + ETX;
		let TransmitData = STX + NextSeqNo + Msg;

	//	WriteToFile('Info', ' Sending Live Telegram [ ' + TransmitData + ' ]');

		if(PtlSend){
			PtlSend.write(TransmitData);
			INT_MSG_QUEUE.push({SeqNo: NextSeqNo, Type: 'LIVE_TELEGRAM', Msg: TransmitData});
		}
	}, 500);
} /* SendLiveTelegram */

function FormatDisplayData(DispData){
	let DisplayData = DispData;

	let DispDataLen = DisplayData.length;
	let Diff = MAX_DISP_LEN - DispDataLen;

	if(Diff > 0) {
		let x = 0;
		while(x < Diff) {
	  		DisplayData += ' ';
	  		x++;
		}
	}

  	return DisplayData;
} /* FormatDisplayData */

function FormatLocName(IntZoneLoc){
	let ZoneLoc = 'PZ';
	if(IntZoneLoc < 10){
		ZoneLoc += '0';
	}

	ZoneLoc += IntZoneLoc;

	return ZoneLoc;
} /* FormatLocName */

function SendZCTRLTelegram(Data, Zone) {
	let CommNo = COMMISION_NO;
	let DisplayData = FormatDisplayData(Data.Location);
	let ValueData = FormatDisplayData(Data.Value);
	let Color = Data.Color;

	let blinkVal;
	let blinkLED;

	if(Data.status == constants.Status.DONE) {
		blinkVal = '0';
		blinkLED = '0';
		CommNo = COMMISION_NO_CLEAR;
	} else {
		blinkVal = '4';
		blinkLED = Color;
	}

	let NextSeqNo = GetNextSeqNo();
	let TransmitData = STX + NextSeqNo + OWN_ID + HOST_ID;
	TransmitData += DISPLAY + DisplayData;
	TransmitData += CommNo;
	TransmitData += ESC + ESC_SEQ[1] + blinkVal + ValueData;
	TransmitData += ESC + ESC_SEQ[4] + blinkLED;
	TransmitData += ETX;

	//WriteToFile('Info', ' Queuing Zone Controller Command [ ' + TransmitData + ' ]');

	INT_MSG_QUEUE.push({SeqNo: NextSeqNo, Type: 'ZONE_CONTROLLER', Msg: TransmitData, Zone: Zone});
} /* SendZCTRLTelegram */

function SendTelegram(Data, Zone) {
	let CommNo = COMMISION_NO;
	let DisplayData = FormatDisplayData(Data.Location);
	let ValueData = FormatDisplayData(Data.Value);
	let Color = Data.Color;
	let UserRole = Data.UserID;
	let blinkVal;
	let blinkLED;

	let Dn = '0';
	let Up = '1';

	if(Data.Location[0] != 'C') {
		Dn = '1';
		Up = '0';
	}

	let MinVal = Data.Value;
	let MaxVal = Data.Value;

	if(UserRole == 'CLIVE') {
		MinVal = 0;
	}

	if(Data.status == 'PICKED') {
		blinkVal = ESC_ON;
		blinkLED = Color;
	} else if(Data.status == 'DONE') {
		blinkVal = '0';
		blinkLED = '0';
		Dn = '0';
		Up = '0';
		CommNo = COMMISION_NO_CLEAR;
	} else {
		blinkVal = ESC_BLINK;
		blinkLED = Color;
	}

	let NextSeqNo = GetNextSeqNo();
	let TransmitData = STX + NextSeqNo + OWN_ID + HOST_ID;
	TransmitData += DISPLAY + DisplayData;
	TransmitData += CommNo;
	TransmitData += ESC + ESC_SEQ[1] + blinkVal + ValueData;
	TransmitData += ESC + ESC_SEQ[4];
	TransmitData += blinkLED + ESC + ESC_SEQ[15] + 'L' + MinVal + 'H' + MaxVal;
	TransmitData += ESC + ESC_SEQ[3] + ESC_3_START_POS + ESC_3_END_POS + ESC_3_ROW_NUM;
	TransmitData += ESC + ESC_SEQ[17] + Dn + Up;
	TransmitData += ETX;

	//WriteToFile('Info', ' Queuing Location Command [ ' + TransmitData + ' ]');

	INT_MSG_QUEUE.push({SeqNo: NextSeqNo, Type: 'ZONE_LOCATION', Msg: TransmitData, Zone: Zone});
} /* SendTelegram */

function WriteToFile(logtype, Message) {
	let now = new Date();
  	console.log(moment(now).format('YYYY-MM-DD HH:mm:ss') + ": [" + logtype + "] -> " + Message);
} /* WriteToFile */
