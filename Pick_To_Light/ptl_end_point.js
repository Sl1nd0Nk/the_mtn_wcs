var express         = require('express');
var bodyParser      = require('body-parser');
var mongoose        = require('mongoose');
var moment          = require('moment');
var config          = require('../config/ptl');
var taskController  = require('./controllers/tasks');
var taskController1 = require('./controllers/ToteAtPickZone');

var app = express();

app.use(bodyParser.urlencoded({
	extended: false
}));

var router = express.Router();

app.use("/api", router);

router.route("/addtask").post(taskController.addTask);
router.route("/picktote").post(taskController1.ToteAtPickZone);

mongoose.connect(config.mongoDB.url, function(err){
	if(err){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ': [Error] -> ' + 'MONGO CONNECT ERROR ' + err);
	} else {
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ': [Info] -> ' + 'Connected To Mongo');
		app.listen(3000, function(){
			console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ': [Info] -> ' + 'TEST PTL SUBMITTER LISTENING ON PORT 3000');
		});
	}
}); /* mongoose.connect */



