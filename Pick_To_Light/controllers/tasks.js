var redis      = require('redis');

var Redisclient = redis.createClient();

exports.addTask = function(req, res)
{
	var LocArr = [];
	var Location = req.body.Location;
	LocArr = Location.split('|');

	var ValArr = [];
	var Values = req.body.Value;
	ValArr = Values.split('|');

	var StatusArr = [];
	var Status = req.body.Status;
	StatusArr = Status.split('|');

	if(ValArr.length > 0 && LocArr.length > 0 && ValArr.length == LocArr.length)
	{
		var x = 0;
		while(x < LocArr.length)
		{
			var data =
			{
				Location: LocArr[x],
				Value: ValArr[x],
				status: StatusArr[x],
				Color: "020"
			}

			Redisclient.publish('PTL_INSTRUCT', JSON.stringify(data));
			x++;
		}

		res.send({message: "Task(s) submitted."});
	}

};