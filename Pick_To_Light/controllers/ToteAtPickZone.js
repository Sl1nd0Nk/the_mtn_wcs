var config     = require('../../config/ptl');
var ptlZone    = require('../../db_schema/ptlZoneModel');
var pickList   = require('../../db_schema/pickListModel');
var async      = require('async');
var moment     = require('moment');

var EndPoint = config.ZONE_RELEASE.url;
WriteToFile('Info', 'Startup: ' + EndPoint);

exports.ToteAtPickZone = function(req, res){
	let ZoneID = req.body.Location;
	let Tote = req.body.ToteId;

	WriteToFile('Info','New data received from conveyor -- [Zone ' + ZoneID + '][Tote ' + Tote + ']');

  	if(!ZoneID || !Tote){
		return res.send({message: "invalid request"});
	}

	ConstructZoneData(ZoneID, Tote, function(err, data){
		if(err){
			return res.send({message: "Failed to submit task"});
		}

		return res.send({message: "Task submitted."});
	});
};

function ConstructZoneData(ZoneID, Tote, callback){
	let Stack = {};

	Stack.Zone = function(callback){
		ptlZone.findOne({'ZoneID': ZoneID}, function(err, Zone){
			return callback(err, Zone);
		});
	}

	Stack.PickList = function(callback){
		pickList.findOne({'toteID': Tote}, function(err, PL){
			return callback(err, PL);
		});
	}

	async.parallel(Stack, function(err, Result){
		if(err){
			WriteToFile('Error', err);
			return callback(err, null);
		}

		let Zone = Result.Zone;
		let ValidToLogin = true;
		if(!Zone){
			WriteToFile('Error','Failed To Find Zone ' + ZoneID);
			ValidToLogin = false;
		}

		let PickList = Result.PickList;
		if(!PickList){
			WriteToFile('Error','Failed To Find PickList for Tote ' + Tote);
			ValidToLogin = false;
		}

		let Orders = null;
		if(PickList && ValidToLogin){
			Orders = PickList.orders;
			if(PickList && !Orders){
				WriteToFile('Error','No Orders Found On PickList In Tote ' + Tote);
				ValidToLogin = false;
			}
		}

		if(Zone.ToteID == Tote){
			WriteToFile('Error','Failed To Add Tote ' + Tote + ' To Zone ' + ZoneID + '. Tote Already At Zone.');
			return callback('Failed To Add Tote ' + Tote + ' To Zone ' + ZoneID + '. Tote Already At Zone.', null);
		}

		let PickLocs = null;
		if(ValidToLogin){
			PickLocs = ConstructPickLocs(Orders, Zone.ZoneID, Tote);
			let ResultData = CheckIfTheresAnythingToPick(PickLocs, Tote);
			ValidToLogin = ResultData.SomethingToPick;
			PickLocs = ResultData.PickLocs;
		}

		if(!ValidToLogin){
			WriteToFile('Error','No Locations to pick in zone ' + ZoneID + ' for tote ' + Tote);
			Zone.Status = 'DONE';
			Zone.ZoneError = true;
		} else {
			WriteToFile('Info','There Are Locations to pick in zone ' + ZoneID + ' for tote ' + Tote);
			Zone.Status = 'LOGIN';
			Zone.ZoneError = false;
		}

		Zone.UserID = null;
		Zone.ToteID = Tote;
		Zone.Transmitted = false;
		Zone.ZoneCompleted = false;
		if(ValidToLogin){
			Zone.PickLocations = AddToZonePickLocs(Zone.PickLocations, PickLocs);
		}
		Zone.ToteInTime = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');

		Zone.save(function(error, savedDoc){
			if(error){
				return callback(error, null);
			}

			return callback(null, savedDoc);
		});
	});
} /* ConstructZoneData */

function AddToZonePickLocs(ZonePickLocations, PickLocs){
	let ZLocations = ZonePickLocations;
	let x = 0;

	while(x < ZLocations.length){
		ZLocations[x].Transmitted = false;
		ZLocations[x].LocStatus = 'CLEAR';
		x++;
	}

	x = 0;
	while(x < PickLocs.length){
		ZLocations.push(PickLocs[x]);
		x++;
	}

	return ZLocations;
} /* AddToZonePickLocs */

function CheckIfTheresAnythingToPick(PickLocs){
	let SomethingToPick = false;

	if(!PickLocs || PickLocs.length <= 0){
		return {SomethingToPick: SomethingToPick, PickLocs: PickLocs};
	}

	let x = 0;
	while(x < PickLocs.length){
		if(PickLocs[x].PickQty > PickLocs[x].PickedQty && !PickLocs[x].ShortPicked && !SomethingToPick){
			SomethingToPick = true;
		}

		if(PickLocs[x].PickQty == PickLocs[x].PickedQty){
			PickLocs[x].LocStatus = 'CLEAR';
		}

		if(PickLocs[x].PickQty != PickLocs[x].PickedQty && PickLocs[x].ShortPicked){
			PickLocs[x].LocStatus = 'CLEAR';
		}
		x++;
	}

	return {SomethingToPick: SomethingToPick, PickLocs: PickLocs};
} /* CheckIfTheresAnythingToPick */

function ConstructPickLocs(Orders, ZoneID, ToteID){
	let x = 0;
	let PickLocs = [];
	while(x < Orders.length){
		let Items = Orders[x].items;
		let y = 0;

		while(y < Items.length){
			if(parseInt(Items[y].pickZone) == ZoneID && Items[y].qty > 0){
				let Loc = PTL_GetPtlFriendlyLocName(Items[y].location);
				let z = 0;
				while(z < PickLocs.length){
					if(PickLocs[z].Location == Loc && PickLocs[z].ToteID == ToteID){
						PickLocs[z].PickQty += Items[y].qty;
						PickLocs[z].PickedQty += (Items[y].pickQty == null)? 0:Items[y].pickQty;
						PickLocs[z].ShortPicked = (PickLocs[z].ShortPicked == true)? PickLocs[z].ShortPicked: Items[y].shortPick;
						break;
					}
					z++;
				}

				if(z >= PickLocs.length){
					let LocRec = {
						ToteID: ToteID,
						Location: Loc,
						PickQty: Items[y].qty,
						PickedQty: (Items[y].pickQty == null)? 0:Items[y].pickQty,
						ShortPicked: Items[y].shortPick,
						LocStatus: 'NEW',
						Transmitted: false
					}

					PickLocs.push(LocRec);
				}
			}
			y++;
		}
		x++;
	}

	return PickLocs;
} /* ConstructPickLocs */

function PTL_GetPtlFriendlyLocName(PickLocation){
	let PTL_Loc_Type = '';
  	let TempLoc = [];
  	TempLoc = PickLocation.split('-');

  	let q = 0;
  	while(q < TempLoc.length){
    	if(q == 0){
	  		PTL_Loc_Type = TempLoc[q];
		} else {
	  		PTL_Loc_Type = PTL_Loc_Type + TempLoc[q];
		}
    	q++;
  	}

  	return PTL_Loc_Type;
} /* PTL_GetPtlFriendlyLocName */


function WriteToFile(logtype, Message){
	let now = new Date();
	console.log(moment(now).format('YYYY-MM-DD HH:mm:ss') + ": [" + logtype + "] -> " + Message);
} /* WriteToFile */
