var express     = require('express');
var port        = require('vsu').port;
var bodyParser  = require('body-parser');
var app         = express();

var log4js      = require('log4js');
var logger      = log4js.getLogger('VSU SERVER');

logger.level    = 'debug';

app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(bodyParser.urlencoded({ extended: true }));
require('./appRoutes/expressRoutes')(app);

app.listen(port, function(){
    logger.debug('Listening on port: ', port);
});