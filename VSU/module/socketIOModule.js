/**
 * Created by Ntokozo on 2017-02-03.
 */
var log4js = require('log4js');
var logger = log4js.getLogger('SOCKETIO MODULE');
var net = require('net');
var config =require('vsu').socketIOClient;
var heartbeats = require('heartbeats');

logger.level = 'debug';
var heart = heartbeats.createHeart(10000);

var port = config.port;
var host = config.host;

var createClient=function(){};

console.log(host)

createClient.prototype.createConnect=function (reqData, callback) {
    var client = new net.Socket();
    var responseSent = false

    //create connection to server
    client.connect(port, host, function () {
        logger.debug('connected to: ', host);
        var reqString = reqData+'\r\n';
        logger.info('________REQSTRING___________: ', reqString);
        client.write(reqString);
    });

    //response from server
    client.on('data', function (data) {
        logger.debug('Data received: ', data.toString('utf8'));
        heart.createEvent(1, function(count, last){
            //callback(data);
            //if(last){
                //callback(data);
                //client.destroy();
                console.log('_____________: ', data.toString('utf8'));
                if(data.toString('utf8').substr(0,1) == '0' || data.toString('utf8').substr(0,3) == '203'){
                    client.destroy();

                    // callback(data.toString('utf8'));
		    if(!responseSent){
                        responseSent = true
                        return callback(data.toString('utf8'));
                    }
                }
            //}
        });
    });

    //close connection
    client.on('close', function(){
        logger.debug('CONNECTION CLOSED');
    });
};

module.exports = new createClient();
