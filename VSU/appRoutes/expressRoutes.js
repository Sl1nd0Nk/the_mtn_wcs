var log4js      = require('log4js');
var logger      = log4js.getLogger('ROUTES');
var waterfall   = require('async-waterfall');
var async       = require('async');
var Client      = require('../module/socketIOModule');

logger.level    = 'debug'


var prototype = '1|HOST ID|L01-1|1|1|1|12|OrderID|ProductID|ProductDesc';
var opening;
var tray;
var position;
var istruction;

module.exports=function(app){
    app.post('/vsuReq', function (req, res) {
        var body = req.body;
        logger.debug('Location: ' + body.location)
        console.log('Location: ' + body.location)
        console.log('DESC: ' + body.desc)
        generateInstr(body.location, body.desc, function(instr){
            if(instr){
                Client.createConnect(instr, function(data){
                    res.send({code:'00', message:'success', data: instr, resp:data});
                });
            }
        });
    });
}

function generateInstr(location, desc, callback){
    if(location){
        if(location.substr(0,3) === 'VSU'){
            async.waterfall([getTrayNumber(location, desc), getPosition], function(err, success){
                if(err){
                    logger.debug('Something is wrong! ')
                }else{
                    callback(success);
                }
            });
        }
    }
}

function getTrayNumber(location, desc, callback){
    return function(callback){
        if(location.substr(10, 1) == '0'){
            var tray = location.substr(11,1);
            callback(null, tray, location, desc);
        }else{
            var tray = location.substr(10,2);
            callback(null, tray, location, desc);
        }
    }
}

function getPosition(tray, location, desc, callback){
    if(location.substr(13,1) == '0'){
        position = location.substr(14,1);
        istruction = '1|HOST ID|L01-2|'+tray+'|'+position+'|1|12|1234|x96324#|'+desc;
        callback(null, istruction);
    }else{
        position = location.substr(13,2);
        istruction = '1|HOST ID|L01-2|'+tray+'|'+position+'|1|12|1234|x96324#|'+desc;
        callback(null, istruction);
    }
}

// var istruction = '1|HOST ID|'+ 'L01-'+location.substr(3,1) + '|'+location.substr(6,1)+'|'+location.substr(10,1)+'|1|12|1234|x96324#|Iphone 6 plus';

//VSU1-T1-P01

//VSUN01Z14T01P01

//1|HOST ID|L01-1|1|1|1|12|Order ID|Product ID|Product Desc

//1         - instr to send
//HOST ID   - Connection Name
//L01-1     - Machine number 1, Opening/channel 1     |     /*VSU1*/
//1         - Tray number.    |      /*T1*/
//1         - Position on Tray      |       P01
//1         - Depth     |   Always 1
//12        - Quantity  |   Get this as well.
//Order ID 


//API : location, barcode, qty, picklist 
