/*require('appdynamics').profile({
    controllerHostName: '10.20.88.95',
    controllerPort: 8090,
    accountName: 'customer1',
    accountAccessKey: '91f9b93a-0a53-4caa-8bde-f4f8328773dc',
    applicationName: 'Midrand Prod WCS',
    tierName: 'NodeJS',
    nodeName: 'Pick Trolley',
    debug: true,
logging:{
   'logfiles': [{
   'root_directory':'/tmp/appd',
   'filename':'echo_%N.log',
   'level': 'DEBUG',
   'max_size': 10000,
   'max_files': 1
}]
}
});*/

var async       = require('async');
var express		= require('express');
var app			= express();
var server		= require("http").Server(app);
var fs			= require('fs');
var mDB			= require('pick_trolley').MongoDB
var formidable	= require("formidable");
var rest        = require('restler');
var session		= require('client-sessions');
var moment		= require('moment');
var mongoose	= require('mongoose');
var UserModel	= require('userModel');
var ParModel	= require('parameterModel');
var PLModel		= require('pickListModel');
var TrModel		= require('pickTrolleyModel');
var PickLine    = require('PickLineUpdatesModel');
templatesjs		= require('templatesjs');

var pickListLib	= require('classPicklist');
var pickList    = new pickListLib();

var config      = require('ptl');
var EndPoint    = config.ZONE_RELEASE.url;

mongoose.connect(mDB, {useMongoClient: true}, function(err) {
	if (err) {
		WriteToFile('Error connecting to Mongo DB: ' + err);
	} else {
		WriteToFile('Successfully connected to Mongo DB ');
		app.listen(8087);
		WriteToFile("pick trolley server is listening on port " + 8087);
	}
});

app.use(session( {
	cookieName:		'session',
	secret:			'eg[isfd-8yF9-7w2315df{}+Ijsli;;to8',
	duration:		30 * 60 * 1000,
	activeDuration: 5 * 60 * 1000,
	httpOnly:		true,
	secure:			true,
	ephemeral:		true
}));

app.use(express.static(__dirname + '/public'));

var htmlFields	= {};

function initList() {
	htmlFields.ShowAlert     =   "//";
	htmlFields.endPoint      =   "";
	htmlFields.Logout        =   "<!--";
	htmlFields.EndLogout     =   "-->";
	htmlFields.Login         =   "<!--";
	htmlFields.EndLogin      =   "-->";
	htmlFields.Idle          =   "<!--";
	htmlFields.EndIdle       =   "-->";
	htmlFields.ScannedTrlID  =	 "<!-- -->";
	htmlFields.ScannedZone  =	 "<!-- -->";
	htmlFields.ScannedLocID  =	 "<!-- -->";
	htmlFields.ErrorMessage  =	 "<!-- -->";
	htmlFields.Instruction   =	 "<!-- -->";
	htmlFields.Input         =   "<!--";
	htmlFields.EndInput      =   "-->";
	htmlFields.ReleaseBtn    =   "<!--";
	htmlFields.EndReleaseBtn =	 "-->";
	htmlFields.ChangeZoneBtn =   "<!--";
	htmlFields.EndChangeZoneBtn = "-->";
	htmlFields.TrMode        =   "<!-- -->";

	return htmlFields;
} /* initList */

function ValidateScannedTrolley(ScannedTrolley, UserID, callback) {
	TrModel.findOne({'trolleyID': ScannedTrolley}, function(err, Trolley) {
		if (err) {
			WriteToFile("Err while trying to validate scanned trolley " + ScannedTrolley + ' | Err: ' + err);
			return callback({Err: "Internal Server Error"});
		} else {
			if(Trolley){
				if(Trolley.trolleyInUseBy == null || Trolley.trolleyInUseBy == '' || Trolley.trolleyInUseBy == UserID){
					Trolley.trolleyInUseBy = UserID;
					Trolley.save(function(err, SavedDoc) {
						if (err || !SavedDoc) {
							WriteToFile("error saving pickTrolley for trolleyID '" + Trolley.trolleyID + " | Err: " + err);
							return callback({Err: "Internal server error"});
						} else {
							return callback({Trolley: SavedDoc});
						}
					});
				} else {
					WriteToFile("Attempt by user " + UserID + " to use trolley " + ScannedTrolley + ' which is currently in-use by ' + Trolley.trolleyInUseBy);
					return callback({Err: "Trolley " + ScannedTrolley + " is in use by: " + Trolley.trolleyInUseBy});
				}
			} else {
				WriteToFile("Scanned Trolley " + ScannedTrolley + ' does not exist on the system');
				return callback({Err: "Invalid trolley scanned"});
			}
		}
	});
} /* ValidateScannedTrolley */

function WEB_CallPageRedirect(res, Url){
	res.statusCode = 302;
	res.setHeader('Location', Url);
	res.end();
} /* WEB_CallPageRedirect */

function WriteToFile(Msg){
	if(Msg){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' ' + Msg);
	}
} /* WriteToFile */

function requireLogin (req, res, next){
  if (!req.session.username || !req.session.Trolley){
	 let list = initList();
     ShowLogin(req, res, list);
  } else {
    next();
  }
};

app.get('/login', function(req, res) {
	WEB_CallPageRedirect(res, '/');
});

app.post('/login', function(req, res) {
	var form = new formidable.IncomingForm();
	form.parse(req, function (err, fields, files) {
		if (fields.uid != null && fields.uid != "" && fields.psw != null && fields.psw != "") {
			if ((fields.trlid != null) && (fields.trlid != "")) {
				UserModel.findOne({username: fields.uid, password: fields.psw}, function(err, doc) {
					if (!err && doc) {
						ValidateScannedTrolley(fields.trlid, fields.uid, function(result) {
							if(result.Err){
								req.session.Err	= result.Err;
							} else {
								req.session.user = doc.name;
								req.session.username = doc.username;
								req.session.Trolley = result.Trolley.trolleyID;
							}

							WEB_CallPageRedirect(res, '/');
						});
					} else {
						WriteToFile('User supplied incorrect login info');
						req.session.Err	= "Invalid username or password";
						WEB_CallPageRedirect(res, '/');
					}
				});
			} else {
				WriteToFile('No trolley id supplied by user ' + fields.uid);
				req.session.Err = "Invalid trolley id";
				WEB_CallPageRedirect(res, '/');
			}
		} else {
			WriteToFile('User supplied incomplete login info');
			req.session.Err = "Invalid username or password";
			WEB_CallPageRedirect(res, '/');
		}
	});
});

app.post('/logout', requireLogin, function(req, res) {
	req.session.logout = true;
	WEB_CallPageRedirect(res, '/');
});

app.get('/idle', requireLogin, function(req, res) {
	WEB_CallPageRedirect(res, '/');
});

app.post('/idle', requireLogin, function(req, res) {
	WEB_CallPageRedirect(res, '/');
});

app.get('/', function(req, res) {
	ProcessRoot(req, res);
});

app.post('/', function(req, res) {
	ProcessRoot(req, res);
});

app.get('/selectMode', requireLogin, function(req, res) {
	WEB_CallPageRedirect(res, '/');
});

app.post('/selectMode', requireLogin, function(req, res) {
	var form = new formidable.IncomingForm();
	form.parse(req, function (err, fields, files) {
		if(fields.input == null || fields.input.trim == "") {
			req.session.Err	= "Invalid input";
			WEB_CallPageRedirect(res, '/');
		} else {
			validatePickingMode(fields.input, function(result) {
				if (result.Err) {
					req.session.Err	= result.Err;
					WEB_CallPageRedirect(res, '/');
				} else {
					TrModel.findOne({trolleyID: req.session.Trolley}, function(err, Trl) {
						if (err && !Trl) {
							WriteToFile("error reading trolley in .post('/selectMode'");
							req.session.Err = "Internal server error";
							WEB_CallPageRedirect(res, '/');
						} else {
							Trl.pickMode = result.mode;
							Trl.save(function(err, savedTrl) {
								if (err || !savedTrl) {
									WriteToFile("error saving pickTrolley for trolleyID '" + Trl.trolleyID + "' in .post('/selectMode'");
									req.session.Err	= "Internal server error";
								}

								WEB_CallPageRedirect(res, '/');
							});
						}
					});
				}
			});
		}
	});
});

app.get('/addTote', requireLogin, function(req, res) {
	WEB_CallPageRedirect(res, '/');
});

app.post('/addTote', requireLogin, function(req, res) {
	var form = new formidable.IncomingForm();
	form.parse(req, function(err, fields, files) {
		if(fields.input == null || fields.input.trim == "") {
			req.session.Err	= "Invalid input";
		} else {
			req.session.ScannedTote = fields.input;
		}

		WEB_CallPageRedirect(res, '/');
	});
});

app.get('/addZone', requireLogin, function(req, res) {
	WEB_CallPageRedirect(res, '/');
});

app.post('/addZone', requireLogin, function(req, res) {
	var form = new formidable.IncomingForm();
	form.parse(req, function(err, fields, files) {
		if(fields.input == null || fields.input.trim == "") {
			req.session.Err	= "Invalid input";
		} else {
			req.session.ScannedZoneLoc = fields.input;
		}

		WEB_CallPageRedirect(res, '/');
	});
});

app.get('/newLocation', requireLogin, function(req, res) {
	WEB_CallPageRedirect(res, '/');
});

app.post('/newLocation', requireLogin, function(req, res) {
	var form = new formidable.IncomingForm();
	form.parse(req, function (err, fields, files) {
		if(fields.input == null || fields.input == "") {
			req.session.Err	= "Invalid input";
		} else {
			req.session.ScannedLoc = fields.input;
		}

		WEB_CallPageRedirect(res, '/');
	});
});

app.get('/confirmItemQty', requireLogin, function(req, res) {
	WEB_CallPageRedirect(res, '/');
});

app.post('/confirmItemQty', requireLogin, function(req, res) {
	var form = new formidable.IncomingForm();
	form.parse(req, function (err, fields, files) {
		if(fields.input == null || fields.input.trim == "") {
			req.session.Err	= "Invalid input";
		} else {
			req.session.EnteredQty = fields.input;
		}

		WEB_CallPageRedirect(res, '/');
	});
});

app.get('/confirmItemTote', requireLogin, function(req, res) {
	WEB_CallPageRedirect(res, '/');
});

app.post('/confirmItemTote', requireLogin, function(req, res) {
	var form = new formidable.IncomingForm();
	form.parse(req, function (err, fields, files) {
		if(fields.input == null || fields.input.trim == "") {
			req.session.Err	= "Invalid input";
		} else {
			req.session.ConfirmTote = fields.input;
		}
		WEB_CallPageRedirect(res, '/');
	});
});

app.get('/releaseTrolley', requireLogin, function(req, res) {
	WEB_CallPageRedirect(res, '/');
});

app.post('/releaseTrolley', requireLogin, function(req, res){
	req.session.ReleaseTotes = true;
	WEB_CallPageRedirect(res, '/');
});

app.get('/ChangeZone', requireLogin, function(req, res) {
	WEB_CallPageRedirect(res, '/');
});

app.post('/ChangeZone', requireLogin, function(req, res){
	req.session.ChangeZone = true;
	WEB_CallPageRedirect(res, '/');
});

function ProcessRoot(req, res){
	let list = initList();

	if(req.session.Err){
		list.ErrorMessage = req.session.Err;
		delete req.session.Err;
	}

	if (!req.session.username || !req.session.Trolley) {
		ShowLogin(req, res, list);
	} else {
		TrModel.findOne({'trolleyID': req.session.Trolley}, function(err, Trl) {
			if(!err && Trl) {
				list.ScannedTrlID = Trl.trolleyID;
                list.Logout = "";
				list.EndLogout = "";

				if(Trl.Zone && Trl.pickMode && Trl.pickMode.name){
					list.ScannedZone = Trl.Zone;

					if(Trl.totes.length <= 0){
						list.ChangeZoneBtn =   "";
						list.EndChangeZoneBtn = "";
					}
				}

				if(Trl.trolleyInUseBy == null || Trl.trolleyInUseBy == ''){
					Trl.trolleyInUseBy = req.session.username;
					Trl.Location = null;
					UpdateTrolley(Trl, function(NewTr){
						if(NewTr){
							ProcessTrolleyActivity(req, res, list, NewTr);
						} else {
							req.session.Err = "Internal server error";
							WEB_CallPageRedirect(res, '/');
						}
					});
				} else {
					ProcessTrolleyActivity(req, res, list, Trl);
				}
			} else {
				req.session.Err	= "session trolley is not valid";
				delete req.session.Trolley;
				WEB_CallPageRedirect(res, '/');
			}
		});
	}
} /* ProcessRoot */

function ShowLogin(req, res, list) {
	list.endPoint = "login";
	list.Login	  = "";
	list.EndLogin = "";

	SetAndRenderResponse(list, req, res);
} /* ShowLogin */

function SetAndRenderResponse(list, req, res) {
	if (list.TrMode != "<!-- -->") {
		list.TrMode = '<label style="color: grey">[ ' + list.TrMode + ' ]</label><hr/>';
	}

	if (list.ScannedTrlID != "<!-- -->") {
		list.ScannedTrlID = '<label style="color: blue">Trolley: ' + list.ScannedTrlID + ' </label><hr/>';
	}

	if (list.ScannedZone != "<!-- -->") {
		list.ScannedZone = '<label style="color: blue">Zone: ' + list.ScannedZone + ' </label><hr/>';
	}

	if (list.ScannedLocID != "<!-- -->") {
		list.ScannedLocID = '<label style="color: blue">Loc: ' + list.ScannedLocID + '</label><hr/>';
	}

	if (list.ErrorMessage != "<!-- -->") {
		list.ErrorMessage = '<label style="color: red">' + list.ErrorMessage + '</label><br/>';
	}

	if (list.Instruction  != "<!-- -->") {
		list.Instruction  = '<label style="color: green">' + list.Instruction  + '</label><br/>';
	}

	fs.readFile("./public/pickTrolley.html", function(err, data) {
		if(err) throw err;

		templatesjs.set(data, function(err, data) {
			if(err) throw err;
			templatesjs.renderAll(list, function(err, data) {
				if(err) throw err;
				res.write(data);
				res.end();
			});
		});
	});
} /* SetAndRenderResponse */

function SendCommandToPLC(Trolley, PL, callback){
	rest.post(EndPoint,{
		data: { toteID: PL.toteID, location: Trolley.Zone},
	}).on('complete', function(data, response){
		if(response){
			if (response.statusCode == 200){
				WriteToFile('ZONE RELEASE -> [' + data + ']');
			}
		} else {
			WriteToFile('No response at END POINT');
		}

		return callback();
	});
} /* SendCommandToPLC */

function ProcessTrolleyActivity(req, res, list, Trolley) {
	if(req.session.logout){
		delete req.session.logout;
		delete req.session.username;
		delete req.session.Trolley;

		let OkToClearMode = true;
		if(Trolley.totes){
			if(Trolley.totes.length > 0){
				OkToClearMode = false;
			}
		}

		if(OkToClearMode == true){
			Trolley.pickMode.name = null;
			Trolley.pickMode.isDefaultMode = false;
			Trolley.pickMode.isActive = false;
			Trolley.pickMode.instructionMsgFinal = null;
			Trolley.pickMode.toteIDLeadDigit = null;
		}

		Trolley.trolleyInUseBy = null;

		UpdateTrolley(Trolley, function(NewTr){
			WEB_CallPageRedirect(res, '/');
		});
	} else {
		if(Trolley.pickMode.name == null) {
			ShowModeSelection(req, res, list, Trolley);
		} else {
			list.TrMode = Trolley.pickMode.name;
			if(req.session.ScannedTote){
				let ToteId = req.session.ScannedTote;
				delete req.session.ScannedTote;
				validateTote(Trolley, ToteId, function(Resp){
					if(Resp.Err){
						req.session.Err	= Resp.Err;
					}

					if(Resp.InvalidTote && Trolley.Zone){
						FinalisePickedTotes(req, res, list, Trolley);
					} else {
						WEB_CallPageRedirect(res, '/');
					}
				});
			} else {
				if(req.session.ScannedLoc){
					let ScannedLoc = req.session.ScannedLoc;
					delete req.session.ScannedLoc;

					if(ScannedLoc == Trolley.Location){
						Trolley.LocConfirmed = true;
						Trolley.LocQtyConfirmed = false;
						Trolley.LocQty = null;
						UpdateTrolley(Trolley, function(NewTr){
							WEB_CallPageRedirect(res, '/');
						});
					} else {
						req.session.Err	= "Scanned location is not the requested location";
						WEB_CallPageRedirect(res, '/');
					}
				} else {
					if(req.session.EnteredQty){
						let EnteredQty = req.session.EnteredQty;
						delete req.session.EnteredQty;

						if(EnteredQty == Trolley.LocQty){
							Trolley.LocQtyConfirmed = true;
							UpdateTrolley(Trolley, function(NewTr){
								WEB_CallPageRedirect(res, '/');
							});
						} else {
							req.session.Err	= "Invalid quantity entered";
							WEB_CallPageRedirect(res, '/');
						}
					} else {
						if(req.session.ConfirmTote){
							let ConfirmTote = req.session.ConfirmTote;
							delete req.session.ConfirmTote;
							if(ConfirmTote == Trolley.ToteToConfirm){
								ConfirmQtyIntoTote(Trolley, ConfirmTote, function(Resp){
									if(Resp){
										if(Resp.Err){
											req.session.Err	= Resp.Err;
										}
									}

									WEB_CallPageRedirect(res, '/');
								});
							} else {
								req.session.Err	= 'Scanned Tote Is Not The Requested Tote To Confirm';
								WEB_CallPageRedirect(res, '/');
							}
						} else {
							if(req.session.ReleaseTotes){
								delete req.session.ReleaseTotes;

								if(Trolley.Zone){
									ReleaseZoneTote(Trolley, function(Resp){
										if(Resp.Err){
											req.session.Err	= Resp.Err;
											WEB_CallPageRedirect(res, '/');
										} else {
											if(Resp.PL){
												Trolley.totes = [];
												Trolley.PickingStarted = false;
												Trolley.Location = null;
												UpdateTrolley(Trolley, function(NewTr){
													SendCommandToPLC(Trolley, Resp.PL, function(){
														WEB_CallPageRedirect(res, '/');
													});
												});
											}
										}
									});
								} else {
									ReleaseTotesFromTrolley(Trolley, function(Resp){
										if(Resp.Err){
											req.session.Err	= Resp.Err;
											WEB_CallPageRedirect(res, '/');
										} else {
											if(Resp.Released == true){
												Trolley.totes = [];
												Trolley.PickingStarted = false;
												Trolley.Location = null;
												UpdateTrolley(Trolley, function(NewTr){
													WEB_CallPageRedirect(res, '/');
												});
											}
										}
									});
								}
							} else {
								if(Trolley.pickMode.isDefaultMode){
									if(req.session.ScannedZoneLoc){
										let ZoneLoc = req.session.ScannedZoneLoc;
										delete req.session.ScannedZoneLoc;

										let UpperZoneLoc = ZoneLoc.toUpperCase();
										let ZoneNdx = UpperZoneLoc.indexOf('Z');
										if(ZoneNdx > 0){
											let Zone = UpperZoneLoc.substr((ZoneNdx+1), 2);
											if(Zone && Zone != ''){
												let IZone = parseInt(Zone);
												if(IZone > 0 && IZone < 14){
													Trolley.Zone = IZone;

													UpdateTrolley(Trolley, function(NewTr){
  														WEB_CallPageRedirect(res, '/');
													});
												} else {
													req.session.Err	= 'Scanned location does not belong to the expected PTL zones';
													WEB_CallPageRedirect(res, '/');
												}
											} else {
												req.session.Err	= 'Scanned Zone Location Is Invalid';
												WEB_CallPageRedirect(res, '/');
											}
										} else {
											req.session.Err	= 'Scanned Zone Location Is Invalid';
											WEB_CallPageRedirect(res, '/');
										}
									} else if(req.session.ChangeZone){
										delete req.session.ChangeZone;

										Trolley.Zone = null;
										UpdateTrolley(Trolley, function(NewTr){
											WEB_CallPageRedirect(res, '/');
										});
									} else {
										if(!Trolley.Zone){
											ShowChooseAZone(req, res, list, Trolley);
										} else {
											if(Trolley.totes && Trolley.totes.length > 0){
												if(!Trolley.PickingStarted){
													StartTrolleyWork(req, res, list, Trolley);
												} else {
													ProcessTotes(req, res, list, Trolley);
												}
											} else {
												ShowAddZoneTote(req, res, list, Trolley);
											}
										}
									}
								} else {
									if(Trolley.totes){
										if(Trolley.totes.length > 0){
											ProcessTotes(req, res, list, Trolley);
										} else {
											CheckForTrolleyWork(req, res, list, Trolley);
										}
									} else {
										CheckForTrolleyWork(req, res, list, Trolley);
									}
								}
							}
						}
					}
				}
			}
		}
	}
} /* ProcessTrolleyActivity */

function ShowAddZoneTote(req, res, list, Trolley){
	list.endPoint		= "addTote";
	list.Instruction	= "Scan Tote In Zone";
	list.Input			= "";
	list.EndInput	    = "";

	SetAndRenderResponse(list, req, res);
} /* ShowAddZoneTote */

function ShowChooseAZone(req, res, list, Trolley){
	list.endPoint		= "AddZone";
	list.Instruction	= "Scan Location In Zone To Select Zone";
	list.Input			= "";
	list.EndInput	    = "";

	SetAndRenderResponse(list, req, res);
} /* ShowChooseAZone */

function TotesToRel(task, callback){
	let Tote = task;

	PLModel.findOne({'toteID': Tote}, function(err, doc) {
		if (err) {
			WriteToFile("Error while querying tote " + Tote + ". Err: " + err);
			return callback("Internal server error", null);
		} else {
			if(doc){
				doc.toteInUse = false;
				doc.toteInUseBy = null;
				doc.Status = "PICKED";
				doc.containerLocation = null;
				doc.PickUpdateRequired = true;

				doc.save(function(err, savedDoc){
					return callback(null, savedDoc);
				});
			} else {
				WriteToFile("Error while querying tote " + Tote);
				return callback("Internal server error", null);
			}
		}
	});
} /* TotesToRel */

function ReleaseZoneTote(Trolley, callback){
	let Tote = Trolley.totes[0];
	if(Tote){
	PLModel.findOne({'toteID': Tote}, function(err, doc) {
		if (err) {
			WriteToFile("Error while querying tote " + Tote + ". Err: " + err);
			return callback({Err: "Internal server error"});
		} else {
			if(doc){
				doc.toteInUse = false;
				doc.toteInUseBy = null;

				doc.save(function(err, savedDoc){
					return callback({PL: savedDoc, Err: err});
				});
			} else {
				WriteToFile("Error while querying tote " + Tote);
				return callback({Err: "Internal server error"});
			}
		}
	});
	} else {
                                WriteToFile("No Tote On Trolley");
                                return callback({Err: "Internal server error"});
	}
} /* ReleaseZoneTote */

function ReleaseTotesFromTrolley(Trolley, callback){
	let ToteArr = Trolley.totes;
	let Err = null;
	if(ToteArr.length > 0){
	var qTotesToRel  = async.queue(TotesToRel, ToteArr.length);

	qTotesToRel.push(ToteArr, function (err, data) {
		if(err){
			if(!Err){
				Err = err;
			}
		}
	});

	qTotesToRel.drain = function(){
		if(Err){
			return callback({Err: Err});
		} else {
			return callback({Released: true});
		}
	}
	} else {
		return callback({Released: true});
	}
} /* ReleaseTotesFromTrolley */

function CreatePickLineUpdate(uPL, ordNdx, lnIndx){
	PickLine.findOne({'PickListID': uPL.orders[ordNdx].pickListID,
					  'PickListLine': uPL.orders[ordNdx].items[lnIndx].pickListOrderLine,
					  'OrderId': uPL.orders[ordNdx].orderID}, function(err, PlUpdateRec){
		if(err){
			return;
		}

		if(PlUpdateRec){
			return;
		}

		let NewRec = new PickLine({
			PickListID: uPL.orders[ordNdx].pickListID,
			WcsId: uPL.toteID,
			PickListLine: uPL.orders[ordNdx].items[lnIndx].pickListOrderLine,
			OrderId: uPL.orders[ordNdx].orderID,
			Required: true,
			CreateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
		});

		NewRec.save(function(err, savedDoc){
			WriteToFile('Marked picklist: ' + uPL.orders[ordNdx].pickListID + ' | Line: ' + uPL.orders[ordNdx].items[lnIndx].pickListOrderLine + ' | PickLineRequired to true');
			return;
		});
	});
} /* CreatePickLineUpdate */

function ConfirmQtyIntoTote(Trolley, ConfirmTote, callback){
	PLModel.findOne({'toteID': ConfirmTote}, function(err, doc) {
		if (err) {
			WriteToFile("Error while querying tote " + ConfirmTote + ". Err: " + err);
			return callback({Err: "Internal server error"});
		} else {
			if(doc){
				let x = 0;
				while(x < doc.orders.length){
					let y = 0;
					while(y < doc.orders[x].items.length){
						if(doc.orders[x].items[y].location == Trolley.Location &&
						   doc.orders[x].items[y].itemCode == Trolley.Product){
							doc.orders[x].items[y].pickQty = doc.orders[x].items[y].qty;
							doc.orders[x].items[y].picked = true;
							doc.orders[x].items[y].shortPick = false;
							doc.orders[x].items[y].pickedBy = Trolley.trolleyInUseBy;
							doc.orders[x].items[y].PickLineRequired = true;
							CreatePickLineUpdate(doc, x, y);
						}
						y++;
					}

					if(OrderAllPicked(doc.orders[x]) == true){
						doc.orders[x].picked = true;
					}
					x++;
				}

				doc.save(function(err, savedDoc){
					return callback(null);
				});
			} else {
				WriteToFile("Error while querying tote " + ConfirmTote);
				return callback({Err: "Internal server error"});
			}
		}
	});
} /* ConfirmQtyIntoTote */

function OrderAllPicked(Order){
	let x = 0;
	while(x < Order.items.length){
		if(Order.items[x].picked == false){
			return false;
		}
		x++;
	}

	return true;
} /* OrderAllPicked */

function getPickingModes(callback) {
	ParModel.find({}, function(err, params) {
		if (err || !params || params.length <= 0) {
			WriteToFile("error reading parameters in getPickingModes(): " +  err + "\n\t" + "params: " + params);
			return callback({Err: "internal server error"});
		} else {
			if (params[0].trolleySettings) {
				return callback({Modes: params[0].trolleySettings});
			} else {
				WriteToFile("parameter trolleySettings.maxTotesPerTrolley not found in getPickingModes()");
				return callback({Err: "internal server error"});
			}
		}
	});
} /* getPickingModes */

function ShowModeSelection(req, res, list, Trolley){
	getPickingModes(function(result) {
		if (result.Err) {
			req.session.Err = result.Err;
			delete req.session.Trolley;
			WEB_CallPageRedirect(res, '/');
		} else {
			let TrSettings = result.Modes;
			let pm = TrSettings.pickModes;

			if(pm.length > 0){
				list.Instruction =	"Available Modes:";
				let i = 0;
				while(i < pm.length){
					if (pm[i].isActive) {
						list.Instruction += "<br/>- " + pm[i].name;
					}

					if (pm[i].isDefaultMode) {
						req.session.PickModeDefault = pm[i].name;
					}
					i++;
				}

				list.Instruction += "<hr/>select mode";
				list.endPoint = "selectMode";
				list.Input = "";
				list.EndInput = "";

				SetAndRenderResponse(list, req, res);
			} else {
				req.session.Err = "No available mode to use on picktrolley";
				delete req.session.Trolley;
				WEB_CallPageRedirect(res, '/');
			}
		}
	});
} /* ShowModeSelection */

function UpdateTrolley(Trolley, callback){
	Trolley.save(function(err, savedDoc){
		return callback(savedDoc);
	});
} /* UpdateTrolley */

function FindWorkForTrolley(Trolley, callback){
	let DeStack = {};

	DeStack.PLCount = function(callback){
		let Query = null;
		if(Trolley.pickMode.isDefaultMode == true){
			Query = {'toteID': { $in: [null, '']}, 'toteInUse': false, 'toteInUseBy': { $in: [null, '']}, 'Status': 'NEW', 'PickRegion': { $in: [null, Trolley.pickMode.name]}};
		} else {
			Query = {'toteID': { $in: [null, '']}, 'toteInUse': false, 'toteInUseBy': { $in: [null, '']}, 'Status': 'NEW', 'PickRegion': Trolley.pickMode.name};
		}

		PLModel.count(Query, function(err, PLCount) {
			return callback(err, PLCount);
		});
	}

	DeStack.maxTotesPerTrolley = function(callback){
		ParModel.find({}, function(err, params) {
			if (err || !params || params.length <= 0 || !params[0].trolleySettings) {
				WriteToFile("error reading parameters " +  err);
				return callback(err, null);
			} else {
				if (!params[0].trolleySettings) {
					WriteToFile("error reading trolleySettings in parameters");
					return callback("Internal server error", null);
				} else {
					if(params[0].trolleySettings.maxTotesPerTrolley){
						let maxTotesPerTrolley = params[0].trolleySettings.maxTotesPerTrolley;
						return callback(null, maxTotesPerTrolley);
					} else {
						WriteToFile("error reading maxTotesPerTrolley in trolleySettings");
						return callback("Internal server error", null);
					}
				}
			}
		});
	}

	async.parallel(DeStack, function(err, Result){
		if(err){
			return callback({Err: "Internal server error"});
		} else {
			return callback({PLCount: Result.PLCount, maxTotesPerTrolley: Result.maxTotesPerTrolley});
		}
	});
} /* FindWorkForTrolley */

function CheckForTrolleyWork(req, res, list, Trolley){
	FindWorkForTrolley(Trolley, function(Resp){
		if(Resp.Err){
			req.session.Err = Resp.Err;
			WEB_CallPageRedirect(res, '/');
		} else {
			if(Resp.PLCount > 0){
				list.endPoint		= "addTote";
				list.Instruction	= "Scan Tote (1) of (" + Resp.maxTotesPerTrolley + ")";
				list.Input			= "";
				list.EndInput	    = "";
			} else {
				list.endPoint		= "idle";
				list.ShowAlert		= "";
				list.Idle			= "";
				list.EndIdle	    = "";
			}

			SetAndRenderResponse(list, req, res);
		}
	});
} /* CheckForTrolleyWork */

function ProcessTotes(req, res, list, Trolley){
	if(!Trolley.PickingStarted){
		AddToteOrStartPicking(req, res, list, Trolley);
	} else {
		if(!Trolley.Location){
			if(Trolley.Zone){
				FindZoneLocToPickFrom(req, res, list, Trolley);
			} else {
				FindLocToPickFrom(req, res, list, Trolley);
			}
		} else {
			if(Trolley.LocConfirmed == false){
				list.endPoint = "newLocation";
				list.Instruction = "Proceed to location: " + Trolley.Location + "<hr/>scan location";
				list.Input = "";
				list.EndInput = "";
				SetAndRenderResponse(list, req, res);
			} else {
				list.ScannedLocID = Trolley.Location;

				CheckLocIfAllPicked(Trolley, function(Resp){
					if(Resp.AllPicked == true){
						Trolley.Location = null;
						Trolley.LocConfirmed = false;
						Trolley.LocQty = null;
						Trolley.LocQtyConfirmed = false;
						UpdateTrolley(Trolley, function(NewTr){
							WEB_CallPageRedirect(res, '/');
						});
					} else {
						if(Trolley.LocQtyConfirmed == false){
							Trolley.LocQty = Resp.QtyNeeded;
							UpdateTrolley(Trolley, function(NewTr){
								list.endPoint = "confirmItemQty";
								list.Instruction = "Pick " + Resp.QtyNeeded +  " of<br/>'" + Trolley.Product + "'<hr/>confirm quantity";
								list.Input = "";
								list.EndInput = "";
								SetAndRenderResponse(list, req, res);
							});
						} else {
							GetToteToConfirmQty(Trolley, 0, function(Resp){
								if(Resp){
									if(Resp.Err){
										req.session.Err = Resp.Err;
										WEB_CallPageRedirect(res, '/');
									} else {
										Trolley.ToteToConfirm = Resp.Tote;
										UpdateTrolley(Trolley, function(NewTr){
											list.endPoint = "confirmItemTote";
											list.Instruction = "Put " + Resp.Qty + " into tote " + Resp.Tote + "<hr/>Scan tote to confirm";
											list.Input = "";
											list.EndInput = "";
											SetAndRenderResponse(list, req, res);
										});
									}
								} else {
									Trolley.ToteToConfirm = null;
									Trolley.LocQtyConfirmed = false;
									Trolley.LocQty = null;
									Trolley.Location = null;
									Trolley.LocConfirmed = false;
									Trolley.Product = null;
									UpdateTrolley(Trolley, function(NewTr){
										WEB_CallPageRedirect(res, '/');
									});
								}
							});
						}
					}
				});
			}
		}
	}
} /* ProcessTotes */

function GetToteToConfirmQty(Trolley, index, callback){
	if(index < Trolley.totes.length){
		let Tote = Trolley.totes[index];

		PLModel.findOne({'toteID': Tote}, function(err, doc) {
			if (err) {
				WriteToFile("error reading pick list | Err: " + err);
				return callback({Err: "Internal server error"});
			} else {
				if(doc){
					let Orders = doc.orders;
					let x = 0;
					let TotalToteQty = 0;
					while(x < Orders.length){
						let y = 0;
						let items = Orders[x].items;
						while(y < items.length){
							if(items[y].location == Trolley.Location && items[y].itemCode == Trolley.Product){
								TotalToteQty += (items[y].qty - (items[y].pickQty == null ? 0: items[y].pickQty));
							}
							y++;
						}

						x++;
					}

					if(TotalToteQty > 0){
						return callback({Tote: Tote, Qty: TotalToteQty});
					} else {
						index++;
						GetToteToConfirmQty(Trolley, index, callback);
					}
				} else {
					WriteToFile("error reading pick list");
					return callback({Err: "Internal server error"});
				}
			}
		});
	} else {
		return callback(null);
	}
} /* GetToteToConfirmQty */

function CheckPlLocPicked(task, callback){
	let tote = task.tote;
	let Trolley = task.Tr;

	PLModel.findOne({'toteID': tote}, function(err, doc) {
		if (err) {
			WriteToFile("Error while querying tote " + Totes[index] + ". Err: " + err);
			return callback("Internal server error", null);
		} else {
			if (doc) {
				let Orders = doc.orders;
				let x = 0;
				let TotalQty = 0;
				let AllPicked = true;
				while(x < Orders.length){
					let y = 0;
					while(y < Orders[x].items.length){
						if(Orders[x].items[y].location == Trolley.Location &&
						   Orders[x].items[y].itemCode == Trolley.Product){
							if((Orders[x].items[y].pickQty == null) ||
							   (Orders[x].items[y].pickQty != null &&
							    Orders[x].items[y].pickQty < Orders[x].items[y].qty)){
								if(AllPicked != false){
									AllPicked = false;
								}

								let QtyStillNeeded;
								if(Orders[x].items[y].pickQty == null){
									QtyStillNeeded = Orders[x].items[y].qty;
								} else {
									QtyStillNeeded = Orders[x].items[y].qty - Orders[x].items[y].pickQty;

									if(QtyStillNeeded < 0){
										QtyStillNeeded = 0;
									}
								}

								TotalQty += QtyStillNeeded;
							}
						}
						y++;
					}
					x++;
				}

				return callback(null, {AllPicked: AllPicked, QtyStillNeeded: TotalQty});
			} else {
				WriteToFile("Error while querying tote " + Totes[index]);
				return callback("Internal server error", null);
			}
		}
	});
} /* CheckPlLocPicked */

function CheckLocIfAllPicked(Trolley, callback){
	let ToteDataArr = [];
	let x = 0;
	let ErrMsg = null;
	let TotalQty = 0;
	let AllPicked = true;

	while(x < Trolley.totes.length){
		let Data = {
			tote: Trolley.totes[x],
			Tr: Trolley
		}

		ToteDataArr.push(Data);
		x++;
	}

	var qCheckPlLocPicked  = async.queue(CheckPlLocPicked, ToteDataArr.length);

	qCheckPlLocPicked.push(ToteDataArr, function (err, data) {
		if(err){
			if(!ErrMsg){
				ErrMsg = err;
			}
		} else {
			if(data){
				if(AllPicked != false){
					if(data.AllPicked == false){
						AllPicked = data.AllPicked;
					}
				}

				TotalQty += data.QtyStillNeeded;
			}
		}
	});

	qCheckPlLocPicked.drain = function(){
		if(ErrMsg){
			return callback({Err: ErrMsg});
		} else {
			if(TotalQty > 0){
				return callback({AllPicked: false, QtyNeeded: TotalQty});
			} else {
				return callback({AllPicked: true});
			}
		}
	}

} /* CheckLocIfAllPicked */

function validatePickingMode(selMode, callback) {
	if (selMode == null || selMode.trim == "") {
		WriteToFile("Invalid input: " + selMode);
		return callback({Err: "Invalid input"});
	} else {
		ParModel.find({}, function(err, params) {
			if (err || !params || params.length <= 0) {
				WriteToFile("error reading parameters in validatePickingMode(): " +  err + "\n\t" + "params: " + params);
				return callback({Err: "Internal server error"});
			} else {
				if (params[0].trolleySettings && params[0].trolleySettings.pickModes) {
					let pm = params[0].trolleySettings.pickModes;
					let i = 0;
					while ( i < pm.length ) {
						if (pm[i].isActive && pm[i].name.toLowerCase() == selMode.toLowerCase()) {
							break;
						}

						i++;
					}

					if(i < pm.length){
						return callback({mode: pm[i]});
					} else {
						WriteToFile("Invalid input: " + selMode);
						return callback({Err: "Invalid input"});
					}
				} else {
					WriteToFile("parameter trolleySettings or pickModes not found in validatePickingMode()");
					return callback({Err: "internal server error"});
				}
			}
		});
	}
} /* validatePickingMode */

function CheckIfToteNeedsToPickInZone(doc, Zone){
	let Orders = doc.orders;
	let x = 0;
	while(x < Orders.length){
		let y = 0;
		while(y < Orders[x].items.length){
			let item = Orders[x].items[y];
			let PickedQty = item.pickedBy;
			let Loc = item.location;
			let UpperLoc = Loc.toUpperCase();
			let ZoneNdx = UpperLoc.indexOf('Z');

			if(!PickedQty){
				PickedQty = 0;
			}

			if(ZoneNdx > 0){
				let LocZone = UpperLoc.substr((ZoneNdx+1), 2);
				if((Zone == parseInt(LocZone)) && (item.picked == false || PickedQty < item.qty)){
					return true;
				}
			}
			y++;
		}
		x++;
	}

	return false;
} /* CheckIfToteNeedsToPickInZone */

function validateTote(Trolley, toteid, callback) {
	if (toteid == null || toteid.trim == "" || toteid.length != 7 || toteid[0] != Trolley.pickMode.toteIDLeadDigit) {
		WriteToFile("invalid user input in .post('/addTote'");
		return callback({Err: "Invalid Tote Scanned"});
	} else {
		PLModel.findOne({'toteID': toteid}, function(err, doc) {
			if (err) {
				WriteToFile("error reading pick list in validateTote()" + err);
				return callback({Err: "Internal server error"});
			} else {
				if (doc) {
					if(Trolley.Zone){
						let ValidTote = CheckIfToteNeedsToPickInZone(doc, Trolley.Zone);

						Trolley.totes.push(doc.toteID);
						UpdateTrolley(Trolley, function(NewTr){
							if(ValidTote){
								if(NewTr){
									return callback({Trolley: NewTr});
								} else {
									return callback({Err: "Internal server error"});
								}
							} else {
								WriteToFile("Tote " + toteid + " Does Not need to pick in this zone");
								return callback({InvalidTote: true});
							}
						});
					} else {
						WriteToFile("scanned tote is in use");
						return callback({Err: "Scanned Tote is Not Unique"});
					}
				} else {
					let Totes = Trolley.totes;
					let x = 0;
					while(x < Totes.length){
						if(Totes[x] == toteid){
							break;
						}
						x++;
					}

					if(x < Totes.length){
						return callback({Err: "tote is already scanned onto this trolley"});
					} else {
						let Query = null;
						if(Trolley.pickMode.isDefaultMode == true){
							Query = {'toteID': { $in: [null, '']}, 'toteInUse': false, 'toteInUseBy': { $in: [null, '']}, 'Status': 'NEW', 'PickRegion': { $in: [null, Trolley.pickMode.name]}};
						} else {
							Query = {'toteID': { $in: [null, '']}, 'toteInUse': false, 'toteInUseBy': { $in: [null, '']}, 'Status': 'NEW', 'PickRegion': Trolley.pickMode.name};
						}

						pickList.GetPickListForPickTrolley({KeyValuePair: Query, ScannedToteID: toteid, UserID: Trolley.trolleyInUseBy, TrolleyID: Trolley.trolleyID}, function(Resp){
							if(Resp.Err){
								WriteToFile("error while querying for a new unassigned PL | Err: " + Resp.Err);
								return callback({Err: "Internal server error"});
							} else {
								if(Resp.PL){
									Trolley.totes.push(Resp.PL.toteID);
									UpdateTrolley(Trolley, function(NewTr){
										if(NewTr){
											return callback({Trolley: NewTr});
										} else {
											return callback({Err: "Internal server error"});
										}
									});
								} else {
									WriteToFile("error while querying for a new unassigned PL | Err: No data returned");
									return callback({Err: "Internal server error"});
								}
							}
						});
					}
				}
			}
		});
	}
} /* validateTote */

function AddToteOrStartPicking(req, res, list, Trolley){
	ParModel.find({}, function(err, params) {
		if (err || !params || params.length <= 0 || !params[0].trolleySettings) {
			WriteToFile("error reading parameters " +  err);
			req.session.Err = "Internal server error";
			WEB_CallPageRedirect(res, '/');
		} else {
			if (!params[0].trolleySettings) {
				WriteToFile("error reading trolleySettings in parameters");
				req.session.Err = "Internal server error";
				WEB_CallPageRedirect(res, '/');
			} else {
				if(params[0].trolleySettings.maxTotesPerTrolley){
					let maxTotesPerTrolley = params[0].trolleySettings.maxTotesPerTrolley;
					let AddToTrolley = false;
					if(Trolley.totes.length < maxTotesPerTrolley){
						AddToTrolley = true;
					}

					if(AddToTrolley == true){
						FindWorkForTrolley(Trolley, function(Resp){
							if(Resp.Err){
								req.session.Err = Resp.Err;
								WEB_CallPageRedirect(res, '/');
							} else {
								if(Resp.PLCount > 0){
									list.endPoint		= "addTote";
									list.Instruction	= "Scan Tote (" + (Trolley.totes.length +1) + ") of (" + Resp.maxTotesPerTrolley + ")";
									list.Input			= "";
									list.EndInput	    = "";
									SetAndRenderResponse(list, req, res);
								} else {
									StartTrolleyWork(req, res, list, Trolley);
								}
							}
						});
					} else {
						StartTrolleyWork(req, res, list, Trolley);
					}
				} else {
					WriteToFile("error reading maxTotesPerTrolley in trolleySettings");
					req.session.Err = "Internal server error";
					WEB_CallPageRedirect(res, '/');
				}
			}
		}
	});
} /* AddToteOrStartPicking */

function StartTrolleyWork(req, res, list, Trolley){
	let Stack = {};

	Stack.Trolley = function(callback){
		Trolley.PickingStarted = true;
		Trolley.Location = null;
		UpdateTrolley(Trolley, function(NewTr){
			if(NewTr){
				return callback(null, NewTr);
			} else {
				return callback("Internal server error", null);
			}
		});
	}

	Stack.TrolleyTotes = function(callback){
		StartTrolleyTotes(Trolley, function(){
			return callback(null, 'PLs status changed to STARTED');
		});
	}

	async.parallel(Stack, function(err, Result){
		if(err){
			req.session.Err = err;
			WEB_CallPageRedirect(res, '/');
		} else {
			ProcessTrolleyActivity(req, res, list, Result.Trolley);
		}
	});
} /* StartTrolleyWork */

function StartTotes(task, callback){
	let Tote = task;

	PLModel.findOne({'toteID': Tote}, function(err, doc) {
		if (err) {
			WriteToFile("error reading pick list | Err: " + err);
			return callback("Internal server error", null);
		} else {
			if (doc) {
				doc.Status = "STARTED";
				doc.save(function(err, savedDoc){
					return callback(err, savedDoc);
				});
			} else {
				WriteToFile("error reading pick list");
				return callback("Internal server error", null);
			}
		}
	});
} /* StartTotes */

function StartTrolleyTotes(Trolley, callback){
	var qStartTotes  = async.queue(StartTotes, Trolley.totes.length);

	qStartTotes.push(Trolley.totes, function (err, data) {
	});

	qStartTotes.drain = function(){
		return callback();
	}
} /* StartTrolleyTotes */

function FindLocToPickFrom(req, res, list, Trolley){
	let x = 0;
	let Totes = Trolley.totes;

	CheckEachTote(Totes, 0, null, function(Resp){
		if(Resp.Err){
			req.session.Err = Resp.Err;
			WEB_CallPageRedirect(res, '/');
		} else {
			if(Resp.Loc){
				Trolley.Location = Resp.Loc;
				Trolley.Product = Resp.Prod;
				Trolley.LocConfirmed = false;
				UpdateTrolley(Trolley, function(NewTr){
					WEB_CallPageRedirect(res, '/');
				});
			} else {
				FinalisePickedTotes(req, res, list, Trolley);
			}
		}
	});
} /* FindLocToPickFrom */

function FindZoneLocToPickFrom(req, res, list, Trolley){
	let x = 0;
	let Totes = Trolley.totes;

	CheckEachTote(Totes, 0, Trolley.Zone, function(Resp){
		if(Resp.Err){
			req.session.Err = Resp.Err;
			WEB_CallPageRedirect(res, '/');
		} else {
			if(Resp.Loc){
				Trolley.Location = Resp.Loc;
				Trolley.Product = Resp.Prod;
				Trolley.LocConfirmed = false;
				UpdateTrolley(Trolley, function(NewTr){
					WEB_CallPageRedirect(res, '/');
				});
			} else {
				FinalisePickedTotes(req, res, list, Trolley);
			}
		}
	});
} /* FindZoneLocToPickFrom */

function FinalisePickedTotes(req, res, list, Trolley){
	let Instruction = Trolley.pickMode.instructionMsgFinal;

	if(Trolley.Zone){
		Instruction = 'Picking Complete, Release To From Zone';
	}

	list.endPoint		= "releaseTrolley";
	list.Instruction	= Instruction;
	list.ReleaseBtn		= "";
	list.EndReleaseBtn	= "";
	SetAndRenderResponse(list, req, res);
} /* FinalisePickedTotes */

function CheckEachTote(Totes, index, Zone, callback){
	if(index < Totes.length){
		PLModel.findOne({'toteID': Totes[index]}, function(err, doc) {
			if (err) {
				WriteToFile("Error while querying tote " + Totes[index] + ". Err: " + err);
				return callback({Err: "Internal server error"});
			} else {
				if (doc) {
					let Orders = doc.orders;
					let Data = null;

					if(Zone){
						Data = CheckZoneUnpickedLocInOrders(Orders, Zone);
					} else {
						Data = CheckUnpickedLocInOrders(Orders);
					}

					if(Data.Loc != null && Data.Loc != ''){
						return callback(Data);
					} else {
						index++;
						CheckEachTote(Totes, index, Zone, callback);
					}
				} else {
					WriteToFile("Error while querying tote " + Totes[index]);
					return callback({Err: "Internal server error"});
				}
			}
		});
	} else {
		return callback({Loc: null});
	}
} /* CheckEachTote */

function CheckUnpickedLocInOrders(Orders){
	let x = 0;
	let loc = null;
	let Prod = null;
	while(x < Orders.length){
		let y = 0;
		while(y < Orders[x].items.length){
			let item = Orders[x].items[y];

			if(item.picked != true){
				loc = item.location;
				Prod = item.itemCode;
				break;
			}
			y++;
		}

		if(loc != null){
			break;
		}
		x++;
	}

	return {Loc: loc, Prod: Prod};
} /* CheckUnpickedLocInOrders */

function CheckZoneUnpickedLocInOrders(Orders, Zone){
	let x = 0;
	let loc = null;
	let Prod = null;
	while(x < Orders.length){
		let y = 0;
		while(y < Orders[x].items.length){
			let item = Orders[x].items[y];
			let Loc = item.location;
			let UpperLoc = Loc.toUpperCase();
			let ZoneNdx = UpperLoc.indexOf('Z');

			if(ZoneNdx > 0){
				let LocZone = UpperLoc.substr((ZoneNdx+1), 2);
				if(item.picked != true && Zone == parseInt(LocZone)){
					loc = item.location;
					Prod = item.itemCode;
					break;
				}
			}
			y++;
		}

		if(loc != null){
			break;
		}
		x++;
	}

	return {Loc: loc, Prod: Prod};
} /* CheckZoneUnpickedLocInOrders */
