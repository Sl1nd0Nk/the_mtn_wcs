﻿using System;
using System.ServiceProcess;
using System.Text;
using System.Net;
using System.IO;
using System.Threading;
using AioiSystems.Lightstep;
using System.Web;
using Apex.Helper;

namespace Apex.AIOI.Service
{
    public partial class ApexAIOIAPI : ServiceBase
    {
        #region Private Fields
        private static HttpListener listener;
        private Thread listenThread1;

        /// <summary>
        /// The ethernet controller used to interact with the lighting controller.
        /// </summary>
        private EthernetController controller;

        /// <summary>
        /// Enum used to switch a light OFF or ON.
        /// </summary>
        private enum Switch { ON, OFF }

        /// <summary>
        /// Enum used to switch the color of a light.
        /// </summary>
        private enum Color { RED, GREEN, BLUE, CYAN, ORANGE, PURPLE, WHITE }

        /// <summary>
        /// The URL of the HTTP Listener.
        /// </summary>
        private string httpListenerURL = "http://10.50.120.17:1062/";

        /// <summary>
        /// The URL of the button event send path.
        /// THIS IS AIOI LIGHTS RESPONSE RECEIVING POINT
        /// </summary>
        private string httpSenderURL = "http://10.50.120.17:9303/aoiLights";

        /// <summary>
        /// The ethernet controller IP.
        /// </summary>
        private string ethernetControllerIP = "10.50.120.16";

        /// <summary>
        /// The ethernet controller Port.
        /// </summary>
        private int ethernetControllerPort = 5003;
        #endregion

        /// <summary>
        /// Windows service constructor.
        /// </summary>
        public ApexAIOIAPI()
        {
            InitializeComponent();
            controller = new EthernetController();
            controller.SetLicense("1080-0726-172A-1677-62B8");
            controller.BinaryMode = false;
            controller.CommandReceived += ControllerCommandReceived;

            controller.BeginConnect(ethernetControllerIP, ethernetControllerPort);

            Console.Write("AIOI Service Started");
            listener = new HttpListener();
            listener.Prefixes.Add(httpListenerURL);
            listener.AuthenticationSchemes = AuthenticationSchemes.Anonymous;
            listener.Start();
            listenThread1 = new Thread(new ParameterizedThreadStart(HTTPListenerStart));
            listenThread1.Start();
        }               

        /// <summary>
        /// On windows service stop.
        /// </summary>
        protected override void OnStop()
        {
            if (listener != null)
            {
                listener.Stop();
                Library.WriteErrorLog("Aioi Service stopped");
            }
        }

        #region HTTP
        private void HTTPListenerStart(object o)
        {
            try
            {
                Console.Write("IN start Listening");
                while (true)
                {
                    // Blocks until a client has connected to the server
                    HTTPRequestProcess();
                }
            }
            catch (Exception ex)
            {
                Console.Write("ERR2=" + ex.Message.ToString());
            }
        }

        private void HTTPRequestProcess()
        {
            try
            {
                var result = listener.BeginGetContext(HTTPListenerCallback, listener);
                result.AsyncWaitHandle.WaitOne();
                Console.Write("IN Listening ...");
            }
            catch (Exception ex)
            {
                Console.Write("ERR3=" + ex.Message.ToString());
            }
        }

        /// <summary>
        /// The callback get triggered when a request was sent.
        /// </summary>
        /// <param name="result">The result data of the callback.</param>
        private void HTTPListenerCallback(IAsyncResult result)
        {
            try
            {
                Console.Write("Get Returned Context..");
                var context = listener.EndGetContext(result);
                var data_text = new StreamReader(context.Request.InputStream, context.Request.ContentEncoding).ReadToEnd();
                string cleaned_data = HttpUtility.UrlDecode(data_text);

                string parseResult = HTTPCommandParse(cleaned_data);

                context.Response.StatusCode = 200;
                context.Response.SendChunked = false;
                context.Response.ContentType = "text/plain";
                byte[] body = Encoding.ASCII.GetBytes(parseResult);
                context.Response.ContentLength64 = body.Length;
                context.Response.OutputStream.Write(body, 0, body.Length);

                context.Response.Close();

                //send to Node
                //Console.Write("Send Caught Context..");
                //RequestSend(cleaned_data, "");

            }
            catch (Exception ex)
            {
                Console.Write("ERR4=" + ex.Message.ToString());
            }
        }

        /// <summary>
        /// Sends a request to a URL end-point.
        /// </summary>
        /// <param name="data">The data to send to the end-point.</param>
        /// <param name="url">The url.</param>
        private void HTTPRequestSend(string data, string url)
        {
            // Create a request using a URL that can receive a post 
            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            // Create POST data and convert it to a byte array.  
            //string postData = "This is a test that posts this string to a Web server.";
            byte[] byteArray = Encoding.UTF8.GetBytes(data);
                        
            request.ContentType = "text/plain"; // Set the ContentType property of the WebRequest.  
            request.ContentLength = byteArray.Length;

            // Get the request stream.  
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

        }

        /// <summary>
        /// Parses a command string and then executes a light function.
        /// </summary>
        /// <param name="rawString">The command to parse.</param>
        /// <returns>The result of the command.</returns>
        private string HTTPCommandParse(string rawString)
        {
            string[] commandArray = rawString.Split('_');
            string commandResult = "";

            if (commandArray.Length == 2) // If only 2 command parameters are received we run the light switch functions.
            {
                if (commandArray[1].ToString() == Switch.ON.ToString() || commandArray[1].ToString() == Switch.OFF.ToString())
                {
                    Switch action = (Switch)Enum.Parse(typeof(Switch), commandArray[1]);
                    commandResult = LightSwitch(commandArray[0], action);

                    if (commandResult == "Incorrect Command") return commandResult;
                }
                else return "Incorrect Command";

                return "Switch!";
            }
            else if (commandArray.Length == 3) // Else we run the mode setup + switch functions.
            {
                bool test = Enum.IsDefined(typeof(Color), commandArray[2]);
                if (test)
                {
                    Color color = (Color)Enum.Parse(typeof(Color), commandArray[2].ToString());
                    commandResult = LightModeSetup(commandArray[0], color);

                    if (commandResult == "Incorrect Command") return commandResult;
                }

                if (commandArray[1].ToString() == Switch.ON.ToString() || commandArray[1].ToString() == Switch.OFF.ToString())
                {
                    Switch action = (Switch)Enum.Parse(typeof(Switch), commandArray[1]);
                    commandResult = LightSwitch(commandArray[0], action);

                    if (commandResult == "Incorrect Command") return commandResult;
                }

                return commandResult;
            }
            else if (commandArray[0].Equals("KIT")) // Command is completely wrong.
            {                
                return LightKit();
            }
            else if (commandArray[0].Equals("ALL"))
            {
                return LightAllOn();
            }
            else if (commandArray[0].Equals("RESET"))
            {
                return LightReset();
            }
            else
            {
                return "Incorrect Command.";
            }
        }
        #endregion

        #region Controller
        /// <summary>
        /// Controller command received event. This is where button presses are logged.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ControllerCommandReceived(object sender, EventArgs e)
        {
            CommandInfo cmdInfo = controller.GetCommand();

            if (cmdInfo != null)
            {
                AddressInfo[] addInfoList = AddressInfo.SplitCommand(cmdInfo.GetCommandBytes());

                foreach (AddressInfo address in addInfoList)
                {
                    if (address.Status == "00")
                    {
                        Console.WriteLine(address.Address, address.Status);
                        HTTPRequestSend(address.Address, httpSenderURL);
                    }
                    else
                    {
                        HTTPRequestSend("Failure", httpSenderURL);
                    }
                }
            }
        }

        /// <summary>
        /// Sets the color mode of a light module.
        /// </summary>
        /// <param name="address">The address of a light module.</param>
        /// <param name="color">The color mode of the light module.</param>
        /// <returns>Returns the result of the operation.</returns>
        private string LightModeSetup(string address, Color color)
        {
            // Am110032\u0011\u001f RED
            // Am110021!\u001f" GREEN
            // Am110031\u0012\u001f BLUE
            // Am110011\"? CYAN
            // Am110012!? ORANGE
            // Am110012? PURPLE
            // Am110012\"? WHITE


            // Am110012?
            if (color == Color.RED)
            {
                var commandString = string.Format("Am1{0}2\u0011\u001f", address);
                controller.BeginSendCommand(commandString);
                return "Success";
            }
            else if (color == Color.GREEN)
            {
                var commandString = string.Format("Am1{0}1!\u001f", address);
                controller.BeginSendCommand(commandString);
                return "Success";
            }
            else if (color == Color.BLUE)
            {
                var commandString = string.Format("Am1{0}1\u0012\u001f", address);
                controller.BeginSendCommand(commandString);
                return "Success";
            }
            else if (color == Color.CYAN)
            {
                var commandString = string.Format("Am1{0}1\"?", address);
                controller.BeginSendCommand(commandString);
                return "Success";
            }
            else if (color == Color.ORANGE)
            {
                var commandString = string.Format("Am1{0}2!?", address);
                controller.BeginSendCommand(commandString);
                return "Success";
            }
            else if (color == Color.PURPLE)
            {
                var commandString = string.Format("Am1{0}2?", address);
                controller.BeginSendCommand(commandString);
                return "Success";
            }
            else if (color == Color.WHITE)
            {
                var commandString = string.Format("Am110012\"?", address);
                controller.BeginSendCommand(commandString);
                return "Success";
            }

            return "Incorrect Command";
        }

        /// <summary>
        /// Switch a light module on or off.
        /// </summary>
        /// <param name="address">The address of the light module.</param>
        /// <param name="action">Weither to switch a light ON or OFF.</param>
        /// <returns>Returns the result of the operation.</returns>
        private string LightSwitch(string address, Switch action)
        {
            if (action == Switch.ON)
            {
                // P101100100000 ON

                var commandString = string.Format("P101{0}00000", address);
                controller.BeginSendCommand(commandString);
                return "Success";
            }
            else if (action == Switch.OFF)
            {
                // D1001 OFF

                var commandString = string.Format("D{0}", address);
                controller.BeginSendCommand(commandString);
                return "Success";
            }

            return "Incorrect Command";
        }

        /// <summary>
        /// Switch on all the lights
        /// </summary>
        /// <returns>Returns success.</returns>
        private string LightAllOn()
        {
            controller.BeginSendCommand("A");
            return "Success";
        }

        /// <summary>
        /// Reset all lights to default state.
        /// </summary>
        /// <returns>Returns success.</returns>
        private string LightReset()
        {
            controller.BeginSendCommand("Z");
            return "Success";
        }

        /// <summary>
        /// Runs Kit Lights
        /// </summary>
        /// <returns>Returns Success</returns>
        private string LightKit()
        {
            int address = 1001;

            for (int i = 0; i < 6; i++)
            {
                LightSwitch(address.ToString(), Switch.ON);
                LightModeSetup(address.ToString(), (Color)i);
                Thread.Sleep(100);
                LightSwitch(address.ToString(), Switch.OFF);
                address++;
            }

            return "Success";
        }
        #endregion
    }
}
