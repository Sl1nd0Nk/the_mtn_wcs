var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var scaleSchema = new Schema({
    scaleID         : {type: Number, required:false},
    scaleObjects : [
        {
            transporterID   : {type: String, required:false},
            cartonID        : {type: String, required:false},
            rejected        : {type: Boolean, required:false, default:false},
            date            : {type: Date, required:false},
            timeHours       : {type: Number, required:false}
        }
    ]
});

module.exports = mongoose.model('scaleThroughput', scaleSchema);