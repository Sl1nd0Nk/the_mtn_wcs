var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var pickTrolleyMongoSchema = new Schema({
        trolleyID:       {type: String, default: null},
        trolleyInUseBy:  {type: String, default: null},
        pickMode:        {
			name:                 {type: String, default: null},
			isDefaultMode:        {type: Boolean, default: false},
			isActive:             {type: Boolean, default: false},
			instructionMsgFinal:  {type: String, default: null},
			toteIDLeadDigit:      {type: String, default: null}
        },
        totes:           [],
        PickingStarted:  {type: Boolean, default: false},
        Location:		 {type: String, default: null},
        Zone :		     {type: Number, default: null},
        LocConfirmed:    {type: Boolean, default: false},
        Product:		 {type: String, default: null},
        ToteToConfirm:   {type: String, default: null},
        LocQty:			 {type: Number, default: null},
        LocQtyConfirmed: {type: Boolean, default: false},
        isTotesDone:     {type: Boolean, default: false},
        isPickingDone:   {type: Boolean, default: false},
        pickingPlan:     [{
			pl_ID:                {type: String, default: null},          // used in pickGroup[];  from _id to string: doc._id.toHexString();  from string to _id:  objId("pl_ID");
			toteID:               {type: String, default: null},          // sort order 3
			order_ix:             {type: Number, default: null},
			item_ix:              {type: Number, default: null},
			itemCode:             {type: String, default: null},          // sort order 2
			qty:                  {type: Number, default: null},
			locationID:           {type: String, default: null},          // sort order 1
			isItemPicked:         {type: Boolean, default: false}
    }]
}, {collection: 'pickTrolley'})

module.exports = mongoose.model('pickTrolley', pickTrolleyMongoSchema)
