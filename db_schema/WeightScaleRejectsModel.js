var mongoose = require('mongoose');
var moment   = require('moment');
var Schema = mongoose.Schema;

var WeightScaleRejectsSchema = new Schema({
    TransporterID           : {type: String, required:false, default:''},
    CartonID                : {type: String, required:true, index: {unique: true}},
    RejectReason            : {type: String, required:false},
    ExpectedMinWeight       : {type: Number, required:false},
    ExpectedMaxWeight       : {type: Number, required:false},
    CartonCalculatedWeight  : {type: Number, required:false},
    ActualScaleWeight       : {type: Number, required:false},
    QcScaleUsed				: {type: Number, required:false},
    DateRejected            : {type: String, required:false, default: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')},
    processed               : {type: Boolean, required:false, default:false},
    qcChecked		        : {type: Boolean, required:false, default: false},
    qcCheckedDate           : {type: String, required:false, default: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')},
    qcCheckedBy		        : {type: String, required:false, default: null},
    qcCheckResult	        : [{
     	Date: {type: String, required:false, default: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')},
     	Result: {type: String, required:false, default: null}
    }]
}, { collection: 'WeightScaleRejects'});

module.exports = mongoose.model('WeightScaleRejects', WeightScaleRejectsSchema);