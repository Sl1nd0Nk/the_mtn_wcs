var mongoose    = require('mongoose')
var Schema      = mongoose.Schema

var putWallSchema = new Schema({
    chuteID : {type: Number, default: null},
    Totes   : [
        {
            toteID   : {type: String, default: ''},
            Date     : {type: Date, default: null},
            chuteHour: {type: Number, default: 0},
            processed: {type: Boolean, default: false}
        }
    ]
})

module.exports = mongoose.model('putWallDashboard', putWallSchema)