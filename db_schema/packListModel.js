var mongoose    = require('mongoose')
var Schema      = mongoose.Schema;

var packListMongoSchema = new Schema({
    cartonId: {type: String, default: null},
    carton: {type: String, default: null},
    transporterId: {type: String, default: null},
    cartons: [
        {
          orderID: {type: String, default: null},
          cartonID: {type: String, default: null},
          courierID: {type: String, default: null},
          packingWeight: {type: Number, default: null},
          actualWeight: {type: String, default: null},
          rejected: {type: Boolean, default: false},
          complete: {type: Boolean, default: false},
          weight: {type: Number, default: null},
          items:
          [
            {
                itemCode: {type: String, default: null},
                itemDescription: {type: String, default: null},
                eachWeight: {type: Number, default: null},
                qty: {type: Number, default: null},
                pickQty: {type: Number, default: null},
                location: {type: String, default: null},
                pickZone: {type: String, default: null},
                picked: {type: Boolean, default: false},
                packed: {type: Boolean, default: false},
                shortPick: {type: Boolean, default: false},
                serials: [],
                barcode: {type: String, default: null},
                sku: {type: String, default: null},
                serialised: {type: Boolean, default: false}
            }
          ]
        }
      ]
})

module.exports = mongoose.model('packList', packListMongoSchema)