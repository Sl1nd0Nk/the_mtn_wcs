var mongoose = require('mongoose')
var Schema = mongoose.Schema

var pickToZonesSchema = new Schema({
    ZoneID  :{type: Number, required:false},
    NumTotes:{type: Number, required:false},
    Totes   :[{
        ToteID  : {type: String, required:false},
        Time    : {type: Date, required:false},
        TimeHours:{type: Number, required:false},
        processed: {type: Boolean, required:false, default: false}
    }]
})

module.exports = mongoose.model('picktozones', pickToZonesSchema)