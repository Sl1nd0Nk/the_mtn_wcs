var mongoose = require('mongoose');
var moment   = require('moment');
var Schema = mongoose.Schema;

var PackedSerialUpdatesSchema = new Schema({
    PickListID           : {type: String, default: null},
    WcsId                : {type: String, default: null},
    OrderId              : {type: String, default: null},
	Required			 : {type: Boolean, default: false},
	CreateDate			 : {type: String, default: null},
	LastUpdateDate       : {type: String, default: null},
	Result				 : {type: String, default: null},
	Error				 : {type: Boolean, default: false},
	Sent				 : {type: Boolean, default: false}
}, {collection: 'PackedSerialUpdates'});

module.exports = mongoose.model('PackedSerialUpdates', PackedSerialUpdatesSchema);