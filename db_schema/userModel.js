var mongoose = require('mongoose')
var Schema = mongoose.Schema

var UserSchema = new Schema({
    name: {type: String, required: false},
    surname: {type: String, required: false},
    username: {type: String, required: false},
    password: {type: String, required: false},
    uniqueID: {type: String, required: false},
    roleID: {type: String, required: false},
    PTLID: {type: String, required: false}
})

module.exports = mongoose.model('users', UserSchema)

