var mongoose = require('mongoose')
var Schema = mongoose.Schema

var subSystemSchema = new Schema({
    errorMessage : {type:String, required:false, default:''},
    Date         : {type:String, required:false},
    processed    : {type:Boolean, required:false, default:false},
})

module.exports = mongoose.model('subSystemComms', subSystemSchema)