var mongoose = require('mongoose')
var Schema = mongoose.Schema

var weightFailSchema = new Schema({
    Date    : {type:String, required:false, default:''},
    cartonID: {type:String, required:false, default:''},
    weightOne:{type:Number, required:false, default:0},
    weightTwo:{type:Number, required:false, default:0},
    processed:{type:Boolean, required:false, default:false}
})

module.exports = mongoose.model('weightFailLog', weightFailSchema)