var mongoose = require('mongoose')
var Schema = mongoose.Schema

var ptlZoneSchema = new Schema({
  ZoneID: {type: Number, default: null},
  UserID: {type: String, default: null},
  Status: {type: String, default: null},
  ToteID: {type: String, default: null},
  ToteInTime: {type: String, default: null},
  Transmitted: {type: Boolean, default: false},
  ZoneCompleted: {type: Boolean, default: false},
  ZoneError: {type: Boolean, default: false},
  PickLocations:
    [{
	   ToteID: {type: String, default: null},
       Location: {type: String, default: null},
       PickQty: {type: Number, default: null},
       PickedQty:{type: Number, default: null},
       ShortPicked: {type: Boolean, default: false},
       LocStatus:{type: String, default: null},
       Transmitted: {type: Boolean, default: false}
    }]
}, { collection: 'ptlzone'});

module.exports = mongoose.model('ptlzone', ptlZoneSchema)
