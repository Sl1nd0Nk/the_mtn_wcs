var mongoose = require('mongoose');
var moment   = require('moment');
var Schema = mongoose.Schema;

var CourierRejectsSchema = new Schema({
    TransporterID           : {type: String, required:false, default:''},
    CartonID                : {type: String, required:true, index: {unique: true}},
    RejectReason	        : [{
     	Date: {type: String, required:false, default: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')},
     	Reason: {type: String, required:false, default: null},
		Checked: {type: Boolean, default: false},
		CheckedBy: {type: String, default: null}
    }]
}, { collection: 'CourierRejects'});

module.exports = mongoose.model('CourierRejects', CourierRejectsSchema);