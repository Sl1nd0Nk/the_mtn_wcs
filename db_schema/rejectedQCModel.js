var mongoose = require('mongoose')
var Schema = mongoose.Schema

var rejectedQCSchema = new Schema({
    transporterID   : {type: String, required:false},
    cartonID        : {type: String, required:false},
    reason          : {type: String, required:false},
    minWeight       : {type: Number, required:false},
    maxWeight       : {type: Number, required:false},
    actuallWeight   : {type: Number, required:false},
    dateRejected    : {type: Date, required:false},
    processed       : {type: Boolean, required:false, default: false},
    captured        : {type: Boolean, required:false, default:false}
})

module.exports = mongoose.model('rejectedQC', rejectedQCSchema)
