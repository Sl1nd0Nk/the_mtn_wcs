var mongoose = require('mongoose')
var Schema = mongoose.Schema

var parametersSchema = new Schema({
    toteConsolidation: {
        active: {type: Boolean, default: false},
        maxOrdersToConsolidate: {type: Number, default: null},
        toteMaxVolume: {type: Number, default: null},
        toteMaxWeight: {type: Number, default: null},
        consolidationInProgress: {type: Boolean, default: false},
        consolSeqNum: {type: Number, default: null},
	minProdMatches: {type: Number, default: null}
    },
    autoBagsSeqNo : {type: Number, default: null},
    schuteSettings: {
        maxCapacity: {type: Number, default: 0}
    },
    courierRoutes: [{
        courierName: {type: String, required: false},
        routeNumber: {type: Number, default: null}
    }],
    weightSettings: [{
        StockSize: {type: Number, default: null},
        UpperTolerance: {type: Number, default: null},
        LowerTolerance: {type: Number, default: null},
        isCarton: {type: Boolean, default: false},
        weight: {type: Number, default: null},
        weightCapacity: {type: Number, default: null},
        volume: {type: Number, default: null},
        volumeCapacity: {type: Number, default: null},
        useTransporter: {type: Boolean, default: false},
        useTransporterForBag: {type: Boolean, default: false},
        length: {type: Number, default: null},
        width: {type: Number, default: null},
        height: {type: Number, default: null}
    }],
    orderPrioritySettings:[{
        priorityLevel : {type: Number, default: null},
        priorityDelta  : {type: Number, default: null}
    }],
    trolleySettings: {
        maxTotesPerTrolley:     {type: Number, default: 4},
		trolleyZone:            {type: Number, default: 17},
		pickModes: [{
			name:                   {type: String, default: null},
			isDefaultMode:          {type: Boolean, default: false},
			isActive:               {type: Boolean, default: false},
			instructionMsgFinal:    {type: String, default: null},
			toteIDLeadDigit:        {type: String, default: null}
		}]
    },
    NonConveyableContainerPrefix : {type: String, required: false},
    PrioritySettings: [{
        greaterThanTen:         {type: Boolean, default: true},
        lessThanTen:            {type: Boolean, default: false}
    }],
	orderTypes: [{
		orderType:              {type: String, default: null},
        maxOrdersPerTote:       {type: Number, default: 0}
	}],
	serialSync : {type: String, default: 'PULL'},
	ShipmentControl: {
        Active: {type: Boolean, default: false},
        ExceedHour: {type: Number, default: null}
	}

})

module.exports = mongoose.model('parameter', parametersSchema)
