var mongoose = require('mongoose')
var Schema = mongoose.Schema

var ptlTimerSchema = new Schema({
    toteID      : {type:String, required:false, default:true},
    ptlZoneID   : {type:Number, required:false, default:0},
    Date        : {type:String, required:false, default:true},
    TimeIn      : {type:Date, required:false}
})

module.exports = mongoose.model('ptlTimer', ptlTimerSchema)