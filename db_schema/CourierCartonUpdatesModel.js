var mongoose = require('mongoose');
var moment   = require('moment');
var Schema = mongoose.Schema;

var CourierCartonUpdatesSchema = new Schema({
    OrderId              : {type: String, default: null},
    CartonId      		 : {type: String, default: null},
	Required			 : {type: Boolean, default: false},
	CreateDate			 : {type: String, default: null},
	LastUpdateDate       : {type: String, default: null},
	Result				 : {type: String, default: null},
	Error				 : {type: Boolean, default: false},
	Sent				 : {type: Boolean, default: false}
}, {collection: 'CourierCartonUpdates'});

module.exports = mongoose.model('CourierCartonUpdates', CourierCartonUpdatesSchema);