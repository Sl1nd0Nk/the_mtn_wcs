var mongoose = require('mongoose');
var moment   = require('moment');
var Schema = mongoose.Schema;

var ErpUpdatesSchema = new Schema({
    OrderId              : {type: String, default: null},
    Status      		 : {type: String, default: null},
	Required			 : {type: Boolean, default: false},
	CreateDate			 : {type: String, default: null},
	LastUpdateDate       : {type: String, default: null},
	Result				 : {type: String, default: null},
	Error				 : {type: Boolean, default: false},
	Sent				 : {type: Boolean, default: false}
}, {collection: 'ErpUpdates'});

module.exports = mongoose.model('ErpUpdates', ErpUpdatesSchema);