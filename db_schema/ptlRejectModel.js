var mongoose = require('mongoose')
var Schema = mongoose.Schema

var ptlRejectSchema = new Schema({
    ToteID: {type: String, required:false},
    Reason: {type: String, required:false},
    Time  : {type: String, required:false},
    Reset : {type: Boolean, required:false, default: false}
})

module.exports = mongoose.model('ptlRejected', ptlRejectSchema)
