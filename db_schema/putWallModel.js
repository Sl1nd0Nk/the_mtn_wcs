var mongoose = require('mongoose')
var Schema = mongoose.Schema

var PutWallSchema = new Schema({
    packStation:{type: Number, default: null},
    putAddress: {type: Number, default: null},
    pullAddress:{type: Number, default: null},
    active: {type: Boolean, required:false},
    orderID :{type: String, required: false},
    courier: {type: String, required: false},
    putLightError: {type: Boolean, required:false},
    pullLightError: {type: Boolean, required:false},
    putComplete: {type: Boolean, required:false},
    Data: {type: String, required: false},
    Complete:{type: Boolean, required:false},
    date: {type:Date, required:false, default: null},
    pullWallStation: {type: String, required: false}
}, { collection: 'putPullWallLights' })

module.exports = mongoose.model('putPullWallLights', PutWallSchema)

