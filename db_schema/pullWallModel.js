var mongoose = require('mongoose')
var Schema = mongoose.Schema

var PullWallSchema = new Schema({
    stationID:{type: String, required: false},
    printer: {type: String, default: null},
    transporter:{type: String, default: null},
    Location:{type: Number, default: null},
    orderID :{type: String, required: false, default: null},
    courier: {type: String, required: false, default: null},
    parcelConfirmed:{type: Boolean, required:false, default: false},
    trConfirmed:{type: Boolean, required:false, default: false}
}, { collection: 'pullWallStations' })

module.exports = mongoose.model('pullWallStations', PullWallSchema)

