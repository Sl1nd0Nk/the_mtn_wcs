var mongoose = require('mongoose')
var Schema = mongoose.Schema

var CourierNoReadSchema = new Schema({
    Date    : {type:String, required:false}
})

module.exports = mongoose.model('courierNoRead', CourierNoReadSchema)