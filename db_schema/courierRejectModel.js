var mongoose = require('mongoose')
var Schema = mongoose.Schema

var CourierRejectSchema = new Schema({
    CartonId        : {type: String, required:false},
    Reason          : {type: String, required:false},
    Time            : {type: Date, required:false},
    processed       : {type: Boolean, required:false, default: false}
})

module.exports = mongoose.model('courierReject', CourierRejectSchema)