var mongoose = require('mongoose')
var Schema = mongoose.Schema

var WeightDimsUpdateSchema = new Schema({
	UserID: {type: String, default: null},
	Sku: {type: String, default: null},
	SkuDesc: {type: String, default: null},
	Uom: {type: String, default: null},
	FromWeight: {type: Number, default: null},
	ToWeight: {type: Number, default: null},
	FromLength: {type: Number, default: null},
	ToLength: {type: Number, default: null},
	FromWidth: {type: Number, default: null},
	ToWidth: {type: Number, default: null},
	FromHeight: {type: Number, default: null},
	ToHeight: {type: Number, default: null},
	Date: {type: String, default: null},
	Result: {type: String, default: null}
}, {collection : "WeightDimsUpdate"})

module.exports = mongoose.model('WeightDimsUpdate', WeightDimsUpdateSchema)
