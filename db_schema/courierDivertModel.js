var mongoose = require('mongoose')
var Schema = mongoose.Schema

var courierDivertSchema = new Schema({
    courierID   : {type: Number, default: null, unique:true},
    courierName : {type: String, default: null},
    containers  : [{
        containerID: {type: String, default: null},
        Date       : {type: Date, default: null},
        hourPassed : {type: Number, default: null},
    }]
})

module.exports = mongoose.model('courierDivert', courierDivertSchema)