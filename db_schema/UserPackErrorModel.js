var mongoose = require('mongoose');
var moment   = require('moment');
var Schema = mongoose.Schema;

var UserPackErrorSchema = new Schema({
    Date             : {type: String, default: null},
	User             : {type: String, default: null},
	Supervisor       : {type: String, default: null},
	Error			 : {type: String, default: null},
	Station			 : {type: Number, default: null}
}, {collection: 'UserPackError'});

module.exports = mongoose.model('UserPackError', UserPackErrorSchema);