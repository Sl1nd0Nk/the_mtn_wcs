var mongoose = require('mongoose');
var Schema = mongoose.Schema

var packStationSchema = new Schema({
    stationNumber       : {type: Number, required:false},
    schuteNumber        : {type: Number, required:false},
    totalTotes          : {type: Number, required:false},
    dailyTotal          : {type: Number, required:false},
    stationIP           : {type: String, required:false},
    labelPrinterQName   : {type: String, required:false},
    desktopPrinterQName : {type: String, required:false},
    autobagPrinterQName : {type: String, required:false},
    autobagPrinter      : {type: Boolean, required:false},
    active              : {type: Boolean, required:false},
    totes               : [],
    rdtIP               : {type: String, required:false},
    currentTote             : {type: String, required:false, default: null},
    currentTransporter  : {type: String, required:false, default: null},
    updated_at          : {type: Date, required:false},
    processed           : {type: Boolean, required:false, default: false},
    NumOrders                   : {type: Number, required:false},
    CurrentOrder                : {type: Number, required:false, default: null},
    ToteType                    : {type: String, required:false, default: null},
    OrderIndex                  : {type: Number, required:false, default: null},
    pickList                    : {type: Schema.Types.Mixed, required:false, default: null},
    ProcessOnPutWalls   : {type: Boolean, required:false, default: null},
    ProcUsingDesktopAutoBag   : {type: Boolean, required:false, default: null},
    CartonSizes                 : [{
			Size          : {type: String, required:false},
			Selected      : {type: String, required:false},
			Transporter   : {type: Boolean, required:false},
			weight        : {type: Number, required:false},
			minWeightLimit: {type: Number, required:false},
			maxWeightLimit: {type: Number, required:false}
        }],
    SimMode: {type: Boolean, required:false, default: false},
    SupervisorControl: {type: Boolean, required:false, default: false},
    UserError		 : {type: String, required:false, default: null},
    UserDbError		 : {type: String, required:false, default: null}
}, { collection: 'packStations'})

module.exports = mongoose.model('packStations', packStationSchema)
