var mongoose = require('mongoose');
var moment   = require('moment');
var Schema = mongoose.Schema;

var ShipUpdatesSchema = new Schema({
    OrderId              : {type: String, default: null},
	WmsOrderStatus	     : {type: String, default: null},
	Required			 : {type: Boolean, default: false},
	CreateDate			 : {type: String, default: null},
	LastUpdateDate       : {type: String, default: null},
	Result				 : {type: String, default: null}
}, {collection: 'ShipUpdates'});

module.exports = mongoose.model('ShipUpdates', ShipUpdatesSchema);