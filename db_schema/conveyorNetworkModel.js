var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var pickToZonesSchema = new Schema({
    ZoneID  :{type:Number, require:false, default:null},
    NumTotes:{type:Number, require:false, default:null},
    Totes   :[{
        ToteID  : {type:String, require:false, default:null},
        Time    : {type:Date, require:false, default:null},
        TimeHours:{type:Number, require:false, default:null},
        processed: {type: Boolean, required:false, default: false}
    }]
});
module.exports = mongoose.model('picktozones', pickToZonesSchema);
