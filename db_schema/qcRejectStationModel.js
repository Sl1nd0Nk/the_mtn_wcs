var mongoose = require('mongoose')
var Schema = mongoose.Schema

var qcRejectMongoStation = new Schema({
	stationIP : {type: String, required: false},
	stationNumber : {type: Number, required: false},
	updated_at : {type: Date, required: false},
	active : {type: Boolean, default: null},
	ContainerID : {type: String, required: false},
	Inprogress: {type: String, required: false, default: null},
	TempWeightReads: [],
	AverageWeight: {type: Number, required: false, default: null},
	ScaleQueryCounts: {type: Number, required: false, default: 0},
	ScaleIP: {type: String, required: false},
	ScalePort: {type: Number, required: false},
	ScaleManualMode: {type: Boolean, default: false},
	LastScannedResult: {type: String, default: null},
	Cartons: [{
		CartonID: {type: String, required: false},
		OrderID: {type: String, required: false},
		CartonWeight: {type: Number, required: false, default: null},
		MeasuredWeight: {type: Number, required: false, default: null},
		MinWeight: {type: Number, required: false},
		MaxWeight: {type: Number, required: false},
		PicklistID: {type: String, required: false},
		WeightFailReason: {type: String, required: false},
		CartonChecked: {type: Boolean, default: null},
		CartonProblem: {type: Boolean, default: null},
		CartonCancelled: {type: Boolean, default: null},
		CartonReason: {type: String, default: null},
		WeightCheckComplete: {type: Boolean, default: null},
		transporterID: {type: String, default: null},
		Items: [{
           ItemCode: {type: String, default: null},
           ItemDescription: {type: String, default: null},
           QtyOriginal: {type: Number, default: null},
           QtyPacked: {type: Number, default: null},
           QtyVerified: {type: Number, default: null},
           Serials: [],
           VerifiedSerials: [],
           Sku: {type: String, default: null},
           Barcode: {type: String, default: null},
           Serialised: {type: Boolean, default: false},
           PackedBy: {type: String, default: null}
		}]
	}]
}, {collection : "qcRejectStations"})

module.exports = mongoose.model('qcRejectStations', qcRejectMongoStation)
