var mongoose = require('mongoose');
var moment   = require('moment');
var Schema = mongoose.Schema;

var CancelRequestSchema = new Schema({
    OrderId              : {type: String, default: null},
	Status	             : {type: String, default: null},
	UserID   			 : {type: String, default: null},
	Required			 : {type: Boolean, default: false},
	ManifestRequired	 : {type: Boolean, default: false},
	ManifestPrinted		 : {type: Boolean, default: false},
	PrintedBy			 : {type: String, default: null},
	CreateDate			 : {type: String, default: null},
	LastUpdateDate       : {type: String, default: null},
	Result				 : {type: String, default: null}
}, {collection: 'CancelRequest'});

module.exports = mongoose.model('CancelRequest', CancelRequestSchema);