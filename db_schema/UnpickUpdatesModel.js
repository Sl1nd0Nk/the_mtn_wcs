var mongoose = require('mongoose');
var moment   = require('moment');
var Schema = mongoose.Schema;

var UnpickUpdatesSchema = new Schema({
    PickListID           : {type: String, default: null},
    WcsId                : {type: String, default: null},
    OrderId              : {type: String, default: null},
    PickListLine		 : {type: Number, default: null},
	WmsPicklistStatus	 : {type: String, default: null},
	WmsOrderStatus	     : {type: String, default: null},
	Required			 : {type: Boolean, default: false},
	UserId				 : {type: String, default: null},
	CreateDate			 : {type: String, default: null},
	LastUpdateDate       : {type: String, default: null},
	Result				 : {type: String, default: null}
}, { collection: 'UnpickUpdates'});

module.exports = mongoose.model('UnpickUpdates', UnpickUpdatesSchema);