var mongoose = require('mongoose')
var Schema = mongoose.Schema

var ChuteQtySchema = new Schema({
	chuteNo:{type: Number, default: null},
	active: {type: Boolean, default: false},
    qty :   {type: Number, default: null},
    max :   {type: Number, default: null},
    putwall: {type: Boolean, default: false}
}, { collection: 'chuteQty' });

module.exports = mongoose.model('chuteQty', ChuteQtySchema)
