var mongoose = require('mongoose')
var Schema = mongoose.Schema

var serialMongoSchema = new Schema({
  activated: {type: Boolean, default: false},
  ASNID: {type: String, default: null},
  cartonID: {type: String, default: null},
  cartonNo: {type: Number, default: null},
  dispatched: {type: Boolean, default: false},
  RecNo: {type: String, default: null},
  loadID: {type: String, default: null},
  noOfCartons: {type: Number, default: null},
  orderID : {type: String, default: null},
  parentRecNo : {type: String, default: null},
  received: {type: Boolean, default: false},
  salesOrderID: {type: String, default: null},
  salesOrderLineID: {type: Number, default: null},
  serial: {type: String, required: true, index: {unique: true}},
  serialStatus: {type: String, default: null},
  serialWeight: {type: Number, default: null},
  SKU: {type: String, default: null},
  UOM: {type: String, default: null},
  userID: {type: String, default: null},
  serialCount: {type: Number, default: null},
  resyncRequired: {type: Boolean, default: false},
  last_update_to_wcs: {type:Date, required:false},
  last_update_to_wms: {type:Date, required:false}
}, { collection: 'serials'})

module.exports = mongoose.model('serial', serialMongoSchema)
