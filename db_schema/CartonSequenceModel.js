var mongoose = require('mongoose');
var moment   = require('moment');
var Schema = mongoose.Schema;

var CartonSeqSchema = new Schema({
    OrderId              : {type: String, default: null},
	PicklistId           : {type: String, default: null},
	CartonId   			 : {type: String, default: null},
	CartonNo			 : {type: Number, default: null},
	NoOfCartons			 : {type: Number, default: null}
}, {collection: 'CartonSequence'});

module.exports = mongoose.model('CartonSequence', CartonSeqSchema);