var mongoose = require('mongoose')
var Schema = mongoose.Schema

var batchWeightSchema = new Schema({
    
            RangeStart   : {type: Number, default: 0},
            RangeEnd     : {type: Number, default: 0},
            batchWeight  : {type: Number, default: 0}
})

module.exports = mongoose.model('batchWeightTolerance', batchWeightSchema)