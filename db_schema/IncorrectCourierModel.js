var mongoose = require('mongoose')
var Schema = mongoose.Schema

var IncorrectCourierSchema = new Schema({
	CartonID: {type: String, default: null},
	TransporterID: {type: String, default: null},
	OrderID: {type: String, default: null},
	DesignatedCourier: {type: String, default: null},
	RoutedCourier: {type: String, default: null},
	Required: {type: Boolean, default: false},
	Date: {type: String, default: null},
	Result: {type: String, default: null}
}, {collection : "IncorrectCourier"})

module.exports = mongoose.model('IncorrectCourier', IncorrectCourierSchema)
