var mongoose = require('mongoose')
var Schema = mongoose.Schema

var CartonShippedSchema = new Schema({
    cartonID    : {type: String, required: false, default: ''},
    dateTime    : {type: String, required: false, default: ''},
    shipped     : {type: Boolean, required: false, default: false},
    dispatched  : {type: Boolean, required: false, default: false}
})

module.exports = mongoose.model('cartonShipped', CartonShippedSchema)