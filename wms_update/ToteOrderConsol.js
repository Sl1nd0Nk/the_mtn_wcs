/*require('appdynamics').profile({
    controllerHostName: '10.20.88.95',
    controllerPort: 8090,
    accountName: 'customer1',
    accountAccessKey: '91f9b93a-0a53-4caa-8bde-f4f8328773dc',
    applicationName: 'Midrand Prod WCS',
    tierName: 'NodeJS',
    nodeName: 'Tote Consolidation',
    debug: true,
logging:{
   'logfiles': [{
   'root_directory':'/tmp/appd',
   'filename':'echo_%N.log',
   'level': 'DEBUG',
   'max_size': 10000,
   'max_files': 1
}]
}
});*/

var mongoose       = require('mongoose');
mongoose.Promise   = require('bluebird');
var config         = require('../config/wms_update.json');
var moment         = require('moment');
var CanReq         = require('CancelRequestModel');

var mongooseSchema = mongoose.Schema;	/* MongoDB Schema class object. */

var params		= require('../db_schema/parameterModel');
var pickList	= require('../db_schema/pickListModel');

var MaxNumOfItemsInBag = 3;
var aOrderTypes				= [];		// zp, array of {orderType, maxOrdersPerTote} from parameters
var aOrderTypesOnly			= [];		// zp, array of {orderType}                   from aOrderTypes
var aUniqueOrderTypesInPL	= [];		// zp, array of unique order types present in the pick lists to be consolidated

mongoose.connect(config.mongoDB.url, function (err) {
	if (err) {
		console.error('MONGO CONNECT ERROR ' + err);
	} else {
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' Connected To Mongo');
		GetSysParams();
	}
});

function GetSysParams(){
	var ToteConsolActive	= false;
	var ToteVolume			= null;
	var toteMaxWeight		= null;
	var ReadyToConsolidate	= false;
	var consolSeqNum		= 0;
	var minProdMatches		= 0;
	aOrderTypes				= [];		// [{'orderType': null, 'maxOrdersPerTote': 1}];
	aOrderTypesOnly			= [];		// [null];
	aUniqueOrderTypesInPL	= [];

	var sys = params.find({'toteConsolidation.active': true }).cursor();

	sys.on('data', function(Detail){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' Tote Order Consolidation Active');
		ToteConsolActive			= Detail.toteConsolidation.active;
		ToteVolume					= Detail.toteConsolidation.toteMaxVolume;
		toteMaxWeight				= Detail.toteConsolidation.toteMaxWeight;
		var consolidationInProgress	= Detail.toteConsolidation.consolidationInProgress;
		consolSeqNum				= Detail.toteConsolidation.consolSeqNum;
		minProdMatches				= Detail.toteConsolidation.minProdMatches;

		if(consolidationInProgress){
			WriteToFile('--');
		} else {
			ReadyToConsolidate = true;
		}

		aOrderTypes = Detail.orderTypes;						// zp, previous maxOrdersToConsolidate replaced by aOrderTypes
		for(var i = aOrderTypes.length - 1; i >= 0; i--){		// zp, exclude from consolidation (i.e. leave picklists.Consolidate = false) if .maxOrdersPerTote == 1
			if(aOrderTypes[i].maxOrdersPerTote <= 1){
				aOrderTypes.splice(i, 1);
			} else {
				aOrderTypesOnly.push(aOrderTypes[i].orderType);
			}
		}
	});

	sys.on('end', function () {
		if(!ToteConsolActive){
			WriteToFile('Tote Order Consolidation InActive');
			return WaitForTrigger();
		}

		if(aOrderTypes.length <= 0){
			WriteToFile('Parameter orderTypes is empty');
			return WaitForTrigger();
		}

		if(!ToteVolume || ToteVolume <= 0 || minProdMatches <= 0){
			WriteToFile('Tote volume cannot be zero or less - Current Tote Volume: ' + ToteVolume);
			return WaitForTrigger();
		}

		if(!ReadyToConsolidate){
			WriteToFile('Consolidation already in porgress ');
			return WaitForTrigger();
		}

		SetConsolidationStatus(ReadyToConsolidate, function(Run){
			if(Run){
				return FindOrdersToConsolidate(ToteConsolActive, ToteVolume, toteMaxWeight, minProdMatches);
			}

			return WaitForTrigger();
		});
	});
} /* GetSysParams */

function SetConsolidationStatus(Status, callback){
	params.find({'toteConsolidation.active': true}, function (err, detail) {
		if(err || !detail){
			WriteToFile('ERROR: ' + err);
			return callback(false);
		}

		detail[0].toteConsolidation.consolidationInProgress = Status;
		detail[0].save(function(erroor){
			if(erroor){
				WriteToFile('CONSOL STATUS SAVE ERROR: ' + erroor);
				return callback(false);
			}

			if(Status == false){
				WriteToFile('Consolidation ended');
				return callback(false);
			}

			return callback(true);
		});
	});
} /* SetConsolidationStatus */

function CheckEachOrderInMasterForCancelReq(PL, y, callback){
	if(y >= PL.orders.length){
		return callback();
	}

	CanReq.findOne({'OrderId': PL.orders[y].orderID}, function(err, Req){
		if(err){
			WriteToFile(err);
		}

		if(Req){
			let Order = PL.orders[y];
			Order.WmsOrderStatus = 'CANCELED';
			Order.WmsPickListStatus = 'CANCELED';
			Order.CancelDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			Order.CancelInstruction = 'Cancelled Before Tote Assignment';

			let NewPL = new pickList({
				toteID: null,
				transporterID: null,
				destination: null,
				packType: PL.packType,
				toteInUse: false,
				toteInUseBy: null,
				orders: Order,
				PackedDate: null,
				Status: 'CANCELED',
				FromTote: null,
				PickUpdateRequired: false,
				PackUpdateRequired: false,
				ShipUpdateRequired: false,
				WeightUpdateRequired: false,
				PickListType: null,
				CreateDate: PL.CreateDate
			});

			PL.orders.splice(y, 1);

			if(!PL.orders || PL.orders.length <= 0){
				pickList.Delete({'_id': PL._id}, function(){
					NewPL.save(function(error, Detail){
						return callback();
					});
				});
				return;
			}

			PL.save(function(err, PL){
				NewPL.save(function(error, Detail){
					return CheckEachOrderInMasterForCancelReq(PL, y, callback);
				});
			});

			return;
		}

		y++;
		return CheckEachOrderInMasterForCancelReq(PL, y, callback);
	});
} /* CheckEachOrderInMasterForCancelReq */

function CheckEachMasterForCancelledRequiredOrders(Master, x, callback){
	if(x >= Master.length){
		return callback();
	}

	let PL = Master[x];
	CheckEachOrderInMasterForCancelReq(PL, 0, function(){
		x++;
		return CheckEachMasterForCancelledRequiredOrders(Master, x, callback);
	});
} /* CheckEachMasterForCancelledRequiredOrders */

function CheckEachOrderForCancellation(PlArr, x, callback){
	if(x >= PlArr.length){
		return callback();
	}

	let PL = PlArr[x];
	CanReq.findOne({'OrderId': PL.orders[0].orderID}, function(err, Req){
		if(err){
			WriteToFile(err);
		}

		if(Req){
			PL.orders[0].WmsOrderStatus = 'CANCELED';
			PL.orders[0].WmsPickListStatus = 'CANCELED';
			PL.orders[0].CancelDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			PL.orders[0].CancelInstruction = 'Cancelled Before Tote Assignment';
			PL.Status = 'CANCELED';

			PL.save(function(err, PL){
				x++;
				return CheckEachOrderForCancellation(PlArr, x, callback);
			});

			return;
		}

		x++;
		return CheckEachOrderForCancellation(PlArr, x, callback);
	});
} /* CheckEachOrderForCancellation */

function FindCancelledInConsolidated(callback){
	pickList.find({'PickListType': 'WCS_MASTER', 'toteID': null, 'Status': 'NEW'}, function(err, Master){
		if(err){
			return callback();
		}

		if(!Master || Master.length <= 0){
			return callback();
		}

		CheckEachMasterForCancelledRequiredOrders(Master, 0, function(){
			return callback();
		});
	});
} /* FindCancelledInConsolidated */

function FindCancelledNonConsolidatedOrders(callback){
	pickList.find({'PickListType': {$ne: 'WCS_MASTER'}, 'toteID': null, 'Status': 'NEW'}).limit(100).exec(function(err, PlArr){
		if(err){
			return callback();
		}

		if(!PlArr || PlArr.length <= 0){
			return callback();
		}

		CheckEachOrderForCancellation(PlArr, 0, function(){
			return callback();
		});
	});
} /* FindCancelledNonConsolidatedOrders */

function FindOrdersToConsolidate(ToteConsolActive, ToteVolume, toteMaxWeight, minProdMatches){
	WriteToFile('In FindOrdersToConsolidate');

	pickList.find({'toteID': null, "priorityLevel": 10, 'Status': 'NEW', 'PickListType': {$ne: 'WCS_MASTER'}, 'orders.orderType': {$in: aOrderTypesOnly}, 'PickType':{$ne: 'FULLPICK'}, 'PickRegion': {$ne: '311Non'}}).limit(100).exec( function (err, Details){
		if(err){
			WriteToFile('Failed while trying to find suitable picklist for consolidation');
			SetConsolidationStatus(false, function(Run){
				if(Run == false){
					return WaitForTrigger();
				}
			});
			return;
		}

		if(Details.length <= 0){
			WriteToFile('No picklists found suitable for consolidation');
			FindCancelledInConsolidated(function(){
				FindCancelledNonConsolidatedOrders(function(){
					SetConsolidationStatus(false, function(Run){
						if(Run == false){
							return WaitForTrigger();
						}
					});
				});
			});
			return;
		}

		CalcWeightVolumeAndValidate(Details, 0, ToteVolume, toteMaxWeight, function(){
			WriteToFile('>> Number of PLs with orderType : ' + Details.length + ',  aUniqueOrderTypesInPL: ' + JSON.stringify(aUniqueOrderTypesInPL));
			ConsolidatePerUniqueOrderType(ToteVolume, toteMaxWeight, function(){
				FindCancelledInConsolidated(function(){
					FindCancelledNonConsolidatedOrders(function(){
						SetConsolidationStatus(false, function(Run){
							if(Run == false){
								return WaitForTrigger();
							}
						});
					});
				});
			});
		});
	});
} /* FindOrdersToConsolidate */

function CalcWeightVolumeAndValidate(Details, index, ToteVolume, toteMaxWeight, callback) {
	if(index >= Details.length){
		return callback();
	}

	CanReq.findOne({'OrderId': Details[index].orders[0].orderID}, function(err, Req){
		if(err){
			WriteToFile(err);
		}

		if(Req){
			Details[index].Status = 'CANCELED';
			Details[index].orders[0].WmsOrderStatus = 'CANCELED';
			Details[index].orders[0].WmsPickListStatus = 'CANCELED';
			Details[index].orders[0].CancelDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			Details[index].orders[0].CancelInstruction = 'Cancelled Before Tote Assignment';
			Details[index].save(function (err, savedDoc){
				index++;
				return CalcWeightVolumeAndValidate(Details, index, ToteVolume, toteMaxWeight, callback);
			});

			return;
		}

		let Result;
		let PickListWeightVolume = CalcPLWeightandVol(Details[index].orders[0].items);

		if(PickListWeightVolume.length != 2){
			index++;
			return CalcWeightVolumeAndValidate(Details, index, ToteVolume, toteMaxWeight, callback);
		}

		if((PickListWeightVolume[0] >= (toteMaxWeight / 2)) || (PickListWeightVolume[1] >= (ToteVolume / 2)) || (Details[index].ReadyToRemove == true)){
			index++;
			return CalcWeightVolumeAndValidate(Details, index, ToteVolume, toteMaxWeight, callback);
		}

		params.find({}, function (err, Parameters){
			if(err || !Parameters || Parameters.length <= 0){
				WriteToFile('Failed to find parameters in the system');
				index++;
				return CalcWeightVolumeAndValidate(Details, index, ToteVolume, toteMaxWeight, callback);
			}

			let CartonPrefix = 1;
			if(!Parameters[0].weightSettings || Parameters[0].weightSettings.length <= 0){
				WriteToFile("Failed to find weightSettings parameter in the system");
				index++;
				return CalcWeightVolumeAndValidate(Details, index, ToteVolume, toteMaxWeight, callback);
			}

			let paramIndex = 0;
			while(paramIndex < Parameters[0].weightSettings.length){
				if(Parameters[0].weightSettings[paramIndex].StockSize == CartonPrefix){
					break;
				}
				paramIndex++;
			}

			if(paramIndex >= Parameters[0].weightSettings.length){
				WriteToFile('Falied to Carton Size 1 details');
				index++;
				return CalcWeightVolumeAndValidate(Details, index, ToteVolume, toteMaxWeight, callback);
			}

			pickList.count({'orders.pickListID': Details[index].orders[0].pickListID}, function(err, PLsPerID){
				if(PLsPerID > 1){
					WriteToFile('Picklist ' + Details[index].orders[0].pickListID + ' will not be consolidated as it contains duplicates');
					pickList.remove({ '_id': Details[index]._id }, function () {
						index++;
						return CalcWeightVolumeAndValidate(Details, index, ToteVolume, toteMaxWeight, callback);
					});

					return;
				}

				pickList.count({'orders.orderID': Details[index].orders[0].orderID}, function (err, PLsPerOrder){
					if(PickListWeightVolume[1] <= Parameters[0].weightSettings[paramIndex].volumeCapacity){
						if(PLsPerOrder == 1){
							if(Details[index].orders[0].items.length <= MaxNumOfItemsInBag){
								WriteToFile('Picklist ' + Details[index].orders[0].pickListID + ' marked as a small order - can be packed at autobagger');
								Details[index].SmallOrder = true;
							} else {
								Details[index].SmallOrder = false;
							}
						} else {
							Details[index].SmallOrder = false;
						}
					} else {
						Details[index].SmallOrder = false;
					}

					if(aUniqueOrderTypesInPL.filter((o) => { return o == Details[index].orders[0].orderType; }).length == 0){	// zp, new OT in PL
						aUniqueOrderTypesInPL.push(Details[index].orders[0].orderType);											// zp, add to uniques
					}

					Details[index].Consolidate = true;
					Details[index].save(function (err, savedDoc){
						if(err){
							WriteToFile('Failed to update picklist ' + Details[index].orders[0].pickListID + ' as ready to consolidate');
						} else {
							WriteToFile('Picklist ' + Details[index].orders[0].pickListID + ' marked as ready to consolidate');
						}

						index++;
						return CalcWeightVolumeAndValidate(Details, index, ToteVolume, toteMaxWeight, callback);
					});
				});
			});
		});
	});
} /* CalcWeightVolumeAndValidate */

function ConsolidatePerUniqueOrderType(ToteVolume, toteMaxWeight, callback){
	if(aUniqueOrderTypesInPL.length <= 0){
		WriteToFile('>> ConsolidatePerUniqueOrderType: callback');
		return callback();
	}

	let uniqueOrderTypeInPL = aUniqueOrderTypesInPL[0];
	let maxOrdersToConsolidate = aOrderTypes.filter((o) => { return o.orderType == uniqueOrderTypeInPL; })[0].maxOrdersPerTote;
	WriteToFile('>> ConsolidatePerUniqueOrderType: ' + aUniqueOrderTypesInPL.length + ', order type: ' + uniqueOrderTypeInPL + ', max: ' + maxOrdersToConsolidate);
	AddToOrCreateMasterPL(uniqueOrderTypeInPL, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, true, function(){
		ConsolidateSmallOrdersTogether(uniqueOrderTypeInPL, maxOrdersToConsolidate, function(){
			aUniqueOrderTypesInPL.splice(0, 1);
			ConsolidatePerUniqueOrderType(ToteVolume, toteMaxWeight, callback);
		});
	});
} /* ConsolidatePerUniqueOrderType */

function AddToOrCreateMasterPL(ordType, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, CompareAgainstExisting, callback){
	pickList.find({'Consolidate': true, 'SmallOrder': false, 'toteID': null, 'Status': 'NEW', 'orders.orderType': ordType}, function(err, Details){
		if(err){
			WriteToFile('Error while finding picklists on ready to consolidate');
			return callback();
		}

		if(Details.length <= 0){
			WriteToFile('No picklists found on ready to consolidate');
			return callback();
		}

		if(CompareAgainstExisting){
			TryAndConsolidateThePicklist(Details, 0, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, function () {
				pickList.remove({'ReadyToRemove': true}, function(){
					return AddToOrCreateMasterPL(ordType, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, false, callback);
				});
			});

			return;
		}

		FindMatchingPicklistToConsolidate(Details, 0, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, function (Restart){
			if(Restart){
				pickList.remove({ 'ReadyToRemove': true }, function () {
					return AddToOrCreateMasterPL(ordType, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, false, callback);
				});

				return;
			}

			ClearAllPickListNotConsolidated(ordType, function () {
				return callback();
			});
		});
	});
} /* AddToOrCreateMasterPL */

function ConsolidateSmallOrdersTogether(ordType, maxOrdersToConsolidate, callback) {
	pickList.find({'Consolidate': true, 'priorityLevel': 10, 'SmallOrder': true, 'toteID': null, 'Status': 'NEW', 'orders.orderType': ordType}, function(err, Details){
		if(err){
			WriteToFile('Error while finding small orders picklists on ready to consolidate');
			return callback();
		}

		if(Details.length > 1){
			let max = maxOrdersToConsolidate;
			if(Details.length < maxOrdersToConsolidate){
				max = Details.length;
			}

			CreateNewMaster('PUT', function(Master){
				if(Master){
					ConsolidateToMax(Master, Details, 0, max, function () {
						pickList.remove({ 'ReadyToRemove': true }, function () {
							return ConsolidateSmallOrdersTogether(ordType, maxOrdersToConsolidate, callback);
						});
					});

					return;
				}

				ClearConsolidationStateFromPL(Details, 0, function () {
					return callback();
				});
			});

			return;
		}

		if(Details.length > 0){
			ClearConsolidationStateFromPL(Details, 0, function(){
				return callback();
			});

			return;
		}

		WriteToFile('No small order picklists found on ready to consolidate');
		return callback();
	});
} /* ConsolidateSmallOrdersTogether */

function ClearConsolidationStateFromPL(Details, index, callback) {
	if(index >= Details.length){
		return callback();
	}

	Details[index].Consolidate = false;
	Details[index].save(function(err, savedDoc){
		index++;
		return ClearConsolidationStateFromPL(Details, index, callback);
	});
} /* ClearConsolidationStateFromPL */

function ConsolidateToMax(Master, Details, index, max, callback){
	if(index >= max){
		return callback();
	}

	if(Details[index].ReadyToRemove == true){
		index++;
		return ConsolidateToMax(savedMaster, Details, index, max, callback);
	}

	let SlaveOrder = Details[index].orders[0];
	Master.orders.push(SlaveOrder);
	Master.PickRegion = Details[index].PickRegion;
	Master.PickType = Details[index].PickType;
	Master.save(function (err, savedMaster) {
		if(err || !savedMaster){
			return callback();
		}

		WriteToFile('Added picklist ' + Details[index].orders[0].pickListID + ' to WCS master');
		Details[index].orders.splice(0, 1);
		Details[index].ReadyToRemove = true;
		Details[index].save(function(error, saveSlave){
			index++;
			return ConsolidateToMax(savedMaster, Details, index, max, callback);
		});
	});
} /* ConsolidateToMax */

function OrderFitOnMaster(PickList, Master, ToteVolume, toteMaxWeight) {
	let MasterVol = 0;
	let MasterWeight = 0;

	if(Master.orders){
		let a = 0;
		while(a < Master.orders.length){
			let MasterWeightVolume = CalcPLWeightandVol(Master.orders[a].items);

			MasterVol += MasterWeightVolume[1];
			MasterWeight += MasterWeightVolume[0];
			a++;
		}
	}

	let PickListWeightVolume = CalcPLWeightandVol(PickList.orders[0].items);

	if(MasterWeight + PickListWeightVolume[0] > toteMaxWeight){
		return false;
	}

	if(MasterVol + PickListWeightVolume[1] > ToteVolume){
		return false;
	}

	if(Master.orders.length > 0){
		if(PickList.orders[0].courierID != Master.orders[0].courierID){
			return false;
		}
	}

	return true;
} /* OrderFitOnMaster */

function ClearAllPickListNotConsolidated(ordType, callback){
	pickList.find({'Consolidate': true, 'SmallOrder': false, 'toteID': null, 'Status': 'NEW', 'orders.orderType': ordType}, function(err, PLs){
		if(err || !PLs || PLs.length <= 0){
			return callback();
		}

		SaveEach(PLs, 0, function(){
			return callback();
		});
	});
} /* ClearAllPickListNotConsolidated */

function SaveEach(PL, index, callback){
	if(index >= PL.length){
		return callback();
	}

	PL.Consolidate = false;
	PL.save(function(err, savedPL){
		index++;
		return SaveEach(PL, index, callback);
	});
} /* SaveEach */

function FindMatchingPicklistToConsolidate(Details, index, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, callback){
	if(index >= Details.length){
		return callback(false);
	}

	if(Details[index].ReadyToRemove){
		Details[index].Consolidate = false;
		Details[index].save(function(err, savedDoc){
			index++;
			return FindMatchingPicklistToConsolidate(Details, index, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, callback);
		});

		return;
	}

	let next = index + 1;
	let PLArr = [];
	while(next < Details.length){
		if(Details[next].ReadyToRemove == false &&
		   Details[next].orders.length > 0 &&
		   CheckIfProductsAreTheSame(Details[index], Details[next])){
			PLArr.push(Details[next].orders[0].pickListID);
		}
		next++;
	}

	PLArr.push(Details[index].orders[0].pickListID);
	if(PLArr.length <= 1){
		Details[index].Consolidate = false;
		Details[index].save(function(err, savedDoc){
			index++;
			return FindMatchingPicklistToConsolidate(Details, index, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, callback);
		});

		return;
	}

	CreateNewMaster('MAN', function(Master){
		if(!Master){
			WriteToFile('Failed to create master picklist');
			Details[index].Consolidate = false;
			Details[index].save(function(err, savedDoc){
				index++;
				return FindMatchingPicklistToConsolidate(Details, index, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, callback);
			});

			return;
		}

		let MaxConOrders = maxOrdersToConsolidate;
		AddSubPickListToMaster(Master, PLArr, 0, MaxConOrders, function(){
			return callback(true);
		});
	});
} /* FindMatchingPicklistToConsolidate */

function AddSubPickListToMaster(Master, PLArr, index, MaxConOrders, callback){
	let MasterCapacity = MaxConOrders;

	if(MasterCapacity <= 0){
		return callback();
	}

	if(index >= PLArr.length){
		return callback();
	}

	pickList.count({'orders.pickListID': PLArr[index]}, function(err, PLsPerID){
		if(PLsPerID > 1){
			index++;
			return AddSubPickListToMaster(Master, PLArr, index, MasterCapacity, callback);
		}

		pickList.findOne({'orders.pickListID': PLArr[index]}, function(err, SUBPL){
			if(err || !SUBPL){
				index++;
				return AddSubPickListToMaster(Master, PLArr, index, MasterCapacity, callback);
			}

			let Order = SUBPL.orders[0];
			Master.orders.push(Order);
			Master.PickRegion = SUBPL.PickRegion;
			Master.PickType = SUBPL.PickType;
			Master.save(function(err, savedMaster){
				if(err || !savedMaster){
					index++;
					return AddSubPickListToMaster(Master, PLArr, index, MasterCapacity, callback);
				}

				WriteToFile('Added picklist ' + SUBPL.orders[0].pickListID + ' to WCS master');
				MasterCapacity--;
				SUBPL.orders.splice(0,1);
				SUBPL.ReadyToRemove = true;
				SUBPL.save(function(err2, savedSub){
					index++;
					return AddSubPickListToMaster(savedMaster, PLArr, index, MasterCapacity, callback);
				});
			});
		});
	});
} /* AddSubPickListToMaster */

function TryAndConsolidateThePicklist(Details, index, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, callback){
	if(index >= Details.length){
		return callback();
	}

	pickList.find({'PickListType': 'WCS_MASTER', 'toteID': null, 'Status': 'NEW', "priorityLevel" : 10}, function(err, Master){
		if(err || Details[index].ReadyToRemove == true){
			WriteToFile('Error while finding WCS_MASTER picklists in TryAndConsolidateThePicklist');
			index++;
			return TryAndConsolidateThePicklist(Details, index, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, callback);
		}

		pickList.count({'orders.pickListID':Details[index].orders[0].pickListID}, function( err, PLsPerID){
			if(PLsPerID > 1){
				WriteToFile('NOT consolidating duplicate pickList ' + Details[index].orders[0].pickListID + '. Instead will be marked to be removed');
				Details[index].ReadyToRemove = true;
				Details[index].Consolidate = false;
				Details[index].save(function(err3, savedPLInvalid){
					index++;
					return TryAndConsolidateThePicklist(Details, index, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, callback);
				});

				return;
			}

			if(Master.length <= 0){
				WriteToFile('No WCS_MASTER picklists found');
				index++;
				return TryAndConsolidateThePicklist(Details, index, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, callback);
			}

			CheckIfPickListFitsInWCSMaster(Details[index], Master, 0, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, function(Results){
				if(!Results || !Results.Master){
					WriteToFile('PickList ' + Details[index].orders[0].pickListID + ' | order ' + Details[index].orders[0].orderID + ' not suitable for any existing WCS_MASTERs');
					index++;
					return TryAndConsolidateThePicklist(Details, index, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, callback);
				}

				let SlaveOrder = Details[index].orders[0];
				Results.Master.orders.push(SlaveOrder);
				Results.Master.save(function(err1, savedMaster){
					if(!err && savedMaster){
						WriteToFile('Added pickList ' + SlaveOrder.pickListID + ' | order ' + SlaveOrder.orderID + ' to WCS_MASTER');
					} else {
						WriteToFile('Error whille adding pickList ' + SlaveOrder.pickListID + ' | order ' + SlaveOrder.orderID + ' to WCS_MASTER');
					}

					Details[index].orders.splice(0,1);
					Details[index].ReadyToRemove = true;
					Details[index].save(function(err2, savedPL){
						index++;
						return TryAndConsolidateThePicklist(Details, index, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, callback);
					});
				});
			});
		});
	});
} /* TryAndConsolidateThePicklist */

function CheckIfPickListFitsInWCSMaster(Picklist, MasterList, MasterIndex, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, callback) {
	if(MasterIndex >= MasterList.length){
		return callback({Master: null});
	}

	if(MasterList[MasterIndex].orders.length >= maxOrdersToConsolidate){
		MasterIndex++;
		return CheckIfPickListFitsInWCSMaster(Picklist, MasterList, MasterIndex, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, callback);
	}

	if(!OrderFitOnMaster(Picklist, MasterList[MasterIndex], ToteVolume, toteMaxWeight)){
		MasterIndex++;
		return CheckIfPickListFitsInWCSMaster(Picklist, MasterList, MasterIndex, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, callback);
	}

	let Results = CheckIfProductsAreTheSame(MasterList[MasterIndex], Picklist);
	if(!Results){
		MasterIndex++;
		return CheckIfPickListFitsInWCSMaster(Picklist, MasterList, MasterIndex, maxOrdersToConsolidate, ToteVolume, toteMaxWeight, callback);
	}

	return callback({Master: MasterList[MasterIndex]});
} /* CheckIfPickListFitsInWCSMaster */

function CheckIfProductsAreTheSame(Master, Picklist){
	if(Master.orders.length <= 0){
		return(true);
	}

	let x = 0;
	let SlaveItem = GetPickListUniqueSKUs(Picklist);
	let MasterItem = GetPickListUniqueSKUs(Master);
	let totalMatches = 0;

	if(SlaveItem.length < MasterItem.length){
		while(x < SlaveItem.length){
			let y = 0;
			while(y < MasterItem.length){
				if(SlaveItem[x] == MasterItem[y]){
					totalMatches++;
				}
				y++;
			}
			x++;
		}

		if(totalMatches == SlaveItem.length){
			return(true);
		}

		return(false);
	}

	while(x < MasterItem.length){
		let y = 0;
		while(y < SlaveItem.length){
			if(MasterItem[x] == SlaveItem[y]){
				totalMatches++;
			}
			y++;
		}
		x++;
	}

	if(totalMatches == MasterItem.length){
		return(true);
	}

	return(false);
} /* CheckIfProductsAreTheSame */

function GetPickListUniqueSKUs(PL){
	let ItemList = [];
	let x = 0;
	while(x < PL.orders.length){
		let y = 0;
		while(y < PL.orders[x].items.length){
			if (ItemList.length == 0) {
				ItemList.push(PL.orders[x].items[y].sku);
			} else {
				let z = 0;
				while(z < ItemList.length){
					if(ItemList[z] == PL.orders[x].items[y].sku){
						break;
					}
					z++;
				}

				if(z >= ItemList.length){
					ItemList.push(PL.orders[x].items[y].sku);
				}
			}
			y++;
		}
		x++;
	}

	return ItemList;
} /* GetPickListUniqueSKUs */

function CreateNewMaster(packType, callback) {
	let NewPL = new pickList({
		toteID: null,
		transporterID: null,
		destination: null,
		packType: packType,
		toteInUse: false,
		toteInUseBy: null,
		orders: [],
		PackedDate: null,
		Status: 'NEW',
		FromTote: null,
		PickUpdateRequired: false,
		PackUpdateRequired: false,
		ShipUpdateRequired: false,
		WeightUpdateRequired: false,
		PickListType: 'WCS_MASTER',
		CreateDate: moment(new Date()).format('YYYY-MM-DD HH:mm')
	});

	NewPL.save(function(erroor, Detail){
		if(erroor){
			WriteToFile('Error Creating a new Master PickList');
			return callback(null);
		}

		WriteToFile('Created a new WCS_MASTER pickList');
		return callback(Detail);
	});
} /* CreateNewMaster */

function CalcPLWeightandVol(items){
	let PickListWeightVolume = [];
	let weight = 0;
	let volume = 0;

	if(items.length > 0){
		let x = 0;
		while(x < items.length){
			weight += ((items[x].qty * items[x].eachWeight) / 1);

			if(items[x].eachVolume){
				volume += ((items[x].qty * items[x].eachVolume) / 1);
			}

			x++;
		}

		PickListWeightVolume[0] = weight;
		PickListWeightVolume[1] = volume;
	}

	return PickListWeightVolume;
} /* CalcPLWeightandVol */

function WaitForTrigger(){
	setTimeout(function(){
		GetSysParams();
	}, 60000);
} /* WaitForTrigger */

function WriteToFile(Msg){
	if(Msg){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' ' + Msg);
	}
} /* WriteToFile */
