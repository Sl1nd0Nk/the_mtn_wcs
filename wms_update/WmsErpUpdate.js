var mongoose       = require('mongoose');
mongoose.Promise   = require('bluebird');
var rest           = require('restler');
var config         = require('wms_update');
var moment         = require('moment');
var async          = require('async');

SOShipmentEndPoint = config.HOST.SalesOrderShipmentUpdateUrl;
HostEndPoint       = config.HOST.packedUpdateUrl;

var mongooseSchema = mongoose.Schema // MongoDB Schema class object.

var pickList       = require('pickListModel');
var PackUpdate     = require('PackUpdatesModel');
var PickLineUpdate = require('PickLineUpdatesModel');
var ShipUpdate     = require('ShipUpdatesModel');
var ErpUpdate      = require('ErpUpdatesModel');

mongoose.connect(config.mongoDB.url, function(err){
	if(err){
		WriteToFile('MONGO CONNECT ERROR ' + err);
		return;
	}

	WriteToFile('CONNECTED TO MONGODB');
	WaitForTrigger();
});

function WriteToFile(Msg){
	if(Msg){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + Msg);
	}
} /* WriteToFile */

function WaitForTrigger(){
	setTimeout(function(){
    	RunUpdates();
  	}, 30000);
} /* WaitForTrigger */

function RunERPUpdate(OrderID, Status, callback){
	rest.post(HostEndPoint,{
		data:{
			'orderID': OrderID,
			'status' : Status
		}
	}).on('complete', function(result){
		if (result instanceof Error){
			WriteToFile('Error while sending order ' + OrderID + ' (' + Status + ') status to ERP: ' + result.message);
			return callback({Error: true, RespResult: result.message});
		}

		WriteToFile('Successfully sent order ' + OrderID + ' (' + Status + ') status to ERP. | Result: ' + result);
		return callback({Error: false, RespResult: result});
	});
} /* RunERPUpdate */

function SendEachErpOrder(ErpOrders, index, callback){
	if(index >= ErpOrders.length){
		return callback();
	}

	let OrderData = ErpOrders[index];
	RunERPUpdate(OrderData.OrderId, OrderData.Status, function(Resp){
		if(Resp.Error){
			OrderData.Error = true;
		} else {
			OrderData.Required = false;
			OrderData.Error = false;
		}

		OrderData.Sent = true;
		OrderData.Result = Resp.RespResult;
		OrderData.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
		OrderData.save(function(err, savedDoc){
			index++;
			return SendEachErpOrder(ErpOrders, index, callback);
		});
	});
} /* SendEachErpOrder */

function CheckErpPicked(Resp, callback){
	if(!Resp.ErpPicked || Resp.ErpPicked.length <= 0){
		WriteToFile('No Orders Required To Send (Picked) Status To ERP');
		return callback();
	}

	SendEachErpOrder(Resp.ErpPicked, 0, function(){
		WriteToFile('Processed ' + Resp.ErpPicked.length + ' Order(s) Required To Send (Picked) Status To ERP');
		return callback();
	});
} /* CheckErpPicked */

function CreateErpUpdate(OrderId, Status){
	ErpUpdate.findOne({'OrderId': OrderId, 'Status': Status}, function(err, Ep){
		if(err){
			return;
		}

		if(Ep){
			return;
		}

		let NewRec = new ErpUpdate({
			OrderId: OrderId,
			Status: Status,
			Required: true,
			CreateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
		});

		NewRec.save(function(err, savedDoc){
			WriteToFile('Marked order ' + OrderId + ' required to send ' + Status + ' update to Erp');
			return;
		});
	});
} /* CreateErpUpdate */

function GetValidOrdersToSend(ErpList, Status, index, ValidList, callback){
	if(index >= ErpList.length){
		return callback(ValidList);
	}

	let Order = ErpList[index];
	ErpUpdate.findOne({'OrderId': Order.OrderId, 'Status': Status}, function(err, ErpOrd){
		if(err){
			index++;
			return GetValidOrdersToSend(ErpList, Status, index, ValidList, callback);
		}

		if(!ErpOrd){
			CreateErpUpdate(Order.OrderId, Status);
			index++;
			return GetValidOrdersToSend(ErpList, Status, index, ValidList, callback);
		}

		if(ErpOrd.Required){
			index++;
			return GetValidOrdersToSend(ErpList, Status, index, ValidList, callback);
		}

		if(!ErpOrd.Sent || ErpOrd.Error){
			if(!ErpOrd.Required){
				ErpOrd.Required = true;
				ErpOrd.save(function(err, savedDoc){
					index++;
					return GetValidOrdersToSend(ErpList, Status, index, ValidList, callback);
				});

				return;
			}

			index++;
			return GetValidOrdersToSend(ErpList, Status, index, ValidList, callback);
		}

		ValidList.push(Order);
		index++;
		return GetValidOrdersToSend(ErpList, Status, index, ValidList, callback);
	});
} /* GetValidOrdersToSend */

function CheckErpPacked(Resp, callback){
	if(!Resp.ErpPacked || Resp.ErpPacked.length <= 0){
		WriteToFile('No Orders Required To Send (Packed) Status To ERP');
		return callback();
	}

	GetValidOrdersToSend(Resp.ErpPacked, 'Picked', 0, [], function(PackedValidOrders){
		if(!PackedValidOrders || PackedValidOrders.length <= 0){
			WriteToFile('No Valid Orders Required To Send (Packed) Status To ERP');
			return callback();
		}

		SendEachErpOrder(PackedValidOrders, 0, function(){
			WriteToFile('Processed ' + PackedValidOrders.length + ' Order(s) Required To Send (Packed) Status To ERP');
			return callback();
		});
	});
} /* CheckErpPacked */

function CheckErpShipped(Resp, callback){
	if(!Resp.ErpShipped || Resp.ErpShipped.length <= 0){
		WriteToFile('No Orders Required To Send (Shipped) Status To ERP');
		return callback();
	}

	GetValidOrdersToSend(Resp.ErpShipped, 'Packed', 0, [], function(ShippedValidOrders){
		if(!ShippedValidOrders || ShippedValidOrders.length <= 0){
			WriteToFile('No Valid Orders Required To Send (Shipped) Status To ERP');
			return callback();
		}

		SendEachErpOrder(ShippedValidOrders, 0, function(){
			WriteToFile('Processed ' + ShippedValidOrders.length + ' Order(s) Required To Send (Shipped) Status To ERP');
			return callback();
		});
	});
} /* CheckErpShipped */

function SendErpStatusUpdates(Resp, callback){
	CheckErpPicked(Resp, function(){
		CheckErpPacked(Resp, function(){
			CheckErpShipped(Resp, function(){
				return callback();
			});
		});
	});
} /* SendErpStatusUpdates */

function RunUpdates(){
	let Stack = {};

	Stack.ErpPicked = function(callback){
		ErpUpdate.find({'Required': true, 'Status': 'Picked'}, function(err, PLineArr){
			return callback(err, PLineArr);
		});
	} /* ErpPicked */

	Stack.ErpPacked = function(callback){
		ErpUpdate.find({'Required': true, 'Status': 'Packed'}, function(err, PackPLArr){
			return callback(err, PackPLArr);
		});
	} /* ErpPacked */

	Stack.ErpShipped = function(callback){
		ErpUpdate.find({'Required': true, 'Status': 'Shipped'}, function(err, ShipPLArr){
			return callback(err, ShipPLArr);
		});
	} /* ErpShipped */

	async.parallel(Stack, function(err, Result){
		if(err){
			WriteToFile('Erp Update Find Err: ' + err);
			return WaitForTrigger();
		}

		SendErpStatusUpdates(Result, function(){
			return WaitForTrigger();
		});
	});
} /* RunUpdates */

///////////////////////////// END HELPERS //////////////////////////////////////////////////////
