var mongoose       = require('mongoose');
mongoose.Promise   = require('bluebird');
var rest           = require('restler');
var config         = require('wms_update');
var moment         = require('moment');
var async          = require('async');

SOShipmentEndPoint = config.HOST.SalesOrderShipmentUpdateUrl;
HostEndPoint       = config.HOST.packedUpdateUrl;

var mongooseSchema = mongoose.Schema // MongoDB Schema class object.

var pickList       = require('pickListModel');
var PackUpdate     = require('PackUpdatesModel');
var PickLineUpdate = require('PickLineUpdatesModel');
var UnPickUpdate   = require('UnpickUpdatesModel');
var ShipUpdate     = require('ShipUpdatesModel');
var ErpUpdate      = require('ErpUpdatesModel');
var PackedSerial   = require('PackedSerialUpdatesModel');
var Params 		   = require('parameterModel');
var IncRoute       = require('IncorrectCourierModel');

function SetToStatusEachPick(PLArr, x, WmsOrderStatus, Result, callback){
	if(x >= PLArr.length){
		return callback();
	}

	let PickLineRec = PLArr[x];
	PickLineRec.WmsOrderStatus = WmsOrderStatus;
	PickLineRec.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
	PickLineRec.Result = Result;
	PickLineRec.save(function(err, savedDoc){
		x++;
		return SetToStatusEachPick(PLArr, x, WmsOrderStatus, Result, callback);
	});
} /* SetToStatusEachPick */

function SetToStatusEachPack(PLArr, x, WmsOrderStatus, Result, callback){
	if(x >= PLArr.length){
		return callback();
	}

	let PackRec = PLArr[x];
	PackRec.WmsOrderStatus = WmsOrderStatus;
	PackRec.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
	PackRec.Result = Result;
	PackRec.save(function(err, savedDoc){
		x++;
		return SetToStatusEachPack(PLArr, x, WmsOrderStatus, Result, callback);
	});
} /* SetToStatusEachPack */

function UpdatePicksToStatus(OrderID, WmsOrderStatus, Result){
	PickLineUpdate.find({'Required': false, 'OrderId': OrderID, 'WmsOrderStatus': {$ne: WmsOrderStatus}}, function(err, PLArr){
		if(err){
			return;
		}

		if(!PLArr || PLArr.length <= 0){
			return;
		}

		SetToStatusEachPick(PLArr, 0, WmsOrderStatus, Result, function(){
			return;
		});
	});
} /* UpdatePicksToStatus */

function SetCompleteStatusToEachPicklistLine(PLArr, x, WmsPicklistStatus, callback){
	if(x >= PLArr.length){
		return callback();
	}

	let PickLineRec = PLArr[x];
	if(PickLineRec.WmsPicklistStatus == 'CANCELLED' || PickLineRec.WmsPicklistStatus == 'CANCELED'){
		x++;
		return SetCompleteStatusToEachPicklistLine(PLArr, x, WmsPicklistStatus, callback);
	}

	PickLineRec.WmsPicklistStatus = WmsPicklistStatus;
	PickLineRec.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
	PickLineRec.save(function(err, savedDoc){
		x++;
		return SetCompleteStatusToEachPicklistLine(PLArr, x, WmsPicklistStatus, callback);
	});
} /* SetCompleteStatusToEachPicklistLine */

function UpdatePicklistToStatus(pickListID, WmsPicklistStatus, callback){
	PickLineUpdate.find({'Required': false, 'PickListID': pickListID, 'WmsPicklistStatus': {$ne: WmsPicklistStatus}}, function(err, PLArr){
		if(err){
			return callback();
		}

		if(!PLArr || PLArr.length <= 0){
			return callback();
		}

		SetCompleteStatusToEachPicklistLine(PLArr, 0, WmsPicklistStatus, function(){
			return callback();
		});
	});
} /* UpdatePicklistToStatus */

function SendPickLineUpdateToWms(Order, itemIndex, toteID, PLData, callback){
	let UserId = Order.items[itemIndex].pickedBy;

	if(!UserId){
		UserId = 'Admin';
	}

	let PickLineData = {
		'pickListID'    : Order.pickListID,
		'userID'        : UserId,
		'pickListLineNo': Order.items[itemIndex].pickListOrderLine,
		'pickQTY'       : Order.items[itemIndex].pickQty,
		'toteID'        : toteID
	}

    rest.postJson(config.WMS.pick, PickLineData).on('complete', function(result){
		if(result instanceof Error){
			WriteToFile('Error picking picklist: ' + Order.pickListID + ' | Line: ' + Order.items[itemIndex].pickListOrderLine + ' | ErrorMsg: ' + result.message);
			PLData.Result = result.message;
		} else {
			let Response = JSON.parse(result);
			WriteToFile('PickListID: ' + Order.pickListID + ' | ToteID: ' + toteID + ' | WmsOrderStatus: ' + Response.OrderStatus + ' | WmsPicklistStatus: ' + Response.PickListStatus + ' | PickResultMsg: ' + Response.Message);

			PLData.Result = Response.Message;
			PLData.WmsPicklistStatus = Response.PickListStatus;
			PLData.WmsOrderStatus = Response.OrderStatus;

			if(PLData.WmsOrderStatus == 'PICKED'){
				CreateErpUpdate(Order.orderID, 'Picked');

				UpdatePicklistToStatus(Order.pickListID, PLData.WmsPicklistStatus, function(){
					UpdatePicksToStatus(Order.orderID, PLData.WmsOrderStatus, PLData.Result);
				});
			} else {
				if(PLData.WmsPicklistStatus == 'COMPLETE'){
					UpdatePicklistToStatus(Order.pickListID, PLData.WmsPicklistStatus, function(){});
				}
			}

			PLData.Required = false;
		}

		PLData.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
		PLData.save(function(err, savedDoc){
			return callback();
		});
    });
} /* SendPickLineUpdateToWms */

function SendUnPickUpdateToWms(UnPickPLData, callback){
	let UnPickData = {
		'pickListID'    : UnPickPLData.PickListID,
		'userID'        : UnPickPLData.UserId,
		'pickListLineNo': UnPickPLData.PickListLine
	}

    rest.postJson(config.WMS.unpick, UnPickData).on('complete', function(result){
		if(result instanceof Error){
			WriteToFile('Error unpicking picklist: ' + UnPickPLData.PickListID + ' | Line: ' + UnPickPLData.PickListLine + ' | ErrorMsg: ' + result.message);
			UnPickPLData.Result = result.message;
		} else {
			let Response = JSON.parse(result);
			WriteToFile('UnPick PickListID: ' + UnPickPLData.PickListID + ' | WmsOrderStatus: ' + Response.OrderStatus + ' | WmsPicklistStatus: ' + Response.PickListStatus + ' | PickResultMsg: ' + Response.Message);

			UnPickPLData.Result = Response.Message;
			UnPickPLData.WmsPicklistStatus = Response.PickListStatus;
			UnPickPLData.WmsOrderStatus = Response.OrderStatus;
			UnPickPLData.Required = false;
		}

		UnPickPLData.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
		UnPickPLData.save(function(err, savedDoc){
			return callback();
		});
    });
} /* SendUnPickUpdateToWms */

function FindAndSendForEachPickLine(PLineArr, index, callback){
	if(index >= PLineArr.length){
		return callback();
	}

	let PLData = PLineArr[index];
	pickList.findOne({'orders.pickListID': PLData.PickListID, 'orders.orderID': PLData.OrderId, 'orders.items.pickListOrderLine': PLData.PickListLine}, function(err, PL){
		if(err || !PL){
			WriteToFile('Failed to find picklist record in picklists for picklistID ' + PLData.PickListID + '. ' + err);
			return callback();
		}

		let x = 0;
		ordIndex = -1;
		while(x < PL.orders.length){
			if(PL.orders[x].orderID == PLData.OrderId){
				ordIndex = x;
				break;
			}
			x++;
		}

		if(ordIndex < 0){
			WriteToFile('Order ' + PLData.OrderId + ' not found in picklist for picklistID ' + PLData.PickListID + '. ');
			return callback();
		}

		x = 0;
		let Order = PL.orders[ordIndex];
		let itemIndex = -1;
		while(x < Order.items.length){
			if(Order.items[x].pickListOrderLine == PLData.PickListLine){
				itemIndex = x;
				break;
			}
			x++;
		}

		if(itemIndex < 0){
			WriteToFile('No item found with picklist line ' + PLData.PickListLine + ' in order ' + PLData.OrderId + ' for picklistID ' + PLData.PickListID + '. ');
			return callback();
		}

		SendPickLineUpdateToWms(Order, itemIndex, PL.toteID, PLData, function(){
			index++;
			return FindAndSendForEachPickLine(PLineArr, index, callback);
		});
	});
} /* FindAndSendForEachPickLine */

function FindAndSendForEachUnPick(PLineArr, index, callback){
	if(index >= PLineArr.length){
		return callback();
	}

	let PLData = PLineArr[index];
	SendUnPickUpdateToWms(PLData, function(){
		index++;
		return FindAndSendForEachUnPick(PLineArr, index, callback);
	});
} /* FindAndSendForEachUnPick */

function AddPickLineToTable(PackData, callback){
	pickList.findOne({'orders.pickListID': PackData.PickListID}, function(err, PL){
		if(err){
			WriteToFile('Failed to find picklist. ' + err);
			return callback();
		}

		if(!PL){
			PackData.Required = false;
			PackData.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			PackData.Result = 'Picklist Not Found On The System.'

			PackData.save(function(err, savedDoc){
				return callback();
			});
			return;
		}

		let x = 0;
		let OrdNdx = -1;
		while(x < PL.orders.length){
			if(PL.orders[x].pickListID == PackData.PickListID){
				OrdNdx = x;
				break;
			}
			x++;
		}

		if(OrdNdx < 0){
			PackData.Required = false;
			PackData.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			PackData.Result = 'Order Not Found In The PickList.'

			PackData.save(function(err, savedDoc){
				return callback();
			});
			return;
		}

		x = 0;
		let Order = PL.orders[OrdNdx];
		while(x < Order.items.length){
			if(Order.items[x].pickListOrderLine == PackData.PickListLine){
				break;
			}
			x++;
		}

		if(x >= Order.items.length){
			PackData.Required = false;
			PackData.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			PackData.Result = 'PickListLine Not Found In Order In The PickList.'

			PackData.save(function(err, savedDoc){
				return callback();
			});
			return;
		}

		let NewRec = new PickLineUpdate({
			PickListID: Order.pickListID,
			WcsId: (PL.FromTote)? PL.FromTote: PL.toteID,
			PickListLine: Order.items[x].pickListOrderLine,
			OrderId: Order.orderID,
			Required: true,
			CreateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
		});

		NewRec.save(function(err, savedDoc){
			WriteToFile('info', 'Marked picklist: ' + Order.pickListID + ' | Line: ' + Order.items[x].pickListOrderLine + ' | PickLineRequired to true');
			return callback();
		});
	});
} /* AddPickLineToTable */

function CreateShipUpdate(mPL){
	ShipUpdate.findOne({'OrderId': mPL.orders[0].orderID}, function(err, uPL){
		if(err){
			return;
		}

		if(uPL){
			return;
		}

		let NewRec = new ShipUpdate({
			OrderId: mPL.orders[0].orderID,
			Required: true,
			CreateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
		});

		NewRec.save(function(err, savedDoc){
			console.log('Marked Order: ' + mPL.orders[0].orderID + ' ShipUpdate to true');
			return;
		});
	});
} /* CreateShipUpdate */

function CreatePackedSerialUpdate(mPL){
	PackedSerial.findOne({'PickListID': mPL.orders[0].pickListID}, function(err, uPL){
		if(err){
			return;
		}

		if(uPL){
			return;
		}

		let NewRec = new PackedSerial({
			PickListID: mPL.orders[0].pickListID,
			WcsId: mPL.orders[0].cartonID,
			OrderId: mPL.orders[0].orderID,
			Required: false,
			CreateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
		});

		NewRec.save(function(err, savedDoc){
			console.log('Marked Picklist: ' + mPL.orders[0].pickListID + ' | Carton: ' + mPL.orders[0].cartonID + ' | Order: ' + mPL.orders[0].orderID + ' Serial Update Required to true');
			return;
		});
	});
} /* CreatePackedSerialUpdate */

function AddPackLineToTable(OrderID, PickListID, CartonID, PickListLineNo){
	let NewRec = new PackUpdate({
		PickListID: PickListID,
		WcsId: CartonID,
		PickListLine: PickListLineNo,
		OrderId: OrderID,
		Required: true,
		CreateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
	});

	NewRec.save(function(err, savedDoc){
		WriteToFile('Marked picklist: ' + PickListID + ' | Line: ' + PickListLineNo + ' | Pack Update Required to true');
		return;
	});
} /* AddPackLineToTable */

function CreateErpUpdate(OrderId, Status){
	ErpUpdate.findOne({'OrderId': OrderId, 'Status': Status}, function(err, Ep){
		if(err){
			return;
		}

		if(Ep){
			return;
		}

		let NewRec = new ErpUpdate({
			OrderId: OrderId,
			Status: Status,
			Required: true,
			CreateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
		});

		NewRec.save(function(err, savedDoc){
			WriteToFile('Marked order ' + OrderId + ' required to send ' + Status + ' update to Erp');
			return;
		});
	});
} /* CreateErpUpdate */

function UpdatePackUpdateToStatus(OrderID, WmsOrderStatus, Result){
	UpdatePicksToStatus(OrderID, WmsOrderStatus, Result);

	PackUpdate.find({'Required': false, 'OrderId': OrderID, 'WmsOrderStatus': {$ne: WmsOrderStatus}}, function(err, PLArr){
		if(err){
			return;
		}

		if(!PLArr || PLArr.length <= 0){
			return;
		}

		SetToStatusEachPack(PLArr, 0, WmsOrderStatus, Result, function(){
			return;
		});
	});
} /* UpdatePackUpdateToStatus */

function UpdateShipUpdateToStatus(OrderID, WmsOrderStatus, Result){
	UpdatePackUpdateToStatus(OrderID, WmsOrderStatus, Result);
} /* UpdateShipUpdateToStatus */

function WmsPackPicklist(PackData, callback){
	pickList.findOne({'orders.pickListID': PackData.PickListID}, function(err, PL){
		if(err){
			WriteToFile('Failed to find picklist. ' + err);
			return callback();
		}

		if(!PL){
			PackData.Required = false;
			PackData.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			PackData.Result = 'Picklist Not Found On The System.'

			PackData.save(function(err, savedDoc){
				return callback();
			});
			return;
		}

		let x = 0;
		while(x < PL.orders.length){
			if(PL.orders[x].pickListID == PackData.PickListID){
				break;
			}
			x++;
		}

		if(x >= PL.orders.length){
			PackData.Required = false;
			PackData.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			PackData.Result = 'Order Not Found In Picklist.'

			PackData.save(function(err, savedDoc){
				return callback();
			});
			return;
		}

		let OrdNdx = x;
		let Order = PL.orders[OrdNdx];
		x = 0;
		while(x < Order.items.length){
			if(Order.items[x].pickListOrderLine == PackData.PickListLine){
				break;
			}
			x++;
		}

		if(x >= Order.items.length){
			PackData.Required = false;
			PackData.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			PackData.Result = 'PickListLine Not Found In Order In The PickList.'

			PackData.save(function(err, savedDoc){
				return callback();
			});
			return;
		}

		let User = Order.items[x].packedBy;
		if(!User){
			User = 'Admin';
		}

		let DataForPack = {
		  'pickListID'    : Order.pickListID,
		  'pickListLineNo': Order.items[x].pickListOrderLine,
		  'cartonID'      : Order.cartonID,
		  'userID'        : User
		}

		rest.postJson(config.WMS.pack, DataForPack).on('complete', function(result){
			if (result instanceof Error){
				WriteToFile('Error packing picklist: ' + Order.pickListID + ' | pickListLineNo: ' + Order.items[x].pickListOrderLine + ' | orderID: ' + Order.orderID + ' | cartonID' + Order.cartonID + ' | ErrMsg: ' + result.message);
				PackData.Result = result.message;
			} else {
				let Response = JSON.parse(result);
				WriteToFile('Response Message: ' + Response.Message);

				PackData.Result = Response.Message;

				if(Response.Message == "Load already packed" || Response.Message == "Load does not exists" || Response.Message == "Load already packed to a different packing list"){
					PackData.WmsPackLineStatus = 'PACKED';
					PackData.Result = 'Success';
				} else {
					PackData.WmsPackLineStatus = Response.cartonStatus;
					if(PackData.WmsPackLineStatus == 'NEW'){
						PackData.WmsPackLineStatus = 'PACKED';
					}
				}

				if(Response.Message == 'Could Not Find ToLoad to Pack'){
					PackData.WmsPackLineStatus = 'RELEASED';
				}

				let PStatus = Response.OrderStatus;

				PackData.WmsOrderStatus = PStatus;
				PackData.Required = false;

				if(PackData.Result == 'PACKED' || PackData.Result == 'SHIPPED' || PackData.Result == 'CANCELED' || PackData.Result == 'CANCELLED'){
					PackData.Result = 'Success';
				}

				CreatePackedSerialUpdate(PL);

				if(PackData.WmsOrderStatus == 'PACKED'){
					CreateErpUpdate(Order.orderID, 'Packed');

					UpdatePackUpdateToStatus(Order.orderID, PackData.WmsOrderStatus, PackData.Result);
				}
			}

			PackData.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			PackData.save(function(err, savedDoc){
				return callback();
			});
		});
	});
} /* WmsPackPicklist */

function FindAndSendForEachPackUpdate(PackPLArr, index, callback){
	if(index >= PackPLArr.length){
		return callback();
	}

	let PackData = PackPLArr[index];
	PickLineUpdate.findOne({'PickListID': PackData.PickListID, 'PickListLine': PackData.PickListLine}, function(err, PLine){
		if(err){
			WriteToFile('Failed to find picklist lines in pick update table. ' + err);
			return callback();
		}

		if(!PLine){
			WriteToFile('picklist does not exist in pick update table. Adding it in');
			AddPickLineToTable(PackData, function(){
				index++;
				return FindAndSendForEachPackUpdate(PackPLArr, index, callback);
			});
			return;
		}

		if(PLine.WmsPicklistStatus != 'COMPLETE'){
			WriteToFile('Cannot pack picklist ' + PackData.PickListID + ' Line ' + PackData.PickListLine + ', line not picked.');

			if(PLine.WmsPicklistStatus == 'CANCELED'){
				WriteToFile('picklist ' + PackData.PickListID + ' Line ' + PackData.PickListLine + ' WMS status = CANCELED');
				index++;
				return FindAndSendForEachPackUpdate(PackPLArr, index, callback);
			}

			if(PLine.Required){
				index++;
				return FindAndSendForEachPackUpdate(PackPLArr, index, callback);
			}

			PLine.Required = true;
			PLine.save(function(err, savedDoc){
				index++;
				return FindAndSendForEachPackUpdate(PackPLArr, index, callback);
			});

			return;
		}

		WmsPackPicklist(PackData, function(){
			index++;
			return FindAndSendForEachPackUpdate(PackPLArr, index, callback);
		});
	});
} /* FindAndSendForEachPackUpdate */

function CreateErpOrderShipment(ShipData){
	rest.post(SOShipmentEndPoint,{
		data:{'orderID': ShipData.OrderId}
	}).on('complete', function(result){
		if(result instanceof Error){
			WriteToFile('Error while sending order shipment trigger: ' + result.message);
		} else {
			WriteToFile('Sales Order Shipment Trigger Sent -> OrderID: ' + ShipData.OrderId);
		}
	});
} /* CreateErpOrderShipment */

function RemoveRequiredsForEachPL(PLArr, index, callback){
	if(index >= PLArr.length){
		return callback();
	}

	let PL = PLArr[index];
	PL.PickUpdateRequired = false;
  	PL.PackUpdateRequired = false;
  	PL.ShipUpdateRequired = false;

  	PL.save(function(err, savedDoc){
		index++;
		return RemoveRequiredsForEachPL(PLArr, index, callback);
	});
} /* RemoveRequiredsForEachPL */

function RemoveRequiredStatusesFromPLs(OrderId){
	pickList.find({'orders.orderID': OrderId}, function(err, PLArr){
		if(err){
			WriteToFile('Failed to find picklist. ' + err);
			return;
		}

		if(!PLArr || PLArr.length <= 0){
			return;
		}

		RemoveRequiredsForEachPL(PLArr, 0, function(){
			return;
		});
	});
} /* RemoveRequiredStatusesFromPLs */

function ShipWMSOrder(ShipData, callback){
    let shipOrderData = {
		'consignee': '45146',
		'orderID'  : ShipData.OrderId,
		'userID'   : 'Admin'
	}

    rest.postJson(config.WMS.ship, shipOrderData).on('complete', function(result){
		if(result instanceof Error){
			WriteToFile('Error shipping picklist: ' + result.message);
			ShipData.Result = result.message;
		} else {
			WriteToFile('Successfully sent ship update to wms for order ' + ShipData.OrderId);
			let Response = JSON.parse(result);
			ShipData.WmsOrderStatus = Response.OrderStatus;
			ShipData.Result = Response.Message;

			if(ShipData.WmsOrderStatus == 'SHIPPING' || ShipData.WmsOrderStatus == 'SHIPPED' || ShipData.WmsOrderStatus == 'CANCELED'){
				if(ShipData.WmsOrderStatus == 'SHIPPING'){
					ShipData.WmsOrderStatus = 'SHIPPED';
				}

				if(ShipData.Result == 'Cant Ship order, incorrect status'){
					ShipData.Result = 'Success';
				}

				ShipData.Required = false;

				if(ShipData.WmsOrderStatus != 'CANCELED'){
					CreateErpOrderShipment(ShipData);
					CreateErpUpdate(ShipData.OrderId, 'Shipped');
				}

				UpdateShipUpdateToStatus(ShipData.OrderId, ShipData.WmsOrderStatus, 'Success');
			}
		}

		RemoveRequiredStatusesFromPLs(ShipData.OrderId);

		ShipData.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
		ShipData.save(function(err, savedDoc){
			return callback();
		});
    });
} /* ShipWMSOrder */

function CheckEachItemInRec(Rec, y, callback){
	if(y >= Rec.orders[0].items.length){
		return callback({AllPacked: true});
	}

	PackUpdate.findOne({'PickListID': Rec.orders[0].pickListID, 'PickListLine': Rec.orders[0].items[y].pickListOrderLine}, function(err, PackLine){
		if(err){
			return callback({AllPacked: false});
		}

		if(!PackLine){
			AddPackLineToTable(Rec.orders[0].orderID, Rec.orders[0].pickListID, Rec.orders[0].cartonID, Rec.orders[0].items[y].pickListOrderLine);
			return callback({AllPacked: false});
		}

		if(PackLine.WmsOrderStatus != 'PACKED' && PackLine.WmsOrderStatus != 'SHIPPED' && PackLine.WmsOrderStatus != 'SHIPPING' && PackLine.WmsOrderStatus != 'CANCELED' && PackLine.WmsOrderStatus != 'CANCELLED'){
			if(PackLine.Required){
				return callback({AllPacked: false});
			}

			if(PackLine.WmsPackLineStatus != 'PACKED' && PackLine.WmsPackLineStatus != 'SHIPPED' && PackLine.WmsPackLineStatus != 'SHIPPING' && PackLine.WmsPackLineStatus != 'CANCELED' && PackLine.WmsPackLineStatus != 'CANCELLED'){
				PackLine.Required = true;
				PackLine.save(function(err, savedDoc){
					return callback({AllPacked: false});
				});

				return;
			}
		}

		y++;
		return CheckEachItemInRec(Rec, y, callback);
	});
} /* CheckEachItemInRec */

function CheckIfAllPacked(PLArr, x, callback){
	if(x >= PLArr.length){
		return callback({AllPacked: true});
	}

	if(PLArr[x].Status == 'CANCELLED' ||  PLArr[x].Status == 'CANCELED'){
                x++;
                return CheckIfAllPacked(PLArr, x, callback);
        }

	let Rec = PLArr[x];
	CheckEachItemInRec(Rec, 0, function(Resp){
		if(!Resp || !Resp.AllPacked){
			return callback({AllPacked: false});
		}

		x++;
		return CheckIfAllPacked(PLArr, x, callback);
	});
} /* CheckIfAllPacked */

function ProcessEachIncRouteUpdate(IncArr, index, callback){
	if(index >= IncArr.length){
		return callback();
	}

	let Rec = IncArr[index];
    let Data = {
		'CartonID': Rec.CartonID,
		'TransporterID': Rec.TransporterID,
		'OrderID': Rec.OrderID,
		'DesignatedCourier': Rec.DesignatedCourier,
		'RoutedCourier': Rec.RoutedCourier,
		'Warehouse': ThisWarehouse
	}

	WriteToFile('=== Incorrect Route Payload ===');
	WriteToFile(JSON.stringify(Data));
	WriteToFile('|== Incorrect Route Payload ==|');

    rest.postJson(config.WMS.incRoute, Data).on('complete', function(result){
		WriteToFile('=== Payload Response ===');
		WriteToFile(result);
		WriteToFile('|== Payload Response ==|');

		if(result instanceof Error){
			WriteToFile('Error Updating Incorrect Courier Routing: ' + result.message);
			Rec.Result = result.message;
			Rec.save(function(err, sd){
				index++;
				ProcessEachIncRouteUpdate(IncArr, index, callback);
			});
			return;
		}

		let Response = JSON.parse(result);
		Rec.Result = Response.Message;
		Rec.Required = false;
		Rec.save(function(err, sd){
			index++;
			ProcessEachIncRouteUpdate(IncArr, index, callback);
		});
	});
} /* ProcessEachIncRouteUpdate */

function FindAndSendForEachShipUpdate(ShipPLArr, index, callback){
	if(index >= ShipPLArr.length){
		return callback();
	}

	let ShipData = ShipPLArr[index];
	pickList.find({'orders.orderID': ShipData.OrderId}, function(err, PLArr){
		if(err){
			WriteToFile('Failed to find picklist. ' + err);
			index++;
			return FindAndSendForEachShipUpdate(ShipPLArr, index, callback);
		}

		if(!PLArr || PLArr.length <= 0){
			ShipData.Required = false;
			ShipData.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			ShipData.Result = 'Picklist Not Found On The System.';

			ShipData.save(function(err, savedDoc){
				index++;
				return FindAndSendForEachShipUpdate(ShipPLArr, index, callback);
			});

			return;
		}

		let x = 0;
		while(x < PLArr.length){
			if(PLArr[x].Status != 'SHIPPED' && PLArr[x].Status != 'CANCELLED' && PLArr[x].Status != 'CANCELED'){
				break;
			}
			x++;
		}

		if(x < PLArr.length){
			ShipData.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			ShipData.Result = 'Not All Cartons For Order Are Shipped';

			ShipData.save(function(err, savedDoc){
				index++;
				return FindAndSendForEachShipUpdate(ShipPLArr, index, callback);
			});

			return;
		}

		x = 0;
		let LastCarton = -1;
		while(x < PLArr.length){
			if(PLArr[x].orders[0].numberOfTotes && PLArr[x].orders[0].toteNumber && PLArr[x].orders[0].numberOfTotes == PLArr[x].orders[0].toteNumber){
				LastCarton = x;
				break;
			}
			x++;
		}

		if(LastCarton < 0){
			ShipData.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			ShipData.Result = 'Failed To Find Last Carton For Order';

			ShipData.save(function(err, savedDoc){
				index++;
				return FindAndSendForEachShipUpdate(ShipPLArr, index, callback);
			});

			return;
		}

		CheckIfAllPacked(PLArr, 0, function(Resp){
			if(!Resp || !Resp.AllPacked){
				ShipData.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
				ShipData.Result = 'Order Not Wms Packed';

				ShipData.save(function(err, savedDoc){
					index++;
					return FindAndSendForEachShipUpdate(ShipPLArr, index, callback);
				});

				return;
			}

			ShipWMSOrder(ShipData, function(){
				index++;
				return FindAndSendForEachShipUpdate(ShipPLArr, index, callback);
			});
		});
	});
} /* FindAndSendForEachShipUpdate */

function ShipmentReqForEach(PLArr, index, callback){
	if(index >= PLArr.length){
		return callback();
	}

	let PL = PLArr[index];
	CreateShipUpdate(PL);

	PL.ShipUpdateRequired = false;
	PL.save(function(err, savedDoc){
		index++;
		return ShipmentReqForEach(PLArr, index, callback);
	});
} /* ShipmentReqForEach */

function CheckAnyUpdatesFailed(callback){
	pickList.find({'ShipUpdateRequired': true, 'Status': 'SHIPPED'}, function(err, PLArr){
		if(err || !PLArr || PLArr.length <= 0){
			WriteToFile('No failed updates. ' + err);
			return callback();
		}

		ShipmentReqForEach(PLArr, 0, function(){
			return callback();
		});
	});
} /* CheckAnyUpdatesFailed */

mongoose.connect(config.mongoDB.url, function(err){
	if(err){
		WriteToFile('MONGO CONNECT ERROR ' + err);
		return;
	}

	WriteToFile('CONNECTED TO MONGODB');
	WaitForTrigger();
});

function WriteToFile(Msg){
	if(Msg){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + Msg);
	}
} /* WriteToFile */

function WaitForTrigger(){
	setTimeout(function(){
    	RunUpdates();
  	}, 30000);
} /* WaitForTrigger */

function ShipPacelsFound(ShipAllowedArr, index, ExceedHour, callback){
	if(index >= ShipAllowedArr.length){
		return callback();
	}

	let CartonId = ShipAllowedArr[index];
	pickList.findOne({'orders.cartonID': CartonId}, function(err, PL){
		if(err || !PL){
			index++;
			return ShipPacelsFound(ShipAllowedArr, index, ExceedHour, callback);
		}

		if(PL.orders[0].QcCheckResult == 'Parcel Set Aside. Not All Contents Where Verified'){
			index++;
			return ShipPacelsFound(ShipAllowedArr, index, ExceedHour, callback);
		}

		PL.orders[0].QcCheckResult = 'Auto Shipped. Assumed to be handed over to the courier after ' + ExceedHour + ' hours in PACKED status with no further movement (Could be no reads)';
		PL.Status = 'SHIPPED';
		PL.orders[0].shipped = true;
		PL.dispatched = true;
		PL.ShipUpdateRequired = true;

		if(PL.transporterID){
			PL.transporterID = null;
		}

		if(!PL.actualWeight1){
			PL.actualWeight1 = (PL.orders[0].transporterWeight)? PL.orders[0].transporterWeight: PL.orders[0].picklistWeight;
		}

		if(!PL.actualWeight2){
			PL.actualWeight2 = (PL.orders[0].transporterWeight)? PL.orders[0].transporterWeight: PL.orders[0].picklistWeight;
		}

		PL.save(function(err, sd){
			index++;
			return ShipPacelsFound(ShipAllowedArr, index, ExceedHour, callback);
		});
	});
} /* ShipPacelsFound */

function CheckPacked(ExceedHour, callback){
	let MyDate = moment("2019-10-23").format('YYYY-MM-DD');
	pickList.find({'Status': 'PACKED', 'PackedDate': {$regex: MyDate, $options:'i'}}, function(err, PLArr){
		if(err || !PLArr || PLArr.length <= 0){
			WriteToFile('No pbnd found. ' + err);
			return callback();
		}

		let x = 0;
		let ShipAllowedArr = [];
		while(x < PLArr.length){
			if(PLArr[x].PackedDate){
				let PdArr = PLArr[x].PackedDate.split(' ');
				let TmArr = PdArr[1].split(':');
				let Hr = TmArr[0];

				let now = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
				let NwArr = now.split(' ');
				let NwTmArr = NwArr[1].split(':');
				let TmHr = 21; //parseInt(NwTmArr[0]);

				if((parseInt(Hr) + ExceedHour) < TmHr){
					if(PLArr[x].orders[0].cartonID && PLArr[x].orders[0].QcCheckResult != 'Parcel Set Aside. Not All Contents Where Verified'){
						ShipAllowedArr.push(PLArr[x].orders[0].cartonID);
						WriteToFile('Carton: ' + PLArr[x].toteID + ' | Packed at: ' + PLArr[x].PackedDate);
					}
				}
			}
			x++;
		}

		if(ShipAllowedArr.length <= 0){
			return callback();
		}

		ShipPacelsFound(ShipAllowedArr, 0, ExceedHour, function(){
			return callback();
		});
	});
} /* CheckPacked */

function CheckPackedExceededByExceedHour(callback){
	Params.find({}, function(err, Parameters){
		if(err || !Parameters || Parameters.length <= 0){
			WriteToFile('No parameters found');
			return callback();
		}

		if(!Parameters[0].ShipmentControl){
			WriteToFile('No Shipment Control setting');
			return callback();
		}

		if(!Parameters[0].ShipmentControl.Active || !Parameters[0].ShipmentControl.ExceedHour){
			WriteToFile('Shipment Control option not active');
			return callback();
		}

		CheckPacked(Parameters[0].ShipmentControl.ExceedHour, function(){
			return callback();
		});
	});
} /* CheckPackedExceededByExceedHour */

function RunUpdates(){
	let Stack = {};

	Stack.PickUpdateLine = function(callback){
		PickLineUpdate.find({'Required': true}, function(err, PLineArr){
			if(err || !PLineArr || PLineArr.length <= 0){
				WriteToFile("No Picked Required PickLists Found");
				return callback(err, null);
			}

			FindAndSendForEachPickLine(PLineArr, 0, function(){
				return callback(err, null);
			});
		});
	} /* PickUpdateLine */

	Stack.UnPickUpdate = function(callback){
		UnPickUpdate.find({'Required': true}, function(err, PLineArr){
			if(err || !PLineArr || PLineArr.length <= 0){
				WriteToFile("No PickLists Found For UnPicking");
				return callback(err, null);
			}

			FindAndSendForEachUnPick(PLineArr, 0, function(){
				return callback(err, null);
			});
		});
	} /* UnPickUpdate */

	Stack.PackUpdate = function(callback){
		PackUpdate.find({'Required': true}, function(err, PackPLArr){
			if(err || !PackPLArr || PackPLArr.length <= 0){
				WriteToFile("No Packed Required PickLists Found");
				return callback(err, null);
			}

			FindAndSendForEachPackUpdate(PackPLArr, 0, function(){
				return callback(err, null);
			});
		});
	} /* PackUpdate */

	Stack.ShipUpdate = function(callback){
		ShipUpdate.find({'Required': true}, function(err, ShipPLArr){
			if(err || !ShipPLArr || ShipPLArr.length <= 0){
				WriteToFile("No Shipped Required PickLists Found");
				return callback(err, null);
			}

			FindAndSendForEachShipUpdate(ShipPLArr, 0, function(){
				return callback(err, null);
			});
		});
	} /* ShipUpdate */

	Stack.IncorrectCourierUpdate = function(callback){
		IncRoute.find({'Required': true}, function(err, IncArr){
			if(err || !IncArr || IncArr.length <= 0){
				WriteToFile("No Incorrect Routed Required Updates Found");
				return callback(err, null);
			}

			ProcessEachIncRouteUpdate(IncArr, 0, function(){
				return callback(err, null);
			});
		});
	} /* IncorrectCourierUpdate */

	async.parallel(Stack, function(err, Result){
		CheckAnyUpdatesFailed(function(){
			CheckPackedExceededByExceedHour(function(){
				return WaitForTrigger();
			});
		});
	});
} /* RunUpdates */
