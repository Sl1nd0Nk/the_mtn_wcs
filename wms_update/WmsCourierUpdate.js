var mongoose       = require('mongoose');
mongoose.Promise   = require('bluebird');
var rest           = require('restler');
var config         = require('wms_update');
var moment         = require('moment');
var async          = require('async');

SOShipmentEndPoint = config.HOST.SalesOrderShipmentUpdateUrl;
HostEndPoint       = config.HOST.packedUpdateUrl;

var mongooseSchema = mongoose.Schema // MongoDB Schema class object.

var parameters     = require('parameterModel');
var pickList       = require('pickListModel');
var PackUpdate     = require('PackUpdatesModel');
var PickLineUpdate = require('PickLineUpdatesModel');
var ShipUpdate     = require('ShipUpdatesModel');
var ErpUpdate      = require('ErpUpdatesModel');
var CourierCarton  = require('CourierCartonUpdatesModel');
var WmsPackList    = require('WmsPackListUpdatesModel');

mongoose.connect(config.mongoDB.url, function(err){
	if(err){
		WriteToFile('MONGO CONNECT ERROR ' + err);
		return;
	}

	WriteToFile('CONNECTED TO MONGODB');
	WaitForTrigger();
});

function WriteToFile(Msg){
	if(Msg){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + Msg);
	}
} /* WriteToFile */

function WaitForTrigger(){
	setTimeout(function(){
    	RunUpdates();
  	}, 30000);
} /* WaitForTrigger */

function SendPacklistUpdateToWms(PL, PUpdate, callback){
	let ActualWeight1 = PL.orders[0].actualWeight1;
	if(!ActualWeight1){
		ActualWeight1 = PL.orders[0].picklistWeight;
	}

	let ActualWeight2 = PL.orders[0].actualWeight2;
	if(!ActualWeight2){
		ActualWeight2 = ActualWeight1;
	}

	rest.post(config.updatePacklistWeight1,{
		data: {
			cartonID:          PL.orders[0].cartonID,
			transporterID:     PL.transporterID,
			actualWeight1:     ActualWeight1,
			theoreticalWeight: PL.orders[0].picklistWeight,
			salesOrderID:      PL.orders[0].orderID,
			transporterTheoryWeight: PL.orders[0].transporterWeight
		}
	}).on('complete', function (result) {
		if(result instanceof Error){
			WriteToFile('Error updating packing list weight 1 for Carton ID: ' + PUpdate.CartonId + ' Err: ' + result.message);
			PUpdate.Required = false;
			PUpdate.Error = true;
			PUpdate.Sent = true;
			PUpdate.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			PUpdate.Result = result.message;

			PUpdate.save(function(err, savedDoc){
				return callback();
			});

			return;
		}

		WriteToFile('Packing List Weight 1 Success for Carton ID: ' + PUpdate.CartonId);
		rest.post(config.updatePacklistWeight2, {
			data: {
				cartonID:          PL.orders[0].cartonID,
				actualWeight2:     ActualWeight2
			}
		}).on('complete', function (result) {
			if(result instanceof Error){
				WriteToFile('Error updating packing list weight 2 for Carton ID: ' + PUpdate.CartonId + ' Err: ' + result.message);
				PUpdate.Error = true;
				PUpdate.Result = result.message;
			} else {
				WriteToFile('Packing List Weight 2 Success for Carton ID: ' + PUpdate.CartonId);
				PUpdate.Result = result;
				PUpdate.Error = false;
			}

			PUpdate.Required = false;
			PUpdate.Sent = true;
			PUpdate.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');

			PUpdate.save(function(err, savedDoc){
				return callback();
			});
		});
	});
} /* SendPacklistUpdateToWms */

function SendCartonUpdateToCourier(PL, CUpdate, callback){
	parameters.find({}, function(err, Parameters){
		if(err || !Parameters || Parameters.length <= 0){
			WriteToFile('Failed to find parameters in the system');
			CUpdate.Required = false;
			CUpdate.Error = true;
			CUpdate.Sent = false;
			CUpdate.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			CUpdate.Result = 'Failed to find parameters in the system';

			CUpdate.save(function(err, savedDoc){
				return callback();
			});

			return;
	  	}

		if(!Parameters[0].weightSettings || Parameters[0].weightSettings.length <= 0){
			WriteToFile('Failed to find Weight Settings in system parameters');
			CUpdate.Required = false;
			CUpdate.Error = true;
			CUpdate.Sent = false;
			CUpdate.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			CUpdate.Result = 'Failed to find Weight Settings in system parameters';

			CUpdate.save(function(err, savedDoc){
				return callback();
			});

			return;
		}

		let CartonPrefix = CUpdate.CartonId[0];
		let paramIndex = 0;
		let WeightSettings = Parameters[0].weightSettings;
		while(paramIndex < WeightSettings.length){
			if(WeightSettings[paramIndex].StockSize == parseInt(CartonPrefix)){
				break;
			}
			paramIndex++;
		}

		if(paramIndex >= WeightSettings.length){
			WriteToFile('Failed to find Carton ' + CartonPrefix + ' Weight Settings in system parameters.');
			CUpdate.Required = false;
			CUpdate.Error = true;
			CUpdate.Sent = false;
			CUpdate.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			CUpdate.Result = 'Failed to find Carton ' + CartonPrefix + ' Weight Settings in system parameters.';

			CUpdate.save(function(err, savedDoc){
				return callback();
			});

			return;
		}

		let Items = [];
		let itemIndex = 0;
		while(itemIndex < PL.orders[0].items.length){
			let line = {
				sku: PL.orders[0].items[itemIndex].sku,
				qty: PL.orders[0].items[itemIndex].qtyPacked,
				price: PL.orders[0].items[itemIndex].unitPrice
			}

			Items.push(line);
			itemIndex++;
		}

		let weightBoolean = false;
		let cartonWeight1 = PL.orders[0].actualWeight1;
		let cartonWeight2 = PL.orders[0].actualWeight2;
		let Weight2LowerT = null;
		let Weight2UpperT = null;

		if(!cartonWeight1){
			cartonWeight1 = PL.orders[0].picklistWeight;
		}

		if(cartonWeight2){
			cartonWeight2 = PL.orders[0].picklistWeight;
			Weight2LowerT = cartonWeight2 - WeightSettings[paramIndex].LowerTolerance;
			Weight2UpperT = cartonWeight2 + WeightSettings[paramIndex].UpperTolerance;
		}

		if(PL.orders[0].transporterID){
			cartonWeight1 = PL.orders[0].picklistWeight;
			cartonWeight2 = PL.orders[0].picklistWeight;
			weightBoolean = true;
		} else {
			if(Weight2LowerT && Weight2UpperT && cartonWeight2 >= Weight2LowerT && cartonWeight2 <= Weight2UpperT) {
				weightBoolean = true;
			}
		}

		let CourierCartonData = {
			orderID:       PL.orders[0].orderID,
			route:         PL.orders[0].courierID,
			cartonID:      PL.orders[0].cartonID,
			cartonNo:      PL.orders[0].toteNumber,
			noOfCartons:   PL.orders[0].numberOfTotes,
			transporterID: PL.orders[0].transporterID,
			cartonHeight:  WeightSettings[paramIndex].height,
			cartonWidth:   WeightSettings[paramIndex].width,
			cartonLength:  WeightSettings[paramIndex].length,
			cartonWeight1: cartonWeight1,
			cartonWeight2: cartonWeight2,
			weightOK:      weightBoolean,
			items:         Items
		}

		rest.postJson(config.courierCartonUpdate, CourierCartonData).on('complete', function(result){
			if(result instanceof Error) {
				console.log('Error sending courier carton update for Carton ID: ' + PL.orders[0].cartonID);
				CUpdate.Error = true;
				CUpdate.Result = result.message;
			} else {
				console.log('Courier carton update Success for Carton ID: ' + PL.orders[0].cartonID);
				CUpdate.Result = result;
				CUpdate.Error = false;
			}

			CUpdate.Required = false;
			CUpdate.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			CUpdate.Sent = true;

			CUpdate.save(function(err, savedDoc){
				return callback();
			});
		});
	});
} /* SendCartonUpdateToCourier */

function FindAndSendForEachCourierCartonUpdate(PLArr, index, callback){
	if(index >= PLArr.length){
		return callback();
	}

	let CUpdate = PLArr[index];
	pickList.findOne({'orders.cartonID': CUpdate.CartonId}, function(err, PL){
		if(err){
			WriteToFile('Failed to find picklist. ' + err);
			index++;
			return FindAndSendForEachCourierCartonUpdate(PLArr, index, callback);
		}

		if(!PL){
			CUpdate.Required = false;
			CUpdate.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			CUpdate.Result = 'Picklist Not Found On The System.';
			CUpdate.Error = true;

			CUpdate.save(function(err, savedDoc){
				index++;
				return FindAndSendForEachCourierCartonUpdate(PLArr, index, callback);
			});

			return;
		}

		SendCartonUpdateToCourier(PL, CUpdate, function(){
			index++;
			return FindAndSendForEachCourierCartonUpdate(PLArr, index, callback);
		});
	});
} /* FindAndSendForEachCourierCartonUpdate */

function FindAndSendForEachPacklistUpdate(PLArr, index, callback){
	if(index >= PLArr.length){
		return callback();
	}

	let PUpdate = PLArr[index];
	pickList.findOne({'orders.cartonID': PUpdate.CartonId}, function(err, PL){
		if(err){
			WriteToFile('Failed to find picklist. ' + err);
			index++;
			return FindAndSendForEachPacklistUpdate(PLArr, index, callback);
		}

		if(!PL){
			PUpdate.Required = false;
			PUpdate.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			PUpdate.Result = 'Picklist Not Found On The System.';
			PUpdate.Error = true;

			PUpdate.save(function(err, savedDoc){
				index++;
				return FindAndSendForEachPacklistUpdate(PLArr, index, callback);
			});

			return;
		}

		SendPacklistUpdateToWms(PL, PUpdate, function(){
			index++;
			return FindAndSendForEachPacklistUpdate(PLArr, index, callback);
		});
	});
} /* FindAndSendForEachPacklistUpdate */

function RunUpdates(){
	let Stack = {};

	Stack.CartonUpdates = function(callback){
		CourierCarton.find({'Required': true}, function(err, PLineArr){
			if(err || !PLineArr || PLineArr.length <= 0){
				WriteToFile("No Courier Carton Update Required PickLists Found");
				return callback(err, null);
			}

			FindAndSendForEachCourierCartonUpdate(PLineArr, 0, function(){
				return callback(err, null);
			});
		});
	} /* CartonUpdates */

	Stack.PackListUpdate = function(callback){
		WmsPackList.find({'Required': true}, function(err, PLineArr){
			if(err || !PLineArr || PLineArr.length <= 0){
				WriteToFile("No Packlist Update Required PickLists Found");
				return callback(err, null);
			}

			FindAndSendForEachPacklistUpdate(PLineArr, 0, function(){
				return callback(err, null);
			});
		});
	} /* PackListUpdate */

	async.parallel(Stack, function(err, Result){
		return WaitForTrigger();
	});
} /* RunUpdates */