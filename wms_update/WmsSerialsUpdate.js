var mongoose       = require('mongoose');
mongoose.Promise   = require('bluebird');
var rest           = require('restler');
var config         = require('wms_update');
var moment         = require('moment');
var async          = require('async');

SOShipmentEndPoint = config.HOST.SalesOrderShipmentUpdateUrl;
HostEndPoint       = config.HOST.packedUpdateUrl;

var mongooseSchema = mongoose.Schema // MongoDB Schema class object.

var parameters     = require('parameterModel');
var pickList       = require('pickListModel');
var PackUpdate     = require('PackUpdatesModel');
var PickLineUpdate = require('PickLineUpdatesModel');
var ShipUpdate     = require('ShipUpdatesModel');
var ErpUpdate      = require('ErpUpdatesModel');
var CourierCarton  = require('CourierCartonUpdatesModel');
var WmsPackList    = require('WmsPackListUpdatesModel');
var PackedSerial   = require('PackedSerialUpdatesModel');

mongoose.connect(config.mongoDB.url, function(err){
	if(err){
		WriteToFile('MONGO CONNECT ERROR ' + err);
		return;
	}

	WriteToFile('CONNECTED TO MONGODB');
	WaitForTrigger();
});

function WriteToFile(Msg){
	if(Msg){
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' ' + Msg);
	}
} /* WriteToFile */

function WaitForTrigger(){
	setTimeout(function(){
    	RunUpdates();
  	}, 30000);
} /* WaitForTrigger */

function SendSerialsToWms(PL, Record, callback){
	let items = [];

	let x = 0;
	while(x < PL.orders[0].items.length){
		if(PL.orders[0].items[x].serialised){
			if(PL.orders[0].items[x].serials && PL.orders[0].items[x].serials.length > 0){
				let orderLine = PL.orders[0].items[x].orderLine;

				let lineData = {
					'sku'      : PL.orders[0].items[x].sku,
					'serials'  : PL.orders[0].items[x].serials,
					'qty'      : PL.orders[0].items[x].qty,
					'orderLine': orderLine
				}

				items.push(lineData);
			}
		}
		x++;
	}

	rest.postJson(config.HOST.SerialUpdateUrl,{
		data:{
			'orderID':     PL.orders[0].orderID,
			'cartonID':    PL.orders[0].cartonID,
			'cartonNo':    PL.orders[0].toteNumber,
			'noOfCartons': PL.orders[0].numberOfTotes,
			'items':       items
		}
	}).on('complete', function(result){
		if(result instanceof Error){
	    	WriteToFile('Error updating serial number to WMS: ' + result.message);
            Record.Result = result.message;
            Record.Error = true;
        } else {
	    	WriteToFile('Updated Carton ' + PL.toteID + ' for OrderID:' + PL.orders[0].orderID + ' serial numbers to WMS | Result: ' + result);
	    	Record.Result = result;
	    	Record.Required = false;
	    	Record.Error = false;

            if(result == 'Please ensure that all fields are populated.' || result == 'Please ensure that each orderLine contains serials.'){
            	Record.Required = true;
            	Record.Error = true;
            }
		}

		Record.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
		Record.Sent = true;

		Record.save(function(err, savedDoc){
			return callback();
		});
	});
} /* SendSerialsToWms */

function ProcessEachUpdate(PLArr, index, callback){
	if(index >= PLArr.length){
		return callback();
	}

	let Record = PLArr[index];
	pickList.findOne({'orders.pickListID': Record.PickListID}, function(err, PL){
		if(err){
			WriteToFile('Failed to find picklist ' + Record.PickListID + '. Err: ' + err);
			index++;
			return ProcessEachUpdate(PLArr, index, callback);
		}

		if(!PL){
			Record.Required = false;
			Record.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			Record.Result = 'Picklist Not Found On The System.';
			Record.Error = true;

			Record.save(function(err, savedDoc){
				index++;
				return ProcessEachUpdate(PLArr, index, callback);
			});

			return;
		}

		if(!PL.orders || PL.orders.length <= 0){
			Record.Required = false;
			Record.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			Record.Result = 'No order attached to picklist.';
			Record.Error = true;

			Record.save(function(err, savedDoc){
				index++;
				return ProcessEachUpdate(PLArr, index, callback);
			});

			return;
		}

		if(!PL.orders[0].items || PL.orders[0].items.length <= 0){
			Record.Required = false;
			Record.LastUpdateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			Record.Result = 'No items associated with order in picklist.';
			Record.Error = true;

			Record.save(function(err, savedDoc){
				index++;
				return ProcessEachUpdate(PLArr, index, callback);
			});

			return;
		}

		SendSerialsToWms(PL, Record, function(){
			index++;
			return ProcessEachUpdate(PLArr, index, callback);
		});
	});
} /* ProcessEachUpdate */

function RunUpdates(){
	PackedSerial.find({'Required': true}, function(err, PLArr){
		if(err){
			WriteToFile('Failed to find Packed Serials To Update To WMS. Err: ' + err);
			return WaitForTrigger();
		}

		if(!PLArr || PLArr.length <= 0){
			WriteToFile('No Packed Serials To Update To WMS');
			return WaitForTrigger();
		}

		ProcessEachUpdate(PLArr, 0, function(){
			return WaitForTrigger();
		});
	});
} /* RunUpdates */