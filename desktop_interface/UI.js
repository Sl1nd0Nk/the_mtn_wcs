var express        = require('express');
var bodyParser     = require('body-parser')
//require('body-parser-xml')(bodyParser);
var mongoose       = require('mongoose');
mongoose.Promise   = require('bluebird');
var fs             = require('fs');
var moment         = require('moment');
var mustache       = require('mustache');
var async          = require('async');
//var redis          = require('redis');
var glob           = require('glob');
var formidable     = require("formidable");
var request        = require('request');
var session        = require('client-sessions')
var logging        = require('classLogging');
var app            = express();


var dbUrl          = require('ui').mongo.url;
var port           = require('ui').PackPort;
var pullWallUrl    = require('ui').pullWall;
var DefaultRoute = require('ui').DefaultRoute;
var DefaultPage  = require('ui').DefaultPage;
var Base         = require('ui').BaseSystem;



var HtmlLib	       = require('classHtml');
var html           = new HtmlLib();

var ProcName       = 'UI';
var log            = new logging();
//var Pubclient      = redis.createClient();

app.use(express.static(__dirname + '/public'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.text());

app.use(session({
  cookieName: 'session',
  secret: 'eg[isfd-8yF9-7w2315df{}+Ijsli;;to8',
  duration: 30 * 60 * 1000,
  activeDuration: 5 * 60 * 1000,
  httpOnly: true,
  secure: true,
  ephemeral: true
}));

/********************* DB Connection ***********************************/

mongoose.connect(dbUrl, function(err){
	if(err){
		log.WriteToFile(ProcName,'MONGO COULD NOT CONNECT:  ERROR ' + err);
	} else {
		log.WriteToFile(ProcName,'Connected To Mongo');
		app.listen(port);
		log.WriteToFile(ProcName,'Server running at http://127.0.0.1:' + port + '/');
	}
}); /* mongoose.connect */

/********************* routes *****************************************/
var indexRouter     = require('./routes/index.js');
var PackStations     = require('./routes/PackStations/PackStation_UI.js');
var orderPriority   = require('./routes/orderPriority/orderPriority.js');
var batchWeights    = require('./routes/batchWeights/batchWeights.js');

//var chuteRouter     = require('./routes/chute/chuteRouter.js');


app.use(PackStations);
app.use(orderPriority)
app.use(indexRouter);
app.use(batchWeights);

process.on('uncaughtException', function(err) {
    console.log('Caught exception: ' + err);
    throw err;
});


app.get('*', function(req, res){
  html.WEB_CallPageRedirect(res, '/');
});

module.exports = app
