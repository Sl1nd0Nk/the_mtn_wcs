var express        = require('express');
var mongoose       = require('mongoose');
mongoose.Promise   = require('bluebird');
var fs             = require('fs');
var moment         = require('moment');
var mustache       = require('mustache');
var async          = require('async');
var redis          = require('redis');
var glob           = require('glob');
var rest           = require('restler');
var formidable     = require("formidable");
var request        = require('request');
var session        = require('client-sessions')
var dbUrl          = require('ui').mongo.url;
var port           = require('ui').PackPort;
var pullWallUrl    = require('ui').pullWall;
var app            = express.Router();

var apexLib  = require('apex_lib/moduleIndex.js');
var config   = require('wms_api');
var sm       = require('serialModel');
var CanReq   = require('CancelRequestModel');
var UserErr  = require('UserPackErrorModel');

var Pubclient      = redis.createClient();

var packStnLib	   = require('classPackStation');
var PackUpdate     = require('PackUpdatesModel');
var pickListLib	   = require('classPicklist');
var serialLib 	   = require('classSerial');
var HtmlLib	       = require('classHtml');
var logLib	       = require('classLogging');
var paramLib       = require('classParams');
var PwLib          = require('classPutwallLocs');
var UserLib        = require('classUser');
var prnLib		   = require('classPrinting');
var batchWeightLib	= require('classBatchWeight');
var Param          = require('classParam');
app.use(express.static(__dirname + '/public'));

var DefaultRoute = require('ui').DefaultRoute;
var DefaultPage  = require('ui').DefaultPage;
var Base         = require('ui').BaseSystem;
var CartonUrl    = require('ui').CartonUrl;
var UrlArr       = Base.split('{{}}');
var Dashboard    = UrlArr[1] + 'toteTracking';

var packStn  = new packStnLib();
var pickList = new pickListLib();
var serls    = new serialLib();
var html     = new HtmlLib();
var log      = new logLib();
var param    = new paramLib();
var PwLocs   = new PwLib();
var sysuser	 = new UserLib();
var prn      = new prnLib();
var batchWeight = new batchWeightLib();
var sysParam = new Param();

/* =========== Main Function =======================================*/

function ProcessResponse(req, res, Resp){
	let user = req.session.username;
	let Ip = html.GetHeaderIpAddr(req);
	let Data = "";

	if(!Resp || !Resp.Station){
		Data = html.ShowNoStation(Resp, user, Ip);
		return SendResponse(res, Data, 'normalPacking');
	}

	if(req.session.Err){
		let Err = req.session.Err;
		delete req.session.Err;
		return ProcessResponse(req, res, {Msg: Err, Station: Resp.Station});
	}

	if(req.session.scannedtote){
		let Tote = req.session.scannedtote;
		delete req.session.scannedtote;
		return ProcFindScannedTote(req, res, Resp, Tote);
	}

	let PL = Resp.Station.pickList;
	if(!PL){
		Data = html.ShowScanTote(Resp, user, Ip);
		return SendResponse(res, Data, 'normalPacking');
	}

	if(!PL.orders || PL.orders.length <= 0){
		return ProcClearStation(req, res, Resp, null);
	}

	if(!Resp.Station.ProcessOnPutWalls){
		let OrdNdx = CalcOrderIndex(PL);
		if(OrdNdx == -1){
			return ProcApplyCorrectCompleteOption(req, res, Resp);
		}

		if(OrdNdx != Resp.Station.OrderIndex){
			return UpdateStationOrderIndex(req, res, Resp, OrdNdx);
		}
	}

	if(req.session.uid && req.session.psw){
		return ProcSupervisorOverride(req, res, Resp);
	}

	if(!req.session.errorcontrol){
		return ProcDetermineSysErrorControlMode(req, res, Resp);
	}

	if(Resp.Station.SupervisorControl && Resp.Station.UserError){
		let Station = Resp.Station;
		let SuperMsg = null;
		packStn.Update(Station, function(Resp){
			Station = Resp.SavedDoc;

			if(req.session.SupervisorErr){
				SuperMsg = req.session.SupervisorErr;
				delete req.session.SupervisorErr;
			}

			if(Station.ProcessOnPutWalls == true){
				Data = html.ProcessPwTote({SuperMsg: SuperMsg, Station: Resp.SavedDoc}, user, Ip);
				return SendResponse(res, Data, 'putWallPacking');
			}

			Data = html.ProcessTote({SuperMsg: SuperMsg, Station: Resp.SavedDoc}, user, Ip);
			return SendResponse(res, Data, 'normalPacking');
		});

		return;
	}

	if(req.session.serial){
		let SerData = req.session.serial;
		delete req.session.serial;
		return ProcValidateSerialOrBarcode(req, res, Resp, SerData);
	}

	if(req.session.pwserial){
		let SerData = req.session.pwserial;
		delete req.session.pwserial;
		return ProcValidatePwSerialOrBarcode(req, res, Resp, SerData);
	}

	if(req.session.resettote){
		delete req.session.resettote;
		return ProcResetTote(req, res, Resp);
	}

	if(req.session.resetpwtote){
		delete req.session.resetpwtote;
		return ProcResetPwTote(req, res, Resp);
	}

	if(req.session.changecartonsize){
		let CartonSize = req.session.changecartonsize;
		delete req.session.changecartonsize;
		return ProcCartonSizeChange(req, res, Resp, CartonSize);
	}

	if(req.session.carton){
		let Carton = req.session.carton;
		delete req.session.carton;
		return ProcConfirmCartonscanned(req, res, Resp, Carton);
	}

	if(req.session.reprint){
		delete req.session.reprint;
		return ProcReprintDocuments(req, res, Resp);
	}

	if(req.session.document){
		let Document = req.session.document;
		delete req.session.document;
		return ProcConfirmInvoiceDocument(req, res, Resp, Document);
	}

	if(req.session.pwdocument){
		let Document = req.session.pwdocument;
		delete req.session.pwdocument;
		return ProcConfirmPwInvoiceDocument(req, res, Resp, Document);
	}

	if(req.session.transporter){
		let Transporter = req.session.transporter;
		delete req.session.transporter;
		return ProcConfirmTransporter(req, res, Resp, Transporter);
	}

	if(Resp.Station.ProcessOnPutWalls == null){
		return CheckToteForPutWalls(req, res, Resp, user, Ip, PL);
	}

	if(Resp.Station.ProcessOnPutWalls == true){
		return DoPutWallToteProcess(req, res, Resp, user, Ip);
	}

	if(Resp.Station.ProcUsingDesktopAutoBag == null){
		return DoCheckDesktopAutobag(req, res, Resp, user, Ip);
	}

	return DoNormalToteProcessNew(req, res, Resp, user, Ip);
} /* ProcessResponse */

/* =========== End Main Function ===================================*/

/* =========== Sub Function =======================================*/

function CompletePwLocation(Lane, callback){
	PwLocs.FindOne({'putAddress': Lane}, function(Resp){
		let PWLOC = Resp.pwLoc;
		if(!PWLOC){
			return callback();
		}

		PwLocs.UpdateLocationToBeConfirmed(PWLOC, true, function(ret){
			return callback();
		});
	});
} /* CompletePwLocation */

function CreateRecordForEachLine(mPL, lineNdx, callback){
	if(lineNdx >= mPL.orders[0].items.length){
		return callback();
	}

	let Order = mPL.orders[0];
	let Line = Order.items[lineNdx];

	PackUpdate.findOne({'PickListID': Order.pickListID, 'PickListLine': Line.pickListOrderLine, 'OrderId': Order.orderID}, function(err, uPL){
		if(err){
			lineNdx++;
			return CreateRecordForEachLine(mPL, lineNdx, callback);
		}

		if(uPL){
			lineNdx++;
			return CreateRecordForEachLine(mPL, lineNdx, callback);
		}

		let NewRec = new PackUpdate({
			PickListID: Order.pickListID,
			PickListLine: Line.pickListOrderLine,
			WcsId: Order.cartonID,
			OrderId: Order.orderID,
			Required: true,
			CreateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
		});

		NewRec.save(function(err, savedDoc){
			log.WriteToFile(null, 'Marked picklist: ' + Order.pickListID + ' PickListLine: ' + Line.pickListOrderLine + ' | Order: ' + Order.orderID + ' | Carton: ' + Order.cartonID + ' PackUpdate to true');
			lineNdx++;
			return CreateRecordForEachLine(mPL, lineNdx, callback);
		});
	});
} /* CreateRecordForEachLine */

function CreatePackUpdate(mPL){
	CreateRecordForEachLine(mPL, 0, function(){
		return;
	});
} /* CreatePackUpdate */

function DoCheckDesktopAutobag(req, res, Resp, user, Ip){
	CanProcessOnDesktopAutobag(Resp.Station, function(Result){
		let Station = Resp.Station;
		Station.ProcUsingDesktopAutoBag = Result;
		packStn.Update(Station, function(Result1){
			Resp.Station = Result1.SavedDoc;
			Data = html.ProcessTote(Resp, user, Ip);
			SendResponse(res, Data, 'normalPacking');
		});
	});
} /* DoCheckDesktopAutobag */

function DoPutWallToteProcess(req, res, Resp, user, Ip){
	PwLocs.CheckIfWaitingForConfirmation(Resp.Station.stationNumber, function(ConfirmData){
		return PutwallStationWhatNext(ConfirmData, req, res, Resp, user, Ip);
	});
} /* DoPutWallToteProcess */

function SendResponse(res, Data, FrameName){
	let frame = {}

	if(FrameName == 'normalPacking'){
		frame.Packing = html.GetPage('normalPacking');
	} else if(FrameName == 'putWallPacking'){
		frame.PutWallPacking = html.GetPage('putWallPacking');
	}

	let page = html.GetPage(null);
	let htmlpage = mustache.render(page, Data, frame);

	res.send(htmlpage);
} /* SendResponse */

function UpdateCartonNew(Station, Carton, CartonNo, NoOfCartons, callback){
	let CartonSize = Carton[0];
	let x = 0;
	let minLimit = Station.pickList.orders[Station.OrderIndex].minWeightLimit;
	let maxLimit = Station.pickList.orders[Station.OrderIndex].maxWeightLimit;
	let TotalWeight = Station.pickList.orders[Station.OrderIndex].picklistWeight;
	if(Station.CartonSizes){
		while(x < Station.CartonSizes.length){
			if(Station.CartonSizes[x].Size == CartonSize){
				if(Station.CartonSizes[x].weight){
					minLimit = Station.CartonSizes[x].minWeightLimit;
					maxLimit = Station.CartonSizes[x].maxWeightLimit;
					TotalWeight = Station.CartonSizes[x].weight + Station.pickList.orders[Station.OrderIndex].picklistWeight;
					break;
				}
			}
			x++;
		}
	}

	let QueryStartRange = parseInt(CartonSize) * 10000000;
	let QueryEndRange = (parseInt(CartonSize) + 1) * 10000000;
	let DCarton = parseInt(Carton);

	batchWeight.FindOne({"RangeStart" : {$gte: QueryStartRange},
						 "RangeEnd" : {$lt: QueryEndRange},
						 "RangeStart": {$lte: DCarton},
						 "RangeEnd" : {$gte: DCarton}}, function(BResp){

		let UseNewValue = true;
		let BWData = null;
		if(BResp.Err){
			UseNewValue = false;
		} else {
			if(BResp.batchWeight){
				BWData = BResp.batchWeight;
			} else {
				UseNewValue = false;
			}
		}

		if(UseNewValue == true){
			TotalWeight = BWData.batchWeight + Station.pickList.orders[Station.OrderIndex].picklistWeight;
		}

		Station.pickList.orders[Station.OrderIndex].cartonID = Carton;
		Station.pickList.orders[Station.OrderIndex].minWeightLimit = TotalWeight - minLimit;
		Station.pickList.orders[Station.OrderIndex].maxWeightLimit = TotalWeight + maxLimit;
		Station.pickList.orders[Station.OrderIndex].picklistWeight = TotalWeight;

		Station.pickList.orders[Station.OrderIndex].toteNumber = CartonNo;
		Station.pickList.orders[Station.OrderIndex].numberOfTotes = NoOfCartons;

		packStn.Update(Station, function(Resp){
			return callback(Resp);
		});
	});
} /* UpdateCartonNew */

function ProcGenAndPrintFromDesktopAutobag(req, res, Resp, Ip){
	let Station = Resp.Station;
	let NewResp = null;

	param.GetGeneratedCartonID(function(ParamData){
		if(ParamData.error) {
			NewResp = {Msg: ParamData.error, Station: Station};
			return ProcessResponse(req, res, NewResp);
		}

		param.ParamGetPwCartonDetails(Station.pickList, Station.OrderIndex, function(paramResp){
			if(paramResp.error){
				NewResp = {Msg: 'Failed to find carton number', Station: Station};
				return ProcessResponse(req, res, NewResp);
			}

			Station.pickList.orders[Station.OrderIndex].cartonSize = 1;
			Station.pickList.orders[Station.OrderIndex].picklistWeight += paramResp.EmptyCartonWeight;
			Station.pickList.orders[Station.OrderIndex].minWeightLimit = paramResp.MinTolerance;
			Station.pickList.orders[Station.OrderIndex].maxWeightLimit = paramResp.MaxTolerance;

			packStn.Update(Station, function(Resp){
				Station = Resp.SavedDoc;

				let body = {
					cartonID:   ParamData.CartonID,
					orderID:    Station.pickList.orders[Station.OrderIndex].orderID,
					picklistID: Station.pickList.orders[Station.OrderIndex].pickListID
				}

				rest.postJson(CartonUrl, body).on('complete', function(result){
					if(result instanceof Error){
						NewResp = {Msg: 'Failed to find carton number', Station: Station};
						return ProcessResponse(req, res, NewResp);
					}

					if(result.Err){
						NewResp = {Msg: 'Failed to find carton number', Station: Station};
						return ProcessResponse(req, res, NewResp);
					}

					let CartonNo = result.CartonNo;
					let NoOfCartons = result.NoOfCartons;

					UpdateCartonNew(Station, ParamData.CartonID, CartonNo, NoOfCartons, function(Resp){
						Station = Resp.SavedDoc;
						GetPrintData(Station, Station.OrderIndex, false, function(PrintData){
							prn.PrintDocuments(PrintData, null, Station.OrderIndex, function(Resp){
								if(Resp.Msg){
									log.WriteToFile(null, Resp.Msg);
									NewResp = {Msg: Resp.Msg, Station: Station};
									return ProcessResponse(req, res, NewResp);
								}

								Station.pickList.orders[Station.OrderIndex].printed = true;
								packStn.Update(Station, function(Resp){
									NewResp = {Msg: null, Station: Resp.SavedDoc};
									return ProcessResponse(req, res, NewResp);
								});
							});
						});
					});
				});
			});
		});
	});
} /* ProcGenAndPrintFromDesktopAutobag */

function CompleteEachOrderOnStation(Station, callback){
	let PL = Station.pickList;
	let Orders = PL.orders[0];

	if(!PL.orders || PL.orders.length <= 0){
		return callback({Station: Station});
	}

	pickList.FindOne({'orders.pickListID': PL.orders[0].pickListID}, function(Pklst){
		if(Pklst.Err){
			Station.pickList.orders.splice(0, 1);
			packStn.Update(Station, function(Resp){
				return CompleteEachOrderOnStation(Resp.SavedDoc, callback);
			});

			return;
		}

		let DbPl = Pklst.PL;
		if(!DbPl){
			Station.pickList.orders.splice(0, 1);
			packStn.Update(Station, function(Resp){
				return CompleteEachOrderOnStation(Resp.SavedDoc, callback);
			});

			return;
		}

		if(DbPl.orders.length == 1 && DbPl.Status == 'PACKED'){
			Station.pickList.orders.splice(0, 1);
			packStn.Update(Station, function(Resp){
				return CompleteEachOrderOnStation(Resp.SavedDoc, callback);
			});

			return;
		}

		let x = 0;
		while(x < DbPl.orders.length){
			if(DbPl.orders[x].pickListID == PL.orders[0].pickListID){
				break;
			}
			x++;
		}

		if(x >= DbPl.orders.length){
			Station.pickList.orders.splice(0, 1);
			packStn.Update(Station, function(Resp){
				return CompleteEachOrderOnStation(Resp.SavedDoc, callback);
			});

			return;
		}

		if(DbPl.orders.length == 1){
			Orders.packComplete = true;

			DbPl.orders = Orders;
			DbPl.FromTote = DbPl.toteID;
			DbPl.toteID = PL.orders[0].cartonID;
			DbPl.transporterID = PL.orders[0].transporterID;
			DbPl.toteInUse = false;
			DbPl.Status = 'PACKED';
			DbPl.PackUpdateRequired = false;
			DbPl.ShipUpdateRequired = false;
			DbPl.PackedDate = moment(new Date()).format('YYYY-MM-DD HH:mm');
			DbPl.containerLocation = null;
			DbPl.PickListType = null;

			serls.UpdateSerialsToHost(PL.orders[0], function(){});

			pickList.Update(DbPl, function(Resp){
				Station.pickList.orders.splice(0, 1);
				packStn.Update(Station, function(Resp){
					return CompleteEachOrderOnStation(Resp.SavedDoc, callback);
				});
			});

			return;
		}

		let OrderDate = DbPl.CreateDate;
		if(!OrderDate){
		   OrderDate =  moment(new Date()).format('YYYY-MM-DD HH:mm');
		}

		Orders.packComplete = true;
		let NewPLObj = {
			toteID: PL.orders[0].cartonID,
			transporterID: PL.orders[0].transporterID,
			destination: DbPl.destination,
			packType: DbPl.packType,
			toteInUse: false,
			toteInUseBy: DbPl.toteInUseBy,
			orders: Orders,
			Status: 'PACKED',
			PackUpdateRequired: false,
			ShipUpdateRequired: false,
			PackedDate: moment(new Date()).format('YYYY-MM-DD HH:mm'),
			containerLocation: null,
			PickListType: null,
			FromTote: DbPl.toteID,
			CreateDate: OrderDate
		}

		let NewPL = pickList.FormNewPickList(NewPLObj);
		serls.UpdateSerialsToHost(PL.orders[0], function(){});

		DbPl.orders.splice(x, 1);

		if(DbPl.orders.length <= 0){
			pickList.Delete({'toteID': DbPl.toteID}, function(){
				pickList.Update(NewPL, function(Resp){
					CreatePackUpdate(NewPL);

					Station.pickList.orders.splice(0, 1);
					packStn.Update(Station, function(Resp){
						return CompleteEachOrderOnStation(Resp.SavedDoc, callback);
					});
				});
			});
		} else {
			pickList.Update(DbPl, function(Resp){
				pickList.Update(NewPL, function(Resp){
					CreatePackUpdate(NewPL);

					Station.pickList.orders.splice(0, 1);
					packStn.Update(Station, function(Resp){
						return CompleteEachOrderOnStation(Resp.SavedDoc, callback);
					});
				});
			});
		}
	});
} /* CompleteEachOrderOnStation */

function CompleteEachPackedOrderOnStation(Station, x, callback){
	let PL = Station.pickList;
	let Orders = PL.orders[x];

	if(x >= PL.orders.length){
		return callback({Station: Station});
	}

	if(PL.orders[x].WmsPickListStatus == 'CANCELLED' ||
	   PL.orders[x].WmsPickListStatus == 'CANCELED' ||
	   PL.orders[x].WmsOrderStatus == 'CANCELLED' ||
	   PL.orders[x].WmsOrderStatus == 'CANCELED'){
		x++;
		return CompleteEachPackedOrderOnStation(Station, x, callback);
	}

	pickList.FindOne({'orders.pickListID': PL.orders[x].pickListID}, function(Pklst){
		if(Pklst.Err){
			Station.pickList.orders.splice(x, 1);
			packStn.Update(Station, function(Resp){
				return CompleteEachPackedOrderOnStation(Resp.SavedDoc, x, callback);
			});

			return;
		}

		let DbPl = Pklst.PL;
		if(!DbPl){
			Station.pickList.orders.splice(x, 1);
			packStn.Update(Station, function(Resp){
				return CompleteEachPackedOrderOnStation(Resp.SavedDoc, x, callback);
			});

			return;
		}

		if(DbPl.orders.length == 1 && DbPl.Status == 'PACKED'){
			Station.pickList.orders.splice(x, 1);
			packStn.Update(Station, function(Resp){
				return CompleteEachPackedOrderOnStation(Resp.SavedDoc, x, callback);
			});

			return;
		}

		let y = 0;
		while(y < DbPl.orders.length){
			if(DbPl.orders[y].pickListID == PL.orders[x].pickListID){
				break;
			}
			y++;
		}

		if(y >= DbPl.orders.length){
			Station.pickList.orders.splice(x, 1);
			packStn.Update(Station, function(Resp){
				return CompleteEachPackedOrderOnStation(Resp.SavedDoc, x, callback);
			});

			return;
		}

		if(DbPl.orders.length == 1){
			Orders.packComplete = true;

			DbPl.orders = Orders;
			DbPl.FromTote = DbPl.toteID;
			DbPl.toteID = PL.orders[x].cartonID;
			DbPl.transporterID = PL.orders[x].transporterID;
			DbPl.toteInUse = false;
			DbPl.Status = 'PACKED';
			DbPl.PackUpdateRequired = false;
			DbPl.ShipUpdateRequired = false;
			DbPl.PackedDate = moment(new Date()).format('YYYY-MM-DD HH:mm');
			DbPl.containerLocation = null;
			DbPl.PickListType = null;

			serls.UpdateSerialsToHost(PL.orders[x], function(){});

			pickList.Update(DbPl, function(Resp){
				Station.pickList.orders.splice(x, 1);
				packStn.Update(Station, function(Resp){
					return CompleteEachPackedOrderOnStation(Resp.SavedDoc, x, callback);
				});
			});

			return;
		}

		let OrderDate = DbPl.CreateDate;
		if(!OrderDate){
		   OrderDate =  moment(new Date()).format('YYYY-MM-DD HH:mm');
		}

		Orders.packComplete = true;
		let NewPLObj = {
			toteID: PL.orders[x].cartonID,
			transporterID: PL.orders[x].transporterID,
			destination: DbPl.destination,
			packType: DbPl.packType,
			toteInUse: false,
			toteInUseBy: DbPl.toteInUseBy,
			orders: Orders,
			Status: 'PACKED',
			PackUpdateRequired: false,
			ShipUpdateRequired: false,
			PackedDate: moment(new Date()).format('YYYY-MM-DD HH:mm'),
			containerLocation: null,
			PickListType: null,
			FromTote: DbPl.toteID,
			CreateDate: OrderDate
		}

		let NewPL = pickList.FormNewPickList(NewPLObj);
		serls.UpdateSerialsToHost(PL.orders[x], function(){});

		DbPl.orders.splice(y, 1);

		if(DbPl.orders.length <= 0){
			pickList.Delete({'toteID': DbPl.toteID}, function(){
				pickList.Update(NewPL, function(Resp){
					CreatePackUpdate(NewPL);

					Station.pickList.orders.splice(x, 1);
					packStn.Update(Station, function(Resp){
						return CompleteEachPackedOrderOnStation(Resp.SavedDoc, x, callback);
					});
				});
			});
		} else {
			pickList.Update(DbPl, function(Resp){
				pickList.Update(NewPL, function(Resp){
					CreatePackUpdate(NewPL);

					Station.pickList.orders.splice(x, 1);
					packStn.Update(Station, function(Resp){
						return CompleteEachPackedOrderOnStation(Resp.SavedDoc, x, callback);
					});
				});
			});
		}
	});
} /* CompleteEachPackedOrderOnStation */

function RemoveCancelledOrdersFromStation(Station, callback){
	let PL = Station.pickList;
	let Orders = PL.orders[0];

	if(!PL.orders || PL.orders.length <= 0){
		return callback({Station: Station});
	}

	pickList.FindOne({'orders.pickListID': PL.orders[0].pickListID}, function(Pklst){
		if(Pklst.Err){
			Station.pickList.orders.splice(0, 1);
			packStn.Update(Station, function(Resp){
				return RemoveCancelledOrdersFromStation(Resp.SavedDoc, callback);
			});

			return;
		}

		let DbPl = Pklst.PL;
		if(!DbPl){
			Station.pickList.orders.splice(0, 1);
			packStn.Update(Station, function(Resp){
				return RemoveCancelledOrdersFromStation(Resp.SavedDoc, callback);
			});

			return;
		}

		let x = 0;
		while(x < DbPl.orders.length){
			if(DbPl.orders[x].pickListID == PL.orders[0].pickListID){
				break;
			}
			x++;
		}

		if(x >= DbPl.orders.length){
			Station.pickList.orders.splice(0, 1);
			packStn.Update(Station, function(Resp){
				return RemoveCancelledOrdersFromStation(Resp.SavedDoc, callback);
			});

			return;
		}

		DbPl.orders[0].WmsOrderStatus = 'CANCELED';
		DbPl.orders[0].WmsPickListStatus = 'CANCELED';

		x = 0;
		let AllCancelled = true;
		while(x < DbPl.orders.length){
			if(DbPl.orders[x].WmsPickListStatus != 'CANCELED'){
				AllCancelled = false;
				break;
			}
			x++;
		}

		if(AllCancelled){
			//DbPl.Status = 'CANCELED';
			DbPl.PickListType = null;
			DbPl.toteInUseBy = null;
			DbPl.toteInUse = false;
		}

		pickList.Update(DbPl, function(Resp){
			Station.pickList.orders.splice(0, 1);
			packStn.Update(Station, function(Resp){
				return RemoveCancelledOrdersFromStation(Resp.SavedDoc, callback);
			});
		});
	});
} /* RemoveCancelledOrdersFromStation */

function ProcAllOrdersComplete(req, res, Resp){
	let Station = Resp.Station;
	let PL = Station.pickList;

	CalculateTrWeightSoFar(Station, function(StnResp){
		Resp.Station = StnResp.Station;

		Station = Resp.Station;
		PL = Station.pickList;

		CompleteEachOrderOnStation(Station, function(CResp){
			Resp.Station = CResp.Station;

			let MyMsg = null;
			if(Station.currentTransporter){
				MyMsg = 'Please release transporter ' + Station.currentTransporter + ' from station';
			}

			return ProcClearStation(req, res, Resp, MyMsg);
		});
	});
} /* ProcAllOrdersComplete */

function ProcCompletedWithCancellations(req, res, Resp, CancelledOrderCount){
	let Station = Resp.Station;
	let PL = Station.pickList;

	CalculateTrWeightSoFar(Station, function(StnResp){
		Resp.Station = StnResp.Station;

		Station = Resp.Station;
		PL = Station.pickList;

		CompleteEachPackedOrderOnStation(Station, 0, function(CResp){
			Resp.Station = CResp.Station;
			Station = Resp.Station;
			PL = Station.pickList;
			RemoveCancelledOrdersFromStation(Station, function(CResp){
				Resp.Station = CResp.Station;
				Station = Resp.Station;
				PL = Station.pickList;

				let MyMsg = null;
				if(CancelledOrderCount > 1){
					MyMsg = 'The remaining orders in tote ' + PL.toteID + ' are CANCELLED. Please take the tote to the supervisor';
				} else {
					MyMsg = 'The remaining order in tote ' + PL.toteID + ' is CANCELLED. Please take the tote to the supervisor';
				}

				return ProcClearStation(req, res, Resp, MyMsg);
			});
		});
	});
} /* ProcCompletedWithCancellations */

function ProcApplyCorrectCompleteOption(req, res, Resp){
	let Station = Resp.Station;
	let PL = Station.pickList;
	let x = 0;
	let CancelledOrderCount = 0;

	while(x < PL.orders.length){
		if(PL.orders[x].WmsPickListStatus == 'CANCELLED' ||
		   PL.orders[x].WmsPickListStatus == 'CANCELED' ||
		   PL.orders[x].WmsOrderStatus == 'CANCELLED' ||
		   PL.orders[x].WmsOrderStatus == 'CANCELED'){
			CancelledOrderCount++;
		}
		x++;
	}

	if(CancelledOrderCount <= 0){
		return ProcAllOrdersComplete(req, res, Resp);
	}

	return ProcCompletedWithCancellations(req, res, Resp, CancelledOrderCount);

} /* ProcApplyCorrectCompleteOption */

function UpdateStationOrderIndex(req, res, Resp, OrdNdx){
	let Station = Resp.Station;
	let PL = Station.pickList;
	let NewResp = null;

	Station.OrderIndex = OrdNdx;
	packStn.Update(Station, function(Resp){
		NewResp = {Station: Resp.SavedDoc};
		return ProcessResponse(req, res, NewResp);
	});
} /* UpdateStationOrderIndex */

function CalcOrderIndex(PL){
	let x = 0;
	while(x < PL.orders.length){
		if(!PL.orders[x].packComplete &&
		   PL.orders[x].WmsPickListStatus != 'CANCELLED' &&
		   PL.orders[x].WmsPickListStatus != 'CANCELED' &&
		   PL.orders[x].WmsOrderStatus != 'CANCELLED' &&
		   PL.orders[x].WmsOrderStatus != 'CANCELED'){
			return x;
		}
		x += 1;
	}

	return -1;
} /* CalcOrderIndex */

function SpliceOrderOffStn(req, res, Resp){
	let Station = Resp.Station;
	let PL = Station.pickList;
	let OrdNdx = Station.OrderIndex;
	let NewResp = null;

	Station.pickList.orders.splice(OrdNdx, 1);
	Station.CurrentOrder += 1;
	packStn.Update(Station, function(Resp){
		if(Resp.Err){}

		NewResp = {Station: Resp.SavedDoc};
		return ProcessResponse(req, res, NewResp);
	});
} /* SpliceOrderOffStn */

function UpdateSingleOrderInPicklist(DbPl, req, res, Resp){
	let Station = Resp.Station;
	let PL = Station.pickList;
	let OrdNdx = Station.OrderIndex;
	let NewResp = null;

	PL.orders[OrdNdx].packComplete = true;
	DbPl.orders = PL.orders[OrdNdx];
	DbPl.toteInUse = false;
	DbPl.toteID = PL.orders[OrdNdx].cartonID;
	DbPl.transporterID = PL.orders[OrdNdx].transporterID;
	DbPl.PickListType = null;
	DbPl.FromTote = PL.toteID;
	DbPl.Packing = false;
	DbPl.Status = 'PACKED';
	DbPl.PackedDate = moment(new Date()).format('YYYY-MM-DD HH:mm');

	pickList.Update(DbPl, function(pResp){
		if(pResp.Err){
			NewResp = {Msg: 'Failed To Complete And Remove Order From Station', Station: Station};
			return ProcessResponse(req, res, NewResp);
		}

		CreatePackUpdate(DbPl);
		serls.UpdateSerialsToHost(PL.orders[OrdNdx], function(){});

		if(PL.orders.length == 1){
			return ProcClearStation(req, res, Resp, null);
		}

		return SpliceOrderOffStn(req, res, Resp);
	});
} /* UpdateSingleOrderInPicklist */

function FindAndUpdateByPicklistId(DbPl, req, res, Resp){
	let Station = Resp.Station;
	let PL = Station.pickList;
	let OrdNdx = Station.OrderIndex;
	let NewResp = null;

} /* FindAndUpdateByPicklistId */

function ProcReleaseOrderFromStn(req, res, Resp){
	let Station = Resp.Station;
	let PL = Station.pickList;
	let OrdNdx = Station.OrderIndex;
	let NewResp = null;

	pickList.FindOne({'orders.pickListID': PL.orders[OrdNdx].pickListID}, function(Pklst){
		if(Pklst.Err){
			NewResp = {Msg: 'Failed to release order from station', Station:Station};
			return ProcessResponse(req, res, NewResp);
		}

		let DbPl = Pklst.PL;
		if(!DbPl){
			NewResp = {Msg: 'Failed to release order from station', Station:Station};
			return ProcessResponse(req, res, NewResp);
		}

		if(DbPl.orders.length == 1){
			return UpdateSingleOrderInPicklist(DbPl, req, res, Resp);
		}

		return FindAndUpdateByPicklistId(DbPl, req, res, Resp);
	});
} /* ProcReleaseOrderFromStn */

function ProcSpliceAndReleaseOrderFromStn(req, res, Resp){
	let Station = Resp.Station;
	let PL = Station.pickList;
	let OrdNdx = Station.OrderIndex;
	let NewResp = null;

	pickList.FindOne({'orders.pickListID': PL.orders[OrdNdx].pickListID}, function(Pklst){
		if(Pklst.Err){
			NewResp = {Msg: 'Failed to release order from station', Station:Station};
			return ProcessResponse(req, res, NewResp);
		}

		let DbPl = Pklst.PL;
		if(!DbPl){
			NewResp = {Msg: 'Failed to release order from station', Station:Station};
			return ProcessResponse(req, res, NewResp);
		}

		let x = 0;
		while(x < DbPl.orders.length){
			if(DbPl.orders[x].pickListID == PL.orders[OrdNdx].pickListID){
				break;
			}
			x++;
		}

		if(x >= DbPl.orders.length){
			NewResp = {Msg: 'Failed to release order from station', Station:Station};
			return ProcessResponse(req, res, NewResp);
		}

		let OrderDate = DbPl.CreateDate;
		if(!OrderDate){
		   OrderDate =  moment(new Date()).format('YYYY-MM-DD HH:mm');
		}

		PL.orders[OrdNdx].packComplete = true;

		let NewPLObj = {
			toteID: PL.orders[OrdNdx].cartonID,
			transporterID: PL.orders[OrdNdx].transporterID,
			destination: DbPl.destination,
			packType: DbPl.packType,
			toteInUse: false,
			toteInUseBy: DbPl.toteInUseBy,
			orders: PL.orders[OrdNdx],
			Status: 'PACKED',
			PackUpdateRequired: false,
			ShipUpdateRequired: false,
			PackedDate: moment(new Date()).format('YYYY-MM-DD HH:mm'),
			containerLocation: null,
			PickListType: null,
			FromTote: PL.toteID,
			CreateDate: OrderDate
		}

		let NewPL = pickList.FormNewPickList(NewPLObj);
		serls.UpdateSerialsToHost(PL.orders[OrdNdx], function(){});

		DbPl.orders.splice(x, 1);
		Station.pickList.orders.splice(OrdNdx, 1);
		Station.CurrentOrder += 1;

		if(DbPl.orders.length <= 0){
			pickList.Delete({'toteID': PL.toteID}, function(){
				pickList.Update(NewPL, function(Resp){
					CreatePackUpdate(NewPL);

					packStn.Update(Station, function(Resp){
						NewResp = {Station: Resp.SavedDoc};
						return ProcessResponse(req, res, NewResp);
					});
				});
			});
		} else {
			pickList.Update(DbPl, function(Resp){
				pickList.Update(NewPL, function(Resp){
					CreatePackUpdate(NewPL);

					packStn.Update(Station, function(Resp){
						NewResp = {Station: Resp.SavedDoc};
						return ProcessResponse(req, res, NewResp);
					});
				});
			});
		}
	});
} /* ProcSpliceAndReleaseOrderFromStn */

function CalculateTrWeightSoFar(Station, callback){
	if(!Station.currentTransporter){
		return callback({Station: Station});
	}

	param.GetTransporterWeightDetails(Station, function(Resp){
		if(Resp.Msg){
			return callback({Station: Station});
		}

		let x = 0;
		while(x < Station.pickList.orders.length){
			if(Station.pickList.orders[x].packComplete == true && Station.pickList.orders[x].useTransporter == true && Station.pickList.orders[x].transporterID == Station.currentTransporter){
				Station.pickList.orders[x].transporterWeight = Resp.TotalWeight;
				Station.pickList.orders[x].minWeightLimit = Resp.MinTolerance;
				Station.pickList.orders[x].maxWeightLimit = Resp.MaxTolerance;
			}
			x++;
		}

		packStn.Update(Station, function(Resp){
			if(Resp.Err){
				return callback({Station: Station});
			}

			return callback({Station: Resp.SavedDoc});
		});
	});
} /* CalculateTrWeightSoFar */

function FinaliseCompletedOrder(req, res, Resp){
	let Station = Resp.Station;
	let NewResp = null;
	let PL = Station.pickList;
	let OrdNdx = Station.OrderIndex;

	if(PL.orders.length == 1){
		Station.pickList.orders[OrdNdx].packComplete = true;
		CalculateTrWeightSoFar(Station, function(StnResp){
			Resp.Station = StnResp.Station;
			return ProcReleaseOrderFromStn(req, res, Resp);
		});

		return;
	}

	if(!PL.orders[OrdNdx].useTransporter){
		return ProcSpliceAndReleaseOrderFromStn(req, res, Resp);
	}

	Station.pickList.orders[OrdNdx].packComplete = true;
	Station.CurrentOrder += 1;
	Station.OrderIndex += 1;
	packStn.Update(Station, function(Resp){
		NewResp = {Station: Resp.SavedDoc};
		return ProcessResponse(req, res, NewResp);
	});
} /* FinaliseCompletedOrder */

function DoNormalToteProcessNew(req, res, Resp, user, Ip){
	let Station = Resp.Station;
	let PL = Station.pickList;
	let OrdNdx = Station.OrderIndex;
	let NewResp = null;
	let Data = null;

	if(PL.orders[OrdNdx].orderVerified){
		let Finalise = true;
		if(PL.orders[OrdNdx].useTransporter && !PL.orders[OrdNdx].transporterID){
			Finalise = false;
		}

		if(Finalise){
			return FinaliseCompletedOrder(req, res, Resp);
		}
	}

	if(PL.orders[OrdNdx].packed && Station.ProcUsingDesktopAutoBag && !PL.orders[OrdNdx].cartonID && !Resp.Msg){
		return ProcGenAndPrintFromDesktopAutobag(req, res, Resp, Ip);
	}

	if(PL.orders[OrdNdx].printed){
		if(PL.orders[OrdNdx].toteNumber != PL.orders[OrdNdx].numberOfTotes){
			if(!PL.orders[OrdNdx].useTransporter){
				Station.pickList.orders[OrdNdx].orderVerified = true;
				packStn.Update(Station, function(Resp){
					NewResp = {Station: Resp.SavedDoc};
					return ProcessResponse(req, res, NewResp);
				});

				return;
			}

			if(PL.orders[OrdNdx].useTransporter && PL.orders[OrdNdx].transporterID){
				Station.pickList.orders[OrdNdx].orderVerified = true;
				packStn.Update(Station, function(Resp){
					NewResp = {Station: Resp.SavedDoc};
					return ProcessResponse(req, res, NewResp);
				});

				return;
			}
		}
	}

	Data = html.ProcessTote(Resp, user, Ip);
	SendResponse(res, Data, 'normalPacking');
} /* DoNormalToteProcessNew */

function ToteValidForPutWalls(doc, Station, callback){
	if(doc.packType != 'PUT'){
		/* IF the tote is a priority tote then don't process as a putwall tote */
		if(doc.priorityLevel && doc.priorityLevel < 10){
			return callback(false);
		}

		param.GetAutobaggerMaxVolume(function(Param){
			if(!Param.volumeCapacity){
				return callback(false);
			}

			pickList.CheckIfOrdersInToteAreValidforPutWall(doc, 0, Param.volumeCapacity, function(Result){
				if(!Result){
					return callback(false);
				}

				PwLocs.FindOne({'packStation': Station.stationNumber}, function(Resp){
					if(Resp.Err || !Resp.pwLoc){
						return callback(false);
					}

					return callback(true);
				});
			});
		});
	} else {
		PwLocs.FindOne({'packStation': Station.stationNumber}, function(Resp){
			if(Resp.Err || !Resp.pwLoc){
				return callback(false);
			}

			/* IF the tote is a priority tote then don't process as a putwall tote */
			if(doc.priorityLevel && doc.priorityLevel < 10){
				return callback(false);
			}

			return callback(true);
		});
	}
} /* ToteValidForPutWalls */

function ProcPwPackedOrder(req, res, Resp, index){
	let Station = Resp.Station;
	let PL = Station.pickList;
	let NewResp = null;

	param.ParamGetPwCartonDetails(PL, index, function(CartonDetails){
		if(CartonDetails.error){
			NewResp = {Msg: CartonDetails.error, Station: Station};
			return ProcessResponse(req, res, NewResp);
		}

		PwLocs.CalculatePwCartonWeight(PL, index, CartonDetails.EmptyCartonWeight, function(CartonWeight){
			PL.orders[index].picklistWeight = CartonWeight;
			PL.orders[index].transporterWeight = null;
			PL.orders[index].minWeightLimit = (CartonWeight - CartonDetails.MinTolerance);
			PL.orders[index].maxWeightLimit = (CartonWeight + CartonDetails.MaxTolerance);
			PL.orders[index].toteNumber = 1;

			let Msg = "Putwall: Order " + PL.orders[index].orderID + " weight " + CartonWeight + " | min weight " + (CartonWeight - CartonDetails.MinTolerance) + " | max weight " + (CartonWeight + CartonDetails.MaxTolerance);
			log.WriteToFile(null, Msg);

			Station.pickList = PL;
			GetPrintData(Station, index, true, function(PrintData){
				prn.PrintDocuments(PrintData, true, index, function(Resp){
					Station.pickList.orders[index].printed = true;
					packStn.Update(Station, function(Resp){
						NewResp = {Station: Station};
						return ProcessResponse(req, res, NewResp);
					});
				});
			});
		});
	});
} /* ProcPwPackedOrder */

function SpliceAndCompletePwOrder(req, res, Resp, index, user, Lane){
	let Station = Resp.Station;
	let PL = Station.pickList;
	let picklistID = PL.orders[index].pickListID;
	let Orders = PL.orders[index];
	let NewResp = null;

	pickList.FindOne({'orders.pickListID': picklistID}, function(Resp){
		if(Resp.Err){
			return callback({Msg: 'Failed to find pickList complete packing', Station: Station});
		}

		let DbPl = Resp.PL;

		if(DbPl.orders.length == 1){
			DbPl.orders = Orders;
			DbPl.FromTote = DbPl.toteID;
			DbPl.toteID = 'PW_LOC_' + Lane;
			DbPl.transporterID = null;
			DbPl.toteInUse = false;
			DbPl.Status = 'STORED';
			DbPl.PackUpdateRequired = false;
			DbPl.ShipUpdateRequired = false;
			DbPl.PackedDate = null;
			DbPl.containerLocation = null;
			DbPl.PickListType = null;

			Station.pickList.orders.splice(index, 1);
			pickList.Update(DbPl, function(Resp){
				packStn.Update(Station, function(Result){
					CompletePwLocation(Lane, function(){
						NewResp = {Station: Result.SavedDoc};
						return ProcessResponse(req, res, NewResp);
					});
				});
			});
			return;
		}

		let x = 0;
		while(x < DbPl.orders.length){
			if(DbPl.orders[x].pickListID == picklistID){
				break;
			}
			x++;
		}

		if(x < DbPl.orders.length){
			let now = new Date();
			let OrderDate = DbPl.CreateDate;

			if(!OrderDate){
				OrderDate =  moment(now).format('YYYY-MM-DD HH:mm');
			}

			let NewPLObj = {
					toteID: 'PW_LOC_' + Lane,
					transporterID: null,
					destination: DbPl.destination,
					packType: DbPl.packType,
					toteInUse: false,
					toteInUseBy: DbPl.toteInUseBy,
					orders: Orders,
					Status: 'STORED',
					PackUpdateRequired: false,
					ShipUpdateRequired: false,
					PackedDate: null,
					containerLocation: null,
					PickListType: null,
					FromTote: DbPl.toteID,
					CreateDate: OrderDate
			}

			let NewPL = pickList.FormNewPickList(NewPLObj);

			DbPl.orders.splice(x, 1);
			Station.pickList.orders.splice(index, 1);
			pickList.Update(DbPl, function(Resp){
				pickList.Update(NewPL, function(Resp){
					packStn.Update(Station, function(Result){
						CompletePwLocation(Lane, function(){
							NewResp = {Station: Result.SavedDoc};
							return ProcessResponse(req, res, NewResp);
						});
					});
				});
			});

			return;
		}

		Station.pickList.orders.splice(index, 1);
		packStn.Update(Station, function(Result){
			CompletePwLocation(Lane, function(){
				NewResp = {Station: Result.SavedDoc};
				return ProcessResponse(req, res, NewResp);
			});
		});
	});
} /* SpliceAndCompletePwOrder */

function FinalisePwOrder(req, res, Resp, user, index){
	let Station = Resp.Station;
	let PL = Station.pickList;

	PwLocs.FindPutWallLocationUsingOrderID(PL.orders[index].orderID, Station.stationNumber, function(Lane){
		if(!Lane){
			return callback({Msg: 'Failed to find Lane For Putwall order', Station: Station});
		}

		return SpliceAndCompletePwOrder(req, res, Resp, index, user, Lane);
	});
} /* FinalisePwOrder */

function DoPwGetNext(req, res, Resp, user, Ip){
	let Msg = null;
	if(Resp.Msg){
		Msg = Resp.Msg;
	}

	if(!Resp || !Resp.Station){
		Data = html.ShowNoStation(Resp, user, Ip);
		return SendResponse(res, Data, 'normalPacking');
	}

	if(!Resp.Station.pickList){
		Data = html.ShowScanTote(Resp, user, Ip);
		return SendResponse(res, Data, 'normalPacking');
	}


	let Station = Resp.Station;
	let PL = Station.pickList;
	let x = 0;
	let index = -1;
	while(x < PL.orders.length){
		if(PL.orders[x].orderVerified){
			index = x;
			break;
		}
		x++;
	}

	if(index >= 0){
		return FinalisePwOrder(req, res, Resp, user, index);
	}

	x = 0;
	index = -1;
	while(x < PL.orders.length){
		if(PL.orders[x].printed && !PL.orders[x].orderVerified){
			index = x;
			break;
		}
		x++;
	}

	if(index >= 0){
		Data = html.ProcessPwTote({Next: 'DOC', Station: Station, Msg: Msg}, user, Ip);
		return SendResponse(res, Data, 'putWallPacking');
	}

	x = 0;
	index = -1;
	while(x < PL.orders.length){
		if(PL.orders[x].packed && !PL.orders[x].printed){
			index = x;
			break;
		}
		x++;
	}

	if(index >= 0){
		return ProcPwPackedOrder(req, res, Resp, index);
	}

	x = 0;
	index = -1;
	while(x < PL.orders.length){
		if(!PL.orders[x].packed &&
		   PL.orders[x].WmsPickListStatus != 'CANCELLED' &&
		   PL.orders[x].WmsPickListStatus != 'CANCELED' &&
		   PL.orders[x].WmsOrderStatus != 'CANCELLED' &&
		   PL.orders[x].WmsOrderStatus != 'CANCELED'){
			index = x;
			break;
		}
		x++;
	}

	if(index < 0){
		x = 0;
		let CancelledOrderCount = 0;
		while(x < PL.orders.length){
			if(PL.orders[x].WmsPickListStatus == 'CANCELLED' ||
			   PL.orders[x].WmsPickListStatus == 'CANCELED' ||
			   PL.orders[x].WmsOrderStatus == 'CANCELLED' ||
			   PL.orders[x].WmsOrderStatus == 'CANCELED'){
				CancelledOrderCount++;
			}
			x++;
		}

		return ProcCompletedWithCancellations(req, res, Resp, CancelledOrderCount);
	}

	Data = html.ProcessPwTote({Next: 'BARCODE/SERIAL', Station: Station, Msg: Msg}, user, Ip);
	return SendResponse(res, Data, 'putWallPacking');

} /* DoPwGetNext */

function DoLightLoc(req, res, Resp, user, Ip, ConfirmData){
	PwLocs.LightupLoc(ConfirmData.Location, '_ON_GREEN', Resp.Station.stationNumber, function(Response){
		if(Response.error){
			Resp.Msg = 'Error while trying to switch on light for location ' + ConfirmData.Location;
		}

		Data = html.ProcessPwTote(Resp, user, Ip);
		SendResponse(res, Data, 'putWallPacking');
	});
} /* DoLightLoc */

function PutwallStationWhatNext(ConfirmData, req, res, Resp, user, Ip){
	if(!ConfirmData){
		return DoPwGetNext(req, res, Resp, user, Ip);
	}

	Resp.ConfirmData = ConfirmData;
	if(!req.query.wait){
		return DoLightLoc(req, res, Resp, user, Ip, ConfirmData);
	}

	Data = html.ProcessPwTote(Resp, user, Ip);
	return SendResponse(res, Data, 'putWallPacking');
} /* PutwallStationWhatNext */

function CanProcessOnDesktopAutobag(Station, callback){
	let PL = Station.pickList;

	if(!PL){
		return callback(false);
	}

	if(!Station.autobagPrinter){
		return callback(false);
	}

	/* IF the tote is a priority tote then don't process as a DesktopAutobag tote
	if(PL.priorityLevel && PL.priorityLevel < 10){
		return callback(false);
	}*/

	param.GetAutobaggerMaxVolume(function(Param){
		if(!Param.volumeCapacity){
			return callback(false);
		}

		pickList.CheckIfOrderValidForDeskAutobag(PL, Station.OrderIndex, Param.volumeCapacity, function(Result){
			return callback(Result);
		});
	});
} /* CanProcessOnDesktopAutobag */

function CheckToteForPutWalls(req, res, Resp, user, Ip, PL){
	let Stack = {};

	Stack.Station = function(callback){
		ToteValidForPutWalls(PL, Resp.Station, function(Valid){
			packStn.UpdateStationType(Resp.Station, Valid, function(StnData){
				return callback(null, StnData.Station);
			});
		});
	}

	Stack.ConfirmData = function(callback){
		PwLocs.CheckIfWaitingForConfirmation(Resp.Station.stationNumber, function(ConfirmData){
			return callback(null, ConfirmData);
		});
	}

	Stack.ProcOnDesktopAuto = function(callback){
		CanProcessOnDesktopAutobag(Resp.Station, function(Result){
			return callback(null, Result);
		});
	}

	async.parallel(Stack, function(err, Result){
		if(err){
			log.WriteToFile(null, err);
			Result.Msg = 'Internal Server Error';
		}

		if(Result.Station.ProcessOnPutWalls){
			return PutwallStationWhatNext(Result.ConfirmData, req, res, Resp, user, Ip);
		}

		let Station = Result.Station;
		Station.ProcUsingDesktopAutoBag = Result.ProcOnDesktopAuto;
		packStn.Update(Station, function(Result1){
			Resp.Station = Result1.SavedDoc;

			Data = html.ProcessTote(Resp, user, Ip);
			return SendResponse(res, Data, 'normalPacking');
		});
	});
} /* CheckToteForPutWalls */

function CheckItemQtys(task, callback){
	let items = task.items;
	let x = 0;
	while(x < items.length){
		if(items[x].qty != items[x].pickQty){
			break;
		}
		x++;
	}

	let Msg = null;
	if(x < items.length){
		Msg = 'Not all items are picked';
	}

	return callback(null, {Msg: Msg});
} /* CheckItemQtys */

function ValidateScannedTote(Tote, StnNumber, callback){
	pickList.FindOne({'toteID': Tote}, function(Resp){
		if(Resp.Err){
			if(Resp.Err == 'Not Found'){
				return callback({Msg: 'Scanned tote is empty'});
			}

			return callback({Msg: 'Failed to find tote on the system, call administrator'});
		}

		let PL = Resp.PL;
		if(PL.Status == 'CANCELED'){
			return callback({Msg: 'Tote Contains CANCELLED Order(s). Please Take It To A Supervisor'});
		}

		if(PL.Status != 'PICKED'){
			return callback({Msg: 'Tote Not PICKED'});
		}

		if(PL.toteInUse){
			if(parseInt(PL.toteInUseBy) == StnNumber){
				return callback({PL: PL});
			}

			return callback({Msg: 'Tote is inprogress at pack station ' + PL.toteInUseBy});
		}

		packStn.CheckIfToteInprogressAtAnyStation(Tote, function(Resp){
			if(Resp.Msg){
				return callback(Resp);
			}

			var qCheckOrdPLs  = async.queue(CheckItemQtys, PL.orders.length);

			qCheckOrdPLs.push(PL.orders, function (err, data) {
				if(!Resp.Msg){
					if(data.Msg){
						Resp.Msg = data.Msg;
					}
				}
			});

			qCheckOrdPLs.drain = function(){
				if(Resp.Msg){
					return callback(Resp);
				}

				return callback({PL: PL});
			}
		});
	});
} /* ValidateScannedTote */

function CheckOrdUniq(task, callback){
	let pickListID = task.pickListID;

	pickList.Find({'orders.pickListID': pickListID}, function(Resp){
		if(Resp.Err || !Resp.pickListArr || Resp.pickListArr.length <= 0){
			return callback('error', {unique: false, Msg: 'Failed to check picklist for uniqueness: ' + Resp.Err});
		}

		let PickLists = Resp.pickListArr;
		let Total = 0;
		let x = 0;
		while(x < PickLists.length){
			let y = 0;
			while(y < PickLists[x].orders.length){
				if(PickLists[x].orders[y].pickListID == pickListID){
					Total++;
				}
				y++;
			}
			x++;
		}

		if(Total > 1){
			return callback(null,{unique: false, Msg: 'Order ' + task.orderID + ' in tote is a duplicate'});
		}

		return callback(null, {unique: true, Msg: null});
	});
} /* CheckOrdUniq */

function CheckIfPickListIsUnique(doc, index, callback){
	var qCheckOrdUniq  = async.queue(CheckOrdUniq, 1);
	let Msg = null;

	qCheckOrdUniq.push(doc.orders, function (err, data) {
		if(!Msg){
			if(!data.unique){
				Msg = data.Msg;
			}
		}
	});

	qCheckOrdUniq.drain = function(){
		if(Msg){
			return callback({unique: false, Msg: Msg});
		}

		return callback({unique: true, Msg: null});
	}
} /* CheckIfPickListIsUnique */

function CheckEachOrderOnStation(PL, x, StationNumber, callback){
	if(x >= PL.orders.length){
		return callback({PL: PL});
	}

	CanReq.findOne({'OrderId': PL.orders[x].orderID}, function(err, Req){
		if(err){
			WriteToFile(err);
		}

		if(Req){
			PL.orders[x].WmsOrderStatus = 'CANCELED';
			PL.orders[x].WmsPickListStatus = 'CANCELED';
			PL.orders[x].CancelDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			PL.orders[x].CancelInstruction = 'Fetch Tote From Pack Station: ' + StationNumber;

			PL.save(function(err, sd){
				x++;
				return CheckEachOrderOnStation(PL, x, StationNumber, callback);
			});

			return;
		}

		x++;
		return CheckEachOrderOnStation(PL, x, StationNumber, callback);
	});
} /* CheckEachOrderOnStation */

function CheckForAnyCancellationRequests(Station, callback){
	let PL = Station.pickList;

	CheckEachOrderOnStation(PL, 0, Station.stationNumber, function(Resp){
		Station.pickList = Resp.PL;
		return callback({Station: Station});
	});
} /* CheckForAnyCancellationRequests */

function AddPickListToStation(Station, PL, callback){
	Station.pickList = PL;
	Station.NumOrders = PL.orders.length;
	Station.CurrentOrder = 1;
	Station.OrderIndex = 0;
	Station.ProcessOnPutWalls = null;
	Station.currentTransporter = null;
	Station.ProcUsingDesktopAutoBag = null;

	let Stack = {};

	Stack.ToteType = function(callback){
		pickList.GetToteType(PL.orders.length, PL.orders.orderID, function(toteTypeDetailName){
			return callback(null, toteTypeDetailName);
		});
	}

	Stack.StnData = function(callback){
		param.CalcCartonSizesForOrdOnStation(Station, function(Resp){
			return callback(null, Resp);
		});
	}

	Stack.MarkInUse = function(callback){
		pickList.MarkPLInUse(PL, Station.stationNumber, function(){
			return callback(null, null);
		});
	}

	async.parallel(Stack, function(err, Result){
		if(err){
			log.WriteToFile(null, err);
			Result.Msg = 'Internal Server Error';
			return callback(Result);
		}

		Station = Result.StnData.Station;
		Station.CartonSizes = Result.StnData.CartonSizeArr;
		Station.ToteType = Result.ToteType;

		CheckForAnyCancellationRequests(Station, function(Resp){
			Station = Resp.Station;
			packStn.Update(Station, function(Resp){
				return callback(Resp);
			});
		});
	});
} /* AddPickListToStation */

function ProcFindScannedTote(req, res, Resp, Tote){
	let NewResp = null;
	let Station = Resp.Station;
	let user = req.session.username;

	log.WriteToFile(null, 'User: ' + user + ' | Container ' + Tote + ' scanned onto station ' + Station.stationNumber);

	if((Tote[0] != '0' && Tote[0] != '1') || Tote.length != 7){
		log.WriteToFile(null, 'User: ' + user + ' | Incorrect value ' + Tote + ' scanned as toteID onto station ' + Station.stationNumber);
		NewResp = {Msg: 'Incorrect value scanned as toteID', Station: Station};
		return ProcessResponse(req, res, NewResp);
	}

	ValidateScannedTote(Tote, Station.stationNumber, function(Resp){
		if(Resp.Msg){
			log.WriteToFile(null, 'User: ' + user + ' | Err at station ' + Station.stationNumber + ' while validating tote. Err: ' + Resp.Msg);
			NewResp = {Msg: Resp.Msg, Station: Station};
			return ProcessResponse(req, res, NewResp);
		}

		let PL = Resp.PL;
		CheckIfPickListIsUnique(PL, 0, function(Resp){
			if(Resp.Err){
				log.WriteToFile(null,'User: ' + user + ' | Err at station ' + Station.stationNumber + ' while checking tote ' + Tote + ' for uniqueness | Err: ' + Resp.Err);
				NewResp = {Msg: 'Internal Server Error', Station: Station};
				return ProcessResponse(req, res, NewResp);
			}

			if(Resp.Msg){
				log.WriteToFile(null, 'User: ' + user + ' | Err at station ' + Station.stationNumber + ' while validating tote. Err: ' + Resp.Msg);
				NewResp = {Msg: Resp.Msg, Station: Station};
				return ProcessResponse(req, res, NewResp);
			}

			AddPickListToStation(Station, PL, function(Resp){
				if(Resp.Err){
					log.WriteToFile(null,'User: ' + user + ' | Err at station ' + Station.stationNumber + ' while adding tote to station. Err: ' + Resp.Err);
					NewResp = {Msg: 'Internal Server Error', Station: Station};
					return ProcessResponse(req, res, NewResp);
				}

				if(!Resp.SavedDoc){
					log.WriteToFile(null,'User: ' + user + ' | PickList did not save on pack station ' + Station.stationNumber);
					NewResp = {Msg: 'Internal Server Error', Station: Station};
					return ProcessResponse(req, res, NewResp);
				}

				log.WriteToFile(null, 'User: ' + user + ' | Tote ' + Tote + ' valid at station ' + Station.stationNumber);
				NewResp = {Station: Resp.SavedDoc};
				return ProcessResponse(req, res, NewResp);
			});
		});
	});
} /* ProcFindScannedTote */

function UpdateStationPL(Station, itemIndex, SerialRec, user, callback){
	let SerialWeight = null;
	let OrderIndex = Station.OrderIndex;
    let SerialCount = null;
    let SerialFound = false;

	if(SerialRec){
	  let serial = SerialRec.serial;
	  SerialCount = SerialRec.serialCount;

	  let cnt = 0;
	  while(cnt < Station.pickList.orders[OrderIndex].items[itemIndex].serials.length){
		  if(Station.pickList.orders[OrderIndex].items[itemIndex].serials[cnt] == serial){
			  SerialFound = true;
			  break;
		  }
		  cnt++;
	  }

	  if(!SerialCount){
	  	SerialCount = 1;
	  }

	  if(SerialFound == false){
		  Station.pickList.orders[OrderIndex].items[itemIndex].serials.push(serial);
		  SerialWeight = SerialRec.serialWeight * SerialCount;
	  }

    } else {
		 SerialCount = 1;
	}

	if(SerialWeight == null){
		SerialWeight = Station.pickList.orders[OrderIndex].items[itemIndex].eachWeight;
	}

	if(SerialFound == false){
		Station.pickList.orders[OrderIndex].items[itemIndex].qtyPacked += SerialCount;
		Station.pickList.orders[OrderIndex].picklistWeight += SerialWeight;
	}

	if(Station.pickList.orders[OrderIndex].items[itemIndex].qtyPacked == Station.pickList.orders[OrderIndex].items[itemIndex].pickQty){
		Station.pickList.orders[OrderIndex].items[itemIndex].packed = true;
		Station.pickList.orders[OrderIndex].items[itemIndex].packedBy = user;
	}

	if(packStn.CheckIfOrderFullyPacked(Station.pickList.orders[OrderIndex]) == true){
		Station.pickList.orders[OrderIndex].packed = true;
	}

	packStn.Update(Station, function(Resp){
		if(SerialRec){
			serls.GetSerial(SerialRec.serial, function(Resp){
				if(!Resp.Ser){
					log.WriteToFile(null,'Error while updating serial number ' + UpdateResp.serial + ' in serial table');
				} else {
					let Order = Station.pickList.orders[OrderIndex];
					let Ser = Resp.Ser;
					Ser.dispatched = true;
					Ser.salesOrderID = Order.orderID;
					Ser.salesOrderLineID = Order.items[itemIndex].orderLine;
					Ser.cartonID = Order.pickListID;
					Ser.serialStatus = 'DISPATCHED';
					Ser.userID = user;

					serls.Update(Ser, function(serResp){
						if(serResp.Err){
							log.WriteToFile(null,'Error while updating serial number ' + Ser.serial + ' in serial table');
						} else {
							log.WriteToFile(null,'Successfully dispatched serial ' + Ser.serial + ' in serial table');
						}
					});
				}
			});
		}
		return callback({Station: Resp.SavedDoc});
	});
} /* UpdateStationPL */

function UpdatePwStationPL(Station, itemIndex, SerialRec, user, OrderIndex, Quantity, callback){
	let serial = '';
	let SerialWeight = null;
	let sku = Station.pickList.orders[OrderIndex].items[itemIndex].sku;

	if(SerialRec){
		serial = SerialRec.serial;
		SerialWeight = SerialRec.serialWeight * Quantity;
	}

	if(!SerialWeight){
		SerialWeight = Station.pickList.orders[OrderIndex].items[itemIndex].eachWeight * Quantity;
	}

	PwLocs.FindAndLightAppropriateLane(Station.pickList.orders[OrderIndex].orderID, Station.stationNumber, function(Lane){
		if(Lane.Err){
			Msg = 'Station: ' + Station.stationNumber + ' ran out of working put wall locations to use';
			log.WriteToFile(null, Msg);

			return callback({Msg: Msg})
		}

		return callback({Location: Lane.Lane,
						 Order: Station.pickList.orders[OrderIndex].orderID,
						 Sku: sku,
						 Serial: serial,
						 Quantity: Quantity,
						 Courier: Station.pickList.orders[OrderIndex].courierID,
						 User: user,
						 ToteID: Station.pickList.toteID,
						 PickList: Station.pickList.orders[OrderIndex].pickListID,
						 SerialWeight: SerialWeight});
	});

} /* UpdatePwStationPL */

function FindTrueOrderDispatchedOn(Resp, callback){
	let pickListID = Resp.Ser.cartonID;
	let sku = Resp.Ser.SKU;
	let CheckSer = Resp.Ser.serial;

	let pStack = {};

	pStack.PickPL = function(callback){
		pickList.FindOne({'orders.pickListID': pickListID}, function(pResp){
			if(pResp.Err){
				return callback(null, {Use: true});
			}

			if(!pResp.PL){
				return callback(null, {Use: true});
			}

			let pPL = pResp.PL;
			let pOrders = pPL.orders;
			let x = 0;
			while(x < pOrders.length){
				if(pOrders[x].pickListID == pickListID){
					let pitems = pOrders[x].items;
					let y = 0;
					while(y < pitems.length){
						if(pitems[y].sku == sku){
							let pSerials = pitems[y].serials;
							let pNdx = pSerials.indexOf(CheckSer);

							if(pNdx >= 0){
								return callback(null, {Use: false});
							}
						}
						y++;
					}

					return callback(null, {Use: true});

					break;
				}
				x++;
			}

			return callback(null, {Use: true});
		});
	}

	pStack.PackPL = function(callback){
		packStn.FindOne({'pickList.orders.pickListID': pickListID}, function(paResp){
			if(paResp.Err){
				return callback(null, {Use: true});
			}

			if(!paResp.Station){
				return callback(null, {Use: true});
			}

			if(!paResp.Station.pickList){
				return callback(null, {Use: true});
			}

			let paPL = paResp.Station.pickList;
			let paOrders = paPL.orders;
			let x = 0;
			while(x < paOrders.length){
				if(paOrders[x].pickListID == pickListID){
					let paitems = paOrders[x].items;
					let y = 0;
					while(y < paitems.length){
						if(paitems[y].sku == sku){
							let paSerials = paitems[y].serials;
							let paNdx = paSerials.indexOf(CheckSer);

							if(paNdx >= 0){
								return callback(null, {Use: false});
							}
						}
						y++;
					}

					return callback(null, {Use: true});

					break;
				}
				x++;
			}

			return callback(null, {Use: true});
		});
	}

	async.parallel(pStack, function(err, pResult){
		console.log(JSON.stringify(pResult));
		if(err){
			return callback({Use: false});
		} else {
			if(pResult.PackPL.Use && pResult.PickPL.Use){
				return callback({Use: true});
			}

			return callback({Use: false});
		}
	});
} /* FindTrueOrderDispatchedOn */

function FindInWMS(Resp, SCserial, callback){
	let SelQuery = "select [id],[serial],[loadID],[SKU],[ASNID],[UOM],[dispatched],[received],[orderID]" +
				   " ,[userid],[serialweight],[activated],[parentNodeID],[serialStatus],[salesOrderID],[salesOrderLineID],[cartonID],[cartonNo]" +
				   " ,[noOfCartons],[createDate],[lastEditDate] from apx_serial where serial= '" + Resp.Ser.serial + "' and received = 1 and dispatched = 0";

	apexLib.msSQL.query.execute(global.msSQLClient, SelQuery, null, function (error, recordSet){
		if (error) {
			return callback(Resp);
		}

		if(!recordSet){
			return callback(Resp);
		}

		if(recordSet.length <= 0){
			return callback(Resp);
		}

		let row = recordSet[0];
		let OldSer = Resp.Ser;

		if(row.loadID == OldSer.loadID){
			if(OldSer.cartonID){
				if(OldSer.cartonID[0] != 'P'){
					return callback(Resp);
				}

				FindTrueOrderDispatchedOn(Resp, function(TResp){
					if(TResp.Use){
						OldSer.dispatched = false;
						OldSer.cartonID = null;
						OldSer.cartonNo = null;
						OldSer.salesOrderID = null;
						OldSer.salesOrderLineID = null;
						OldSer.serialStatus = 'AVAILABLE';
						return callback({Ser: OldSer});
					}

					return callback(Resp);
				});

				return;
			}
		}

		let Unow = new Date();
		OldSer.activated = row.activated;
		OldSer.ASNID = row.ASNID;
		OldSer.cartonID = row.cartonID;
		OldSer.cartonNo = row.cartonNo;
		OldSer.dispatched = row.dispatched;
		OldSer.RecNo = row.id;
		OldSer.loadID = row.loadID;
		OldSer.noOfCartons = row.noOfCartons;
		OldSer.orderID = row.orderID;
		OldSer.parentRecNo = row.parentNodeID;
		OldSer.received = row.received;
		OldSer.salesOrderID = row.salesOrderID;
		OldSer.salesOrderLineID = row.salesOrderLineID;
		OldSer.serial = row.serial;
		OldSer.serialStatus = row.serialStatus;
		OldSer.serialWeight = row.serialweight;
		OldSer.SKU = row.SKU;
		OldSer.UOM = row.UOM;
		OldSer.userID = row.userid;
		OldSer.last_update_to_wcs = Unow;

		serls.Update(OldSer, function(er,sd){
			return callback({Ser: OldSer});
		});
	});
} /* FindInWMS */

function GetSerialChildCount(RecNo, callback){
	let SelQuery = "select Count(*) as SerialCount from apx_serial_hierarchy as Tbl1 join" +
	               " (select MAX(depth) as Cnt from apx_serial_hierarchy where id_ancestor = '" + RecNo + "') as Tbl2" +
	               " on Tbl1.depth = Tbl2.Cnt" +
				   " where id_ancestor ='" + RecNo  + "'";

	apexLib.msSQL.query.execute(global.msSQLClient, SelQuery, null, function (error, CountRecordSet){
		if (error) {
			WriteToFile('RetrieveSerialsFromSql says: Error while querying for serial count ' + error.message);
			return callback({Count: 1});
		}

		if(CountRecordSet.length > 0){
			return callback({Count: CountRecordSet[0].SerialCount});
		}

		return callback({Count: 1});
	});
} /* GetSerialChildCount */

function GetInWMS(Resp, SCserial, callback){
	let SelQuery = "select [id],[serial],[loadID],[SKU],[ASNID],[UOM],[dispatched],[received],[orderID]" +
      			   " ,[userid],[serialweight],[activated],[parentNodeID],[serialStatus],[salesOrderID],[salesOrderLineID],[cartonID],[cartonNo]" +
      			   " ,[noOfCartons],[createDate],[lastEditDate] from apx_serial where serial= '" + SCserial + "' and received = 1 and dispatched = 0";

	apexLib.msSQL.query.execute(global.msSQLClient, SelQuery, null, function (error, recordSet){
		if (error) {
			return callback(Resp);
		}

		if(!recordSet){
			return callback(Resp);
		}

		if(recordSet.length <= 0){
			return callback(Resp);
		}

		let row = recordSet[0];

		GetSerialChildCount(row.id, function(Resp){
			let SerialCount = Resp.Count;

			let AddNow = new Date();
			let SerialRec = new sm({
				activated: row.activated,
				ASNID: row.ASNID,
				cartonID: row.cartonID,
				cartonNo: row.cartonNo,
				dispatched: row.dispatched,
				RecNo: row.id,
				loadID: row.loadID,
				noOfCartons: row.noOfCartons,
				orderID : row.orderID,
				parentRecNo : row.parentNodeID,
				received: row.received,
				salesOrderID: row.salesOrderID,
				salesOrderLineID: row.salesOrderLineID,
				serial: row.serial,
				serialStatus: row.serialStatus,
				serialWeight: row.serialweight,
				SKU: row.SKU,
				UOM: row.UOM,
				userID: row.userid,
				serialCount: SerialCount,
				last_update_to_wcs: AddNow,
				last_update_to_wms: null
			});

			SerialRec.save(function(err, docs){
				return callback({Ser: SerialRec});
			});
		});
	});
} /* GetInWMS */

function GetScannedSerialOrBarcode(req, callback){
	let form = new formidable.IncomingForm();
	form.parse(req, function (err, fields, files){
		if(!fields.serial){
			return callback({Ser:{Msg: 'Invalid serial scan received'}});
		}

		serls.GetSerial(fields.serial, function(Resp){
			if(!Resp.Ser){
				GetInWMS(Resp, fields.serial, function(Resp){
					if(!Resp.Ser){
						return callback({Ser:{CheckNonSerial: true, SKU: fields.serial}});
					}

					return callback(Resp);
				});

				return;
			}

			if(!Resp.Ser.dispatched){
				return callback(Resp);
			}

			FindInWMS(Resp, fields.serial, function(Resp){
				return callback(Resp);
			});
		});
	});
} /* GetScannedSerialOrBarcode */

function GetScannedPwSerialOrBarcode(req, callback){
	let form = new formidable.IncomingForm();
	form.parse(req, function (err, fields, files){
		if(!fields.pwserial){
			return callback({Ser:{Msg: 'Invalid serial scan received'}});
		}

		serls.GetSerial(fields.pwserial, function(Resp){
			if(!Resp.Ser){
				GetInWMS(Resp, fields.pwserial, function(Resp){
					if(!Resp.Ser){
						return callback({Ser:{CheckNonSerial: true, SKU: fields.pwserial}});
					}

					return callback(Resp);
				});

				return;
			}

			if(!Resp.Ser.dispatched){
				return callback(Resp);
			}

			FindInWMS(Resp, fields.pwserial, function(Resp){
				return callback(Resp);
			});
		});
	});
} /* GetScannedPwSerialOrBarcode */

function ProcNonSerialScanned(req, res, Resp, SerData, SerialSku){
	let Station = Resp.Station;
	let PL = Station.pickList;
	let user = req.session.username;
	let Msg = null;
	let StnResp = null;

	let x = 0;
	let items = PL.orders[Station.OrderIndex].items;
	let itemFound = false;
	let QtyShortageFound = false;
	let ProdSerial = false;
	while(x < items.length){
		if(items[x].barcode == SerialSku){
			itemFound = true;

			if(items[x].qtyPacked <= items[x].pickQty){
				ProdSerial = items[x].serialised;
				QtyShortageFound = true;
				break;
			}
		}
		x++;
	}

	if(!itemFound){
		Msg = 'Scanned item is not for any product in the order';
		log.WriteToFile(null, 'User ' + user + ' scanned item ' + SerialSku + ' is not for any product in order ' + PL.orders[Station.OrderIndex].orderID + ' | Station ' + Station.stationNumber);
		return HandleUserError(req, res, Station, Msg, 'Scanned item ' + SerialSku + ' is not for any product in order ' + PL.orders[Station.OrderIndex].orderID);
	}

	if(!QtyShortageFound){
		Msg = 'Too much stock scanned for product';
		log.WriteToFile(null, 'User ' + user + ' | Too much stock scanned for product ' + SerialSku + ' on tote ' + PL.toteID + ' | Order ' + PL.orders[Station.OrderIndex].orderID + ' | Station ' + Station.stationNumber);
		return HandleUserError(req, res, Station, Msg, 'Too much stock scanned for product ' + SerialSku + ' on tote ' + PL.toteID + ' | Order ' + PL.orders[Station.OrderIndex].orderID);
	}

	if(ProdSerial){
		Msg = 'Scanned product barcode is not for a non-serialised item';
		log.WriteToFile(null, 'User ' + user + ' | Product barcode ' + SerialSku + ' scanned on tote ' + PL.toteID + ' | Order ' + PL.orders[Station.OrderIndex].orderID + ' | Station ' + Station.stationNumber + ' is not for a non-serialised item');
		return HandleUserError(req, res, Station, Msg, 'Product barcode ' + SerialSku + ' scanned on tote ' + PL.toteID + ' | Order ' + PL.orders[Station.OrderIndex].orderID + ' is not for a non-serialised item');
	}

	let itemIndex = x;
	let orderIndex = Station.OrderIndex;
	UpdateStationPL(Station, itemIndex, null, user, function(Resp){
		log.WriteToFile(null, 'Successfully added non serialised item for SKU ' + SerialSku + ' to order ' + PL.orders[Station.OrderIndex].orderID + ' | Station ' + Station.stationNumber);
		StnResp = {Msg: Msg, Station: Resp.Station};
		return ProcessResponse(req, res, StnResp);
	});
} /* ProcNonSerialScanned */

function HandleUserError(req, res, Station, Msg, DbMsg){
	let MyMsg = null;
	if(Station.SupervisorControl){
		Station.UserError = Msg;
		Station.UserDbError = DbMsg;
	} else {
		MyMsg = Msg;
	}

	let StnResp = {Msg: MyMsg, Station: Station};
	return ProcessResponse(req, res, StnResp);
} /* HandleUserError */

function ProcPwNonSerialScanned(req, res, Resp, SerData, SerialSku){
	let Station = Resp.Station;
	let PL = Station.pickList;
	let user = req.session.username;
	let Msg = null;
	let StnResp = null;

	let a = 0;
	let QtyShortageFound = false;
	let ProdSerial = false;
	let itemFound = false;
	while(a < PL.orders.length){
		let x = 0;
		let items = PL.orders[a].items;
		while(x < items.length){
			if(items[x].barcode == SerialSku){
				itemFound = true;

				if(items[x].qtyPacked <= items[x].pickQty){
					ProdSerial = items[x].serialised;
					QtyShortageFound = true;
					break;
				}
			}

			if(QtyShortageFound == true){
				break;
			}
			x++;
		}
		a++;
	}

	if(!itemFound){
		Msg = 'Scanned item is not for any product in the tote';
		log.WriteToFile(null, 'User ' + user + ' scanned item ' + SerialSku + ' is not for any product in tote ' + PL.toteID  + ' | Station ' + Station.stationNumber);
		return HandleUserError(req, res, Station, Msg, 'Scanned item ' + SerialSku + ' is not for any product in tote ' + PL.toteID);
	}

	if(!QtyShortageFound){
		Msg = 'Too much stock scanned for product';
		log.WriteToFile(null, 'User ' + user + ' | Too much stock scanned for product ' + SerialSku + ' on tote ' + PL.toteID + ' | Station ' + Station.stationNumber);
		return HandleUserError(req, res, Station, Msg, 'Too much stock scanned for product ' + SerialSku + ' on tote ' + PL.toteID);
	}

	if(ProdSerial){
		Msg = 'Scanned product barcode is not for a non-serialised item';
		log.WriteToFile(null, 'User ' + user + ' | Product barcode ' + SerialSku + ' scanned on tote ' + PL.toteID + ' | Station ' + Station.stationNumber + ' is not for a non-serialised item');
		return HandleUserError(req, res, Station, Msg, 'Product barcode ' + SerialSku + ' scanned on tote ' + PL.toteID + ' is not for a non-serialised item');
	}

	let itemIndex = x;
	let orderIndex = a;
	UpdatePwStationPL(Station, itemIndex, null, user, orderIndex, 1, function(Resp){
		if(Resp.Msg){
			StnResp = {Msg: Resp.Msg, Station: Station};
			return ProcessResponse(req, res, StnResp);
		}

		let LocationData = {
			Sku: Resp.Sku,
			Serial: Resp.Serial,
			Quantity: Resp.Quantity,
			PickList: Resp.PickList,
			User: Resp.User,
			ToteID: Resp.ToteID,
			SerialWeight: Resp.SerialWeight,
			PackStation: Station.stationNumber
		}

		PwLocs.UpdateLocationToBeInUse(Resp.Location, Resp.Order, Resp.Courier, LocationData, function(){
			StnResp = {Station: Station};
			return ProcessResponse(req, res, StnResp);
		});
	});
} /* ProcPwNonSerialScanned */

function ProcSerialScanned(req, res, Resp, SerialRec, SerialSku){
	//let SerialRec = SerData.Ser;
	let Station = Resp.Station;
	let user = req.session.username;

	let serialStatus = SerialRec.serialStatus;
	let received = SerialRec.received;
	let parentRecNo = SerialRec.parentRecNo;
	let dispatched = SerialRec.dispatched;
	let UOM = SerialRec.UOM;
	let RecNo = SerialRec.RecNo;
	let serial = SerialRec.serial;
	let serialCount = SerialRec.serialCount;
	let salesOrderID = SerialRec.salesOrderID;
	let Msg = null;
	let StnResp = null;

	if(!serialCount){
		serialCount = 1;
	}

	if(serialStatus == 'BROKEN' && serialCount > 1){
		Msg = 'Scanned container serial number ' + serial + ' is no longer a fullpack';
		log.WriteToFile(null, 'User: ' + user + ' | Station: ' + Station.stationNumber + ' | Scanned container serial number ' + serial + ' is no longer a fullpack');
		return HandleUserError(req, res, Station, Msg, 'Scanned container serial number ' + serial + ' is no longer a fullpack');
	}

	if(dispatched){
		let cPL = Station.pickList;
		if(salesOrderID == cPL.orders[Station.OrderIndex].orderID){
			Msg = 'Serialnumber ' + serial + ' Already Scanned';
			log.WriteToFile(null, 'User: ' + user + ' | Station: ' + Station.stationNumber + ' | Serialnumber ' + serial + ' already scanned to order ' + cPL.orders[Station.OrderIndex].orderID);
			return HandleUserError(req, res, Station, Msg, 'Serialnumber ' + serial + ' already scanned to order ' + cPL.orders[Station.OrderIndex].orderID);
		} else {
			Msg = 'Scanned Serialnumber ' + serial + ' is dispatched';
			log.WriteToFile(null, 'User: ' + user + ' | Station: ' + Station.stationNumber + ' | Scanned Serialnumber ' + serial + ' is dispatched');
			return HandleUserError(req, res, Station, Msg, 'Scanned Serialnumber ' + serial + ' is dispatched');
		}
	}

	let PL = Station.pickList;
	let x = 0;
	let items = PL.orders[Station.OrderIndex].items;
	let itemFound = false;
	let QtyShortageFound = false;
	while(x < items.length){
		if(items[x].sku == SerialSku){
			itemFound = true;
			if((items[x].qtyPacked + serialCount) <= items[x].pickQty){
				QtyShortageFound = true;
				break;
			}
		}
		x++;
	}

	if(!itemFound){
		Msg = 'Scanned Serialnumber ' + serial + ' is not for any product in the order';
		log.WriteToFile(null, 'User: ' + user + ' | Station: ' + Station.stationNumber + ' | Scanned Serialnumber ' + serial + ' is not for any product in order ' + PL.orders[Station.OrderIndex].orderID);

		return HandleUserError(req, res, Station, Msg, 'Scanned Serialnumber ' + serial + ' is not for any product in order ' + PL.orders[Station.OrderIndex].orderID);
	}

	if(!QtyShortageFound){
		Msg = 'Scanned Serialnumber ' + serial + '. Too much stock scanned for product ' + SerialSku;
		log.WriteToFile(null, 'User: ' + user + ' | Station: ' + Station.stationNumber + ' | Scanned Serialnumber ' + serial + ' | Too much stock scanned for product ' + SerialSku + ' on tote ' + PL.toteID + ' | Order ' + PL.orders[Station.OrderIndex].orderID);

		return HandleUserError(req, res, Station, Msg, 'Scanned Serialnumber ' + serial + ' | Too much stock scanned for product ' + SerialSku + ' on tote ' + PL.toteID + ' | Order ' + PL.orders[Station.OrderIndex].orderID);
	}

	let itemIndex = x;
	let orderIndex = Station.OrderIndex;
	UpdateStationPL(Station, itemIndex, SerialRec, user, function(Resp){
		log.WriteToFile(null, 'User: ' + user + ' | Station: ' + Station.stationNumber + ' | Successfully added serialnumber ' + serial + ' to order ' + PL.orders[Station.OrderIndex].orderID);
		StnResp = {Msg: Msg, Station: Resp.Station};
		return ProcessResponse(req, res, StnResp);
	});
} /* ProcSerialScanned */

function ProcPwSerialScanned(req, res, Resp, SerData, SerialSku){
	let SerialRec = SerData.Ser;
	let Station = Resp.Station;
	let user = req.session.username;

	let serialStatus = SerialRec.serialStatus;
	let received = SerialRec.received;
	let parentRecNo = SerialRec.parentRecNo;
	let dispatched = SerialRec.dispatched;
	let UOM = SerialRec.UOM;
	let RecNo = SerialRec.RecNo;
	let serial = SerialRec.serial;
	let serialCount = SerialRec.serialCount;
	let Msg = null;
	let StnResp = null;

	if(!serialCount){
		serialCount = 1;
	}

	if(serialStatus == 'BROKEN'){
		Msg = 'Scanned container serial number ' + serial + ' is no longer a fullpack';
		log.WriteToFile(null, 'User: ' + user + ' | Station: ' + Station.stationNumber + ' | Scanned container serial number ' + serial + ' is no longer a fullpack');
		return HandleUserError(req, res, Station, Msg, 'Scanned container serial number ' + serial + ' is no longer a fullpack');
	}

	if(dispatched){
		Msg = 'Scanned Serialnumber ' + serial + ' is dispatched';
		log.WriteToFile(null, 'User: ' + user + ' | Station: ' + Station.stationNumber + ' | Scanned Serialnumber ' + serial + ' is dispatched');
		return HandleUserError(req, res, Station, Msg, 'Scanned Serialnumber ' + serial + ' is dispatched');
	}

	let PL = Station.pickList;
	let a = 0;
	let itemFound = false;
	let QtyShortageFound = false;
	let itemIndex = -1;
	while(a < PL.orders.length){
		if(PL.orders[a].WmsOrderStatus != 'CANCELED' &&
		   PL.orders[a].WmsOrderStatus != 'CANCELLED' &&
		   PL.orders[a].WmsPickListStatus != 'CANCELED' &&
		   PL.orders[a].WmsPickListStatus != 'CANCELLED'){
			let x = 0;
			let items = PL.orders[a].items;
			while(x < items.length){
				if(items[x].sku == SerialSku){
					itemFound = true;
					if((items[x].qtyPacked + serialCount) <= items[x].pickQty){
						QtyShortageFound = true;
						itemIndex = x;
						break;
					}
				}
				x++;
			}

			if(QtyShortageFound == true){
				break;
			}
		}
		a++;
	}

	if(!itemFound){
		Msg = 'Scanned Serialnumber ' + serial + ' is not for any product in the tote';
		log.WriteToFile(null, 'User: ' + user + ' | Station: ' + Station.stationNumber + ' | Scanned Serialnumber ' + serial + ' is not for any product in tote ' + PL.toteID);
		return HandleUserError(req, res, Station, Msg, 'Scanned Serialnumber ' + serial + ' is not for any product in tote ' + PL.toteID);
	}

	if(!QtyShortageFound){
		Msg = 'Too much stock scanned for product';
		log.WriteToFile(null, 'User: ' + user + ' | Station: ' + Station.stationNumber + ' | Scanned Serialnumber ' + serial + ' | Too much stock scanned for product ' + SerialSku + ' on tote ' + PL.toteID);
		return HandleUserError(req, res, Station, Msg, 'Scanned Serialnumber ' + serial + ' | Too much stock scanned for product ' + SerialSku + ' on tote ' + PL.toteID);
	}

	let orderIndex = a;
	UpdatePwStationPL(Station, itemIndex, SerialRec, user, orderIndex, serialCount, function(Resp){
		if(Resp.Msg){
			StnResp = {Msg: Resp.Msg, Station: Station};
			return ProcessResponse(req, res, StnResp);
		}

		let LocationData = {
			Sku: Resp.Sku,
			Serial: Resp.Serial,
			Quantity: Resp.Quantity,
			PickList: Resp.PickList,
			User: Resp.User,
			ToteID: Resp.ToteID,
			SerialWeight: Resp.SerialWeight,
			PackStation: Station.stationNumber
		}

		PwLocs.UpdateLocationToBeInUse(Resp.Location, Resp.Order, Resp.Courier, LocationData, function(){
			log.WriteToFile(null, 'User: ' + user + ' | Station: ' + Station.stationNumber + ' | Successful scan of serialnumber ' + Resp.Serial + ' to order ' + Resp.Order + ' at putwall Station.');
			StnResp = {Station: Station};
			return ProcessResponse(req, res, StnResp);
		});
	});
} /* ProcPwSerialScanned */

function ProcValidateSerialOrBarcode(req, res, Resp, SerData){
	let SerialRec = SerData.Ser;
	let Station = Resp.Station;
	let user = req.session.username;
	let StnResp = null;

	if(!SerialRec){
		StnResp = {Msg: null, Station: Station};
		return ProcessResponse(req, res, StnResp);
	}

	if(SerialRec.Msg){
		log.WriteToFile(null, 'Nothing scanned as serial/barcode at station ' + Station.stationNumber );
		StnResp = {Msg: SerialRec.Msg, Station: Station};
		return ProcessResponse(req, res, StnResp);
	}

	let SerialSku = SerialRec.SKU;

	if(SerialRec.CheckNonSerial){
		return ProcNonSerialScanned(req, res, Resp, SerData, SerialSku);
	}

	return ProcSerialScanned(req, res, Resp, SerialRec, SerialSku);
} /* ProcValidateSerialOrBarcode */

function ProcValidatePwSerialOrBarcode(req, res, Resp, SerData){
	let SerialRec = SerData.Ser;
	let Station = Resp.Station;
	let user = req.session.username;
	let StnResp = null;

	if(!SerialRec){
		StnResp = {Msg: null, Station: Station};
		return ProcessResponse(req, res, StnResp);
	}

	if(SerialRec.Msg){
		log.WriteToFile(null, 'Nothing scanned as serial/barcode at station ' + Station.stationNumber );
		StnResp = {Msg: SerialRec.Msg, Station: Station};
		return ProcessResponse(req, res, StnResp);
	}

	let SerialSku = SerialRec.SKU;
	let itemFound = false;
	if(SerialRec.CheckNonSerial){
		return ProcPwNonSerialScanned(req, res, Resp, SerData, SerialSku);
	}

	return ProcPwSerialScanned(req, res, Resp, SerData, SerialSku);
} /* ProcValidatePwSerialOrBarcode */

function ProcClearStation(req, res, Resp, Mymsg){
	let Station = packStn.ResetPackStationValues(Resp.Station);

	packStn.Update(Station, function(Resp){
		let StnResp = null;

		if(Resp.Err){
			StnResp = {Msg: 'Failed To Remove Order From Station', Station: Station};
		} else {
			StnResp = {Station: Resp.SavedDoc, Msg: Mymsg};
		}

		return ProcessResponse(req, res, StnResp);
	});
} /* ProcClearStation */

function HandleEachOrderOnStation(StnNo, callback){
	packStn.FindOne({'stationNumber': StnNo}, function(Resp){
		if(Resp.Err){
			return callback({Err: Resp.Err});
		}

		if(!Resp.Station){
			return callback({Err: 'Station Not Found'});
		}

		let Station = Resp.Station;
		if(!Station.pickList){
			return callback({Err: 'No Picklist On Station'});
		}

		let PL = Station.pickList;
		if(!PL.orders || PL.orders.length <= 0){
			return callback({ToteID: PL.toteID});
		}

		let StnOrder = PL.orders[0];
		if(!StnOrder.cartonID){
			serls.ResetSerials(StnOrder.orderID, StnOrder.pickListID, function(){});
			Station.pickList.orders.splice(0, 1);
			packStn.Update(Station, function(Resp){
				return HandleEachOrderOnStation(StnNo, callback);
			});

			return;
		}

		pickList.FindOne({'orders.pickListID': StnOrder.pickListID}, function(Resp){
			if(Resp.Err || !Resp.PL){
				return callback({Err: 'No Picklist Found'});
			}

			let x = 0;
			let DbPl = Resp.PL;
			while(x < DbPl.orders.length){
				if(DbPl.orders[x].pickListID == StnOrder.pickListID){
					break;
				}
				x++;
			}

			if(x >= DbPl.orders.length){
				return callback({Err: 'No Picklist Found'});
			}

			if(DbPl.orders.length > 1){
				let OrderDate = DbPl.CreateDate;
				if(!OrderDate){
				   OrderDate =  moment(new Date()).format('YYYY-MM-DD HH:mm');
				}

				StnOrder.packComplete = true;

				let NewPLObj = {
					toteID: StnOrder.cartonID,
					transporterID: StnOrder.transporterID,
					destination: DbPl.destination,
					packType: DbPl.packType,
					toteInUse: false,
					toteInUseBy: DbPl.toteInUseBy,
					orders: StnOrder,
					Status: 'PACKED',
					PackUpdateRequired: false,
					ShipUpdateRequired: false,
					PackedDate: moment(new Date()).format('YYYY-MM-DD HH:mm'),
					containerLocation: null,
					PickListType: null,
					FromTote: DbPl.toteID,
					CreateDate: OrderDate
				}

				serls.UpdateSerialsToHost(StnOrder, function(){});
				DbPl.orders.splice(x, 1);

				if(DbPl.orders.length <= 0){
					pickList.Delete({'toteID': PL.toteID}, function(){
						let NewPL = pickList.FormNewPickList(NewPLObj);
						pickList.Update(NewPL, function(Resp){
							CreatePackUpdate(NewPL);
							Station.pickList.orders.splice(0, 1);
							packStn.Update(Station, function(Resp){
								return HandleEachOrderOnStation(StnNo, callback);
							});
						});
					});
				} else {
					pickList.Update(DbPl, function(Resp){
						let NewPL = pickList.FormNewPickList(NewPLObj);
						pickList.Update(NewPL, function(Resp){
							CreatePackUpdate(NewPL);
							Station.pickList.orders.splice(0, 1);
							packStn.Update(Station, function(Resp){
								return HandleEachOrderOnStation(StnNo, callback);
							});
						});
					});
				}
			} else {
				StnOrder.packComplete = true;

				DbPl.orders = StnOrder;
				DbPl.toteInUse = false;
				DbPl.FromTote = DbPl.toteID;
				DbPl.toteID = StnOrder.cartonID;
				DbPl.transporterID = StnOrder.transporterID;
				DbPl.PickListType = null;
				DbPl.Packing = false;
				DbPl.Status = 'PACKED';
				DbPl.PackedDate = moment(new Date()).format('YYYY-MM-DD HH:mm');

				pickList.Update(DbPl, function(Resp){
					CreatePackUpdate(DbPl);
					serls.UpdateSerialsToHost(StnOrder, function(){});
					Station.pickList.orders.splice(0, 1);
					packStn.Update(Station, function(Resp){
						return HandleEachOrderOnStation(StnNo, callback);
					});
				});
			}
		});
	});
} /* HandleEachOrderOnStation */

function ProcResetTote(req, res, Resp){
	let user = req.session.username;
	let Station = Resp.Station;
	let PL = Station.pickList;
	let StnNo = Station.stationNumber;
	let StnResp = null;

	log.WriteToFile(null, 'User: ' + user + ' | Tote ' + PL.toteID + ' reset at station ' + Station.stationNumber);

	CalculateTrWeightSoFar(Station, function(StnResp){
		Resp.Station = StnResp.Station;
		Station = Resp.Station;

		packStn.Update(Station, function(SaveResp){
			Resp.Station = SaveResp.SavedDoc;
			Station = Resp.Station;

			HandleEachOrderOnStation(StnNo, function(hResp){
				if(hResp.Err){
					log.WriteToFile(null, 'User: ' + user + ' | Tote ' + PL.toteID + ' | Error while trying to reset tote at station ' + Station.stationNumber);
					StnResp = {Msg: hResp.Err, Station: Station};
					return ProcessResponse(req, res, StnResp);
				}

				pickList.FindOne({'toteID': hResp.ToteID}, function(pResp){
					if(pResp.Err || !pResp.PL){
						log.WriteToFile(null, 'User: ' + user + ' | Tote ' + PL.toteID + ' | Error while trying to find picklist to reset tote at station ' + Station.stationNumber);
						return ProcClearStation(req, res, Resp, null);
					}

					let DbPl = pResp.PL;
					DbPl.toteInUse = false;
					DbPl.toteInUseBy = null;
					pickList.Update(DbPl, function(pResp){
						let MyMsg = null;
						if(Station.currentTransporter){
							log.WriteToFile(null, 'User: ' + user + ' | Transporter ' + Station.currentTransporter + ' to release after tote reset at station ' + Station.stationNumber);
							MyMsg = 'Please release transporter ' + Station.currentTransporter + ' from station';
						}

						log.WriteToFile(null, 'User: ' + user + ' | Tote ' + PL.toteID + ' successfully reset at station ' + Station.stationNumber);
						return ProcClearStation(req, res, Resp, MyMsg);
					});
				});
			});
		});
	});
} /* ProcResetTote */

function PwEvaluteEachOrderInPL(PL, index, callback){
	if(index >= PL.orders.length){
		return callback();
	}

	serls.ResetSerials(PL.orders[index].orderID, PL.orders[index].pickListID, function(){
		index++;
		return PwEvaluteEachOrderInPL(PL, index, callback);
	});
} /* PwEvaluteEachOrderInPL */

function LightAllLocToClear(PL, index, StationNumber, user, callback){
	if(index >= PL.orders.length){
		return callback();
	}

	PwLocs.FindPutWallLocationUsingOrderID(PL.orders[index].orderID, StationNumber, function(Lane){
		if(!Lane){
			index++;
			return LightAllLocToClear(PL, index, StationNumber, user, callback);
		}

		PwLocs.LightupLoc(Lane, '_ON_RED', StationNumber, function(Response){
			PwLocs.FindOne({'putAddress': Lane}, function(Resp){
				let PWLOC = Resp.pwLoc;
				if(PWLOC){
					PwLocs.ClearPutWallLoc(PWLOC, function(){
						log.WriteToFile(null, 'User: ' + user + ' | Putwall location ' + Lane + ' cleared, displaying in RED for physical stock to be placed back into tote ' + PL.toteID + ' at putwall station ' + StationNumber);
						index++;
						return LightAllLocToClear(PL, index, StationNumber, user, callback);
					});
				}
			});
		});
	});
} /* LightAllLocToClear */

function ProcResetPwTote(req, res, Resp){
	let user = req.session.username;
	let Station = Resp.Station;
	let PL = Station.pickList;
	let StnNo = Station.stationNumber;
	let StnResp = null;

	log.WriteToFile(null, 'User: ' + user + ' | Tote ' + PL.toteID + ' reset at putwall station ' + Station.stationNumber);

	pickList.FindOne({'toteID': PL.toteID}, function(pResp){
		if(pResp.Err || !pResp.PL){
			log.WriteToFile(null, 'User: ' + user + ' | Tote ' + PL.toteID + ' | Error while trying to find picklist to reset at putwall station ' + Station.stationNumber);
			return ProcClearStation(req, res, Resp, null);
		}

		let DbPl = pResp.PL;
		DbPl.toteInUse = false;
		DbPl.toteInUseBy = null;

		PwEvaluteEachOrderInPL(PL, 0, function(){
			pickList.Update(DbPl, function(pResp){
				log.WriteToFile(null, 'User: ' + user + ' | Tote ' + PL.toteID + ' reset successful at putwall station ' + Station.stationNumber);
				LightAllLocToClear(PL, 0, Station.stationNumber, user, function(){
					return ProcClearStation(req, res, Resp, null);
				});
			});
		});
	});
} /* ProcResetPwTote*/

function ProcCartonSizeChange(req, res, Resp, CartonSize){
	let user = req.session.username;
	let Station = Resp.Station;
	let NewResp = null;

	if(!Station.CartonSizes){
		return ProcessResponse(req, res, Resp);
	}

	let x = 0;
	while(x < Station.CartonSizes.length){
		if(parseInt(Station.CartonSizes[x].Size) == parseInt(CartonSize)){
			break;
		}
		x++;
	}

	if(x >= Station.CartonSizes.length){
		return ProcessResponse(req, res, Resp);
	}

	let OldSize = Station.pickList.orders[Station.OrderIndex].cartonSize;
	Station.pickList.orders[Station.OrderIndex].cartonSize = parseInt(Station.CartonSizes[x].Size);
	Station.pickList.orders[Station.OrderIndex].useTransporter = Station.CartonSizes[x].Transporter;

	packStn.Update(Station, function(Resp){
		if(Resp.Err){
			NewResp = {Msg: 'Failed To Update New Carton Size', Station: Station};
			return ProcessResponse(req, res, NewResp);
		}

		log.WriteToFile(null, 'User: ' + user + ' | Order ' + Station.pickList.orders[Station.OrderIndex].orderID + ' at station ' + Station.stationNumber + ' carton size change from ' + OldSize + ' to ' + Station.pickList.orders[Station.OrderIndex].cartonSize);
		NewResp = {Station: Resp.SavedDoc};
		return ProcessResponse(req, res, NewResp);
	});
} /* ProcCartonSizeChange */

function CartonUniqueInSystem(Carton, callback){
	pickList.FindOne({'orders.cartonID': Carton}, function(Resp){
		if(Resp.Err){
			return callback(true);
		}

		if(Resp.PL){
			return callback(false);
		}

		return callback(true);
	});
} /* CartonUniqueInSystem */

function CartonUsedAtAnyStation(Carton, callback){
	packStn.FindOne({'pickList.orders.cartonID': Carton}, function(Resp){
		if(Resp.Err){
			return callback(false);
		}

		if(Resp.Station){
			return callback(true);
		}

		return callback(false);
	});
} /* CartonUsedAtAnyStation */

function GetPrintData(Station, OrdIndex, PwOrder, callback){
	let PL = Station.pickList;
	let PicklistID = PL.orders[OrdIndex].pickListID;
	let CollectedPLs = [];
	let Cartons = [];
	let OrdItems = [];
	let Collect = true;

	packStn.Find({'pickList.orders.orderID': PL.orders[OrdIndex].orderID}, function(paResp){
		if(paResp.Err){
			Collect = false;
		}

		if(!paResp.PackingStnArr || paResp.PackingStnArr.length <= 0){
			Collect = false;
		}

		if(Collect){
			let x = 0;
			let StnArr = paResp.PackingStnArr;
			while(x < StnArr.length){
				let y = 0;
				let MyStnPL = StnArr[x].pickList;
				while(y < MyStnPL.orders.length){
					if(MyStnPL.orders[y].orderID == PL.orders[OrdIndex].orderID && (MyStnPL.orders[y].cartonID || PwOrder)){
						let z = 0;
						while(z < MyStnPL.orders[y].items.length){
							Cartons.push(MyStnPL.orders[y].cartonID);
							OrdItems.push(MyStnPL.orders[y].items[z]);
							z++;
						}

						CollectedPLs.push(MyStnPL.orders[y].pickListID);
					}
					y++;
				}
				x++;
			}
		}

		pickList.Find({'orders.orderID': PL.orders[OrdIndex].orderID}, function(Resp){
			if(Resp.Err){
				return callback({Msg: 'Failed print data', Station: Station});
			}

			if(!Resp.pickListArr || Resp.pickListArr.length <= 0){
				return callback({Msg: 'Failed print data', Station: Station});
			}

			let SysPLs = Resp.pickListArr;
			let x = 0;
			while(x < SysPLs.length){
				let y = 0;
				while(y < SysPLs[x].orders.length){
					let PlIndex = CollectedPLs.indexOf(SysPLs[x].orders[y].pickListID);
					if(SysPLs[x].orders[y].orderID == PL.orders[OrdIndex].orderID && PlIndex < 0 && (SysPLs[x].orders[y].cartonID || PwOrder)){
						let z = 0;
						while(z < SysPLs[x].orders[y].items.length){
							Cartons.push(SysPLs[x].orders[y].cartonID);
							OrdItems.push(SysPLs[x].orders[y].items[z]);
							z++;
						}
					}
					y++;
				}
				x++;
			}

			return callback({Station: Station, Cartons: Cartons, OrdItems: OrdItems});
		});
	});
} /* GetPrintData */

function ProcSupervisorOverride(req, res, Resp){
	let uid = req.session.uid;
	delete req.session.uid;
	let psw = req.session.psw;
	delete req.session.psw;

	let Station = Resp.Station;

	sysuser.FindOne({'username': uid}, function(Resp){
		if(Resp.Err || !Resp.User){
			log.WriteToFile(null, 'Err while checking supervisor user access | Err: ' + Resp.Err);
			req.session.SupervisorErr = 'Invalid supervisor username or password';
			NewResp = {Station: Station};
			return ProcessResponse(req, res, NewResp);
		}

		if(Resp.User.password != psw){
			log.WriteToFile(null, 'Supervisor user access failed -> password mismatch for user ' + uid);
			req.session.SupervisorErr = 'Invalid supervisor username or password';
			NewResp = {Station: Station};
			return ProcessResponse(req, res, NewResp);
		}

		if(Resp.User.roleID < 2){
			log.WriteToFile(null, 'Supervisor user access failed -> User ' + uid + ' is not a supervisor or administrator');
			req.session.SupervisorErr = 'Unauthorised login. Only supervisors or administrators allowed';
			NewResp = {Station: Station};
			return ProcessResponse(req, res, NewResp);
		}

		let user = req.session.username;
		let MyRec = new UserErr({
			Date: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
			User: user,
			Supervisor: uid,
			Error: Station.UserDbError,
			Station: Station.stationNumber
		});

		MyRec.save(function(err, savedDoc){
			Station.UserError = null;
			Station.UserDbError = null;

			packStn.Update(Station, function(Resp){
				log.WriteToFile(null, 'Supervisor: ' + uid + ' cleared error for user ' + user + ' at station ' + Station.stationNumber);
				NewResp = {Station: Resp.SavedDoc};
				return ProcessResponse(req, res, NewResp);
			});
		});
	});
} /* ProcSupervisorOverride */

function ProcDetermineSysErrorControlMode(req, res, Resp){
	let Station = Resp.Station;
	let OkToProceed = true;
	let Status = false;

	sysParam.FindOne({'ParameterName' : 'PACKING_SUPERVISOR_CONTROL'}, function(Resp){
		if(Resp.Err){
			log.WriteToFile(null, 'Failed To Find Paramater PACKING_SUPERVISOR_CONTROL | Err [' + Resp.Err + ']');
			OkToProceed = false;
		}

		if(!Resp.Param && OkToProceed){
			log.WriteToFile(null, 'Failed To Find Paramater PACKING_SUPERVISOR_CONTROL');
			OkToProceed = false;
		}

		if(OkToProceed){
			let SupControl = Resp.Param;
			Status = SupControl.Fields.SupervisorControl;

			if(!Status){
				req.session.errorcontrol = 'OFF';
			} else {
				req.session.errorcontrol = 'ON';
			}
		} else {
			req.session.errorcontrol = 'OFF';
		}

		Station.SupervisorControl = Status;
		packStn.Update(Station, function(Resp){
			log.WriteToFile(null, 'Station ' + Station.stationNumber + ' SupervisorControl set ' + req.session.errorcontrol);
			NewResp = {Station: Resp.SavedDoc};
			return ProcessResponse(req, res, NewResp);
		});
	});
} /* ProcDetermineSysErrorControlMode */

function ProcConfirmCartonscanned(req, res, Resp, Carton){
	let user = req.session.username;
	let Station = Resp.Station;
	let NewResp = null;

	log.WriteToFile(null, 'User: ' + user + ' | Carton ' + Carton + ' scan at station ' + Station.stationNumber + ' for order ' + Station.pickList.orders[Station.OrderIndex].orderID);

	if(parseInt(Carton[0]) != Station.pickList.orders[Station.OrderIndex].cartonSize || Carton.length != 8){
		log.WriteToFile(null, 'User: ' + user + ' | Incorrect value ' + Carton + ' scanned as Carton ID at station ' + Station.stationNumber);
		return HandleUserError(req, res, Station, 'Incorrect value scanned as Carton ID', 'Incorrect value ' + Carton + ' scanned as Carton ID');
	}

	CartonUniqueInSystem(Carton, function(Unique){
		if(!Unique){
			log.WriteToFile(null, 'User: ' + user + ' | Scanned Carton ' + Carton + ' scanned at station ' + Station.stationNumber + ' is not unique');
			return HandleUserError(req, res, Station, 'Scanned Carton ID is not unique', 'Scanned Carton ' + Carton + ' scanned is not unique');
		}

		CartonUsedAtAnyStation(Carton, function(Used){
			if(Used){
				log.WriteToFile(null, 'User: ' + user + ' | Scanned Carton ' + Carton + ' scanned at station ' + Station.stationNumber + ' is not unique');
				return HandleUserError(req, res, Station, 'Scanned Carton ID is no unique', 'Scanned Carton ' + Carton + ' scanned is not unique');
			}

			let body = {
				cartonID:   Carton,
				orderID:    Station.pickList.orders[Station.OrderIndex].orderID,
				picklistID: Station.pickList.orders[Station.OrderIndex].pickListID
			}

			rest.postJson(CartonUrl, body).on('complete', function(result){
				if(result instanceof Error){
					log.WriteToFile(null, 'User: ' + user + ' | Failed to find sequence data for carton ' + Carton + ' scanned at station ' + Station.stationNumber + ' | Order ' + Station.pickList.orders[Station.OrderIndex].orderID);
					NewResp = {Msg: 'Failed to find carton number', Station: Station};
					return ProcessResponse(req, res, NewResp);
				}

				if(result.Err){
					log.WriteToFile(null, 'User: ' + user + ' | Failed to find sequence data for carton ' + Carton + ' scanned at station ' + Station.stationNumber + ' | Order ' + Station.pickList.orders[Station.OrderIndex].orderID);
					NewResp = {Msg: 'Failed to find carton number', Station: Station};
					return ProcessResponse(req, res, NewResp);
				}

				let CartonNo = result.CartonNo;
				let NoOfCartons = result.NoOfCartons;
				log.WriteToFile(null, 'User: ' + user + ' | Carton ' + Carton + ' scanned at station ' + Station.stationNumber + ' is number ' + CartonNo + ' of ' + NoOfCartons + ' for order ' + Station.pickList.orders[Station.OrderIndex].orderID);
				UpdateCartonNew(Station, Carton, CartonNo, NoOfCartons, function(Resp){
					Station = Resp.SavedDoc;
					GetPrintData(Station, Station.OrderIndex, false, function(PrintData){
						if(PrintData.Msg){
							log.WriteToFile(null, 'User: ' + user + ' | Carton ' + Carton + ' scanned at station ' + Station.stationNumber + ' has failed to get print data. Err: ' + PrintData.Msg);
							NewResp = {Msg: 'Failed to find print data', Station: Station};
							return ProcessResponse(req, res, NewResp);
						}

						prn.PrintDocuments(PrintData, null, Station.OrderIndex, function(Resp){
							if(Resp.Msg){
								log.WriteToFile(null, 'User: ' + user + ' | Carton ' + Carton + ' scanned at station ' + Station.stationNumber + ' has failed to print invoice/label. Err: ' + Resp.Msg);
							}

							Station.pickList.orders[Station.OrderIndex].printed = true;

							packStn.Update(Station, function(Resp){
								log.WriteToFile(null, 'User: ' + user + ' | Carton ' + Carton + ' scanned at station ' + Station.stationNumber + ' printed successfully for order ' + Station.pickList.orders[Station.OrderIndex].orderID);
								NewResp = {Station: Resp.SavedDoc};
								return ProcessResponse(req, res, NewResp);
							});
						});
					});
				});
			});
		});
	});
} /* ProcConfirmCartonscanned */

function ProcReprintDocuments(req, res, Resp){
	let user = req.session.username;
	let Station = Resp.Station;

    log.WriteToFile(null, 'User: ' + user + ' | Carton ' + Station.pickList.orders[Station.OrderIndex].cartonID + ' | Order ' + Station.pickList.orders[Station.OrderIndex].orderID + ' at station ' + Station.stationNumber + ' reprint');
	GetPrintData(Station, Station.OrderIndex, false, function(PrintData){
		if(PrintData.Msg){
			log.WriteToFile(null, 'User: ' + user + ' | Carton ' + Station.pickList.orders[Station.OrderIndex].cartonID + ' | Order ' + Station.pickList.orders[Station.OrderIndex].orderID + ' at station ' + Station.stationNumber + ' failed to reprint');
			NewResp = {Msg: 'Failed to find print data', Station: Station};
			return ProcessResponse(req, res, NewResp);
		}

		prn.PrintDocuments(PrintData, null, Station.OrderIndex, function(Resp){
			if(Resp.Msg){
				log.WriteToFile(null, Resp.Msg);
			}

			Station.pickList.orders[Station.OrderIndex].printed = true;
			packStn.Update(Station, function(Resp){
				log.WriteToFile(null, 'User: ' + user + ' | Carton ' + Station.pickList.orders[Station.OrderIndex].cartonID + ' | Order ' + Station.pickList.orders[Station.OrderIndex].orderID + ' at station ' + Station.stationNumber + ' reprinted successfully');
				NewResp = {Station: Resp.SavedDoc};
				return ProcessResponse(req, res, NewResp);
			});
		});
	});
} /* ProcReprintDocuments */

function ProcConfirmInvoiceDocument(req, res, Resp, Document){
	let user = req.session.username;
	let Station = Resp.Station;
	let NewResp = null;

	log.WriteToFile(null, 'User: ' + user + ' | Invoice document ' + Document + ' scan | Order ' + Station.pickList.orders[Station.OrderIndex].orderID + ' at station ' + Station.stationNumber);

	if(Document != Station.pickList.orders[Station.OrderIndex].orderID){
		log.WriteToFile(null, 'User: ' + user + ' | Invalid invoice document ' + Document + ' scan for order ' + Station.pickList.orders[Station.OrderIndex].orderID + ' at station ' + Station.stationNumber);
		return HandleUserError(req, res, Station, 'Scanned Document Barcode Is Not For The Order At This Station', 'Invalid invoice document ' + Document + ' scan for order ' + Station.pickList.orders[Station.OrderIndex].orderID);
	}

	Station.pickList.orders[Station.OrderIndex].orderVerified = true;
	packStn.Update(Station, function(Resp){
		log.WriteToFile(null, 'User: ' + user + ' | Invoice document ' + Document + ' scan | Order ' + Station.pickList.orders[Station.OrderIndex].orderID + ' at station ' + Station.stationNumber + ' successfully validated.');
		NewResp = {Station: Resp.SavedDoc};
		return ProcessResponse(req, res, NewResp);
	});
} /* ProcConfirmInvoiceDocument */

function ProcConfirmPwInvoiceDocument(req, res, Resp, Document){
	let user = req.session.username;
	let Station = Resp.Station;
	let NewResp = null;

	log.WriteToFile(null, 'User: ' + user + ' | Invoice document ' + Document + ' at putwall station ' + Station.stationNumber);

	let x = 0;
	while(x < Station.pickList.orders.length){
		if(Document == Station.pickList.orders[x].orderID){
			break;
		}
		x++;
	}

	if(x >= Station.pickList.orders.length){
		log.WriteToFile(null, 'User: ' + user + ' | Invoice document ' + Document + ' at putwall station ' + Station.stationNumber + ' is not for any order at station.');
		return HandleUserError(req, res, Station, 'Scanned Document Barcode Is Not For Any Order At This Station', 'Invoice document ' + Document + ' is not for any order at station.');
	}

	PwLocs.FindPutWallLocationUsingOrderID(Station.pickList.orders[x].orderID, Station.stationNumber, function(Lane){
		if(!Lane){
			log.WriteToFile(null, 'User: ' + user + ' | Failed to find location for invoice document ' + Document + ' | Order ' + Station.pickList.orders[x].orderID + ' at putwall station ' + Station.stationNumber);
			NewResp = {Msg: 'Failed To Find Location For Scanned Invoice Document', Station: Station};
			return ProcessResponse(req, res, NewResp);
		}

		PwLocs.LightupLoc(Lane, '_ON_GREEN', Station.stationNumber, function(Response){
			if(Response.error){
				log.WriteToFile(null, 'User: ' + user + ' | Invoice document ' + Document + ' | Order ' + Station.pickList.orders[x].orderID + ' at putwall station ' + Station.stationNumber + ' failed to light up location ' + Lane);
				let Msg = 'Error while trying to switch on light for location ' + Lane;
				NewResp = {Msg: Msg, Station: Station};
				return ProcessResponse(req, res, NewResp);
			}

			let LocationData = {
				Document: Station.pickList.orders[x].orderID,
				PickList: Station.pickList.orders[x].pickListID,
				ToteID: Station.pickList.toteID,
				PackStation: Station.stationNumber
			}

			PwLocs.UpdateLocationToBeInUse(Lane, Station.pickList.orders[x].orderID, Station.pickList.orders[x].courierID, LocationData, function(){
				log.WriteToFile(null, 'User: ' + user + ' | Invoice document ' + Document + ' | Order ' + Station.pickList.orders[x].orderID + ' at putwall station ' + Station.stationNumber + ' successfully validated. Needs to be placed in location ' + Lane + ' lit in GREEN.');
				NewResp = {Station: Station};
				return ProcessResponse(req, res, NewResp);
			});
		});
	});
} /* ProcConfirmPwInvoiceDocument */

function ClearShippedOrdersFromTransporter(docArr, index, callback){
	if(index >= docArr.length){
		return callback();
	}

	docArr[index].transporterID = null;
	pickList.Update(docArr[index], function(Resp){
		index++;
		ClearShippedOrdersFromTransporter(docArr, index, callback);
	});
} /* ClearShippedOrdersFromTransporter */

function ProcAddTransporter(req, res, Resp, Transporter){
	let user = req.session.username;
	let Station = Resp.Station;
	let NewResp = null;

	pickList.Find({'transporterID': Transporter}, function(Resp){
        let docArr = Resp.pickListArr;
		let Unique = true;
		if(!Resp.Err && docArr){
			let x = 0;
			while(x < docArr.length){
				if(docArr[x].Status != 'SHIPPED'){
					Unique = false;
					break;
				}
				x++;
			}
		}

		if(!Unique){
			log.WriteToFile(null, 'User: ' + user + ' | Transporter ' + Transporter + ' scanned at station ' + Station.stationNumber + ' is not valid because its not empty. ');
			NewResp = {Msg: 'Scanned Transporter is not empty', Station: Station};
			return ProcessResponse(req, res, NewResp);
		}

		if(docArr && docArr.length > 0){
			ClearShippedOrdersFromTransporter(docArr, 0, function(){});
		}

		Station.currentTransporter = Transporter;
		Station.pickList.orders[Station.OrderIndex].transporterID = Transporter;

		packStn.Update(Station, function(Resp){
			log.WriteToFile(null, 'User: ' + user + ' | Transporter ' + Transporter + ' scanned at station ' + Station.stationNumber + ' successfully added to station');
			log.WriteToFile(null, 'User: ' + user + ' | Transporter ' + Transporter + ' scanned at station ' + Station.stationNumber + ' successfully validated for order ' + Station.pickList.orders[Station.OrderIndex].orderID);
			NewResp = {Station: Resp.SavedDoc};
			return ProcessResponse(req, res, NewResp);
		});
	});
} /* ProcAddTransporter */

function ProcConfirmTransporter(req, res, Resp, Transporter){
	let user = req.session.username;
	let Station = Resp.Station;
	let NewResp = null;

	log.WriteToFile(null, 'User: ' + user + ' | Transporter ' + Transporter + ' scan at station ' + Station.stationNumber);

	if(Transporter[0] != '7' || Transporter.length != 8){
		log.WriteToFile(null, 'User: ' + user + ' | Invalid transporter ' + Transporter + ' value scanned as transporter at station ' + Station.stationNumber);
		return HandleUserError(req, res, Station, 'Incorrect value scanned as Transporter ID', 'Invalid transporter ' + Transporter + ' value scanned as transporter');
	}

	if(!Station.currentTransporter){
		return ProcAddTransporter(req, res, Resp, Transporter);
	}

	if(Transporter != Station.currentTransporter){
		log.WriteToFile(null, 'User: ' + user + ' | Transporter ' + Transporter + ' scanned at station ' + Station.stationNumber + ' differs from current station transporter ' + Station.currentTransporter);
		return HandleUserError(req, res, Station, 'Scanned Transporter Differs From The Current Transporter On The Station', 'Transporter ' + Transporter + ' scanned differs from current station transporter ' + Station.currentTransporter);
	}

	Station.pickList.orders[Station.OrderIndex].transporterID = Transporter;
	packStn.Update(Station, function(Resp){
		log.WriteToFile(null, 'User: ' + user + ' | Transporter ' + Transporter + ' scanned at station ' + Station.stationNumber + ' successfully validated for order ' + Station.pickList.orders[Station.OrderIndex].orderID);
		NewResp = {Station: Station};
		return ProcessResponse(req, res, NewResp);
	});
} /* ProcConfirmTransporter */

function ShowPrintToPdfScreen(req, res){
	let Ip = html.GetHeaderIpAddr(req);
	let user = req.session.username;

	sysuser.FindOne({'username': user}, function(Resp){
		let dt;
		if(Resp.Err){
			log.WriteToFile(null, 'Err while checking user access type [printtopdf] | Err: ' + Resp.Err);
			dt = {NoAccess: true};
		} else {
			if(Resp.User){
				if(Resp.User.roleID < 2){
					 dt = {NoAccess: true};
				} else {
					 dt = {Access: true};
				}
			} else {
				 dt = {NoAccess: true};
			}
		}

		let frame = {
			PrintToPDF: html.GetPage('PrintToPDF')
		}

		let Data = html.ProcessPdfData(dt, user, Ip);
		let page = html.GetPage(null);
		let htmlpage = mustache.render(page, Data, frame);

		res.send(htmlpage);
	});
} /* ShowPrintToPdfScreen */

function PrintToPdfResponse(frame, Resp, user, Ip, res){
	let Data = html.ProcessPdfData(Resp, user, Ip);
	let page = html.GetPage(null);
	let htmlpage = mustache.render(page, Data, frame);

	return res.send(htmlpage);
} /* PrintToPdfResponse */

function ProcInvoiceToPdf(req, res){
	let Ip = html.GetHeaderIpAddr(req);
	let form = new formidable.IncomingForm();

	form.parse(req, function (err, fields, files){
		let user = req.session.username;
		let frame = {
			PrintToPDF: html.GetPage('PrintToPDF')
		}

		if(!fields.orderID){
			return PrintToPdfResponse(frame, {Msg: 'Nothing Entered'}, user, Ip, res);
		}

		pickList.Find({'orders.orderID': fields.orderID}, function(Resp){
			let Process = true;
			if(Resp.Err){
				return PrintToPdfResponse(frame, {Msg: 'Failed to find the entered order ID in the system'}, user, Ip, res);
			}

			if(!Resp.pickListArr || Resp.pickListArr.length <= 0){
				return PrintToPdfResponse(frame, {Msg: 'Entered order ID is not found on the system'}, user, Ip, res);
			}

			let x = 0;
			let pickListArr = Resp.pickListArr;
			let LastCartonIndex = -1;
			let NotAllComplete = false;

			while(x < pickListArr.length){
				if(pickListArr[x].Status == 'SHIPPED' ||
				   pickListArr[x].Status == 'PACKED' ||
				   pickListArr[x].Status == 'RETURNED' ||
				   pickListArr[x].Status == 'STORED'){
					if(pickListArr[x].numberOfTotes == pickListArr[x].toteNumber){
						LastCartonIndex = x;
					}
				} else {
					NotAllComplete = true;
					break;
				}
				x++;
			}

			if(NotAllComplete){
				return PrintToPdfResponse(frame, {Msg: 'Not all totes for order have been processed'}, user, Ip, res);
			}

			if(LastCartonIndex < 0){
				return PrintToPdfResponse(frame, {Msg: 'Error while trying to calculate all cartons for order'}, user, Ip, res);
			}

			x = 0;
			let OrderIndex = 0;
			while(x < pickListArr[LastCartonIndex].orders.length){
				if(pickListArr[LastCartonIndex].orders[x].orderID == fields.orderID){
					OrderIndex = x;
					break;
				}

				x++;
			}

			pickList.DetermineCartonNo(pickListArr[LastCartonIndex], OrderIndex, function(Resp){
				if(Resp.Msg){
					return PrintToPdfResponse(frame, Resp, user, Ip, res);
				}

				let Cartons = Resp.Cartons;
				let OrdItems = Resp.OrdItems;

				prn.PrintDocumentsPdf(pickListArr[LastCartonIndex], {OrderIndex: OrderIndex, Cartons: Cartons, OrdItems: OrdItems}, true, 36, function(Resp){
					return PrintToPdfResponse(frame, Resp, user, Ip, res);
				});
			});
		});
	});
} /* ProcInvoiceToPdf */

function ProcPdfLoaded(req, res){
	let Ip = html.GetHeaderIpAddr(req);
	let user = req.session.username;
	let Data = null;

	let frame = {
		PrintToPDF: html.GetPage('PrintToPDF')
	}

	glob('./public/pdf/' + req.query.id, function(err, files){
		if(err){
			console.log('Error while glob | ' + err);
			return PrintToPdfResponse(frame, {File: req.query.id}, user, Ip, res);
		}

		if(files.length > 0){
			return PrintToPdfResponse(frame, {Loaded: '/pdf/' + req.query.id}, user, Ip, res);
		}

		return PrintToPdfResponse(frame, {File: req.query.id}, user, Ip, res);
	});
} /* ProcPdfLoaded */

function ExtractScannedTote(req, res){
	let form = new formidable.IncomingForm();

	form.parse(req, function (err, fields, files){
		if(!fields.scannedTote){
			req.session.Err = 'Please scan a valid tote';
		} else {
			req.session.scannedtote = fields.scannedTote;
		}

		html.WEB_CallPageRedirect(res, DefaultRoute);
	});
} /* ExtractScannedTote */

function ExtractScannedSerialOrBarcode(req, res){
	GetScannedSerialOrBarcode(req, function(SerData){
		req.session.serial = SerData;
		html.WEB_CallPageRedirect(res, DefaultRoute);
	});
} /* ExtractScannedSerialOrBarcode */

function ExtractPwScannedSerialOrBarcode(req, res){
	GetScannedPwSerialOrBarcode(req, function(SerData){
		req.session.pwserial = SerData;
		html.WEB_CallPageRedirect(res, DefaultRoute);
	});
} /* ExtractPwScannedSerialOrBarcode */

function ExtractNewCartonsizeData(req, res){
	let form = new formidable.IncomingForm();

	form.parse(req, function (err, fields, files){
		if(!fields.cartonsize){
			req.session.Err = 'No Carton Size Specified';
		} else {
			req.session.changecartonsize = fields.cartonsize;
		}

		html.WEB_CallPageRedirect(res, DefaultRoute);
	});
} /* ExtractNewCartonsizeData */

function ExtractScannedCarton(req, res){
	let form = new formidable.IncomingForm();

	form.parse(req, function (err, fields, files){
		if(!fields.carton){
			req.session.Err = 'Please scan a valid Carton ID';
		} else {
			req.session.carton = fields.carton;
		}

		html.WEB_CallPageRedirect(res, DefaultRoute);
	});
} /* ExtractScannedCarton */

function ExtractScannedDocument(req, res){
	let form = new formidable.IncomingForm();

	form.parse(req, function (err, fields, files){
		if(!fields.document){
			req.session.Err = 'Please scan a valid Document Barcode';
		} else {
			req.session.document = fields.document;
		}

		html.WEB_CallPageRedirect(res, DefaultRoute);
	});
} /* ExtractScannedDocument */

function ExtractPwScannedDocument(req, res){
	let form = new formidable.IncomingForm();

	form.parse(req, function (err, fields, files){
		if(!fields.pwdocument){
			req.session.Err = 'Please scan a valid Document Barcode';
		} else {
			req.session.pwdocument = fields.pwdocument;
		}

		html.WEB_CallPageRedirect(res, DefaultRoute);
	});
} /* ExtractPwScannedDocument */

function ExtractScannedTransporter(req, res){
	let form = new formidable.IncomingForm();

	form.parse(req, function (err, fields, files){
		if(!fields.transporter){
			req.session.Err = 'Please scan a valid Transporter ID';
		} else {
			req.session.transporter = fields.transporter;
		}

		html.WEB_CallPageRedirect(res, DefaultRoute);
	});
} /* ExtractScannedTransporter */

function ExtractSuperCreds(req, res){
	let form = new formidable.IncomingForm();

	form.parse(req, function (err, fields, files){
		if(!fields.uid || !fields.psw){
			req.session.SupervisorErr = 'Invalid supervisor username or password';
		} else {
			req.session.uid = fields.uid;
			req.session.psw = fields.psw;
		}

		return html.WEB_CallPageRedirect(res, DefaultRoute);
	});
} /* ExtractSuperCreds */


/* =========== End Sub Function ===================================*/

/* =========== Endpoints =======================================*/

app.get('/', html.requireLogin, function(req, res){
	let Ip = html.GetHeaderIpAddr(req);
	html.WEB_CallPageRedirect(res, UrlArr[0] + Ip + Dashboard);
});

app.get(DefaultRoute, html.requireLogin, function(req, res){
	packStn.GetStation({'stationIP': html.GetIpAddress(req)}, function(Resp){
		return ProcessResponse(req, res, Resp);
	});
});

app.post(DefaultRoute, html.requireLogin, function(req, res){
	html.WEB_CallPageRedirect(res, DefaultRoute);
});

app.get('/scantote', html.requireLogin, function(req, res){
	html.WEB_CallPageRedirect(res, DefaultRoute);
});

app.post('/scantote', html.requireLogin, function(req, res){
	return ExtractScannedTote(req, res);
});

app.get('/serial', html.requireLogin, function(req, res){
	html.WEB_CallPageRedirect(res, DefaultRoute);
});

app.post('/serial', html.requireLogin, function(req, res){
	return ExtractScannedSerialOrBarcode(req, res);
});

app.get('/changecartonsize', html.requireLogin, function(req, res){
	html.WEB_CallPageRedirect(res, DefaultRoute);
});

app.post('/changecartonsize', html.requireLogin, function(req, res){
	return ExtractNewCartonsizeData(req, res);
});

app.get('/carton', html.requireLogin, function(req, res){
	html.WEB_CallPageRedirect(res, DefaultRoute);
});

app.post('/carton', html.requireLogin, function(req, res){
	return ExtractScannedCarton(req, res);
});

app.get('/reprint', html.requireLogin, function(req, res){
	html.WEB_CallPageRedirect(res, DefaultRoute);
});

app.post('/reprint', html.requireLogin, function(req, res){
	req.session.reprint = true;
	html.WEB_CallPageRedirect(res, DefaultRoute);
});

app.get('/document', html.requireLogin, function(req, res){
	html.WEB_CallPageRedirect(res, DefaultRoute);
});

app.post('/document', html.requireLogin, function(req, res){
	return ExtractScannedDocument(req, res);
});

app.get('/transporter', html.requireLogin, function(req, res){
	html.WEB_CallPageRedirect(res, DefaultRoute);
});

app.post('/transporter', html.requireLogin, function(req, res){
	return ExtractScannedTransporter(req, res);
});

app.get('/resettote', html.requireLogin, function(req, res){
	req.session.resettote = true;
	html.WEB_CallPageRedirect(res, DefaultRoute);
});

app.post('/resettote', html.requireLogin, function(req, res){
	html.WEB_CallPageRedirect(res, DefaultRoute);
});

app.get('/pwserial', html.requireLogin, function(req, res){
	html.WEB_CallPageRedirect(res, DefaultRoute);
});

app.post('/pwserial', html.requireLogin, function(req, res){
	return ExtractPwScannedSerialOrBarcode(req, res);
});

app.get('/checklocconfirm', html.requireLogin, function(req, res){
	html.WEB_CallPageRedirect(res, DefaultRoute + '?wait=1');
});

app.post('/checklocconfirm', html.requireLogin, function(req, res){
	html.WEB_CallPageRedirect(res, DefaultRoute + '?wait=1');
});

app.get('/pwdocument', html.requireLogin, function(req, res){
	html.WEB_CallPageRedirect(res, DefaultRoute);
});

app.post('/pwdocument', html.requireLogin, function(req, res){
	return ExtractPwScannedDocument(req, res);
});

app.get('/resetpwtote', html.requireLogin, function(req, res){
	req.session.resetpwtote = true;
	html.WEB_CallPageRedirect(res, DefaultRoute);
});

app.post('/resetpwtote', html.requireLogin, function(req, res){
	html.WEB_CallPageRedirect(res, DefaultRoute);
});

app.get('/printtopdf', html.requireLogin, function(req, res){
	return ShowPrintToPdfScreen(req, res);
});

app.post('/printtopdf', html.requireLogin, function(req, res){
	return ProcInvoiceToPdf(req, res);
});

app.get('/checkloaded', html.requireLogin, function(req, res){
	html.WEB_CallPageRedirect(res, '/printtopdf');
});

app.post('/checkloaded', html.requireLogin, function(req, res){
	return ProcPdfLoaded(req, res);
});

app.get('/superorverride', html.requireLogin, function(req, res){
	html.WEB_CallPageRedirect(res, DefaultRoute);
});

app.post('/superorverride', html.requireLogin, function(req, res){
	return ExtractSuperCreds(req, res);
});

/* =========== End Endpoints ===================================*/

module.exports = app;
