var express        = require('express');
var mustache       = require('mustache');
var router         = express.Router();

var HtmlLib	       = require('classHtml');
var html           = new HtmlLib();


router.get('/', function (req, res) {
    html.WEB_CallPageRedirect(res, '/');
});

router.get('/', html.requireLogin, function (req, res) {
    html.WEB_CallPageRedirect(res, '/manifesting');
});

module.exports = router