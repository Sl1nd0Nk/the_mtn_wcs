var express        = require('express');
//var mongoose       = require('../node_modules/mongoose');
var mongoose       = require('mongoose');
mongoose.Promise   = require('bluebird');
var fs             = require('fs');
var moment         = require('moment');
var mustache       = require('mustache');
var async          = require('async');
var redis          = require('redis');
var glob           = require('glob');
var formidable     = require("formidable");
var request        = require('request');
var session        = require('client-sessions')
var app            = express.Router();

var Pubclient      = redis.createClient();
var pickListModel = require('pickListModel')
var packStnLib	   = require('classPackStation');
var pickListLib	   = require('classPicklist');
var serialLib 	   = require('classSerial');
var HtmlLib	       = require('classHtml');
var logLib	       = require('classLogging');
var paramLib       = require('classParams');
var PwLib          = require('classPutwallLocs');
var UserLib        = require('classUser');
var prnLib		   = require('classPrinting');
var batchWeightLib	= require('classBatchWeight');


var packStn  = new packStnLib();
var pickList = new pickListLib();
var serls    = new serialLib();
var html     = new HtmlLib();
var log      = new logLib();
var param    = new paramLib();
var PwLocs   = new PwLib();
var sysuser	 = new UserLib();
var prn      = new prnLib();
var batchWeight = new batchWeightLib();
var Parameters = require('parameterModel')




app.get('/addBatch', html.requireLogin, function(req, res){
			var frame = {
				PrintToPDF: html.GetPage('batchWeights')
				}

			
					batchWeight.Find({}, function(Response){
							let Resp = {add1 : true,
								addBatch : Response.batchWeightArr,
							addNewBatch : req.query.addNewBatch,
							deleteBatch : req.query.deleteBatch}

							processResponse(res, req, Resp, frame);

					})
			
		
})


app.get('/editBatch', html.requireLogin, function(req, res){

	var frame = {
				PrintToPDF: html.GetPage('batchWeights')
				}
	batchWeight.FindOne({"_id" : req.query.editBatch}, function(Resp1){
				let batchRecord = Resp1.batchWeight
				batchWeight.Find({}, function(Response){
				let Resp = {addBatch : Response.batchWeightArr,
							editBatch	: batchRecord}

				processResponse(res, req, Resp, frame);
						
			});
	});	

})
app.get('/deleteBatch', html.requireLogin, function(req, res){

	batchWeight.Delete({"_id" : req.query.deleteBatch}, function(Resp){
		html.WEB_CallPageRedirect(res, '/addBatch');
	})
	
})



app.post('/editBatch', html.requireLogin, function(req, res){

	var Ip = html.GetHeaderIpAddr(req);
	var form = new formidable.IncomingForm();

	form.parse(req, function(err, fields, files){
		var user = req.session.username;
		var frame = {
			PrintToPDF: html.GetPage('batchWeights')
		}
		if(fields.startRange < fields.endRange){


		batchWeight.FindOne({"_id" : req.query.recordID}, function(Resp){
			var batchWeightObj = Resp.batchWeight;

			batchWeightObj.RangeStart 	= fields.startRange;
			batchWeightObj.RangeEnd		= fields.endRange,
			batchWeightObj.batchWeight 	= fields.batchWeight

			batchWeight.Find({"RangeStart" : {$lte : fields.endRange}, "RangeEnd" : {$gte : fields.startRange}}, function(Response){
			 

			 if(Response.batchWeightArr){
			 	if(Response.batchWeightArr.length >= 1){
			 		var msg =  "The start range is overlapping";
			 		displayBatchList(msg,res, req, frame);
			 	}else{
			 		batchWeight.Update(batchWeightObj, function(Resp){
						html.WEB_CallPageRedirect(res, '/addBatch');
					})
			 		
			 		
			 	}
			 }
				

			})
		})
	}else{
			var Resp = {Msg: 'The start range cannot be more than the end range',
						addBatch: true}
			processResponse(res, req, Resp, frame);
	}
	})
	
})


app.post('/addBatch', html.requireLogin, function(req, res){
	var Ip = html.GetHeaderIpAddr(req);
	var form = new formidable.IncomingForm();

	form.parse(req, function(err, fields, files){
		var user = req.session.username;
		var frame = {
			PrintToPDF: html.GetPage('batchWeights')
		}

		var batchWeightObj = {
			add1 :true,
			RangeStart	: fields.startRange,
			RangeEnd	: fields.endRange,
			batchWeight : fields.batchWeight
		}
		
		let Stack = {};

		Stack.paramArr = function(callback){
				param.Find({}, function(Resp){
					return callback(Resp.Err, Resp.paramArr);
				})
		}


		Stack.batchWeight = function(callback){
			batchWeight.Find({"RangeStart" : {$lte : fields.endRange}, "RangeEnd" : {$gte : fields.startRange}}, function(Response){
				return callback(Response.Err, Response.batchWeightArr)
			})
		}

		async.parallel(Stack, function(err, Result){
			if(err){
				Result.Msg = 'Internal Server Error';
				console.log(Result.Msg )
			} else {
				checkCorrectRange(res, req, frame, batchWeightObj, Result)
			}
		});
		

	})

})



function checkCorrectRange(res, req, frame, values, Resp){
	var startRange  = values.RangeStart;
	var endRange 	= values.RangeEnd;
	var returnValue = false;
	var returnValue1 = false;
		var msg = null;
			let x = 0;
			let y = 0;
			while(x < Resp.paramArr.length){
				let weightSettings = Resp.paramArr[x].weightSettings;
				while(y < weightSettings.length){
					if(((endRange.substring(0, 1) == weightSettings[y].StockSize)|| (startRange.substring(0, 1) ==  weightSettings[y].StockSize)) && (weightSettings[y].isCarton == false) ){	
						 msg =  "The entered range is invalid";
						returnValue =  false;
						break;
					}else{
											
						returnValue =  true;
							
					}
					y++;
				}
				x++;
			}

			if(Resp.batchWeight){
				if(Resp.batchWeight.length > 0){
					msg =  "The start range is overlapping";
				}
			}
				

				if((msg == null)){
					batchWeight.New(values, function(Resp){
						html.WEB_CallPageRedirect(res, '/addBatch');
						})
					}else{
						displayBatchList(msg,res, req, frame);	
						msg = null;
					}
		 	
}


function displayBatchList(msg,res, req, frame){
	batchWeight.Find({}, function(Response){
			var Resp = {Msg: msg ,
						add1 : true,
						addBatch : Response.batchWeightArr};
			processResponse(res, req, Resp, frame);
	})
}



function processResponse(res,req,Resp, frame){
	var Ip = html.GetHeaderIpAddr(req);
	var user = req.session.username;

	var Data = html.ProcessPdfData(Resp, user, Ip);
	var page = html.GetPage(null);
	var htmlpage = mustache.render(page, Data, frame);
	res.send(htmlpage);
}
module.exports = app;