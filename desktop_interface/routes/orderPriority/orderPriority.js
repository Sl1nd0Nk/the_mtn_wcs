var express        = require('express');
//var mongoose       = require('../node_modules/mongoose');
var mongoose       = require('mongoose');
mongoose.Promise   = require('bluebird');
var fs             = require('fs');
var moment         = require('moment');
var mustache       = require('mustache');
var async          = require('async');
var redis          = require('redis');
var glob           = require('glob');
var formidable     = require("formidable");
var request        = require('request');
var session        = require('client-sessions')
var app            = express.Router();

var Pubclient      = redis.createClient();
var pickListModel = require('pickListModel')
var packStnLib	   = require('classPackStation');
var pickListLib	   = require('classPicklist');
var serialLib 	   = require('classSerial');
var HtmlLib	       = require('classHtml');
var logLib	       = require('classLogging');
var paramLib       = require('classParams');
var PwLib          = require('classPutwallLocs');
var UserLib        = require('classUser');
var prnLib		   = require('classPrinting');
var batchWeightLib	= require('classBatchWeight');


var packStn  = new packStnLib();
var pickList = new pickListLib();
var serls    = new serialLib();
var html     = new HtmlLib();
var log      = new logLib();
var param    = new paramLib();
var PwLocs   = new PwLib();
var sysuser	 = new UserLib();
var prn      = new prnLib();
var batchWeight = new batchWeightLib();
var Parameters = require('parameterModel')


// process.on('uncaughtException', function(err) {
//     console.log('Caught exception: ' + err);
//     throw err;
// });

// app.use(session({
//   cookieName: 'session',
//   secret: 'eg[isfd-8yF9-7w2315df{}+Ijsli;;to8',
//   duration: 30 * 60 * 1000,
//   activeDuration: 5 * 60 * 1000,
//   httpOnly: true,
//   secure: true,
//   ephemeral: true
// }));

app.get('/orderPriority', html.requireLogin, function(req, res){
	let count = 0;
	
	var Stack = {};

	Stack.top200 = function(callback){
		pickList.getTop200({"Status" : "NEW", "PickListType" :{$ne: "WCS_MASTER"}, "toteID" : null}, function(Resp1){
			return callback(Resp1.Err, Resp1.pickListArr);
		})
	}
		async.parallel(Stack, function(err, Result){
			if(err){
				Result.Msg = 'Internal Server Error';
				processOrderPriority(Result, req, res);
			} else {
				
				processOrderPriority(Result, req, res)
			}
		});


});

function processOrderPriority(orderPriorityArr, req, res){
		var Ip = html.GetHeaderIpAddr(req);
		var frame = {
		PrintToPDF: html.GetPage('orderPriority')
		}	
	if(orderPriorityArr.top200){
			let plRecord = orderPriorityArr.top200;
			let singleRecord = 	req.query.selectedPickList;
			
			var success = req.session.success;
			var msg = orderPriorityArr.top200;
								
			let Resp = {orderPriority1:true,
						orderPriority : plRecord,
						singleRecord : singleRecord,
						PicklistID : req.query.selectedPickList,
						req : req,
						res : res,
						Msg : msg,
						success : success};
				delete req.session.singleRecord;	
				delete req.session.PicklistID;	
				delete req.session.msg;
				delete req.session.success;
			processResponse(res, req, Resp, frame);

	}else if(orderPriorityArr.findOnePicklist){

		let a = 0;
		let plRecord = orderPriorityArr.findOnePicklist;
		while(a < plRecord.orders.length){
			if(plRecord.orders[a].pickListID == req.query.selectedPickList){
					var add_minutes =  function (dt, minutes) {
						return new Date(dt.getTime() + (minutes * 60000));
					}
					var q = 0;
					var paramsData = orderPriorityArr.getParameters;
					while(q < paramsData.orderPrioritySettings.length){
						if(paramsData.orderPrioritySettings[q].priorityLevel == orderPriorityArr.newPriority){
							 	var minutes = ((orderPriorityArr.newPriority) * paramsData.orderPrioritySettings[q].priorityDelta);
				   				var startTime = add_minutes(moment(plRecord.CreateDate,"YYYY-MM-DD HH:mm"), minutes); 
				   				plRecord.startTime = moment(startTime, "YYYY-MM-DD HH:mm");
				   				plRecord.priorityLevel = orderPriorityArr.newPriority;

				   				pickList.Update(plRecord, function(Resp){
									if(Resp.Err){
										req.session.msg = Resp.Err; 
										html.WEB_CallPageRedirect(res, '/orderPriority');
									}else{
										req.session.success = true;
										html.WEB_CallPageRedirect(res, '/orderPriority');
									}	
							});
						}
					q++;
				}
			}
		}
	}else{
		let plRecord = orderPriorityArr.top200;
		let Resp = {orderPriority1:true,
				orderPriority:plRecord}
		processResponse(res, req, Resp, frame);	
}
}


app.post('/orderPriority', html.requireLogin, function(req, res){
	var form = new formidable.IncomingForm();
	var frame = {
			PrintToPDF: html.GetPage('orderPriority')
		}
	let a = 0;
	form.parse(req, function(err, fields, files){
	       pickList.FindOne({"orders.pickListID" : req.query.selectedPickList}, function(Resp1){
		   let plRecord = Resp1.PL;
			while(a < plRecord.orders.length){
			plRecord.priorityLevel = fields.txtNewPriority;   
			 if(plRecord.orders[a].pickListID == req.query.selectedPickList){
				  var add_minutes =  function (dt, minutes) {
				      return new Date(dt.getTime() + (minutes * 60000));
				  }
			     let cursor = Parameters.find({}).cursor();
		             cursor.on('data',function(data){
			       var q = 0;
			          while(q < data.orderPrioritySettings.length){
					if(data.orderPrioritySettings[q].priorityLevel == fields.txtNewPriority){
					     var minutes = (-(fields.txtNewPriority) * data.orderPrioritySettings[q].priorityDelta);
					     var startTime = add_minutes(new Date(plRecord.CreateDate), minutes); 
					     plRecord.startTime = moment(startTime).format("YYYY-MM-DD HH:mm");
		                            // console.log("order priority"+ plRecord.orders.priorityLevel);
					     //plRecord.orders[a].priorityLevel = fields.txtNewPriority;
					     pickList.Update(plRecord, function(Resp){
					     if(Resp.Err){			
					         req.session.msg = Resp.Err; 
						 html.WEB_CallPageRedirect(res, '/orderPriority');
					    }else{	
						req.session.success = true;
				  		html.WEB_CallPageRedirect(res, '/orderPriority');
					    }	
					});
				    }
				q++;
			      }	
			   });
		       }
		    a++;	
		 }
	    });
       });		
 });




function processResponse(res,req,Resp, frame){
	var Ip = html.GetHeaderIpAddr(req);
	var user = req.session.username;

	var Data = html.ProcessPdfData(Resp, user, Ip);
	var page = html.GetPage(null);
	var htmlpage = mustache.render(page, Data, frame);
	res.send(htmlpage);
}

module.exports = app;
