var mongoose       = require('mongoose');
mongoose.Promise   = require('bluebird');
var config         = require('ui');
var moment         = require('moment');

var mongooseSchema = mongoose.Schema;	/* MongoDB Schema class object. */

var params		= require('parameterModel');
var pickList	= require('pickListModel');
var chutesQty   = require('chuteQtyModel');
var packStations = require('packStationModel');

mongoose.connect(config.mongo.url, function (err) {
	if (err) {
		console.error('MONGO CONNECT ERROR ' + err);
	} else {
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' Connected To Mongo');
		WaitForTrigger();
	}
});

function CalcChutes(){
	chutesQty.find({}, function(err, ChutesArr){
		if(err){
			WriteToFile('Failed to find Chutes for Calculation. Err: ' + err);
			WaitForTrigger();
		} else {
			if(ChutesArr.length > 0){
				GetChuteQtyForIndex(ChutesArr, 0, function(){
					WaitForTrigger();
				});
			} else {
				WriteToFile('No Chutes found for Calculation.');
				WaitForTrigger();
			}
		}
	});
} /* CalcChutes */

function GetChuteQtyForIndex(ChutesArr, index, callback){
	if(index < ChutesArr.length){
    	packStations.find({'schuteNumber': ChutesArr[index].chuteNo}, function(error, Stations){
       		if(error || Stations.length <= 0){
         		index++;
         		GetChuteQtyForIndex(ChutesArr, index, callback);
       		} else {
         		let x = 0;
         		let count = 0;
         		let hasAutobag = false
         		while(x < Stations.length){
           			if(Stations[x].active){
             			count++;
           			}

           			if(Stations[x].autobagPrinterQName != null){
             			hasAutobag = true;
           			}
           			x++;
         		}

         		ChutesArr[index].max = 7;
         		ChutesArr[index].putwall = hasAutobag;

         		if(count > 0){
           			ChutesArr[index].active = true;
         		}

         		let ChuteNdx = ChutesArr[index].chuteNo;
         		let StrChute = ChuteNdx.toString();

         		pickList.find({'containerLocation': StrChute}, function(err, Totes){
           			if(err){
             			ChutesArr[index].qty = 0;
           			} else {
             			if(Totes.length > 0) {
               				ChutesArr[index].qty = Totes.length;
             			} else {
               				ChutesArr[index].qty = 0;
             			}
           			}

					ChutesArr[index].save(function(err, savedDoc){
           				index++;
           				GetChuteQtyForIndex(ChutesArr, index, callback);
           			});
         		});
       		}
    	});
	} else {
		return callback();
	}
} /* GetChuteQtyForIndex */

function WaitForTrigger() {
	setTimeout(function () {
		CalcChutes();
	}, 500);
} /* WaitForTrigger */

function WriteToFile(Msg) {
	if (Msg) {
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' ' + Msg);
	}
} /* WriteToFile */
