var mongoose = require('mongoose');
var fs       = require('fs');

var Schema = mongoose.Schema

var rejectSchema = new Schema({
	WaveId		 : { type: String, default: null},
  	Barcode 	 : { type: String, required: true},
  	Reason  	 : { type: String, default: null},
  	Checked  	 : { type: Boolean, default: false},
  	CheckedBy	 : { type: String, default: null},
  	CreateDate   : { type: String, default: moment(new Date()).format('YYYY-MM-DD HH:mm:ss:SSS')}
}, { collection: 'Reject_Backup_2020_03_18'});

var reject = mongoose.model('Reject_Backup_2020_03_18', rejectSchema)

mongoose.connect('mongodb://localhost:27017/ConnXionDSV', function (error, client) {
    if (error) {
        console.error(ProcName, 'MongoDB: Connection Error: ' + error);
    } else {
        console.log(ProcName, 'MongoDB: Connection Established.');
		GetRejectData();
    }
}); /* Mongo Connection */

function GetRejectData(){
	var CurrWaveId = null;
	var Filename = null;

	var cursor = reject.find({}).sort({'WaveId': 1}).cursor();

	cursor.on('data', function(Rec){
		if(Rec.WaveId){
			cursor.pause();

			if(!CurrWaveId || CurrWaveId != Rec.WaveId){
				CurrWaveId = Rec.WaveId;

				Filename = 'REJECT_' + Rec.WaveId + '.csv';

				console.log('New File: ' + Filename);

				let FileHeader = "WaveId,Barcode,Reason\n";
				fs.appendFile(Filename, FileHeader, function (err) {
					if (err){
						console.log(err);
					} else {
						let Data = Rec.WaveId + ',' + Rec.Barcode + ',' + Rec.Reason + '\n';
						fs.appendFile(Filename, Data, function (err) {
							if (err){
								console.log(err);
							}

							cursor.resume();
						});
					}
				});
			} else {
				let Data = Rec.WaveId + ',' + Rec.Barcode + ',' + Rec.Reason + '\n';
				fs.appendFile(Filename, Data, function (err) {
					if (err){
						console.log(err);
					}

					cursor.resume();
				});
			}
		}
	}

	cursor.on('end', function(){
		console.log("Finished");
	}
} /* GetRejectData */