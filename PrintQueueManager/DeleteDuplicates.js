var mongoose       = require('mongoose');
mongoose.Promise = require('bluebird');
var async          = require('async');

var pickListLib	   = require('classPicklist');

var pickList = new pickListLib();

function CheckOrdUniq(task, callback){
	let pickListID = task.pickListID;

	pickList.Find({'orders.pickListID': pickListID}, function(Resp){
		 if(Resp.Err || !Resp.pickListArr || Resp.pickListArr.length <= 0){
			 return callback({unique: false, Err: 'Failed to check picklist for uniqueness: ' + err});
		 } else {
			 let PickLists = Resp.pickListArr;
			 let Total = 0;
			 let x = 0;

			 while(x < PickLists.length){
				 let y = 0;
				 while(y < PickLists[x].orders.length){
					 if(PickLists[x].orders[y].pickListID == pickListID){
						 Total++;
					 }
					 y++;
				 }
				 x++;
			 }

			 if(Total > 1){
			   	return callback(null,{unique: false, Msg: 'Order ' + task.orderID + ' in tote is a duplicate', Order: task.orderID});
			 } else {
				return callback(null, {unique: true, Msg: null});
			 }
		 }
	 });
} /* CheckOrdUniq */

function CheckIfPickListIsUnique(doc, index, callback){
	var qCheckOrdUniq = async.queue(CheckOrdUniq, doc.orders.length);
	let Msg = null;
	let DupArr = [];

	qCheckOrdUniq.push(doc.orders, function (err, data) {
		if(!Msg){
			if(!data.unique){
				Msg = data.Msg;
				DupArr.push(data.Order);
			}
		}
	});

	qCheckOrdUniq.drain = function(){
		if(Msg){
			return callback({unique: false, Msg: Msg, DupArr: DupArr});
		} else {
			return callback({unique: true, Msg: null});
		}
	}

} /* CheckIfPickListIsUnique */

function DeleteOrdersFound(PL, DupArr, ndx, callback){
	if(ndx < DupArr.length){
		let y = 0;
		while(y < PL.orders.length){
			if(PL.orders[y].orderID == DupArr[ndx]){
				PL.orders.splice(y, 1);
				break;
			}
			y++;
		}

		ndx++;
		DeleteOrdersFound(PL, DupArr, ndx, callback);
	} else {
		if(PL.orders){
			if(PL.orders.length <= 0){
				pickList.Delete({'toteID': PL.toteID}, function(){
					return callback();
				});
			} else {
				pickList.Update(PL, function(Resp){
					return callback();
				});
			}
		} else {
			return callback();
		}
	}
} /* DeleteOrdersFound */

function processEach(Arr, index, callback){
	if(index < Arr.length){
		let Tote = Arr[index];

		if(Tote[0] == '0' || Tote[0] == '1' && Tote.length == 7){
			console.log(Tote);
			pickList.FindOne({'toteID':Tote}, function(Resp){
				if(Resp.Err){
					if(Resp.Err == 'Not Found'){
						console.log('Scanned tote is empty');
					} else {
						console.log('Failed to find tote on the system, call administrator');
					}

					index++;
					processEach(Arr, index, callback);
				} else {
					let PL = Resp.PL;
					CheckIfPickListIsUnique(PL, 0, function(Resp){
						if(Resp.Err){
							console.log(Resp.Err);
						} else {
							if(Resp.DupArr){
								if(Resp.DupArr.length > 0){
									DeleteOrdersFound(PL, Resp.DupArr, 0, function(){
										console.log('Done with tote ' + PL.toteID);
										index++;
										processEach(Arr, index, callback);
									});
								} else {
									console.log('No Duplicate found B');
									index++;
									processEach(Arr, index, callback);
								}
							} else {
								console.log('No Duplicate found');
								index++;
								processEach(Arr, index, callback);
							}
						}
					});
				}
			});
		} else {
			console.log('Skipping Number: ' + Tote + ' from list as its not valid');
			index++;
			processEach(Arr, index, callback);
		}
	} else {
		return callback();
	}
} /* processEach */

function ClearDuplicates(CartonArr, callback){
	var Arr = [];

	var ndx = 2;
	while(ndx < CartonArr.length){
	   Arr.push(CartonArr[ndx]);
	   ndx++;
	}

	if(Arr.length > 0){
		processEach(Arr, 0, function(){
			return callback();
		});
	} else {
		return callback();
	}
} /* ClearDuplicates */

mongoose.connect('mongodb://localhost/MTNWCS', function(err){
	if(err){
		console.error( 'MONGO CONNECT ERROR ' + err);
	} else {
		console.log('Connected To Mongo');

		console.log("Entered Carton: " + process.argv[2])
		ClearDuplicates(process.argv, function(){
			console.log('Done');
		});
	}
}); /* mongoose.connect */