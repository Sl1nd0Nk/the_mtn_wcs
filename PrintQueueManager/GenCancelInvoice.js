var fs = require('fs');
var replace = require('replace');
var LineByLineReader = require('line-by-line');
var config = require('printing');
var redis = require('redis');
var exec = require('child_process').spawn;
var rest = require('restler');
var moment = require('moment');

var Subclient = redis.createClient();
var Channel = 'CANCEL_QUEUE';

console.log("Channel argument: " + Channel)
console.log("Template Directory: " + process.env.INVOICE_TEMPLATE_DIR)
console.log("Template Out Directory: " + process.env.INVOICE_TMP_DIR)
console.log("Pdf Out Directory: " + process.env.PDFDIR)

var Templatedir = process.env.INVOICE_TEMPLATE_DIR;
var TempOutDir = process.env.INVOICE_TMP_DIR;

if(!Templatedir || Templatedir === 'undefined' || Templatedir == null){
  Templatedir = '/apps/wcs/source/services/invoice_templates/';
  console.log("Template Directoryi adjusted to : " + Templatedir);

}

if(!TempOutDir || TempOutDir === 'undefined' || TempOutDir == null){
  TempOutDir = '/apps/wcs/source/services/log_files/';
  console.log("Template Out Directory adjusted to : " + TempOutDir);

}

console.log("Current vat: " + config.Printout.currentVat);
console.log("New vat: " + config.Printout.newVat);
console.log("New Vat Effective Date: " + config.Printout.NewVatEffectiveDate);

WmsEndPoint = config.WMS.url;
ProcessEnv = config.Printout.ENV;

Subclient.subscribe(Channel);

var SectionReplace = false;
var Section = [];
var Coordinates = null;
var BigTotal = 0;

var Keywords = [
    '{vat_no}',
    '({barcode})',
    '({invoice_number})',
    '({due_date})',
    '({customer_account})',
    '({customer_order})',
    '({invoice_date})',
    '({mtn_ref})',
    '({courier_order})',
    '({terms})',
    '{pgnum}',
    '{pgnumtot}',
    '{ord_barcode}'
];

function convertBrackets(data) {
    var result = "";
    var i, j = 0;

    if (typeof data != 'undefined') {
        if (data) {
            for (i = 0; i < data.length; i++) {
                if ((data[i] == '(') ||
                    (data[i] == ')') || (data[i] == '\\')) {
                    result = result + '\\';
                    j++;
                }

                result = result + data[i];
                j++;
            }
        }
    }

    return result;
} /* convertBrackets */

function GenerateAddressData(HeaderData) {
    var x = 0;
    var Vals = [];

    /*if(HeaderData.BILLTO != null && HeaderData.BILLTO != "")
    {
      Vals[x++] = convertBrackets(HeaderData.BILLTO);
    }*/

    //var HeaderData = JSON.parse(sHeaderData);

    if (HeaderData.orderType == 'Corporate') {
        if (HeaderData.billTo.contact1Name != null && HeaderData.billTo.contact1Name != "") {
            Vals[x++] = convertBrackets(HeaderData.billTo.contact1Name);
        }
    }

    if (HeaderData.billTo.street1 != null && HeaderData.billTo.street1 != "") {
        if (HeaderData.billTo.street1.length > 38) {
            var pos = 0;
            var Remaining = HeaderData.billTo.street1.length - 38;

            var tmp = HeaderData.billTo.street1;
            var tmp1 = tmp.substring(pos, 38);
            pos += 38;

            Vals[x++] = convertBrackets(tmp1);

            while (Remaining > 0) {
                y = 0;
                tmp = HeaderData.billTo.street1;
                if (Remaining > 38)
                    tmp1 = tmp.substring(pos, pos + 38);
                else
                    tmp1 = tmp.substring(pos, HeaderData.billTo.street1.length);

                pos += 38;
                Remaining = Remaining - 38;
                Vals[x++] = convertBrackets(tmp1);
            }
        } else
            Vals[x++] = convertBrackets(HeaderData.billTo.street1);
    }

    if (HeaderData.billTo.street2 != null && HeaderData.billTo.street2 != "") {
        if (HeaderData.billTo.street2.length > 38) {
            var pos = 0;
            var Remaining = HeaderData.billTo.street2.length - 38;

            var tmp = HeaderData.billTo.street2;
            var tmp1 = tmp.substring(pos, pos + 38);
            pos += 38;

            Vals[x++] = convertBrackets(tmp1);

            while (Remaining > 0) {
                y = 0;
                tmp = HeaderData.billTo.street2;
                if (Remaining > 38)
                    tmp1 = tmp.substring(pos, pos + 38);
                else
                    tmp1 = tmp.substring(pos, HeaderData.billTo.street2.length);

                pos += 38;
                Remaining = Remaining - 38;
                Vals[x++] = convertBrackets(tmp1);
            }
        } else
            Vals[x++] = convertBrackets(HeaderData.billTo.street2);
    }

    if (HeaderData.billTo.street3 != null && HeaderData.billTo.street3 != "") {
        if (HeaderData.billTo.street3.length > 38) {
            var pos = 0;
            var Remaining = HeaderData.billTo.street3.length - 38;

            var tmp = HeaderData.billTo.street3;
            var tmp1 = tmp.substring(pos, pos + 38);
            pos += 38;

            Vals[x++] = convertBrackets(tmp1);

            while (Remaining > 0) {
                y = 0;
                tmp = HeaderData.billTo.street3;
                if (Remaining > 38)
                    tmp1 = tmp.substring(pos, pos + 38);
                else
                    tmp1 = tmp.substring(pos, HeaderData.billTo.street3.length);

                pos += 38;
                Remaining = Remaining - 38;
                Vals[x++] = convertBrackets(tmp1);
            }
        } else
            Vals[x++] = convertBrackets(HeaderData.billTo.street3);
    }

    if (HeaderData.billTo.street4 != null && HeaderData.billTo.street4 != "") {
        if (HeaderData.billTo.street4.length > 38) {
            var pos = 0;
            var Remaining = HeaderData.billTo.street4.length - 38;

            var tmp = HeaderData.billTo.street4;
            var tmp1 = tmp.substring(pos, pos + 38);
            pos += 38;

            Vals[x++] = convertBrackets(tmp1);

            while (Remaining > 0) {
                y = 0;
                tmp = HeaderData.billTo.street4;
                if (Remaining > 38)
                    tmp1 = tmp.substring(pos, pos + 38);
                else
                    tmp1 = tmp.substring(pos, HeaderData.billTo.street4.length);

                pos += 38;
                Remaining = Remaining - 38;
                Vals[x++] = convertBrackets(tmp1);
            }
        } else
            Vals[x++] = convertBrackets(HeaderData.billTo.street4);
    }

    if (HeaderData.billTo.city != null && HeaderData.billTo.city != "") {
        Vals[x++] = convertBrackets(HeaderData.billTo.city);
    }

    if (HeaderData.billTo.zip != null && HeaderData.billTo.zip != "") {
        Vals[x++] = convertBrackets(HeaderData.billTo.zip);
    }

    if (HeaderData.billTo.contact1Name != null && HeaderData.billTo.contact1Name != "") {
        Vals[x++] = convertBrackets(HeaderData.billTo.contact1Name);
    }

    if (HeaderData.billTo.contact1Email != null && HeaderData.billTo.contact1Email != "") {
        Vals[x++] = convertBrackets(HeaderData.billTo.contact1Email);
    }

    x = x - 1;
    return Vals;
} /* GenerateAddressData */

function GenerateInvAddressData(HeaderData) {
    var x = 0;
    var Vals = [];

    if (HeaderData.orderType == 'Corporate') {
        if (HeaderData.shipTo.contact1Name != null && HeaderData.shipTo.contact1Name != "") {
            Vals[x++] = convertBrackets(HeaderData.shipTo.contact1Name);
        }
    }

    if (HeaderData.shipTo.street1 != null && HeaderData.shipTo.street1 != "") {
        if (HeaderData.shipTo.street1.length > 38) {
            var pos = 0;
            var Remaining = HeaderData.shipTo.street1.length - 38;

            var tmp = HeaderData.shipTo.street1;
            var tmp1 = tmp.substring(pos, pos + 38);
            pos += 38;

            Vals[x++] = convertBrackets(tmp1);

            while (Remaining > 0) {
                y = 0;
                tmp = HeaderData.shipTo.street1;
                if (Remaining > 38)
                    tmp1 = tmp.substring(pos, pos + 38);
                else
                    tmp1 = tmp.substring(pos, HeaderData.shipTo.street1.length);

                pos += 38;
                Remaining = Remaining - 38;
                Vals[x++] = convertBrackets(tmp1);
            }
        } else
            Vals[x++] = convertBrackets(HeaderData.shipTo.street1);
    }

    if (HeaderData.shipTo.street2 != null && HeaderData.shipTo.street2 != "") {
        if (HeaderData.shipTo.street2.length > 38) {
            var pos = 0;
            var Remaining = HeaderData.shipTo.street2.length - 38;

            var tmp = HeaderData.shipTo.street2;
            var tmp1 = tmp.substring(pos, pos + 38);
            pos += 38;

            Vals[x++] = convertBrackets(tmp1);

            while (Remaining > 0) {
                y = 0;
                tmp = HeaderData.shipTo.street2;
                if (Remaining > 38)
                    tmp1 = tmp.substring(pos, pos + 38);
                else
                    tmp1 = tmp.substring(pos, HeaderData.shipTo.street2.length);

                pos += 38;
                Remaining = Remaining - 38;
                Vals[x++] = convertBrackets(tmp1);
            }
        } else
            Vals[x++] = convertBrackets(HeaderData.shipTo.street2);
    }

    if (HeaderData.shipTo.street3 != null && HeaderData.shipTo.street3 != "") {
        if (HeaderData.shipTo.street3.length > 38) {
            var pos = 0;
            var Remaining = HeaderData.shipTo.street3.length - 38;

            var tmp = HeaderData.shipTo.street3;
            var tmp1 = tmp.substring(pos, pos + 38);
            pos += 38;

            Vals[x++] = convertBrackets(tmp1);

            while (Remaining > 0) {
                y = 0;
                tmp = HeaderData.shipTo.street3;
                if (Remaining > 38)
                    tmp1 = tmp.substring(pos, pos + 38);
                else
                    tmp1 = tmp.substring(pos, HeaderData.shipTo.street3.length);

                pos += 38;
                Remaining = Remaining - 38;
                Vals[x++] = convertBrackets(tmp1);
            }
        } else
            Vals[x++] = convertBrackets(HeaderData.shipTo.street3);
    }

    if (HeaderData.shipTo.street4 != null && HeaderData.shipTo.street4 != "") {
        if (HeaderData.shipTo.street4.length > 38) {
            var pos = 0;
            var Remaining = HeaderData.shipTo.street4.length - 38;

            var tmp = HeaderData.shipTo.street4;
            var tmp1 = tmp.substring(pos, pos + 38);
            pos += 38;

            Vals[x++] = convertBrackets(tmp1);

            while (Remaining > 0) {
                y = 0;
                tmp = HeaderData.shipTo.street4;
                if (Remaining > 38)
                    tmp1 = tmp.substring(pos, pos + 38);
                else
                    tmp1 = tmp.substring(pos, HeaderData.shipTo.street4.length);

                pos += 38;
                Remaining = Remaining - 38;
                Vals[x++] = convertBrackets(tmp1);
            }
        } else
            Vals[x++] = convertBrackets(HeaderData.shipTo.street4);
    }

    if (HeaderData.shipTo.city != null && HeaderData.shipTo.city != "") {
        Vals[x++] = convertBrackets(HeaderData.shipTo.city);
    }

    if (HeaderData.shipTo.zip != null && HeaderData.shipTo.zip != "") {
        Vals[x++] = convertBrackets(HeaderData.shipTo.zip);
    }

    if (HeaderData.shipTo.contact1Name != null && HeaderData.shipTo.contact1Name != "") {
        Vals[x++] = convertBrackets(HeaderData.shipTo.contact1Name);
    }

    if (HeaderData.shipTo.contact1Email != null && HeaderData.shipTo.contact1Email != "") {
        Vals[x++] = convertBrackets(HeaderData.shipTo.contact1Email);
    }

    x = x - 1;
    return Vals;
} /* GenerateAddressData */

function GenerateReplaceableData(HeaderData, OrderData, PageNo, NumberOfPages) {
    var ReplaceVals = [];
    var vat_no = HeaderData.custVatNo;

    if (!vat_no) {
        vat_no = "";
    } else {
        vat_no = "Cust VAT Number: " + vat_no;
    }
    ReplaceVals[0] = vat_no;
    ReplaceVals[1] = HeaderData.invoiceNumber;
    ReplaceVals[2] = HeaderData.invoiceNumber;

    var valueAddedTax = 0.14;

    var PackedOrderDate = OrderData.PackedDate;

    if (PackedOrderDate && PackedOrderDate != null) {
        ReplaceVals[3] = convertBrackets(moment(OrderData.PackedDate).format('YYYY-MM-DD'));
    } else {
        ReplaceVals[3] = convertBrackets(moment(new Date()).format('YYYY-MM-DD'));

        PackedOrderDate = moment(new Date()).format('YYYY-MM-DD');
        console.log("Today's Date: " + PackedOrderDate);
    }

    valueAddedTax = DateCompare(PackedOrderDate);

    ReplaceVals[4] = convertBrackets(HeaderData.customerAccount);

    if(HeaderData.orderType == 'RETAIL' || HeaderData.orderType == 'RETAILNW'){
        let RefOrd = HeaderData.referenceOrd;

		if(!RefOrd){
		   RefOrd = '';
		}

        ReplaceVals[5] = convertBrackets(RefOrd);
    } else {
    	ReplaceVals[5] = convertBrackets(HeaderData.customerOrderNo);
    }

    ReplaceVals[6] = convertBrackets(moment(new Date()).format('YYYY-MM-DD'));

    ReplaceVals[7] = convertBrackets(OrderData.orderId);
    ReplaceVals[8] = convertBrackets(OrderData.courierId);
    ReplaceVals[9] = '';
    ReplaceVals[10] = PageNo;

    if (OrderData.Reprint)
        ReplaceVals[11] = NumberOfPages + ' Reprint';
    else
        ReplaceVals[11] = NumberOfPages;

    ReplaceVals[12] = convertBrackets(OrderData.orderId);

    return ReplaceVals;
} /* GenerateReplaceableData */

function DateCompare(OrderPackedDate) {
    var PackedDate = moment(OrderPackedDate, 'YYYY-MM-DD');
    var VatChangeDate = moment(config.Printout.NewVatEffectiveDate, 'YYYY-MM-DD');
    console.log('Vat date: ' + VatChangeDate + ' === order date: ' + PackedDate);
    if (VatChangeDate > PackedDate) {
        console.log('==== Using old vat info =======');
        return config.Printout.currentVat;
    } else {
        console.log('==== Using new vat info =======');
        return config.Printout.newVat;
    }
}

function GenerateItemValues(PackListData) {
    var ItemVals = [];
    var LineItem = [];

    var lineIndex = 0;
    var x = 0;
    while (x < PackListData.items.length) {
        var QPacked = PackListData.items[x].qtyPacked;
        if (QPacked != null && QPacked > 0) {
            var y = 0;
            var ndx = 0;
            LineItem[y++] = convertBrackets(PackListData.Cartons[x]);
            LineItem[y++] = PackListData.items[x].qtyPacked;

            if (PackListData.items[x].itemCode.length > 40) {
                var pos = 0;
                var Remaining = PackListData.items[x].itemCode.length - 39;

                var tmp = PackListData.items[x].itemCode;
                var tmp1 = tmp.substring(pos, pos + 39);
                pos += 40;

                LineItem[y++] = convertBrackets(tmp1);
                LineItem[y++] = convertBrackets(PackListData.items[x].uom);

                /*if (PackListData.items[x].serials.length > 0) {
                    LineItem[y] = convertBrackets(PackListData.items[x].serials[ndx++]);
                } else {
                    LineItem[y] = '';
                }*/
                ItemVals[lineIndex] = LineItem;
                LineItem = [];
                lineIndex++;

                while (Remaining > 0) {
                    y = 0;
                    LineItem[y++] = '';
                    LineItem[y++] = '';
                    tmp = PackListData.items[x].itemCode;
                    if (Remaining > 40)
                        tmp1 = tmp.substring(pos, pos + 39);
                    else
                        tmp1 = tmp.substring(pos, PackListData.items[x].itemCode.length);

                    pos += 40;
                    Remaining = Remaining - 40;
                    LineItem[y++] = convertBrackets(tmp1);
                    LineItem[y++] = '';

                    /*if (ndx < PackListData.items[x].serials.length) {
                        LineItem[y] = convertBrackets(PackListData.items[x].serials[ndx++]);
                    } else
                        LineItem[y] = '';*/

                    ItemVals[lineIndex] = LineItem;
                    LineItem = [];
                    lineIndex++;
                }

                /*if (ndx < PackListData.items[x].serials.length) {
                    while (ndx < PackListData.items[x].serials.length) {
                        y = 0;
                        LineItem[y++] = '';
                        LineItem[y++] = '';
                        LineItem[y++] = '';
                        LineItem[y++] = '';
                        LineItem[y] = convertBrackets(PackListData.items[x].serials[ndx++]);

                        ItemVals[lineIndex] = LineItem;
                        LineItem = [];
                        lineIndex++;
                    }
                }*/
            } else {
                LineItem[y++] = convertBrackets(PackListData.items[x].itemCode);
                LineItem[y++] = convertBrackets(PackListData.items[x].uom);

                /*if (PackListData.items[x].serials.length > 0) {
                    LineItem[y] = convertBrackets(PackListData.items[x].serials[ndx++]);
                } else {
                    LineItem[y] = '';
                }*/

                ItemVals[lineIndex] = LineItem;
                LineItem = [];
                lineIndex++;

                /*if (ndx < PackListData.items[x].serials.length) {
                    while (ndx < PackListData.items[x].serials.length) {
                        y = 0;
                        LineItem[y++] = '';
                        LineItem[y++] = '';
                        LineItem[y++] = '';
                        LineItem[y++] = '';
                        LineItem[y] = convertBrackets(PackListData.items[x].serials[ndx++]);

                        ItemVals[lineIndex] = LineItem;
                        LineItem = [];
                        lineIndex++;
                    }
                }*/

            }
        }
        x++;
    }

    lineIndex--;

    return ItemVals;

} /* GenerateItemValues */


function ProcessLineDelInfo(line, DestFile, Print, DelKey, DelVals, DelIndex) {
    var Print = Print;
    var x = 0;
    var tmpLine = line;
    var res = line.split(' ');
    if (res[0] == DelKey) {
        while (x < DelIndex) {
            var i = 0;

            line = Coordinates + x + ' 12 mul add moveto\n';
            line = line + '(' + DelVals[x] + ')';
            var j = 1;
            while (j < res.length) {
                line = line + ' ' + res[j];
                j++;
            }

            fs.appendFileSync(DestFile, line + '\n');
            line = tmpLine;
            Print = false;

            x++;
        }
    }

    return Print;
} /* ProcessLineDelInfo */

function ProcessLineInfo(line, DestFile, Print, index, ItemVals, StartLine, EndLine, ItemKeys) {
    var Print = Print;
    var x = 0;
    var itr = StartLine;
    var tmpLine = line;
    while (x < EndLine) {
        //console.log('replacing line');
        var res = line.split(' ');
        var i = 0;
        while (i < ItemKeys.length) {
            if (res[0] == ItemKeys[i]) {
                line = Coordinates + x + ' 15 mul add moveto\n';
                line = line + '(' + ItemVals[itr][i] + ')';
                var j = 1;
                while (j < res.length) {
                    line = line + ' ' + res[j];
                    j++;
                }

                if (res[res.length - 1] == 'rmoveto') {
                    line = line + '\n(' + ItemVals[itr][i] + ') show';
                }

                fs.appendFileSync(DestFile, line + '\n');

                line = tmpLine;
                Print = false;
                break;
            }
            i++;
        }
        x++;
        itr++;
    }

    return Print;
} /* ProcessLineInfo */

function ProcessLineInvInfo(line, DestFile, Print, InvKey, InvVals, InvIndex) {
    var Print = Print;
    var x = 0;
    var tmpLine = line;
    var res = line.split(' ');
    if (res[0] == InvKey) {
        while (x < InvIndex) {
            var i = 0;

            line = Coordinates + x + ' 12 mul add moveto\n';
            line = line + '(' + InvVals[x] + ')';
            var j = 1;
            while (j < res.length) {
                line = line + ' ' + res[j];
                j++;
            }

            fs.appendFileSync(DestFile, line + '\n');
            line = tmpLine;
            Print = false;

            x++;
        }
    }

    return Print;
} /* ProcessLineInvInfo */

function ProcessOutputFile(SourceFile, Keywords, ReplaceVals, index, ItemVals, Data, StartLine, EndLine, DelKey, DelVals, DelIndex, InvKey, InvVals, InvIndex, ItemKeys, TaxInvoice, HeaderData, Page, NumberOfPages, MaxNoLinesPerPage, callback) {
    if (Page == NumberOfPages) {
        EndLine = index - StartLine;
    }

    var DestFile = TempOutDir + Data.orderId + '_' + Page + '.ps';

    console.log("Start:" + new Date().toISOString());
    const stream = fs.createWriteStream(DestFile);

    var lr = new LineByLineReader(SourceFile);
    lr.on('error', function(err) {
        console.log(err);
    });

    lr.on('line', function(line) {
        var Print = true;
        lr.pause();

        if (SectionReplace && Section.length > 1) {
            var BreakAt = line.split('$$');
            if (BreakAt.length == 2) {
                Coordinates = BreakAt[0];
                Print = false;
            }

            switch (Section[Section.length - 1]) {
                case 'line':
                    {
                        Print = ProcessLineInfo(line, DestFile, Print, index, ItemVals, StartLine, EndLine, ItemKeys);
                    }
                    break;
                case 'del_add':
                    {
                        Print = ProcessLineDelInfo(line, DestFile, Print, DelKey, DelVals, DelIndex);
                    }
                    break;
                case 'inv_add':
                    {
                        Print = ProcessLineInvInfo(line, DestFile, Print, InvKey, InvVals, InvIndex);
                    }
                    break;
            }
        }

        var compare = line.substring(0, 7);
        if (compare == 'section') {
            if (line.length > compare.length) {
                SectionReplace = true;
                Section = line.split('$');
            } else if (line.length == compare.length) {
                SectionReplace = false;
                Coordinates = null;
            }
        } else {
            if (Print) {
                fs.appendFileSync(DestFile, line + '\n');
            }
        }

        lr.resume();
    });

    lr.on('end', function() {
        var Max = Keywords.length

        for (var i = 0; i < Max; i++) {
            replace({
				regex: Keywords[i],
				replacement: ReplaceVals[i],
				paths: [DestFile],
				recursive: true,
				silent: true
			});
        }

        stream.close();
        console.log("End:" + new Date().toISOString());

        var child = exec("lpr", ["-P", Data.printerName, "-#2", "-s", "-h", DestFile]);

        child.on('close', function(code) {
            /*try {
            	fs.unlink(DestFile);
			} catch(e){
				console.log('Failed to unlink file ' + DestFile);
			}*/

            StartLine = StartLine + MaxNoLinesPerPage;
            EndLine = MaxNoLinesPerPage;
            Page++;

            if (Page <= NumberOfPages) {
                ReplaceVals = GenerateReplaceableData(HeaderData, Data, Page, NumberOfPages);
                ProcessOutputFile(SourceFile, Keywords, ReplaceVals, index, ItemVals, Data, StartLine, EndLine, DelKey, DelVals, DelIndex, InvKey, InvVals, InvIndex, ItemKeys, TaxInvoice, HeaderData, Page, NumberOfPages, MaxNoLinesPerPage, callback);
            } else {
				return callback();
			}
        });
    });

} /* ProcessOutputFile */

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }

    return true;
} /* isEmpty */

Subclient.on("message", function(channel, message) {
    if (channel == Channel) {
        console.log('Incoming:', message);
        var Data = JSON.parse(message);
        if (!isEmpty(Data)) {
            rest.post(WmsEndPoint, {
                data: { 'orderID': Data.orderId }
            }).on('complete', function(result) {
                if (result instanceof Error) {
                    console.log('Error:', result.message);
                } else {
					var resultCheck = result.indexOf('Error: NO DATA FOUND');

					if (resultCheck <= -1){
						var HeaderData = JSON.parse(result);
						console.log(result);

						ProcessDocumentInvoice(Data, HeaderData);
					} else {
						console.log(result);
					}
                }
            });
        } else {
            console.log("Incorrect data supplied: " + moment(new Date()).format('YYYY-MM-DD HH:mm') + " - channel: " + channel);
        }
    }
});

function ProcessDocumentInvoice(Data, HeaderData) {
    var TaxInvoice = false;

	var DocumentTemplate = Templatedir + 'cancelmanifest.ps';
	var ItemVals = GenerateItemValues(Data);

	var ItemKeys = [
		'({carton_id})',
		'({qty})',
		'({description})',
		'({uom})'
	];

    var MaxNoLinesPerPage = 35;

    var index = ItemVals.length;
    var StartLine = 0;
    var EndLine = index;

    var NumberOfPages = index / MaxNoLinesPerPage;
    NumberOfPages = Math.floor(NumberOfPages);
    var Remainder = index % MaxNoLinesPerPage;

    var Page = 1;

    if (Remainder != 0) { NumberOfPages += 1; }

    var DelKey = '({delivery_address})';
    DelVals = GenerateInvAddressData(HeaderData);
    var DelIndex = DelVals.length;

    var InvKey = '({invoice_address})';
    InvVals = GenerateAddressData(HeaderData);
    var InvIndex = InvVals.length;

    // Should there be more than one page then only create lines to the maximum that can fit on a page
    if (NumberOfPages > 1) {
        EndLine = MaxNoLinesPerPage;
    }

    var ReplaceVals = GenerateReplaceableData(HeaderData, Data, Page, NumberOfPages);

	ProcessOutputFile(DocumentTemplate, Keywords, ReplaceVals, index, ItemVals, Data, StartLine, EndLine, DelKey, DelVals, DelIndex, InvKey, InvVals, InvIndex, ItemKeys, TaxInvoice, HeaderData, Page, NumberOfPages, MaxNoLinesPerPage, function(){
	});

} /* ProcessDocumentInvoice */

