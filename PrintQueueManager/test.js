function BreakLineAtLimit(limit, Line, Arr){
	let pos = 0;
	let x = Arr.length;
	let Index = pos + limit -1;

	Index = GetSpaceIndex(Line, Index);

	let str = Line.substr(pos, (Index+1));
	let Remaining = Line.length - (Index+1);

	Arr[x++] = convertBrackets(str);
	pos += Index;

	while (Remaining > 0) {
		let len = 0;

		if(Remaining > limit){
			console.log('pos ' + pos);
			console.log('limit ' + limit);
			console.log('Remaining ' + Remaining);
			Index = pos + limit -1;
			console.log('Index ' + Index);
			Index = GetSpaceIndex(Line, Index);
			console.log('NewIndex ' + Index);
			len = Index + 1;

			len = len - pos;

			if(len <= 1){
				len = Line.length - pos;
			}

			str = Line.substr(pos, len);
			console.log('len ' + len);
			console.log('pos ' + pos);
			console.log('Gr > ' + str);
			pos += len;
		} else {
			len = Remaining + 1;
			str = Line.substr(pos, len);
		}

		Remaining = Remaining - len;
		Arr[x++] = convertBrackets(str);
	}

	return Arr;
} /* BreakLineAtLimit */

function GetSpaceIndex(Line, Index){
	let lndx = Index;
	while(Line[lndx] != ' '){
		lndx--;
	}

	return lndx;
} /* GetSpaceIndex */

function convertBrackets(data) {
    var result = "";
    var i, j = 0;

    if (typeof data != 'undefined') {
        if (data) {
            for (i = 0; i < data.length; i++) {
                if ((data[i] == '(') ||
                    (data[i] == ')') || (data[i] == '\\')) {
                    result = result + '\\';
                    j++;
                }

                result = result + data[i];
                j++;
            }
        }
    }

    return result;
} /* convertBrackets */

let Vals = [];

Vals = BreakLineAtLimit(38, '70 Aitkenaitkenaitkeneastleighaitkenaitkene Rd', Vals);

console.log(Vals);