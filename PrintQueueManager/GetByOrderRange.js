var redis = require('redis');
var mongoose       = require('mongoose');
mongoose.Promise = require('bluebird');
var moment           = require('moment');
var rest       = require('restler');

var Pubclient = redis.createClient();

var mongooseSchema = mongoose.Schema // MongoDB Schema class object.

var pickList = require('pickListModel')

function SerialUpdateToHost(Data, callback)
{
	var items = [];

  if(Data.orders)
  {
		var x = 0;
		while(x < Data.orders[0].items.length)
		{
			var orderLine = null;

			if(Data.orders[0].items[x].orderLine)
				orderLine = Data.orders[0].items[x].orderLine;

			var lineData =
			{
				'sku': Data.orders[0].items[x].sku,
				'serials': Data.orders[0].items[x].serials,
				'qty': Data.orders[0].items[x].qty,
				'orderLine': orderLine
			}

			items.push(lineData);
			x++;
		}

		rest.postJson('http://10.211.110.130:2021/warehouseExpert/serial/update',
		{
			data:
			{
				'orderID': Data.orders[0].orderID,
				'cartonID': Data.orders[0].cartonID,
				'cartonNo': Data.orders[0].toteNumber,
				'noOfCartons': Data.orders[0].numberOfTotes,
				'items': items
            }
		}).on('complete', function(result)
		{
			if (result instanceof Error)
			{
				console.log('Error:', result.message);
				//this.retry(5000); // try again after 5 sec
                return callback();
			}
			else
			{
                                console.log(result);
				console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ': Updated Carton ' + Data.toteID + ' for OrderID:' + Data.orders[0].orderID + ' to Host');
                return callback();
			}
		});
	}
	else
	{
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ': No ERP Items Found For Carton ' + Data.toteID + ' For OrderID:' + Data.orders[0].orderID + ' To Sent To Host');
        return callback();
	}
}

function CheckForDuplicateOrder(CartonArr)
{
	var Arr = [];

	var ndx = 2;
	while(ndx < CartonArr.length)
	{
		Arr.push(CartonArr[ndx]);
		ndx++;
	}



	console.log('My List: ' + Arr);
	now = new Date();

    //var value = moment(now).format('YYYY-MM-DD');



    //var Param = '60266';
    var param = /2018-10-26/;
 
    //console.log(param);

	//var cursor1 = pickList.find({'orders.orderID':{$in:Arr}}).cursor();
	//var cursor1 = pickList.find({'transporterID':{$ne : null}}).cursor();
	var cursor1 = pickList.find({"PackedDate":{$regex:param}, "Status":"PACKED"}).cursor();

	cursor1.on('data', function(match)
	{
		cursor1.pause();
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' ' + match.toteID);

        if(match.Status == "PACKED" || match.Status == "SHIPPED")
        {
			/*SerialUpdateToHost(match, function()
			{*/
				match.ShipUpdateRequired = true;
                                match.PackUpdateRequired = true;
				match.WmsSerialsUpdated = false;
				match.Status = "SHIPPED";
				match.orders[0].shipped = true;
                                match.transporterID = null;
				match.save(function()
				{
					console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' ' + match.toteID + ' now requied');
                                    cursor1.resume();

			})
        /*})*/
        }
        else
        {
          cursor1.resume();
        }
	});

	cursor1.on('end', function(match)
	{
		console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' ' + ' Done');
		WaitForTrigger();
	});
}

mongoose.connect('mongodb://10.211.110.131/MTNWCS', function(err)
{
	if(err)
	{
		console.error( 'MONGO CONNECT ERROR ' + err);
	}
	else
	{
		console.log('Connected To Mongo');

		console.log("Entered list: " + process.argv)
		CheckForDuplicateOrder(process.argv);
	}
}); /* mongoose.connect */

/* Intervals per minute */
function WaitForTrigger()
{
	setTimeout(function()
	{
	  CheckForDuplicateOrder(process.argv);
	}, 900000);
} /* WaitForTrigger */

//var Testdata = {"orderId":"1000063_1","printerName":"qdp2018","labelprinterName":"qlp2018","items":[{"_id":"597f48085540162ea0bff8ea","packedBy":null,"pickedBy":null,"serialised":true,"sku":"5638388","barcode":null,"serials":["8927000004671325700"],"shortPick":false,"packed":false,"picked":true,"pickZone":"2","location":"PPZ02B05P01","qtyPacked":1,"pickQty":1,"qty":1,"uom":"EA","unitPrice":10,"eachWeight":10,"itemDescription":"Contract MultiSim for LTE","itemCode":"Contract MultiSim for LTE","$$hashKey":"object:89"},{"_id":"597f480f5540162ea0bff8f0","packedBy":null,"pickedBy":null,"serialised":true,"sku":"6575426","barcode":"6901443182404","serials":["863162030163673"],"shortPick":false,"packed":false,"picked":true,"pickZone":"3","location":"CPZ03B08P12","qtyPacked":1,"pickQty":1,"qty":1,"uom":"EA","unitPrice":10,"eachWeight":374,"itemDescription":"Huawei P8 Lite (51091QYM) 2017 Gold CT","itemCode":"Huawei P8 Lite (51091QYM) 2017 Gold CT","$$hashKey":"object:90"}],"cartonId":"33003309","courierId":"DSV","station":36,"CartonNo":1,"NoOfCartons":1,"CartionWeight":588,"LastCarton":true,"FirstCarton":true,"orderType":"ONLINEHE"}


