var fs = require('fs');
var replace = require('replace');
var LineByLineReader = require('line-by-line');
var config = require('printing');
var redis = require('redis');
var exec = require('child_process').spawn;
var rest = require('restler');
var moment = require('moment');

var Subclient = redis.createClient();
var Channel = process.argv[2];

console.log("Channel argument: " + process.argv[2])
console.log("Template Directory: " + process.env.INVOICE_TEMPLATE_DIR)
console.log("Template Out Directory: " + process.env.INVOICE_TMP_DIR)
console.log("Pdf Out Directory: " + process.env.PDFDIR)

var Templatedir = process.env.INVOICE_TEMPLATE_DIR;
var TempOutDir = process.env.INVOICE_TMP_DIR;
var PdfOutDir = process.env.PDFDIR;

if(!Templatedir || Templatedir === 'undefined' || Templatedir == null){
  Templatedir = '/apps/wcs/source/services/invoice_templates/';
  console.log("Template Directoryi adjusted to : " + Templatedir);

}

if(!TempOutDir || TempOutDir === 'undefined' || TempOutDir == null){
  TempOutDir = '/apps/wcs/source/services/log_files/';
  console.log("Template Out Directory adjusted to : " + TempOutDir);

}

//if(!PdfOutDir || PdfOutDir === 'undefined' || PdfOutDir == null){
  PdfOutDir = '/apps/wcs/source/services/desktop_interface/public/pdf/';
  console.log("Pdf Out Directory adjusted to : " + PdfOutDir);

//}

console.log("Current vat: " + config.Printout.currentVat);
console.log("New vat: " + config.Printout.newVat);
console.log("New Vat Effective Date: " + config.Printout.NewVatEffectiveDate);

WmsEndPoint = config.WMS.url;
HostEndPoint = config.HOST.packedUpdateUrl;
SOShipmentEndPoint = config.HOST.SalesOrderShipmentUpdateUrl;
HostSerialEndPoint = config.HOST.SerialUpdateUrl;
CourierUpdateEndPoint = config.COURIER.updateUrl;
CourierLabelEndPoint = config.COURIER.labelUrl;
CourierNotifyEndPoint = config.COURIER.NotifyUrl;
ProcessEnv = config.Printout.ENV;

Subclient.subscribe(Channel);

var SectionReplace = false;
var Section = [];
var Coordinates = null;
var BigTotal = 0;
var RetailCopy = '%';

var Keywords;

var OrdKeywords = [
    '{vat_no}',
    '({barcode})',
    '({invoice_number})',
    '({due_date})',
    '({customer_account})',
    '({customer_order})',
    '({invoice_date})',
    '({mtn_ref})',
    '({courier_order})',
    '({carton_id})',
    '({terms})',
    '{pgnum}',
    '{pgnumtot}',
    '{ord_barcode}',
    '{vat}',
    '{total}',
    '{specialInstructions}',
    '{specialInstructions1}',
    '{store_name}',
    '{store_massage1}',
    '{store_massage2}'
];

var RetailKeywords = [
    '{vat_no}',
    '{due_date}',
    '{customer_account}',
    '{customer_order}',
    '{invoice_date}',
    '{m_tel_ref}',
    '{terms}',
    '{pnum}',
    '{ptot}',
    '{ord_barcode}',
    '{vat}',
    '{total}',
    '{invoice_number}',
    '{total_ex_vat}',
    '{copy}'
];

var RetailMKeywords = [
    '{ord_barcode}',
    '{po_number}',
    '{customer_order}',
    '{customer_account}',
    '{invoice_number}',
    '{vat_no}',
    '{invoice_date}',
    '{pgnum}',
    '{pgnumtot}',
    '{end_section}'
];

function convertBrackets(data) {
    var result = "";
    var i, j = 0;

    if (typeof data != 'undefined') {
        if (data) {
            for (i = 0; i < data.length; i++) {
                if ((data[i] == '(') ||
                    (data[i] == ')') || (data[i] == '\\')) {
                    result = result + '\\';
                    j++;
                }

                result = result + data[i];
                j++;
            }
        }
    }

    return result;
} /* convertBrackets */

function GetSpaceIndex(Line, Index){
	let lndx = Index;
	while(Line[lndx] != ' ' && lndx > 0){
		lndx--;
	}

	return lndx;
} /* GetSpaceIndex */

/*function BreakLineAtLimit(limit, Line, Arr){
	let pos = 0;
	let x = Arr.length;
	let Index = pos + limit -1;

	Index = GetSpaceIndex(Line, Index);

	let str = Line.substr(pos, (Index+1));
	let Remaining = Line.length - (Index+1);

	Arr[x++] = convertBrackets(str);
	pos += Index;

	while (Remaining > 0) {
		let len = 0;

		if(Remaining > limit){
			Index = pos + limit -1;
			Index = GetSpaceIndex(Line, Index);
			len = Index + 1;
			str = Line.substr(pos, len);
			pos += Index;
		} else {
			len = Line.length - pos;
			str = Line.substr(pos, len);
		}

		Remaining = Remaining - len;
		Arr[x++] = convertBrackets(str);
	}

	return Arr;
}  BreakLineAtLimit */

function BreakLineAtLimit(limit, Line, Arr){
	let pos = 0;
	let x = Arr.length;
	let Index = pos + limit -1;

	Index = GetSpaceIndex(Line, Index);

	let str = Line.substr(pos, (Index+1));
	let Remaining = Line.length - (Index+1);

	Arr[x++] = convertBrackets(str);
	pos += Index;

	while (Remaining > 0) {
		let len = 0;

		if(Remaining > limit){
			Index = pos + limit -1;
			Index = GetSpaceIndex(Line, Index);
			len = Index + 1;

			len = len - pos;

			if(len <= 1){
				len = limit;
			}

			str = Line.substr(pos, len);
			pos += len;
		} else {
			len = Remaining + 1;
			str = Line.substr(pos, len);
		}

		Remaining = Remaining - len;
		Arr[x++] = convertBrackets(str);
	}

	return Arr;
} /* BreakLineAtLimit */

function GenerateAddressData(HeaderData) {
    var x = 0;
    var Vals = [];

    /*if(HeaderData.BILLTO != null && HeaderData.BILLTO != "")
    {
      Vals[x++] = convertBrackets(HeaderData.BILLTO);
    }*/

    //var HeaderData = JSON.parse(sHeaderData);

    if (HeaderData.orderType == 'Corporate') {
        if (HeaderData.billTo.contact1Name != null && HeaderData.billTo.contact1Name != "") {
            Vals[x++] = convertBrackets(HeaderData.billTo.contact1Name);
        }
    }

    if (HeaderData.billTo.street1 != null && HeaderData.billTo.street1 != "") {
        if (HeaderData.billTo.street1.length > 38) {
			Vals = BreakLineAtLimit(38, HeaderData.billTo.street1, Vals);
			x = Vals.length;
        } else
            Vals[x++] = convertBrackets(HeaderData.billTo.street1);
    }

    if (HeaderData.billTo.street2 != null && HeaderData.billTo.street2 != "") {
        if (HeaderData.billTo.street2.length > 38) {
			Vals = BreakLineAtLimit(38, HeaderData.billTo.street2, Vals);
			x = Vals.length;
        } else
            Vals[x++] = convertBrackets(HeaderData.billTo.street2);
    }

    if (HeaderData.billTo.street3 != null && HeaderData.billTo.street3 != "") {
        if (HeaderData.billTo.street3.length > 38) {
			Vals = BreakLineAtLimit(38, HeaderData.billTo.street3, Vals);
			x = Vals.length;
        } else
            Vals[x++] = convertBrackets(HeaderData.billTo.street3);
    }

    if (HeaderData.billTo.street4 != null && HeaderData.billTo.street4 != "") {
        if (HeaderData.billTo.street4.length > 38) {
			Vals = BreakLineAtLimit(38, HeaderData.billTo.street4, Vals);
			x = Vals.length;
        } else
            Vals[x++] = convertBrackets(HeaderData.billTo.street4);
    }

    if (HeaderData.billTo.city != null && HeaderData.billTo.city != "") {
        Vals[x++] = convertBrackets(HeaderData.billTo.city);
    }

    if (HeaderData.billTo.zip != null && HeaderData.billTo.zip != "") {
        Vals[x++] = convertBrackets(HeaderData.billTo.zip);
    }

    if (HeaderData.billTo.contact1Name != null && HeaderData.billTo.contact1Name != "") {
		if (HeaderData.billTo.contact1Name.length > 38) {
			Vals = BreakLineAtLimit(38, HeaderData.billTo.contact1Name, Vals);
			x = Vals.length;
		} else
        	Vals[x++] = convertBrackets(HeaderData.billTo.contact1Name);
    }

    if (HeaderData.billTo.contact1Email != null && HeaderData.billTo.contact1Email != "") {
        Vals[x++] = convertBrackets(HeaderData.billTo.contact1Email);
    }

    x = x - 1;
    return Vals;
} /* GenerateAddressData */

function GenerateInvAddressData(HeaderData) {
    var x = 0;
    var Vals = [];

    if (HeaderData.orderType == 'Corporate') {
        if (HeaderData.shipTo.contact1Name != null && HeaderData.shipTo.contact1Name != "") {
            Vals[x++] = convertBrackets(HeaderData.shipTo.contact1Name);
        }
    }

    if (HeaderData.shipTo.street1 != null && HeaderData.shipTo.street1 != "") {
        if (HeaderData.shipTo.street1.length > 38) {
			Vals = BreakLineAtLimit(38, HeaderData.shipTo.street1, Vals);
			x = Vals.length;
        } else
            Vals[x++] = convertBrackets(HeaderData.shipTo.street1);
    }

    if (HeaderData.shipTo.street2 != null && HeaderData.shipTo.street2 != "") {
        if (HeaderData.shipTo.street2.length > 38) {
			Vals = BreakLineAtLimit(38, HeaderData.shipTo.street2, Vals);
			x = Vals.length;
        } else
            Vals[x++] = convertBrackets(HeaderData.shipTo.street2);
    }

    if (HeaderData.shipTo.street3 != null && HeaderData.shipTo.street3 != "") {
        if (HeaderData.shipTo.street3.length > 38) {
			Vals = BreakLineAtLimit(38, HeaderData.shipTo.street3, Vals);
			x = Vals.length;
        } else
            Vals[x++] = convertBrackets(HeaderData.shipTo.street3);
    }

    if (HeaderData.shipTo.street4 != null && HeaderData.shipTo.street4 != "") {
        if (HeaderData.shipTo.street4.length > 38) {
			Vals = BreakLineAtLimit(38, HeaderData.shipTo.street4, Vals);
			x = Vals.length;
        } else
            Vals[x++] = convertBrackets(HeaderData.shipTo.street4);
    }

    if (HeaderData.shipTo.city != null && HeaderData.shipTo.city != "") {
        Vals[x++] = convertBrackets(HeaderData.shipTo.city);
    }

    if (HeaderData.shipTo.zip != null && HeaderData.shipTo.zip != "") {
        Vals[x++] = convertBrackets(HeaderData.shipTo.zip);
    }

    if (HeaderData.shipTo.contact1Name != null && HeaderData.shipTo.contact1Name != "") {
		if (HeaderData.shipTo.contact1Name.length > 38) {
			Vals = BreakLineAtLimit(38, HeaderData.shipTo.contact1Name, Vals);
			x = Vals.length;
		} else
        	Vals[x++] = convertBrackets(HeaderData.shipTo.contact1Name);
    }

    if (HeaderData.shipTo.contact1Email != null && HeaderData.shipTo.contact1Email != "") {
        Vals[x++] = convertBrackets(HeaderData.shipTo.contact1Email);
    }

    x = x - 1;
    return Vals;
} /* GenerateAddressData */

function GenerateLabelReplaceVals(LineData, HeaderData) {

    var vals = [];

    vals[0] = LineData.orderId;
    vals[1] = moment(new Date()).format('YYYY-MM-DD');
    vals[2] = LineData.cartonId;
    vals[3] = HeaderData.shipTo.contact1Name;
    vals[4] = '';
    vals[5] = HeaderData.shipTo.street1;
    vals[6] = HeaderData.shipTo.street2;
    vals[7] = HeaderData.shipTo.city;
    vals[8] = HeaderData.shipTo.zip;
    vals[9] = LineData.CartonNo; //carton_no
    vals[10] = LineData.NoOfCartons; //no_of_cartons
    vals[11] = moment(new Date()).format('YYYY-MM-DD HH:mm');

    return vals;

} /* GenerateLabelReplaceVals */

function GenerateReplaceableRetailMData(HeaderData, OrderData, PageNo, NumberOfPages) {
	let RVals = [];

	let CVatNo = HeaderData.custVatNo;
    if (!CVatNo) {
        CVatNo = "";
    } else {
        CVatNo = CVatNo;
    }

	let RefOrd = HeaderData.referenceOrd;

	if(!RefOrd){
	   RefOrd = '';
	}

	let InvoiceDate = moment(new Date()).format('YYYY-MM-DD');
    if(OrderData.PackedDate && OrderData.PackedDate != null)
        InvoiceDate = moment(OrderData.PackedDate).format('YYYY-MM-DD');

    RVals[0] = convertBrackets(OrderData.orderId);           //'{ord_barcode}';
    RVals[1] = convertBrackets(RefOrd);                      //'{po_number}';
    RVals[2] = convertBrackets(HeaderData.customerOrderNo);  //'{customer_order}'
    RVals[3] = convertBrackets(HeaderData.customerAccount);  //'{customer_account}'
    RVals[4] = HeaderData.invoiceNumber;                     //'{invoice_number}'
    RVals[5] = CVatNo;                                       //'{vat_no}'
    RVals[6] = convertBrackets(InvoiceDate);                 //'{invoice_date}'
    RVals[7] = PageNo;                                       //'{pgnum}';
    RVals[8] = NumberOfPages;                                //'{pgnumtot}';
    RVals[9] = '%';                                          //'{end_section}'

    return RVals;
} /* GenerateReplaceableRetailMData */

function GenerateReplaceableRetailData(HeaderData, OrderData, PageNo, NumberOfPages) {
	let RVals = [];

	let CVatNo = HeaderData.custVatNo;
    if (!CVatNo) {
        CVatNo = "";
    } else {
        CVatNo = "Customer Vat Reg No:   " + CVatNo;
    }

	let RefOrd = HeaderData.referenceOrd;

	if(!RefOrd){
	   RefOrd = '';
	}

	let InvoiceDate = moment(new Date()).format('YYYY-MM-DD');
    if(OrderData.PackedDate && OrderData.PackedDate != null)
        InvoiceDate = moment(OrderData.PackedDate).format('YYYY-MM-DD');

	let dBigTotal = BigTotal;
	let RvalueAddedTax = DateCompare(InvoiceDate);
    let OrdVat = (dBigTotal * RvalueAddedTax);
    let MyOrdTotal = (parseFloat(dBigTotal) + parseFloat(OrdVat));

	OrdVat = OrdVat.toFixed(2);
	let sOrdVat = FormatNumber(OrdVat);
	MyOrdTotal = MyOrdTotal.toFixed(2);
	let sMyOrdTotal = FormatNumber(MyOrdTotal);
	dBigTotal = dBigTotal.toFixed(2);
	let sBigTotal = FormatNumber(dBigTotal);

	let TermsArr;
	let TermDays;
	if(HeaderData.terms){
		let LTerms = HeaderData.terms;
		TermsArr = LTerms.split(' ');
		TermDays = parseInt(TermsArr[0]);
	}

	if(!TermDays || TermDays <= 0){
		TermDays = 30;
	}

	let DueDT = new Date();
	DueDT.setDate(DueDT.getDate() + TermDays);
	DueDT.setMonth(DueDT.getMonth() + 1);
	DueDT.setDate(0);
	let DueDate = moment(DueDT).format('YYYY-MM-DD');

    RVals[0] = CVatNo;                                       //'{vat_no}'
    RVals[1] = convertBrackets(DueDate);                     //'{due_date}';
    RVals[2] = convertBrackets(HeaderData.customerAccount);  //'{customer_account}'
    RVals[3] = convertBrackets(RefOrd);                      //'{customer_order}'
    RVals[4] = convertBrackets(InvoiceDate);                 //'{invoice_date}'
    RVals[5] = convertBrackets(HeaderData.customerOrderNo);  //'{m_tel_ref}';
    RVals[6] = convertBrackets(HeaderData.terms);            //'{terms}';
    RVals[7] = PageNo;                                       //'{pnum}';
    RVals[8] = NumberOfPages;                                //'{ptot}';
    RVals[9] = convertBrackets(OrderData.orderId);           //'{ord_barcode}';
    RVals[10] = sOrdVat;                                     //'{vat}';
    RVals[11] = sMyOrdTotal;                                 //'{total}';
    RVals[12] = HeaderData.invoiceNumber;                    //'{invoice_number}'
    RVals[13] = sBigTotal;                                   //'{total_ex_vat}'
    RVals[14] = RetailCopy;                                  //'{copy}'

    return RVals;
} /* GenerateReplaceableRetailData */

function GenerateReplaceableData(HeaderData, OrderData, PageNo, NumberOfPages, pRetailManifest) {
	if((HeaderData.orderType == 'RETAIL' || HeaderData.orderType == 'RETAILNW')){
		if(pRetailManifest){
			if(HeaderData.hardCopyManifest){
				Keywords = RetailMKeywords;
				console.log('=== GenerateReplaceableData hardCopyManifest ===');
				return GenerateReplaceableRetailMData(HeaderData, OrderData, PageNo, NumberOfPages);
			}
		} else {
			if(HeaderData.hardCopyInvoice){
				console.log('<<< GenerateReplaceableData hardCopyInvoice >>>');
				Keywords = RetailKeywords;
				return GenerateReplaceableRetailData(HeaderData, OrderData, PageNo, NumberOfPages);
			}
		}
	}

	Keywords = OrdKeywords;
    var ReplaceVals = [];
    var vat_no = HeaderData.custVatNo;

    if (!vat_no) {
        vat_no = "";
    } else {
        vat_no = "Cust VAT Number: " + vat_no;
    }
    ReplaceVals[0] = vat_no;
    ReplaceVals[1] = HeaderData.invoiceNumber;
    ReplaceVals[2] = HeaderData.invoiceNumber;

    var valueAddedTax = 0.14;

    var PackedOrderDate = OrderData.PackedDate;

    if (PackedOrderDate && PackedOrderDate != null) {
        ReplaceVals[3] = convertBrackets(moment(OrderData.PackedDate).format('YYYY-MM-DD'));
    } else {
        ReplaceVals[3] = convertBrackets(moment(new Date()).format('YYYY-MM-DD'));

        PackedOrderDate = moment(new Date()).format('YYYY-MM-DD');
        console.log("Today's Date: " + PackedOrderDate);
    }

    valueAddedTax = DateCompare(PackedOrderDate);

    ReplaceVals[4] = convertBrackets(HeaderData.customerAccount);

    if(HeaderData.orderType == 'RETAIL' || HeaderData.orderType == 'RETAILNW'){
        let RefOrd = HeaderData.referenceOrd;

		if(!RefOrd){
		   RefOrd = '';
		}
        ReplaceVals[5] = convertBrackets(RefOrd);
    } else {
    	ReplaceVals[5] = convertBrackets(HeaderData.customerOrderNo);
    }

    if (PackedOrderDate && PackedOrderDate != null)
        ReplaceVals[6] = convertBrackets(moment(OrderData.PackedDate).format('YYYY-MM-DD'));
    else
        ReplaceVals[6] = convertBrackets(moment(new Date()).format('YYYY-MM-DD'));

    ReplaceVals[7] = convertBrackets(OrderData.orderId);
    ReplaceVals[8] = convertBrackets(OrderData.courierId);
    ReplaceVals[9] = convertBrackets(OrderData.cartonId);
    ReplaceVals[10] = '';
    ReplaceVals[11] = PageNo;

    if (PackedOrderDate && PackedOrderDate != null)
        ReplaceVals[12] = NumberOfPages + ' Reprint';
    else
        ReplaceVals[12] = NumberOfPages;

    ReplaceVals[13] = convertBrackets(OrderData.orderId);
    var vat = (BigTotal * valueAddedTax);
    var total = (BigTotal + vat);
    ReplaceVals[14] = vat.toFixed(2);
    ReplaceVals[15] = total.toFixed(2);

    var SpecIns = HeaderData.specialInstructions;
    if (!SpecIns) {
        SpecIns = "";
    }

    if (SpecIns.length > 110) {
        ReplaceVals[16] = convertBrackets(SpecIns.substring(0, 100));
        ReplaceVals[17] = convertBrackets(SpecIns.substring(100, 200));
    } else {
        ReplaceVals[16] = convertBrackets(SpecIns);
        ReplaceVals[17] = ' ';
    }

	if(HeaderData.storeName){
		ReplaceVals[18] = convertBrackets(HeaderData.storeName);
	} else {
		ReplaceVals[18] = ' ';
	}

	if(HeaderData.storeMessage){
		let MsgLen = HeaderData.storeMessage.length;
		if(MsgLen <= 50){
			ReplaceVals[19] = convertBrackets(HeaderData.storeMessage);
			ReplaceVals[20] = ' ';
		} else {
            		let MyPos = 0;
            		let Remaining = HeaderData.storeMessage.length - 50;

            		let MyTmp = HeaderData.storeMessage;

			let Adjust = 0;
			if(MyTmp[50] != ' '){
				let slider = 50;
			   	while(slider > 1){
					if(MyTmp[slider-1] != ' '){
						slider--;
						Adjust = slider;
				   	} else {
						break;
					}
				}

				if(Adjust > 0){
					Remaining = HeaderData.storeMessage.length - Adjust;
				}
			}

            		let MyTmp1 = MyTmp.substring(MyPos, MyPos + Adjust);
            		MyPos += Adjust;

            		ReplaceVals[19] = convertBrackets(MyTmp1);

			MyTmp1 = MyTmp.substring(MyPos, HeaderData.storeMessage.length);
			ReplaceVals[20] = convertBrackets(MyTmp1);
		}
	} else {
		ReplaceVals[19] = ' ';
		ReplaceVals[20] = ' ';
	}

    return ReplaceVals;
} /* GenerateReplaceableData */

function DateCompare(OrderPackedDate) {
    var PackedDate = moment(OrderPackedDate, 'YYYY-MM-DD');
    var VatChangeDate = moment(config.Printout.NewVatEffectiveDate, 'YYYY-MM-DD');
    console.log('Vat date: ' + VatChangeDate + ' === order date: ' + PackedDate);
    if (VatChangeDate > PackedDate) {
        console.log('==== Using old vat info =======');
        return config.Printout.currentVat;
    } else {
        console.log('==== Using new vat info =======');
        return config.Printout.newVat;
    }
}

function GenerateItemValues(PackListData, descriptLen, printSerials) {
    var ItemVals = [];
    var LineItem = [];

    var lineIndex = 0;
    var x = 0;
    while (x < PackListData.items.length) {
        var QPacked = PackListData.items[x].qtyPacked;
        if (QPacked != null && QPacked > 0) {
            var y = 0;
            var ndx = 0;
            LineItem[y++] = convertBrackets(PackListData.Cartons[x]);
            LineItem[y++] = PackListData.items[x].qtyPacked;

            if (PackListData.items[x].itemCode.length > descriptLen) {
                var pos = 0;
                var Remaining = PackListData.items[x].itemCode.length - descriptLen;

                var tmp = PackListData.items[x].itemCode;
                var tmp1 = tmp.substring(pos, pos + descriptLen);
                pos += descriptLen;

                LineItem[y++] = convertBrackets(tmp1);
                LineItem[y++] = convertBrackets(PackListData.items[x].uom);

                if (PackListData.items[x].serials.length > 0) {
                    LineItem[y] = convertBrackets(PackListData.items[x].serials[ndx++]);
                } else {
                    LineItem[y] = '';
                }
                ItemVals[lineIndex] = LineItem;
                LineItem = [];
                lineIndex++;

                while (Remaining > 0) {
                    y = 0;
                    LineItem[y++] = '';
                    LineItem[y++] = '';
                    tmp = PackListData.items[x].itemCode;
                    if (Remaining > descriptLen)
                        tmp1 = tmp.substring(pos, pos + descriptLen);
                    else
                        tmp1 = tmp.substring(pos, PackListData.items[x].itemCode.length);

                    pos += descriptLen;
                    Remaining = Remaining - descriptLen;
                    LineItem[y++] = convertBrackets(tmp1);
                    LineItem[y++] = '';

                    if (ndx < PackListData.items[x].serials.length) {
                        LineItem[y] = convertBrackets(PackListData.items[x].serials[ndx++]);
                    } else
                        LineItem[y] = '';

                    ItemVals[lineIndex] = LineItem;
                    LineItem = [];
                    lineIndex++;
                }

                if (ndx < PackListData.items[x].serials.length && printSerials) {
                    while (ndx < PackListData.items[x].serials.length) {
                        y = 0;
                        LineItem[y++] = '';
                        LineItem[y++] = '';
                        LineItem[y++] = '';
                        LineItem[y++] = '';
                        LineItem[y] = convertBrackets(PackListData.items[x].serials[ndx++]);

                        ItemVals[lineIndex] = LineItem;
                        LineItem = [];
                        lineIndex++;
                    }
                }
            } else {
                LineItem[y++] = convertBrackets(PackListData.items[x].itemCode);
                LineItem[y++] = convertBrackets(PackListData.items[x].uom);

                if (PackListData.items[x].serials.length > 0) {
                    LineItem[y] = convertBrackets(PackListData.items[x].serials[ndx++]);
                } else {
                    LineItem[y] = '';
                }

                ItemVals[lineIndex] = LineItem;
                LineItem = [];
                lineIndex++;

                if (ndx < PackListData.items[x].serials.length && printSerials) {
                    while (ndx < PackListData.items[x].serials.length) {
                        y = 0;
                        LineItem[y++] = '';
                        LineItem[y++] = '';
                        LineItem[y++] = '';
                        LineItem[y++] = '';
                        LineItem[y] = convertBrackets(PackListData.items[x].serials[ndx++]);

                        ItemVals[lineIndex] = LineItem;
                        LineItem = [];
                        lineIndex++;
                    }
                }

            }
        }
        x++;
    }

    lineIndex--;

    return ItemVals;

} /* GenerateItemValues */

function InList(NewPackList, Sku, UnitPrice){
	let e = 0;
	while(e < NewPackList.length){
		if(NewPackList[e].Sku == Sku && NewPackList[e].UnitPrice == UnitPrice){
			return e;
		}
		e++;
	}

	return -1;
} /* InList */

function FormatNumber(num){
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
} /* FormatNumber */

function GenerateRIItemValues(PackListData){
    let ItemVals = [];
    let LineItem = [];

    let lineIndex = 0;
    let x = 0;
	let OrdTotal = 0;

    let NewPackList = [];
    while(x < PackListData.items.length){
        let QPacked = PackListData.items[x].qtyPacked;
        if(QPacked != null && QPacked > 0){
			let Sku = PackListData.items[x].sku;
			let UnitPrice = PackListData.items[x].unitPrice;
			if(!UnitPrice){
				UnitPrice = 0;
			}

			let ListIndex = InList(NewPackList, Sku, UnitPrice);
			if(ListIndex < 0){
				let Rec = {
					Sku: Sku,
					QtyPacked: QPacked,
					ItemCode: PackListData.items[x].itemCode,
					UOM: 'Each',
					Code: 'ZAR',
					UnitPrice: UnitPrice.toFixed(2)
				};

				NewPackList.push(Rec);
			} else {
				NewPackList[ListIndex].QtyPacked += QPacked;
			}
		}
		x++;
	}

	if(NewPackList.length > 0){
		x = 0;

		while(x < NewPackList.length){
			let y = 0;

			LineItem[y++] = NewPackList[x].QtyPacked;

			let Amount = NewPackList[x].QtyPacked * NewPackList[x].UnitPrice;
			OrdTotal += Amount;
			Amount = Amount.toFixed(2);
			Amount = FormatNumber(Amount);

			if(NewPackList[x].ItemCode.length > 38){
				let Vals = [];
				let nc = 1;
				Vals = BreakLineAtLimit(38, NewPackList[x].ItemCode, Vals);

                LineItem[y++] = Vals[0];
                LineItem[y++] = NewPackList[x].UOM;
                LineItem[y++] = NewPackList[x].Code;
                LineItem[y++] = FormatNumber(NewPackList[x].UnitPrice);
                LineItem[y++] = Amount;

                ItemVals[lineIndex] = LineItem;
                LineItem = [];
                lineIndex++;
                y = 0;

                while(nc < Vals.length) {
					LineItem[y++] = '';
                    LineItem[y++] = Vals[nc];
                    LineItem[y++] = '';
					LineItem[y++] = '';
					LineItem[y++] = '';
					LineItem[y++] = '';

                    ItemVals[lineIndex] = LineItem;
                    LineItem = [];
                    lineIndex++;
                    y = 0;
                    nc++;
                }
			} else {
                LineItem[y++] = convertBrackets(NewPackList[x].ItemCode);
                LineItem[y++] = NewPackList[x].UOM;
                LineItem[y++] = NewPackList[x].Code;
                LineItem[y++] = FormatNumber(NewPackList[x].UnitPrice);
                LineItem[y++] = Amount;

				ItemVals[lineIndex] = LineItem;
				LineItem = [];
				lineIndex++;
			}

			x++;
		}
	}

    lineIndex--;
    BigTotal = OrdTotal;

    return ItemVals;
} /* GenerateRIItemValues */

function GenerateRMItemValues(PackListData){
    let ItemVals = [];
    let LineItem = [];

    let lineIndex = 0;
    let x = 0;
	let OrdTotal = 0;

    let NewPackList = [];
    while(x < PackListData.items.length){
        let QPacked = PackListData.items[x].qtyPacked;
        if(QPacked != null && QPacked > 0){
			let y = 0;
			let ndx = 0;

			let CartonPrinted = false;
			while(ndx < PackListData.items[x].serials.length){
				y = 0;

				if(!CartonPrinted){
					LineItem[y++] = convertBrackets(PackListData.Cartons[x]);
					CartonPrinted = true;
				} else {
					LineItem[y++] = '';
				}

				LineItem[y++] = PackListData.items[x].sku;

				if(PackListData.items[x].itemCode.length > 38){
					let Vals = [];
					let nc = 1;
					Vals = BreakLineAtLimit(38, PackListData.items[x].itemCode, Vals);

					LineItem[y++] = Vals[0];
					LineItem[y] = convertBrackets(PackListData.items[x].serials[ndx]);

					ItemVals[lineIndex] = LineItem;
					LineItem = [];
					lineIndex++;
					y = 0;

					while(nc < Vals.length) {
						LineItem[y++] = '';
						LineItem[y++] = '';
						LineItem[y++] = Vals[nc];
						LineItem[y] = '';

						ItemVals[lineIndex] = LineItem;
						LineItem = [];
						lineIndex++;
						y = 0;
						nc++;
					}
				} else {
					LineItem[y++] = convertBrackets(PackListData.items[x].itemCode);
					LineItem[y] = convertBrackets(PackListData.items[x].serials[ndx]);

					ItemVals[lineIndex] = LineItem;
					LineItem = [];
					lineIndex++;
				}

				ndx++;
			}
		}

		x++;
	}

    lineIndex--;

    return ItemVals;
} /* GenerateRMItemValues */

function GenerateItemTaxValues(PackListData) {
    var Total = 0;
    var ItemVals = [];
    var LineItem = [];

    var lineIndex = 0;
    var x = 0;
    while (x < PackListData.items.length) {
        var y = 0;

        if (PackListData.items[x].qtyPacked != null && PackListData.items[x].qtyPacked > 0) {
            LineItem[y++] = PackListData.items[x].qtyPacked;
            if (PackListData.items[x].itemCode.length > 41) {
                var pos = 0;
                var Remaining = PackListData.items[x].itemCode.length - 40;

                var tmp = PackListData.items[x].itemCode;
                var tmp1 = tmp.substring(pos, 40);
                pos += 40;

                LineItem[y++] = convertBrackets(tmp1);
                LineItem[y++] = convertBrackets(PackListData.items[x].uom);
                var Cost = 0;
                if (PackListData.items[x].unitPrice != null) {
                    Cost = PackListData.items[x].unitPrice;
                }
                LineItem[y++] = Cost.toFixed(2);
                Total = Total + (PackListData.items[x].qtyPacked * Cost)
                var amount = (PackListData.items[x].qtyPacked * Cost);
                LineItem[y] = amount.toFixed(2);
                ItemVals[lineIndex] = LineItem;
                LineItem = [];
                lineIndex++;

                while (Remaining > 0) {
                    y = 0;
                    LineItem[y++] = '';
                    tmp = PackListData.items[x].itemCode;
                    if (Remaining > 40)
                        tmp1 = tmp.substring(pos, 40);
                    else
                        tmp1 = tmp.substring(pos, PackListData.items[x].itemCode.length);

                    pos += 40;
                    Remaining = Remaining - 40;
                    LineItem[y++] = convertBrackets(tmp1);
                    LineItem[y++] = '';
                    LineItem[y++] = '';
                    LineItem[y] = '';
                    ItemVals[lineIndex] = LineItem;
                    LineItem = [];
                    lineIndex++;
                }
            } else {
                LineItem[y++] = convertBrackets(PackListData.items[x].itemCode);

                LineItem[y++] = convertBrackets(PackListData.items[x].uom);
                var Cost = 0;
                if (PackListData.items[x].unitPrice != null) {
                    Cost = PackListData.items[x].unitPrice;
                }
                LineItem[y++] = Cost.toFixed(2);
                Total = Total + (PackListData.items[x].qtyPacked * Cost);
                var amount = (PackListData.items[x].qtyPacked * Cost);
                LineItem[y] = amount.toFixed(2);
                ItemVals[lineIndex] = LineItem;
                LineItem = [];
                lineIndex++;
            }

            var MoreToAdd = true;
            var SerialIndex = 0;
            while (SerialIndex < PackListData.items[x].serials.length) {
                LineItem = AddSerialToNextLine(PackListData, x, SerialIndex);

                if (LineItem.length > 0) {
                    ItemVals[lineIndex++] = LineItem;
                    LineItem = [];
                }

                SerialIndex++;
            }
        }
        x++;
    }

    if (lineIndex == 1) {
        ItemVals[lineIndex] = LineItem;
    } else {
        lineIndex = lineIndex - 1;
    }

    BigTotal = Total;
    return ItemVals;

} /* GenerateItemTaxValues */

function AddSerialToNextLine(PackListData, index, SerialIndex) {
    var x = 0;
    var y = 0;
    var SerialItem = [];
    if (PackListData.items[index].serials.length > 0) {
        if (SerialIndex >= PackListData.items[index].serials.length) {
            return SerialItem;
        } else {
            SerialItem[y++] = '';
            SerialItem[y++] = PackListData.items[index].serials[SerialIndex];
            SerialItem[y++] = '';
            SerialItem[y++] = '';
            SerialItem[y] = '';

            return SerialItem;
        }
    } else {
        return SerialItem;
    }
} /* AddSerialToNextLine */

function ProcessLineDelInfo(line, DestFile, Print, DelKey, DelVals, DelIndex) {
    var Print = Print;
    var x = 0;
    var tmpLine = line;
    var res = line.split(' ');
    if (res[0] == DelKey) {
        while (x < DelIndex) {
            var i = 0;

            line = Coordinates + x + ' 12 mul add moveto\n';
            line = line + '(' + DelVals[x] + ')';
            var j = 1;
            while (j < res.length) {
                line = line + ' ' + res[j];
                j++;
            }

            fs.appendFileSync(DestFile, line + '\n');
            line = tmpLine;
            Print = false;

            x++;
        }
    }

    return Print;
} /* ProcessLineDelInfo */

function ProcessLineInfo(line, DestFile, Print, index, ItemVals, StartLine, EndLine, ItemKeys) {
    var Print = Print;
    var x = 0;
    var itr = StartLine;
    var tmpLine = line;
    while (x < EndLine) {
        //console.log('replacing line');
        var res = line.split(' ');
        var i = 0;
        while (i < ItemKeys.length) {
            if (res[0] == ItemKeys[i]) {
                line = Coordinates + x + ' 15 mul add moveto\n';
                line = line + '(' + ItemVals[itr][i] + ')';
                var j = 1;
                while (j < res.length) {
                    line = line + ' ' + res[j];
                    j++;
                }

                if (res[res.length - 1] == 'rmoveto') {
                    line = line + '\n(' + ItemVals[itr][i] + ') show';
                }

                fs.appendFileSync(DestFile, line + '\n');

                line = tmpLine;
                Print = false;
                break;
            }
            i++;
        }
        x++;
        itr++;
    }

    if(EndLine == 0){
		Print = false;
	}

    return Print;
} /* ProcessLineInfo */

function ProcessLineInvInfo(line, DestFile, Print, InvKey, InvVals, InvIndex) {
    var Print = Print;
    var x = 0;
    var tmpLine = line;
    var res = line.split(' ');
    if (res[0] == InvKey) {
        while (x < InvIndex) {
            var i = 0;

            line = Coordinates + x + ' 12 mul add moveto\n';
            line = line + '(' + InvVals[x] + ')';
            var j = 1;
            while (j < res.length) {
                line = line + ' ' + res[j];
                j++;
            }

            fs.appendFileSync(DestFile, line + '\n');
            line = tmpLine;
            Print = false;

            x++;
        }
    }

    return Print;
} /* ProcessLineInvInfo */

function ProcessCombinedOutputFile(DestFile, stream, SourceFile, Keywords, ReplaceVals, index, ItemVals, Data, StartLine, EndLine, DelKey, DelVals, DelIndex, InvKey, InvVals, InvIndex, ItemKeys, TaxInvoice, HeaderData,Page, NumberOfPages, MaxNoLinesPerPage, PrintRetManifest, pRetailManifest){
	if(Page == NumberOfPages){
		EndLine = index - StartLine;
	}

	var lrNew = new LineByLineReader(SourceFile);
	lrNew.on('error', function (err){
		console.log(err);
	});

	lrNew.on('line', function (line){
		var Print = true;
		lrNew.pause();

		if(SectionReplace && Section.length > 1){
			var BreakAt = line.split('$$');
			if(BreakAt.length == 2){
				Coordinates = BreakAt[0];
				Print = false;
			}

			switch(Section[Section.length -1]){
				case 'line':{
					Print = ProcessLineInfo(line, DestFile, Print, index, ItemVals, StartLine, EndLine, ItemKeys);
				}
				break;
				case 'del_add':{
					Print = ProcessLineDelInfo(line, DestFile, Print, DelKey, DelVals, DelIndex);
				}
				break;
				case 'inv_add':{
					Print = ProcessLineInvInfo(line, DestFile, Print, InvKey, InvVals, InvIndex);
				}
				break;
			}
		}

		var compare = line.substring(0,7);
		if(compare == 'section'){
			if(line.length > compare.length){
				SectionReplace = true;
				Section = line.split('$');
			} else if(line.length == compare.length){
				SectionReplace = false;
				Coordinates = null;
			}
		} else {
			if(Print){
        		fs.appendFileSync(DestFile, line + '\n');
			}
		}

		lrNew.resume();
	});

	lrNew.on('end', function (){
		var Max = Keywords.length
		/*if(TaxInvoice == false){
			Max -= 6;
		}*/

		for(var i = 0; i < Max ; i++){
			replace ({
			 regex: Keywords[i],
			 replacement: ReplaceVals[i],
			 paths: [DestFile],
			 recursive: true,
			 silent: true
			});
		}

		console.log("End of Page: " + Page);

		/* Increment Start of the line section should there be more than one page */
		StartLine = StartLine + MaxNoLinesPerPage;

		/* Increment End of the line section should there be more than one page */
		EndLine = MaxNoLinesPerPage;

		/* Increment the page */
		Page++;

		if(Page <= NumberOfPages){
			/* Recreate these values per page due to the changing page number */
			ReplaceVals = GenerateReplaceableData(HeaderData, Data, Page, NumberOfPages, pRetailManifest);
			ProcessCombinedOutputFile(DestFile, stream, SourceFile, Keywords, ReplaceVals, index, ItemVals, Data, StartLine, EndLine, DelKey, DelVals, DelIndex, InvKey, InvVals, InvIndex, ItemKeys, TaxInvoice, HeaderData,Page, NumberOfPages, MaxNoLinesPerPage, PrintRetManifest, pRetailManifest);
		} else {
			if(HeaderData.hardCopyManifest && PrintRetManifest){
				PrintRetManifest = false;
				pRetailManifest = true;

				SourceFile = Templatedir + 'Retail_Shipping_Manifest.ps';
				ItemVals = GenerateRMItemValues(Data); // Returns an array of values to be used later

				ItemKeys = [
					'({carton_id})',
					'({prodcode})',
					'({description})',
					'({serial_num})'
				];

				MaxNoLinesPerPage = 35; // Set the maximum lines thats a single page can handle

    			index = ItemVals.length; // Workout the number of lines items
    			StartLine = 0; // Default to the start of the array
				EndLine = index; // Default to the end of the array

				NumberOfPages = index / MaxNoLinesPerPage; // Workout how many pages this invoice document should be
    			NumberOfPages = Math.floor(NumberOfPages);
    			let rRemainder = index % MaxNoLinesPerPage;

				Page = 1; // Set default page to 1 and then increment there after

				if (rRemainder != 0) { NumberOfPages += 1; } // Always add an extra page should there be a remainder on Modulus

				if (NumberOfPages > 1) {
					EndLine = MaxNoLinesPerPage;
				}

				ReplaceVals = GenerateReplaceableData(HeaderData, Data, Page, NumberOfPages, pRetailManifest);
				return ProcessCombinedOutputFile(DestFile, stream, SourceFile, RetailMKeywords, ReplaceVals, index, ItemVals, Data, StartLine, EndLine, DelKey, DelVals, DelIndex, InvKey, InvVals, InvIndex, ItemKeys, TaxInvoice, HeaderData,Page, NumberOfPages, MaxNoLinesPerPage, PrintRetManifest, pRetailManifest);
			}

			stream.close();
			var child = exec("ps2pdf", [DestFile, Data.PdfPrint]);

			child.on('close', function(code){
				var child1 = exec("mv", [TempOutDir + Data.PdfPrint, Data.PdfPrint]);
				child1.on('close', function(code){
            				try {
                				//fs.unlink(DestFile);
            				} catch(e){
                				console.log('Failed to unlink file ' + DestFile);
            				}
					//fs.unlink(DestFile);
					console.log('Pdf file ' + Data.PdfPrint + ' ready!!');
				});
			});
		}

	});

} /* ProcessCombinedOutputFile */

function ProcessOutputFile(SourceFile, Keywords, ReplaceVals, index, ItemVals, Data, StartLine, EndLine, DelKey, DelVals, DelIndex, InvKey, InvVals, InvIndex, ItemKeys, TaxInvoice, HeaderData, Page, NumberOfPages, MaxNoLinesPerPage, PrintRetManifest, pRetailManifest, callback) {
    //Update the line page end
    //if (Page == NumberOfPages) {
    //    EndLine = index - StartLine;
    //}

	if(Page == NumberOfPages){
		EndLine = index - StartLine;
	}

    var DestFile = TempOutDir + Data.orderId + '_' + Page + '.ps'; // Output filename - Changes per page

    console.log("Start:" + new Date().toISOString());
    const stream = fs.createWriteStream(DestFile);

    var lr = new LineByLineReader(SourceFile);
    lr.on('error', function(err) {
        console.log(err);
    });

    lr.on('line', function(line) {
        var Print = true;
        lr.pause();

        if (SectionReplace && Section.length > 1) {
            var BreakAt = line.split('$$');
            if (BreakAt.length == 2) {
                Coordinates = BreakAt[0];
                Print = false;
            }

            switch (Section[Section.length - 1]) {
                case 'line':
                    {
                        Print = ProcessLineInfo(line, DestFile, Print, index, ItemVals, StartLine, EndLine, ItemKeys);
                    }
                    break;
                case 'del_add':
                    {
                        Print = ProcessLineDelInfo(line, DestFile, Print, DelKey, DelVals, DelIndex);
                    }
                    break;
                case 'inv_add':
                    {
                        Print = ProcessLineInvInfo(line, DestFile, Print, InvKey, InvVals, InvIndex);
                    }
                    break;
            }
        }

        var compare = line.substring(0, 7);
        if (compare == 'section') {
            if (line.length > compare.length) {
                SectionReplace = true;
                Section = line.split('$');
            } else if (line.length == compare.length) {
                SectionReplace = false;
                Coordinates = null;
            }
        } else {
            if (Print) {
                fs.appendFileSync(DestFile, line + '\n');
            }
        }

        lr.resume();
    });

    lr.on('end', function() {
        var Max = Keywords.length
        /*if (TaxInvoice == false) {
            Max -= 6;
        }*/

        for (var i = 0; i < Max; i++) {
            replace({
				regex: Keywords[i],
				replacement: ReplaceVals[i],
				paths: [DestFile],
				recursive: true,
				silent: true
			});
        }

        stream.close();
        console.log("End:" + new Date().toISOString());

        if (ProcessEnv) {
            if (ProcessEnv != "LINUX") {
                return;
            }
        }

        var child = exec("lpr", ["-P", Data.printerName, "-#2", "-s", "-h", DestFile]);

        child.on('close', function(code) {
            try {
            	fs.unlink(DestFile);
			} catch(e){
				console.log('Failed to unlink file ' + DestFile);
			}

            StartLine = StartLine + MaxNoLinesPerPage; // Increment Start of the line section should there be more than one page
            EndLine = MaxNoLinesPerPage; // Increment End of the line section should there be more than one page
            Page++; // Increment the page

            if (Page <= NumberOfPages) {
                // Recreate these values per page due to the changing page number
                ReplaceVals = GenerateReplaceableData(HeaderData, Data, Page, NumberOfPages, pRetailManifest);
                ProcessOutputFile(SourceFile, Keywords, ReplaceVals, index, ItemVals, Data, StartLine, EndLine, DelKey, DelVals, DelIndex, InvKey, InvVals, InvIndex, ItemKeys, TaxInvoice, HeaderData, Page, NumberOfPages, MaxNoLinesPerPage, PrintRetManifest, pRetailManifest, callback);
            } else {
				if(HeaderData.hardCopyManifest && PrintRetManifest){
					PrintRetManifest = false;
					pRetailManifest = true;

					SourceFile = Templatedir + 'Retail_Shipping_Manifest.ps';
					ItemVals = GenerateRMItemValues(Data); // Returns an array of values to be used later

					ItemKeys = [
						'({carton_id})',
						'({prodcode})',
						'({description})',
						'({serial_num})'
					];

					MaxNoLinesPerPage = 35; // Set the maximum lines thats a single page can handle

					index = ItemVals.length; // Workout the number of lines items
					StartLine = 0; // Default to the start of the array
					EndLine = index; // Default to the end of the array

					NumberOfPages = index / MaxNoLinesPerPage; // Workout how many pages this invoice document should be
					NumberOfPages = Math.floor(NumberOfPages);
					let rRemainder = index % MaxNoLinesPerPage;

					Page = 1; // Set default page to 1 and then increment there after

					if (rRemainder != 0) { NumberOfPages += 1; } // Always add an extra page should there be a remainder on Modulus

					if (NumberOfPages > 1) {
						EndLine = MaxNoLinesPerPage;
					}

					ReplaceVals = GenerateReplaceableData(HeaderData, Data, Page, NumberOfPages, pRetailManifest);

					return ProcessOutputFile(SourceFile, RetailMKeywords, ReplaceVals, index, ItemVals, Data, StartLine, EndLine, DelKey, DelVals, DelIndex, InvKey, InvVals, InvIndex, ItemKeys, TaxInvoice, HeaderData, Page, NumberOfPages, MaxNoLinesPerPage, PrintRetManifest, pRetailManifest, callback);
				}

				return callback();
			}
        });
    });

} /* ProcessOutputFile */

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }

    return true;
} /* isEmpty */

Subclient.on("message", function(channel, message) {
    if (channel == Channel) {
        // order line detail from WCS
        console.log('Incoming:', message);
        var Data = JSON.parse(message);
        if (!isEmpty(Data)) {
            // Request to WMS to retrieve order header info
            rest.post(WmsEndPoint, {
                data: { 'orderID': Data.orderId }
            }).on('complete', function(result) {
                if (result instanceof Error) {
                    console.log('Error:', result.message);
                } else {
					var resultCheck = result.indexOf('Error: NO DATA FOUND');

					if (resultCheck <= -1){
						var HeaderData = JSON.parse(result);
						console.log(result);

						if(Data.PdfPrint){
							console.log('printing to PDF');
							RetailCopy = '';
							ProcessDocumentInvoice(Data, HeaderData);
						} else {
							if(Data.LastCarton){
								if(Data.NoDoc){
									console.log('Pull wall:Not printing document');
								} else {
									ProcessDocumentInvoice(Data, HeaderData);
								}
							}

							if(!Data.FirstCarton){
								if(Data.NoLabel){
									console.log('Put wall:Not printing label');
								} else {
									ProcessCourierLabel(Data, HeaderData);
								}
							}

							if(Data.FirstCarton){
								if(Data.NoLabel){
									console.log('Put wall:Not printing label');
								} else {
									ProcessCourierNotification(Data, HeaderData);
								}
							}
						}
					} else {
						console.log(result);
					}
                }
            });
        } else {
            console.log("Incorrect data supplied: " + moment(new Date()).format('YYYY-MM-DD HH:mm') + " - channel: " + channel);
        }
    }
});

function SerialUpdateToHost(Data) {
    var items = [];

    if (Data.ErpItems) {
        var x = 0;
        while (x < Data.ErpItems.length) {
            var orderLine = null;

            if (Data.ErpItems[x].orderLine)
                orderLine = Data.ErpItems[x].orderLine;

            var lineData = {
                'sku': Data.ErpItems[x].sku,
                'serials': Data.ErpItems[x].serials,
                'qty': Data.ErpItems[x].qty,
                'orderLine': orderLine,

                'totalQty': totalQty + Data.ErpItems[x].qty,

            }

            items.push(lineData);
            x++;
        }

        rest.postJson(HostSerialEndPoint, {
            data: {
                'orderID': Data.orderId,
                'cartonID': Data.cartonId,
                'cartonNo': Data.CartonNo,
                'noOfCartons': Data.NoOfCartons,
                'items': items
            }
        }).on('complete', function(result) {
            if (result instanceof Error) {
                console.log('Error:', result.message);
            } else {
                console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ' Serial update result: ' + result);
                console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ': Updated Carton ' + Data.cartonId + ' for OrderID:' + Data.orderId + ' to Host');
            }
        });
    } else {
        console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ': No ERP Items Found For Carton ' + Data.cartonId + ' For OrderID:' + Data.orderId + ' To Sent To Host');
    }
}

function HostIntTrigger(Data) {
    rest.post(SOShipmentEndPoint, {
        data: { 'orderID': Data.orderId }
    }).on('complete', function(result) {
        if (result instanceof Error) {
            console.log('Error:', result.message);
        } else {
            console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ': Sales Order Trigger Sent -> OrderID:' + Data.orderId);
        }
    });
} /* HostIntTrigger */

function ProcessERPPickedUpdate(Data) {
    rest.post(HostEndPoint, {
        data: { 'orderID': Data.orderId, 'status': 'Picked' }
    }).on('complete', function(result) {
        if (result instanceof Error) {
            console.log('Error:', result.message);
        } else {
            console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ': Updated PICKED status to HOST -> OrderID:' + Data.orderId);
            ProcessERPPackedUpdate(Data);
        }
    });
}

function ProcessERPPackedUpdate(Data) {
    rest.post(HostEndPoint, {
        data: { 'orderID': Data.orderId, 'status': 'Packed' }
    }).on('complete', function(result) {
        if (result instanceof Error) {
            console.log('Error:', result.message);
        } else {
            console.log(moment(new Date()).format('YYYY-MM-DD HH:mm') + ': Updated PACKED status to HOST -> OrderID:' + Data.orderId);
        }
    });
}

function ProcessDocumentInvoice(Data, HeaderData) {
    var TaxInvoice = false;
    // Need to expand this based on order type but for now default to that document

    if (HeaderData.orderType == 'ONLINEATG' || HeaderData.orderType == 'ONLINEHE') {
        var DocumentTemplate = Templatedir + 'TaxInvoice_1.ps';
        var ItemVals = GenerateItemTaxValues(Data); // Returns an array of values to be used later

        var ItemKeys = [
        	'({qty})',
            '({description})',
            '({uom})',
            '({unit_cost})',
            '({amount})'
        ];

        TaxInvoice = true;
        var MaxNoLinesPerPage = 24; // Set the maximum lines thats a single page can handle

    } else if (HeaderData.orderType == 'Corporate') {
        var DocumentTemplate = Templatedir + 'TaxInvoice_2.ps';
        var ItemVals = GenerateItemTaxValues(Data); // Returns an array of values to be used later

        var ItemKeys = [
        	'({qty})',
            '({description})',
            '({uom})',
            '({unit_cost})',
            '({amount})'
        ];

        TaxInvoice = true;
        var MaxNoLinesPerPage = 24; // Set the maximum lines thats a single page can handle

    } else if (HeaderData.orderType == 'DEANWHWMS') {
        var DocumentTemplate = Templatedir + 'TaxInvoice_3.ps';
        var ItemVals = GenerateItemTaxValues(Data); // Returns an array of values to be used later

        var ItemKeys = [
        	'({qty})',
            '({description})',
            '({uom})',
            '({unit_cost})',
            '({amount})'
        ];

        TaxInvoice = true;
        var MaxNoLinesPerPage = 24; // Set the maximum lines thats a single page can handle

    } else if (HeaderData.orderType == 'EBUZTE') {

        var DocumentTemplate = Templatedir + 'ShippingManifest_1.ps';
        var ItemVals = GenerateItemValues(Data, 40, true); // Returns an array of values to be used later

        var ItemKeys = [
            '({carton_id})',
            '({qty})',
            '({description})',
            '({uom})',
            '({serial_num})'
        ];

        var MaxNoLinesPerPage = 35; // Set the maximum lines thats a single page can handle


    } else if (HeaderData.orderType == 'INTTRFNWH') {

        var DocumentTemplate = Templatedir + 'ShippingManifest_2.ps';
        var ItemVals = GenerateItemValues(Data, 40, true); // Returns an array of values to be used later

        var ItemKeys = [
            '({carton_id})',
            '({qty})',
            '({description})',
            '({uom})',
            '({serial_num})'
        ];

        var MaxNoLinesPerPage = 35; // Set the maximum lines thats a single page can handle

	} else {
		if(HeaderData.hardCopyInvoice){
			var DocumentTemplate = Templatedir + 'Retail_Tax_Invoice.ps';
			var ItemVals = GenerateRIItemValues(Data); // Returns an array of values to be used later

			var ItemKeys = [
				'({qty})',
				'({description})',
				'({uom})',
				'({code})',
				'({unit_price})',
				'({amount})'
			];

			var MaxNoLinesPerPage = 35; // Set the maximum lines thats a single page can handle
		} else {
			var DocumentTemplate = Templatedir + 'Retail_Del.ps';
			var ItemVals = GenerateItemValues(Data, 73, false); // Returns an array of values to be used later

			var ItemKeys = [
				'({carton_id})',
				'({qty})',
				'({description})',
				'({uom})',
				'({serial_num})'
			];

			var MaxNoLinesPerPage = 35; // Set the maximum lines thats a single page can handle
		}
    }
    // Generate Invoice line details

    var index = ItemVals.length; // Workout the number of lines items
    var StartLine = 0; // Default to the start of the array
    var EndLine = index; // Default to the end of the array

    var NumberOfPages = index / MaxNoLinesPerPage; // Workout how many pages this invoice document should be
    NumberOfPages = Math.floor(NumberOfPages);
    var Remainder = index % MaxNoLinesPerPage;

    var Page = 1; // Set default page to 1 and then increment there after

    if (Remainder != 0) { NumberOfPages += 1; } // Always add an extra page should there be a remainder on Modulus

    var DelKey = '({delivery_address})';
    DelVals = GenerateInvAddressData(HeaderData);
    var DelIndex = DelVals.length;

    var InvKey = '({invoice_address})';
    InvVals = GenerateAddressData(HeaderData);
    var InvIndex = InvVals.length;

    // Should there be more than one page then only create lines to the maximum that can fit on a page
    if (NumberOfPages > 1) {
		EndLine = MaxNoLinesPerPage;
    }

    let PrintRetManifest = false;
    if(HeaderData.hardCopyManifest){
		PrintRetManifest = true;
	}

	let pRetailManifest = false;
    var ReplaceVals = GenerateReplaceableData(HeaderData, Data, Page, NumberOfPages, pRetailManifest);

	if(Data.PdfPrint){
		Data.PdfPrint = PdfOutDir + Data.PdfPrint;
		var DestFile = TempOutDir + Data.orderId + '.ps';
		const stream = fs.createWriteStream(DestFile);

		ProcessCombinedOutputFile(DestFile, stream, DocumentTemplate, Keywords, ReplaceVals, index, ItemVals, Data, StartLine, EndLine, DelKey, DelVals, DelIndex, InvKey, InvVals, InvIndex, ItemKeys, TaxInvoice, HeaderData,Page, NumberOfPages, MaxNoLinesPerPage, PrintRetManifest, pRetailManifest);
	} else {
    	ProcessOutputFile(DocumentTemplate, Keywords, ReplaceVals, index, ItemVals, Data, StartLine, EndLine, DelKey, DelVals, DelIndex, InvKey, InvVals, InvIndex, ItemKeys, TaxInvoice, HeaderData, Page, NumberOfPages, MaxNoLinesPerPage, PrintRetManifest, pRetailManifest, function(){
			Data.PdfPrint = moment(new Date()).format('YYYYMMDD') + '_' + Data.orderId + '.pdf';
			var DestFile = Data.PdfPrint;
			const stream = fs.createWriteStream(DestFile);
			Data.PdfPrint = TempOutDir + Data.PdfPrint;

			pRetailManifest = false;
			ReplaceVals = GenerateReplaceableData(HeaderData, Data, Page, NumberOfPages, pRetailManifest);

			if(HeaderData.hardCopyManifest){
				PrintRetManifest = true;
			}

			ProcessCombinedOutputFile(DestFile, stream, DocumentTemplate, Keywords, ReplaceVals, index, ItemVals, Data, StartLine, EndLine, DelKey, DelVals, DelIndex, InvKey, InvVals, InvIndex, ItemKeys, TaxInvoice, HeaderData,Page, NumberOfPages, MaxNoLinesPerPage, PrintRetManifest, pRetailManifest);
		});
	}

} /* ProcessDocumentInvoice */

function SaveLabelAndPrint(OutputFile, Data, data) {
    fs.writeFile(OutputFile, data, function(err) {
        if (err) {
            return console.log('Error writing to file:' + err);
        }

        if (ProcessEnv) {
            if (ProcessEnv != "LINUX") {
                return;
            }
        }

        var child = exec("lpr", ["-P", Data.labelprinterName, "-#1", "-s", "-h", OutputFile]);

        child.on('close', function(code) {
            try {
                fs.unlink(OutputFile);
            } catch(e){
                console.log('Failed to unlink file ' + OutputFile);
            }
            //fs.unlink(OutputFile);
            console.log("The courier file was saved!");
        });


        child.on('error', function(code) {
            try {
                fs.unlink(OutputFile);
            } catch(e){
                console.log('Failed to unlink file ' + OutputFile);
            }
            //fs.unlink(OutputFile);
            console.log("Failed!");
        });
    });
} /* SaveLabelAndPrint */

function PrintInternalLabel(OutputFile, TemplateFile, HeaderData, Data) {
    var LabelKeyValues = ['{order_no}', '{date}', '{carton_id}',
        '{contact}', '{ship_to}', '{address_1}', '{address_2}',
        '{city}', '{zip}', '{carton_no}', '{no_of_cartons}', '{date_time}'
    ];

    var LabelReplaceVals = GenerateLabelReplaceVals(Data, HeaderData);

    try {
        fs.writeFileSync(OutputFile, fs.readFileSync(TemplateFile, 'utf8'));
    } catch (e) {
        console.log(e);
    }

    try {
        for (var i = 0; i < LabelKeyValues.length; i++) {
            replace ({
				regex: LabelKeyValues[i],
				replacement: LabelReplaceVals[i],
				paths: [OutputFile],
				recursive: true,
				silent: true
            });
        }
    } catch (e) {
        console.log(e);
    }

    if (ProcessEnv) {
        if (ProcessEnv != "LINUX") {
            return;
        }
    }

    var child = exec("lpr", ["-P", Data.labelprinterName, "-#1", "-s", "-h", OutputFile]);

    child.on('close', function(code) {
       	try {
                fs.unlink(OutputFile);
        } catch(e){
        	console.log('Failed to unlink file ' + OutputFile);
        }
        //fs.unlink(OutputFile);
        console.log("The courier file was saved!");
    });

} /* PrintInternalLabel */

function ProcessCourierLabel(Data, HeaderData) {
    // Request to Courier to retrieve label
    rest.post(CourierLabelEndPoint, {
        data: { 'orderID': Data.orderId, 'route': Data.courierId, 'cartonID': Data.cartonId, 'noOfCartons': Data.NoOfCartons, 'cartonNo': Data.CartonNo, 'weight': Data.CartionWeight }
    }).on('complete', function(result) {
        var OutputFile = TempOutDir + Data.orderId + '.TXT'; // Output filename
        if (result instanceof Error) {
            console.log('Error getting courier label:', result.message);
            PrintInternalLabel(OutputFile, Templatedir + 'InternalLabelTemplate.prn', HeaderData, Data);
        } else {
            console.log('Got Courier Label:' + result);

            if (result != null && result != "") {
                SaveLabelAndPrint(OutputFile, Data, result);
            } else {
                console.log('Printing internal Courier Label');
                PrintInternalLabel(OutputFile, Templatedir + 'InternalLabelTemplate.prn', HeaderData, Data);
            }
        }
    });

} /* ProcessCourierLabel */

function ProcessCourierNotification(Data, HeaderData) {
    // Request to Courier to notify about order
    rest.post(CourierNotifyEndPoint, {
        data: { 'orderID': Data.orderId, 'route': Data.courierId, 'noOfCartons': Data.NoOfCartons }
    }).on('complete', function(result) {
        if (result instanceof Error) {
            console.log('Error notifying courier:', result.message);
        } else {
            console.log('Courier Order notification: ' + result);
            ProcessCourierLabel(Data, HeaderData);
        }
    });
} /* ProcessCourierNotification */
