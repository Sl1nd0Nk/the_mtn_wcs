var path        = require('path')
var log4js      = require('log4js')
var logger      = log4js.getLogger('ROUTES')
var formidable  = require('formidable')
var fs          = require('fs')
var templatesjs = require('templatesjs')
var DBAccess    = require('../module/dbAccessModule')
var request     = require('request')
var config      = require('vsu_rdt')

logger.level    = 'debug'

module.exports = function(app) {
    var pageRendered= path.join(__dirname + '/../public/views/vsuPicking.html')
    var form = new formidable.IncomingForm();

    var list = {
        toteID      : '<!-- -->',
        endPoint    : '<!-- -->',
        lblScanShelf: '<!-- -->',
        locationID  : '<!-- -->',
        itemDesc    : '<!-- -->',
        itemCode    : '<!-- -->',
        scanItem    : '<!-- -->',
        actualItemCode: '<!-- -->',
        actualLocID: '<!-- -->',
        pickQTY : '<!-- -->',
        confirmQty : '<!-- -->',
        actualQty : '<!-- -->',
        btnDisplay : '<!-- -->',
        inputBox : '<!-- -->',
        errorMessage : '<!-- -->',
        textBox: '<!-- -->',
        btnLogin    : '<!-- -->'
    }

    app.post('/home', function (req, res) {
        var loginData = {username:req.body.txtInput, password:req.body.txtTextbox}
        console.log('USERNAME: ' + loginData.username + ' PASSWORD: ' + loginData.password)
        DBAccess.getUser(loginData, function(respData){
            if(respData.code == '00'){
                list.lblToteLabel   = 'ToteID'
                list.endPoint       = 'getPickList'
                list.inputBox       = '<input type="text" id="txtInput" name="txtInput" autofocus>' + '<br><br>'
                list.textBox        = '<!-- -->'
                list.btnLogin       = '<!-- -->'
                list.errorMessage='<!-- -->'
                setAndRender(list, req, res)
            }else{
                list.errorMessage = '<label style="color: red">ERROR USER NOT FOUND!.</label>' + '<br><br>'
                setAndRender(list, req, res)
            }
        })
    })

    app.get('/', function (req, res) {
        list.lblToteLabel   = 'Login'
        list.endPoint       = 'home'
        list.inputBox       = '<input type="text" id="txtInput" name="txtInput" placeholder="Username">' + '<br><br>'
        list.textBox        = '<input type="password" id="txtTextbox" name="txtTextbox" placeholder="Password">' + '<br><br>'
        // list.btnLogin       = '<button>Login</button>'
        // setAndRender(list, req, res)
	list.btnLogin       = '<input type="submit"></input>'
        setAndRender(list, req, res)
    })

    app.post('/getPickList', function (req, res) {
        var locationFound = false
        DBAccess.getVsuItems(req.body.txtInput, function (respData) {
            if (respData.code == '00') {
                var x = 0
                while (x < respData.data.orders.length) {
                    var y = 0
                    while (y < respData.data.orders[x].items.length) {
                        if (respData.data.orders[x].items[y].location.substr(0, 3) == 'VSU' && !respData.data.orders[x].items[y].picked) {
                            locationFound = true
                            request({
                                method:'POST',
                                url: config.vsuRequest.url,
                                json:{location:respData.data.orders[x].items[y].location, desc:respData.data.orders[x].items[y].itemDescription}
                            }, function (error, body, response) {
                                if (!error && body) {
                                    list.toteID = req.body.txtInput
                                    list.lblScanShelf = 'Scan Shelf Location' + '<br><br>'
                                    list.locationID = respData.data.orders[x].items[y].location + '<br><br>'
                                    list.actualLocID=respData.data.orders[x].items[y].location
                                    list.errorMessage='<!-- -->'
                                    list.endPoint     = 'getItems'
                                    setAndRender(list, req, res)
                                } else {
                                    list.errorMessage = '<label style="color: red">Error connecting to the VSU. Contact Support.</label>' + '<br><br>'
                                    setAndRender(list, req, res)
                                }
                            })
                            break
                        }
                        y++
                    }
                    if (locationFound) {
                        break
                    }
                    x++
                }

                if (!locationFound) {
                    list.errorMessage = '<label style="color: red">No VSU Location for this tote.</label>' + '<br><br>'
                }
            } else {
                list.toteID = req.body.txtInput
                list.errorMessage = '<label style="color: red">Picklist not found.</label>' + '<br><br>'
                setAndRender(list, req, res)
            }
        })
    })

    app.post('/getItems', function (req, res) {
        console.log('TOTE ID: ' + list.toteID)
        DBAccess.getVsuItems(list.toteID, function (respData) {
            if (respData.code == '00') {
                var itemFound = false
                var locationString = 'Location: ' + list.locationID

                var x = 0
                while (x < respData.data.orders.length) {
                    var y = 0
                    while (y < respData.data.orders[x].items.length) {
                        if (req.body.txtInput == respData.data.orders[x].items[y].location && !respData.data.orders[x].items[y].picked) {
                            list.lblScanShelf = locationString
                            list.locationID = '<!-- -->' + '<br><br>'
                            list.scanItem = 'SCAN ITEM' + '<br><br>'
                            list.itemCode = respData.data.orders[x].items[y].itemCode + '<br><br>'
                            list.itemDesc = respData.data.orders[x].items[y].itemDescription + '<br><br>'
                            list.actualItemCode= respData.data.orders[x].items[y].itemCode
                            list.errorMessage = '<!-- -->'
                            list.endPoint     = 'getItemQty'
                            itemFound = true
                            setAndRender(list, req, res)
                            break
                        }
                        y++
                    }
                    if(itemFound) {
                        break
                    }
                    x++
                }

                if (!itemFound) {
                    // list.toteID = req.body.txtInput
                    list.errorMessage = '<label style="color: red">Location not found.</label>' + '<br><br>'
                    setAndRender(list, req, res)
                }
            } else {
                // list.toteID = req.body.txtInput
                list.errorMessage = '<label style="color: red">Picklist not found.</label>' + '<br><br>'
                setAndRender(list, req, res)
            }
        })
    })

    app.post('/getItemQty', function (req, res) {
        DBAccess.getVsuItems(list.toteID, function (respData) {
            if (respData.code == '00') {
                console.log('LOCATION ID: ' + list.actualItemCode)
                var itemFound = false
                var locationString = 'Location: ' + list.locationID

                var x = 0
                while (x < respData.data.orders.length) {
                    var y = 0
                    while (y < respData.data.orders[x].items.length) {
                        if (req.body.txtInput == respData.data.orders[x].items[y].barcode && !respData.data.orders[x].items[y].picked) {
                            list.lblScanShelf = locationString
                            list.locationID = 'ITEM: ' + list.itemDesc + '<br><br>'
                            list.scanItem = '<!-- -->'
                            list.itemCode = '<!-- -->'
                            list.itemDesc = '<!-- -->'
                            actualItemCode= respData.data.orders[x].items[y].itemCode + '<br><br>'
                            list.pickQTY  = 'PICK: ' + respData.data.orders[x].items[y].qty + ' EACH <br><br>'
                            list.actualQty = respData.data.orders[x].items[y].qty
                            list.confirmQty = 'CONFIRM QTY' + '<br><br>'
                            list.errorMessage = '<!-- -->'
                            list.endPoint     = 'getRelease'
                            itemFound = true
                            setAndRender(list, req, res)
                            break
                        }
                        y++
                    }
                    if(itemFound) {
                        break
                    }
                    x++
                }

                if (!itemFound) {
                    list.errorMessage = '<label style="color: red">Item not found.</label>' + '<br><br>'
                    setAndRender(list, req, res)
                }
            } else {
                list.errorMessage = '<label style="color: red">Item not found.</label>' + '<br><br>'
                setAndRender(list, req, res)
            }
        })
    })

    app.post('/getRelease', function (req, res) {
        DBAccess.getVsuItems(list.toteID, function (respData) {
            if (respData.code == '00') {
                console.log('LOCATION ID: ' + list.actualLocID)
                var itemFound = false
		        var itemUpdate = false
                var locationString = 'Location: ' + list.locationID

                var x = 0
                while (x < respData.data.orders.length) {
                    var y = 0
                    while (y < respData.data.orders[x].items.length) {
                        if (req.body.txtInput == respData.data.orders[x].items[y].qty && !respData.data.orders[x].items[y].picked && respData.data.orders[x].items[y].location == list.actualLocID) {
                            console.log(req.body.txtInput + ' | ' + respData.data.orders[x].items[y].qty + ' | ' + respData.data.orders[x].items[y].picked)
                            itemFound = true
                            DBAccess.updateVSUItem(list.toteID, list.actualLocID,req.body.txtInput, function (updated) {
                                if(updated.code == '00') {
				                    itemUpdate = true
                                    DBAccess.getVsuItems(list.toteID, function (nextData) {
                                        if(nextData.code == '00') {
                                            var nextItemFound = false
					                        var reqSent = false
                                            locationString = 'Location: ' + list.locationID
                                            var z = 0
                                            while (z < nextData.data.orders.length) {
                                                var c = 0
                                                while (c < nextData.data.orders[z].items.length) {
                                                    if(nextData.data.orders[z].items[c].location.substr(0, 3) == 'VSU' && !nextData.data.orders[z].items[c].picked) {
                                                        list.lblScanShelf = 'Scan Shelf Location' + '<br><br>'
                                                        list.locationID = respData.data.orders[z].items[c].location + '<br><br>'
                                                        list.actualLocID=respData.data.orders[z].items[c].location
                                                        list.errorMessage='<!-- -->'
							                            list.pickQTY = '<!-- -->',
                                                        list.confirmQty = '<!-- -->',
                                                        list.endPoint     = 'getItems'
                                                        nextItemFound = true
							                            list.toteID = nextData.data.toteID
							                            console.log('______TOTE ID_____: ' + list.toteID)
                                                        if (!reqSent){
                                                        request({
                                                            method:'POST',
                                                            url:config.vsuRequest.url,
                                                            json:{location:respData.data.orders[z].items[c].location, desc:respData.data.orders[x].items[y-1].itemDescription}
                                                        }, function (error, body, response) {
                                                            if (!error && body) {
								                                reqSent = false
                                                                setAndRender(list, req, res)
                                                            } else {
								                                reqSent = false
                                                            }
                                                        })
							                        }
			                                            reqSent = true
                                                        break
                                                    }
						                            if (nextItemFound) {
                                                        break
                                                    }
                                                    c++
                                                }
                                                if(nextItemFound) {
                                                    break
                                                }
                                                z++
                                            }

                                            if (!nextItemFound) {
                                                list.lblScanShelf = '<!-- -->'
                                                list.locationID = '<!-- -->'
                                                list.scanItem = '<!-- -->'
                                                list.itemCode = '<!-- -->'
                                                list.itemDesc = '<!-- -->'
                                                actualItemCode= '<!-- -->'
                                                list.pickQTY  = '<!-- -->'
                                                list.actualQty = '<!-- -->'
                                                list.confirmQty = '<!-- -->'
                                                list.btnDisplay = '<button type="submit" name="btnTote">Release Tote</button>'
                                                list.errorMessage = '<!-- -->'
                                                list.inputBox = '<!-- -->'
                                                list.endPoint     = 'releaseTote'
                                                itemFound = true
                                                setAndRender(list, req, res)
                                            }
                                        } else {
                                            list.errorMessage = '<label style="color: red">Picklist Not Found.</label>' + '<br><br>'
                                            setAndRender(list, req, res)
                                        }
                                    })
                                } else if(updated.code == '01') {
                                    itemFound = true
                                    list.errorMessage = '<label style="color: red">Picklist Not Found.</label>' + '<br><br>'
                                    setAndRender(list, req, res)
                                }
                            })
                        }
			            // if (itemUpdate) {
                        //     break
                        // }
                        y++
                    }
                    if(itemFound) {
                        break
                    }
                    x++
                }

                if (!itemFound) {
                    list.errorMessage = '<label style="color: red">Invalid Number Of Items.</label>' + '<br><br>'
                    setAndRender(list, req, res)
                }
            } else {
                list.errorMessage = '<label style="color: red">Picklist Not Found.</label>' + '<br><br>'
                setAndRender(list, req, res)
            }
        })
    })

    app.post('/releaseTote', function (req, res) {
        request({
            method:'POST',
            url: config.ptlRelease.url,
            json:{toteID: list.toteID, location: '14'}
        }, function (error, body, response) {
            if (!error && body) {
                list.toteID = '<!-- -->'
                list.btnDisplay = '<!-- -->'
                list.endPoint     = 'getPickList'
                list.inputBox = '<input type="text" id="txtInput" name="txtInput" autofocus>' + '<br><br>'
                request({
                    method:'POST',
                    url: config.vsuRequest.url,
                    json:{location:'VSUN01Z14T00P00', desc:'CLOSE'}
                }, function (error, body, response) {
                    if (!error && body) {
                        setAndRender(list, req, res)
                    } else {
                        setAndRender(list, req, res)
                    }
                })
               // setAndRender(list, req, res)
            } else {
                list.toteID = '<!-- -->'
                list.btnDisplay = '<!-- -->'
                list.endPoint     = 'getPickList'
                list.inputBox = '<input type="text" id="txtInput" name="txtInput" autofocus>' + '<br><br>'
                request({
                    method:'POST',
                    url: config.vsuRequest.url,
                    json:{location:'VSUN01Z14T00P00', desc:'ERROR RELEASING TOTE.'}
                }, function (error, body, response) {
                    if (!error && body) {
                        setAndRender(list, req, res)
                    } else {
                        setAndRender(list, req, res)
                    }
                })
                // setAndRender(list, req, res)
            }
        })
    })

    function processRequest (req, res) {
        form.parse(req, function(error, fields, files) {
            setAndRender(list, req, res)
        })
    }

    function setAndRender (list, req, res) {
        fs.readFile(pageRendered, function (error, data) {
            if (!error && data) {
                templatesjs.set(data, function(err, setData) {
                    if (!err && setData) {
                        templatesjs.renderAll(list, function (renderErr, renerData) {
                            if(renderErr) {
                                throw renderErr;
                            } else {
                                res.write(renerData);
                                res.end();
                            }
                        })
                    } else {
                        throw err
                    }
                })
            } else {
                throw error
            }
        })
    }
}
