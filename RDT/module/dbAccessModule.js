var PickList = require('pickListModel')
var Users    = require('userModel');
var moment   = require('moment');
var PickLine = require('PickLineUpdatesModel');

var DBAccess = function () {}

DBAccess.prototype.getVsuItems = function (toteID, callback) {
    PickList.findOne({toteID:toteID}, function (error, docs) {
        if (!error && docs) {
            callback({code: '00', message: 'success', data: docs})
        } else {
            callback({code: '01', message: 'fail.pickList.ntf', data: error})
        }
    })
}

DBAccess.prototype.getUser = function(userDetails, callback){
    var username = userDetails.username
    var password = userDetails.password

    Users.findOne({username:username, password:password}, function(error, docs){
        if(!error && docs){
            callback({code: '00', message: 'success', data: docs})
        }else{
            callback({code: '01', message: 'fail.User.ntf', data: error})
        }
    })
}

function CreatePickLineUpdate(uPL, ordNdx, lnIndx){
	PickLine.findOne({'PickListID': uPL.orders[ordNdx].pickListID,
					  'PickListLine': uPL.orders[ordNdx].items[lnIndx].pickListOrderLine,
					  'OrderId': uPL.orders[ordNdx].orderID}, function(err, PlUpdateRec){
		if(err){
			return;
		}

		if(PlUpdateRec){
			return;
		}

		let NewRec = new PickLine({
			PickListID: uPL.orders[ordNdx].pickListID,
			WcsId: uPL.toteID,
			PickListLine: uPL.orders[ordNdx].items[lnIndx].pickListOrderLine,
			OrderId: uPL.orders[ordNdx].orderID,
			Required: true,
			CreateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
		});

		NewRec.save(function(err, savedDoc){
			WriteToFile('Marked picklist: ' + uPL.orders[ordNdx].pickListID + ' | Line: ' + uPL.orders[ordNdx].items[lnIndx].pickListOrderLine + ' | PickLineRequired to true');
			return;
		});
	});
} /* CreatePickLineUpdate */

DBAccess.prototype.updateVSUItem = function (toteID, locationID,quantity, callback) {
    var updatedItem = false
    PickList.findOne({toteID:toteID}, function (error, docs) {
        if (!error && docs) {
            var x = 0
            while (x < docs.orders.length){
                var y = 0
                while (y < docs.orders[x].items.length) {
                    if (docs.orders[x].items[y].location == locationID) {
                        console.log('LOCATION ID: ' + docs.orders[x].items[y].location + ' | ' + locationID)
                        docs.orders[x].items[y].picked = true
                        docs.orders[x].items[y].pickQty = quantity

                        CreatePickLineUpdate(docs, x, y);
                        docs.save({}, function (err, saved) {
                            if (!err && saved) {
                                updatedItem = true
                                callback({code:'00', message:'success', data:saved})
                            } else {
                                updatedItem = true
                                callback({code: '01', message:'fail', data:err})
                            }
                        })
                        break
                    }
                    y++
                }
                if(updatedItem){
                    break
                }
                x++
            }
        } else {
            callback({code: '01', message:'fail', data:error})
        }
    })
}

module.exports = new DBAccess()
