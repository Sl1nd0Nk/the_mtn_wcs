var express = require('express')
var port    = require('vsu_rdt').port
var log4js  = require('log4js')
var logger  = log4js.getLogger('SERVER')
var bodyParser= require('body-parser')
var mongoose = require('mongoose')
var config = require('vsu_rdt')

logger.level= 'debug'

mongoose.connect(config.mongo.url, {useMongoClient:true});

var app     = express()

app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname+'/public'))

require('./appRoutes/routes')(app)

app.listen(port, function () {
    logger.debug('Listening on port: ' + port)
})
